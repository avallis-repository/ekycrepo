<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<%
	response.setHeader("Cache-Control", "no-cache"); 
	response.setHeader("Pragma", "no-cache"); 
	response.setDateHeader("Expires", -1);
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Avallis | eKYC | Login</title>
 <link href="https://fonts.googleapis.com/css?family=Karla:400,700&display=swap" rel="stylesheet">
<link rel="stylesheet" href="${contextPath}/vendor/bootstrap453/css/bootstrap.css">
<link rel="stylesheet" href="${contextPath}/vendor/avallis/css/login.css">

<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet" />
<script src="${contextPath}/vendor/jquery/jquery.js"></script>
	<script src="${contextPath}/vendor/jquery/popper.min.js"></script>
	<script src="${contextPath}/vendor/bootstrap453/js/bootstrap.js"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
	
	<script src="${contextPath}/vendor/avallis/js/Common Script.js"></script>
	<script src="${contextPath}/vendor/avallis/js/login.js"></script>

</head>
<body id="aallis_ekyc_loginpage">
	<div class="d-flex align-items-center min-vh-100 py-3 py-md-0">
	<div class="container">
		<div style="background-color: transparent">

			<div class="row no-gutters">
				<div class="col-md-6 col-sm-12 col-xs-12"  style="background-color: transparent">&nbsp;</div>

				<div class="col-md-6 col-sm-12 col-xs-12">

					<div class="loginBox">
                         <form name="frmLogin" method="POST"  >

							<div class="row" style="background: white; border-radius: 10px; color: #1d655cf7;">
								<div class="col-4">

									<img src="${contextPath}/vendor/avallis/img/logo.png" alt="Avallis Logo"
										style="height: 60px;">

								</div>

								<div class="col-8 center">Sign in to your account</div>

							</div>


							<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
							<p>UserId</p>
							<input type="text" name="txtFldUserName" id="txtFldUserName" Placeholder="Type your UserId" autocomplete="off"  autofocus="true"/>
							 <div class="logn-ErrorMsg hide mb-2" id="txtFldUserNameError">UserId is Required !</div>	
							
							<p>Password</p>
							<input type="password" name="txtFldpassword" id="txtFldpassword" placeholder="Type your password" autocomplete="off"  autofocus="true"/> 
							 <div class="logn-ErrorMsg hide" id="txtFldpasswordError">Password is Required !</div>
							
							<input type="button" name="sign-in" value="Login" onclick="validateLogin()">
							
							<span class="logn-ErrorMsg hide" id="lognError">${error_message}</span><p>&nbsp;</p>
							
							<span class="logn-ErrorMsg hide">${logout_message}</span><p>&nbsp;</p>
							
							<a href="#">&copy;&nbsp;<script>document.write(new Date().getFullYear());</script> - <strong>Avallis Financial Pte Ltd.,</strong> </a>
							
									<input type="hidden" value="${KYC_APPROVE_DISTID}" name="hTxtFldDistId"/>
									<input type="hidden" value="${KYC_APPROVE_ADVID}" name="hTxtFldAdvId"/>
									<input type="hidden" value="${KYC_APPROVE_MGRACSFLG}" name="hTxtFldMgrAcsFlg"/>
									<input type="hidden" value="${KYC_APPROVE_FNAID}" name="hTxtFldFnaId"/>
									<input type="hidden" value="${KYC_APPROVE_FLAG}" name="hTxtFldFlag"/>
						</form>
					</div>

				</div>
			</div>
		</div>

	</div>
	</div>
	
	
	
</body>


<script>

localStorage.openpages = Date.now();
var onLocalStorageEvent = function(e){
  if(e.key == "openpages"){
    // Emit that you're already available.
    localStorage.page_available = Date.now();
  }
  if(e.key == "page_available"){
	  //swal.fire("One more page already open")
	 
	  var firstPath = window.location.pathname.split('/')[1];
	  
	  var url = location.protocol +'//'+ location.host+'/'+ firstPath
	  
	  
    alert("The same " + url + " is open in another browser  window or tab, close that one and try again");
	    
    window.close();return false;
  }
};
window.addEventListener('storage', onLocalStorageEvent, false);


 
</script>
</html>
