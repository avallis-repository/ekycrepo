<!DOCTYPE html>

<html lang="en">
<head>
  <link href="vendor/FileUpload/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
  </head>
<body>
<div class="modal fade" id="uplodModal" aria-modal="true" role="dialog" style="padding-right: 15px;" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-xl  modal-dialog-scrollable"><!-- style="max-width: 92%;"  -->
      <div class="modal-content">
      
       
        
        <!-- Modal body -->
        <div class="modal-body" style="overflow:hidden">
            
          <div class="row">
                <div class="col-12">
                      <div class="upload">
					 <div class="upload-files">
					  <div class="row">
					            <div class="col-12">
					               <header>
					                  <div class="row">
							             <div class="col-4">
							                   <div class="">
									             <div class="pl-2 pt-3"><img src = "vendor/avallis/img/custName.png" class="rounded-circle" alt="custName"> 
									             <small><span class="badge badge-light ltrspace-01 p-2" style="cursor: default;" title="Client Name : ${CURRENT_CUSTNAME}">${CURRENT_CUSTNAME}</span>
									             </small></div>
							                   </div>
							             </div>
							              <div class="col-4">
							                        <p>
												    <i class="fa fa-cloud-upload" aria-hidden="true"></i>
												    <span class="up">up</span>
												    <span class="load">load</span>
												   </p>
										   </div>
										   
										   <div class="col-4">
										   	<div class="btn-group float-right">
										   	
										   	 <button type="button" class="btn btn-bs3-prime btn-sm m-2 p-2 ltrspc02 d-none" id="btnUplodAndEmail">Save & Email</button>
										   	 <!-- <button type="button" class="btn btn-bs3-prime btn-sm m-2 p-2 d-none" id="btnWithoutUplodAndEmail">Email Only</button> -->
										     <button type="button" class="btn btn-bs3-success btn-sm m-2 p-2 ltrspc02" id="btnUplodCustAttchFile">Save</button>
										     <button type="button" class="btn btn-bs3-warning amber btn-sm m-2 p-2 ltrspc02" data-dismiss="modal">Exit</button>
										  	</div>
										  </div>
										   
										 
									 </div>
										  </header>
								 </div>
								 
				          
				            
				    <div class="container-fluid  my-4" style="max-height: calc(100vh - 210px);  overflow-y: auto;">
							<div class="row">
					            <div class="col-9">
					                <div class="card">
					                    <div class="card-header" id="cardHeaderStyle-22"><i class="fa fa-cloud-upload" aria-hidden="true"></i> New Uploads&nbsp;
					                    <span class="font-sz-level5 font-normal pl-1">[Click Browse Button to Upload Files.]</span>
					                     </div>
					            	     <div class="card-body">
					            	          <form enctype="multipart/form-data" class="uploadMultipartFile">
									              <div class="file-loading">
										  		       <input id="file-0"  name="file-0[]"  type="file"  multiple="multiple"  multiple data-theme="fas" onchange="handleFiles(this.files)">
										  	      </div>
								 	         </form>
					            	     </div>
					            	</div>
					             </div>
					            
					            <div class="col-3">
					                 <div class="card h-100">
					                     <div class="card-header" id="cardHeaderStyle-22"><i class="fa fa-cloud-download" aria-hidden="true"></i> Downloads
					                     <!--poovathi show file download count on 15-06-2021 -->
					                      <small class="badge badge-pill badge-light right p-1 font-sz-level7" id="fileCountTxt" ></small></div>
						                 <div class="card-body" style="height: 57.5vh;
						                       overflow-y: scroll;overflow-x:hidden;padding: 0.5rem !important;border: 1px solid #5675C2;">
						                       
						                        <div class="row noDocFoundSec mt-5 show" id="noDocFoundSec">
						                             <div class="col-1"></div>
									                 <div class="col-10">
									                       <img src="vendor/avallis/img/noFiles.png" class="rounded mx-auto d-block img-fluid  prod-img" id="noDocImg"
									                        style="width: 75%;border:1px solid #80808042;padding: 0px 12px;background: aliceblue;">
									                        <p class="center noData mt-2" id="noDocFoundInfo">
									                        <img src="vendor/avallis/img/warning.png" class="">&nbsp;No File(s) Available&nbsp;</p>
						                            </div>
						                            <div class="col-1"></div>
						                        </div>
						                       
						                       
						                       <div class="row hide" id="DocListSec">
						                            <div class="col-12">
						                                  <div class="list-group" id="dynaAttachNTUCList">
						                            </div>
						                       </div>
						                       </div>
						                  </div>
					               </div>
					          </div>
					 </div>
				</div> 
                  
		                   
                	 </div>
					</div> 
                 </div>
           </div>
        </div>
       
        
        
      </div>
    </div>
  </div>
  
</div>
  
  </body>
    <script src="vendor/FileUpload/js/piexif.js" type="text/javascript"></script>
    <script  src="vendor/FileUpload/js/sortable.js" type="text/javascript"></script>
    <script  src="vendor/FileUpload/js/fileinput.js" type="text/javascript"></script>
    <script  src="vendor/avallis/js/clientAttachments.js" type="text/javascript"></script>
    <script> //strMastAttachCateg = ${MASTER_ATTACH_CATEG_LIST}
    currentCustId = '${CURRENT_CUSTID}';
    </script>
  
  </html>