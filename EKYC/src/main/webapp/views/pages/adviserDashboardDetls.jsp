

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>eKYC - Sent for Approvals</title>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
 
 <link rel="stylesheet" href="${contextPath}/vendor/bootstrap453/css/bootstrap.css">
 <link rel="stylesheet" href="${contextPath}/views/newapproval/css/normalize.min.css">
 <link rel="stylesheet" href="${contextPath}/views/newapproval/css/dataTables.bootstrap4.min.css">
 <link rel="stylesheet" href="${contextPath}/views/newapproval/css/ekyc-layout.css">
 

 
 <script>
 var baseUrl="<%=request.getContextPath()%>";
 </script>

<script src="${contextPath}/views/newapproval/js/jquery.min.js"></script>
<script src="${contextPath}/views/newapproval/js/popper.min.js"></script>
<script src="${contextPath}/vendor/bootstrap453/js/bootstrap.js"></script>
<script src="${contextPath}/views/newapproval/js/jquery.dataTables.min.js"></script>
<script src="${contextPath}/views/newapproval/js/dataTables.bootstrap4.min.js"></script>
<script src="${contextPath}/vendor/avallis/js/Common Script.js"></script>
<script src="${contextPath}/vendor/avallis/js/Constants.js"></script>
<script src="${contextPath}/views/newapproval/js/Constants.js"></script>
<script src="${contextPath}/views/newapproval/js/commonScript.js"></script>
<script src="${contextPath}/views/newapproval/js/advdashboard.js"></script>

    <!--  html to pdf file script files-->
	      <!-- <script src="vendor/HTML2PDF/canvg.js"></script>
	       <script src="vendor/HTML2PDF/d3SvgToPng.js"></script>
	       <script src="vendor/HTML2PDF/html2canvas.js"></script>
	       <script src="vendor/HTML2PDF/html2canvas.min.js"></script>
	       <script src="vendor/HTML2PDF/jspdf.debug.js"></script>
	       <script src="vendor/HTML2PDF/jspdf.min.js"></script>
	       <script src="vendor/HTML2PDF/jspdf.plugin.autotable.js"></script>
	       <script src="vendor/HTML2PDF/moment.min.js"></script> 
	       <script src="vendor/HTML2PDF/rgbcolor.js"></script>
	       <script src="vendor/HTML2PDF/StackBlur.js"></script> -->
	        <!--  html to pdf file script files-->
	        
	    <!-- poovathi commented on 02-12-2021 -->
	    <!-- <script src="vendor/HTML2PDF/jspdf.debug.js" type="text/javascript" ></script>
	    <script src="vendor/HTML2PDF/jspdf.plugin.autotable.js"  type="text/javascript" ></script>
	    <script src="vendor/HTML2PDF/html2canvas.min.js" type="text/javascript" ></script>
	    <script src="vendor/HTML2PDF/rgbcolor.js" type="text/javascript"></script>
	    <script src="vendor/HTML2PDF/StackBlur.js" type="text/javascript" ></script>
	    <script src="vendor/HTML2PDF/canvg.js" type="text/javascript" ></script>
	    <script src="vendor/HTML2PDF/d3SvgToPng.js" type="text/javascript" ></script>
	    <script src="vendor/HTML2PDF/moment.min.js"></script>
        <script src="vendor/avallis/js/ekyc.html2pdf.js"></script> -->
        
	<!--  html to pdf file script files End -->

		  
		
</head>		
<body id="page-top">
<div id="cover-spin"></div>
 <div id="tooltip"></div>
        <div class="container-fluid" id="container-wrapper" style="margin-top: 0px !important;font-size: 75%">
              <div class="card mb-1 ">
			      <div class="card-body">
			      
			      <!--poovathi add on 16-07-2021  -->
			      <div class="row">
			           <div class="col-12">
			                  <button class="btn btn-bs3-info p-1 right bold d-none" onclick="generateHTMLtoPDF()">Gen. Adviser Submissions PDF</button>
			           </div>
			      </div>
			      <!--end  -->
			      
		<div class="row">
		
		 <div class="tab-regular" id="clntProdTabs" style="width:100%;">
		 
		  <ul class="nav nav-tabs small" id="landingpage" role="tablist" style="list-style-type:bullet;">
                <li class="nav-item" style="list-style-type:none;">
                	<a class="nav-link active" id="advdasbord" data-toggle="tab" href="#homepage" role="tab" 
                	aria-controls="homepage" aria-selected="true" title=""  onclick="loadPie('ADVISER')">
                		<img src="${contextPath}/vendor/avallis/img/analyze.png"><span>Adviser Submissions</span>
                	</a>
                	<!-- id="homepage-tab" -->
                </li>
                
                
                <c:if test = "${KYC_APPROVE_MGRACSFLG eq 'Y'}">
            		<li class="nav-item" style="list-style-type:none;">
                	<a class="nav-link"  data-toggle="tab" id="managdasbord" href="#managerdashboardspage" role="tab"
                	 aria-controls="managerdashboardspage" aria-selected="true" title="" onclick="loadPie('MANAGER')">
                		<img src="${contextPath}/vendor/avallis/img/analyze.png" ><span>Manager Approvals</span>
                	</a>
                    </li> 
               </c:if>
            		 
            	 
            		 
                  
                <!-- id="detailspage-tab" -->
            </ul>
            
            
           
                              <div class="tab-content" id="myTabContent" style="margin-top: 10px;">
                                 <div class="tab-pane fade show active" id="homepage" role="tabpanel" aria-labelledby="client1-tab">
									  
									  <!-- HOME PAGE(DASHBOARD CONTENT START HERE -->
									       <div class="row">

    <div class="col-3">
    	<div class="right-tabs clearfix">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs block">
          <li class="nav-item ">
            <a class="nav-link" data-toggle="tab" href="#shome"><span class="txt_colr"><img src="${contextPath}/vendor/avallis/img/survey.png"> FNA Status</span></a>
          </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content block">

          <div class="tab-pane container active" id="shome">
          
            <div class="row stats cht cursor-pointer mt-1" onclick="openListAdv('PENDING')">

              <div class="col-sm-12">

                <div class="row">

                  <div class="col-sm-12">

                    <div class="row">

                      <div class="col-sm-5">

                        <span style="vertical-align: middle; font-size:100%;" ><i class="fa fa-question-circle"></i>&nbsp;Pending</span>

                      </div>

                      <div class="col-sm-4">

                        <div class="progress" style="height:25px">
                          <div class="progress-bar bg-ekyc-secondary progress-bar-striped progress-bar-animated" style="width:${FNA_MGR_STATUS_LIST.PENDING}%;height:25px;font-size:180%;"></div>
                        </div>

                      </div>

                      <div class="col-sm-3">

                        <span><c:if test="${not empty FNA_MGR_STATUS_LIST}">
		                      	 ${FNA_MGR_STATUS_LIST.PENDING}
		                      	 </c:if>	</span>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>
          
          

            <div class="row stats cht cursor-pointer mt-1" onclick="openListAdv('APPROVED')">

              <div class="col-sm-12">

                <div class="row">

                  <div class="col-sm-12">

                    <div class="row">

                      <div class="col-sm-5">

                        <span style="vertical-align: middle; font-size:100%;"><i class="fa fa-check-circle-o"></i>&nbsp;Approved</span>

                      </div>

                      <div class="col-sm-4">

                        <div class="progress" style="height:25px">
                          <div class="progress-bar bg-ekyc progress-bar-striped progress-bar-animated" style="width:${FNA_MGR_STATUS_LIST.APPROVE}%;height:25px;font-size:180%;"></div>
                        </div>

                      </div>

                      <div class="col-sm-3">

                        <span><c:if test="${not empty FNA_MGR_STATUS_LIST}">
		                    	${FNA_MGR_STATUS_LIST.APPROVE}
		                     </c:if> </span>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>

          

            <div class="row stats cht cursor-pointer mt-1" onclick="openListAdv('REJECTED')">

              <div class="col-sm-12">

                <div class="row">

                  <div class="col-sm-12">

                    <div class="row">

                      <div class="col-sm-5">

                        <span style="vertical-align: middle;font-size:100%;"><i class="fa fa-times-circle"></i>&nbsp;Rejected</span>

                      </div>

                      <div class="col-sm-4">

                        <div class="progress" style="height:25px">
                          <div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" style="width:${FNA_MGR_STATUS_LIST.REJECT}%;height:25px;font-size:180%;"></div>
                        </div>

                      </div>

                      <div class="col-sm-3">

                        <span><c:if test="${not empty FNA_MGR_STATUS_LIST}">
		                      	 ${FNA_MGR_STATUS_LIST.REJECT}
		                      	 </c:if></span>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>
            
            
            <!-- poovathi add 07-02-2021 -->
            
             <div class="row stats cht cursor-pointer mt-1" onclick="openListAdv('PENDINGREQ')">

              <div class="col-sm-12">

                <div class="row">

                  <div class="col-sm-12">

                    <div class="row">

                      <div class="col-sm-5">

                        <span style="vertical-align: middle; font-size:100%;"><i class="fa fa-info-circle  text-info"></i>&nbsp;Pending Req.</span>

                      </div>

                      <div class="col-sm-4">

                        <div class="progress" style="height:25px">
                          <div class="progress-bar bg-info progress-bar-striped progress-bar-animated" 
                           style="width:${FNA_MGR_STATUS_LIST.PENDINGREQ}%;height:25px;font-size:180%;"></div>
                        </div>

                      </div>

                      <div class="col-sm-3">

                        <span><c:if test="${not empty FNA_MGR_STATUS_LIST}">
		                    	${FNA_MGR_STATUS_LIST.PENDINGREQ}
		                     </c:if> </span>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>
            
            
            <!--End  -->

          </div>
          
          <hr/>
          <div class="clearfix"></div>
          
          <section>
                  <div class="pieID pie adv"></div>
                    <ul class="pieID legend adv">
                         <li>
                            <em>Pending</em>
                            <span>${FNA_MGR_STATUS_LIST.PENDING}</span>
                         </li>
                         
                          <li>
                             <em>Approved</em>
                             <span>${FNA_MGR_STATUS_LIST.APPROVE}</span>
                          </li>
                          
                          <li>
                             <em>Rejected</em>
                             <span>${FNA_MGR_STATUS_LIST.REJECT}</span>
                         </li>
                         
                          
                         <!-- poovathi add on 02-07-2021 -->
                           <li>
                             <em>Pending Req.</em>
                             <span>${FNA_MGR_STATUS_LIST.PENDINGREQ}</span>
                          </li>
                         <!--  End-->
      
                    </ul>
            </section>

        </div>
        </div>
        
       
     
    </div>
    
    
    
    
    
    <div class="col-9">
   
    	<div class="right-tabs clearfix">
    	
    	<ul class="nav nav-tabs block"  id="yourAprovTabsAdv">
          <li class="nav-item">
            <a class="nav-link" onclick="openListcoladj('PENDING')" data-toggle="tab" href="#home" ><i class="fa fa-question-circle"></i><span>&nbsp;Pending</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" onclick="openListcoladj('APPROVE')" data-toggle="tab" href="#menu1" ><i class="fa fa-check-circle-o"></i><span>&nbsp;Approved</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" onclick="openListcoladj('REJECT')" data-toggle="tab" href="#menu2" ><i class="fa fa-times-circle-o"></i><span>&nbsp;Rejected</span></a>
          </li>
          
           <!--poovathi add on 02-07-2021  -->
           <li class="nav-item">
            <a class="nav-link"  onclick="openListcoladj('PENDINGREQ')" data-toggle="tab" href="#menu3"><i class="fa fa-info-circle text-info"></i><span>&nbsp;Pending Req.</span></a>
          </li>
          <!--End  -->
          
        </ul>
        
    	

        <!-- Tab panes -->
        <div class="tab-content">

          <div class="tab-pane active  mt-1" id="home">
          
          <table id="tblPendingAdv" class="table table-striped table-bordered hover"  style="width:100%">
				        <thead>
				            <tr>
				                <th><div>#</div></th>
				                <th><div style="width:100px">FNA ID</div></th>
				                <th><div style="width:180px">Client</div></th>				                
				                <th><div style="width:60px">Manager</div></th>
				                <th><div style="width:50px">Admin</div></th>
				                <th><div style="width:50px">Compl.</div></th>
				                <th><div style="width:60px">CustId</div></th>
				                <th><div style="width:60px">AdvId</div></th>
				                <th><div style="width:60px">Email</div></th>
				                <th><div style="width:40px">NTUC?</div></th>
				                <th><div style="width:20px">Mgr Appr By</div></th>
				                <th><div style="width:20px">Mgr Appr Date</div></th>
				                <th><div style="width:20px">Mgr Appr Rem</div></th>
				                
				                <th><div style="width:20px">Admin Appr By</div></th>
				                <th><div style="width:20px">Admin Appr Date</div></th>
				                 <th><div style="width:20px">Admin Appr Rem</div></th>
				                 
				                <th><div style="width:20px">Comp Appr By</div></th>
				                <th><div style="width:20px">Comp Appr Date</div></th>
				                 <th><div style="width:20px">Comp Appr Rem</div></th>
				                 
				                 <th><div style="width:20px">s1</div></th>
				                <th><div style="width:20px">s2</div></th>
				                 <th><div style="width:20px">s3</div></th>
				            </tr>
				        </thead>
				        <tbody>
				        </tbody>
			        </table>


          </div>

          <div class="tab-pane fade mt-1" id="menu1">
          
          	<table id="tblApprovedAdv" class="table table-striped table-bordered hover" style="width:100%">
				        <thead>
				           <tr>
				                <th><div>#</div></th>
				                <th><div style="width:100px">FNA ID</div></th>
				                <th><div style="width:180px">Client</div></th>				                
				                <th><div style="width:60px">Manager</div></th>
				                <th><div style="width:50px">Admin</div></th>
				                <th><div style="width:50px">Compl.</div></th>
				                <th><div style="width:60px">CustId</div></th>
				                <th><div style="width:60px">AdvId</div></th>
				                <th><div style="width:60px">Email</div></th>
				                <th><div style="width:40px">NTUC?</div></th>
				                <th><div style="width:20px">Mgr Appr By</div></th>
				                <th><div style="width:20px">Mgr Appr Date</div></th>
				                <th><div style="width:20px">Mgr Appr Rem</div></th>
				                
				                <th><div style="width:20px">Admin Appr By</div></th>
				                <th><div style="width:20px">Admin Appr Date</div></th>
				                 <th><div style="width:20px">Admin Appr Rem</div></th>
				                 
				                <th><div style="width:20px">Comp Appr By</div></th>
				                <th><div style="width:20px">Comp Appr Date</div></th>
				                 <th><div style="width:20px">Comp Appr Rem</div></th>
				                 
				                 <th><div style="width:20px">s1</div></th>
				                <th><div style="width:20px">s2</div></th>
				                 <th><div style="width:20px">s3</div></th>

				            </tr>
				        </thead>
				        <tbody>
				        </tbody>
			        </table>
          </div>

          <div class="tab-pane fade mt-1" id="menu2">
         	 <table id="tblRejectedAdv" class="table table-striped table-bordered hover" style="width:100%">
				        <thead>
				            <tr>
				                <th><div>#</div></th>
				                <th><div style="width:100px">FNA ID</div></th>
				                <th><div style="width:180px">Client</div></th>				                
				                <th><div style="width:60px">Manager</div></th>
				                <th><div style="width:50px">Admin</div></th>
				                <th><div style="width:50px">Compl.</div></th>
				                <th><div style="width:60px">CustId</div></th>
				                <th><div style="width:60px">AdvId</div></th>
				                <th><div style="width:60px">Email</div></th>
				                <th><div style="width:40px">NTUC?</div></th>
				                <th><div style="width:20px">Mgr Appr By</div></th>
				                <th><div style="width:20px">Mgr Appr Date</div></th>
				                <th><div style="width:20px">Mgr Appr Rem</div></th>
				                
				                <th><div style="width:20px">Admin Appr By</div></th>
				                <th><div style="width:20px">Admin Appr Date</div></th>
				                 <th><div style="width:20px">Admin Appr Rem</div></th>
				                 
				                <th><div style="width:20px">Comp Appr By</div></th>
				                <th><div style="width:20px">Comp Appr Date</div></th>
				                 <th><div style="width:20px">Comp Appr Rem</div></th>
				                 
				                 <th><div style="width:20px">s1</div></th>
				                <th><div style="width:20px">s2</div></th>
				                 <th><div style="width:20px">s3</div></th>

				            </tr>
				        </thead>
				        <tbody>
				        </tbody>
			        </table>
          </div>
          <!-- poovathi add on 02-07-2021 -->
                <div class="tab-pane fade mt-2" id="menu3">
                <div class="row">
                      <div class="col-md-12" id="pendTblSec">
                
         	         <table id="tblPendingReqAdv" class="table table-striped table-bordered hover" style="width:100%">
				        <thead>
				            <tr>
				                <th><div>#</div></th>
				                <th><div style="width:100px">FNA ID</div></th>
				                <th><div style="width:180px">Client</div></th>				                
				                <th><div style="width:60px">Manager</div></th>
				                <th><div style="width:50px">Admin</div></th>
				                <th><div style="width:50px">Compl.</div></th>
				                <th><div style="width:60px">CustId</div></th>
				                <th><div style="width:60px">AdvId</div></th>
				                <th><div style="width:60px">Email</div></th>
				                <th><div style="width:40px">NTUC?</div></th>
				                <th><div style="width:20px">Mgr Appr By</div></th>
				                <th><div style="width:20px">Mgr Appr Date</div></th>
				                <th><div style="width:20px">Mgr Appr Rem</div></th>
				                
				                <th><div style="width:20px">Admin Appr By</div></th>
				                <th><div style="width:20px">Admin Appr Date</div></th>
				                 <th><div style="width:20px">Admin Appr Rem</div></th>
				                 
				                <th><div style="width:20px">Comp Appr By</div></th>
				                <th><div style="width:20px">Comp Appr Date</div></th>
				                 <th><div style="width:20px">Comp Appr Rem</div></th>
				                 
				                 <th><div style="width:20px">s1</div></th>
				                <th><div style="width:20px">s2</div></th>
				                 <th><div style="width:20px">s3</div></th>
				            </tr>
				        </thead>
				        <tbody>
				        </tbody>
			        </table>
			        </div>
			        <div class="col-md-12" >
			          
					<div class="card hide" id="pendMsgSec">
					  <div class="card-header" id="cardHeaderStyle-22">
					       View Pending Req. Messages:-
					       <a class="badge badge-pill badge-warning amber right p-2"  onclick="showPendReqTbl()"
					       role="button"><i class="fa fa-long-arrow-left" aria-hidden="true"></i>
					       View Table</a>
					  </div>
					  <div class="card-body" id="pendReqListSec" style="max-height: 85vh;overflow-y: scroll;">
					  
					  </div>
					</div>
			        
			        
			        </div>
			     </div>   
              </div>
          <!--End  -->

        </div>
    	
       
        </div>

    </div>

  </div> 
									   <!--DASHBOARD TAB SEC CONTENT END HERE  -->     
                                 </div>
                                  
                            
                             <!-- MANAGER DASHBOARD CONTENT START HERE -->
                               <div class="tab-pane fade show" id="managerdashboardspage" role="tabpanel" aria-label="client2-tab"   >   
                              
                            
                            <div class="container-fluid">

 <div class="row" >   <!-- style="margin-top: 20px;" -->

    <div class="col-3">
    	<div class="right-tabs clearfix">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs block">
          <li class="nav-item ">
            <a class="nav-link" data-toggle="tab" href="#shome"><span class="txt_colr"><img src="${contextPath}/vendor/avallis/img/survey.png"> FNA Status</span></a>
          </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content block">

          <div class="tab-pane container active" id="shome">
          
            <div class="row stats cht cursor-pointer mt-2" onclick="openListMng('PENDING')">

              <div class="col-sm-12">

                <div class="row">

                  <div class="col-sm-12">

                    <div class="row">

                      <div class="col-sm-5">

                        <span style="vertical-align: middle; font-size:100%;" ><i class="fa fa-question-circle"></i>&nbsp;Pending</span>

                      </div>

                      <div class="col-sm-4">

                        <div class="progress" style="height:25px">
                          <div class="progress-bar bg-ekyc-secondary progress-bar-striped progress-bar-animated" style="width:${FNA_MGR_STATUS_LIST_DB.PENDING}%;height:25px;font-size:180%;"></div>
                        </div>

                      </div>

                      <div class="col-sm-3">

                        <span><c:if test="${not empty FNA_MGR_STATUS_LIST_DB}">
		                      	 ${FNA_MGR_STATUS_LIST_DB.PENDING}
		                      	 </c:if>	</span>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>
          
          

            <div class="row stats cht cursor-pointer mt-2" onclick="openListMng('APPROVED')">

              <div class="col-sm-12">

                <div class="row">

                  <div class="col-sm-12">

                    <div class="row">

                      <div class="col-sm-5">

                        <span style="vertical-align: middle; font-size:100%;"><i class="fa fa-check-circle-o"></i>&nbsp;Approved</span>

                      </div>

                      <div class="col-sm-4">

                        <div class="progress" style="height:25px">
                          <div class="progress-bar bg-ekyc progress-bar-striped progress-bar-animated" style="width:${FNA_MGR_STATUS_LIST_DB.APPROVE}%;height:25px;font-size:180%;"></div>
                        </div>

                      </div>

                      <div class="col-sm-3">

                        <span><c:if test="${not empty FNA_MGR_STATUS_LIST_DB}">
		                    	${FNA_MGR_STATUS_LIST_DB.APPROVE}
		                     </c:if> </span>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>

          

            <div class="row stats cht cursor-pointer mt-2" onclick="openListMng('REJECTED')">

              <div class="col-sm-12">

                <div class="row">

                  <div class="col-sm-12">

                    <div class="row">

                      <div class="col-sm-5">

                        <span style="vertical-align: middle;font-size:100%;"><i class="fa fa-times-circle"></i>&nbsp;Rejected</span>

                      </div>

                      <div class="col-sm-4">

                        <div class="progress" style="height:25px">
                          <div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" style="width:${FNA_MGR_STATUS_LIST_DB.REJECT}%;height:25px;font-size:180%;"></div>
                        </div>

                      </div>

                      <div class="col-sm-3">

                        <span><c:if test="${not empty FNA_MGR_STATUS_LIST_DB}">
		                      	 ${FNA_MGR_STATUS_LIST_DB.REJECT}
		                      	 </c:if></span>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>
            
              <!-- poovathi add 07-02-2021 -->
            
             <div class="row stats cht cursor-pointer mt-1" onclick="openListMng('PENDINGREQ')">

              <div class="col-sm-12">

                <div class="row">

                  <div class="col-sm-12">

                    <div class="row">

                      <div class="col-sm-5">

                        <span style="vertical-align: middle; font-size:100%;"><i class="fa fa-info-circle  text-info"></i>&nbsp;Pending Req.</span>

                      </div>

                      <div class="col-sm-4">

                        <div class="progress" style="height:25px">
                          <div class="progress-bar bg-info progress-bar-striped progress-bar-animated" 
                           style="width:${FNA_MGR_STATUS_LIST_DB.PENDINGREQ}%;height:25px;font-size:180%;"></div>
                        </div>

                      </div>

                      <div class="col-sm-3">

                        <span><c:if test="${not empty FNA_MGR_STATUS_LIST_DB}">
		                    	${FNA_MGR_STATUS_LIST_DB.PENDINGREQ}
		                     </c:if> </span>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>
            
            
            <!--End  -->

          </div>
          
          <hr/>
          <div class="clearfix"></div>
          
          <section>
                  <div class="pieID pie mgr"></div>
                  <ul class="pieID legend mgr">
                         <li>
                            <em>Pending</em>
                            <span>${FNA_MGR_STATUS_LIST_DB.PENDING}</span>
                         </li>
                         
                          <li>
                             <em>Approved</em>
                             <span>${FNA_MGR_STATUS_LIST_DB.APPROVE}</span>
                          </li>
                          
                          <li>
                             <em>Rejected</em>
                             <span>${FNA_MGR_STATUS_LIST_DB.REJECT}</span>
                         </li>
                         
                           <!-- poovathi add on 02-07-2021 -->
                           <li>
                             <em>Pending Req.</em>
                             <span>${FNA_MGR_STATUS_LIST_DB.PENDINGREQ}</span>
                          </li>
                         <!--  End-->
      
                    </ul>
            </section>

        </div>
        </div>
        
       
     
    </div>
    
    
    
    
    
    <div class="col-9">
   
    	<div class="right-tabs clearfix">
    	
    	 <ul class="nav nav-tabs block"  id="yourAprovTabsMng">
          <li class="nav-item">
            <a class="nav-link"  data-toggle="tab" href="#home10"><i class="fa fa-question-circle"></i><span>&nbsp;Pending</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link"  data-toggle="tab" href="#menu11"><i class="fa fa-check-circle-o"></i><span>&nbsp;Approved</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link"  data-toggle="tab" href="#menu12"><i class="fa fa-times-circle-o"></i><span>&nbsp;Rejected</span></a>
          </li>
          
            <!--poovathi add on 02-07-2021  -->
           <li class="nav-item">
            <a class="nav-link"  data-toggle="tab" href="#menu13"><i class="fa fa-info-circle text-info"></i><span>&nbsp;Pending Req.</span></a>
          </li>
          <!--End  -->
          
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">

          <div class="tab-pane active" id="home10">
          
          <table id="tblPendingMgr" class="table table-striped table-bordered hover"  style="width:100%">
				        <thead>
				            <tr>
				                <th>#</th>
				                <th><div style="width:120px">FNA ID</div></th>
				                <th><div style="width:160px">Client</div></th>				                
				                <th><div style="width:60px">Manager</div></th>
				                <th><div style="width:45px">Admin</div></th>
				                <th><div style="width:45px">Compl.</div></th>
				                <th><div style="width:60px">CustId</div></th>
				                <th><div style="width:60px">AdvId</div></th>
				                <th><div style="width:60px">Email</div></th>
				                <th><div style="width:60px">NTUC?</div></th>
				                <th><div style="width:50px">Mgr Appr By</div></th>
				                <th><div style="width:50px">Mgr Appr Date</div></th>
				                <th><div style="width:50px">Mgr Appr Rem</div></th>
				                
				                <th><div style="width:50px">Admin Appr By</div></th>
				                <th><div style="width:50px">Admin Appr Date</div></th>
				                 <th><div style="width:50px">Admin Appr Rem</div></th>
				                 
				                <th><div style="width:50px">Comp Appr By</div></th>
				                <th><div style="width:50px">Comp Appr Date</div></th>
				                 <th><div style="width:50px">Comp Appr Rem</div></th>
				                 
				                 <th><div style="width:50px">s1</div></th>
				                <th><div style="width:50px">s2</div></th>
				                 <th><div style="width:50px">s3</div></th>
				                 <th><div style="width:90px">Adviser</div></th>
				            </tr>
				        </thead>
				        <tbody>
				        </tbody>
			        </table>


          </div>

          <div class="tab-pane fade mt-2" id="menu11">
          
          	<table id="tblApprovedMgr" class="table table-striped table-bordered hover" style="width:100%">
				        <thead>
				           <tr>
				                <th>#</th>
				                <th><div style="width:120px">FNA ID</div></th>
				                <th><div style="width:160px">Client</div></th>				                
				                <th><div style="width:60px">Manager</div></th>
				                <th><div style="width:50px">Admin</div></th>
				                <th><div style="width:45px">Compl.</div></th>
				                <th><div style="width:45px">CustId</div></th>
				                <th><div style="width:50px">AdvId</div></th>
				                <th><div style="width:50px">Email</div></th>
				                <th><div style="width:60px">NTUC?</div></th>
				                <th><div style="width:50px">Mgr Appr By</div></th>
				                <th><div style="width:50px">Mgr Appr Date</div></th>
				                <th><div style="width:50px">Mgr Appr Rem</div></th>
				                
				                <th><div style="width:50px">Admin Appr By</div></th>
				                <th><div style="width:50px">Admin Appr Date</div></th>
				                 <th><div style="width:50px">Admin Appr Rem</div></th>
				                 
				                <th><div style="width:50px">Comp Appr By</div></th>
				                <th><div style="width:50px">Comp Appr Date</div></th>
				                 <th><div style="width:50px">Comp Appr Rem</div></th>
				                 <th><div style="width:50px">s1</div></th>
				                <th><div style="width:50px">s2</div></th>
				                 <th><div style="width:50px">s3</div></th>
				                 <th><div style="width:90px">Adviser</div></th>
				            </tr>
				        </thead>
				        <tbody>
				        </tbody>
			        </table>
          </div>

          <div class="tab-pane fade mt-2" id="menu12">
          
         	 <table id="tblRejectedMgr" class="table table-striped table-bordered hover" style="width:100%">
				        <thead>
				            <tr>
				                <th>#</th>
				                <th><div style="width:120px">FNA ID</div></th>
				                <th><div style="width:160px">Client</div></th>				                
				                <th><div style="width:60px">Manager</div></th>
				                <th><div style="width:45px">Admin</div></th>
				                <th><div style="width:45px">Compl.</div></th>
				                <th><div style="width:50px">CustId</div></th>
				                <th><div style="width:50px">AdvId</div></th>
				                <th><div style="width:50px">Email</div></th>
				                <th><div style="width:60px">NTUC?</div></th>
				                 <th><div style="width:50px">Mgr Appr By</div></th>
				                <th><div style="width:50px">Mgr Appr Date</div></th>
				                <th><div style="width:50px">Mgr Appr Rem</div></th>
				                
				                <th><div style="width:50px">Admin Appr By</div></th>
				                <th><div style="width:50px">Admin Appr Date</div></th>
				                 <th><div style="width:50px">Admin Appr Rem</div></th>
				                 
				                <th><div style="width:50px">Comp Appr By</div></th>
				                <th><div style="width:50px">Comp Appr Date</div></th>
				                 <th><div style="width:50px">Comp Appr Rem</div></th>
				                 <th><div style="width:50px">s1</div></th>
				                <th><div style="width:50px">s2</div></th>
				                 <th><div style="width:50px">s3</div></th>
				                 <th><div style="width:90px">Adviser</div></th>
				            </tr>
				        </thead>
				        <tbody>
				        </tbody>
			        </table>
          </div>
          
          <!-- poovathi add on 02-07-2021 -->
                <div class="tab-pane fade mt-2" id="menu13">
         	         <table id="tblPendingReqMgr" class="table table-striped table-bordered hover" style="width:100%">
				        <thead>
				            <tr>
				                <th>#</th>
				                <th><div style="width:120px">FNA ID</div></th>
				                <th><div style="width:160px">Client</div></th>				                
				                <th><div style="width:60px">Manager</div></th>
				                <th><div style="width:45px">Admin</div></th>
				                <th><div style="width:45px">Compl.</div></th>
				                <th><div style="width:50px">CustId</div></th>
				                <th><div style="width:50px">AdvId</div></th>
				                <th><div style="width:50px">Email</div></th>
				                <th><div style="width:60px">NTUC?</div></th>
				                 <th><div style="width:50px">Mgr Appr By</div></th>
				                <th><div style="width:50px">Mgr Appr Date</div></th>
				                <th><div style="width:50px">Mgr Appr Rem</div></th>
				                
				                <th><div style="width:50px">Admin Appr By</div></th>
				                <th><div style="width:50px">Admin Appr Date</div></th>
				                 <th><div style="width:50px">Admin Appr Rem</div></th>
				                 
				                <th><div style="width:50px">Comp Appr By</div></th>
				                <th><div style="width:50px">Comp Appr Date</div></th>
				                 <th><div style="width:50px">Comp Appr Rem</div></th>
				                 <th><div style="width:50px">s1</div></th>
				                <th><div style="width:50px">s2</div></th>
				                 <th><div style="width:50px">s3</div></th>
				                 <th><div style="width:90px">Adviser</div></th>
				            </tr>
				        </thead>
				        <tbody>
				        </tbody>
			        </table>
              </div>
          <!--End  -->

        </div>
    	
       
        </div>

    </div>

  </div> 
  </div>
                            
                               
                              </div>  
                              <!--MANAGER DASHBOARD TAB SEC CONTENT END HERE  -->
                              </div>
                           </div>
		    
		</div>     
						
							   
							   
				  </div>
	  
	          </div>
		
        </div>
        
        <!-- poovathi add on 07-07-2011  manager approval link to iframe modal-->  
        
         <!-- The Modal -->
  <div class="modal fade" id="mngrApprvMdl">
	    <div class="modal-dialog modal-xl modal-dialog-scrollable" 
	         style="max-width: 1475px;"><!-- modal-dialog-centered  -->
		      <div class="modal-content"><!--style="min-height: 100%;min-width:100%"  -->
		      
		        <!-- Modal Header -->
		        <div class="modal-header" id="cardHeaderStyle-22">
		          <h6 class="modal-title">Manager Approval</h6>
		             <button type="button" class="close" data-dismiss="modal">&times;</button>
		        </div>
		        
		        <!-- Modal body -->
		        <div class="modal-body" style="overflow-y: none;height: 85vh;">
		           <iframe 
		            width='100%' height='100%' frameborder='0' scrolling='yes' allowtransparency='true' src="" id="mgrApprvFrame" style="width: 100%;height:100vh;"></iframe>
		        </div><!--  class="responsive-iframe"-->
		        
		        <!-- Modal footer -->
		       <!--  <div class="modal-footer p-1">
		          <button type="button" class="btn btn-bs3-prime" data-dismiss="modal">Close</button>
		        </div> -->
		        
		      </div>
	    </div>
  </div>
        <!--  -->
</body>
<script>

strALLFNAIdsadv=<%=request.getAttribute("ALL_FNA_IDS")%>;
strALLFNAIds=<%=request.getAttribute("ALL_FNA_IDS_DB")%>;
 
$(document).ready(function(){
$('#cover-spin').hide(0);
 
callBodyInitadviser();
});

</script>

</html>

