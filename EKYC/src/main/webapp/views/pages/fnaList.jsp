<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
 <div class="modal fade" id="fnaListModal" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="fnaListModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header" id="cardHeaderStyle-22">
                  <h6 class="modal-title" id="fnaListModalLabel"><i class="fa  fa-user-plus"></i>&nbsp;Create / Search FnaList</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:#fff;">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="card">
                    
                      <div class="card-body" style="height:350px;">
                      <div class="row">
                      
                           <div class="col-12">
                                <div class="custom-control custom-radio">
								    <input type="radio" class="custom-control-input checkdb " id="radBtnSrchFna" name="radfnaList" value="SRCHFNA" checked="checked">
								    <label class="custom-control-label" for="radBtnSrchFna"><i class="fa  fa-search" style="color: #1799b7;"></i>&nbsp;Search Existing FnaDetails Details</label>
								 </div>
                           
                           </div>
                          
                           <div class="col-12 mt-3">
                                 <form action="">
									  <div class="form-group ml-5">
									    <label class="frm-lable-fntsz" for="" style="margin-bottom: 0px;">Search  FnaId:</label>
									     <select class="form-control checkdb" id="selFldFnaList">
									     	<option value="" selected>Select FnaId</option>
											     <optgroup id="searchExistFnaList" label="Search FnaId">
												</optgroup>
			                             </select>
			                             
			                             <div class="invalid-feedbacks hide" id="searchExistFnaListError">
			                                 Please select anyone of the FNAId for Proceed!
			                             </div>
			                             
			                            
									  </div>
						        </form>
                           </div>
                           
                           <div class="col-12 mt-4">
                                <div class="custom-control custom-radio">
								    <input type="radio" class="custom-control-input checkdb " id="radBtnCrtFna" name="radfnaList" value="CRTFNA" onclick="openKycIntroPage()">
								    <label class="custom-control-label" for="radBtnCrtFna"><i class="fa  fa-user-plus" style="color: #0aa00ad4;"></i>&nbsp;Create new Fna Details</label>
								 </div>
                           
                           </div>
                           
                           
                      </div>
                        
                      </div>
                      
                    
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-sm btn-bs3-prime" id="addSrchFnaListBtn">Proceed</button>
                </div>
              </div>
            </div>
          </div>

</body>
</html>