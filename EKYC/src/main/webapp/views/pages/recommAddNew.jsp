<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="img/logo/logo.png" rel="icon">
</head>

<body id="page-top">
  		<!-- Container Fluid-->
       <div class="container-fluid" id="container-wrapper" style="min-height:75vh">
		<div id="frmBasisOfRecomForm">
		
		<input type="hidden" name="fnaId" id="fnaId" value="${CURRENT_FNAID}" >
		<input type="hidden" name="dataformId" id="dataformId" value="${CURRENT_DATAFORM_ID}"/>
		<div class="d-sm-flex align-items-center justify-content-between mb-1">
		 
	       <div class="col-5">
		       <h5>UT - Add New.</h5>
		  </div>
		  <div class="col-md-7">
		           <jsp:include page="/views/pages/policyE-Submission.jsp"></jsp:include>
		  </div>
       
         </div>
         
          <div class="row">
		     <div class="col-12">
		     	 <small class="pl-3 warningTitleMsg float-right"  title="Note Information">
		     	 <img src="vendor/avallis/img/infor.png" alt="Note" class="img-rounded mr-2">
		     	 &nbsp;<span id="applValidMessage">show validation message here</span></small>
		        
		     </div>
		 </div>
		  
		<div class="card mb-4 " style="border:1px solid #044CB3;min-height:65vh">
		
		    <div class="card-body">
			   
			    <div class="row">
			    </div>
			    
		      </div>
		    <div class="card-footer"></div>
	  </div>
      
      </div>
   </div>
</body>

 <script type="text/javascript" src="vendor/avallis/js/recommUTAddNew.js"></script>

</html>