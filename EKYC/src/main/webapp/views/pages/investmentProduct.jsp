<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="img/logo/logo.png" rel="icon">
  <style>
  body {
  margin: 0;
}
iframe {
  height:calc(100vh - 4px);
  width:calc(80vw - 4px);
  box-sizing: border-box;
}
  </style>
</head>

<body id="page-top">
  		<!-- Container Fluid-->
       <div class="container-fluid" id="container-wrapper"  style="min-height:75vh;">
		<div id="frmBasisOfRecomForm">
		
		<input type="hidden" name="fnaId" id="fnaId" value="${CURRENT_FNAID}" >
		<input type="hidden" name="dataformId" id="dataformId" value="${CURRENT_DATAFORM_ID}"/>
		<div class="d-sm-flex align-items-center justify-content-between mb-1">
		        <div class="col-md-5">
		            <h6>Investment Product</h6>
		        </div>
		        
		        <div class="col-md-7">
		            <jsp:include page="/views/pages/policyE-Submission.jsp"></jsp:include>
				</div>
          </div>
          
          <div class="row">
             <jsp:include page="/views/pages/signApproveStatus.jsp"></jsp:include>
         </div>
         
        
		  
		<div class="card mb-4 " style="border:1px solid #044CB3;min-height:65vh;">
		
		    <div class="card-body">
			   
			    <div class="row">
			         <div class="col-12">
                          <iframe class="responsive-iframe"
                           src="https://internal-ekyc-ias.avallis.com/EKYC_IAS/applicant/getApplicantDets/${CURRENT_FNAID}"></iframe>
                    </div> 
			    </div>
			    
		      </div>
		    <div class="card-footer"></div>
	  </div>
      
      </div>
   </div>
</body>

  <script type="text/javascript" src="vendor/avallis/js/recommUTAddNew.js"></script>

</html>