<%@ page language="java" import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%ResourceBundle Ekycprop =ResourceBundle.getBundle("AppResource");%>
<!DOCTYPE html>
<html lang="en">

<head>
  <link href="vendor/jquery/jquerysctipttop.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="vendor/avallis/css/style_toggle.css">
  <link rel="stylesheet" href="vendor/avallis/css/bs_style.css">
  <link href="vendor/avallis/css/stepper.css" rel="stylesheet">
  <!-- <link href="css/fileinput.css" media="all" rel="stylesheet" type="text/css"/> -->
  <link href="vendor/avallis/css/theme2.css" media="all" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
  <link href="vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" >
   <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
   
   <style>
   
   .hide{
   display: none;
   }
   
   .show{
   display : block;
   }
   
   #adressTbl .thead-light th{
     padding: 3px;
   }
   .typeahead{
    font-size: 11px;
    max-height: 25vh;
    overflow-y: scroll;
   }
   
   .typeahead .dropdown-item{
     padding: 1px 1.5rem;
   }
   </style>
 </head>

<body id="page-top">
       <div class="container customerDetailsFrm" id="container-wrapper">   
       
       
         <input type="hidden" name="custid" id="custid"/>
         <input type="hidden" name="createdDate" id="createdDate"/>
         <input type="hidden" name="createdBy" id="createdBy"/>
         
     
        <input type="hidden" name="custPassportNum" id="custPassportNum"/>
        <input type="hidden" name="custFin" id="custFin"/>
        
        
        <!--poovathi add 16-09-21 (ekycdev change)  -->
         <input type="hidden" name="newClntName" id="newClntName" value="${NEWCLNTNAME}" />
         
      <!--multisteps-form-->
      <div class="multisteps-form" style="margin-top:10px;">
      
        <!--progress bar-->
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 ml-auto mr-auto ">
            <div class="multisteps-form__progress">
              <button class="multisteps-form__progress-btn js-active" type="button" title="User Personal Info" id="btnPersonalInfo" onclick="openTabSec('Personal')"><i class="fa fa-user" style="font-size:1.4rem;color:#4caf50;" ></i><span>&nbsp;Personal&nbsp;Details</span></button>
              <button class="multisteps-form__progress-btn" type="button" title="Residential Address" id="btnResiAddress" onclick="openTabSec('Address')"><i class="fa fa-map-marker" style="font-size:1.4rem;color:#f33;" ></i><span>&nbsp;Residential&nbsp;Address</span></button>
              <button class="multisteps-form__progress-btn" type="button" title="Line Of Business" id="btnLOB" onclick="openTabSec('LOB')"><i class="fa fa-industry" style="font-size:1.4rem;color:#ff9800;"></i><span>&nbsp;LOB </span></button>
              <button class="multisteps-form__progress-btn hide" type="button" title="Document Upload" id="btnUploads" onclick="openTabSec('Uploads')"><i class="fa fa-upload" style="font-size:1.4rem;color:#3f9a9a;"></i><span>&nbsp;Uploads</span></button>
            </div>
          </div>
        </div>
        <!--form panels-->
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12  m-auto">
            <div class="multisteps-form__form" style="height:550px;">
              <!--single form panel-->
              <div class="multisteps-form__panel shadow p-4 rounded bg-white js-active" data-animation="scaleIn" id="PerInfoSec">
                
                <div class="multisteps-form__content">
                
                <div class="form-row  mt-2">
				      <div class="col-md-6 col-sm-12 col-xs-12"><span class="bagge"><span style="color: red;"><sup>*</sup></span> <small style="color:#000000;">Indicated Fields are Mandatory.</small></span></div>
				      <div class="col-md-6 col-sm-12 col-xs-12">
				      	<h4 id="advListCombo" class="d-none">
				      	<img src="vendor/avallis/img/srchClnt.png" alt="Adviser Staff Img" class="" style="width: 7%;">
							<span class="badge badge-primary srvAdvstyle">Servicing Adviser : ${LOGGED_USER_INFO.LOGGED_USER_ADVSTFNAME}</span></h4>
							<span class="badge badge-primary srvAdvstyle float-right d-none default-cursor" style="cursor: default;" id="custIdLabel">CUSTID : </span>	
				      </div>
				     
				     <!--  <div class="col-md-3 col-sm-12 col-xs-12">
				          poovathi commented myInfo btn on 16-09-21 (ekc dev change)
					        <a class="btn btn-sm btn-outline-info float-right btnMyInfoLink"> Get &nbsp;&nbsp;<img
					         src="vendor/avallis/img/myinfo.png" id="btnMyInfo" style="width:75px;"/>&nbsp;&nbsp;<i class="fa fa-external-link "></i></a> 
				         End of comment 
				     </div>  -->
               </div>
           
				<div class="form-row mt-2"  id="custTypeRadioBtn">
				<div class="col-md-6 col-sm-12 col-xs-12">
				 <div class="row">
					  <div class="col-md-4 col-sm-12 col-xs-12">
						<span class="input-group-text p-2">Select Client Type</span>
					  </div>
					  
					  <div class="col-md-4 col-sm-12 col-xs-12">
					  <input type="radio" name="custCateg" id="custCatPer" value="PERSON" checked>
					  <label class="lblStyle" for="custCatPer">
					    <img src="vendor/avallis/img/individual.png">&nbsp;Individual
					    </label>
                 </div>
                 
                 <div class="col-md-4 col-sm-12 col-xs-12">
				  <input type="radio"  name="custCateg" id="custCatComp" value="COMPANY" disabled>
				  <label class="lblStyle" for="custCatComp" >
				  <img src="vendor/avallis/img/corporate.png">&nbsp;Corporate
				  </label>
               </div>
               
		         </div>			   
			</div>
			
				<div class="col-md-3 col-sm-12 col-xs-12">
				
				<div class="input-group" style="">
									 <div class="input-group-prepend">
				                        <span class="input-group-text">Adviser<sup style="color: red;"><strong>*</strong></sup></span>
				                        </div>
				                         <select class="form-control" id="currAdviserId" name="currAdviserId">
					                                  <option selected value="">--Select--</option>
					                                   <c:if test="${not empty ACTIVE_ADVISER}">
											   <c:forEach items="${ACTIVE_ADVISER}" var="adv">
											   <option value="${adv[0]}">${adv[1]}</option>
											   </c:forEach>
											   </c:if>
					                     </select>
				                      <div class="invalid-feedbacks hide" id="currAdviserIdError">Select the Servicing Adviser</div>
                                </div>
				</div>
				
				<div class="col-md-3 col-sm-12 col-xs-12">
				     <div class="input-group" style="margin-left: 0rem;">
									 <div class="input-group-prepend">
				                        <span class="input-group-text">Client Status<sup style="color: red;"><strong>*</strong></sup></span>
				                        
				                    </div>
											   <select class="form-control checkMand" name="custStatus" id="custStatus">
											   <option value="" selected>--Select--</option>
											   <c:if test="${not empty MASTER_CUSTSTS}">
											   <c:forEach items="${MASTER_CUSTSTS}" var="sts">
											   	<option value="${sts.customerStatusName}">${sts.customerStatusName}</option>
											   	
											   </c:forEach>
											   
											   </c:if> 
										    </select>
										    
										    <div class="invalid-feedbacks hide" id="custStatusError">Select the Client Status.</div>
                                </div>
				</div>
				
				</div>
				
				
               <div class="form-row  mt-3">
                  <div class="col-md-6 col-sm-12 col-xs-12 col-12">
					  <div class="input-group">
						  <div class="input-group-prepend">
							<span class="input-group-text"  id="basic-addon1">Name<sup style="color: red;"><strong>*</strong></sup></span>
						  </div>
						  <input type="text" class="form-control checkMand"  name="custName" id="custName"  maxlength="75" />
						  <div class="input-group-prepend ">
							<span class="input-group-text prime-bgcolor">
								<a class="pointer-cursor prime-bgcolor white-text" target="_new" onclick="window.open('https://secure-fpms.avallis.com/FPMS/')"><i class="fa fa-external-link"></i>&nbsp;FPMS</a>
							</span>
						  </div>
						  <div class="invalid-feedbacks hide" id="custNameError">Keyin Client Name.</div>
	                 </div>
                </div>
                
                  
                <div class="col-md-2 col-sm-12 col-xs-12 col-12 mt-sm-0">
	                    <div class="input-group input-group date">
							  <div class="input-group-prepend">
							    <span class="input-group-text">Initials<sup style="color: red;"><strong>*</strong></sup></span>
							  </div>
							<input type="text" class="form-control checkMand"  id="custInitials" name="custInitials"  maxlength="40" />
							  <div class="invalid-feedbacks hide" id="custInitialsError">Keyin Client Initials.</div>
							 
							</div>
                </div> 
                  
                <div class="col-md-3 col-sm-12 col-xs-12 col-12 col-sm-3 mt-sm-0" id="simple-date1">
                    <div class="input-group input-group date">
						  <div class="input-group-prepend">
						    <span class="input-group-text" id="" >DOB&nbsp;<small class="form-text text-muted font-sz-level10">(DD/MM/YYYY)</small></span>
						  </div>
						 <input type="text" class="form-control"  id="dob" name="dob" onchange="setClientDob(this)">
						 
						 <div class="invalid-feedbacks hide" id="dobError"></div>
						 
						</div>
                 </div> 
                     
                <div class="col-md-1 col-sm-12 col-xs-12 col-12">
		                       <div class="input-group ">
								  <div class="input-group-prepend">
								    <span class="input-group-text" id="" >Age</span>
								  </div>
								 <input type="text" class="form-control"  id="txtFldClntAge" name="txtFldClntAge">
								</div>
			   </div>
		</div> 
				
				
				
               
                   <div class="form-row mt-3">
                   
                     <div class="col-md-4  col-sm-3">
						       <div class="input-group">
									 <div class="input-group-prepend">
				                        <span class="input-group-text">Marital Status</span>
				                    </div>
											  <select class="form-control" id="maritalStatus" name="maritalStatus">
											    <option selected value="">--Select--</option>
											    <option value="Single">Single</option>
											    <option value="Married">Married</option>
											    <option value="Separated">Separated</option>
											    <option value="Divorced">Divorced</option>
											    <option value="Attached">Attached</option>
											    <option value="Widow">Widow</option>
											    <option value="Widower">Widower</option>
											    
											</select>
  
					 
				                    </div>
						</div>
						
			<div class="col-12 col-sm-2">
			      <div id="custTypeRadioBtn">
					  <input type="radio" name="sex" id="radBtnmale" value="M">
					  <label for="radBtnmale" class="lblStyle">
					  <img src="vendor/avallis/img/man.png">&nbsp;Male
					  </label>
                 </div>
			</div>
			
			<div class="col-12 col-sm-2">
			      <div id="custTypeRadioBtn">
					  <input type="radio"  name="sex" id="radBtnFemale" value="F">
					  <label for="radBtnFemale" class="lblStyle">
					  <img src="vendor/avallis/img/woman.png">&nbsp;Female
					  </label>
			     </div>
			</div>
			             
			 <div class="col-12 col-sm-2">
                       <div class="custom-control custom-radio">
							  <input type="radio" class="custom-control-input" id="notsmokerFlg" name="smokerFlg" value="N">
							  <label class="custom-control-label font-sz-level8" id="notsmokerFlg" for="notsmokerFlg" style="font-size: 0.8rem;color: #000;">Non-Smoker</label>
							  <img class="pl-2" alt="smokeImg" src="vendor/avallis/img/no-smoking.png" id="smokeImg">
                       </div>
                       
                       <div class="custom-control custom-radio">
							  <input type="radio" class="custom-control-input" id="smokerFlg" name="smokerFlg" value="Y">
							  <label class="custom-control-label font-sz-level8" id="smokerFlg" for="smokerFlg" style="font-size: 0.8rem;color: #000;">Smoker</label>
							  <img class="pl-2" alt="smokeImg" src="vendor/avallis/img/smoking.png" id="smokeImg">
                       </div>
                       
			</div>
								  
								   <div class="col-12 col-sm-2">
								       
								       <div class="input-group">
									 <div class="input-group-prepend">
				                        <span class="input-group-text" >Race</span>
				                    </div>
											  <select class="form-control" id="race" name="race">
											    <option value="" selected>--Select--</option>
										        <option value="Chinese" title="Chinese">Chinese</option>
										        <option value="Eurasian" title="Eurasian">Eurasian</option>
											    <option value="Indian" title="Indian">Indian</option>
											    <option value="Malay" title="Malay">Malay</option>
											    <option value="Others" title="Others">Others</option>
											</select>
                                      </div>
								  
								  </div>
								  
						
                   </div>
                    
                    
                    
                    
                    
                     <div class="form-row mt-2"> 
			                    <div class="col-12 col-sm-4">
										  	  <div class="input-group">
								 
								  				  
								  <select class="form-control" id="nricType" name="nricType" 
								  onchange="setnricType(this)" style="max-width: 100px;">
											    <!-- <option value="" >--Select--</option> -->
											     <option value="NRIC" selected>NRIC</option>
											     <option value="FIN">FIN</option>
											     <option value="Passport">Passport</option>
							    	</select>
								<input type="text" class="form-control checkNricStstus checkMand"  id="nric" name="nric" maxlength="15" placeholder="NRIC" />
								<div class="invalid-feedbacks" id="nricError"></div>
								 </div>
								  </div>
								  
								  <div class="col-12 col-sm-4">
										<div class="input-group">
										 <div class="input-group-prepend">
					                         <span class="input-group-text">Email<sup style="color: red;"><strong>*</strong></sup></span>
					                       </div>
					             
					                        <input type="text" class="form-control checkMand "  id="emailId" name="emailId" onblur="validateEmail(this,'emailId')"  maxlength="60">
									        <div class="invalid-feedbacks hide" id="emailIdError"></div>
									        <div class="invalid-feedbacks hide" id="emailIdFldError">keyin Email Id</div>
									    
									    </div>
                      
                                 </div>
                                  
	                                   <div class="col-12 col-sm-4  mt-sm-0">
	                                      <div class="btn-group dropup">
													  <button type="button" class="btn btn-bs3-success" data-toggle="dropdown" aria-haspopup="true" 
													  aria-expanded="false"  >
													   <i class="fa fa-phone-square " aria-hidden="true"></i><sub><i class="fa fa-plus-circle" aria-hidden="true">
														  </i></sub> Click to Add  Contact Detls.<sup style="color: red;"><strong>*</strong></sup> </button>
													 
														  <button type="button" class="btn btn-bs3-prime" data-toggle="contactDetls" data-trigger="hover" >
														    <i class="fa fa-eye" aria-hidden="true"></i> Click to View Contact Detls.
														  </button>	
														  	
													  <div class="dropdown-menu dropdown-menu-right p-2 ml-5 hide" id="btnConSec">
													       <div class="form-group">
																		<div class="row">
																			 <div class="col-md-12">
																					 <label class="frm-lable-fntsz" for="resHandPhone">Mobile No<sup style="color: red;"><strong>*</strong></sup></label>
																					<input id="resHandPhone" name="resHandPhone" type="text" class="form-control input-md checkdb mt-n2" 
																					 maxlength="60" title="" autocomplete="nope" onchange="setContDetls()">
																			 </div>
																		 </div>
																		</div>
																							  
																	<div class="form-group">
																		<div class="row">
																			<div class="col-md-12">
																				 <label class="frm-lable-fntsz" for="resPh">Home No<sup style="color: red;"><strong>*</strong></sup></label>
																				<input id="resPh" name="resPh" type="text" class="form-control input-md checkdb mt-n2"
																				 maxlength="60" title="" autocomplete="nope" onchange="setContDetls()">
																				 </div>
																			 </div>
																		</div>
																																		   
																		 <div class="form-group">
																			  <div class="row">
																				 <div class="col-md-12">
																			          <label class="frm-lable-fntsz" for="offPh">Office No<sup style="color: red;"><strong>*</strong></sup></label>
																					<input id="offPh" name="offPh" type="text" class="form-control input-md checkdb mt-n2"
																					 maxlength="60" title="" autocomplete="nope" onchange="setContDetls()">
																				</div>
																			</div>
																		</div>
																		
																		  <div class="invalid-feedbacks hide" id="contactFldError">keyin atleast one contact details.</div>
																		
													                      </div>
                  </div>             
	                                   </div>
					              </div>
                    </div>
                    
				  
				  <div class="form-row mt-3">
				  
				  <div class="col-12 col-sm-4 mt-4 mt-sm-0">
				             <div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text" >Nationality</span>
					  </div>
					  	 <select class="form-control" id="nationality" name="nationality">
									 <option value="" selected >--Select--</option>
									 <!-- <option value="Singaporean">Singaporean</option>
									 <option value="Singaporean-PR">Singaporean-PR</oprtion> -->
									 <%for(String data:Ekycprop.getString("app.nationality").split("\\^")){ %>
									 		<option value="<%=data%>"><%=data%></option>
									 <%} %>
								</select>		  
				            </div>
                      
                    </div>
                    
                    <!-- vignesh add on 13-05-2021 -->
                    <div class="col-sm-4 mt-sm-0">
		                         <div class="input-group">
										  <div class="input-group-prepend">
											<span class="input-group-text" >Country of Birth</span>
										  </div>
										  	<select class="form-control" id="countryofBirth" name="countryofBirth" >
												<option value="" selected>--Select--</option>
											    <% for(String country:Ekycprop.getString("app.country").split("\\^")){ %>
											    <option value="<%=country%>"><%=country%></option>
											    <%}%>
											</select>		  
							      </div>
		             </div>
                    
                    
                    
                       <div class="col-12 col-sm-4">
					         <div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text"  id="basic-addon1">Height &amp; Weight</span>
					  </div>
					  <input type="text" class="form-control"  id="height" name="height" maxlength="6"/>
					  		<div class="input-group-prepend">
						    <span class="input-group-text">m</span>
						  </div>		  
					  <input type="text" class="form-control"  id="weight" name="weight" maxlength="6">
					  <div class="input-group-prepend">
						    <span class="input-group-text"  style="border-radius: 0px 5px 5px 0px;">Kg</span>
						  </div>
			           </div>
					
                       </div>
                       
                       
                      
                    
                     
            </div>
				  
                <div class="form-row mt-3 ">
                
                <div class="col-12 col-sm-4">
                       <div class="input-group mb-3">
						  <div class="input-group-prepend">
						    <span class="input-group-text">Company</span>
						  </div>
						 <input type="text" class="form-control"  id="companyName" name="companyName" maxlength="200">
						</div>
					 </div>
                     
                     	<div class="col-12 col-sm-4">
							  	 <div class="input-group mb-3">
						  <div class="input-group-prepend">
						    <span class="input-group-text" >Estd. Annual Income ($)</span>
						  </div>
						 <input type="text" class="form-control numberFldClass"  id="income" name="income" maxlength="24">
						  <div class="input-group-prepend">
						    <span class="input-group-text"  style="border-radius: 0px 5px 5px 0px;">.00</span>
						  </div>
                        </div> 
					  </div>
                     
                
                
                      <div class="col-12 col-sm-4  mt-sm-0">
                             <div class="input-group mb-3">
						  <div class="input-group-prepend">
						    <span class="input-group-text">Occupation</span>
						  </div>
						 <input type="text" class="form-control"  id="occpnDesc" name="occpnDesc" maxlength="200">
                        </div>
                      
                        
                    </div>
                    
                    
                       
                  
                  </div>
                  
                   <div class="form-row ">
                  
		                 	
		                 <div class="col-12 col-sm-4 mt-sm-0">
                               <div class="input-group">
									 <div class="input-group-prepend">
				                        <span class="input-group-text" >Type of Prospecting</span>
				                    </div>
											  <select class="form-control" name="typesOfProspect" id="typesOfProspect" maxlength="75">
											    <option value="" selected >--Select--</option>
											    <option value="Branch Walk-in">Branch Walk-in</option>
											    <option value="CompareFirst">CompareFirst</option>
											    <option value="Cold-calling">Cold-calling</option>
											    <option value="Client/Natural Market">Client/Natural Market</option>
											    <option value="Door-to-door knocking">Door-to-door knocking</option>
											    <option value="Introducer">Introducer</option>
											    <option value="Other Forms of Public Prospecting">Other Forms of Public Prospecting</option>
											    <option value="Prospecting">Prospecting</option>
											    <option value="Roadshows">Roadshows</option>
											    <option value="Referral">Referral</option>
											    <option value="Web-sites">Web-sites</option>
										    </select>
                                </div>     
                        </div>
                     
		                     <div class="col-sm-4 mt-sm-0">
		                         <div class="input-group">
										  <div class="input-group-prepend">
											<span class="input-group-text" >Nature Of Business</span>
										  </div>
										  	<select class="form-control" id="businessNatr" name="businessNatr" maxlength="75" onchange="valiBusNatOthers(this)">
																    <option value="" selected>--Select--</option>
																    <%for (String value:Ekycprop.getString("fna.business.list").split("\\^")){%>
																      <option value="<%=value%>"><%=value%></option>
																   <%}%>
											</select>		  
							      </div>
		                 	</div>
	                 	
		                 	<div class="col-12 col-sm-4  mt-sm-0 hide" id="BusiNatOthSec">
	                             <div class="input-group mb-3">
							        <div class="input-group-prepend">
							            <span class="input-group-text">Other Detls.</span>
							       </div>
							          <input type="text" class="form-control checkMand"  id="businessNatrDets" name="businessNatrDets" maxlength="75">
							         <div class="invalid-feedbacks hide" id="businessNatrDetsError">keyin Other Details </div>
	                           </div>
	                      </div>
				</div>
                  
                  <div class="button-row d-flex mt-3">
                    <button class="btn btn-sm btn-bs3-prime  ml-auto js-btn-next  SteperBtnStyle" type="button" title="Next" onclick="openTabSec('Address')">Next&nbsp;<i class="fa fa-arrow-circle-right " ></i></button>
                  </div>
                </div>
              </div>
              <!--single form panel-->
              <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleIn" id="AddrSec">
                
                <div class="multisteps-form__content">
				<div class="form-row mt-1">
				  <div class="col-2 col-sm-8"></div>
				 <div class="col-2 col-sm-4">
				 </div> 
                </div>
                  <div class="form-row mt-2">
				  <div class="col">
					
					<div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text"  id="basic-addon3">Address1<sup style="color: red;"><strong>*</strong></sup></span>
					  </div>
					  <input class="multisteps-form__input form-control checkMand" type="text" name="resAddr1" id="resAddr1" maxlength="60"/>
				       <div class="invalid-feedbacks hide" id="resAddr1Error">Keyin Address 1 Field.</div>
				      
				      </div>
				  
                    </div>
                  </div>
                  <div class="form-row mt-4">
                    <div class="col">
					
					<div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text"  id="basic-addon3">Address2</span>
					  </div>
					  <input class="multisteps-form__input form-control" type="text" name="resAddr2" id="resAddr2"  maxlength="60"/>
				  
				
				</div>
				 </div>
                  </div>
                  
                  
                  <div class="form-row mt-4">
                    <div class="col">
					
					<div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text"  id="basic-addon3">Address3</span>
					  </div>
					  <input class="multisteps-form__input form-control" type="text" name="resAddr3" id="resAddr3"  maxlength="60"/>
				
				
				</div>
				 </div>
                  </div>
                  
                  
                    <div class="form-row mt-4">
				 <div class="col-10 col-sm-3">
					<div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text"  id="basic-addon4">PostalCode<sup style="color: red;"><strong>*</strong></sup></span>
					  </div>
					 <input class="multisteps-form__input form-control checkMand" type="text"  name="resPostalcode" id="resPostalcode" maxlength="60"/>
			      <div class="invalid-feedbacks hide" id="resPostalcodeError">Keyin  PostalCode Field.</div>
			      
			       </div>
                 </div>
                    
                    <!--poovathi comment sglocate btn action 0n 16-09-21(ekycdev change)  -->
                   <!--  <div class="col-2 col-sm-2">
					 <a  class="btn btn-sm btn-outline-info btnSgLink" style="height: 5.5vh;"> Get &nbsp;&nbsp;<img src="vendor/avallis/img/sglocate.png" style="width:53px;"/>&nbsp;&nbsp;<i class="fa fa-external-link "></i></a>
				       <button id="btnSgLocate" class="btn btn-sm btn-outline-info" style="height: 5.4vh;"> Get &nbsp;&nbsp;
				       <img src="vendor/avallis/img/sglocate.png" style="width:53px;"/>&nbsp;&nbsp;<i class="fa fa-external-link "></i></button>
				   </div> -->
				   
				   <!--comment End   -->
				   
				   <!-- https://www.sglocate.com/documentations.aspx -->
				   
				    <div class="col-12 col-sm-3 mt-4 mt-sm-0"><!--col-sm-4 column 4 change to column 3 for space adjustment by poovathi on 16-09-21 (ekycdev change) -->
                        <div class="input-group mb-3">
									  <div class="input-group-prepend">
									    <span class="input-group-text" >Country<sup style="color: red;"><strong>*</strong></sup></span>
									  </div>
									  <select class="form-control checkMand" id="resCountry" name="resCountry" onchange="filtrCuntry(this)">
									   <option value="" selected>--Select--</option>
									   
									    <% for(String country:Ekycprop.getString("app.country").split("\\^")){ %>
									    	<option value="<%=country.toUpperCase()%>"><%=country%></option>
									     <%}%>
									    
									  </select>
									  
									  <div class="invalid-feedbacks hide" id="resCountryError">Keyin  Country Field.</div>
							</div>      
                      </div>
                      
                       <div class="col-12 col-sm-3 mt-4 mt-sm-0">
					
					<div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text"  id="basic-addon3">City</span>
					  </div>
					  <input class="multisteps-form__input form-control" type="text"  value="" name="resCity" id="resCity" maxlength="60"/>
				</div>
				 </div>
				 
				 <!--poovathi add on 16-09-21 ekycdev change  -->
				  <div class="col-12 col-sm-3 mt-4 mt-sm-0">
					            <div class="input-group">
								  <div class="input-group-prepend">
									<span class="input-group-text"  id="basic-addon3">State</span>
								  </div>
								  <input class="multisteps-form__input form-control" type="text"  value="" name="resState" id="resState" maxlength="60"/>
							   </div>
				         </div>
               
					</div>
					<!-- comment end -->
                  
               <!--  poovathi commented on 16-09-21 (ekycdev change)-->
                 <!-- <div class="form-row mt-2">
                       <div class="col-12 col-sm-3 mt-4 mt-sm-0">
					            <div class="input-group">
								  <div class="input-group-prepend">
									<span class="input-group-text"  id="basic-addon3">State</span>
								  </div>
								  <input class="multisteps-form__input form-control" type="text"  value="" name="resState" id="resState" maxlength="60"/>
							   </div>
				         </div>
				   </div>   -->
               <!--  End-->
                  
				
					 
                  <div class="button-row d-flex mt-4">
                    <button class="btn btn-sm btn-bs3-prime js-btn-prev SteperBtnStyle" type="button" title="Prev" onclick="openTabSec('Personal')"><i class="fa fa-arrow-circle-left "></i>&nbsp;Prev</button>
                    <button class="btn btn-sm btn-bs3-prime ml-auto js-btn-next SteperBtnStyle" type="button" title="Next" onclick="openTabSec('LOB')">Next&nbsp;<i class="fa fa-arrow-circle-right "></i></button>
                  </div>
                </div>
              </div>
              
              <!--single form panel-->
              <div class="multisteps-form__panel shadow p-3 rounded bg-white" data-animation="scaleIn" id="LOBSec">
                <h3 class="multisteps-form__title">&nbsp;</h3>
                <div class="multisteps-form__content">
				
				     <div class="col-12">
						<p class="mb-4 pb-2" style="color: #1d655cf7;">Select Any Line of Business:</p>
					</div>
					
					 <div class="col-12">
						<div class="invalid-feedbacks hide" id="LOBError">Select any one of the Line of Business below </div>
					</div>
					
			<div class="col-12 pb-3 LobDisableSec">
						<input class="checkbox-budget" type="radio" name="typeofpdt" id="typeofpdt_life" value="LIFE" onclick="chkLob(this)">
						<label class="for-checkbox-budget" for="typeofpdt_life">
							<img src="vendor/avallis/img/life.jpg" class="rounded-circle" style="width:150px"><br>Insurance
						</label><!--
						--><input class="checkbox-budget" type="radio" name="typeofpdt" id="typeofpdt_inv" value="INVESTMENT" onclick="chkLob(this)">
						<label class="or-checkbox-budget" for="typeofpdt_inv">
							<img src="vendor/avallis/img/investment7.jpg" class="rounded-circle" style="width:150px"><br>Investments
						</label>
						
						<input class="checkbox-budget" type="radio" name="typeofpdt" id="typeofpdt_all" value="ALL" onclick="chkLob(this)">
						<label class="or-checkbox-budget" for="typeofpdt_all">
							<img src="vendor/avallis/img/design.jpg" class="rounded-circle" style="width:150px"><br>Both
						</label>
			  </div>
				
                  
                  <div class="row">
                    <div class="button-row d-flex mt-4 col-12">
                      <button class="btn btn-sm btn-bs3-prime js-btn-prev SteperBtnStyle" type="button" title="Prev" onclick="openTabSec('Address')"><i class="fa fa-arrow-circle-left "></i>&nbsp;Prev</button>
                      <button class="btn btn-sm btn-bs3-prime ml-auto SteperBtnStyle" onclick="validateClientMand()"   type="button" title="Send"><i class="fa fa-floppy-o"></i>&nbsp;Save Client Dets &amp; Proceed</button>
                     
                    </div>
                  </div>
                </div>
              </div>
              
              <!--single form panel-->
              <div class="multisteps-form__panel shadow p-4 rounded bg-white hide" data-animation="scaleIn" id="DocUplodSec">
                <h3 class="multisteps-form__title">&nbsp;</h3>
                <div class="multisteps-form__content">
				
				<div class="upload">
					 <div class="upload-files">
					  <header>
					   <p>
					    <i class="fa fa-cloud-upload" aria-hidden="true"></i>
					    <span class="up">up</span>
					    <span class="load">load</span>
					   </p>
					  </header>
					  <div class="body" id="drop">
					   <i class="fa fa-file-text-o pointer-none" aria-hidden="true"></i>
					   <p class="pointer-none"><b>Drag and drop</b> files here <br /> or <a href="" id="triggerFile">browse</a> to begin the upload</p>
								<input type="file" multiple="multiple" />
					  </div>
					  <footer>
					   <div class="divider">
					    <span><AR>FILES</AR></span>
					   </div>
					   <div class="list-files">
					    <!--   template   -->
					   </div>
								<button class="importar" type="button">UPDATE FILES</button>
					  </footer>
					 </div>
					</div>
				  <div class="button-row d-flex mt-4">
                    <button class="btn btn-sm btn-bs3-prime js-btn-prev SteperBtnStyle" type="button" title="Prev" onclick="openTabSec('LOB')"><i class="fa fa-arrow-circle-left "></i>&nbsp;Prev</button>
					 <button class="btn btn-sm btn-bs3-prime ml-auto SteperBtnStyle"  data-toggle="modal" data-target="#finishedModal"  type="button" title="Send"><i class="fa fa-floppy-o"></i>&nbsp;Save Client Dets &amp; Proceed</button>
					
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    

        <!--   </div> -->
         
          <!--Row-->
     <div class="modal fade" id="finishedModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLogout"
      data-backdrop="static" data-keyboard="false" tabindex="-1"   aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header" id="cardHeaderStyle-22" >
                  <h5 class="modal-title" id="finishedModalLabelClnt"> Client Details</h5>
                 <!--  <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:#fff;">
                    <span aria-hidden="true">&times;</span>
                  </button> -->
                </div>
                <div class="modal-body text-center">
				
				 
                  <div class="circle-loader">
					  <div class="checkmark draw hide"></div>
					   
					</div>
					
					<h4><div id="modalMsgLbl">Client Information being Saved...</div></h4>


                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-sm  btn-bs3-prime" data-dismiss="modal">Close</button>
                  <a  class="btn btn-sm  btn-bs3-prime disabled" id="generateKYCBtn">Generate KYC</a>
                </div>
              </div>
            </div>
          </div>
		  <!-- </div> -->
		  
		  
		       <!-- Client Search Modal -->
            <!-- Modal Scrollable -->
          <div class="modal fade" id="srchClntModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"
            aria-labelledby="srchClntModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document" style=" height: 90%;" >
              <div class="modal-content" style=" height: 90%;">
                <div class="modal-header" id="cardHeaderStyle-22">
                  <h6 class="modal-title" id="srchClntModalTitle"><img src="vendor/avallis/img/srch.png">&nbsp;&nbsp;Search Client</h6>
                  <button type="button" class="btnNricMdlClse close" data-dismiss="modal" aria-label="Close" style="color:#fff;">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                
                <div class="col-lg-12">
              <div class="card mb-4" >
              
              <div class="card-body" id="secOverlay">
              
               <div class="row">
                   <div class="co-12">
                   <small class="pl-3 noteInfoMsg" title="Note Information"><img src="vendor/avallis/img/infor.png" alt="Note" class="img-rounded mr-2">&nbsp;<span id="noteInfo">
                   The Same NRIC is already registered with the following client, Click on the name/nric to continue</span></small>
                   </div>
              </div>
               
               <div class="row show" id="srchClntTblSec">
                   <div class="col-12">
	                
	                    
	                     <div class="table-responsive p-2" >
			                  <table class="table align-items-center table-flush table-striped table-hover" id="tblNricListRec" >
			                    <thead class="thead-light">
			                      <tr>					    
			                        <th><div style="width:200px">Name</div></th>
			                        <th><div style="width:90px">Initials</div></th>
			                        <th><div style="width:150px">NRIC</div></th>
			                       	<th><div style="width:90px">DOB</div></th>
			                        <th><div style="width:90px">Contact</div></th>
			                         <th><div style="width:90px">CustID</div></th>
			                      </tr>
			                    </thead>
			                   
			                    <tbody>
			                      
			                    </tbody>
			                  </table>
                        </div>
                   
                   
                   </div>
              </div>
              
              
                
               
                </div>
              </div>
            </div>
                  
                  
                </div>
                <div class="modal-footer float-left p-1">
				
                  <button type="button" class="btn btn-sm pl-3 pr-3 btn-bs3-prime btnNricMdlClse" data-dismiss="modal" id="">OK</button>
                  
                </div>
              </div>
            </div>
          </div>
     <!-- client Search Modal End -->
     
     <!-- fnaList Modal Page -->
         <!--poovathi commented on 24-08-2021  -->
           <!-- <div class="modal fade" id="fnaListModal" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="fnaListModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header" id="cardHeaderStyle-22">
                  <h6 class="modal-title" id="fnaListModalLabel"><i class="fa  fa-user-plus"></i>&nbsp;Create / Search FNA List</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:#fff;">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="card">
                    
                      <div class="card-body" style="min-height:60vh;">
                      <div class="row">
                      
                      <div class="col-12">
                           <div class="alert alert-success p-1 hide" id="fnaListCountInfo"> </div>
                      </div>
                      
                      <div class="col-12">
                                <div class="custom-control custom-radio">
								    <input type="radio" class="custom-control-input checkdb " id="radBtnCrtFna" name="radfnaList" 
								     value="CRTFNA" onclick="createNewFnaFun()">
								    <label class="custom-control-label" for="radBtnCrtFna"><i class="fa  fa-user-plus" style="color: #0aa00ad4;"></i>&nbsp;Create new FNA Details</label>
							   </div>
                      </div>
                      
                       <div class="col-12 mt-4">
                                <div class="custom-control custom-radio">
								    <input type="radio" class="custom-control-input checkdb " id="radBtnSrchFna"
								     name="radfnaList" value="SRCHFNA">
								    <label class="custom-control-label" for="radBtnSrchFna"><i class="fa  fa-search" style="color: #1799b7;">
								    </i>&nbsp;Search Existing FNA Details</label>
								 </div>
                           
                       </div>
                          
                       <div class="col-12">
                               <div class="form-group ml-3">
									    <label class="frm-lable-fntsz" for="selFldFnaList" style="margin-bottom: 0px;">Search  FNA Id:</label>
									     <select class="form-control checkdb" id="selFldFnaList" style="width:350px"
										      aria-hidden="true" onchange="getFnaListVal(this)">
										     	    <option value="" selected>Select FNA Id</option>
												    <optgroup id="searchExistFnaList" label="Search FNA Id">
												        <option value="">--Select FNA Id--</option>
													</optgroup>
			                             </select>
			                             
			                             <div class="invalid-feedbacks btnshake hide" id="searchExistFnaListError">
			                                    Please select anyone of the FNA Id to Proceed!
			                             </div>
			                   </div>
					  </div>
                           
                         
                           
                           
                      </div>
                        
                      </div>
                      
                    
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-sm btn-bs3-prime" id="addSrchFnaListBtn" onclick="validateCrtNewFna()">Proceed</button>
               </div>
              </div>
            </div>
          </div> -->
      <!-- fna List Modal Page End  -->
     
     
     <!--SGLocate API Details Modal add on poovathi on 08-07-2021  -->
          <div class="modal fade" id="addressModal" tabindex="-1" aria-labelledby="chooseAddressModalTitle" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header" id="cardHeaderStyle-22">
						<h6 class="modal-title">Choose your Address</h6>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">
					    <div class="row">
					    <div class="col-md-12 col-12">
					         <small><img src="vendor/avallis/img/infor.png" alt="Note" class="img-rounded mr-2">&nbsp;
					         <span>Click on table row to set MyInfo details in client information section.</span></small>
					    </div>
					    
					    <div class="col-md-12 col-12">
					         <table class="table table-bordered font-sz-level6" id="adressTbl">
							<thead class="thead-light">
								<tr>
									<th>Block</th>
									<th>Building Name</th>
									<th>Street Name</th>
									<th>Postal Code</th>
								</tr>
							</thead>
							<tbody id="tBodyAddress">
							
							</tbody>
						</table>
					    </div>
					    
					    </div>
						
					</div>
					
				</div>
			</div>
		</div>
     <!--SGLocate modal End  -->
   
     
     <!--view Contact Details Popover Msg  -->
     
    <div id="popover-contactDetls" style="display: none">
		<ul class="list-group custom-popover"><!-- <i class="fa fa-mobile" aria-hidden="true" id="i-icon-note-style"></i> -->
			    <li class="list-group-item" id="reshandPhList"><img src="vendor/avallis/img/phone.png" width="10px;">&nbsp;Mobile No : --NIL-- </li>
				<li class="list-group-item" id="resPhList"><img src="vendor/avallis/img/home.png" style="width:10px;">&nbsp;Home No : --NIL-- </li>
				<li class="list-group-item" id="offPhList"><img src="vendor/avallis/img/office.png" style="width:10px;">&nbsp;Office No : --NIL-- </li>
		</ul>
	</div>
	
	<script src="vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
  	<script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript" src="vendor/avallis/js/client_info.js"></script>
    <script src="vendor/avallis/js/Common Script.js"></script>
    
    <script>
     
 // Bootstrap Date Picker
	 $('#simple-date1 .input-group.date').datepicker({
	 	//dateFormat: 'dd/mm/yyyy',
	 	format: 'dd/mm/yyyy',
	 	todayBtn: 'linked',
	 	language: "it",
	 	todayHighlight: true,
	 	autoclose: true,         
	 });

        var customer_details = ${CUSTOMER_DETAILS};
        

       

     </script>
  
  
</body>
</html>