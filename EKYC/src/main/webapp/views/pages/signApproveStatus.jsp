
<div class="col-7">
		     	
		     	
		     	<div class="stepwizard">
        <div class="stepwizard-row setup-panel">
            <div class="stepwizard-step col-xs-3"> 
                <button class="btn btn-circle btn-secondary" disabled="disabled">
                <i class="fa fa-remove" id="btnAllPgeNoti" aria-hidden="true"></i></button>  
                <p><small>Key Information</small></p>
            </div>
            <div class="stepwizard-step col-xs-3"> 
                <button class="btn btn-circle btn-secondary" disabled="disabled">
                <i class="fa fa-remove" id="btnSignStatus" aria-hidden="true"></i></button>
                <p><small>Signature Completion </small></p>
            </div>
            <div class="stepwizard-step col-xs-3"> 
                <button class="btn btn-circle btn-secondary" disabled="disabled">
                <i class="fa fa-remove" id="btnMngrApproval" aria-hidden="true"></i></button>
                <p><small>Manager Approval</small></p>
            </div>
            <div class="stepwizard-step col-xs-3"> 
                <button class="btn btn-secondary btn-circle" disabled="disabled">
                <i class="fa fa-remove" id="btnAdmnApproval" aria-hidden="true"></i></button>
                <p><small>Admin  Approval</small></p>
            </div>
            
            <div class="stepwizard-step col-xs-3"> 
                <button class="btn btn-secondary btn-circle" disabled="disabled">
                <i class="fa fa-remove" id="btnCompApproval" aria-hidden="true"></i></button>
                <p><small>Compliance Approval</small></p>
            </div>
        </div>
    </div>
    
             <!--poovathi add on 26-08-2021 to show old Archive info to user  -->
		    <div class="alert alert-danger p-1 m-1 font-sz-level5 oldArchvnoteinfo hide" id="oldArchvnoteinfo"></div> <!--End  -->	
	
	     	
		      </div>
             <div class="col-5">
             
             	<div class="alert alert-default" role="alert" id="applValidMessage"></div>
             
             </div>
