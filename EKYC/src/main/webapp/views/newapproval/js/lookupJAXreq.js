var READY_STATE_COMPLETE = 4;
var PAGE_SUCCESS = 200;
var xmlHttpRequestHandler = new Object();
xmlHttpRequestHandler.createXmlHttpRequest = function()
{

  var XmlHttpRequestObject;
  if (typeof XMLHttpRequest != "undefined"){  
   XmlHttpRequestObject = new XMLHttpRequest();
  }
  else if (window.ActiveXObject){
   // look up the highest possible MSXML version
   
   var tryPossibleVersions=["MSXML2.XMLHttp.5.0",
                            "MSXML2.XMLHttp.4.0",
                            "MSXML2.XMLHttp.3.0",
                            "MSXML2.XMLHttp",
                            "Microsoft.XMLHttp",
                            "Microsoft.XMLDOM",
                            "Microsoft.XMLHTTP"];

  for (i=0; i < tryPossibleVersions.length; i++){
   try{   
      XmlHttpRequestObject = new ActiveXObject(tryPossibleVersions[i]);
      //alert("wjat c "+i +"  XmlHttpRequestObject "+XmlHttpRequestObject);
     // break;
   }
   catch (xmlHttpRequestObjectError){
      //ignore
//      alert("at error");
   }
  }
 }
 return XmlHttpRequestObject;
}

function callAjax(strQueryString, ajaxSyncFlag){
	

	var contentType = "application/x-www-form-urlencoded; charset=UTF-8";
    var param = strQueryString; 
    var date = new Date();
    var timestamp = date.getTime();
    
    var url = baseUrl + "/KYCServlet?";
     

    requestObject = xmlHttpRequestHandler.createXmlHttpRequest();
    
   
    
    if(ajaxSyncFlag){
		var postParam="KYC";
	    requestObject.open("post",url+param+"&time=" + timestamp,ajaxSyncFlag);
	    requestObject.setRequestHeader("Content-Type", contentType);
	    requestObject.send(postParam);
    }else{
    	requestObject.open("post",url,ajaxSyncFlag);
	    requestObject.setRequestHeader("Content-Type", contentType);
	    requestObject.send(param);

	    return requestObject.responseText;

    }
     
    	 


}


