
var strGlblUsrPrvlg="";

var strFnaMgrUnAppDets="";
var STR_FNA_MGRAPPRSTS_LIST = "APPROVE^REJECT";
var strAdminFNADets = "";
var strComplianceFNADets = "";
var strLoggedUserEmailId = "";
var archiveflg= false;
var baseUrl;
var currDate = "";
var strMngrFlg="",strAdminFlg="",strCompFlg="";
var strALLFNAIds="";


var COMMON_SESS_VALS="";
var LOGGED_DISTID="";//,FNA_RPT_FILELOC="",FNA_PDFGENLOC="",FNA_FORMTYPE="";
var strLoggedAdvStfId = "";
var strLoggedUserId = "";
var ntuc_policy_access_flag=false,ntuc_sftp_acs_flg=false;
var isFromMenu="";

var pendingdatatable,approveddatatable,rejecteddatatable;
var acsmode=""
var jsnDataFnaDetails = "";
var mgrmailsentflag = false;



//var pendingdatatable,approveddatatable,rejecteddatatable;
//var strALLFNAIds;

function callBodyInitManagerDetls(){
	
	
for ( var sess in COMMON_SESS_VALS) {
		
		var sessDets = COMMON_SESS_VALS[sess];	  
		for (var dets in sessDets) {		  
			if (sessDets.hasOwnProperty(dets)) {
			  
				var sesskey = dets;
				var sessvalue = sessDets[dets];
				
			    if(sesskey == "LOGGED_DISTID") {LOGGED_DISTID = sessvalue;}
			    
			    if(sesskey == "LOGGED_USER_EMAIL_ID"){strLoggedUserEmailId= sessvalue;}
			    
			    if(sesskey == "TODAY_DATE"){currDate = sessvalue;}
			    
			    if(sesskey == "MANAGER_ACCESSFLG"){strMngrFlg = sessvalue;}
			    
			    if(sesskey == "LOGGED_ADVSTFID"){strLoggedAdvStfId = sessvalue;}
			    
			    if(sesskey == "LOGGED_USERID" ){strLoggedUserId = sessvalue;}//|| sessKey == "STR_LOGGEDUSER" || || sessKey == "LOGGED_USER"
			    
			    if(sesskey == "RPT_FILE_LOC"){FNA_RPT_FILELOC = sessvalue;}
			    
			    if(sesskey == "ADMIN_ACCESSFLG"){strAdminFlg = sessvalue;}
			    
			    if(sesskey == "COMPLIANCE_ACCESSFLG"){strCompFlg = sessvalue;}
			    
			    if(sesskey == "NTUC_POLICY_ACCESS"){ntuc_policy_access_flag = sessvalue;}
			    
			    if(sesskey == "NTUC_SFTP_ACCESS"){ntuc_sftp_acs_flg = sessvalue;}
			    
			}
		}
	}
	

strMngrFlg="Y"
	
	openListMng('PENDING');



	 pendingdatatable = $('#tblPendingMgr').DataTable(
			     {
				   scrollX: true,
					scrollY:"800px",
					scroller: true,
					scrollCollapse:true,
					autoWidth:false,
					"sDom": '<"top"flp>rt<"bottom"i><"clear">',
				 "columnDefs": [{ "targets": [ 6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21  ],"visible": false,"searchable": false}]}		 
	
	 );

	  approveddatatable = $('#tblApprovedMgr').DataTable(
			     {
				    scrollX: true,
					scrollY:"800px",
					scroller: true,
					scrollCollapse:true,
					autoWidth:false,
					"sDom": '<"top"flp>rt<"bottom"i><"clear">',
				  "columnDefs": [{ "targets": [ 6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21 ],"visible": false,"searchable": false}]}
			  );

	  rejecteddatatable = $('#tblRejectedMgr').DataTable(
			     { 
				    scrollX: true,
					scrollY:"800px",
					scroller: true,
					scrollCollapse:true,
					autoWidth:false,
					"sDom": '<"top"flp>rt<"bottom"i><"clear">',
				  "columnDefs": [{ "targets": [ 6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21 ],"visible": false,"searchable": false}]}
			  );
	
	  
	  //added vignesh on 07 06 2021
	  $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
	      $($.fn.dataTable.tables(true)).DataTable()
	         .columns.adjust();
	   }); 
	 
	  if(isFromMenu == true){
		  $("#landingpage").find("li:eq(0) a").trigger("click");
	  }else{
		 
		  $("#landingpage").find("li:eq(1) a").trigger("click");
		  
		 // $('#cover-spin').hide(0);
	  }
		
		if(strALLFNAIds != null){
			loadALLFNAId();
		}
		
//	}
	
		if(strMngrFlg != "Y"){
			$("#appr-mgr-tab").hide();
			$("#appr-mgr-tab-cont").hide();
		}
		if(strAdminFlg != "Y"){		
			$("#appr-admin-tab").hide();
			$("#appr-admin-tab-cont").hide();		
		}
		if(strCompFlg != "Y"){		
			$("#appr-comp-tab").hide();
			$("#appr-comp-tab-cont").hide();		
		}

		$("#spanCrtdUser").text(strLoggedUserId);
		if(acsmode != "MANAGER"){
			$("#btnQRCodesignMgr").hide()
			$("#btnQRCodesignMgrExtra").hide()
		}
		
	createPieMng(".pieID.legend", ".pieID.pie");
	
	if(!isEmpty(jsnDataFnaDetails)  && !isJsonObjEmpty(jsnDataFnaDetails)){
		  for(var obj in jsnDataFnaDetails){
			  if (jsnDataFnaDetails.hasOwnProperty(obj)) {
				  var objValue = jsnDataFnaDetails[obj];
//				  console.log(obj,objValue,"--------------")
				  switch(obj){
				  
				  case "adviser_name":
					  $(".adviserNameTab").text(objValue);
					  break;
				  
				  case "mgrEmailSentFlg":
					  if(!isEmpty(objValue) && objValue == "Y" && strMngrFlg == "Y") {
						  mgrmailsentflag = true;
					  }else{
						  mgrmailsentflag = false;
					  }
					  
					  break;
				
				  case "suprevMgrFlg":
					  if(objValue == "agree" ){ $("#suprevMgrFlgA").prop("checked",true);} 
					  else if(objValue == "disagree" ){ $("#suprevMgrFlgD").prop("checked",true);} 
					  else {$("#suprevMgrFlgA").prop("checked",false);$("#suprevMgrFlgD").prop("checked",false);}
					  
					  if(objValue == "agree" ){
						  
						  $("#diManagerSection").find(":input").prop("disabled",true);
						  $("#diManagerSection").addClass("disabledsec")
							
					  }
					  
				  case "mgrName":
//					  No action
					  break;
					  
				  
				  default:
					  $("#"+obj).val(objValue);
				  }
				  
			  }
			  
		  }

	}
	
	
	
	//var fnaId=$("#txtFnaId").text();
	//getAllSign(fnaId);
	

	/*if(!mgrmailsentflag) {
//		alert("")
		
		Swal.fire({
			width:"650px",
	  	  title: "eKYC Notification",
	  	  html: "This FNA is not yet ready for approval. Please contact the adviser for more details",
	  	  		
	  	  icon: "info",
//	  	  showCancelButton: true,
	  	  allowOutsideClick:false,
	  	  allowEscapeKey:false,
	  	  confirmButtonClass: "btn-danger",
	  	  confirmButtonText: "OK",
//	  	  cancelButtonText: "No,Cancel",
//	  	  closeOnConfirm: false,
	  	  //showLoaderOnConfirm: true
//	  	  closeOnCancel: false
	  	}).then((result) => {
			  if (result.isConfirmed) {
				  Swal.close();
				  $("#landingpage").find("li:eq(0) a").trigger("click");
			  } 

	  	});
		
		
	}*/
	
	
}

function openListMng(statusMsg){
	// alert(statusMsg)
	 $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
   if(statusMsg == "PENDING"){
	  $("#yourAprovTabsMng").find("li:eq(0) a").trigger("click");
	  $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
    }
	if(statusMsg == "APPROVED"){
	$("#yourAprovTabsMng").find("li:eq(1) a").trigger("click");	
	$.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
    }
    if(statusMsg == "REJECTED"){
	$("#yourAprovTabsMng").find("li:eq(2) a").trigger("click");
	$.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
   }
}




/*PIE CHART START*/
function sliceSizeMng(dataNum, dataTotal) {
	  return (dataNum / dataTotal) * 360;
	}
	function addSliceMng(sliceSizeMng, pieElement, offset, sliceID, color) {
	  $(pieElement).append("<div class='slice "+sliceID+"'><span></span></div>");
	  var offset = offset - 1;
	  var sizeRotation = -179 + sliceSizeMng;
	  $("."+sliceID).css({
	    "transform": "rotate("+offset+"deg) translate3d(0,0,0)"
	  });
	  $("."+sliceID+" span").css({
	    "transform"       : "rotate("+sizeRotation+"deg) translate3d(0,0,0)",
	    "background-color": color
	  });
	}
	function iterateSlicesMng(sliceSizeMng, pieElement, offset, dataCount, sliceCount, color) {
	  var sliceID = "s"+dataCount+"-"+sliceCount;
	  var maxSize = 179;
	  if(sliceSizeMng<=maxSize) {
	    addSliceMng(sliceSizeMng, pieElement, offset, sliceID, color);
	  } else {
	    addSliceMng(maxSize, pieElement, offset, sliceID, color);
	    iterateSlicesMng(sliceSizeMng-maxSize, pieElement, offset+maxSize, dataCount, sliceCount+1, color);
	  }
	}
	function createPieMng(dataElement, pieElement) {
	  var listData = [];
	  $(dataElement+" span").each(function() {
	    listData.push(Number($(this).html()));
	  });
	  var listTotal = 0;
	  for(var i=0; i<listData.length; i++) {
	    listTotal += listData[i];
	  }
	  var offset = 0;
	  var color = [ "#333E48", 	    "#009A78",	    "#d9534f"  ];
	  for(var i=0; i<listData.length; i++) {
	    var size = sliceSizeMng(listData[i], listTotal);
	    iterateSlicesMng(size, pieElement, offset, i, 0, color[i]);
	    $(dataElement+" li:nth-child("+(i+1)+")").css("border-color", color[i]);
	    offset += size;
	  }
	}
/*PIE CHART END*/
	
	function loadALLFNAId(){
		
		if(strALLFNAIds != null){
		
			var retval = strALLFNAIds;

			for ( var val in retval) {
				var tabdets = retval[val];
				for ( var tab in tabdets) {
					if (tabdets.hasOwnProperty(tab)) {
						
						var key = tab;
						var value = tabdets[tab];
					        if (key == "MANAGER_FNA_DETAILS" || key == "ADMIN_FNA_DETAILS" || key == "COMPLIANCE_FNA_DETAILS") {
							 var ResultLength = value.length;
							 
							  for(var len=0;len<ResultLength;len++){
								  
								  
				                var status = "";
				                
								if(key == "MANAGER_FNA_DETAILS"){
									status = value[len]["txtFldMgrApproveStatus"];
								}else if(key == "ADMIN_FNA_DETAILS"){
									status = value[len]["txtFldAdminApproveStatus"];
								}else if(key == "COMPLIANCE_FNA_DETAILS"){
									status = value[len]["txtFldCompApproveStatus"];
								}
								
	//PENDING  Table List
								if(isEmpty(status)){
		                    	   
							        var fnaid = value[len]["txtFldFnaId"],
							        custname = value[len]["txtFldCustName"];
							        pendingdatatable.row.add( [
							                    pendingdatatable.rows().count()+1 ,
							                    value[len]["txtFldFnaId"],
							                    value[len]["txtFldCustName"],						                    
							                    (value[len]["txtFldMgrApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldMgrApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
							                    (value[len]["txtFldAdminApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldAdminApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
							                    (value[len]["txtFldCompApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldCompApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
							                    value[len]["txtFldCustId"],
							                    value[len]["txtFldAdvStfId"],
							                    value[len]["txtFldAdvStfEmailId"],
							                    (value[len]["txtFldFnaPrin"] == "NTUC" ? '<i class="fa fa-check-square"></i>' : '<i class="fa fa-window-close-o"></i>'),
	   											value[len]["txtFldMgrStsApprBy"],value[len]["txtFldMgrApproveDate"],value[len]["txtFldMgrApproveRemarks"],
											    value[len]["txtFldAdminStsApprBy"],value[len]["txtFldAdminApproveDate"],value[len]["txtFldAdminApproveRemarks"],
											    value[len]["txtFldCompStsApprBy"],value[len]["txtFldCompApproveDate"],value[len]["txtFldCompApproveRemarks"],
												value[len]["txtFldMgrApproveStatus"],value[len]["txtFldAdminApproveStatus"],value[len]["txtFldCompApproveStatus"],value[len]["txtFldAdvStfName"]
							                ] ).draw( false );
							       
							        
							        	  $("#optgrpPending").append('<option value="'+fnaid+'">'+custname+'</option>');
							        	  $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
								        // console.log("Pending"+ "  "+fnaid+" "+ custname )
								}
								
							
	//APPROVE Table List							
								if(status.toUpperCase() == "APPROVE"){
									
									
									approveddatatable.row.add( [
											                    approveddatatable.rows().count()+1 ,
											                    value[len]["txtFldFnaId"],
											                    value[len]["txtFldCustName"],						                    
											                    (value[len]["txtFldMgrApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldMgrApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
											                    (value[len]["txtFldAdminApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldAdminApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
											                    (value[len]["txtFldCompApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldCompApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
											                    value[len]["txtFldCustId"],
											                    value[len]["txtFldAdvStfId"],
											                    value[len]["txtFldAdvStfEmailId"],
											                    (value[len]["txtFldFnaPrin"] == "NTUC" ? '<i class="fa fa-check-square"></i>' : '<i class="fa fa-window-close-o"></i>'),
											                    value[len]["txtFldMgrStsApprBy"],value[len]["txtFldMgrApproveDate"],value[len]["txtFldMgrApproveRemarks"],
											                    value[len]["txtFldAdminStsApprBy"],value[len]["txtFldAdminApproveDate"],value[len]["txtFldAdminApproveRemarks"],
											                    value[len]["txtFldCompStsApprBy"],value[len]["txtFldCompApproveDate"],value[len]["txtFldCompApproveRemarks"],
																value[len]["txtFldMgrApproveStatus"],value[len]["txtFldAdminApproveStatus"],value[len]["txtFldCompApproveStatus"],value[len]["txtFldAdvStfName"]
											                ] ).draw( false );
//									
									 var fnaid = value[len]["txtFldFnaId"],
								        custname = value[len]["txtFldCustName"] ;
									 
									
							        		  $("#optgrpApproved").append('<option value="'+fnaid+'">'+custname +'</option>');
							        		  $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();  
							        		 // console.log("Approve"+ "  "+fnaid+" "+ custname )
									 
									 
								}
								
								
//		REJECTED Table List						
								if(status.toUpperCase() == "REJECT"){
									
									
									 var fnaid = value[len]["txtFldFnaId"],
								        custname = value[len]["txtFldCustName"];
									 
									rejecteddatatable.row.add( [
											                    rejecteddatatable.rows().count()+1 ,
											                    value[len]["txtFldFnaId"],
											                    value[len]["txtFldCustName"],						                    
											                    (value[len]["txtFldMgrApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldMgrApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
											                    (value[len]["txtFldAdminApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldAdminApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
											                    (value[len]["txtFldCompApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldCompApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
											                    value[len]["txtFldCustId"],
											                    value[len]["txtFldAdvStfId"],
											                    value[len]["txtFldAdvStfEmailId"],
											                    (value[len]["txtFldFnaPrin"] == "NTUC" ? '<i class="fa fa-check-square"></i>' : '<i class="fa fa-window-close-o"></i>'),
											                    value[len]["txtFldMgrStsApprBy"],value[len]["txtFldMgrApproveDate"],value[len]["txtFldMgrApproveRemarks"],
											                    value[len]["txtFldAdminStsApprBy"],value[len]["txtFldAdminApproveDate"],value[len]["txtFldAdminApproveRemarks"],
											                    value[len]["txtFldCompStsApprBy"],value[len]["txtFldCompApproveDate"],value[len]["txtFldCompApproveRemarks"],
																value[len]["txtFldMgrApproveStatus"],value[len]["txtFldAdminApproveStatus"],value[len]["txtFldCompApproveStatus"],value[len]["txtFldAdvStfName"]
											                ] ).draw( false );
									
									 
							        		 $("#optgrpRejected").append('<option value="'+fnaid+'">'+custname +'</option>');
							        		 $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
							        		 //console.log("Reject"+ "  "+fnaid+" "+ custname )
									
									
								}
								
								
								
							   }
							  
							  $('#cover-spin').hide(0);
							
							}					
						if (key == "MANAGER_NO_RECORDS_FNA_DETAILS") {
							  $('#cover-spin').hide(0);
							return false;
						}
						
					}
				}
			}
			 
			var currFNAID = $("#hTxtFldFnaId").val();
			
		//	alert(custname)
		//    $("#dropDownQuickLinkFNAHist").val(custname.toUpperCase())
		//	$("#dropDownQuickLinkFNAHist").val(currFNAID);
			
			 $('#tblPendingMgr tbody').on('click', 'tr', function () {
				 $(this).toggleClass('selected');
		            var data = pendingdatatable.row( this ).data();
		           // getSelectedFNADetsNew(data);
		        	$.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
					openEKYCNewMgr(data);
					
					populateApprovedRec(data,acsmode);
										
		        } );
			 
			 $('#tblApprovedMgr tbody').on('click', 'tr', function () {
				 $(this).toggleClass('selected');
		            var data = approveddatatable.row( this ).data();
		           // getSelectedFNADetsNew(data);
		        	$.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
					openEKYCNewMgr(data);
					
					populateApprovedRec(data,acsmode);
					
						
		        } );
			 
			 $('#tblRejectedMgr tbody').on('click', 'tr', function () {
				 $(this).toggleClass('selected');
		            var data = rejecteddatatable.row( this ).data();
		           // getSelectedFNADetsNew(data);
		        	$.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
					openEKYCNewMgr(data);
					
					populateApprovedRec(data,acsmode);
		        } );
			 
		
		pendingdatatable.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
	   		 var data = this.data();
	  		if(data[1] == currFNAID){
	  			
		           	 $(this).addClass('selected');
		           	 
		           	$("#suprevFollowReason").val("")
					$("#diManagerSection").find('input:radio').prop('checked',false);
					
					$("#txtFldMgrSignDateDeclarepage").prop('readOnly',true);
					
					$("#diManagerSection").find(":input").removeAttr("disabled");
					  $("#diManagerSection").removeClass("disabledsec")
					  
					  

						$("#txtFldMgrStsApprBy").val("");
						$("#txtFldAdminStsApprBy").val("");
						$("#txtFldCompStsApprBy").val("");
						
//						$("#hdnSessFnaId").val(data[1] );
//						callBodyInit();
						
						getAllSign(data[1] );
						
						openEKYCNewMgr(data);
						
						populateApprovedRec(data,acsmode);
						
						


												
		           }else{
		        	   $(this).removeClass('highlight');
		           }
		});
		
		
		approveddatatable.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
	   		 var data = this.data();

	  		if(data[1] == currFNAID){
		           	 $(this).addClass('selected');
						
						$("#suprevFollowReason").val("")
						$("#diManagerSection").find('input:radio').prop('checked',false);
						
						$("#txtFldMgrSignDateDeclarepage").prop('readOnly',true);
						
						$("#diManagerSection").find(":input").removeAttr("disabled");
						  $("#diManagerSection").removeClass("disabledsec")
						  
						  

							$("#txtFldMgrStsApprBy").val("");
							$("#txtFldAdminStsApprBy").val("");
							$("#txtFldCompStsApprBy").val("");
							
//							$("#hdnSessFnaId").val(data[1] );
//							callBodyInit();
							
							getAllSign(data[1] );

							openEKYCNewMgr(data);
							
							populateApprovedRec(data,acsmode);
							
												
		           }else{
		        	   $(this).removeClass('highlight');
		           }
		});
		
		rejecteddatatable.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
	   		 var data = this.data();

	  		if(data[1] == currFNAID){
	  			
		           	  $(this).addClass('selected');
						
						$("#suprevFollowReason").val("")
						$("#diManagerSection").find('input:radio').prop('checked',false);
						
						$("#txtFldMgrSignDateDeclarepage").prop('readOnly',true);
						
						$("#diManagerSection").find(":input").removeAttr("disabled");
						  $("#diManagerSection").removeClass("disabledsec")
						  
						  

							$("#txtFldMgrStsApprBy").val("");
							$("#txtFldAdminStsApprBy").val("");
							$("#txtFldCompStsApprBy").val("");
							
//							$("#hdnSessFnaId").val(data[1] );
//							callBodyInit();
							
							getAllSign(data[1] );

							openEKYCNewMgr(data);
							
							populateApprovedRec(data,acsmode);
							
												
		           }else{
		        	   $(this).removeClass('highlight');
		           }
		});
			
			 
			 
			 $('#tblApprovedMgr').on('mousemove', 'tr', function(e) {
				   var rowData = approveddatatable.row(this).data();
				  
				   var info = '<h6>'+rowData[1]+'<br/><small>(Click the row to view the details)</small></h6><small><strong>'+rowData[3]+'</strong> by(Manager) : <strong>'+rowData[10]+'</strong>'+
				   '<hr/>';
				   if(rowData[4] != "Pending"){
					   info += ' <strong>'+rowData[4]+'</strong> by(Admin) : '+rowData[12]+'<hr/>';
				   }
				   if(rowData[5] != "Pending"){
					   info += '<strong>'+rowData[5]+'</strong> by(Compliance) : '+rowData[14]+'<hr/>';
				   }
				   
				   
				   info += '</small>';
				 
				   $("#tooltip").html(info).animate({ left: e.pageX, top: e.pageY }, 1);
				   if (!$("#tooltip").is(':visible')) $("#tooltip").show();
				})
				
				$('#tblApprovedMgr').on('mouseleave', function(e) {
				  $("#tooltip").hide();
				}) ;
			 
			 $('#tblRejectedMgr').on('mousemove', 'tr', function(e) {
				   var rowData = rejecteddatatable.row(this).data();
				   
				   var info = '<h6>'+rowData[1]+'</h6><small>(Click the row to view the details)</small><br/><small><strong>'+rowData[3]+'</strong> by(Manager) : <strong>'+rowData[10]+'</strong>'+
				   '<hr/>';
				   if(rowData[4] != "Pending"){
					   info += ' <strong>'+rowData[4]+'</strong> by(Admin) : '+rowData[12]+'<hr/>';
				   }
				   if(rowData[5] != "Pending"){
					   info += '<strong>'+rowData[5]+'</strong> by(Compliance) : '+rowData[14]+'<hr/>';
				   }
				   
				   
				   info += '</small>';
				   
				   $("#tooltip").html(info).animate({ left: e.pageX, top: e.pageY }, 1);
				   if (!$("#tooltip").is(':visible')) $("#tooltip").show();
				})
				
				$('#tblRejectedMgr').on('mouseleave', function(e) {
				  $("#tooltip").hide();
				}) ;
			
			/*$('#dropDownQuickLinkFNAHist').select2({
				width :'400',				
				 templateResult: formatDataSelect2
			})*/
			

			
		}

	}
	
	
	

	function openEKYCNewMgr(dataset){
		
		//vignesh add on 20-05-2021
		var logadvid = dataset[7];
		var mgrflg = $("#hTxtFldMgrAcsFlg").val();
		var distid = $("#hTxtFldDistId").val();
		var fnaid = dataset[1];
		
		//var parameter = "dbcall=GETAPPROVALMANAGERURL&strLogAdvID="+logadvid+"&strMgrFlg="+mgrflg+"&strDistID="+distid+"&strFnaID="+fnaid+"&strTxtFldUserId="+hTxtFldUserId+"&strTxtFldPassword="+hTxtFldPassword;
		var parameter = "dbcall=GETAPPROVALMANAGERURL&strLogAdvID="+logadvid+"&strMgrFlg="+mgrflg+"&strDistID="+distid+"&strFnaID="+fnaid;
		var retval = ajaxCall(parameter);

		//var newWindow = window.open(retval,"_blank");
		//end
		//vignesh add on 01-06-2021
		window.open(retval,'avallis');
		
		
		
		var kycformname = "SIMPLIFIED";
		var lobArrtoKYC = "ALL";
		var strLoggedUserDesig ="";
		var strLoggedDistrbutor="";
		//var emailId = dataset[0]["txtFldAdvStfEmailId"]; 
		var formType = "SIMPLIFIED";
		
		
		var fnaId = dataset[1];
		$("#txtFnaId").text(fnaId)
		$("#txtFldCurFnaId").val(fnaId)
		
	    getLatestFnaIdDetls(fnaId)
			
		
		
		
	//	var custname = dataset[2].toUpperCase();
		//$("#txtFnaId").text(fnaId)
		
	//	 $("#optgrpPending").append('<option value="'+fnaId+'">'+custname+'</option>');
	//	 $('#dropDownQuickLinkFNAHist').val(custname);
	//	 $('#dropDownQuickLinkFNAHist').trigger('change.select2');
	var custname = dataset[2];
	 
	$('#dropDownQuickLinkFNAHist').val(fnaId);
	$('#dropDownQuickLinkFNAHist').trigger('change.select2');	
	
	
		var custid = dataset[6]; 
		var advId = dataset[7]; 
		var emailId = dataset[8]; 
		
		
		
		var machine = "";
		machine = BIRT_URL + "/frameset?__report=" + FNA_RPT_FILELOC //
				+ "&P_CUSTID=" + custid + "&P_FNAID=" + fnaId+"&__format=pdf"
				+ "&P_PRDTTYPE=" + lobArrtoKYC
				+ "&P_CUSTOMERNAME=&P_CUSTNRIC=&P_FORMTYPE=" + kycformname
				+ "&P_DISTID=" + LOGGED_DISTID;
				setTimeout(function(){ 
					$('#cover-spin').show(0);
					$("#dynaFrame").attr("src",machine);
				 }, 0);
			
				
				
			//var iframe = document.getElementById('dynaFrame');
			//iframe.onload = function() {$('#cover-spin').hide(0);}; 
		
		setTimeout(function(){$('#cover-spin').hide(0);	}, 1500);
		
	}



	function populateApprovedRec(dataset,loggedUser){
		
		var fnaId = dataset[1];
		var custname = dataset[2].toUpperCase();
		var mgrstatus = dataset[19]//value[0]["txtFldMgrApproveStatus"];
	    var adminstatus = dataset[20]//value[0]["txtFldAdminApproveStatus"];
	    var compstatus = dataset[21]//value[0]["txtFldCompApproveStatus"];
		var custid = dataset[6]; 
		var advId = dataset[7]; 
		var emailId = dataset[8]; 
	    var ntucCaseId = ""//value[0]["txtFldFnaNtucCaseId"];
	var mgrapprby = dataset[10];
	var mgrapprdate=dataset[11];
	var mgrremarks = dataset[12];    
	var adminapprby = dataset[13];
	var adminapprdate=dataset[14];
	var adminremarks = dataset[15];
	var compapprby = dataset[16];
	var compapprdate=dataset[17];
	var compremarks = dataset[18];    
		
		
	//	$("#dropDownQuickLinkFNAHist").val(custname);
		
		
		// $("#optgrpPending").append('<option value="'+fnaId+'">'+custname+'</option>');
		// $('#dropDownQuickLinkFNAHist').val(custname);
		// $('#dropDownQuickLinkFNAHist').trigger('change.select2');
		 
		/*$('#dropDownQuickLinkFNAHist').select2({
				width :'400',				
				 templateResult: formatDataSelect2
			})*/
		 $("#landingpage").find("li:eq(1) a").trigger("click");

		$("#txtFldFnaId").val(fnaId)
		$("#txtFldCustName").val(custname)
//		$("#txtFldAdvStfName").val(advId);
		$("#spanLoggedAdviser").text($("#txtFldAdvStfName").val())	
		$("#txtFldMgrName").val();
		
	    //poovathi get hidden text field values below on 16-03-2020
		$("#txtFldNTUCCaseId").val(ntucCaseId);
		$("#txtFldCustId").val(custid);
		//alert(custid)
		//setcustId to hidden field
		$("#hTxtFldCurrCustId").val(custid);
		$("#txtFldAdvEmailId").val(emailId);
		
		if(isEmpty(ntucCaseId)){
			$("#ntucblock").addClass("fa fa-times-circle-o");
		}else{
			$("#ntucblock").addClass("fa fa-check-square-o").removeClass("fa fa-times-circle-o");
		}
		
		
		//MANAGER	    
		if((mgrstatus.toUpperCase()) == "APPROVE"){
			
			$("#mngrapprLbl").text('Approved Date');
			$("#mngrapprByLbl").text('Approved By');
			$("#iFontMgr").removeClass("fa-question-circle-o");
			$("#iFontMgr").removeClass("fa-times-circle-o");
			$("#iFontMgr").addClass("fa-check-circle-o");
			
			$("#txtFldMgrApproveStatus").prop("disabled",true);
			$("#txtFldMgrApproveRemarks").prop("readOnly",true)
			
		}else if((mgrstatus.toUpperCase()) == "REJECT") {
			
			$("#mngrapprLbl").text('Rejected Date');
			$("#mngrapprByLbl").text('Rejected By');
			$("#iFontMgr").addClass("fa fa-times-circle-o");
			$("#iFontMgr").removeClass("fa fa-check-circle-o");
			$("#iFontMgr").removeClass("fa fa-question-circle");
			
		}else {
			
			$("#mngrapprLbl").text('');
			$("#mngrapprByLbl").text('');
			//$("#mngrapprByLbl").text('Yet to Approve By');
			$("#iFontMgr").removeClass("fa fa-times-circle-o")
			$("#iFontMgr").removeClass("fa fa-check-circle-o")
			$("#iFontMgr").addClass("fa fa-question-circle");
		}
		
	//ADMIN
		
		if(adminstatus.toUpperCase() == "APPROVE"){

			$("#admnapprLbl").text('Approved Date');
			$("#admnapprByLbl").text('Approved By');
			$("#iFontAdmin").removeClass("fa fa-times-circle-o")
			$("#iFontAdmin").addClass("fa fa-check-circle-o");
			$("#iFontAdmin").removeClass("fa fa-question-circle");
			
		}else if((adminstatus.toUpperCase()) == "REJECT") {
			
			$("#admnapprLbl").text('Rejected Date');
			$("#admnapprByLbl").text('Rejected By');
			$("#iFontAdmin").addClass("fa fa-times-circle-o");
			$("#iFontAdmin").removeClass("fa fa-check-circle-o");
			$("#iFontAdmin").removeClass("fa fa-question-circle");
			
		 }else{
			$("#admnapprLbl").text('');
			$("#admnapprByLbl").text('');
			//$("#admnapprByLbl").text('Yet to Approve By');
			$("#iFontAdmin").removeClass("fa fa-times-circle-o")
			$("#iFontAdmin").removeClass("fa fa-check-circle-o")
			$("#iFontAdmin").addClass("fa fa-question-circle");
		}
		
		//COMPLIANCE					
		if(compstatus.toUpperCase() == "APPROVE"){
			
			$("#compapprLbl").text('Approved Date');
			$("#compapprByLbl").text('Approved By');
			$("#iFontComp").removeClass("fa fa-times-circle-o")
			$("#iFontComp").addClass("fa fa-check-circle-o")
			$("#iFontComp").removeClass("fa fa-question-circle");
			
		}else 	if(compstatus.toUpperCase() == "REJECT") {
			
			$("#compapprLbl").text('Rejected Date');
			$("#compapprByLbl").text('Rejected By');
			$("#iFontComp").addClass("fa fa-times-circle-o")
			$("#iFontComp").removeClass("fa fa-check-circle-o")
			$("#iFontComp").removeClass("fa fa-question-circle");
			
		 }else {
			 
			$("#compapprLbl").text('');
			$("#compapprByLbl").text('');
			//$("#compapprByLbl").text('Yet to Approve By');
			$("#iFontComp").removeClass("fa fa-times-circle-o")
			$("#iFontComp").removeClass("fa fa-check-circle-o")
			$("#iFontComp").addClass("fa fa-question-circle");
		}
		
	// FOR MANAGER
		$("#txtFldMgrApproveStatus").val(mgrstatus)
		$("#txtFldMgrApproveDate").val(mgrapprdate);
		$("#txtFldMgrApproveRemarks").val(mgrremarks);	
		$("#txtFldMgrStsApprBy").val(mgrapprby);
		setApprovalStatus($("#txtFldMgrApproveStatus"));
		
	// FOR ADMIN				
		$("#txtFldAdminApproveStatus").val(adminstatus);
		$("#txtFldAdminApproveDate").val(adminapprdate);
		$("#txtFldAdminApproveRemarks").val(adminremarks);
		$("#txtFldAdminStsApprBy").val(adminapprby);
		setApprovalStatus($("#txtFldAdminApproveStatus"));
		
	// FOR COMPLIANCE	
		$("#txtFldCompApproveStatus").val(compstatus);
		$("#txtFldCompApproveDate").val(compapprdate);
		$("#txtFldCompApproveRemarks").val(compremarks);
		$("#txtFldCompStsApprBy").val(compapprby);
		setApprovalStatus($("#txtFldCompApproveStatus"));
		
		var mgrApprStatus = mgrstatus;//$("#txtFldMgrApproveStatus").val();
		
		var adminApprStatus = adminstatus;//$("#txtFldAdminApproveStatus").val();
		
		var compApprStatus = compstatus;//$("#txtFldCompApproveStatus").val();
		
	    openEKYCNewMgr(dataset);

		//downLoadAllFile(fnaId,false);
		
		getAllExisitingDocs(custid,fnaId);
//		alert(loggedUser)
		
		if(loggedUser == "ADMIN" ){//|| loggedUser == "COMPANDADMIN"
			
			$("#appr-mgr-tab").addClass("no-drop1").removeClass("active");
			$("#appr-mgr-tab-cont").removeClass("show").removeClass("active");;
			
	    	$("#appr-admin-tab").removeClass("no-drop1");
	    	$("#appr-admin-tab").addClass("active");
	    	$("#appr-admin-tab-cont").addClass("show").addClass("active");
	    	$("#txtFldAdminApproveRemarks").prop("readOnly",false);
	    	 
	    	$("#appr-comp-tab").addClass("no-drop1");	
	    	$("#appr-comp-tab").removeClass("show").removeClass("active");
	    	$("#appr-comp-tab-cont").removeClass("show").removeClass("active");
	    	
	    	setApprovalStatus($("#txtFldAdminApproveStatus"));
		}else if(loggedUser == "COMPLIANCE"){//|| loggedUser == "COMPANDADMIN"
			
			var tabfocus = true;
			
			$("#appr-mgr-tab").addClass("no-drop1").removeClass("active");
			$("#appr-mgr-tab-cont").addClass("show").removeClass("active");
			
			$("#appr-admin-tab").addClass("no-drop1").removeClass("active");				    					    	
	    	$("#appr-admin-tab-cont").addClass("show").removeClass("active");				    	
	    	$("#txtFldAdminApproveRemarks").prop("readOnly",true);
	    	 
	    	$("#appr-comp-tab").removeClass("no-drop1");
	    	$("#appr-comp-tab").removeClass("show");
	    	$("#appr-comp-tab").addClass("active");
	    	$("#appr-comp-tab-cont").addClass("show").addClass("active");
			
			if(strAdminFlg == "Y" && strCompFlg == "Y"){
				
				if(adminApprStatus == "APPROVE"){}else{}
				
				setApprovalStatus($("#txtFldAdminApproveStatus"));
				
			}else if(strAdminFlg != "Y" && strCompFlg == "Y"){
				
	/*			$("#appr-admin-tab").addClass("no-drop1").removeClass("active");				    					    	
		    	$("#appr-admin-tab-cont").removeClass("show").removeClass("active");				    	
		    	$("#txtFldAdminApproveRemarks").prop("readOnly",true);
		    	 
		    	$("#appr-comp-tab").removeClass("show").addClass("active");
		    	$("#appr-comp-tab").addClass("active");
		    	$("#appr-comp-tab-cont").addClass("show").addClass("active");
	*/			
			}
			
			
			setApprovalStatus($("#txtFldCompApproveStatus"));
			
		}else if(loggedUser == "COMPANDADMIN"){//|| loggedUser == "COMPANDADMIN"
			
			var tabfocus = true;
			
			$("#appr-mgr-tab").addClass("no-drop1").removeClass("active");
			$("#appr-mgr-tab-cont").addClass("show").removeClass("active");
			
			$("#appr-admin-tab").addClass("no-drop1").removeClass("active");				    					    	
	    	$("#appr-admin-tab-cont").addClass("show").removeClass("active");				    	
	    	$("#txtFldAdminApproveRemarks").prop("readOnly",true);
	    	 
	    	$("#appr-comp-tab").removeClass("no-drop1");
	    	$("#appr-comp-tab").removeClass("show");
	    	$("#appr-comp-tab").addClass("active");
	    	$("#appr-comp-tab-cont").addClass("show").addClass("active");
			
			if(strAdminFlg == "Y" && strCompFlg == "Y"){
				
				if(adminApprStatus == "APPROVE"){
					
					$("#appr-comp-tab").removeClass("no-drop1");
			    	$("#appr-comp-tab").removeClass("show");
			    	$("#appr-comp-tab").addClass("active");
			    	$("#appr-comp-tab-cont").addClass("show").addClass("active");
			    	
				}else{
					
					
			    	$("#appr-admin-tab").removeClass("no-drop1");
			    	$("#appr-admin-tab").addClass("active");
			    	$("#appr-admin-tab-cont").addClass("show").addClass("active");
			    	$("#txtFldAdminApproveRemarks").prop("readOnly",false);
			    	 
			    	$("#appr-comp-tab").addClass("no-drop1");	
			    	$("#appr-comp-tab").removeClass("show").removeClass("active");
			    	$("#appr-comp-tab-cont").removeClass("show").removeClass("active");

			    	
				}
				
				setApprovalStatus($("#txtFldAdminApproveStatus"));
				
			}else if(strAdminFlg != "Y" && strCompFlg == "Y"){
				
	/*			$("#appr-admin-tab").addClass("no-drop1").removeClass("active");				    					    	
		    	$("#appr-admin-tab-cont").removeClass("show").removeClass("active");				    	
		    	$("#txtFldAdminApproveRemarks").prop("readOnly",true);
		    	 
		    	$("#appr-comp-tab").removeClass("show").addClass("active");
		    	$("#appr-comp-tab").addClass("active");
		    	$("#appr-comp-tab-cont").addClass("show").addClass("active");
	*/			
			}
			
			
			setApprovalStatus($("#txtFldCompApproveStatus"));
			
		}
		
		
		
		
		/*if(mgrApprStatus.toUpperCase() == "APPROVE"){
			$("#appr-mgr-tab-cont").find(":input").prop("disabled",true);
		}else{
			$("#appr-mgr-tab-cont").find(":input").prop("disabled",false);
		}*/
		
		if(adminApprStatus.toUpperCase() == "APPROVE"){
			$("#appr-admin-tab-cont").find(":input").prop("disabled",true);
		}else{
			$("#appr-admin-tab-cont").find(":input").prop("disabled",false);
		}
		
		if(compApprStatus.toUpperCase() == "APPROVE"){
			$("#appr-comp-tab-cont").find(":input").prop("disabled",true);
		}else{
			$("#appr-comp-tab-cont").find(":input").prop("disabled",false);
		}
		
		
		
		if(ntuc_policy_access_flag == "true" && compApprStatus.toUpperCase() == "APPROVE"){			
			 $("#imgpolapi").show()		
		}else{
			 $("#imgpolapi").hide()
		}
		

		
	}
	
	
	

	function setApprovalStatus(object){
		var thisval = $(object).val();
//		thisval = $(object).find('option[value="' + thisval+ '"]').html();
//		  option = options.find("[value='" + i + "']");
		  
		if(thisval == "APPROVE"){
			$(object).find('option:contains("Approve")').text('Approved');
//			document.getElementById($(object).prop("id")).options[0].innerHTML = "Approved";
		}
		if(thisval == "REJECT"){
			$(object).find('option:contains("Reject")').text('Rejected');
//			document.getElementById($(object).prop("id")).options[1].innerHTML = "Rejected";
		}	
	}
	
	
	


	function getAllExisitingDocs(currentCustId,fnaId){
		
		$("#dynaAttachNRICList").empty();
		
	    $.ajax({
	    	    url:baseUrl+ "/CustAttach/getAllNricCustDocs",
	            data:{"custid":currentCustId,"fnaId":fnaId},
	            type: "GET",
	            async:false,
                success: function (response) {
                 for(var d=0;d<response.length;d++){
                     loadDocJsnDataNew(response[d],null)
            	}
            	
            	if(response.length == 0) {
                    $("#dynaAttachNRICList").html("-No Refernce Docs-");
					
					var totlDocListLen = $("#dynaAttachNTUCList").children().length;

					$("#noDocFoundSec").addClass("show").removeClass("hide")
					$("#DocListSec").addClass("hide").removeClass("show")
		
            	}else{
					
					$("#noDocFoundSec").addClass("hide").removeClass("show")
					$("#DocListSec").addClass("show").removeClass("hide")
					
					
            	}
            	
            },
	            error: function(xhr,textStatus, errorThrown){
		 			//$('#cover-spin').hide(0);
		 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
				}
            });
	}
	
	
	
	
	
	
	