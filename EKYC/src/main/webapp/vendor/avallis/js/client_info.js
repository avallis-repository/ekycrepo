

$('#srchClntModal').on('shown.bs.modal', function (e) {
    $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
});


var searchtable2 = $('#tblNricListRec').DataTable({
	   scrollX: true,
		scrollY:"300px",
		scroller: true,
		scrollCollapse:true,
		autoWidth:false,
		paging:false,
		info:false,
		searching:false,
		//pagingType: "simple",
		columnDefs: [
		                {
		                    "targets": [ 5 ],
		                    "visible": false,
	                    "searchable": false
	                }
		            ]
		
	});
	
//occupatuion array moved to constant.js file by poovathi(ekycdev change)

$(document).ready(function () {
	
	
	$('#cover-spin').hide(0);
     

	var customerDetails={};
	var gender="Male";
	
	   
	// show contact tooltip
	   $('[data-toggle="contactDetls"]').popover({
	    	placement :'bottom',
	    	html: true,
	       content: function() {
	         return $('#popover-contactDetls').html();
	       }
	     }); 
	  
	
	 
	//set Nationality field as   Default Singaporean 
//	 $("#nationality").val("Singaporean");//comment 28032021
	 
	// Set country field Default as Singapore
	$("#resCountry").val("SINGAPORE");
	$("#resState").prop("readOnly",true)
	$("#resCity").prop("readOnly",true)
	
	// set Customer Status default as A-APPLICANT
	$("#custStatus").val("A-APPLICANT");
	
	//poovathi add on 16-09-21 for set new client in client name field(ekycdev change)
	var newClient = $("#newClntName").val();
		if(!isEmpty(newClient)){
			$("#custName").val(newClient);
			toastr.clear($('.toast'));
			toastr.options = {  timeOut: "5000", extendedTimeOut: "1000"};
			toastr["success"]("New Client(1) Name  : " + newClient + " is  Successfully Created.");
			//after new Clnt Create focus to initials fld ,poovathi add on 21-09-21
			 $("#custInitials").focus();
		}
	

	
//	paraplanadviser =  JSON.parse(paraplanadviser);
	var totalpara = paraplanadviser.length;
  
	for(var para=0;para<totalpara;para++) {

	}
	
	var loggedUserStfLvl = $("#hTxtloggedUsrStftype").val();
	var loggedAdvId = $("#hTxtFldLoggedAdvId").val();
	if (loggedUserStfLvl == 'ADVISER'){
		$("#currAdviserId").val(loggedAdvId).prop("disabled",true);
	}else {
		$("#currAdviserId").removeAttr("disabled");
	}
	
//console.log(JSON.stringify(customer_details))
if(!isJsonObjEmpty(customer_details)){

for ( var cust in customer_details) {

	if (customer_details.hasOwnProperty(cust)) {

		var custvalue = customer_details[cust];
//		console.log(cust,custvalue)
		
		var clntGndr,clntSmgFlg;
		switch(cust){
			
		     //cust Staus -->Client validation
		
			case "custid":
				$("#custIdLabel").removeClass("d-none").html(custvalue);
				$("#"+cust).val(custvalue);
				break;
		
		    case "custStatus":
		    	if(custvalue == 'C-CLIENT'){
		    		$("#"+cust).val(custvalue);
		    		$("#custStatus").prop("disabled",true)
		    		//$("#"+cust).prop("readOnly",true);
		    	}else{
		    		$("#"+cust).val(custvalue);
		    		$("#custStatus").removeAttr("disabled")
		    		//$("#"+cust).prop("readOnly",false);
					chkLogUsrAdvStaffLevel();
		    	}
			break;
		
			case "sex":
				clntGndr = isEmpty(custvalue)?"M":custvalue;
				if(clntGndr == "F"){
					$("INPUT[name=sex]").val([custvalue]);
					$("#radBtnmale").prop("checked",false);
				}else{
					$("INPUT[name=sex]").val([custvalue]);
					$("#radBtnfemale").prop("checked",false);
				}
				
	  			break;
	  			
	  		case "smokerFlg":
	  			clntSmgFlg = isEmpty(custvalue)?"N":custvalue;
	  			
	  			if(clntSmgFlg == "Y"){
					$("INPUT[name=smokerFlg]").val([clntSmgFlg]);
				}else{
					$("INPUT[name=smokerFlg]").val([clntSmgFlg]);
				}
	  			
	  			break;
	  		case "typeofpdt":
	      		var typeofprd= isEmpty(custvalue)?"ALL":custvalue;
	      		typeofprd = (typeofprd == "ILP/INVESTMENTS" ) ? "INVESTMENT" : typeofprd;
	  			$("input[name=typeofpdt][value=" + typeofprd+ "]").attr('checked', true);
	      		break;
	      		
	  		case "dob":
	  			   $("#"+cust).val(custvalue);
	  			    // getAge(cust);
	  			    // set updated date in datepicker calendar
                    $('#simple-date1 .input-group.date').datepicker("update", custvalue);
	  			   
					$("#txtFldClntAge").val(calcAge(custvalue));
	  			break;
	  			
	  		case "resCountry":
	  			custvalue = custvalue.toUpperCase(); 
	  			
 			     if(custvalue == "SINGAPORE"){
 			    	  $("#"+cust).val(custvalue);
 			    	  $("#resCity").val("").prop("readonly",true)
 			    	  $("#resState").val("").prop("readonly",true)
 			    	 
 			     }else{
 			    	 $("#resCity").prop("readonly",false)
 			    	 $("#resState").prop("readonly",false)
 			    	 $("#"+cust).val(custvalue);
 			     }
 			    
 			break;

	  		case "currAdviserId":  
	  			
	  			if(!chkOptionExists("currAdviserId",custvalue)) {
      				addOption("currAdviserId",custvalue,custvalue)
      			}
	  			
	  			$("#"+cust).val(custvalue).prop("disabled",true);
	  			break;
	  			
	  		case "resHandPhone":
	  			
	  			if(!isEmpty(custvalue)){
	  			$("#"+cust).val(custvalue);
	  			// $("#ContactType").val("MOBILE")
	  			    $("#reshandPhList").html('<img src="vendor/avallis/img/phone.png" style="width:10px;">&nbsp;&nbsp;Mobile No : '+custvalue+' ');
	  			}else{
	  				$("#reshandPhList").html('<img src="vendor/avallis/img/phone.png" style="width:10px;">&nbsp;&nbsp;Mobile No : --NIL-- ');	
	  			}
	  		    break;
	  		    
	  			case "resPh":
	  			
	  			if(!isEmpty(custvalue)){
	  			$("#resPh").val(custvalue);	
	  		    	$("#resPhList").html('<img src="vendor/avallis/img/home.png" style="width:10px;">&nbsp;&nbsp;Home No : '+custvalue+' ');
	  			}else{
	  			   $("#resPhList").html('<img src="vendor/avallis/img/home.png" style="width:10px;">&nbsp;&nbsp;Home No : --NIL-- ');	
	  			}
	  		    break;
	  		   
               case "offPh":
	  			if(!isEmpty(custvalue)){
	  			$("#offPh").val(custvalue);
	  			    $("#offPhList").html('<img src="vendor/avallis/img/office.png" style="width:10px;">&nbsp;&nbsp;Office No : '+custvalue+' ');
	  			}else{
	  				$("#offPhList").html('<img src="vendor/avallis/img/office.png" style="width:10px;">&nbsp;&nbsp;Office No : --NIL-- ');	
	  			}
	  		    break;
	  		    
	  		
	  		    
               case "nric":
   	  			if(!isEmpty(custvalue)){
   	  			$("#"+cust).val(custvalue);
   	  			$("#nricType").val("NRIC")
   	  			}
   	  		    break;
   	  		    
   	  		  case "custPassportNum":
   	  			if(!isEmpty(custvalue)){
   	  		    $("#custPassportNum").val(custvalue);	
   	  			$("#nricType").val("Passport")
   	  			}
   	  		    break;
   	  		   
               case "custFin":
   	  			if(!isEmpty(custvalue)){
   	  			$("#custFin").val(custvalue);
   	  			$("#nricType").val("FIN")
   	  			}
   	  		    break;
	  		    
	  		    //nature of business
               case "businessNatr":
            	   //poovathi add on 21-09-21 to dynamic add business nature field(if option not exists)
            	   addDynaOptCombo(cust,custvalue);
            	   
            	if(custvalue == 'Others'){
   		    		$("#"+cust).val(custvalue);
   		    		$("#BusiNatOthSec").removeClass("hide").addClass("show")
   		    	}else{
   		    		$("#"+cust).val(custvalue);
   		    		$("#BusiNatOthSec").removeClass("show").addClass("hide");
   		    		$("#businessNatrDets").val("");
   					chkLogUsrAdvStaffLevel();
   		    	}
   			break;
	  		    
	  		default:
	      			$("#"+cust).val(custvalue);
			
			}

		}

	}	
}else{
	chkLogUsrAdvStaffLevel();
}





$('#finishedModal').on('shown.bs.modal', function () {
	$('.checkmark').hide();
    $('.circle-loader').removeClass('load-complete');
    $("#modalMsgLbl").html("Client Information being Saved...");
	 $("#generateKYCBtn").addClass("disabled");
	setTimeout(function(){
		$('.circle-loader').addClass('load-complete');
		$('.checkmark').show();
		$("#modalMsgLbl").html("Client Details Saved Successfully !");
		$("#generateKYCBtn").removeClass("disabled");
	},2500);
	
});



//setNric type default as Nric
if(!isEmpty($("#nric").val())){
	$("#nric").val($("#nric").val());
	$("#nricType").val("NRIC")
}else{
	if(!isEmpty($("#custPassportNum").val())){
		$("#nricType").val("Passport");
        $("#nric").val($("#custPassportNum").val())
	}else{
		if(!isEmpty($("#custFin").val())){
		 $("#nricType").val("FIN")
         $("#nric").val($("#custFin").val())
		}
	}
}

//Poovathi add fnaList select combo Select 2 Initialization on 05-05-2021
$('#selFldFnaList').select2({
	   templateResult: formatFnaListOpt,
	   placeholder: '--Select FNA Id below--'
});


//poovathi add autocomplete list for occupatuion combo on 27-08-2021
//poovathi add on 08-06-2021
 var $input = $("#occpnDesc");
 $input.typeahead({
  source:OccuAutoCompList,
  autoSelect: false
});
// End

});

//Poovathi add fnaList select combo Select 2 Initialization on 05-05-2021
function formatFnaListOpt (fnaList) {
		
		  if (!fnaList.id ) {
			 return fnaList.text;
		  }
		  
		  if( fnaList.id == "Select FNA Id/Add New FNA Id"){
			  return fnaList.text;
		  }
		 
		  var image = "vendor/avallis/img/idCard.png";
		  var $fnaList = $(
		         '<span><img style="width:25px" src="' + image +'" class="img-flag" /> ' + fnaList.text + '</span>'
		  );
		  return $fnaList;
}
	
function setClientDob(obj){
	
	//vignesh add on 13-05-2021		
	var date_regex = /^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/;
	if (!(date_regex.test(obj.value))) {
		  	
			$("#dobError").removeClass("hide").addClass("show");
		    $("#dobError").html("Keyin Valid DOB.");
			$("#dob").val("").focus();
			$("#txtFldClntAge").val("");
		
	    return false;
	    
	}

	 $("#dobError").removeClass("show").addClass("hide");
     $("#txtFldClntAge").val(calcAge(obj.value));

}


function openTabSec(value){
	
	if(value == 'Personal'){
		showPersnalSec();
	  }

	if(value == 'Address'){
	//client personal details validation here
	if(!validateClntInfoPersonalDetls()){showPersnalSec();return;}
	    showAddrSec();
	 }

	if(value == 'LOB'){
	//client address details validation here
    if(!validateClntInfoAddrDetls()){showAddrSec(); return;}
    showLobSec();
    }
}

function showPersnalSec(){
	$('#btnResiAddress').removeClass('js-active');
	$('#btnLOB').removeClass('js-active');
	$('#btnPersonalInfo').addClass('js-active');

	$('#AddrSec').removeClass('js-active');
	$('#LOBSec').removeClass('js-active');
	$('#PerInfoSec').addClass('js-active');
}

function showAddrSec(){
	$('#btnPersonalInfo').removeClass('js-active');
	$('#btnLOB').removeClass('js-active');
	$('#btnResiAddress').addClass('js-active');

	$('#LOBSec').removeClass('js-active');
	$('#PerInfoSec').removeClass('js-active');
	$('#AddrSec').addClass('js-active');
}

function showLobSec(){
	$('#btnPersonalInfo').removeClass('js-active');
	$('#btnResiAddress').removeClass('js-active');
	$('#btnLOB').addClass('js-active');

	$('#PerInfoSec').removeClass('js-active');
	$('#AddrSec').removeClass('js-active');
	$('#LOBSec').addClass('js-active');
}




function validateClientMand(){
	
	//validate 3 tabe data with mandatory and return false if not
	
	if(!validateClntInfoPersonalDetls()){showPersnalSec();return;}
	
	if(!validateClntInfoAddrDetls()){showAddrSec();return;}
	
	if(!validateClntInfoLobDetls()){return;}
	
	if($("#custStatus").val() == "C-CLIENT"){
		$("#custStatus").removeAttr("disabled");	
	}	
	$("#currAdviserId").removeAttr("disabled");
	

	
	//nric type validation 
	
	var nricType = $("#nricType").val();
	if(nricType == 'NRIC'){
		$("#nric").val($("#nric").val());
		$("#custFin").val("");
		$("#custPassportNum").val("");
	}else if(nricType == 'Passport'){
		$("#custPassportNum").val($("#nric").val());
		$("#nric").val("");
		$("#custFin").val("");
	}else if(nricType == 'FIN'){
		$("#custFin").val($("#nric").val());
		$("#nric").val("");
		$("#custPassportNum").val("");
	}
	
	 var frmElements = $('.customerDetailsFrm :input').serializeObject();
//	 console.log(JSON.stringify(frmElements));
		$.ajax({
	            url:"customerdetails/register/client",
	            async:false,
	            type: "POST",
	            dataType: "json",
	            contentType: "application/json",
	            data: JSON.stringify(frmElements),
	            success: function (response) {
		
		        $('#finishedModal').modal('show');
	
	            	$("#custid").val(response.custid);   
	            	$("#createdDate").val(response.createdDate);
	            	$("#createdBy").val(response.createdBy);

				if($("#custStatus").val() == "C-CLIENT"){
						$("#custStatus").prop("disabled",true);	
					}
				$("#currAdviserId").prop("disabled",true);	
					            	
	            },
	            error: function(xhr,textStatus, errorThrown){
		 			ajaxCommonError(textStatus,xhr.status,xhr.statusText);
				}
	            });
	
}

$("#generateKYCBtn").on("click",function(){
	//hide ok button when click generate Ekyc btn
	$("#btncloseModl").addClass("hide");
	$('#finishedModal').modal('hide');
	window.location.href="kycIntro"
	//var custId = $("#hTxtFldCurrCustId").val();
	
	//var custId = $("#custid").val();
	//poovathi commented on 24-08-21
	/*var fnaId = $("#hdnLatestArchFnaId").val();
	
	if(isEmpty(fnaId)){
		window.location.href="kycIntro"
	}else{
		if(fnaId == "CRTFNA"){
			window.location.href="CreateNewFNA"
		}else{
			callAjaxParam(fnaId);
		}
	}*/
		
	
	//commented by poovathi on 10-06-2021
	  /*$.ajax({
	        url:baseUrl+"/customerdetails/getAllFnaList/"+custId,
	        type: "GET",
            //dataType: "json",
            async:false,
            contentType: "application/json",
	        success: function (data) {
	        	
	        	$('#cover-spin').hide(0);
	        	var fnaList = JSON.parse(data)
	        	
	        	var fnaListLen = fnaList.length;
	        	
	        	var custName = $("#custName").val();
	        	var fnaListInfo= '<span class="font-sz-level6  text-custom-color-gp" title="Total FNA List : '+fnaListLen+'">Client : <strong>'+custName+'</strong> totally have '
	        	+'<span class="badge badge-pill badge-primary" title="'+fnaListLen+'">'+ fnaListLen +'</span>&nbsp;FNA List(s). Select anyone of the FNA Id and Click <strong>Proceed</strong> Button.</span>';
	        	 $("#fnaListCountInfo").prop("title","Total FNA List : "+ fnaListLen );
	        	if(fnaListLen > 0){
	        		$('#fnaListModal').modal('show');
	        		 //if fnaList exists ,have to enable search fnadetls radio button 
	        		 $("input:radio[name=radfnaList]:last").prop('checked', true);
	        		//show notification msg
	        		 $("#fnaListCountInfo").html(fnaListInfo).addClass("show").removeClass("hide");
	        		 $.each(fnaList,function(key,val){
		        		$.each(val,function(key,val){
		        			if((key == "fnaId")&&(val != undefined)){
		        				$("#selFldFnaList").find('optgroup#searchExistFnaList').append('<option value="'+val+'" >'+val+'</option>');
		        			}
			        	});
		        	});
	        		
	        		if(fnaListLen == 1){
	        		    	
	        		    	var fnaId = $('#searchExistFnaList > option').eq(1).val();
	        		    	$('#fnaListModal').modal('hide');
	        		    	//console.log("only one fna id"+ fnaId)
	        		    	window.location.href="kycIntro?id="+fnaId;
	        		    	
	        			  //if only one fnalist present,have to set that fnaid in combo
	        			  $("#selFldFnaList").val($('#searchExistFnaList > option').eq(1).val());
	        			  //$('#selFldFnaList').trigger('change.select2');
		        	}
	        		
	        		if(fnaListLen > 1 ){
	        			 //if only one fnalist present,have to set that fnaid in combo
	        			  $('#finishedModal').modal('show');
	        			  $("#selFldFnaList").val($('#searchExistFnaList > option').eq(0).val());
	        			  $('#selFldFnaList').trigger('change.select2');
	        			  
	        		}
	        		
	        	}
	        	
	        	//if no fnaList exist navigate to create new fna Section
	        	if(fnaListLen == 0){
	        		$('#fnaListModal').modal('hide');
	        		$("#fnaListCountInfo").html("").addClass("hide").removeClass("show");
	        		window.location.href="kycIntro"
	        	}
	        	
	        	
	        	
	        	
	         },
	 		   error: function(xhr,textStatus, errorThrown){
	 			$('#cover-spin').hide(0);
	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			}
	        });*/
	
});

 
 
//show Drag and drop Text Content...in File Upload Modal
 $('#fnaListModal').on('shown.bs.modal', function () {
	$('#cover-spin').hide(0);
 })
 //End
 
 //poovathi commented on 24-08-2021
 /*function validateCrtNewFna(){
	 
	 var curFnaId = "";
	 
   	   var curOpt =  $('input[name="radfnaList"]:checked').val();
   	   if(curOpt == "SRCHFNA"){
   		   
   		   curFnaId = $("#selFldFnaList option:selected").val();
   		   
   		   if(isEmpty(curFnaId)){
   			    $("#searchExistFnaListError").addClass("show").removeClass("hide")
   			    $("#selFldFnaList").focus();
   			    return false;
   		   }else{
   			    $("#searchExistFnaListError").addClass("hide").removeClass("show") 
   			 }
   		   
   		 callAjaxParam(curFnaId);
   		   
   		 }else if(curOpt == "CRTFNA"){
   			  window.location.href="CreateNewFNA"
   		 }
   	   
   	   
 }*/
 
 //poovathi commented on 24-08-21
/* // hide error message of fnaListCombo
 function getFnaListVal(selObj){
	var  curFnaId = $("#selFldFnaList option:selected").val();
	  if(isEmpty(curFnaId)){
			    $("#searchExistFnaListError").addClass("show").removeClass("hide")
			    $("#selFldFnaList").focus();
			    return false;
		   }else{
			    $("#searchExistFnaListError").addClass("hide").removeClass("show") 
	  }
 }
 
 
 function createNewFnaFun(){
	 $("#searchExistFnaListError").addClass("hide").removeClass("show");
	 $('#selFldFnaList').val('');
	 $('#selFldFnaList').select2({
		   templateResult: formatFnaListOpt,
		   placeholder: '--Select FNA Id below--'
	});

 }
  
 
 function callAjaxParam(curFnaId){
	 
	//alert(curFnaId)
	 
	 $.ajax({
	        url:baseUrl+"/fnadetails/register/Fnadetails/"+curFnaId, 
	        type: "GET",
            dataType: "json",
            async:false,
	        contentType: "application/json",
	        success: function (data) {
	        	 //alert(data.FNA_ID)
	        	 var fnaId = window.btoa(data.FNA_ID);
	        	 window.location.href="kycIntro?id="+fnaId;
	       },
	       error: function(xhr,textStatus, errorThrown){
	 			$('#cover-spin').hide(0);
	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			}
    });
	 
}*/

 
//Client Info Personal Detls mandotary Fld Validation
 function validateClntInfoPersonalDetls(){
 	var custName,custInials,dob,clntStatus,custEmail,busNature,busNatOthDet,
 	nricType,nric,passport,fin,Fld="Error";
 	 clntStatus = $('#custStatus').val();
 	 custName = $('#custName').val();
 	 custInials = $('#custInitials').val();
 	 dob = $('#dob').val();
 	 clntStatus = $('#custStatus').val();
 	 custEmail = $('#emailId').val();
 	 busNature = $('#businessNatr').val();
 	 busNatOthDet = $('#businessNatrDets').val();
 	 var advId = $("#currAdviserId").val();
 	 nricType = $('#nricType').val();
 	nric = $('#nric').val();
 	// passport = $('#custPassportNum').val();
 	// fin = $('#custFin').val();
 	//poovathi add on 16-08-2021 
 	var mobno = $("#resHandPhone").val();
 	var phno = $("#resPh").val();
 	var offno = $("#offPh").val();
 	//end 
 	 
 	 
 	//Client Name
 	  if(isEmptyFld(advId)){
 			$("#currAdviserId"+Fld).removeClass("hide").addClass("show");
 			$("#currAdviserId").addClass('err-fld');
 			$("#currAdviserId").focus();
 			return;
 		}else{
 			$("#currAdviserId"+Fld).removeClass("show").addClass("hide");
 			$("#currAdviserId").removeClass('err-fld');
 		}
 	 
 	 if(isEmptyFld(clntStatus)){
 			$("#custStatus"+Fld).removeClass("hide").addClass("show");
 			$("#custStatus").addClass('err-fld');
 			$("#custStatus").focus();
 			return;
 		}else{
 			$("#custStatus"+Fld).removeClass("show").addClass("hide");
 			$("#custStatus").removeClass('err-fld');
 		}
 	 
 	 //Client Name
 	  if(isEmptyFld(custName)){
 			$("#custName"+Fld).removeClass("hide").addClass("show");
 			$("#custName").addClass('err-fld');
 			$("#custName").focus();
 			return;
 		}else{
 			$("#custName"+Fld).removeClass("show").addClass("hide");
 			$("#custName").removeClass('err-fld');
 		}
 	  
 	 //Client Initials
 	   if(isEmptyFld(custInials)){
 			$("#custInitials"+Fld).removeClass("hide").addClass("show");
 			$("#custInitials").addClass('err-fld');
 			$("#custInitials").focus();
 			return;
 		}else{
 			$("#custInitials"+Fld).removeClass("show").addClass("hide");
 			$("#custInitials").removeClass('err-fld');
 		}
 	   
       //Nric Key Fld Validation 
 	   
 	   if(nricType == "NRIC" || nricType == "FIN" || nricType == "Passport"){
 		  if(isEmpty(nric)){
 				$("#nricError").html(" keyin "+ nricType + " value");
 				$("#nric").addClass('err-fld');
 				$("#nric").focus();
 				return;
 			}else{
 				$("#nricError").html("");
 				$("#nric").removeClass('err-fld');
 			}
 		}
 	   
 	   if(isEmptyFld(custEmail)){
 			$("#emailIdFld"+Fld).removeClass("hide").addClass("show");
 			$("#emailId").addClass('err-fld');
 			$("#emailId").focus();
 			return;
 		}else{
 			$("#emailIdFld"+Fld).removeClass("show").addClass("hide");
 			$("#emailId").removeClass('err-fld');
 		}
 	  
 	   //validate email format while ckick next btn
 	     if(!isEmptyFld(custEmail)){
 			var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
 		         if(!regex.test(custEmail)) {
 					$("#emailFldError").removeClass('hide').addClass('show');
 					$("#emailFldError").html("Invalid Email Id Format");
 					$("#emailId").focus();
 			    return;
 			  }else{
 				  $("#emailFldError").removeClass('show').addClass('hide');
 			     }
 		    }
 	   
 	  // poovathi add contact fld validation on 16-08-2021
 	     if(isEmptyFld(mobno) && isEmptyFld(phno) && isEmptyFld(offno) ){
 	    	    $("#btnConSec").addClass("show").removeClass("hide");
 			    $("#contactFld"+Fld).removeClass("hide").addClass("show");
 				$("#resHandPhone").addClass('err-fld');
 				$("#resPh").addClass('err-fld');
 				$("#offPh").addClass('err-fld');
 				$("#resHandPhone").focus();
 			 return;
 		 }else{
 			    $("#contactFld"+Fld).removeClass("show").addClass("hide");
 				$("#resHandPhone").removeClass('err-fld');
 				$("#resPh").removeClass('err-fld');
 				$("#offPh").removeClass('err-fld');
 				$("#btnConSec").addClass("hide").removeClass("show");
 		 }
 	    //end of contact field Validation 
 	     
 	   
 	 //BusinessNature Others Fld Validation
 	   var busNatOthDetFlg = $("#BusiNatOthSec").hasClass("show")
 	   if((busNatOthDetFlg == true)&&(busNature == 'Others')){
 		   if(isEmptyFld(busNatOthDet)){
 			    $("#businessNatrDets"+Fld).removeClass("hide").addClass("show");
 				$("#businessNatrDets").addClass('err-fld');
 				$("#businessNatrDets").focus();
 				return;
 			}else{
 				$("#businessNatrDets"+Fld).removeClass("show").addClass("hide");
 				$("#businessNatrDets").removeClass('err-fld');
 			}
 	   }
 	   
 	return true;
 }

 

 
function validateClntInfoAddrDetls(){
	
	var resaddr1,resContry,resPostCde;Fld="Error";
	
	resaddr1 = $('#resAddr1').val();
	
    
	resContry = $('#resCountry').val();
	resPostCde = $('#resPostalcode').val();
	
	 if(isEmptyFld(resaddr1)){
			$("#resAddr1"+Fld).removeClass("hide").addClass("show");
			$("#resAddr1").addClass('err-fld');
			$("#resAddr1").focus();
			return;
		}else{
			$("#resAddr1"+Fld).removeClass("show").addClass("hide");
			$("#resAddr1").removeClass('err-fld');
		}
	 
	
	   if(isEmptyFld(resContry)){
			$("#resCountry"+Fld).removeClass("hide").addClass("show");
			$("#resCountry").addClass('err-fld');
			$("#resCountry").focus();
			return;
		}else{
			$("#resCountry"+Fld).removeClass("show").addClass("hide");
			$("#resCountry").removeClass('err-fld');
		}
	   
	   if(isEmptyFld(resPostCde)){
			$("#resPostalcode"+Fld).removeClass("hide").addClass("show");
			$("#resPostalcode").addClass('err-fld');
			$("#resPostalcode").focus();
			return;
		}else{
			$("#resPostalcode"+Fld).removeClass("show").addClass("hide");
			$("#resPostalcode").removeClass('err-fld');
		}
	 
	   return true; 
}


//validate lob Details

function validateClntInfoLobDetls(){
	
	var strClntLob  = $('input[name="typeofpdt"]:checked').val();
	
	
	
	 if(isEmptyFld(strClntLob)){
			$("#LOBError").removeClass("hide").addClass("show");
			return;
		}else{
			$("#LOBError").removeClass("show").addClass("hide");
		}
	 return true;
}


function chkLob(obj){
	
	if(obj.checked == true){
		$("#LOBError").removeClass("show").addClass("hide");
	}
   if(obj.checked == false){
	   $("#LOBError").removeClass("hide").addClass("show");
	}
}

function setCustGender(obj,Ele){
	var gndrFlg = $(obj).hasClass('active');
	if(gndrFlg == true){
		$("#"+Ele).val("M");
	}else{
		$("#"+Ele).val("F");
	}
}

function getAge(obj){
	
	
	var Dob  = $("#"+obj).val();
	
	 if(isEmptyFld(Dob)){
		    $("#txtFldClntAge").val("");
			$("#txtFldClntAge").removeAttr("disabled",true);
			return;
		}else{
            var today = new Date();
		    var birthDate = new Date(Dob);
		    var age = today.getFullYear() - birthDate.getFullYear();
		    
		    var m = today.getMonth() - birthDate.getMonth();
		    
		    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
		        age--;
               }
		    if(age >= 0){
		    	$("#dobError").removeClass("show").addClass("hide");
        	    $("#txtFldClntAge").val(age);
			    $("#txtFldClntAge").prop('disabled',"disabled");
		    
		      }else{
		    	//$("#dobError").removeClass("hide").addClass("show");
			  //  $("#dobError").html("Keyin Valid DOB.");
			    $("#txtFldClntAge").val(age);
				$("#txtFldClntAge").removeAttr("disabled",true);
		    }
		  
		 }
  
}

// contry field validation 

function filtrCuntry(obj){
	var thisVal = $(obj).find("option:selected").val();
	
	if(thisVal == "SINGAPORE"){
	    	 $("#resCity").val("").prop("readonly",true)
	    	 $("#resState").val("").prop("readonly",true)
	     }else{
	    	 $("#resCity").prop("readonly",false)
	    	 $("#resState").prop("readonly",false)
	     }
}

// Client Status Field Validation

function chkLogUsrAdvStaffLevel(){
	var loggedUserStfLvl = $("#hTxtloggedUsrStftype").val();
	var loggedAdvId = $("#hTxtFldLoggedAdvId").val();
	var loggedDistId = "DIS000000001"
	
	
	if (loggedUserStfLvl == 'ADVISER'){
		$("#custStatus > option").each(function(){
			if($(this).val() =='C-CLIENT'){
				$(this).prop("disabled",true);
				$(this).text($(this).text() + "(Adviser Cannot select this)")
				//$("#custStatus").val("");
		      }
		});
		
	}
//	alert(loggedUserStfLvl)
	if(loggedUserStfLvl == 'PARAPLANNER'){
		
		
		
	}
	
	else{
		$("#custStatus").find("option:selected").prop("disabled",false);
	}
	
}


//clientInfo page Mandotary field Validation onchange input value hide error Messages

$(".checkMand").bind("change",function(){
	var thisFldId = $(this).attr("id");
	var thisFldVal = $(this).val();
	
	if(isEmptyFld(thisFldVal)){
        $("#"+thisFldId+"Error").removeClass("hide").addClass("show");
        $("#"+thisFldId).addClass('err-fld');
		return;
	  }else{
		  $("#"+thisFldId+"Error").removeClass("show").addClass("hide");
		  $("#"+thisFldId).removeClass('err-fld');
		 }
})


$('input[name=nric]').change(function(){
	
	var strCustId = $("#custid").val();
	
	var strNric = $(this).val();
	var strAdvId = $("#hTxtFldLoggedAdvId").val();
	

	
//	    var tblId = document.getElementById("tblNricListRec");
//		var tbody = tblId.tBodies[0]; 
		searchtable2.clear().draw();
		var col0="",col1="",col2="",col3="",col4="",col5="",col6="";
	    $.ajax({
	  	        url: baseUrl+"/customerdetails/getAllNricList/"+strNric,
	  	            type: "GET",
	  	            async:false,
	  	            dataType: "json",
		            contentType: "application/json",
		            data : {"CustId":strCustId,"adviserId":strAdvId},
	  	            success: function (data) {
	  	            	
	  	            	if(data.length >0){
	  	            		$('#srchClntModal').modal('show');
	  	            		
	  	            	}else{
	  	            	//	$('#srchClntModal').modal('hide');
	  	            	//	swal.fire("No Nric List Found for You.");
	  	            	}
	  	            	
	  	            	$("#srchClntModal").find(".modal-body").find("#srchClntTblSec").removeClass('hide').addClass('show');
	  	            	
	  	            	//$("#tblNricListRec th").trigger("click")
	  	          	    
	  	            	//var data = JSON.parse(data);
	  	            	for (var i=0; i<data.length; i++) {
	  	            		var jsonObj = (data[i]);
	  	            		
	  	            		col0=jsonObj.custName;
			            	col1=jsonObj.custInitials;
			            	col2=jsonObj.nric;
			            	col3=jsonObj.dob;
			            	col4=jsonObj.resHandPhone;
			            	col5=jsonObj.custid;
			            	searchtable2.row.add( [col0,col1,col2,col3,col4,col5] ).draw( false );
            	
                          }
	  	            	
	  	            	$('#tblNricListRec tbody').on('click', 'tr', function () {
	  	            	    var data = searchtable2.row(this).data();
	  	            	    var custId = data[5];
	  	            	    //poovathi add on 12-07-2021
	  	            	     custId = window.btoa(custId);
	  	            	     window.location.href="clientInfo?c="+custId;
	  	            	} );
	  	            	
	  	            	$('#tblNricListRec tbody').on('mouseover', 'tr', function () {
	  	            		$(this).css('cursor', 'pointer');
	  	            		$(this).css('background', '#337ab7');
	  	            		$(this).css('color', '#fff');
	  	            	});
	  	            	
	  	            	$('#tblNricListRec tbody').on('mouseout', 'tr', function () {
	  	            		$(this).css('cursor', '');
	  	            		$(this).css('background', '');
	  	            		$(this).css('color', '');
	  	            	});
                           
	  	            
	  	           
	  	            },
	  	          error: function(xhr,textStatus, errorThrown){
	  	 			ajaxCommonError(textStatus,xhr.status,xhr.statusText);
	  			}
	  	            });
	});


$(".btnNricMdlClse").bind("click",function(){
	
	     Swal.fire({
//		  title: 'Are you Not Proceed?',
		  text: "Do you want to clear NRIC value in client information screen?",
		  icon: 'question',
         allowOutsideClick:false,
          allowEscapeKey:false,
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, Clear it!'
		}).then((result) => {
		  if (result.isConfirmed) {
			  $("#nric").val('');
		      swal.fire("Cleared!", "Your NRIC Value in Client Information Screen", "success");
		      $('#srchClntModal').modal('hide');
		  }else{
			  swal.fire("Select!", "Any on of the Row in above Table", "info");
			  $('#srchClntModal').modal('show');
			}
		  
		})
})

$("#currAdviserId").bind("change",function(){
	var strSelAdv = $(this).val();
	if(isEmpty(strSelAdv)) {
		$("#currAdviserIdError").addClass("show").removeClass("hide");
		$("#currAdviserId").addClass("err-fld");
		
	}else {
		$("#currAdviserIdError").removeClass("show").addClass("hide");
		$("#currAdviserId").removeClass("err-fld");
	}
})

// contact type combo validation
/*
 * function setContactType(selObj){ var curntContType = $(selObj).val();
 * if(curntContType == "MOBILE"){
 * $("#resHandPhone").val($("#resHandPhone").val()); } if(curntContType ==
 * "HOME"){ $("#resHandPhone").val($("#resPh").val()); }if(curntContType ==
 * "OFFICE"){ $("#resHandPhone").val($("#offPh").val()); } }
 */

function setnricType(selObj){
	var curntContType = $(selObj).val();
	
	var nric = $("#nric").val();
	if(!isEmpty(nric)){
		$("#nricError").html("");
		$("#nric").removeClass('err-fld');
	}else{
		$("#nricError").html(" keyin "+ curntContType + " value");
		$("#nric").focus();
	}
	    
	
    if(curntContType == "NRIC"){
		$("#nric").val($("#nric").val());
	} if(curntContType == "Passport"){
		$("#nric").val($("#custPassportNum").val());
	}if(curntContType == "FIN"){
		$("#nric").val($("#custFin").val());
	}
}


//validate BusinessNature fld validation

function valiBusNatOthers(selObj){
	$("#businessNatrDets").val("");
	var selFldVal= $(selObj).val();
	if (selFldVal == 'Others'){
		 $("#BusiNatOthSec").removeClass("hide").addClass("show");
		 if(isEmpty($("#businessNatrDets").val())){
			 $("#businessNatrDets").addClass("err-fld");
			 $("#businessNatrDets").focus();
			 $("#businessNatrDetsError").addClass("show").removeClass("hide");
		 }else{
			 $("#businessNatrDets").removeClass("err-fld");
			 $("#businessNatrDetsError").removeClass("show").addClass("hide");
		 }
	}else{
		$("#BusiNatOthSec").removeClass("show").addClass("hide");
	}
}


//poovathi add on 10-05-2021

function setContDetls(){
	
	var mobno = $("#resHandPhone").val();
	var homeno = $("#resPh").val();
	var offno = $("#offPh").val();
	
	 if( !isEmpty( mobno ) ){
		  $("#reshandPhList").html('<img src="vendor/avallis/img/phone.png" style="width:10px;">&nbsp;&nbsp;Mobile No : '+mobno+' '); 
		  //poovathi add on 15-09-21 to hide content sec
		  $("#btnConSec").removeClass("show").addClass("hide");
	 }else{
		  $("#reshandPhList").html('<img src="vendor/avallis/img/phone.png" style="width:10px;">&nbsp;&nbsp;Mobile No : --NIL-- '); 
	 }
	  
	 if( !isEmpty( homeno ) ){
		 $("#resPhList").html('<img src="vendor/avallis/img/home.png" style="width:10px;">&nbsp;&nbsp;Home No : '+homeno+' ');  
		 //poovathi add on 15-09-21 to hide content sec
		  $("#btnConSec").removeClass("show").addClass("hide");
	 }else{
		 $("#resPhList").html('<img src="vendor/avallis/img/home.png" style="width:10px;">&nbsp;&nbsp;Home No : --NIL-- ');
	 }
	 
	 if( !isEmpty( offno ) ){
		 $("#offPhList").html('<img src="vendor/avallis/img/office.png" style="width:10px;">&nbsp;&nbsp;Office No : '+offno+' ');
		//poovathi add on 15-09-21 to hide content sec
		  $("#btnConSec").removeClass("show").addClass("hide");
	 }else{
		 $("#offPhList").html('<img src="vendor/avallis/img/office.png" style="width:10px;">&nbsp;&nbsp;Office No : --NIL--');  
	 }
}

//poovathi commented sg locate and myInfo button actions as per sabrina req doc start (ekycdev change)
//poovathi add MYInfo and SGLocate related functionality here
/*$("#btnMyInfo").on("click", function () {
	var nricCode = $("#nric").val();
		if (nricCode) {
		$.ajax({
			type: "GET",
			// url: "https://sandbox.api.myinfo.gov.sg/com/v3/person-sample/" + nricCode,
			url: "externalApi/getMyInfo/" + nricCode,
			success: function (resultStr) {
				if (resultStr) {
					var result = JSON.parse(resultStr);
					if (result.status == "success" && result.response) {
						populateClientInfoFields(JSON.parse(result.response));
					} else if (result.status == "failed" && result.response) {
						if (JSON.parse(result.response) && JSON.parse(result.response).message) {
							Swal.fire({
								icon: 'error',
								text: JSON.parse(result.response).message,
							});
						}
					} else {
						Swal.fire({
							icon: 'error',
							text: 'Please Try again Later or else Contact your System Administrator',
						});
					}
				} else {
					Swal.fire({
						icon: 'info',
						text: 'myInfo is not available for the given NRIC code',
					});
				}
			},
			error: function (error) {
				Swal.fire({
					icon: 'error',
					text: 'Please Try again Later or else Contact your System Administrator',
				});
			}
		});
	} else {
		$("#nricError").addClass("show").removeClass("hide");
		$("#nric").focus();
		Swal.fire({
			icon: "error",
			html: "Please key in <b>NRIC</b>!"
		}).then((result) => {
			if (result.isConfirmed) {
				$("#nric").addClass("err-fld");
			}
		});
	}
});

$("#nric").on("keydown", function () {
	$("#nricError").addClass("hide").removeClass("show");
	$("#nric").removeClass("err-fld");
});

function populateClientInfoFields(myInfo) {
	if (myInfo.name) {
		$("#custName").val(myInfo.name.value);
	}

	if (myInfo.dob && myInfo.dob.value) {
		var dobArray = myInfo.dob.value.split("-");
		if (dobArray && dobArray.length == 3) {
			$("#dob").val(dobArray[2] + "/" + dobArray[1] + "/" + dobArray[0]);
			$("#simple-date1 .input-group.date").datepicker("setDate", $("#dob").val());
			$("#txtFldClntAge").val(calcAge($("#dob").val()));
		}
	}

	if (myInfo.marital && myInfo.marital.desc && myInfo.marital.desc.length > 0) {
		$("#maritalStatus").val(myInfo.marital.desc[0].toUpperCase() + myInfo.marital.desc.substring(1).toLowerCase());
	}

	if (myInfo.sex) {
		$("input[name='sex'][value='" + myInfo.sex.code).prop("checked", true);
	}

	if (myInfo.race && myInfo.race.desc) {
		var raceArr = ["American", "Australian", "Chinese", "Indian", "Singaporean", "Malay", "European"];
		var desc = myInfo.race.desc.substring(0, 1) + myInfo.race.desc.substring(1).toLowerCase();
		if (!raceArr.includes(desc)) {
			desc = "Others";
		}
		if (desc) {
			$("#race").val(desc);
		}
	}

	if (myInfo.email) {
		$("#emailId").val(myInfo.email.value);
	}

	if (myInfo.mobileno) {
		var mobilenoValue = "";
		if (myInfo.mobileno.prefix) {
			mobilenoValue += myInfo.mobileno.prefix.value;
		}
		if (myInfo.mobileno.areacode) {
			mobilenoValue += myInfo.mobileno.areacode.value;
		}
		if (myInfo.mobileno.nbr) {
			mobilenoValue += myInfo.mobileno.nbr.value;
		}
		$("#resHandPhone").val(mobilenoValue).trigger("change");
	}

	if (myInfo.nationality && myInfo.nationality.code) {
		$("#nationality").val(myInfo.nationality.code);
	}

	if (myInfo.cpfemployers && myInfo.cpfemployers.history && myInfo.cpfemployers.history.length > 0) {
		var lastEmployerHistory = myInfo.cpfemployers.history[myInfo.cpfemployers.history.length - 1];
		if (lastEmployerHistory && lastEmployerHistory.employer) {
			$("#companyName").val(lastEmployerHistory.employer.value);
		}
	}

	if (myInfo.cpfcontributions && myInfo.cpfcontributions.history && myInfo.cpfcontributions.history.length > 0) {
		var lastContributionHistory = myInfo.cpfcontributions.history[myInfo.cpfcontributions.history.length - 1];
		if (lastContributionHistory && lastContributionHistory.amount) {
			$("#income").val(lastContributionHistory.amount.value);
		}
	}

	if (myInfo.occupation) {
		$("#occpnDesc").val(myInfo.occupation.value);
	}

	if (myInfo.hdbownership && myInfo.hdbownership.length > 0) {
		$("#tBodyAddress").empty();
		if (myInfo.hdbownership.length == 1) {
			var ownerAddress = myInfo.hdbownership[0];
			if (ownerAddress && ownerAddress.address) {
				if (ownerAddress.address.block) {
					$("#resAddr1").val(ownerAddress.address.block.value);
				}
				if (ownerAddress.address.building) {
					$("#resAddr2").val(ownerAddress.address.building.value);
				}
				if (ownerAddress.address.street) {
					$("#resAddr3").val(ownerAddress.address.street.value);
				}
				if (ownerAddress.address.postal) {
					$("#resPostalCode").val(ownerAddress.address.postal.value);
				}
			}
		} else {
			var tableData = "";
			for (var addrIndex = 0; addrIndex < myInfo.hdbownership.length; addrIndex++) {
				var ownerAddress = myInfo.hdbownership[addrIndex];
				tableData += "<tr>"
				if (ownerAddress && ownerAddress.address) {
					if (ownerAddress.address.block) {
						tableData += "<td class='tdBlkAddr'>" + ownerAddress.address.block.value + "</td>";
					}
					if (ownerAddress.address.building) {
						tableData += "<td class='tdBldngAddr'>" + ownerAddress.address.building.value + "</td>";
					}
					if (ownerAddress.address.street) {
						tableData += "<td class='tdStrtAddr'>" + ownerAddress.address.street.value + "</td>";
					}
					if (ownerAddress.address.postal) {
						tableData += "<td class='tdPostAddr'>" + ownerAddress.address.postal.value + "</td>";
					}
				}
				tableData += "</tr>";
			}
			$("#tBodyAddress").append(tableData);
			$("#addressModal").modal("show");
			$("#tBodyAddress tr").css("cursor", "pointer");
		}
	}
}

$("#tBodyAddress").on("click", "tr", function () {
	var block = $(this).closest("tr").find(".tdBlkAddr").text();
	var building = $(this).closest("tr").find(".tdBldngAddr").text();
	var street = $(this).closest("tr").find(".tdStrtAddr").text();
	var postal = $(this).closest("tr").find(".tdPostAddr").text();
	if (block) {
		$("#resAddr1").val(block).removeClass("err-fld");
		$("#resAddr1Error").removeClass("show").addClass("hide");
	}
	if (building) {
		$("#resAddr2").val(building);
	}
	if (street) {
		$("#resAddr3").val(street).removeClass("err-fld");
		$("#resAddr3Error").removeClass("show").addClass("hide");
	}
	
	if (postal) {
		$("#resPostalcode").val(postal);
		$("#resPostalCode").removeClass("err-fld");
		$("#resPostalCodeError").removeClass("show").addClass("hide");
	}
	$("#addressModal").modal("hide");
});

$("#resPostalCode, #resAddr3, #resAddr1").on("keydown", function () {
	$("#resPostalCode, #resAddr3, #resAddr1").removeClass("err-fld");
});



$("#btnSgLocate").on("click", function () {
	var postalCode = $("#resPostalcode").val();
	
	var buildingNumber = $("#resAddr1").val();
	var streetName = $("#resAddr3").val();
	var requestBody = {};
	requestBody["Postcode"] = postalCode;
	requestBody["Block"] = buildingNumber;
	requestBody["StreetName"] = streetName;
	var api;
	if (postalCode) {
		api = "postCode";
	} else if (streetName && buildingNumber) {
		api = "streetAndBuilding";
	}
	if (api) {
		$.ajax({
			type: "POST",
			url: "externalApi/getSgLocate",
			data: JSON.stringify(requestBody),
			success: function (resultStr) {
				
				if (resultStr) {
					var result = JSON.parse(resultStr);
					if (result && result.status == "success" && result.response) {
						var response = JSON.parse(result.response);
						if (response.ErrorCode == 1) {
							if (response.Postcodes && response.Postcodes.length > 0) {
								$("#tBodyAddress").empty();
								if (response.Postcodes.length == 1) {
									var clientAddress = response.Postcodes[0];
									if (clientAddress) {
										$("#resAddr1").val(clientAddress.BuildingNumber);
										$("#resAddr2").val(clientAddress.BuildingName);
										$("#resAddr3").val(clientAddress.StreetName);
										$("#resPostalCode").val(clientAddress.Postcode);
										if (clientAddress.BuildingNumber) {
											$("#resAddr1").removeClass("err-fld");
											$("#resAddr1Error").removeClass("show").addClass("hide");
										}
										if (clientAddress.StreetName) {
											$("#resAddr3").removeClass("err-fld");
											$("#resAddr3Error").removeClass("show").addClass("hide");
										}
										if (clientAddress.Postcode) {
											$("#resPostalCode").removeClass("err-fld");
											$("#resPostalCodeError").removeClass("show").addClass("hide");
										}
									}
								} else {
									var tableData = "";
									for (var addIndex = 0; addIndex < response.Postcodes.length; addIndex++) {
										var clientAddress = response.Postcodes[addIndex];
										tableData += "<tr>"
										if (clientAddress) {
											tableData += "<td class='tdBlkAddr'>" + clientAddress.BuildingNumber + "</td>";
											tableData += "<td class='tdBldngAddr'>" + clientAddress.BuildingName + "</td>";
											tableData += "<td class='tdStrtAddr'>" + clientAddress.StreetName + "</td>";
											tableData += "<td class='tdPostAddr'>" + clientAddress.Postcode + "</td>";
										}
										tableData += "</tr>";
									}
									$("#tBodyAddress").append(tableData);
									$("#addressModal").modal("show");
									$("#tBodyAddress tr").css("cursor", "pointer");
								}
							}
						} else {
							var errorText = 'Please Try again Later or else Contact your System Administrator';
							if (response.ErrorDetails) {
								errorText = response.ErrorDetails;
							}
							Swal.fire({
								icon: 'error',
								text: errorText,
							});
						}
					} else {
						Swal.fire({
							icon: 'error',
							text: 'Please Try again Later or else Contact your System Administrator',
						});
					}
				}
			},
			error: function (error) {
				Swal.fire({
					icon: 'error',
					text: 'Please Try again Later or else Contact your System Administrator',
				});
			}
		});
	} else {
		Swal.fire({
			icon: "error",
			html: "Please key in <b>PostalCode</b> alone or both <b>Address1(Building Number)</b> and <b>Address3(Street Name)</b>!"
		}).then((result) => {
			if (result.isConfirmed) {
				$("#resAddr1").addClass("err-fld");
				$("#resAddr3").addClass("err-fld");
				$("#resPostalCode").addClass("err-fld");
			}
		});
	}
});*/  //poovathi commented sg locate and myInfo button actions as per sabrina req doc end

$(".btnNricMdlClse").bind("click", function () {

	Swal.fire({
		//		  title: 'Are you Not Proceed?',
		text: "Do you want to clear NRIC value in client information screen?",
		icon: 'question',
		allowOutsideClick: false,
		allowEscapeKey: false,
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes, Clear it!'
	}).then((result) => {
		if (result.isConfirmed) {
			$("#nric").val('');
			swal.fire("Cleared!", "Your NRIC Value in Client Information Screen", "success");
			$('#srchClntForNricModal').modal('hide');
		} else {
			swal.fire("Select!", "Any on of the Row in above Table", "info");
			$('#srchClntForNricModal').modal('show');
		}
	});
});


//poovathi add on 17-11-21 to chk clientName Duplications.
//poovathi commented this function on 18-11-2021  as per asik sir req.

/*$('input[name=custName]').change(function(){
	  var custName = $(this).val();
	  var loggedAdvId = $("#hTxtFldLoggedAdvId").val() ;//$("#hTxtFldLoggedAdvId").val();
	  //alert("loggedAdvId"+ loggedAdvId)
	  var clientNameList = [];
	  if(!isEmpty(loggedAdvId)) {
		  $.ajax({
	        url:baseUrl+"/customerdetails/getAllClients/"+loggedAdvId,
	        type: "GET",
	        dataType: "json",async:false,
	        contentType: "application/json",
	        data : {"clientname":custName},
	        success: function (data) {
	        	for (var i=0; i<data.length; i++) {
	        		clientNameList.push(data[i].custName.trim());//Add Client Names in Autocomplete List Array
	            }
	        	
	        	if(clientNameList.includes(custName.trim())){
	        		toastr.clear($('.toast'));
					toastr.options = {timeOut: "5000", extendedTimeOut: "1000"};
					toastr["error"]("ClientName already Exists,Keyin Different ClientName");
					$("#custName").val("");
					$("#custName").focus();
	        	}
	        	
             },
		        error: function(xhr,textStatus, errorThrown){
		 			$('#cover-spin').hide(0);
		 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
				}
	        });
		   
	  }else {
			$('#cover-spin').hide(0);
		}
	  
});
*/