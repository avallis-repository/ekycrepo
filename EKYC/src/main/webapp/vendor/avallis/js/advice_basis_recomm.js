
var jsnDataFnaDetails = "";

$( document ).ready(function() {
	
    
    
    $('[data-toggle="popover1"]').popover({
 	   
        placement :'top',   
        html: true,
        content: function() {
         return $('#popover-contentPg10CardNotes1').html();
       }
     });
   
   $('[data-toggle="popover2"]').popover({
	   
	   placement :'top', 
       html: true,
       content: function() {
         return $('#popover-contentPg10CardNotes2').html();
       }
     });
   
   $('[data-toggle="popover3"]').popover({
       html: true,
       content: function() {
         return $('#popover-contentPg10CardNotes3').html();
       }
     });
   
    $('[data-toggle="popover4"]').popover({
       html: true,
       content: function() {
         return $('#popover-contentPg10CardNotes4').html();
       }
     }); 
    
    
    
    if(!isEmpty(jsnDataFnaDetails)  && !isJsonObjEmpty(jsnDataFnaDetails)){
		  for(var obj in jsnDataFnaDetails){
			  if (jsnDataFnaDetails.hasOwnProperty(obj)) {
				  var objValue = jsnDataFnaDetails[obj];
				  switch(obj){
				  
				  case "ccRepexistinvestflg":
		        	     if(objValue == "Y"){
		        	    	 $("#prodSwtchRecommMenu").removeClass("hide").addClass("show"); 
		        	    	 $("#prodSwtchRecommMenu").addClass("d-block").removeClass("d-none");
		        	    	 $("#prodRecommMenu").removeClass("show").addClass("hide");//prod recomm menu not appear 
						 }else if (objValue == "N"){
							 $("#prodRecommMenu").removeClass("hide").addClass("show");
							 $("#prodSwtchRecommMenu").removeClass("show").addClass("hide"); 
							 $("#prodSwtchRecommMenu").addClass("d-none").removeClass("d-block");
						  }
		    	 break;
				  default:
					  $("#"+obj).val(objValue);
				  }
				  
			  }
			  
		  }
	}
	var fnaId=$("#hTxtFldCurrFNAId").val();
	getAllSign(fnaId);

	setPageManagerSts();
	setPageAdminSts();
	setPageComplSts();
	//poovathi add on 25-08-2021
	showhidePrevArchKycDetls('divAdvBasicRecomm');
	
 });
	
 function saveCurrentPageData(obj,nextscreen,navigateflg,infoflag){
	 
 	var frmElements = $('#frmBasisOfRecomForm :input').serializeObject();
 	  
 	    
 		$.ajax({
 	            url:"fnadetails/register/advicerecomm/",
 	            type: "POST",
 	            async:false,
 	            dataType: "json",
 	            contentType: "application/json",
 	            data: JSON.stringify(frmElements),
 	            success: function (response) {
	if(navigateflg){
 	            	window.location.href=nextscreen;
}
 	            	
 	            },
 	           error: function(xhr,textStatus, errorThrown){
 		 		      ajaxCommonError(textStatus,xhr.status,xhr.statusText);
 				}
 	            });
 	  }
 
 
 //tab onchange hide if note content shows
   $("#Life-tab").bind("click",function(){
     	hideNoteMsg();
	});
   
    $("#UT-tab").bind("click",function(){
    	hideNoteMsg();
	});
 
 //hide note message fnunction start
    
 function hideNoteMsg(){
 	$("#Note,#Note1,#Note2,#Note3,#NoteDummy,#Note1Dummy,#Note2Dummy,#Note3Dummy").trigger("mouseout");
 }
 
 //set AutoFocus to Each TextArea while Trigger Modal PopUp 
 $('.modal').on('shown.bs.modal', function () {
	    //LIFE Tab
	    $('#advrecReason_pop').focus();
	    $("#advrecReason1_pop").focus();
	    $("#advrecReason2_pop").focus();
	    $("#advrecReason3_pop").focus();
	    //UT Tab
	    $("#advrecReasonDUMMY_pop").focus();
	    $("#advrecReasonDUMMY1_pop").focus();
	    $("#advrecReasonDUMMY2_pop").focus();
	    $("#advrecReasonDUMMY3_pop").focus();
	})//end autofocus function  


	//Life Tab 	validation start
 $(".viewRemarkContLIFE").bind("click",function(){
	 hideNoteMsg();
	 $("#advrecReason").val();
	 $("#advrecReason_pop").val($("#advrecReason").val());
	 
	 $("#advrecReason1").val();
	 $("#advrecReason1_pop").val($("#advrecReason1").val());
	 
	 $("#advrecReason2").val();
	 $("#advrecReason2_pop").val($("#advrecReason2").val());
	 
	 $("#advrecReason3").val();
	 $("#advrecReason3_pop").val($("#advrecReason3").val());
	 
 })
 
 
 $(".setValuetoTxtAreaLIFE").bind("click",function(){
	  $("#advrecReason_pop").val();
	  $("#advrecReason").val($("#advrecReason_pop").val());
	 
	  $("#advrecReason1_pop").val();
	  $("#advrecReason1").val($("#advrecReason1_pop").val());
	 
	  $("#advrecReason2_pop").val();
	  $("#advrecReason2").val($("#advrecReason2_pop").val());
	 
	  $("#advrecReason3_pop").val();
	  $("#advrecReason3").val($("#advrecReason3_pop").val());
	 
 })//end Life tab Validations
 
//UT Tab Validations 
  $(".viewRemarkContUT").bind("click",function(){
	 hideNoteMsg();
	 $("#advrecReasonDUMMY").val();
	 $("#advrecReasonDUMMY_pop").val($("#advrecReasonDUMMY").val());
	 
	 $("#advrecReason1DUMMY").val();
	 $("#advrecReasonDUMMY1_pop").val($("#advrecReason1DUMMY").val());
	 
	 $("#advrecReason2DUMMY").val();
	 $("#advrecReasonDUMMY2_pop").val($("#advrecReason2DUMMY").val());
	 
	 $("#advrecReason3DUMMY").val();
	 $("#advrecReasonDUMMY3_pop").val($("#advrecReason3DUMMY").val());
	 
 })

 
 
 $(".setValuetoTxtAreaUT").bind("click",function(){ 
	 $("#advrecReasonDUMMY_pop").val();
	 $("#advrecReasonDUMMY").val($("#advrecReasonDUMMY_pop").val());
	 
	  $("#advrecReasonDUMMY1_pop").val();
	  $("#advrecReason1DUMMY").val($("#advrecReasonDUMMY1_pop").val());
	 
	  $("#advrecReasonDUMMY2_pop").val();
	  $("#advrecReason2DUMMY").val($("#advrecReasonDUMMY2_pop").val());
	 
	  $("#advrecReasonDUMMY3_pop").val();
	  $("#advrecReason3DUMMY").val($("#advrecReasonDUMMY3_pop").val());
	 
 })//End UT tab Validations