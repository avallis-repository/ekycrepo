
/*Qr code implementation*/

var signatureDoneArr = [];
;

var selfData;
var spsData;
var advData;
var mgrData;

var timesRun = 0;
var timer;



function getQrCode(value,id,loaderId,delflag){
	
	//poovathi add on 25-06-2021 for hide loader,if signperson qr code not empty
	var extQrforSignPer =   $('#'+id).attr("src");
	if(isEmpty(extQrforSignPer)){
		 document.getElementById(loaderId).style.display = "block";	
	}else{
		 document.getElementById(loaderId).style.display = "none";
	}//end
	
   
    $.ajax({
    	 url : baseUrl+'/FnaSignature/getQrCodeById/'+value,
		 type: 'GET',async:false,
		 data : {"isDelete":delflag},
		 success: function (byteArray) {
	       $('#'+id).attr("src", 'data:image/png;base64,'+byteArray);
			// console.log("SUCCESS : ", document.getElementById("qrCode").src);
		  document.getElementById(loaderId).style.display = "none";
          // startTimer();
       },
       error: function(xhr,textStatus, errorThrown){
			//$('#cover-spin').hide(0);
		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
		}
   });
}

function startTimer(){
	
	if( (isEmpty(sessspsname) && signatureDoneArr.length == 2) || (!isEmpty(sessspsname) && signatureDoneArr.length == 3)){
		clearInterval(timer);
	}else{
		var isScantoSignOpen = $('#ScanToSignModal').is(':visible');
		if(isScantoSignOpen){
			// poovathi changed 8sec to 10 seconds
			timer = setInterval(getAllSignDatas, 10000);//10sec
		}else{
			clearInterval(timer);
		}
	}
}

function cancelScanToSign() {

	clearInterval(timer);
	
	if(approvalscreen == "true"){
		$("#suprevFollowReason").val("")
		$("#diManagerSection").find('input:radio').prop('checked',false);
		
		
		
		mgrClearDiagram('mgrSavedDiagram');
		
		mgrData=null;
		$("#spanMgrSignWaitMsg").html("")	;
		$("#btnMgrSignToggle").trigger("click");//temp
		
	}
	$('#ScanToSignModal').modal('hide');
}

function closeScanToSign(){
	clearInterval(timer);
	
	if(approvalscreen == "true"){
		
		$('#cover-spin').show(0);
          
		setTimeout(function() { mgrSave() ;}, 0);	
		
	}else{
		
		
		
		if($("#curPage").val() == "ADVISER") {
			$('#cover-spin').show(0);
			setTimeout(function() { advSave() ;}, 0);	
		}
		
		//open Adviser sweet Alert mes here instead of sendmail to manager function
		if( (isEmpty(sessspsname) && signatureDoneArr.length == 2) || (!isEmpty(sessspsname) && signatureDoneArr.length == 3)){
			
			var mgrkycSentStatus = $("#kycSentStatus").val();
			
			if(isEmpty(mgrkycSentStatus) && approvalscreen != "true"){
				
				setToolbarSignBtnSucc();
				setPageKeyInfoSucs()
				setPageSignInfoSucs();
				
				var fnaId=$("#hdnSessFnaId").val();
				var isLater = Cookies.get(fnaId);
				
				if(isLater != 'Y') {
					showManagerMailConfirm();
				}
			}
		}
	}
	
	$('#ScanToSignModal').modal("hide");
	
	

}



function showManagerMailConfirm(){
	
	
	Swal.fire({
	  title: 'Manager Approval',
	  width: 800,
	 allowOutsideClick:false,
		allowEscapeKey:false,
	  showClass: {
		    popup: 'animate__animated animate__fadeInDown'
		  },
		  hideClass: {
		    popup: 'animate__animated animate__fadeOutUp'
		  },
	  text: 'This FNA details can send to manager approval.',
             icon:"info",

	  showDenyButton: true,
	  showCancelButton: true,
	  cancelButtonText: "No, will Send Later",
	  confirmButtonText: "Yes,Send Now",
	  denyButtonText: "Upload Docs.+ Mail",
	  //showCloseButton: false,
	  
	     
	}).then((result) => {
		//console.log(result,"result")
	  if (result.isConfirmed) {
		  sendMailtoManger();
	    //Swal.fire('Mail send!', '', 'success')
	  } 

	if (result.isDenied) {
		// $('#uplodModal').modal("show");
		$("#btnAppUpload").trigger("click");
		$("#btnUplodAndEmail").removeClass('d-none');
		$("#btnWithoutUplodAndEmail").removeClass('d-none');
		$("#btnUplodCustAttchFile").addClass("d-none")
	   // Swal.fire('Changes are not saved', '', 'info')
	  }

if(result.isDismissed){
	//alert("isDismissed")
	
	//commented by poovathi on 07-06-2021
	//var fnaId=$("#hdnSessFnaId").val();
		
	// poovathi add on 07-06-2021
	  var fnaId=$("#hTxtFldCurrFNAId").val();
	  Cookies.set(fnaId, "Y");

}


	} )
	
}



function getAllSignDatas(){
	timesRun++;
	var isModalopen = $('#ScanToSignModal').is(':visible');
	//if(isModalopen){
		var fnaId=$("#hdnSessFnaId").val();
		
		
		    getAllSign(fnaId);
		   
		    genQrOrSign('CLIENT','Client1');
		
		    genQrOrSign('SPOUSE','Client2');	
		
			genQrOrSign('ADVISER','Adviser');
		
			genQrOrSign('MANAGER','Manager');
	//}
	
	
	
 		
}


/*Signpad*/
/* Self */
function setClientType(type){
	$("#perType").val(type);
	
}
var count=0;
function selfInit(diagramId,savedDiagram) {
	var valid=document.getElementById("selfLoad").value;
	//
     if(valid == "Y"){
	     if (window.goSamples) goSamples(); 
	 	 var $ = go.GraphObject.make;
	  selfDiagramObj = $(go.Diagram,diagramId); 
      selfDiagramObj.toolManager.mouseDownTools.insertAt(3, new GeometryReshapingTool());
      selfDiagramObj.nodeTemplateMap.add("FreehandDrawing",
        $(go.Part,
          { locationSpot: go.Spot.Center, isLayoutPositioned: false },
          new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
          {
            selectionAdorned: true, selectionObjectName: "SHAPE",
            selectionAdornmentTemplate:  // custom selection adornment: a blue rectangle
              $(go.Adornment, "Auto",
                $(go.Shape, { stroke: "dodgerblue", fill: null }),
                $(go.Placeholder, { margin: -1 }))
          },
          { resizable: true, resizeObjectName: "SHAPE" },
          { rotatable: true, rotateObjectName: "SHAPE" },
          { reshapable: true },  // GeometryReshapingTool assumes nonexistent Part.reshapeObjectName would be "SHAPE"
          $(go.Shape,
            { name: "SHAPE", fill: null, strokeWidth: 1.5 },
            new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
            new go.Binding("angle").makeTwoWay(),
            new go.Binding("geometryString", "geo").makeTwoWay(),
            new go.Binding("fill"),
            new go.Binding("stroke"),
            new go.Binding("strokeWidth"))
        ));
      // create drawing tool for myDiagram, defined in FreehandDrawingTool.js
      var tool = new FreehandDrawingTool();
      // provide the default JavaScript object for a new polygon in the model
      tool.archetypePartData =
        { stroke: "blue", strokeWidth: 3, category: "FreehandDrawing" };
      // allow the tool to start on top of an existing Part
      tool.isBackgroundOnly = false;
      // install as first mouse-move-tool
      selfDiagramObj.toolManager.mouseMoveTools.insertAt(0, tool);
      
      selfLoad(savedDiagram);  // 
      document.getElementById("selfLoad").value="N";
     }
    }
   function selfClearDiagram(savedDiagram){
	  
       clearDiagram(savedDiagram,selfDiagramObj);
    }
    function selfLoad(savedDiagram) {
      load(savedDiagram,selfDiagramObj);

    
    }
//Sps

function spsInit(diagramId,savedDiagram) {
	var valid=document.getElementById("spsLoad").value;
	
     if (window.goSamples) goSamples(); 
 	 var $ = go.GraphObject.make;
    if(valid == "Y"){
     spsDiagramObj = $(go.Diagram,diagramId);
      spsDiagramObj.toolManager.mouseDownTools.insertAt(3, new GeometryReshapingTool());
      spsDiagramObj.nodeTemplateMap.add("FreehandDrawing",
        $(go.Part,
          { locationSpot: go.Spot.Center, isLayoutPositioned: false },
          new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
          {
            selectionAdorned: true, selectionObjectName: "SHAPE",
            selectionAdornmentTemplate:  // custom selection adornment: a blue rectangle
              $(go.Adornment, "Auto",
                $(go.Shape, { stroke: "dodgerblue", fill: null }),
                $(go.Placeholder, { margin: -1 }))
          },
          { resizable: true, resizeObjectName: "SHAPE" },
          { rotatable: true, rotateObjectName: "SHAPE" },
          { reshapable: true },  // GeometryReshapingTool assumes nonexistent Part.reshapeObjectName would be "SHAPE"
          $(go.Shape,
            { name: "SHAPE", fill: null, strokeWidth: 1.5 },
            new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
            new go.Binding("angle").makeTwoWay(),
            new go.Binding("geometryString", "geo").makeTwoWay(),
            new go.Binding("fill"),
            new go.Binding("stroke"),
            new go.Binding("strokeWidth"))
        ));
      // create drawing tool for myDiagram, defined in FreehandDrawingTool.js
      var tool = new FreehandDrawingTool();
      // provide the default JavaScript object for a new polygon in the model
      tool.archetypePartData =
        { stroke: "blue", strokeWidth: 3, category: "FreehandDrawing" };
      // allow the tool to start on top of an existing Part
      tool.isBackgroundOnly = false;
      // install as first mouse-move-tool
      spsDiagramObj.toolManager.mouseMoveTools.insertAt(0, tool);
      spsLoad(savedDiagram);  // 
     document.getElementById("spsLoad").value="N";
    }
    }
   function spsClearDiagram(savedDiagram){
       clearDiagram(savedDiagram,spsDiagramObj);
    }
    function spsLoad(savedDiagram) {
      load(savedDiagram,spsDiagramObj);

    
    }
//End
//Advisor

function advInit(diagramId,savedDiagram) {
	var valid=document.getElementById("advLoad").value;
	if(valid == "Y"){
     if (window.goSamples) goSamples(); 
 	 var $ = go.GraphObject.make;
    
     advDiagramObj = $(go.Diagram,diagramId);
      advDiagramObj.toolManager.mouseDownTools.insertAt(3, new GeometryReshapingTool());
      advDiagramObj.nodeTemplateMap.add("FreehandDrawing",
        $(go.Part,
          { locationSpot: go.Spot.Center, isLayoutPositioned: false },
          new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
          {
            selectionAdorned: true, selectionObjectName: "SHAPE",
            selectionAdornmentTemplate:  // custom selection adornment: a blue rectangle
              $(go.Adornment, "Auto",
                $(go.Shape, { stroke: "dodgerblue", fill: null }),
                $(go.Placeholder, { margin: -1 }))
          },
          { resizable: true, resizeObjectName: "SHAPE" },
          { rotatable: true, rotateObjectName: "SHAPE" },
          { reshapable: true },  // GeometryReshapingTool assumes nonexistent Part.reshapeObjectName would be "SHAPE"
          $(go.Shape,
            { name: "SHAPE", fill: null, strokeWidth: 1.5 },
            new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
            new go.Binding("angle").makeTwoWay(),
            new go.Binding("geometryString", "geo").makeTwoWay(),
            new go.Binding("fill"),
            new go.Binding("stroke"),
            new go.Binding("strokeWidth"))
        ));
      // create drawing tool for myDiagram, defined in FreehandDrawingTool.js
      var tool = new FreehandDrawingTool();
      // provide the default JavaScript object for a new polygon in the model
      tool.archetypePartData =
        { stroke: "blue", strokeWidth: 3, category: "FreehandDrawing" };
      // allow the tool to start on top of an existing Part
      tool.isBackgroundOnly = true;
      // install as first mouse-move-tool
      advDiagramObj.toolManager.mouseMoveTools.insertAt(0, tool);
      advLoad(savedDiagram);  // 
      document.getElementById("advLoad").value="N";
    }
    }
   function advClearDiagram(savedDiagram){
       clearDiagram(savedDiagram,advDiagramObj);
    	
    }
  
    function advLoad(savedDiagram) {
      load(savedDiagram,advDiagramObj);
    }



function clearDiagram(savedDiagram,diagramObj){
	 var str1 = '{ "position": "-5 -5", "model": { "class": "GraphLinksModel", "nodeDataArray": [], "linkDataArray": []} }';
    	 document.getElementById(savedDiagram).value = str1;

    	 var str = document.getElementById(savedDiagram).value;
         try {
           var json = JSON.parse(str);

           diagramObj.initialPosition = go.Point.parse(json.position || "-5 -5");
           diagramObj.model = go.Model.fromJson(json.model);
           diagramObj.model.undoManager.isEnabled = true;
         } catch (ex) {
           alert(ex);
         }
}
function load(savedDiagram,diagramObj){
var str = document.getElementById(savedDiagram).value;
      try {
        var json = JSON.parse(str);
        diagramObj.initialPosition = go.Point.parse(json.position || "-5 -5");
        diagramObj.model = go.Model.fromJson(json.model);
        diagramObj.model.undoManager.isEnabled = true;
      } catch (ex) {
        alert(ex);
      }
}
//End
//Manager
var mgrDiagramObj=""
function mgrInit(diagramId,savedDiagram) {
	var valid=document.getElementById("mgrLoad").value;
	if(valid == "Y"){
 	 var $ = go.GraphObject.make;
     mgrDiagramObj = $(go.Diagram,diagramId);
      mgrDiagramObj.toolManager.mouseDownTools.insertAt(3, new GeometryReshapingTool());
      mgrDiagramObj.nodeTemplateMap.add("FreehandDrawing",
        $(go.Part,
          { locationSpot: go.Spot.Center, isLayoutPositioned: false },
          new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
          {
            selectionAdorned: true, selectionObjectName: "SHAPE",
            selectionAdornmentTemplate:  // custom selection adornment: a blue rectangle
              $(go.Adornment, "Auto",
                $(go.Shape, { stroke: "dodgerblue", fill: null }),
                $(go.Placeholder, { margin: -1 }))
          },
          { resizable: true, resizeObjectName: "SHAPE" },
          { rotatable: true, rotateObjectName: "SHAPE" },
          { reshapable: true },  // GeometryReshapingTool assumes nonexistent Part.reshapeObjectName would be "SHAPE"
          $(go.Shape,
            { name: "SHAPE", fill: null, strokeWidth: 1.5 },
            new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
            new go.Binding("angle").makeTwoWay(),
            new go.Binding("geometryString", "geo").makeTwoWay(),
            new go.Binding("fill"),
            new go.Binding("stroke"),
            new go.Binding("strokeWidth"))
        ));
      // create drawing tool for myDiagram, defined in FreehandDrawingTool.js
      var tool = new FreehandDrawingTool();
      // provide the default JavaScript object for a new polygon in the model
      tool.archetypePartData =
        { stroke: "blue", strokeWidth: 3, category: "FreehandDrawing" };
      // allow the tool to start on top of an existing Part
      tool.isBackgroundOnly = false;
      // install as first mouse-move-tool
      mgrDiagramObj.toolManager.mouseMoveTools.insertAt(0, tool);
      mgrLoad(savedDiagram);  // 
      document.getElementById("mgrLoad").value="N";
     }
    }
   function mgrClearDiagram(savedDiagram){
       
    	 clearDiagram(savedDiagram,mgrDiagramObj);
    }
    function mgrLoad(savedDiagram) {
     load(savedDiagram,mgrDiagramObj);

    
    }

  
    function mode(draw) {
      var tool = myDiagram.toolManager.findTool("FreehandDrawing");
      tool.isEnabled = draw;
    }
    function updateAllAdornments() {  // called after checkboxes change Diagram.allow...
      myDiagram.selection.each(function(p) { p.updateAdornments(); });
    }
    
    // save a model to and load a model from Json text, displayed below the Diagram
    function selfSave() {
    	makeBlob(selfDiagramObj);
 	}
    function spsSave() {
    	makeBlob(spsDiagramObj);
 	}
    function advSave() {
    	makeBlob(advDiagramObj);
 	}
    function mgrSave() {
    	
    		makeBlob(mgrDiagramObj);
 	}
  
    function makeBlob(selfDiagramObj) {
        var blob = selfDiagramObj.makeImageData({ background: "white", returnType: "blob", callback: sendSignData });
      }
    function sendSignData(blob) {
    	mgrSign=blob;
       	 var url = URL.createObjectURL(blob);

	    	
	    	var signByte;
	    	var request = new XMLHttpRequest();
	        request.open('GET', url, true);
	        request.responseType = 'blob';
	        request.onload = function() {
	            var reader = new FileReader();
	            reader.readAsDataURL(request.response);
	            reader.onload =  function(e){
	            	signByte=e.target.result;
	            	getImage(signByte);
	            	
	            };
	        };
	        request.send();
 }
   function checkSignedOrNot(esignaturestr) {

		var tempjson = JSON.parse(esignaturestr);
		var tempmpde =( tempjson.model );
		
   	if(tempmpde.nodeDataArray.length == 0) {
			return false;
	}
   	return true;
   }
   function getImage(signByte){
	   var perType=$('#perType').val();
       var esignaturestr;
       var esignId;
	    if(perType == "CLIENT"){
	     	esignaturestr = '{ "position": "' + go.Point.stringify(selfDiagramObj.position) + '",\n  "model": ' + selfDiagramObj.model.toJson() + ' }';
			document.getElementById("selfSavedDiagram").value = esignaturestr;
			esignId=document.getElementById('selfSignId').value;
	      //  alert(esignaturestr);
         }else if(perType == "SPOUSE"){
	     	esignaturestr = '{ "position": "' + go.Point.stringify(spsDiagramObj.position) + '",\n  "model": ' + spsDiagramObj.model.toJson() + ' }';
			document.getElementById("spsSavedDiagram").value = esignaturestr;
			esignId=document.getElementById('spsSignId').value;
	       // alert(esignaturestr);
         }
         else if(perType == "ADVISER"){
	     	esignaturestr = '{ "position": "' + go.Point.stringify(advDiagramObj.position) + '",\n  "model": ' + advDiagramObj.model.toJson() + ' }';
			document.getElementById("advSavedDiagram").value = esignaturestr;
			esignId=document.getElementById('advSignId').value;
			
			
			if(!checkSignedOrNot(esignaturestr)) {
				toastr.clear($('.toast'));
				toastr.options = {  timeOut: "5000", extendedTimeOut: "1000"};
				toastr["error"]("Signature cannot be empty!");
				$('#cover-spin').hide(0);
				 $("#openMngesecModal").modal("hide");
				 return;
			}
			
	       
         }else if(perType == "MANAGER"){
        	
	     	esignaturestr = '{ "position": "' + go.Point.stringify(mgrDiagramObj.position) + '",\n  "model": ' + mgrDiagramObj.model.toJson() + ' }';
			document.getElementById("mgrSavedDiagram").value = esignaturestr;
			esignId=document.getElementById('mgrSignId').value;
			
			if(!checkSignedOrNot(esignaturestr)) {
				toastr.clear($('.toast'));
				toastr.options = {  timeOut: "5000", extendedTimeOut: "1000"};
				toastr["error"]("Signature cannot be empty!");
				$('#cover-spin').hide(0);
				$("#suprevMgrFlgA").prop("checked",false);$("#suprevMgrFlgD").prop("checked",false);
				 $("#openMngesecModal").modal("hide");
				 return;
			}
			
			
			
         }
	    
	     var signImgString=signByte.split(",")[1];
        
	     
	     var fnaId=$("#hTxtFldFnaId").val();
	     
	  //   alert("fnaId --->>>>>"+fnaId)

	    
	    
	     saveSignature(fnaId,esignaturestr,signImgString,perType,esignId);
   }  

  
    
   function saveSignature(fnaId,esignaturestr,signImgString,perType,esignId){
	var curPage=$("#curPage").val();
	 $.ajax({
	    	  url: baseUrl+"/FnaSignature/saveFnaSignature",
	    	  type: "POST",
	    	  async:false,
	    	  data:{fnaId:fnaId,
	    		  	esign:esignaturestr,
	    		  	signDoc:signImgString,
	    		  	perType:perType,
                  	esignId:esignId,
                  	curPage:curPage
	    	  },
	    	
	    		  
	    	  success : function(data){
		        
                 //After isertion set esign id -Avoid duplicate entries
                 setSignPersonData(data);
                 
                 $('#cover-spin').hide(0);
                 
                 getAllSign(fnaId);
                 
                	 if(approvalscreen == "true") {
                    	 setTimeout(function() {  $("#btnApprvMngr").trigger("click"); }, 1000);
                     }else {
//                    	 alert("afer sign")
                     }
                 
                 
                 


	 			
	
				 //alert("success");
	    	  },
	    	  error : function(data){
	    		//  alert("Error in saving Signature");
	    		  $('#cover-spin').hide(0);
	    		  $("#openMngesecModal").modal("hide");
	    		  
	    			toastr.clear($('.toast'));
					toastr.options = {  timeOut: "5000", extendedTimeOut: "1000"};
					toastr["error"]("Error in saving Signature, Try again!");
	    		 
	    	  }});
} 


/*//Get all data by fnaId
function editSignData(){
	var fnaId=$("#hdnSessFnaId").val();
	getAllSign(fnaId);
}*/



//Set signature data
 function getAllSign(fnaId){
	signatureDoneArr = [],signatureDoneArr.length=0;
	//vignesh added on 31 05 2021
	clrMgrSignData();

	
	//console.log("approvalscreen",approvalscreen)
			if(approvalscreen == "true"){
				fnaId=fnaId;
			}else {
				fnaId = $("#hTxtFldCurrFNAId").val()		
			}
		
 	   $.ajax({
 		    url: baseUrl+'/FnaSignature/getAllDataById/'+fnaId,
 		    type: 'GET',
 		    async:false,
 		    success: function(data) {
	          	//var obj=JSON.stringify(data);
	          	for(var i=0;i<data.length;i++){
		
					if (signatureDoneArr.includes(data[i].signPerson) === false) signatureDoneArr.push(data[i].signPerson);

					if(data[i].signPerson == "CLIENT"){						
						$("#signmodalclient1qrsign").removeClass("fa-exclamation-triangle").addClass("fa-check-circle");
					}
					if(data[i].signPerson == "SPOUSE"){						
						$("#signmodalclient2qrsign").removeClass("fa-exclamation-triangle").addClass("fa-check-circle");
					}
					if(data[i].signPerson == "ADVISER"){						
						$("#signmodaladviserqrsign").removeClass("fa-exclamation-triangle").addClass("fa-check-circle");
					}
					if(data[i].signPerson == "MANAGER"){						
						$("#signmodalmanagerqrsign").removeClass("fa-exclamation-triangle").addClass("fa-check-circle");
					}
				}
	          
	          	
//	          	alert(data.length)
	          	if(data.length > 0){
 		    		setAllSign(data);
 		    	}


				if( (isEmpty(sessspsname) && signatureDoneArr.length >= 2) || (!isEmpty(sessspsname) && signatureDoneArr.length >= 3)){
					
					clearInterval(timer);
					
					setPageKeyInfoSucs();
					setPageSignInfoSucs();					
					setToolbarSignBtnSucc();
					
				}else{
					
					if(isEmpty(strPageValidMsgFld)){
						setPageKeyInfoFail();
					}
					setPageSignInfoFail();					
					setToolbarSignBtnFail();
					
					
						
				}
 		    	
 		    },
 		   error: function(xhr,textStatus, errorThrown){
	 			//$('#cover-spin').hide(0);
	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			}
 		});
 		
    }
    
function clrMgrSignData(){
	
	
	$(".client1QrSec").removeClass("show").addClass("hide")
	$(".client1QrSecNoData").removeClass("hide").addClass("show")
	
	$(".client2QrSec").removeClass("show").addClass("hide")
	$(".client2QrSecNoData").removeClass("hide").addClass("show")
	
	$(".advQrSec").removeClass("show").addClass("hide")
	$(".advQrSecNoData").removeClass("hide").addClass("show")
	
	$(".mgrQrSec").removeClass("show").addClass("hide")
	$(".mgrQrSecNoData").removeClass("hide").addClass("show")
	
	$("#selfSignDate").text("")
	$("#spsSignDate").text("")
	$("#advSignDate").text("")
	$("#mgrSignDate").text("")
	
	//remove disable calss from manager sec if sign not exist poovathi add on 01-06-2021
	$("#mgrDiagram").removeClass("disabledsec");
	//selfData = null,spsdata= null,advData = null,
	mgrData = null;
	
}


function setAllSign(data){
	
//	var mailtrigger=0;
	
//	fnaSignData=data;
	
	
	for(var i=0;i<data.length;i++){
		//console.log('---->'+data[i])
		setSignPersonData(data[i]);
		
	}
	
	
}



function setSignPersonData(signData){
	
	var signPerson=signData.signPerson;
	console.log("signPerson--->"+signPerson)
	if(signPerson == "CLIENT"){
		     selfData=signData;
			setViewSign(signData,"client1Qr","selfSignDate");
		}if(signPerson == "SPOUSE"){
			spsData=signData;
			setViewSign(signData,"client2Qr","spsSignDate")
		}if(signPerson == "ADVISER"){
			advData=signData;
			setViewSign(signData,"advQr","advSignDate");
		}if(signPerson == "MANAGER"){
			
			mgrData=signData;
			setViewSign(signData,"mgrQr","mgrSignDate");
			var supreFlg=$('input[name="suprevMgrFlg"]:checked').val();
			//alert("supreFlg"+supreFlg)
			if(supreFlg!=undefined){
			}
		}
}
function setViewSign(signData,roleQr,signDateId){
	
	var byteArray=signData.signDocBlob;
	
	if((byteArray != "")&&(byteArray != null)){
 	
		$("."+roleQr+"SecNoData").removeClass("show").addClass("hide")
		$("."+roleQr+"Sec").removeClass("hide").addClass("show")
		$("."+roleQr).attr('src', 'data:image/png;base64,'+byteArray);
		
	}else{
	
		$("."+roleQr+"Sec").removeClass("show").addClass("hide")
		$("."+roleQr+"NoData").removeClass("hide").addClass("show")
		//$("."+roleQr).attr('src', 'vendor/avallis/img/contract.png');
		
	}

	$("#"+signDateId).text((signData.signDate));   	
 	 
 	if( (isEmpty(sessspsname) && signatureDoneArr.length == 2) || (!isEmpty(sessspsname) && signatureDoneArr.length == 3)){
		//$("#mailModal").modal("show");
		/*if(approvalscreen != "true"){
			$('#ScanToSignModal').modal('hide')	
		}*/
			

	setTimeout(function(){
		var mgrkycSentStatus = $("#kycSentStatus").val()
				if(isEmpty(mgrkycSentStatus)  && approvalscreen != "true"){
					
//					if(approvalscreen != "true"){
						$('#ScanToSignModal').modal('hide')	
//					}
						
					//commented by poovathi on 07-06-2021
					//var fnaId=$("#hdnSessFnaId").val();
						
					// poovathi add on 07-06-2021
					  var fnaId=$("#hTxtFldCurrFNAId").val();
					  var isLater = Cookies.get(fnaId);
					
					if(isLater != 'Y') {          //command by vignesh on 28-05-2021
						//uncomment below function by poovthi on 07-06-21
						showManagerMailConfirm() 
					}
				}	
		
	},2200);
	

	}

}
function selfSignData(){
	if(selfData != undefined){
		setSignData(selfData,'selfSignId',selfDiagramObj,'selfSavedDiagram');
	}
	
}
function advSignData(){
	if(advData != undefined){
	setSignData(advData,'advSignId',advDiagramObj,'advSavedDiagram');
	}
	
}
function mgrSignData(){
	    if(mgrData!=undefined){
			setSignData(mgrData,'mgrSignId',mgrDiagramObj,'mgrSavedDiagram');
		}
		
}
function spsSignData(){
	    if(spsData!=undefined){
			setSignData(spsData,'spsSignId',spsDiagramObj,'spsSavedDiagram');
		}
	
}
function setSignData(signData,signId,diagramObj,savedDiagram){
	if((signData !=null)){
		document.getElementById(signId).value=signData.esignId;
		try {
			   
	        var json =JSON.parse(signData.eSign);
	        diagramObj.initialPosition = go.Point.parse(json.position || "-5 -5");
	        diagramObj.model = go.Model.fromJson(json.model);
	        diagramObj.model.undoManager.isEnabled = true;
	        document.getElementById(savedDiagram).value=signData.eSign;
	    } catch (ex) {
	        alert(ex);
	      }
	}
	
}

function genQrOrSign(personType,id){
	
	var clientSignExist = false,sposeSignExist=false;
//	console.log("inside genQrOrSign  return :"+signatureDoneArr)
		if (signatureDoneArr.includes("CLIENT") === true) {
			clientSignExist =true;
		}
		
		if(isEmpty(sessspsname)){
			sposeSignExist =true;
		}else{
			if(signatureDoneArr.includes("SPOUSE") === true){
				sposeSignExist =true;
			}
		}
		
	
	if(personType == "CLIENT"){
		if(selfData != undefined){
			//alert(selfData)
			hideQrCode(id);
			selfInit('selfDiagram','selfSavedDiagram');setClientType('CLIENT');
			setSignData(selfData,'selfSignId',selfDiagramObj,'selfSavedDiagram');
			$("#spanClientSignWaitMsg").html('<i class="fa fa-check-circle" style="font-size:24px"></i>&nbsp;Client(1) signature captured successfully!');
			 $("#selfDiagram").addClass("disabledsec");
		}else{
			//alert(selfData)
			hideSignPad(id);
		    getQrCode('CLIENT','qrCodeClient1','signLoaderClient1',false);
			selfData=null
			$("#spanClientSignWaitMsg").html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>&nbsp;<span class="">Please wait to load client(1) signature!</sapn>');
		}
	}else if(personType == "SPOUSE"){
		if(spsData != undefined){
			hideQrCode(id);
			spsInit('spsDiagram','spsSavedDiagram');setClientType('SPOUSE');
			setSignData(spsData,'spsSignId',spsDiagramObj,'spsSavedDiagram');
			$("#spanSpsSignWaitMsg").html('<i class="fa fa-check-circle" style="font-size:24px"></i>&nbsp;<span class="">Client(2) signature captured successfully!</span>');
			$("#spsDiagram").addClass("disabledsec");
		}else{
			hideSignPad(id);
		    getQrCode('SPOUSE','qrCodeClient2','signLoaderClient2',false);
			spsData=null
			$("#spanSpsSignWaitMsg").html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>&nbsp;<span class="">Please wait to load client(2) signature!</span>');
			
		}
	}else if(personType == "ADVISER"){
		
		if(clientSignExist && sposeSignExist){
			
			$("#btnAdvSignToggle").show();
			
			if(advData != undefined){
				hideQrCode(id);
				advInit('advDiagram','advSavedDiagram');setClientType('ADVISER')
				setSignData(advData,'advSignId',advDiagramObj,'advSavedDiagram');
				$("#spanAdvSignWaitMsg").html('<i class="fa fa-check-circle" style="font-size:24px"></i>&nbsp;<span class="">Adviser signature captured successfully!</span>');
				$("#advDiagram").addClass("disabledsec");
			}else{
				
			//	if( $("#hTxtFldCurrAdvSignMode").val() =="qr") {
					hideSignPad(id);
				    getQrCode('ADVISER','qrCodeAdviser','signLoaderAdviser',false);
					advData=null;
					$("#spanAdvSignWaitMsg").html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>&nbsp;<span class="">Please wait to load Adviser signature!</span>');
				//}
						
			}
			
		}else{
			hideQrCode(id);
			hideSignPad(id);
			advData=null;
			$("#spanAdvSignWaitMsg").html('<i class="fa fa-remove" style="font-size:24px"></i>&nbsp;<span class="">Client(1)/Client(2) signature yet to capture!</span>');
			$("#btnAdvSignToggle").hide()
		}
		
		
	}else if(personType == "MANAGER"){
		
		$("#btnMgrSignToggle").trigger("click");
		
		if(mgrData != undefined){
			
			hideQrCode(id);
			mgrInit('mgrDiagram','mgrSavedDiagram');
			setClientType('MANAGER');
			mgrSignData()
			setSignData(mgrData,'mgrSignId',mgrDiagramObj,'mgrSavedDiagram');
			$("#spanAdvSignWaitMsg").html('<i class="fa fa-check-circle" style="font-size:24px"></i>&nbsp;<span class="">Manager signature captured successfully!</span>');
			$("#mgrDiagram").addClass("disabledsec");
		}else{
		    /*hideSignPad(id);
		    getQrCode('MANAGER','qrCodeManager','signLoaderManager',false);
			mgrData=null;
			$("#spanAdvSignWaitMsg").html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>&nbsp;<span class="">Please to load Manager signature!</span>');
			$("#mgrDiagram").removeClass("disabledsec");*/
		}
	}
}
function hideQrCode(id){
	document.getElementById("signSec"+id).style.display="block";
	document.getElementById("qrCodeSec"+id).style.display="none";
}
function hideSignPad(id){
	document.getElementById("signSec"+id).style.display="none";
	document.getElementById("qrCodeSec"+id).style.display="block";
}
function generateQrCode(perType,id){
	
	
	Swal.fire({
	    	  title: "Clear Signature",
	    	  text: "Are you sure to clear the signature?",
	    	  type: "warning",
	    	  showCancelButton: true,
	    	  allowOutsideClick:false,
	    	  allowEscapeKey:false,
	    	  confirmButtonClass: "btn-danger",
	    	  confirmButtonText: "Yes, Clear it!",
	    	  cancelButtonText: "No,Cancel",
	    	  closeOnConfirm: false,
	    	  //showLoaderOnConfirm: true
	    	  closeOnCancel: false
	    	}).then((result) => {
			  if (result.isConfirmed) {
				 if(perType == "CLIENT"){ 
							selfData=null;	
							$("#spanClientSignWaitMsg").html("")	
						}
						
						if(perType == "SPOUSE"){ 
							spsData=null;
							$("#spanSpsSignWaitMsg").html("")	
						}
						
						if(perType == "ADVISER"){ 
							advData=null;
							$("#spanAdvSignWaitMsg").html("")	
						}
						
						if(perType == "MANAGER"){
							
							mgrClearDiagram('mgrSavedDiagram');
							
							mgrData=null;
							$("#spanMgrSignWaitMsg").html("")	;
							$("#btnMgrSignToggle").trigger("click");//temp
							

							
						}
						
						swal.close();
						if(perType != "MANAGER") {//temp
						
				    		hideSignPad(id);
							getQrCode(perType,'qrCode'+id,'signLoader'+id,true);
						}
			  } 

	
	});
	
	
	
	    
}
//End qr code

function toggleSignQrCode(obj,id) {
	
	$("#signSec"+id).show( 'slow');
	$("#qrCodeSec"+id).hide( 'slow');
	
	$(obj).hide('slow')
	
	   $(obj).text(function(i, text){
	          return text === "QR - Scan to Sign" ? "e-Sign Pad" : "QR - Scan to Sign";
	      });
	   
	   

	mgrInit('mgrDiagram','mgrSavedDiagram');
	setClientType('MANAGER');
	mgrSignData()
	$("#curPage").val("MANAGER");
	setSignData(mgrData,'mgrSignId',mgrDiagramObj,'mgrSavedDiagram');
	
//	$("#signManagerQr").trigger("click");
}



function toggleSignQrCodeAdv(obj,id) {
	
	$("#signSec"+id).toggle( 'slow');
	$("#qrCodeSec"+id).toggle( 'slow');
	
	
//	clearInterval(timer);

	   $(obj).text(function(i, text){
		   
		   if(text === "QR - Scan to Sign") {
			   $("#hTxtFldCurrAdvSignMode").val("sign")
		   }
		   
	          return text === "QR - Scan to Sign" ?	  	  "e-Sign Pad"   	: "QR - Scan to Sign";
	      })

	   advInit('advDiagram','advSavedDiagram');
	setClientType('ADVISER');
	advSignData()
	$("#curPage").val("ADVISER");
	setSignData(advData,'advSignId',advDiagramObj,'advSavedDiagram');
	
//	$("#signManagerQr").trigger("click");
}
