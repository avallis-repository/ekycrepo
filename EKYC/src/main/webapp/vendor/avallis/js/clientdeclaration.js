

var jsnDataFnaDetails = "";

$(document).ready(function() {
	
	if(!isEmpty(jsnDataFnaDetails)  && !isJsonObjEmpty(jsnDataFnaDetails)){
		  for(var obj in jsnDataFnaDetails){
			  if (jsnDataFnaDetails.hasOwnProperty(obj)) {
				  var objValue = jsnDataFnaDetails[obj];
				  switch(obj){
				  case "ccRepexistinvestflg":
		        	     if(objValue == "Y"){
		        	    	 $("#prodSwtchRecommMenu").removeClass("hide").addClass("show"); 
		        	    	 $("#prodSwtchRecommMenu").addClass("d-block").removeClass("d-none");
		        	    	 $("#prodRecommMenu").removeClass("show").addClass("hide");//prod recomm menu not appear 
						 }else if (objValue == "N"){
							 $("#prodRecommMenu").removeClass("hide").addClass("show");
							 $("#prodSwtchRecommMenu").removeClass("show").addClass("hide"); 
							 $("#prodSwtchRecommMenu").addClass("d-none").removeClass("d-block");
						  }
		    	 break;
				  case "cdackAgree":
					  if(!isEmpty(objValue) && objValue.toLowerCase() == "agree" ){ $("#radFldackAgree").prop("checked",true);} 
					  else if(!isEmpty(objValue) && objValue.toLowerCase() == "partial" ){ $("#radFldackPartial").prop("checked",true);} 
					  else {$("#radFldackAgree").prop("checked",false);$("#radFldackPartial").prop("checked",false);}
					  break;
					  
				  case "cdConsentApprch":
					  if(objValue == "F2F" ){ $("#cdConsentApprchF").prop("checked",true);} 
					  else if(objValue == "NONF2F" ){ $("#cdConsentApprchNF").prop("checked",true);} 
					  else {$("#cdConsentApprchF").prop("checked",false);$("#cdConsentApprchNF").prop("checked",false);}
					  break;
					  
				  case "cdMrktmatPostalflg":
					  if(objValue == "Y"){$("#cdMrktmatPostalflg").prop("checked",true);}
					  else{$("#cdMrktmatPostalflg").prop("checked",false);}
					  
				  case"cdMrktmatEmailflg":
					  if(objValue == "Y"){$("#cdMrktmatEmailflg").prop("checked",true);}
					  else{$("#cdMrktmatEmailflg").prop("checked",false);}
					  
				  
				  default:
					  $("#"+obj).val(objValue);
				  }
				  
			  }
			  
		  }
	}
	
	var fnaId=$("#hTxtFldCurrFNAId").val();
	getAllSign(fnaId);

	setPageManagerSts();
	setPageAdminSts();
	setPageComplSts();
	
	//poovathi add on 25-08-2021
	showhidePrevArchKycDetls('divClntDeclaration');
	
	
});

function saveCurrentPageData(obj,nextscreen,navigateflg,infoflag){
	
	
 	var frmElements = $('#frmClientDeclForm :input').serializeObject();
 	  
 	    
 		$.ajax({
 	            url:"fnadetails/register/clientdecl/",
 	            type: "POST",
 	            async:false,
 	            dataType: "json",
 	            contentType: "application/json",
 	            data: JSON.stringify(frmElements),
 	            success: function (response) {
	if(navigateflg){
 	            	window.location.href=nextscreen;
}
 	            	
 	            },
 	           error: function(xhr,textStatus, errorThrown){
 		 			//$('#cover-spin').hide(0);
 		 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
 				}
 	            });
 	  }