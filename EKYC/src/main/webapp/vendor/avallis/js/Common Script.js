
var LOG_USER_MGRACS="",LOG_USER_ADVID=""


var changedarrGlobal = [],changedarrGlobal1 = [], changedarrGlobal2 = [], changedarrGlobal3 = [];
var changedarrGlobal4 = [],changedarrGlobal5 = [],changedarrGlobal6 = [];
var changedarrGlobal7 = [],changedarrGlobal8 = [],changedarrGlobal9 = [];
var changedarrGlobal10 = [],changedarrGlobal11 = [],changedarrGlobal12 = [];
var changedarrGlobal13 = [],changedarrGlobal14 = [],changedarrGlobal15 = [];

var cdBenfTppPepObj = {

		BENF : [ 'txtFldCDBenfName', 'txtFldCDBenfNric', 'txtFldCDBenfIncNo','txtFldCDBenfAddr', 'txtFldCDBenfJob', 'txtFldCDBenfJobCont','txtFldCDBenfRel','txtFldCDBenfId'],
		TPP : [ 'txtFldCDTppName', 'txtFldCDTppNric', 'txtFldCDTppIncNo','txtFldCDTppAddr', 'txtFldCDTppJob', 'txtFldCDTppJobCont',	'txtFldCDTppRel','txtFldCDTppId'],
		PEP : [ 'txtFldCDPepName', 'txtFldCDPepNric', 'txtFldCDPepIncNo','txtFldCDPepAddr', 'txtFldCDPepJob', 'txtFldCDPepJobCont',	'txtFldCDPepRel','txtFldCDPepId'],
		OTH : [ 'txtFldCDTppCcontact' ]//'txtFldCDTppChqNo',//-> MAR_2018,, 'txtFldCDTppBankName', ;dec2019
	};

function isEmptyFld(value){
	if(value == " " || value == null || value.length == 0 || value == undefined)
		return true;
	else
		return false;
}


function isJsonObjEmpty(obj){
	
	
	if(Object.keys(obj).length === 0 && obj.constructor === Object)
		return true;
	else
		return false;

	
}

function isEmpty(str){
	if(str == null || str.length == 0 || str===undefined)return true;
	else return false;
}

//income,sumAssured and Premium Field Number validation it allows Numbers and Decimal Points

$('.numberFldClass').keyup(function(){
    var val = $(this).val();
    if(isNaN(val)){
         val = val.replace(/[^0-9\.]/g,'');
         if(val.split('.').length>2) 
             val =val.replace(/\.+$/,"");
    }
    $(this).val(val); 
});


//Poovathi Add Common Email Id Validation on 10-11-2020

function validateEmail(obj,Elem) {
	var email = $(obj).val();
	if(isEmptyFld(email)){
		$('#'+Elem+"Error").removeClass('show').addClass('hide');
		 return;
	}else{
		$('#'+Elem+"FldError").removeClass('show').addClass('hide');
		var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		  if(!regex.test(email)) {
			  $('#'+Elem+"Error").removeClass('hide').addClass('show');
			  $('#'+Elem+"Error").html("Invalid Email Id Format");
			  $('#'+Elem).focus();
		    return false;
		  }else{
			$('#'+Elem+"Error").removeClass('show').addClass('hide');
		    return true;
		   }
	}
	  
	}
//end of Email validation

function makeid(length) {
	   var result           = '';
	   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	   var charactersLength = characters.length;
	   for ( var i = 0; i < length; i++ ) {
	      result += characters.charAt(Math.floor(Math.random() * charactersLength));
	   }
	   return ("PR_"+result).toUpperCase();
	}


function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};





//old KYC Reuse Common functions below

function chkJsnOptions(chkbox,jsnFld){
    var advAppTypeOpt = $("#"+jsnFld+"");
	var jsonvalues = JSON.parse(isEmpty(advAppTypeOpt.val()) ? "{}" : advAppTypeOpt.val());
	var chkd = chkbox.checked;
	var val = $(chkbox).attr("data");

	var newobj=jsonvalues;
	newobj[val]=(chkd == true ? "Y" :"N");
	advAppTypeOpt.val(JSON.stringify(newobj));
}

function fnaAssgnFlgCommon(obj, id) {
	if (id == "chk") {
		if (obj.checked) {
			obj.value = "Y";
		} else if (!obj.checked) {
		    obj.value = "";
		}
		
	} else {
		if (obj.checked) {
			document.getElementById(id).value = obj.value;
		}
	}
}

function PasteOnlyNum(obj,pre){
	setTimeout(function(){
//		console.log(obj.value);
		
		if(pre==0){
		 var regNoPre = /^\d+$/;
		 if(!regNoPre.test(obj.value.trim())){
				obj.value="";
				alert("Numbers only allowed.")
	     }
		}else{
		 var reg =new RegExp("^[0-9]+(\.[0-9]{1,"+pre+"})?$");
		 if(!reg.test(obj.value.trim())){
			obj.value="";
			alert("Numbers only allowed.")
		 } 
		
		}
		
		obj.value=obj.value.trim();
		
	},400);
}

function textCounter( field,maxlimit ) {

	var len=field.value.length;
	  if ( len > maxlimit )
	  {
	    field.value = field.value.substring( 0, maxlimit );
	    donothing();
	    return false;
	  }else if(len <= maxlimit){
		 $(field).next().addClass("show").removeClass("hide")
		 $(field).next().html(maxlimit-len+" characters are left from "+'<span class="text-success">'+maxlimit+'</span>' +" Characters" );
	  }
	  
	  
	  // trigger note content while keyup text area Field Start
	  var thisFldId =  $(field).attr("id");
	      enableNoteContent(thisFldId);
	  //end of note content display trigger
	    
	 }//end textCounter


function donothing(){
       document.returnValue=true;
}//end donothing


function chkSelfSpsOthrValid(chkbox,Ele){
	
	var chkd = chkbox.checked;
	if(chkd){
		$("#"+Ele+"").removeClass("readOnlyText").addClass("writeModeTextbox")
		.attr('readOnly',false).prop('disabled',false).attr('disabled',false);
	}else{
		$("#"+Ele+"").removeClass("writeModeTextbox").addClass("readOnlyText")
		.attr('readOnly',true).val("");
	}
}


function resetSelect2(select2id){
	$("#"+select2id).empty().trigger('change');
}



function enableMgr(){
if(document.getElementById("signManager"))document.getElementById("signManager").style.display="block";
	if(document.getElementById("signManagerQr"))document.getElementById("signManagerQr").style.display="block";
}

function getCurrFNASigns(fnaId) {
	var signArr = [];
	var retFlag = false;
	 $.ajax({
 		    url: baseUrl+'/FnaSignature/getAllDataById/'+fnaId,
 		    type: 'GET',
 		    async:false,
 		    success: function(data) {
	          	//var obj=JSON.stringify(data);
 		    	//alert(data)
	          	for(var i=0;i<data.length;i++){
		
					if (signArr.includes(data[i].signPerson) === false) signArr.push(data[i].signPerson);
					
				} 


				if( (isEmpty(sessspsname) && signArr.length >= 2) || (!isEmpty(sessspsname) && signArr.length >= 3)){
					retFlag =  true;
				}else{
					retFlag =  false;
				}
 		    	
 		    },
 		   error: function(xhr,textStatus, errorThrown){
	 			//$('#cover-spin').hide(0);
	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			}
 		});
	 
	 return retFlag;
}


function getSignDataStatus(fnaId) {
	var signArr = [];
	var retFlag = false;
	 $.ajax({
 		    url: baseUrl+'/FnaSignature/setSignData/'+fnaId,
 		    type: 'GET',
 		    async:false,
 		    success: function(data) {
	          	//var obj=JSON.stringify(data);
	          	for(var i=0;i<data.length;i++){
		
					if (signArr.includes(data[i].signPerson) === false) signArr.push(data[i].signPerson);
					
				} 


				if( (isEmpty(sessspsname) && signArr.length >= 2) || (!isEmpty(sessspsname) && signArr.length >= 3)){
					retFlag =  true;
				}else{
					retFlag =  false;
				}
 		    	
 		    },
 		   error: function(xhr,textStatus, errorThrown){
	 			//$('#cover-spin').hide(0);
	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			}
 		});
	 
	 return retFlag;
}

function chkAgreeDisagreeFld(radBtn,txtFldId){
	
	var fnaid = $("#txtFldFnaId").val();

	if($(radBtn).is(":checked")){
		var thisFldId = $(radBtn).attr("id");
		
	
		if((thisFldId =='radFldackAgree')||(thisFldId =='suprevMgrFlgA')){
			
			if(thisFldId =='suprevMgrFlgA'){
				
				
				$(radBtn).val("Y")
				$("#mngrDisagrReason").html("");
				$("#mngrBtnErrorMsg").html("");
				
				$("#errorFld").css("border","");
				
				$("#ScanToSignModal").modal({
				    backdrop: 'static',
				    keyboard: false,
				    show: true
				});
				
				
				$("#signManagerQr").trigger("click");
				
				$('#ScanToSignModal').on('shown.bs.modal', function () {
					var curntFnaId = '';
					//set Scanto Sign Self and Sps Names
					var fnaId = $("#txtFldCurFnaId").val();
					
					if(isEmpty(fnaId)){
						curntFnaId = $("#hTxtFldFnaId").val();
					}else{
						curntFnaId = fnaId;
					}
					
			    	 getSignPageData(curntFnaId);
			    	
			        // getLatestFnaIdDetls(curntFnaId);
			         getMngrDetlsbyFnaId(curntFnaId)
			         getAllSign(curntFnaId);
			    	
					if(approvalscreen == "true"){
		          		$("#btnMgrSignToggle").trigger("click");
		          		
		          	}
				})
				
				  
				$(".genqrbtn").hide();
				
			}
			//$("#"+txtFldId).val('');
			
		}
		
		if((thisFldId =='radFldackPartial')||(thisFldId =='suprevMgrFlgD')){
			if(thisFldId =='suprevMgrFlgD'){
				$(radBtn).val("Y")
				$("#mngrDisagrReason").html("please state the Reasons and Follow-up Action for Disagree the option!");
				$("#mngrBtnErrorMsg").html("");
				$("#errorFld").css("border","");
				$("#"+txtFldId).addClass("err-fld");
			}
			$("#"+txtFldId).removeAttr('disabled');
			$("#"+txtFldId).val('');
			$("#"+txtFldId).focus();
			$("#"+txtFldId).removeClass("err-fld");
			
		}
	}
}

function chkforOthers(radbtn, txtfldid) {
	
    if($(radbtn).is(":checked")){
		$("#"+txtfldid).removeAttr('disabled');
		
	}else{
		$("#"+txtfldid).val('');
		$("#"+txtfldid).prop('disabled','true');
	}

	/*if (radbtn.value == value) {

		$("#" + txtfldid).focus();
		$("#" + txtfldid).attr('readonly', false);

		$("#" + txtfldid).attr('class', '');
		$("#" + txtfldid).removeClass('hidden');
		$("#" + txtfldid).addClass("writeModeTextbox");

	} else {
		$("#" + txtfldid).val("");
		$("#" + txtfldid).attr('readonly', true);

		$("#" + txtfldid).attr('class', '');

		$("#" + txtfldid).addClass("readOnlyText");
	}*/
}

function setQValue(obj){
	if($(obj).prop("checked")){	$(obj).val("Y")}
		else{	$(obj).val("Y") }
}


function setQ1Value(obj){
	var id = $(obj).attr("id");
	if(id == 'swrepConflgY'){
		
		if(obj.checked == true){
			$("#swrepConfDets").removeAttr('disabled');
			//$("#swrepConflg").val("");
			$("#swrepConflgN").val("");
			$("#swrepConflgN").prop("checked",false)
			$("#swrepConflg").val("Y");
			$("#swrepConflgY").val("Y");
		}
		else if(obj.checked == false){
			$("#swrepConflgY").prop("checked",false)
	        $("#swrepConflg").val("");
			$("#swrepConflgN").val("");
			$("#swrepConflgY").val("");
		}
	}
	
	
	
	if(id == 'swrepConflgN'){
		
		if(obj.checked == true){
			$("#swrepConfDets").val('');
			$("#swrepConfDets").prop('disabled','true');
			//$("#swrepConflg").val("N");
			$("#swrepConflgY").val("");
			$("#swrepConflgY").prop("checked",false)
			$("#swrepConflg").val("N");
			$("#swrepConflgN").val("N");
		}
		else if(obj.checked == false){
			$("#swrepConflgN").prop("checked",false)
	        $("#swrepConflg").val("");
			$("#swrepConflgN").val("");
			$("#swrepConflgY").val("");
		}
	}
	//strPageValidMsgFld="retest"
	chkNextChange();
	
}
function setQ2Value(obj){
	var id = $(obj).attr("id");
	if(id == 'swrepAdvbyflgY'){
		if(obj.checked == true){
			//$("#swrepAdvbyflg").val("");
			$("#swrepAdvbyflgN").val("");
			$("#swrepAdvbyflgN").prop("checked",false)
			$("#swrepAdvbyflg").val("Y");
			$("#swrepAdvbyflgY").val("Y");
		}
		else if(obj.checked == false){
			$("#swrepAdvbyflgY").prop("checked",false)
	        $("#swrepAdvbyflg").val("");
			$("#swrepAdvbyflgY").val("");
			$("#swrepAdvbyflgN").val("");
		}
	
}
	
	if(id == 'swrepAdvbyflgN'){
		if(obj.checked == true){
			//$("#swrepAdvbyflg").val("");
			$("#swrepAdvbyflgY").val("");
			$("#swrepAdvbyflgY").prop("checked",false)
			$("#swrepAdvbyflg").val("N");
			$("#swrepAdvbyflgN").val("N");
		}
		else if(obj.checked == false){
			$("#swrepAdvbyflgN").prop("checked",false)
	        $("#swrepAdvbyflg").val("");
			$("#swrepAdvbyflgN").val("");
			$("#swrepAdvbyflgY").val("");
		}
	}
	chkNextChange();

}

function setQ3Value(obj){
	var id = $(obj).attr("id");
	if(id == 'swrepDisadvgflgY'){
	if(obj.checked == true){
		//$("#swrepConflg").val("");
		$("#swrepDisadvgflgN").val("");
		$("#swrepDisadvgflgN").prop("checked",false)
		$("#swrepDisadvgflg").val("Y");
		$("#swrepDisadvgflgY").val("Y");
	}
	else if(obj.checked == false){
		$("#swrepDisadvgflgY").prop("checked",false)
        $("#swrepDisadvgflg").val("");
		$("#swrepDisadvgflgY").val("");
		$("#swrepDisadvgflgN").val("");
	}
	
	}
	if(id == 'swrepDisadvgflgN'){
		if(obj.checked == true){
			//$("#swrepDisadvgflg").val("");
			$("#swrepDisadvgflgY").val("");
			$("#swrepDisadvgflgY").prop("checked",false)
			$("#swrepDisadvgflg").val("N");
			$("#swrepDisadvgflgN").val("N");
		}
		else if(obj.checked == false){
			$("#swrepDisadvgflgN").prop("checked",false)
	        $("#swrepDisadvgflg").val("");
			$("#swrepDisadvgflgN").val("");
			$("#swrepDisadvgflgY").val("");
		}
	}
	chkNextChange();

}

function setQ4Value(obj){
	var id = $(obj).attr("id");
	if(id == 'swrepProceedflgY'){
	if(obj.checked == true){
		$("#swrepProceedflg").val("");
		$("#swrepProceedflgN").val("");
		$("#swrepProceedflgN").prop("checked",false)
		$("#swrepProceedflg").val("Y");
		$("#swrepProceedflgY").val("Y");
	}
	else if(obj.checked == false){
		$("#swrepProceedflgY").prop("checked",false)
        $("#swrepProceedflg").val("");
		$("#swrepProceedflgY").val("");
	}
	}
	
	if(id == 'swrepProceedflgN'){
		if(obj.checked == true){
			//$("#swrepProceedflg").val("");
			$("#swrepProceedflgY").val("");
			$("#swrepProceedflgY").prop("checked",false)
			$("#swrepProceedflg").val("N");
			$("#swrepProceedflgN").val("N");
		}
		else if(obj.checked == false){
			$("#swrepProceedflgN").prop("checked",false)
	        $("#swrepProceedflg").val("");
			$("#swrepProceedflgN").val("");
			$("#swrepProceedflgY").val("");
		}
	}

	chkNextChange();

}

// go Prev Page 

function goPrevPage(obj,prevScreen){
	saveCurrentPageData(obj,prevScreen,true,false);
	//window.location.href= prevScreen;
}



// All Page Mandotary Field Validation 

var page_wise_err=false;

function allPageValidation(){
	
var isPrevArchvFlg = '';
	
	isPrevArchvFlg = chkPrevArchFnaDetls();
	console.log("isPrevArchvFlg=-----------------", isPrevArchvFlg)
	
	if(isPrevArchvFlg ){
   //if(!isEmpty(lat_FnaId)&&!isEmpty(curnt_FnaId)){
	 // if((lat_FnaId == curnt_FnaId) || (!isEmpty(lat_FnaId))){
		  
	 var signornot = $("#hTxtFldSignedOrNot").val();
	var mgrkycSentStatus = $("#kycSentStatus").val();
	var mgrapproveStatus = $("#mgrApproveStatus").val();
	
	if(signornot == "Y" ) {
		if(!isEmpty(mgrkycSentStatus)) {
			Swal.fire({
			  text: "This FNA Form is already signed and sent for approval process",
			  icon: 'info',
			  showCancelButton: false,
			}).then((result) => {
			  if (result.isConfirmed) {
				  Swal.close() 
				  
				  $("#ScanToSignModal").modal({
					    backdrop: 'static',
					    keyboard: false,
					    show: true
					  });
				  
				  var fnaId = $("#hTxtFldCurrFNAId").val();
			    	getSignPageData(fnaId);
			    	
			    	//poovathi add on 17-06-2021
			    	$("#scanToSignClient1Btn").trigger("click")
				  
				  $(".genqrbtn").hide();
				  $(".esignpad").addClass("d-none");
			  }
			})
			return false;
		}
		
	}
	
//	else {
		
	
		page_wise_err=false;
		
		saveCurrentPageData($("#btnNextFormUniq"),$("#applNextScreen").val(),false,false);
		
		fetchAllPageData();

//		if (!validateKycIntro(true)) //kycHome
//			return;
		
		if (!validateFnaSelfSpsDets(true)) //kycHome
			return;
			
		if (!validateFnaFatcaTaxDets(true))
			return;
			
		if (!validateFnaDepentDets(true))
			return;

		if(!validateFnaWellnesReview(true))
			return;

		//Vignesh Add On 07-05-2021
        var repInvFlg =  FNA_TABLE_DATA.ccRepexistinvestflg;
      //product objectives part validation 
    	if(!validAdvAndRecmChkboxes(true)){
			return;
		}
      //products validation prod recommendation
    	
               var totalprods = ADVRECPRDTPLAN_DATA.length;
    		   var selfSpsArr = [];
    		
    			if(!isEmpty(seesselfName.toUpperCase())){
    				selfSpsArr.push(seesselfName.toUpperCase())
    			}
    			
    			if(!isEmpty(sessspsname)){
    				selfSpsArr.push(sessspsname.toUpperCase())
    			}
    			
    			var advrecplanData = ADVRECPRDTPLAN_DATA;
    			var prodRecomClntNames = [];
    			var page4 = "productRecommend";	
    			
    			for ( var advrec in advrecplanData) {
    				var ClntName = advrecplanData[advrec]["txtFldRecomPpName"];
    				if(!prodRecomClntNames.includes(ClntName.trim())){
    					prodRecomClntNames.push(ClntName);
    					
    				}
    			}
    			
    			var validateFlg = false;
    			var prodSwalTitle = '';
    			var prodSwalTxt = '';
    			
    			if(!isEmpty(seesselfName) && (prodRecomClntNames.length > 0)){
    				
    				if(!prodRecomClntNames.includes(seesselfName.toUpperCase())){
    				     validateFlg = true;
    					 prodSwalTitle = 'Client (1)';
    		    	}
    			}
    			
    			if(!isEmpty(sessspsname) && (prodRecomClntNames.length > 0)){
    				
    				if(!prodRecomClntNames.includes(sessspsname.toUpperCase())){
    					 validateFlg = true;
    					 prodSwalTitle = 'Client (2)';
		    		}
    			}
    			
    			if(selfSpsArr.length > 0  && totalprods == 0){
    				 validateFlg = true;
    				 if(selfSpsArr.length == 1){
    					 prodSwalTitle = 'Client (1)';
    				 }else{
    					 prodSwalTitle = 'Client (1) and Client (2)';
    				 }
    				 
	    		}
    			
    			var prodScrnValiFlg = $("#hTxtFldProdSrnValidation").val();
    			
    			if(validateFlg == true && isEmpty(prodScrnValiFlg)){
    				  swal.fire({
    				  title:"New Purchases / Top-up Recommendation " +prodSwalTitle+ " Details is not entered.",
    				  text: "Are you  proceed without entering "+prodSwalTitle+ " New Purchases / Top-up recommendations?",
    				  icon: 'question',
    				  showCancelButton: true,
    				  confirmButtonColor: '#d33',
    				  cancelButtonColor: '#3085d6',
    				  cancelButtonText: 'Enter Now',
    				  confirmButtonText: 'Confirm',
    				  showLoaderOnConfirm: true,
    				  allowOutsideClick:false,
    				  allowEscapeKey:false,
    				}).then((result) => {
    				  if (result.isConfirmed) {
    				    $.ajax({
    				     	    url: baseUrl+"/fnadetails/prodScreenValidation/",
    					            type: "GET",
    					            async:false,
    					            success: function (response) {
    					            	
    					       },
    					       error: function(xhr,textStatus, errorThrown){
    					 			ajaxCommonError(textStatus,xhr.status,xhr.statusText);
    							}
    				      });
    				     swal.close();
    				     procedOtherScrnValidations();
    				  }else{
    					   if (!validateFnaArtuPlantDet(true))
    	    			   return;
    					   procedOtherScrnValidations();
    					}
    				})
    				
    		}else{
    			procedOtherScrnValidations();
    		}
   }
	
}


function procedOtherScrnValidations(){
		
		 var repInvFlg =  FNA_TABLE_DATA.ccRepexistinvestflg;
		
		if(repInvFlg == "Y"){
         	
         	//validate switch / replace screen
           	if (!validateFnaSwitchReplacePlantDet(true)){
           		return;
           	}
       		//end	
           	
         	//prod witch and replace
         	if (!validateFnaSwrepPlantDet(true))
     			return;
         	
        }
		  
       if (!validateReasonforRecomm(true))
			return;
			
		if(!validateClntConsent(true)){
			return;
		}

			
			loaderBlock();
			
			saveCurrentPageData($("#btnNextFormUniq"),$("#applNextScreen").val(),false,false);
			
			setPageKeyInfoSucs();		
			
			setTimeout(function(){
				
				closeElement() ;
				
				
				Swal.fire({
				 // title: 'Are you sure?',
				  html: "Key Information <strong>verification is completed</strong>. Do you want to <strong>proceed to signature?</strong>",
				  icon: 'success',
				  allowOutsideClick:false,
				  allowEscapeKey:false,
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Yes, Proceed'
				}).then((result) => {
				  if (result.isConfirmed) {
					  
					  
					  $('#AddProdRecommDetlsMdl').modal('hide');
				    	$("#ScanToSignModal").modal({
					    backdrop: 'static',
					    keyboard: false,
					    show: true
					  });
				    	
				    	$("#scanToSignClient1Btn").trigger("click");
				    	
				    	var cfnaId=$("#hdnSessFnaId").val();
				    	Cookies.remove(cfnaId, { path: '/' })
				    	var fnaId = $("#hTxtFldCurrFNAId").val();
				    	
				        getSignPageData(fnaId);
				    	getAllSign(fnaId)
				    	//end
			
						setTimeout(function(){
							startTimer()
						},1200);
						
						
				  }
				})
			
			
		
			
		
			},1000);

		return true;	
			
		}


function getSignPageData(fnaId) {
	
	
	
	var selfName = '',spsName = '';               	
             $.ajax({
        	    url: baseUrl+"/fnadetails/getSignPageData/"+fnaId,
  	            type: "GET",
  	            async:false,
  	            dataType: "json",
	            contentType: "application/json",
	            success: function (response) {
	            	selfName = response.dfSelfName;
	                spsName = response.dfSpsName;
	                if (!isEmpty(spsName) && (spsName.length > 0)) {
	                	$(".spsNameTab").text(spsName);
	                	$("#scanToSignClient2Btn").removeClass("hide").addClass("show")
	                	sessspsname = spsName;
	                }else{
	                	$(".spsNameTab").text("");
	                	$("#scanToSignClient2Btn").removeClass("show").addClass("hide")
	                	sessspsname = "";
	                }
	            	$(".selfNameTab").text(selfName)
	       },
	       error: function(xhr,textStatus, errorThrown){
	 			//$('#cover-spin').hide(0);
	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			}
         });
	
}

function getLatestFnaIdDetls(fnaId){
	var jsnDataFnaDetails = {};
	$.ajax({
	    url: baseUrl+"/fnadetails/getLatestFnaDetls/"+fnaId,
          type: "GET",
          async:false,
          dataType: "json",
          contentType: "application/json",
          success: function (response) {
        	//alert(response)
        	 jsnDataFnaDetails = response;
        	setLatestFnaDetlstoPage(jsnDataFnaDetails);
         },
         error: function(xhr,textStatus, errorThrown){
	 			//$('#cover-spin').hide(0);
	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			}
     });
	
}



  function setCurrentPageData(jsnDataFna){
	  if(!isEmpty(jsnDataFna)  && !isJsonObjEmpty(jsnDataFna)){
		  for(var obj in jsnDataFna){
			  if (jsnDataFna.hasOwnProperty(obj)) {
				  var objValue = jsnDataFna[obj];
//				  console.log(obj,objValue,"-----------------------------------------")
				  
			  }
			  
		  }
	  }
  }


	function setLatestFnaDetlstoPage(jsnDataFnaDetails){
		if(!isEmpty(jsnDataFnaDetails)  && !isJsonObjEmpty(jsnDataFnaDetails)){
    		
    		  for(var obj in jsnDataFnaDetails){
    			  if (jsnDataFnaDetails.hasOwnProperty(obj)) {
    				  var objValue = jsnDataFnaDetails[obj];
    				  //console.log("key"+obj+"value" + objValue)
    				 // console.log(obj,objValue,"--------------")
    				  switch(obj){
    				  
    				  case "custId":
    					  $("#hTxtFldCurrCustId").val(objValue);
    					  break;
    				  case "adviser_name":
    					  $(".adviserNameTab").text(objValue);
    					//poovathi add on 16-09-21 (ekyc dev change) 
    					  $("#txtFldAdvStfName").text(objValue);
    					  break;
    					  
    				  case "manager_name":
    					  //$(".managerNameTab").text(objValue);
    					//poovathi add on 16-09-21 (ekyc dev change) 
    					  $("#txtManager").text(objValue);
    					  break;
    					  
    				  case "mgrEmailSentFlg":
    					  if(!isEmpty(objValue) && objValue == "Y" && strMngrFlg == "Y") {
    						  mgrmailsentflag = true;
    					  }else{
    						  mgrmailsentflag = false;
    					  }
    					  
    					  break;
    				
    					  
    				     case "suprevMgrFlg":
    					   if(objValue == "agree" ){ 
    						
    						     $("#suprevMgrFlgA").prop("checked",true);
                                 $("INPUT[name=suprevMgrFlg]").val([objValue]);
                                 //$("#diManagerSection").find(":input").prop("disabled",true);
       						     $("#diManagerSection").addClass("disabledsec");
    						  } 
    					  else if(objValue == "disagree" ){
    						  $("#suprevMgrFlgD").prop("checked",true);
    						  $("INPUT[name=suprevMgrFlg]").val([objValue]);
    						  $("#diManagerSection").removeClass("disabledsec");
    						  } 
    					  else {
    						  $("#suprevMgrFlgA").prop("checked",false);
    						  $("#suprevMgrFlgD").prop("checked",false);
    						  $("#diManagerSection").removeClass("disabledsec");
    						  //$("#diManagerSection").find(":input").removeAttr("disabled");
    						  }
    					   break;
    					   
    					case "mgrName":
//    					  No action
    					  break;
    					  
    				  
    				  default:
    					  $("#"+obj).val(objValue);
    				  }
    				  
    			  }
    			  
    		  }

    	}
	}
	
	
	function getMngrDetlsbyFnaId (fnaId){
		var jsnDataFnaDetails = {};
		$.ajax({
		    url: baseUrl+"/fnadetails/getLatestFnaDetls/"+fnaId,
	          type: "GET",
	          async:false,
	          dataType: "json",
	          contentType: "application/json",
	          success: function (response) {
	        	//alert(response)
	        	 jsnDataFnaDetails = response;
	        	 if(!isEmpty(jsnDataFnaDetails)  && !isJsonObjEmpty(jsnDataFnaDetails)){
	         		
	       		  for(var obj in jsnDataFnaDetails){
	       			  if (jsnDataFnaDetails.hasOwnProperty(obj)) {
	       				  var objValue = jsnDataFnaDetails[obj];
	       				  switch(obj){
	       				   case "adviser_name":
	       					  $(".adviserNameTab").text(objValue);
	       					  break;
	       					  
	       				   case "manager_name":
	       					  //$(".managerNameTab").text(objValue);
	       					  break;
	       				   default:
	       					  $("#"+obj).val(objValue);
	       				  }
	       				  
	       			  }
	       			  
	       		  }

	       	}
	        	
	         },
	         error: function(xhr,textStatus, errorThrown){
		 			//$('#cover-spin').hide(0);
		 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
				}
	     });
		
	}
/*
$(".err-fld").on("change",function(){
//	rmveErrMsg($(this),"")


if($(this).parent().find("div").hasClass("inerrfld")){
	$(this).parent().find("div.inerrfld").remove();
}

$(this).removeClass("err-fld");

})
*/
/*function rmveErrMsg(Errsec,thisObjErrCls){
//	if($("#"+Errsec).find("."+thisObjErrCls).length > 0){
//	   $("#"+Errsec).find("."+thisObjErrCls).remove();
//	}

var fldid = $(Errsec).prop("id");
	
	if($("#"+fldid).parent().find("div").hasClass("inerrfld")){
		 $("#"+fldid).parent().find("div.inerrfld").remove();
	 }
	$("#"+fldid).removeClass("err-fld");
}*/




function clearSpsfld(){
	
	var spsClearArr=["dfSpsNric","dfSpsDob","dfSpsHp","dfSpsGender","dfSpsOffice","dfSpsPersemail","dfSpsMartsts"
	                 ,"dfSpsHomeaddr","dfSpsOccpn","dfSpsHeight","dfSpsWeight"];
	
	$("#tblSelfSps tbody #radSpsSG").prop("checked",false);
	
	if($("#tblSelfSps tbody #radSpsOTH")[0].checked){
		$("#tblSelfSps tbody #radSpsSG").trigger("click").prop("checked",false);
	}
	
	if($("#tblSelfSps tbody #radSpsSGPR")[0].checked){
		$("#tblSelfSps tbody #radSpsSG").trigger("click").prop("checked",false);
	}
	
	
	$.each(spsClearArr,function(i,obj){
		$("[name="+obj+"]").val("");
	});
}


function genreportkyc() {

	var fnaform = document.fnaForm;
	var fnaid = fnaform.txtFldFnaId.value;
	var custid = fnaform.txtFldCustId.value;
	var formtype = document.getElementById("txtFldKycForm").value;
	
	
	 

	if(isEmpty(fnaid)){

	}else{
//		 $("#loadingMessage").show();

		var machine = "";
		machine = BIRT_URL + "/frameset?__report=" + FNA_RPT_FILELOC //
				+ "&P_CUSTID=" + custid + "&P_FNAID=" + fnaid+"&__format=pdf"
				+ "&P_PRDTTYPE=" + custLobDets
				+ "&P_CUSTOMERNAME=&P_CUSTNRIC=&P_FORMTYPE=" + formtype
				+ "&P_DISTID=" + LOGGED_DISTID;

		if(!chkAllDataSaved(machine,true,FNA_CONS_MODIFY_PDF)){
			return;
		}
//		 $("#loadingMessage").hide();
	}





//	creatSubmtDynaReportPDF(machine);

}


function loadExistCustDet(strDataCust) {
//	 alert("loadexisting...")

	var custDets = strDataCust;

	for ( var cval in custDets) {

		var ctabdets = custDets[cval];

		for ( var ctab in ctabdets) {

			if (ctabdets.hasOwnProperty(ctab)) {
				var ckey = ctab;
				var cvalue = ctabdets[ctab];
				// / alert(ckey + "="+cvalue);

				var elemObj = eval('fnaForm.' + ckey);
				if (elemObj)
					elemObj.value = cvalue;

				if (elemObj && elemObj.type == "checkbox" && cvalue == "Y") {
					// alert("cvalue ----->"+cvalue)
					elemObj.checked = true;
					fnaAssgnFlgCommon(elemObj, 'chk');
					// elemeObj.value=cvalue;
				}

				if (elemObj && elemObj.type == "radio") {
					elemObj.value = cvalue;
					elemObj.checked = true;
				}

				if (ckey == "dfSelfNationality" || ckey == "dfSpsNationality") {
//					alert(ckey +"="+ cvalue)

					for(var cnt=0;cnt<3;cnt++){
							if(document.getElementsByName(ckey))
								document.getElementsByName(ckey)[cnt].checked=false;
						}

						switch (cvalue) {
							case "SG":document.getElementsByName(ckey)[0].checked = true;;break;
							case "SGPR":document.getElementsByName(ckey)[1].checked = true;break;
							case "OTH":document.getElementsByName(ckey)[2].checked = true;break;
						}


					chkForContry(ckey, cvalue);
				}


				if(ckey=='dfSelfBusinatr' && cvalue.toUpperCase()=='OTHERS'){
					document.getElementById('dfSelfBusinatr').onchange();
				}

				for (var elem = 0; elem < document.getElementsByName(ckey).length; elem++) {
					document.getElementsByName(ckey)[elem].value = cvalue;
				}
				
				if(ckey == 'dfSelfHeight'){
					$("#dfSelfHeight").val(removeDecimalPoint(cvalue));
				}
				
				if(ckey == 'dfSpsHeight'){
					$("#dfSpsHeight").val(removeDecimalPoint(cvalue));
				}

			}
		}
	}
}

function trim(stringToTrim) {
    return stringToTrim.replace(/^\s+|\s+$/g,"");
}//end trim 


function removeErrFld(obj){
	
	$(obj).closest('div[class^="col-"], div[class*=" col-"]').find("div.inerrfld").remove();
	if($(obj).prop("name") == "prodrecObjectives") {//exceptional
		$(obj).parents('div[class^="col-"], div[class*=" col-"]').find("div.inerrfld").remove();
	}
	
	//console.log($(obj).prop("id"),"")
	
	if($(obj).prop("id") == "span_dfSelfUstaxno") {//exceptional
		$(obj).parents('div[class^="col-"], div[class*=" col-"]').find("div.inerrfld").remove();
	}
	
var classList = isEmpty($(obj).prop("class"))?"":$(obj).prop("class").split(" ")
if(classList.length >0){

			for(var l=0;l<classList.length;l++) {
				var classname = classList[l];
				if(classname.indexOf("grp") > 0) {
					$('.'+classname).each(function(){
						$(this).closest('div[class^="col-"], div[class*=" col-"]').find("div.inerrfld").remove();
						$(this).removeClass("err-fld")
					})
					
				}
			}	
	
}

	$(obj).removeClass("err-fld");
	$(obj).closest(".card-body").removeClass("err-fld")
			
}


function makeErrMsg(Errsec,ErrCls,Err,ErrPge){
	  
	  var inerr = ' <div class="invalid-feedbacks show inerrfld" >'+Err+'</div>';

if(!isEmpty(Errsec) && $("#"+Errsec).length){
	if($("#"+Errsec).closest('div[class^="col-"], div[class*=" col-"]').find(".inerrfld").length ==0){
	var nodetype = $("#"+Errsec).prop('nodeName').toUpperCase();
	  if(nodetype != "SPAN"){
		 $("#"+Errsec).addClass("err-fld");
		}
		
	
	$("#"+Errsec).closest('div[class^="col-"], div[class*=" col-"]').append(inerr);
		
	}
}else if(!isEmpty(Errsec) && $("."+Errsec).length){
	if($("."+Errsec).closest('div[class^="col-"], div[class*=" col-"]').find(".inerrfld").length ==0){
	var nodetype = $("."+Errsec).prop('nodeName').toUpperCase();
	  if(nodetype != "SPAN"){
		 $("."+Errsec).addClass("err-fld");
		}
		
	
	$("."+Errsec).closest('div[class^="col-"], div[class*=" col-"]').append(inerr);
		
	}
}	
	 
		 
		 
}

function validateFnaSelfSpsDets(flag){
	var Page2="kycHome";
	
	if(isEmpty(FNASSDET_TABLE_DATA.dfSelfName)){
		addErrorMsgToChat("dfSelfName",NAMEOFCLIENT,Page2,null);
		
	}
	
	if(isEmpty(FNASSDET_TABLE_DATA.dfSelfNric)){
		addErrorMsgToChat("dfSelfNric",NRICOFCLIENT,Page2,null);
	}
	
	if(isEmpty(FNASSDET_TABLE_DATA.dfSelfGender)){
		addErrorMsgToChat("dfSelfGendergrp",GENDEROFCLIENT,Page2,"dfSelfGendergrp");
	}
	
	
	if(isEmpty(FNASSDET_TABLE_DATA.dfSelfDob)){
		addErrorMsgToChat("dfSelfDob",DOBOFCLIENT,Page2,null);
	}
	
	if(isEmpty(FNASSDET_TABLE_DATA.dfSelfMartsts)){
		addErrorMsgToChat("dfSelfMartsts",MARTIALSTATUSOFCLIENT,Page2,null);
	}
	
	
	if(isEmpty(FNASSDET_TABLE_DATA.dfSelfBirthcntry)){
		addErrorMsgToChat("dfSelfBirthcntry",BIRTHCOUNTRYCLIENT,Page2,null);
	}
	
	if(isEmpty(FNASSDET_TABLE_DATA.dfSelfNationality)){
		$("#dfSelfNatyDets").addClass("invisible").removeClass("show");
		addErrorMsgToChat("dfSelfNationalitySign",NATLYCLIENT,Page2,"dfSelfNationalitygrp");
	} 
	
	
	if(!isEmpty(FNASSDET_TABLE_DATA.dfSelfNationality) && (FNASSDET_TABLE_DATA.dfSelfNationality != "SG" && FNASSDET_TABLE_DATA.dfSelfNationality != "Singaporean")){
		$("#dfSelfNatyDets").addClass("show").removeClass("invisible");
		if(isEmpty(FNASSDET_TABLE_DATA.dfSelfNatyDets)){
			addErrorMsgToChat("dfSelfNatyDets",NATLYCLIENTOTH,Page2,null);
		}
	}else{
		$("#dfSelfNatyDets").addClass("invisible").removeClass("show");
	}
	
	//contact details sec changed
	if(isEmpty(FNASSDET_TABLE_DATA.dfSelfMobile) && isEmpty(FNASSDET_TABLE_DATA.dfSelfHome) && isEmpty(FNASSDET_TABLE_DATA.dfSelfOffice)){
		addErrorMsgToChat("dfSelfMobilegrp",CONTACTDETAILS1,Page2,"dfSelfMobilegrp");
		
	}
	/*if(isEmpty(FNASSDET_TABLE_DATA.dfSelfPersemail) && isEmpty(FNASSDET_TABLE_DATA.dfSelfPersemail)){
		addErrorMsgToChat("dfSelfPersemail",CONTACTDETAILS1,Page2);
	}*/
	
	if(isEmpty(FNASSDET_TABLE_DATA.dfSelfHomeaddr)){
		addErrorMsgToChat("dfSelfHomeaddr",REGADDRESSCLIENT,Page2,null);
	}
	
	if(!isEmpty(FNASSDET_TABLE_DATA.dfSelfSameasregadd) && FNASSDET_TABLE_DATA.dfSelfSameasregadd == "Y" && isEmpty(FNASSDET_TABLE_DATA.dfSelfMailaddr) ){
		addErrorMsgToChat("dfSelfMailaddr",MAILINGADDRESSOTHERS,Page2,null);
	}
	
	//Employment &Financial Detls Sec Validation start
	
	//Employer Name
	if(isEmpty(FNASSDET_TABLE_DATA.dfSelfCompname)){
		addErrorMsgToChat("dfSelfCompname",EMPLOYEEROFCLIENT,Page2,null);
	}
	
	//Occupation
	if(isEmpty(FNASSDET_TABLE_DATA.dfSelfOccpn)){
		addErrorMsgToChat("dfSelfOccpn",OCCUPATIONOFCLIENT,Page2,null);
	}
	
	//Estd.AnnualIncome
	if(isEmpty(FNASSDET_TABLE_DATA.dfSelfAnnlincome)){
		addErrorMsgToChat("dfSelfAnnlincome",ANNUALINCOMEOFCLIENT,Page2,null);
	}
	
	//Nature of Business
	if(isEmpty(FNASSDET_TABLE_DATA.dfSelfBusinatr)){
		addErrorMsgToChat("dfSelfBusinatr",NATUREBUSINESSOFCLIENT,Page2,null);
	}
	//End
	
	//Nature of Business Others validation self
	if(FNASSDET_TABLE_DATA.dfSelfBusinatr == "Others" && isEmpty(FNASSDET_TABLE_DATA.dfSelfBusinatrDets)){
		addErrorMsgToChat("dfSelfBusinatrDets",NATUREBUSINESSOTHERSOFCLIENT,Page2,null);
	}
	//End
	
	var srcFunds = isEmpty(FNASSDET_TABLE_DATA.dfSelfFundsrc) ? "{}": JSON.parse(FNASSDET_TABLE_DATA.dfSelfFundsrc);
	if(!isEmpty(srcFunds)){
		
		if( (srcFunds.CERN == "Y"  || srcFunds.CINV == "Y" || srcFunds.CPRS == "Y" || srcFunds.CCPF == "Y" || srcFunds.COTH == "Y")) {
			
		}else {
			addErrorMsgToChat("chkCDClntFundSrcgrp",SOURCEOFFFUNDCLIENT,Page2,"chkCDClntFundSrcgrp");	
		}
		
		
	}
//	else if(!isEmpty(srcFunds) && srcFunds == "{}" ) {
//		addErrorMsgToChat("chkCDClntErndIncm",SOURCEOFFFUNDCLIENT,Page2,"dfSelfFundsrc");
//	}
	
	
	if(srcFunds.COTH == "Y" && isEmpty(FNASSDET_TABLE_DATA.dfSelfFundsrcDets)){
		addErrorMsgToChat("dfSelfFundsrcDets",SRCFUNDOTHCLIENT,Page2,null);
	}
	
	if(isEmpty(FNASSDET_TABLE_DATA.dfSelfEdulevel) ){
		addErrorMsgToChat("dfSelfEdulevel",EDULVLCLIENT,Page2,"dfSelfEdulevelRad");
	}
	
	if(isEmpty(FNASSDET_TABLE_DATA.dfSelfEngSpoken) ){
		addErrorMsgToChat("dfSelfEngSpokenY",LANGSPOKCLIENT,Page2,"dfSelfEngSpoken");
	}
	
	
	if(isEmpty(FNASSDET_TABLE_DATA.dfSelfEngWritten) ){
		addErrorMsgToChat("dfSelfEngWrittenY",LANGWRITECLIENT,Page2,"dfSelfEngWritten");
	}
	
	//---------------- Interpreter and Language
	
//	alert("FNA_TABLE_DATA.cdIntrprtflg--->"+FNA_TABLE_DATA.cdIntrprtflg)
	var selfLanguage = isEmpty(FNA_TABLE_DATA.cdLanguages) ? "{}" : JSON.parse(FNA_TABLE_DATA.cdLanguages);
	
	if(!isEmpty(FNA_TABLE_DATA.cdIntrprtflg) && FNA_TABLE_DATA.cdIntrprtflg == 'Y'){
		
		if(selfLanguage.ENG == "Y" || selfLanguage.OTH == "Y"  || selfLanguage.MAN == "Y"  || selfLanguage.MAL == "Y"  || selfLanguage.TAM == "Y" ){
//			no action		
		}else {
			addErrorMsgToChat("htflaneng","Select Any 1 Language Use",Page2,null);
		}
		
		if(isEmpty(FNA_TABLE_DATA.intprtName) ){
			addErrorMsgToChat("intprtName",INTEPRE_NAME,Page2,null);
		}
		
		if(isEmpty(FNA_TABLE_DATA.intprtContact) ){
			addErrorMsgToChat("intprtContact",INTEPRE_CONTACT,Page2,null);
		}
		if(isEmpty(FNA_TABLE_DATA.intprtRelat) ){
			addErrorMsgToChat("intprtRelat",INTEPRE_SEL_RELSHIP,Page2,null);
		}
	}
	//----------------end of interpreter
	
	//-----------------------------Spouse Elements---------
	
	if(!isEmpty(FNASSDET_TABLE_DATA.dfSpsName)){
		var spstabfocus = false;
		
		if(isEmpty(FNASSDET_TABLE_DATA.dfSpsNric)){
			addErrorMsgToChat("dfSpsNric",NRICOFSPOUSE,Page2,null);
//			$('#myTab li:eq(1) a').tab('show');
			spstabfocus = true;
			
		}
		
		if(isEmpty(FNASSDET_TABLE_DATA.dfSpsGender)){
			addErrorMsgToChat("dfSpsGendergrp",NAMEOFSPOUSEGENDER,Page2,"dfSpsGendergrp");
//			$('#myTab li:eq(1) a').tab('show');
			spstabfocus = true;
		}
		
		
		if(isEmpty(FNASSDET_TABLE_DATA.dfSpsDob)){
			addErrorMsgToChat("dfSpsDob",DOBOFSPOUSE,Page2,null);
			//$('#myTab li:eq(1) a').tab('show');
			spstabfocus = true;
		}
		
		if(isEmpty(FNASSDET_TABLE_DATA.dfSpsMartsts)){
			addErrorMsgToChat("dfSpsMartsts",MARTIALSTATUSOFSPOUSE,Page2,null);
//			$('#myTab li:eq(1) a').tab('show');
			spstabfocus = true;
		}
		if(isEmpty(FNASSDET_TABLE_DATA.dfSpsBirthcntry)){
			addErrorMsgToChat("dfSpsBirthcntry",BIRTHCOUNTRYSPOUSE,Page2,null);
//			$('#myTab li:eq(1) a').tab('show');
			spstabfocus = true;
		}
		
		
		if(isEmpty(FNASSDET_TABLE_DATA.dfSpsNationality)){
			$("#dfSpsNatyDets").addClass("invisible").removeClass("show");
			addErrorMsgToChat("dfSpsNationalitySign",NATLYSPOUSE,Page2,"dfSpsNationalitygrp");
//			$('#myTab li:eq(1) a').tab('show');
			spstabfocus = true;
		}
		
		
		
		if(!isEmpty(FNASSDET_TABLE_DATA.dfSpsNationality) && (FNASSDET_TABLE_DATA.dfSpsNationality != "SG" && FNASSDET_TABLE_DATA.dfSpsNationality != "Singaporean")){
			$("#dfSpsNatyDets").addClass("show").removeClass("invisible");
			if(isEmpty(FNASSDET_TABLE_DATA.dfSpsNatyDets)){
				addErrorMsgToChat("dfSpsNatyDets",NATLYSPOUSEOTH,Page2,null);
//				$('#myTab li:eq(1) a').tab('show');
				spstabfocus = true;
			}
		}else{
			$("#dfSpsNatyDets").addClass("invisible").removeClass("show");
		}
		
		
		//spouse contact detls change
		if(isEmpty(FNASSDET_TABLE_DATA.dfSpsHp)  && isEmpty(FNASSDET_TABLE_DATA.dfSpsHome) && isEmpty(FNASSDET_TABLE_DATA.dfSpsOffice)){
			addErrorMsgToChat("dfSpsHp",CONTACTDETAILSSPOUSE1,Page2,"dfSpsHpgrp");
//			$('#myTab li:eq(1) a').tab('show');
			spstabfocus = true;
		}
		
		
		if(isEmpty(FNASSDET_TABLE_DATA.dfSpsHp) && isEmpty(FNASSDET_TABLE_DATA.dfSpsPersemail)){
			addErrorMsgToChat("dfSpsPersemail",CONTACTDETAILSSPOUSE1,Page2,null);
			spstabfocus = true;
		}
		
		if(isEmpty(FNASSDET_TABLE_DATA.dfSpsHomeaddr)){
			addErrorMsgToChat("dfSpsHomeaddr",REGADDRESSSPOUSE,Page2,null);
//			$('#myTab li:eq(1) a').tab('show');
			spstabfocus = true;
		}
		
		
		if(!isEmpty(FNASSDET_TABLE_DATA.dfSpsMailaddrflg) && FNASSDET_TABLE_DATA.dfSpsMailaddrflg == "Y" && isEmpty(FNASSDET_TABLE_DATA.dfSpsMailaddr) ){
			addErrorMsgToChat("dfSpsMailaddr",MAILINGADDRESSOTHERS,Page2,null);
//			$('#myTab li:eq(1) a').tab('show');
			spstabfocus = true;
		}
		
		var srcFundsSps = isEmpty(FNASSDET_TABLE_DATA.dfSpsFundsrc) ?"{}": JSON.parse(FNASSDET_TABLE_DATA.dfSpsFundsrc);
		
		if(!isEmpty(srcFundsSps)){
			if(srcFundsSps.SCERN == "Y"  || srcFundsSps.SCINV == "Y" || srcFundsSps.SCPRS == "Y" || srcFundsSps.SCCPF == "Y" || srcFundsSps.SCOTH == "Y") {
				
			}else {
				addErrorMsgToChat("spsSrcFundgrp",SOURCEOFFFUNDSPOUSE,Page2,"spsSrcFundgrp");
				
//				$('#myTab li:eq(1) a').tab('show');
				spstabfocus = true;
			}
			
		}
		
		
		//Employment &Financial Detls Sec Validation start spouse
		
		//Employer Name
		if(isEmpty(FNASSDET_TABLE_DATA.dfSpsCompname)){
			addErrorMsgToChat("dfSpsCompname",EMPLOYEEROFSPOUSE,Page2,null);
			spstabfocus = true;
		}
		
		//Occupation
		if(isEmpty(FNASSDET_TABLE_DATA.dfSpsOccpn)){
			addErrorMsgToChat("dfSpsOccpn",OCCUPATIONOFSPOUSE,Page2,null);
			spstabfocus = true;
		}
		
		//Estd.AnnualIncome
		if(isEmpty(FNASSDET_TABLE_DATA.dfSpsAnnlincome)){
			addErrorMsgToChat("dfSpsAnnlincome",ANNUALINCOMEOFSPOUSE,Page2,null);
			spstabfocus = true;
		}
		
		//Nature of Business
		if(isEmpty(FNASSDET_TABLE_DATA.dfSpsBusinatr)){
			addErrorMsgToChat("dfSpsBusinatr",NATUREBUSINESSOFSPOUSE,Page2,null);
			spstabfocus = true;
		}
		//End
		
		//Nature ofBuss Others validation spouse
		if(FNASSDET_TABLE_DATA.dfSpsBusinatr == "Others" && isEmpty(FNASSDET_TABLE_DATA.dfSpsBusinatrDets)){
			addErrorMsgToChat("dfSpsBusinatrDets",NATUREBUSINESSOTHERSOFSPOUSE,Page2,null);
			spstabfocus = true;
		}
		//End
		
		
		if(srcFundsSps.SCOTH == "Y" && isEmpty(FNASSDET_TABLE_DATA.dfSpsFundsrcDets)){
			addErrorMsgToChat("dfSpsFundsrcDets",SRCFUNDOTHSPOUSE,Page2,null);
//			$('#myTab li:eq(1) a').tab('show');
			spstabfocus = true;
		}
		
		
		if(isEmpty(FNASSDET_TABLE_DATA.dfSpsEdulevel) ){
			addErrorMsgToChat("dfSpsEdulevel",EDULVLSPOUSE,Page2,"dfSpsEdulevelRad");
//			$('#myTab li:eq(1) a').tab('show');
			spstabfocus = true;
		}
		
		if(isEmpty(FNASSDET_TABLE_DATA.dfSpsEngSpoken) ){
			addErrorMsgToChat("dfSpsEngSpokenY",LANGSPOKSPOUSE,Page2,"dfSpsEngSpoken");
//			$('#myTab li:eq(1) a').tab('show');
			spstabfocus = true;
		}
		
		
		if(isEmpty(FNASSDET_TABLE_DATA.dfSpsEngWritten) ){
			addErrorMsgToChat("dfSpsEngWrittenY",LANGWRITESPOUSE,Page2,"dfSpsEngWritten");
//			$('#myTab li:eq(1) a').tab('show');
			spstabfocus = true;
		}
		
	}
	//-----------------------------end of spouse elements
	
	if(	spstabfocus) {
		//$('#myTab li:eq(1) a').tab('show');
	}
	
	
	
/*	$("#chkSignPersDet").prop("checked",true);
	$("#validation1").removeClass("err-fld");
	*/
//	console.log(page_wise_err,"page_wise_err")
	
	if(page_wise_err)return false;
	
	//closeElement()
	return true;
} 



function validateFnaFatcaTaxDets(flag) {
	
	//clear all errmessages in keyInformation PopUp chat window
	removeAllErrorMsgInChat();
	
	var page4="taxResidency"
	var spsName = FNASSDET_TABLE_DATA.dfSpsName;//$("#dfSpsName").val();


	var clientFatcaFlag = FNASSDET_TABLE_DATA.dfSelfUsnotifyflg ;// $("#dfSelfUsnotifyflg").prop("checked");
	var spouseFatcaFlag  = FNASSDET_TABLE_DATA.dfSpsUsnotifyflg;
	
	var clientUsPersFlag = FNASSDET_TABLE_DATA.dfSelfUspersonflg;
	var spouseUsPersFlag = FNASSDET_TABLE_DATA.dfSpsUspersonflg;
	
	if(clientFatcaFlag == "N" && clientUsPersFlag =="N") {
		addErrorMsgToChat("dfSelfUsnotifyflg1","Select any US Tax Declaration option for client(1)" ,page4,"dfSelfUsnotifyflggrp");
	}
	
	 if(!isEmpty(spsName)) {
		 if(spouseFatcaFlag == "N" && spouseUsPersFlag =="N") {
				addErrorMsgToChat("dfSpsUsnotifyflg1","Select any US Tax Declaration option for client(2)",page4,"dfSpsUsnotifyflggrp");
			}
	 }
	
	 
	var selfTaxNo =FNASSDET_TABLE_DATA.dfSelfUstaxno ;//document.getElementById('dfSelfUstaxno');
	var spouseTaxNo = FNASSDET_TABLE_DATA.dfSpsUstaxno ;//document.getElementById('dfSpsUstaxno');

	if (clientUsPersFlag =="Y") {
		if(isEmpty(selfTaxNo)){
			removeAllErrorMsgInChat();
			addErrorMsgToChat("dfSelfUstaxno",USTAX_PRSNCLNT_VAL_MSG ,page4,null);
		}
	}// end of if

	
	if (spouseUsPersFlag =="Y" && !isEmpty(spsName)) {
		if(isEmpty(spouseTaxNo)){
			removeAllErrorMsgInChat();
			addErrorMsgToChat("dfSpsUstaxno",USTAX_PRSNSPS_VAL_MSG,page4,null);
		}
	}// end of if

	if(clientFatcaFlag =="Y") {
		
		if (FATCATAXDET_DATA.length == 0) {
			removeAllErrorMsgInChat();
			addErrorMsgToChat("span_dfSelfUstaxno",FNA_FATCA_ATLEAST_1TAX +" for Client(1)" ,page4,null);
			$('body').scrollTo('#span_dfSelfUstaxno');
		//	$(".addtaxdetsbtn").trigger("click");
			
		}else {

			removeErrFld($("#span_dfSelfUstaxno"));  
			var fatcaData = FATCATAXDET_DATA;
			
			for ( var tax in fatcaData) {
				
				var refNo = fatcaData[tax]["txtFldFnaTaxRefNo"];
				var cntry = fatcaData[tax]["selFnaTaxCntry"];
				var reason = fatcaData[tax]["txtFldFnaNoTaxReason"];
				var reasonDets = fatcaData[tax]["txtFldFnaReasonDets"];
//				console.log(cntry)
				
				if(isEmpty(cntry)){
					removeAllErrorMsgInChat();
					addErrorMsgToChat("span_dfSelfUstaxno",FNA_FATCACONTRY_VALMSG+" for Client(1) ",page4,null);
				}
				
				if(isEmpty(refNo) && isEmpty(reason) ){
					removeAllErrorMsgInChat();
					addErrorMsgToChat("span_dfSelfUstaxno",FNA_NOTAXREASON +" for Client(1) ",page4,null);
				}
				
				if(!isEmpty(reason) && reason == "Reason B"&& isEmpty(reasonDets)){
					removeAllErrorMsgInChat();
					addErrorMsgToChat("span_dfSelfUstaxno",FNA_REASONDETAILS + " for Client(1) ",page4,null);
				}
				
			}
			
			
		
		}
	}

	if(spouseFatcaFlag =="Y" && !isEmpty(spsName)){
		if (FATCATAXDETSPS_DATA.length == 0) {
			removeAllErrorMsgInChat();
			addErrorMsgToChat("span_dfSpsUstaxno",FNA_FATCA_ATLEAST_1TAX +" for Client(2)" ,page4,null);
		}else {

           var fatcaDataSps = FATCATAXDETSPS_DATA;
			
			for ( var stax in fatcaDataSps) {
				
				var refNo = fatcaDataSps[stax]["txtFldFnaTaxRefNo"];
				var cntry = fatcaDataSps[stax]["selFnaTaxCntry"];
				var reason = fatcaDataSps[stax]["txtFldFnaNoTaxReason"];
				var reasonDets = fatcaDataSps[stax]["txtFldFnaReasonDets"];
				
				if(isEmpty(cntry)){
					removeAllErrorMsgInChat();
					addErrorMsgToChat("span_dfSpsUstaxno",FNA_FATCACONTRY_VALMSG + " for Client(2) ",page4,null);
					
				}
				
				if(isEmpty(refNo) && isEmpty(reason)){
					removeAllErrorMsgInChat();
					addErrorMsgToChat("span_dfSpsUstaxno",FNA_NOTAXREASON + " for Client(2) ",page4,null);
				}
				
//				if(isEmpty(reasonDets)){
//					addErrorMsgToChat("span_dfSpsUstaxno",FNA_REASONDETAILS + " for Client(2) ",page4,null);
//				}
				
				
				if(!isEmpty(reason) && reason == "Reason B"&& isEmpty(reasonDets)){
					removeAllErrorMsgInChat();
					addErrorMsgToChat("span_dfSelfUstaxno",FNA_REASONDETAILS + " for Client(2) ",page4,null);
				}
				
				
				
				
			}
			
			
			
		
		}
	}

/*	$("#TaxResi").prop("checked",true);
	$("#validation2").removeClass("err-fld");*/

	if(page_wise_err)return false;
	return true;
}


function validateFnaDepentDets(flag) {
	
	var page4 = "finanWellReview";
	
	var depData = FNADEPNT_TABLE_DATA;
	
	if(depData.length >0 ){
		for ( var dep in depData) {
			var depvalue = depData[dep];
			if(isEmpty(depvalue["txtFldFnaDepntName"])){
				addErrorMsgToChat("",FNA_DEPNTNAME_VALMSG + " for Client(1) ",page4,null);
			}
		}
	}
	
	

	
	$("#FinWell").prop("checked",true);
	$("#validation3").removeClass("err-fld");
	
	if(page_wise_err)return false;
	return true;
}

function skipProdRecom(){
	ADVRECPRDTPLAN_DATA.length=1;
	$('#AddProdRecommDetlsMdl').modal('hide');
//	allPageValidation();
	if(page_wise_err)return false;
	return true;
}

function validateFnaArtuPlantDet(flag) {
	var prodRecomClntNames = [];
		var page4 = "productRecommend";	
		
		var totalprods = ADVRECPRDTPLAN_DATA.length;
		if(totalprods == 0){
			$("#btnignoreNewProd").show()
			 addErrorMsgToChat("span_prodrecomm_msg",FNA_ARTURECOM_SELF_PROD_DET ,page4,null);
			$("#badgeAddProdRecomm").trigger("click");
		}else{
			$("#btnignoreNewProd").hide();
			
			var advrecplanData = ADVRECPRDTPLAN_DATA;
		
			for ( var advrec in advrecplanData) {
				
				var planOpt = advrecplanData[advrec]["txtFldRecomPpOpt"];
				var prodType = advrecplanData[advrec]["selFnaAdvRecPrdtType"]
				var prinName = advrecplanData[advrec]["selFnaAdvRecCompName"]
				var riskrate = advrecplanData[advrec]["selRecomProdRisk"];
				var ClntName = advrecplanData[advrec]["txtFldRecomPpName"];
				
				
				
				if(!prodRecomClntNames.includes(ClntName.trim())){
					prodRecomClntNames.push(ClntName);
					 console.log(prodRecomClntNames)
				}
			
			
				
				if(isEmpty(prodType)){
					addErrorMsgToChat("",FNA_ARTUPLANPRDTTYPE_VALMSG ,page4,null);
				}
				
				if(isEmpty(prinName)){
					addErrorMsgToChat("",FNA_ARTUPLAN_COMP_VALMSG ,page4,null);
				}
				
				
				if(prodType == "ILP"){
					
					if(isEmpty(riskrate)){
					addErrorMsgToChat("",FNA_ARTUPLAN_RISKRATE_VALMSG ,page4,null);
				}
					
				}
			}//testing end
			
			if(!isEmpty(seesselfName) && (prodRecomClntNames.length > 0)){
				
				if(!prodRecomClntNames.includes(seesselfName.toUpperCase())){
				    addErrorMsgToChat("span_prodrecomm_msg",FNA_ARTURECOM_SELF_PROD_DET ,page4,null);
					$("#badgeAddProdRecomm").trigger("click");
				}
			}
			
			if(!isEmpty(sessspsname) && (prodRecomClntNames.length > 0)){
				
				if(!prodRecomClntNames.includes(sessspsname.toUpperCase())){
					addErrorMsgToChat("span_prodrecomm_msg",FNA_ARTURECOM_SPS_PROD_DET ,page4,null);
					$("#badgeAddProdRecomm").trigger("click");
				}
			}
		}
	
		
	

	$("#LifeILPNewPurchase").prop("checked",true);
	$("#validation4").removeClass("err-fld");

	if(page_wise_err)return false;
	return true;

}

//poovathi add on 24-10-2021 to validate switch/replace screen 
function validateFnaArtuPlantDet111(flag) {
	var prodRecomClntNames = [];
		var page4 = "productRecommend";	
		
		var totalprods = ADVRECPRDTPLAN_DATA.length;
		if(totalprods == 0){
			  addErrorMsgToChat("span_prodrecomm_msg",FNA_ARTURECOM_SELF_PROD_DET ,page4,null);
			  $("#badgeAddProdRecomm").trigger("click");
		}else{
//testing     
			 
			var advrecplanData = ADVRECPRDTPLAN_DATA;
			
			for ( var advrec in advrecplanData) {
				
				var planOpt = advrecplanData[advrec]["txtFldRecomPpOpt"];
				var prodType = advrecplanData[advrec]["selFnaAdvRecPrdtType"]
				var prinName = advrecplanData[advrec]["selFnaAdvRecCompName"]
				var riskrate = advrecplanData[advrec]["selRecomProdRisk"];
				var ClntName = advrecplanData[advrec]["txtFldRecomPpName"];
				
				prodRecomClntNames.push(ClntName.trim());
				
				if(isEmpty(prodType)){
					addErrorMsgToChat("",FNA_ARTUPLANPRDTTYPE_VALMSG ,page4,null);
				}
				
				if(isEmpty(prinName)){
					addErrorMsgToChat("",FNA_ARTUPLAN_COMP_VALMSG ,page4,null);
				}
				
				
				if(prodType == "ILP"){
					
					if(isEmpty(riskrate)){
					addErrorMsgToChat("",FNA_ARTUPLAN_RISKRATE_VALMSG ,page4,null);
				}
					
				}
			}//testing end
			
			if(!isEmpty(seesselfName.trim()) && (prodRecomClntNames.length > 0)){
				
				if(!prodRecomClntNames.includes(seesselfName.toUpperCase().trim())){
				    addErrorMsgToChat("span_prodrecomm_msg",FNA_ARTURECOM_SELF_PROD_DET ,page4,null);
					$("#badgeAddProdRecomm").trigger("click");
				}
			}
			
			if(!isEmpty(sessspsname.trim()) && (prodRecomClntNames.length > 0)){
				
				if(!prodRecomClntNames.includes(sessspsname.toUpperCase().trim())){
					addErrorMsgToChat("span_prodrecomm_msg",FNA_ARTURECOM_SPS_PROD_DET ,page4,null);
					$("#badgeAddProdRecomm").trigger("click");
				}
			}
		}
	
		
	

	$("#LifeILPNewPurchase").prop("checked",true);
	$("#validation4").removeClass("err-fld");

	if(page_wise_err)return false;
	return true;

}
function validateFnaSwitchReplacePlantDet(flag) {
	
	//alert("testing fun inside")
	var prodRecomSwtchClntNames = [];
		var page4 = "productSwitchReplace";	
		
		var totalprods = SWREPPLANDET_DATA.length;
		if(totalprods == 0){
			 addErrorMsgToChat("span_prodrecommSwtch_msg",FNA_SWREPPLAN_SELF_PROD_DET ,page4,null);
			  $("#btnAddRecommDetls").trigger("click");
		}else{
			 
			var advrecSwtchplanData = SWREPPLANDET_DATA;
			
			for ( var advrec in advrecSwtchplanData) {
				
				var planOpt = advrecSwtchplanData[advrec]["txtFldFnaSwrepPlanOpt"];
				var prodType = advrecSwtchplanData[advrec]["selFnaSwrepPlanPrdtType"]
				var prinName = advrecSwtchplanData[advrec]["selFnaSwrepPlanCompName"]
				var riskrate = advrecSwtchplanData[advrec]["selSwrepPpRiskRate"];
				var ClntName = advrecSwtchplanData[advrec]["txtFldSwrepPpName"];
				
				prodRecomSwtchClntNames.push(ClntName.trim());
			
				
				if(isEmpty(prodType)){
					addErrorMsgToChat("",FNA_SWREPPLANPRDTTYPE_VALMSG ,page4,null);
				}
				
				if(isEmpty(prinName)){
					addErrorMsgToChat("",FNA_SWREPPLAN_COMP_VALMSG ,page4,null);
				}
				
				
				if(prodType == "ILP"){
					
					if(isEmpty(riskrate)){
					addErrorMsgToChat("",FNA_SWREPPLAN_RISKRATE_VALMSG ,page4,null);
				}
					
				}
			}//testing end
			
			if(!isEmpty(seesselfName.trim()) && (prodRecomSwtchClntNames.length > 0)){
				
				if(!prodRecomSwtchClntNames.includes(seesselfName.toUpperCase().trim())){
				    addErrorMsgToChat("span_prodrecommSwtch_msg",FNA_SWREPPLAN_SELF_PROD_DET ,page4,null);
					$("#btnAddRecommDetls").trigger("click");
				}
			}
			
			//poovathi commented this function on 18-11-2021
			/*if(!isEmpty(sessspsname.trim()) && (prodRecomSwtchClntNames.length > 0)){
				
				if(!prodRecomSwtchClntNames.includes(sessspsname.toUpperCase().trim())){
					addErrorMsgToChat("span_prodrecommSwtch_msg",FNA_SWREPPLAN_SPS_PROD_DET ,page4,null);
					$("#btnAddRecommDetls").trigger("click");
				}
			}*/
			
		}
	
		$("#LifeILPSwtchReplace").prop("checked",true);
//		$("#validation9").removeClass("err-fld");

	if(page_wise_err)return false;
	return true;

}



function validAdvAndRecmChkboxes() {
	
	var page4 = "productRecommend";	
	
	var prodObject = isEmpty(FNA_TABLE_DATA.prodrecObjectives) ? "{}" : JSON.parse(FNA_TABLE_DATA.prodrecObjectives);
	
	//if(!isJsonObjEmpty(prodObject) && prodObject.ALLOBJ != "Y" && prodObject.PRO != "Y" &&  prodObject.INV != "Y" && prodObject.RET != "Y" && prodObject.HIN != "Y" && prodObject.SAV != "Y"){
	if(!isJsonObjEmpty(prodObject) && (prodObject.ALLOBJ != "Y" && prodObject.PRO != "Y" &&  prodObject.INV != "Y" && prodObject.RET != "Y" && prodObject.HIN != "Y" && prodObject.SAV != "Y")){
//		showObjSecError();
		addErrorMsgToChat("prodrecObjectives",ADNT_INCMPROTECT_VAL_MSG ,page4,"chkProdObject");
		$("#badgeAddProdRecomm").trigger("click");
	}


//	$("#LifeILPNewPurchase").prop("checked",true);
//	$("#validation4").removeClass("err-fld");
//	hideObjSecError();
	
	if(page_wise_err)return false;
	$("#divProdRecomFrm").find("div.inerrfld").remove()
	//hideObjSecError();
	 $("#objSecError").html("");
	return true;
}// end of validAdvAndRecmChkboxes



function validateFnaSwrepPlantDet(flag) {
	
	var page4="productSwitchRecomm"
		
		var question1 = FNA_TABLE_DATA.swrepConflg;
		var question2 = FNA_TABLE_DATA.swrepAdvbyflg;
		var question3 = FNA_TABLE_DATA.swrepDisadvgflg;
		var question4 = FNA_TABLE_DATA.swrepProceedflg;
		
		//q1
		if(isEmpty(question1)){
			addErrorMsgToChat("swrepConflgY",SWREPCONFLGQ1,page4,null);
			}
		
		//q2
		if(isEmpty(question2)){addErrorMsgToChat("swrepAdvbyflgY",SWREPADVBYFLGQ2,page4,null);}
		
		//q3
		if(isEmpty(question3)){addErrorMsgToChat("swrepDisadvgflgY",SWREPDISADVGFLGQ3,page4,null);}
		
		//q4
		if(isEmpty(question4)){addErrorMsgToChat("swrepProceedflgY",SWREPPROCEEDFLGQ4,page4,null);}
		
		if(page_wise_err)return false;
	    $("#LifeILPSwtch").prop("checked",true);
		$("#validation5").removeClass("err-fld");
	return true;

}

function validateReasonforRecomm(flag) {
	
	var page4 = "adviceBasisRecomm";	

	var resnForRecom = FNA_TABLE_DATA.advrecReason;//document.getElementById('advrecReason');
	var resnForRecom1 = FNA_TABLE_DATA.advrecReason1;//document.getElementById('advrecReason1');
	var resnForRecom2 = FNA_TABLE_DATA.advrecReason2;//document.getElementById('advrecReason2');
	//var  resnForRecom3= FNA_TABLE_DATA.advrecReason3;
	if(flag){

		if(isEmpty(resnForRecom)){
				addErrorMsgToChat("advrecReason",ADNT_RESNRECOM_VAL_MSG ,page4,null);
		}
		
		if(isEmpty(resnForRecom1)){
					addErrorMsgToChat("advrecReason1",ADNT_RESNRECOM1_VAL_MSG ,page4,null);
		}
		
		if(isEmpty(resnForRecom2)){
					addErrorMsgToChat("advrecReason2",ADNT_RESNRECOM2_VAL_MSG ,page4,null);
		}
		
		/*if(isEmpty(resnForRecom3)){
					addErrorMsgToChat("advrecReason3",ADNT_RESNRECOM3_VAL_MSG ,page4,null);
		}*/

	}
		
		
		/*$("#AdvBscRecomm").prop("checked",true);
	$("#validation6").removeClass("err-fld");*/

	if(page_wise_err)return false;
	return true;
}


function validateClntConsent() {
	
	var page4="clientDeclaration"

	var clientack = isEmpty(FNA_TABLE_DATA.cdackAgree) ? "" : (FNA_TABLE_DATA.cdackAgree)//.toUpperCase();
	var clientackremarks = FNA_TABLE_DATA.cdackAdvrecomm;
	var  clientconsent = FNA_TABLE_DATA.cdConsentApprch;
	

		if(isEmpty(clientack)){
				addErrorMsgToChat("radFldackAgree",CLNT_ACKN_VAL_MSG  ,page4,"cdackAgree");
//				$('body').scrollTo('#radFldackAgree');
		}// end of if
	
	
	if(clientack == "partial" && isEmpty(clientackremarks)){
				//addErrorMsgToChat("cdackAdvrecomm",CLNT_CHOICE_REPRECOMM_VAL_MSG  ,page4,"cdackAdvrecomm");
	}
	if(isEmpty(clientconsent)){
				addErrorMsgToChat("span_cdConsentApprch",CLNT_CONSENT_VAL_MSG  ,page4,"cdConsentApprch");
		}else{
			removeErrFld($("#span_cdConsentApprch"));
		}
	
	
	
	/*$("#ClntDecl").prop("checked",true);
	$("#validation7").removeClass("err-fld");*/
	
	if(page_wise_err)return false;
	return true;
}// end of validateClntConsent

function validateKycIntro() {
	
	var page4="kycIntro"

	var clientack = isEmpty(FNA_TABLE_DATA.ccRepexistinvestflg) ? "" : (FNA_TABLE_DATA.ccRepexistinvestflg)//.toUpperCase();

			if(isEmpty(clientack)){
				
				//addErrorMsgToChat("ccRepexistinvestflg",CLNT_ACKN_VAL_MSG  ,page4,"ccRepexistinvestflggrp");
				//$("#wizard-t-2").trigger("click")
				
				var currentPage      = window.location.pathname;
				currentPage = currentPage.substr(currentPage.lastIndexOf("/")+1) ;
				
//				console.log(currentPage,page,"currpage and page",field)
				if(currentPage != page4){
					creatSubmtDynaForm(page4+"?param1=ccRepexistinvestflg");
					//window.location.href=page;
				} 
				
				

				
		}// end of if
	
	
	if(page_wise_err)return false;
	return true;
}// end of validateClntConsent


function advDeclrAndSupReview(flag) {

var page4="managerDeclaration";
	var servAdvId = "";//$("#txtFldClntServAdv").val();
	var servAdvMgrId = "" ;//$("#txtFldMgrId").val();
	
	if (LOG_USER_MGRACS == "Y" && LOG_USER_ADVID == servAdvMgrId && flag) {
		
		var radSupReview = FNA_TABLE_DATA.suprevMgrFlg;

			if(isEmpty(radSupReview)){
				addErrorMsgToChat("suprevMgrFlg",SUPR_REVIEW_VAL_MSG  ,page4,null);
		}// end of if

		if (radSupReview == "N") {
			var suprRevFld = FNA_TABLE_DATA.suprevFollowReason;

			if(isEmpty(suprRevFld)){
				addErrorMsgToChat("suprevFollowReason",SUPRREVW_RESN_VAL_MSG  ,page4,null);
			}
		}// end of if
		
	}

		

	$("#AdvMngrDecl").prop("checked",true);
	$("#validation8").removeClass("err-fld");
	
	if(page_wise_err)return false;
	return true;
}// end of advDeclrAndSupReview


function validateFnaWellnesReview(flag){
var page4 = "finanWellReview";
	var depntSec = FNA_TABLE_DATA.fwrDepntflg;//$("#fwrDepntflg").val();
	var finCommSec =FNA_TABLE_DATA.fwrFinassetflg; //$("#fwrFinassetflg").val();

	if(!isEmpty(depntSec) && depntSec == "Y" ){
		if(FNADEPNT_TABLE_DATA.length == 0){
			addErrorMsgToChat("span_fwrDepntflg",FNA_DEPENT_NODET  ,page4,null);
		} 

	} 
	$("#FinWell").prop("checked",true);
	$("#validation3").removeClass("err-fld");
	
	if(page_wise_err)return false;
	return true;
}



function setJsonObjToElm(cont, data) {
	var val = JSON.stringify(data);
	$('input[name="' + cont + '"]').val("");
	if (!isEmpty(data) && !jQuery.isEmptyObject(val)) {
		$('input[name="' + cont + '"]').val(val);
		$.each(data, function(obj, val) {
			if (val == 'Y')
				$('input[data="' + obj + '"]').prop("checked", true);
			else
				$('input[data="' + obj + '"]').prop("checked", false);
		});
	}
}


function ajaxCall(parameter) {
	

	var jsnResponse = "";

	$.ajax({
		type : "POST",
		url : "KYCServlet",
		data : parameter,
		dataType : "json",

		async : false,

		beforeSend : function() {
			$('#cover-spin').show(0);
		},

		success : function(data) {
			//console.log(data)
			jsnResponse = data;
		},
		complete : function() {
			$('#cover-spin').hide(0);
		},
		error : function() {
			$('#cover-spin').hide(0);
		}
	});

	return jsnResponse;
}

var FNASSDET_TABLE_DATA ={}, FNA_TABLE_DATA={},FATCATAXDET_DATA=[],FATCATAXDETSPS_DATA=[],FNADEPNT_TABLE_DATA=[],ADVRECPRDTPLAN_DATA=[],SWREPPLANDET_DATA=[];

function fetchAllPageData(){
	
	
	FNASSDET_TABLE_DATA ={}, FNA_TABLE_DATA={},FATCATAXDET_DATA=[],FATCATAXDETSPS_DATA=[],FNADEPNT_TABLE_DATA=[],ADVRECPRDTPLAN_DATA=[],SWREPPLANDET_DATA=[]
	
	var fnaId = $("#hTxtFldCurrFNAId").val();
	
	var parameter = "dbcall=VER&FNA_ID=" + fnaId;
	var respText = ajaxCall(parameter);

//	console.log(JSON.stringify(respText));
	
	var retval = respText;
	
	for ( var val in retval) {
		var tabdets = retval[val];
		
		
		/*if (tabdets["SESSION_EXPIRY"]) {
			window.location = baseUrl + "sessionExpired";
			return;
		}*/
		
		
		
		for ( var tab in tabdets) {
			if (tabdets.hasOwnProperty(tab)) {
				var value = tabdets[tab];
				
			
				if (tab == "ARCH_FNA_TABLE_DATA") {
					FNA_TABLE_DATA = value;
				}
				if (tab == "ARCH_FNASSDET_TABLE_DATA") {
				
					FNASSDET_TABLE_DATA = value;
						
				}
				
				if (tab == "ARCH_FATCATAXDET_DATA") {
					FATCATAXDET_DATA = value;
				}
				if(tab == "ARCH_FATCATAXDETSPS_DATA"){
					FATCATAXDETSPS_DATA= value;
				}
				
				if(tab == "ARCH_FNADEPNT_TABLE_DATA"){
					FNADEPNT_TABLE_DATA=value;
				}
				
				if (tab == "ARCH_ADVRECPRDTPLAN_DATA") {

					ADVRECPRDTPLAN_DATA = value;
				}
				
				if (tab == "ARCH_SWREPPLANDET_DATA") {

					SWREPPLANDET_DATA = value;
				}
				
				
				
			}
		}
		
	}
	
}

function sendMailtoManger(){
		
		var fnaid = $("#hTxtFldCurrFNAId").val();
		var ntuccaseid  = $("#hTxtFldCurrCustId").val();
		var ntucexist =  false;//chkAnyNTUCExist();
		var flag = false;
		
		setTimeout(function() { loaderBlock() ;}, 0);	
	   	setTimeout(function() {  triggerMgrEmail(fnaid, ntuccaseid, ntucexist) ;}, 1000);
		
	}
	
	
function loaderBlock() {
	
	$('#cover-spin').show(0);
}


function loaderHide() {
	
	$('#cover-spin').hide(0);

}


function triggerMgrEmail(fnaid,ntuccaseid,ntucexist){
		
		
		var parameter = "dbcall=NONNTUCMGRMAIL&strFNAId=" + fnaid + "&ntuccaseid="+ ntuccaseid + "&ntucexist="+ntucexist;

		var retval = ajaxCall(parameter);

		for ( var val in retval) {

			var tabdets = retval[val];

			if (tabdets["SESSION_EXPIRY"]) {
				//window.location = baseUrl + SESSION_EXP_JSP;
				return;
			}

			for ( var tab in tabdets) {
				if (tabdets.hasOwnProperty(tab)) {
					var key = tab;
					var value = tabdets[tab];
					
					if(key == "SEND_MAIL_FAILURE" || key =="SEND_MAIL_SUCCESS"){
                           /**
	                        * Changed/added by <vignesh S.B> on 04-05-2021
	                        * To Show SUCCESS or FAILURE Message
	                        */
                            if(key =="SEND_MAIL_SUCCESS"){
                            	 
                            	alert(value)
	                      
	                             /*Swal.fire({
	                                  icon: 'success',
	                                  title: 'Mail has been Sent Successfully',
	                                  showConfirmButton: false,
	                                  //timer:2000
	                                }) */
	                                
	                        }
	                        
	                        if(key =="SEND_MAIL_FAILURE"){
	                            /*Swal.fire({
	                                  icon: 'warning',
	                                  title: 'Error in Sending the Mail. Contact Administrator',
	                                  showConfirmButton: false,
	                                  //timer:2000
	                                })*/
	                        	alert(value)
	                        }
						

						toastr.clear($('.toast'));
						toastr.options = {  timeOut: "5000", extendedTimeOut: "1000"};
						toastr["success"](value);
						//swal.close();
//						strPageValidMsgFld ="";
						
						var cfnaId=$("#hdnSessFnaId").val();
				    	Cookies.remove(cfnaId, { path: '/' })
				    	
				    	var currentPage      = window.location.pathname;
						currentPage = currentPage.substr(currentPage.lastIndexOf("/")+1) 
						creatSubmtDynaForm(currentPage+"?param1=");
						
//						window.location.reload(true);
						//var loadMsg = document.getElementById("loadingMessage");
//						loadMsg.style.display = "none";
					}
					
					if(key == "MGR_EMAIL_SENT_FLG"){
						$("#hTxtFldSentToMgr").val(value);
						$("#hTxtFldMgrAppSts").val("");
						var ntucexist = false//chkAnyNTUCExist();
						var sentsts = $("#hTxtFldSentToMgr").val();
						if(sentsts == "Not Sent" && !ntucexist){
							$("#btnMgrApprResend").prop("disabled",false);
							$("#btnMgrApprResend").prop("value","Send Now!");
						}else{
							$("#btnMgrApprResend").prop("disabled",true);	
						}
						
//						window.location.reload(true);
					}
				}
			}
		}
		
	}


function globalSearchClient(){
	var clientname = $("#txtFldGlblSrchClient").val();
	var loggedAdvId = $("#hTxtFldLoggedAdvId").val();
		
	
	$.ajax({
            url:baseUrl+"/customerdetails/getAllClients/"+loggedAdvId,
            type: "GET",
            dataType: "json",
			async:false,
            contentType: "application/json",
            data : {"clientname":clientname},
            success: function (data) {
	            for (var i=0; i<data.length; i++) {
	            	
	            	col0=data[i].custName;
	            	col1=data[i].dob;
	            	col2=data[i].resHandPhone;
	            	col3=data[i].custid;
	            	//searchtable.row.add( [col0,col1,col2,col3] ).draw( false );
	            	
              }
            },
            error: function(xhr,textStatus, errorThrown){
	 			//$('#cover-spin').hide(0);
	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			}
      });
	
	
	
}

function focusToField(page,field){
	var currentPage      = window.location.pathname;
	currentPage = currentPage.substr(currentPage.lastIndexOf("/")+1) ;
	
//	console.log(currentPage,page,"currpage and page",field)
	if(currentPage != page){
		creatSubmtDynaForm(page+"?param1="+field);
		//window.location.href=page;
	} 
	if(!isEmpty(field)){
		
		var tag="";var nodetype ="";var elemtof = null;
		if( $("#"+field) .length) {
			
			tag = $("#"+field).prop("tagName").toUpperCase();
			if(tag != "SPAN"){nodetype = $('#'+field).prop('type').toUpperCase();}
			elemtof = $('#'+field);
		}else {
			tag = $("."+field).prop("tagName").toUpperCase();
			if(tag != "SPAN"){nodetype = $('.'+field).prop('type').toUpperCase();}
			elemtof = $('.'+field);
		}
		
			
			if(tag != "SPAN"){
				
				if(nodetype == "HIDDEN" || nodetype == "RADIO" || nodetype=="CHECKBOX"){
					
					$(elemtof).closest(".card-body").find(":input:eq(0)").focus()
					$(elemtof).closest(".card-body").addClass("err-fld")


				}
			}
			
		
		
		if($("#"+field).length){
			
//			console.log("------> "+field +","+$("#"+field).closest("div.tab-pane").prop("id"))
			var tabid = $("#"+field).closest("div.tab-pane").prop("id");
			
			if(!isEmpty(tabid)) {
				var clz = field.indexOf("dfSelf") == 0 ? "dfself" : "dfsps";//temp solu
				$('a.'+clz).trigger("click");
				$("#"+field).focus();
			}
			
			
			
			$("#"+field).focus();
			
			
		}	
	}
	
	
}


function calcAge(dob) {
	var age = 0;
	if (!isEmpty(dob)) {

		var birthday = dob.split("/")[2] + "-" + dob.split("/")[1] + "-"+ dob.split("/")[0];
		var now = new Date();
		var past = new Date(birthday);

		age = now.getFullYear() - past.getFullYear();


	}
	return age;
}


function creatSubmtDynaReportPDF(href){

	var lex1 = href.split('?');
	var action= lex1[0];
	var qstr = lex1[1];
	var obj = " ";

	var formId = document.getElementById("hiddenForm");
	var divId = document.getElementById("dynamicFormDiv");

	if(formId){
		if(divId)
			formId.removeChild(divId);
	}

	var newdiv = document.createElement('div');
	newdiv.id = "dynamicFormDiv";

	if(qstr != null) {
		var params = qstr.split('&');
		for(var p=0;p< params.length;p++){
			var keyValue = params[p].split('=');
			var name = keyValue[0];
			var value = keyValue[1];

			if(value.indexOf("\'") != -1)
				value = value.replace("\'","\'");

			obj += '<input type="hidden" name="'+name+'" value="'+value+'"/>';

		}
	}
	newdiv.innerHTML = obj;

	if(document.getElementById("hiddenForm"))
			document.getElementById("hiddenForm").appendChild(newdiv);

	document.forms["hiddenForm"].action=action;
	document.forms["hiddenForm"].method="POST";

	var wind = window.open('', 'TheWindowBIRT',"channelMode,toolbar=no,scrollbars=no,location=no,resizable =yes");
	document.getElementById("hiddenForm").submit();
	wind.document.title = 'FNA Form Preview/Print';

}//end creatSubmtDynaReport



   function generateFNAPDF(){
	   
  
	   saveCurrentPageData($("#btnNextFormUniq"),$("#applNextScreen").val(),false,false);

	var fnaid = $("#hTxtFldCurrFNAId").val();
	
	var custid = $("#hTxtFldCurrCustId").val();
	
	var custLobDets = "ALL";
	
	var formtype = "SIMPLIFIED";

	var machine = "";
	
	
	machine = BIRT_URL + "/frameset?__report=" + FNA_RPT_FILELOC + "&__format=pdf" 
			+ "&P_CUSTID=" + custid + "&P_FNAID=" + fnaid
			+ "&P_PRDTTYPE=" + custLobDets+ "&P_CUSTOMERNAME=&P_CUSTNRIC=&P_FORMTYPE=" + formtype+ "&P_DISTID=DIS000000001" ;

	creatSubmtDynaReportPDF(machine);
	 

}


function creatSubmtDynaForm(href){	
		var lex1 = href.split('?');
		var action= lex1[0];
		var qstr = lex1[1];
		var obj = " ";

		var newdiv = document.createElement('div');
		
		if(qstr != null) {
			var params = qstr.split('&');
			for(var p=0;p< params.length;p++){
				var keyValue = params[p].split('=');
				var name = keyValue[0];
				var value = keyValue[1];
				obj += "<input type='hidden' name='"+name+"' value='"+value+"'/>";
				
			}
		}
		newdiv.innerHTML = obj;
		if(document.getElementById("validationForm"))
				document.getElementById("validationForm").appendChild(newdiv);		
		
document.getElementById("validationForm").action=action;
document.getElementById("validationForm").method="POST";
document.getElementById("validationForm").submit();	
}//end creatSubmtDynaForm

function chkOptionExists(selectid,value){
	var exists = false; 
$('#'+selectid+'  option').each(function(){
	if(!isEmpty(value)) {
		if (this.value.toUpperCase() == value.toUpperCase()) {
		    return exists = true;
		  }	
	}
  
});
return exists;
}

function addOption(selectid,optkey,optvalue) {
	 $('#'+selectid)
     .append($("<option></option>")
                .attr("value", optkey)
                .text(optvalue)); 
}

function removeOptionByVal(selectid,value) {
	
	var sourceSelect = document.getElementById(selectid);
	
	for(var opt=0;opt<sourceSelect.options.length;opt++){
		if(sourceSelect.options[opt].value.toUpperCase() == value.toUpperCase()) {
			sourceSelect.remove(opt);
		}
	}
	
	/*$('#'+selectid +' option[value='+value+']').each(function() {
	    $(this).remove();
	});*/
}

   function isEmptyObj(obj) {
   	      return Object.keys(obj).length === 0;
   }
   
   // open search screen wihle click seach icon in header
   
   function openSrchScreen(){
	   window.location.href = "index"
   }




function addErrorMsgToChat(elemId,message,pagetonav,elemname){
	
	if(!isEmpty(elemId)){
		
		var spanId = "spanerr"+elemId;
	
		focusToField(pagetonav,elemId);
	
		makeErrMsg(elemId,"errorclasss",message,pagetonav);
		
		var elem = '<li class="other" id="'+spanId+'" onclick=focusToField("'+pagetonav+'","'+elemId+'")>'+message +' </li>'
		
		
		//console.log(elemId,message,pagetonav,spanId,elem,elemname,"***")
		if($("#"+elemId).length) {
		
			$("#"+elemId).bind("change",function(){
				removeErrFld($(this));
				$("#"+spanId).remove();
				
				
				if ( $('#ekycValidMessageList li').length == 0 ) {
					$("#btntoolbarsign").trigger("click");
					page_wise_err=false;
				}
				
			});
			
		}else if($("."+elemId).length) {
		
			$("."+elemId).bind("change",function(){
				removeErrFld($(this));
				$("#"+spanId).remove();
				
				
				if ( $('#ekycValidMessageList li').length == 0 ) {
					$("#btntoolbarsign").trigger("click");
					page_wise_err=false;
				}
				
			});
			
		}
		
		
		if(!isEmpty(elemname)){
			
			var nodetype = "";
			if(($('input[name="'+elemname+'"]').length)) {
				nodetype = $('input[name="'+elemname+'"]').prop('type').toUpperCase() 
			}else {
				nodetype = $('.'+elemname).prop('type').toUpperCase(); 
			}
			 
			
			if(nodetype == "RADIO" || nodetype == "CHECKBOX" ){
				$(':input[name="'+elemname+'"]').bind("click",function(){
					removeErrFld($(this));
					$("#"+spanId).remove();		
					
					if ( $('#ekycValidMessageList li').length == 0 ) {
						$("#btntoolbarsign").trigger("click");
						page_wise_err=false;
					}
				
				});
				
				$('.'+elemname).bind("click,",function(){
					removeErrFld($(this));
					$("#"+spanId).remove();		
					
					if ( $('#ekycValidMessageList li').length == 0 ) {
						$("#btntoolbarsign").trigger("click");
						page_wise_err=false;
					}
				
				});	
				
			}else{
				$(':input[name="'+elemname+'"]').bind("change",function(){
					removeErrFld($(this));
					$("#"+spanId).remove();	
					
					if ( $('#ekycValidMessageList li').length == 0 ) {
						$("#btntoolbarsign").trigger("click");
						page_wise_err=false;
					}		
				});	
				
				$('.'+elemname).bind("change",function(){
					removeErrFld($(this));
					$("#"+spanId).remove();	
					
					if ( $('#ekycValidMessageList li').length == 0 ) {
						$("#btntoolbarsign").trigger("click");
						page_wise_err=false;
					}		
				});	
			}
		}
		
		
		 if ($("#ekycValidMessageList").find('li#'+spanId).length == 0) {
			$("#ekycValidMessageList").append(elem);
			
		} 
	}else{
		var spanId = "span_"+message;
		 spanId = spanId.replace(/[&\/\\,+()$~%.'":*?<>{}]/g, '_');	 	
	
		focusToField(pagetonav,elemId);
	
		makeErrMsg(elemId,"errorclasss",message,pagetonav);
		
		var elem = '<li class="other" id="'+spanId+'">'+message +' </li>'
		 if ($("#ekycValidMessageList").find('li#'+spanId).length == 0) {
			$("#ekycValidMessageList").append(elem);
		}
			if(!isEmpty(elemname)){
			var nodetype = $('input[name="'+elemname+'"]').prop('type').toUpperCase();
			
			if(nodetype == "RADIO" || nodetype == "CHECKBOX" ){
				$(':input[name="'+elemname+'"]').bind("click",function(){
					removeErrFld($(this));
					$("#"+spanId).remove();		
					
					if ( $('#ekycValidMessageList li').length == 0 ) {
						$("#btntoolbarsign").trigger("click");
						page_wise_err=false;
					}
				
				});	
			}else{
				$(':input[name="'+elemname+'"]').bind("change",function(){
					removeErrFld($(this));
					$("#"+spanId).remove();	
					
					if ( $('#ekycValidMessageList li').length == 0 ) {
						$("#btntoolbarsign").trigger("click");
						page_wise_err=false;
					}		
				});	
			}
			
			
		}
		
	}
	
		
		page_wise_err=true;
		setPageKeyInfoFail()	
		openElement();
		
	
}


function removeTblRows(tblId) {

	var tBodyObj = document.getElementById(tblId).tBodies[0];
	var len = tBodyObj.rows.length;
	if (len > 0) {
		for (var i = len; i > 0; i--) {
			tBodyObj.removeChild(tBodyObj.rows[0]);
		}
	}
}


//enable note Contents
function enableNoteContent(thisFldId){
	
	  if(thisFldId.trim()== "advrecReason"){
//	    	$("#Note").trigger("mouseover");
//	    	$("#Note2,#Note1,#Note3,#NoteDummy").trigger("mouseout");
	    }
	    
	    if(thisFldId.trim()== "advrecReasonDUMMY"){
	    	$("#NoteDummy").trigger("mouseover");
	    	$("#Note1Dummy,#Note2Dummy,#Note3Dummy,#Note").trigger("mouseout");
	    }
	    
	    if(thisFldId.trim()== "advrecReason1"){
//	    	$("#Note2").trigger("mouseover");
//	    	$("#Note,#Note1,#Note3,#Note2Dummy").trigger("mouseout");
	    }
	    
	    if(thisFldId.trim()== "advrecReason1DUMMY"){
	    	$("#Note2Dummy").trigger("mouseover");
	    	$("#NoteDummy,#Note1Dummy,#Note3Dummy,#Note2").trigger("mouseout");
	    }
	    
	    if(thisFldId.trim()== "advrecReason2"){
//	    	$("#Note1").trigger("mouseover");
//	    	$("#Note,#Note2,#Note3,#Note1Dummy").trigger("mouseout");
	    }
	    
	    if(thisFldId.trim()== "advrecReason2DUMMY"){
	    	$("#Note1Dummy").trigger("mouseover");
	    	$("#NoteDummy,#Note2Dummy,#Note3Dummy,#Note1").trigger("mouseout");
	    }
	    
	    if(thisFldId.trim()== "advrecReason3"){
//	    	$("#Note3").trigger("mouseover");
//	    	$("#Note,#Note2,#Note1,#Note3Dummy").trigger("mouseout");
	    }
	    
	    if(thisFldId.trim()== "advrecReason3DUMMY"){
	    	$("#Note3Dummy").trigger("mouseover");
	    	$("#NoteDummy,#Note1Dummy,#Note2Dummy,#Note3").trigger("mouseout");
	    } 
   }//end of enable note contents


       

function setPageManagerSts(){
	//clr sign Status for new fna poovathi add on 02-06-2021
    resetSignStatus();
    
	var mgrkycSentStatus = $("#kycSentStatus").val();
	var mgrapproveStatus = $("#mgrApproveStatus").val();
	var mgrapprovestaff =$("#mgrapprstsByadvstf").val();
	var mgrapprovedate = $("#mgrApproveDate").val();
	
	var dynatext = " on <strong>" + mgrapprovedate +" </strong> by <strong> "+mgrapprovestaff +"</strong>";
	var dynatextSimple = " on " + mgrapprovedate +"   by   "+mgrapprovestaff ;
		if(mgrkycSentStatus == "YES"){
			if(isEmpty(mgrapproveStatus)){
				
				$("#applValidMessage").html('<small><i class="fa fa-info-circle"></i>&nbsp;<span id="sgnStsMsg">This FNA already sent to manager and awaiting for approval.</span><br/><i class="fa fa-exclamation-triangle"></i>&nbsp;In case of any changes required in FNA Form, re-signature is required</small>')
				$("#applValidMessage").removeClass("alert-default").addClass("alert-warning")
				
				$("#btnMngrApproval").addClass("fa-question").removeClass("fa-remove");
				$("#btnMngrApproval").closest("button").addClass("btn-info").removeClass("btn-danger");
				$("#hTxtFldSignedOrNot").val("Y");
				
			}else if(!isEmpty(mgrapproveStatus) && mgrapproveStatus =="APPROVE"){
				
				$("#applValidMessage").html('<small><i class="fa fa-info-circle"></i>&nbsp;<span id="sgnStsMsg">This FNA approved by manager '+dynatext+'</span></small>')
				$("#applValidMessage").removeClass("alert-default").addClass("alert-warning");
				
				
				$("#btnMngrApproval").addClass("fa-check").removeClass("fa-remove");
	 			$("#btnMngrApproval").closest("button").removeAttr("disabled");
				$("#btnMngrApproval").closest("button").addClass("btn-success").removeClass("btn-secondary").removeClass("btn-danger");
				$("#btnMngrApproval").parent().attr("title","Approved "+dynatextSimple);
				$("#hTxtFldSignedOrNot").val("Y");
			
			}else if(!isEmpty(mgrapproveStatus) && mgrapproveStatus =="REJECT"){
			//	alert("inside")
				$("#applValidMessage").html('<small><i class="fa fa-info-circle"></i>&nbsp;<span id="sgnStsMsg">This FNA rejected by manager.</span><a class="helptextlink" onclick="showManagerMailConfirm()">CLICK HERE </a> to Resend Manager Approval!<br/><i class="fa fa-exclamation-triangle"></i>&nbsp;In case of any changes required in FNA Form, re-signature is required</small>')
				$("#applValidMessage").removeClass("alert-default").addClass("alert-warning");
				
				$("#btnMngrApproval").addClass("fa-remove").removeClass("fa-question").removeClass("fa-check");
	 			$("#btnMngrApproval").closest("button").removeAttr("disabled");
				$("#btnMngrApproval").closest("button").addClass("btn-danger").removeClass("btn-secondary").removeClass("btn-success");
				$("#btnMngrApproval").parent().attr("title","Rejected "+dynatextSimple);
				$("#hTxtFldSignedOrNot").val("Y");
				
			} 
			
			
		}else{
			
			$("#btnMngrApproval").removeClass("fa-check").addClass("fa-question");
	 		//$("#btnMngrApproval").closest("button").removeAttr("disabled");
			$("#btnMngrApproval").closest("button").removeClass("btn-success").addClass("btn-secondary").removeClass("btn-danger");
			
			//$("#applValidMessage").html('<small><i class="fa fa-info-circle"></i>&nbsp;Signatures are captured. Adviser can send this FNA to <a style="cursor: pointer;color:blue" onclick="alert()">manager approval</a>!<br/></small>')
			//$("#applValidMessage").removeClass("alert-default").addClass("alert-secondary")
		}
}


function setPageAdminSts(){
	
	//clr sign Status for new fna poovathi add on 02-06-2021
    resetSignStatus();
    
	var mgrkycSentStatus = $("#adminKycSentStatus").val()
	var mgrapproveStatus = $("#adminApproveStatus").val()
			var mgrapprovestaff =$("#adminapprstsByadvstf").val();
			var mgrapprovedate = $("#adminApproveDate").val();
			
			var dynatext = " on <strong>" + mgrapprovedate +" </strong> by <strong> "+mgrapprovestaff +"</strong>";
			var dynatextSimple = " on " + mgrapprovedate +"  by  "+mgrapprovestaff ;
		if(mgrkycSentStatus == "YES"){
			
			
			if(isEmpty(mgrapproveStatus)){
				
				$("#applValidMessage").html('<small><i class="fa fa-info-circle"></i>&nbsp;<span id="sgnStsMsg">This FNA already sent to admin and awaiting for approval.</span><br/><i class="fa fa-exclamation-triangle"></i>&nbsp;In case of any changes required in FNA Form, re-signature is required</small>')
				$("#applValidMessage").removeClass("alert-default").addClass("alert-warning")
				
				$("#btnAdmnApproval").addClass("fa-question").removeClass("fa-remove");
				$("#btnAdmnApproval").closest("button").addClass("btn-info").removeClass("btn-danger");
				$("#hTxtFldSignedOrNot").val("Y");
				
			}else if(!isEmpty(mgrapproveStatus) && mgrapproveStatus =="APPROVE"){
				
				$("#applValidMessage").html('<small><i class="fa fa-info-circle"></i>&nbsp;<span id="sgnStsMsg">This FNA approved by admin '+dynatext+'.</span><br/><i class="fa fa-exclamation-triangle"></i>&nbsp;In case of any changes required in FNA Form, re-signature is required</small>')
				$("#applValidMessage").removeClass("alert-default").addClass("alert-warning");
				
				
				$("#btnAdmnApproval").addClass("fa-check").removeClass("fa-remove");
	 			$("#btnAdmnApproval").closest("button").removeAttr("disabled");
				$("#btnAdmnApproval").closest("button").addClass("btn-success").removeClass("btn-secondary").removeClass("btn-danger");
				$("#btnAdmnApproval").parent().attr("title","Approved "+dynatextSimple);
				$("#hTxtFldSignedOrNot").val("Y");
			
			}else if(!isEmpty(mgrapproveStatus) && mgrapproveStatus =="REJECT"){
				
				$("#applValidMessage").html('<small><i class="fa fa-info-circle"></i>&nbsp;<span id="sgnStsMsg">This FNA rejected by admin.</span><br/><i class="fa fa-exclamation-triangle"></i>&nbsp;In case of any changes required in FNA Form, re-signature is required</small>')
				$("#applValidMessage").removeClass("alert-default").addClass("alert-warning");
				
				$("#btnAdmnApproval").addClass("fa-remove").removeClass("fa-question").removeClass("fa-check");
	 			$("#btnAdmnApproval").closest("button").removeAttr("disabled");
				$("#btnAdmnApproval").closest("button").addClass("btn-danger").removeClass("btn-secondary").removeClass("btn-success");
				$("#btnAdmnApproval").parent().attr("title","Rejected "+dynatextSimple);
				$("#hTxtFldSignedOrNot").val("Y");
			} 
			
			
		}else{
			$("#btnAdmnApproval").removeClass("fa-check").addClass("fa-question");
		}
}



function setPageComplSts(){
	
	//clr sign Status for new fna poovathi add on 02-06-2021
      resetSignStatus();
	
	var mgrkycSentStatus = $("#compKycSentStatus").val()
	var mgrapproveStatus = $("#compApproveStatus").val();
	var mgrapprovestaff =$("#compapprstsByadvstf").val();
	var mgrapprovedate = $("#compApproveDate").val();
	
	var dynatext = " on <strong>" + mgrapprovedate +" </strong> by <strong> "+mgrapprovestaff +"</strong>";
	var dynatextSimlple = " on " + mgrapprovedate +"  by  "+mgrapprovestaff;
		
		if(mgrkycSentStatus == "YES"){
			
			if(isEmpty(mgrapproveStatus)){
				
				$("#applValidMessage").html('<small><i class="fa fa-info-circle"></i>&nbsp;<span id="sgnStsMsg">This FNA already sent to Compliance and awaiting for approval.</span><br/><i class="fa fa-exclamation-triangle"></i>&nbsp;In case of any changes required in FNA Form, re-signature is required</small>')
				$("#applValidMessage").removeClass("alert-default").addClass("alert-warning")
				
				$("#btnCompApproval").addClass("fa-question").removeClass("fa-remove");
				$("#btnCompApproval").closest("button").addClass("btn-info").removeClass("btn-danger");
				$("#hTxtFldSignedOrNot").val("Y");
				
			}else if(!isEmpty(mgrapproveStatus) && mgrapproveStatus =="APPROVE"){
				
				$("#applValidMessage").html('<small><i class="fa fa-info-circle"></i>&nbsp;<span id="sgnStsMsg">This FNA approved by Compliance '+dynatext+'.</span><br/><i class="fa fa-exclamation-triangle"></i>&nbsp;In case of any changes required in FNA Form, re-signature is required</small>')
				$("#applValidMessage").removeClass("alert-default").addClass("alert-warning");
				
				$("#btnCompApproval").addClass("fa-check").removeClass("fa-remove");
	 			$("#btnCompApproval").closest("button").removeAttr("disabled");
				$("#btnCompApproval").closest("button").addClass("btn-success").removeClass("btn-secondary").removeClass("btn-danger");
				$("#btnCompApproval").parent().attr("title","Approved "+dynatextSimlple);
				$("#hTxtFldSignedOrNot").val("Y");
			
			}else if(!isEmpty(mgrapproveStatus) && mgrapproveStatus =="REJECT"){
				
				$("#applValidMessage").html('<small><i class="fa fa-info-circle"></i>&nbsp;<span id="sgnStsMsg">This FNA rejected by Compliance.</span><br/><i class="fa fa-exclamation-triangle"></i>&nbsp;In case of any changes required in FNA Form, re-signature is required</small>')
				$("#applValidMessage").removeClass("alert-default").addClass("alert-warning");
				
				$("#btnCompApproval").addClass("fa-remove").removeClass("fa-question").removeClass("fa-check");
	 			$("#btnCompApproval").closest("button").removeAttr("disabled");
				$("#btnCompApproval").closest("button").addClass("btn-danger").removeClass("btn-secondary").removeClass("btn-success");
				$("#btnCompApproval").parent().attr("title","Rejected "+dynatextSimlple);
				$("#hTxtFldSignedOrNot").val("Y");
				
			} 
			
			
		}else{
			$("#btnCompApproval").removeClass("fa-check").addClass("fa-question");
		}
}


//$("#btnAllPgeNoti").addClass("fa-check").removeClass("fa-remove");
//$("#btnAllPgeNoti").closest("button").removeAttr("disabled");
//$("#btnAllPgeNoti").closest("button").addClass("btn-success").removeClass("btn-secondary").removeClass("btn-danger");

function setPageKeyInfoSucs(){
	$("#btnAllPgeNoti").addClass("fa-check").removeClass("fa-remove");
	$("#btnAllPgeNoti").closest("button").removeAttr("disabled");
	$("#btnAllPgeNoti").closest("button").addClass("btn-success").removeClass("btn-secondary").removeClass("btn-danger");
	$("#applValidMessage").html('<small><img src="vendor/avallis/img/check.png" alt="completed">&nbsp;<span id="sgnStsMsg">Key Information verification is completed.</span></small>');
	$("#applValidMessage").removeClass("alert-default").addClass("alert-warning")
	
}

function setPageKeyInfoFail() {
    $("#btnAllPgeNoti").closest("button").addClass("btn-danger").removeClass("btn-secondary").removeClass("btn-success");
	$("#btnAllPgeNoti").closest("button").removeAttr("disabled");
	$("#btnAllPgeNoti").addClass("fa-remove").removeClass("fa-check");
	$("#applValidMessage").html("")
	$("#applValidMessage").removeClass("alert-default").removeClass("alert-warning")
	$("#hTxtFldSignedOrNot").val("N");
}

function setPageSignInfoSucs(){
	$("#btnSignStatus").addClass("fa-check").removeClass("fa-remove");
	$("#btnSignStatus").closest("button").removeAttr("disabled");
	$("#btnSignStatus").closest("button").addClass("btn-success").removeClass("btn-secondary").removeClass("btn-danger");
	$("#applValidMessage").html('<small><img class="ml-2" src="vendor/avallis/img/check.png" alt="completed" style="width: 3.8%;">&nbsp;<span id="sgnStsMsg">Signatures are captured.</span><br/><img src="vendor/avallis/img/infor.png" class="ml-2" alt="info">&nbsp;<a class="helptextlink" onclick="showManagerMailConfirm()">CLICK HERE</a> to send this FNA to manager approval!</small>');
	$("#applValidMessage").removeClass("alert-default").addClass("alert-warning")
	$("#hTxtFldSignedOrNot").val("Y");
}



function setPageSignInfoFail(){
	$("#btnSignStatus").removeClass("fa-check").addClass("fa-remove");
	$("#btnSignStatus").closest("button").attr("disabled",true);
	$("#btnSignStatus").closest("button").removeClass("btn-success").removeClass("btn-secondary").addClass("btn-danger");
	$("#applValidMessage").html('<small><img class="ml-2" src="vendor/avallis/img/remove.png"  alt="remove" style="width: 3.8%;">&nbsp;<span id="sgnStsMsg">Signatures are not yet captured.</span><br/><img src="vendor/avallis/img/infor.png" class="ml-2" alt="info">&nbsp;<a class="helptextlink" onclick="allPageValidation()">CLICK HERE</a> to verify and proceed to signature.</small>');
	$("#applValidMessage").removeClass("alert-default").addClass("alert-warning")
	$("#hTxtFldSignedOrNot").val("N");
}
				 		
				 	

function setToolbarSignBtnFail() {
	$("#btntoolbarsign").addClass("btn-bs3-danger");
	$("#btntoolbarsign").removeClass("btn-bs3-prime");
	$("#btntoolbarsign").find("i").removeClass("fa-pencil");
	$("#btntoolbarsign").find("i").addClass("fa-exclamation-circle");
}

function setToolbarSignBtnSucc() {
	$("#btntoolbarsign").removeClass("btn-bs3-danger");
	$("#btntoolbarsign").addClass("btn-bs3-prime");
	$("#btntoolbarsign").find("i").addClass("fa-pencil");
	$("#btntoolbarsign").find("i").removeClass("fa-exclamation-circle");
}

function chkNextChange() {
//	alert(strPageValidMsgFld)
	if(!isEmpty(strPageValidMsgFld)){
		setTimeout(function(){
			$('#cover-spin').show(0);
		}
		,100)
		
		setTimeout(function(){
			$("#btntoolbarsign").trigger("click");}
		,1200)
	}
	
}

//$(".checkdb").each(function(){

	
	
	
//	if(!isEmpty(isSigned) && isSigned == "Y") {

$(".checkdb").off().on("change,click",function(){
			
			var nodetype = $(this).prop('type').toUpperCase();
//			console.log("nodetype",nodetype,$(this).prop("name"))
			if(nodetype == "HIDDEN" || nodetype == "RADIO" || nodetype=="CHECKBOX"){
				
			}

			var isSigned = $("#hTxtFldSignedOrNot").val();
			if(isSigned == "Y") {
				
				var nodetype = $(this).prop('nodeName').toLowerCase();


				var chkanychange = $("#chkanychange");
				var jsonvalues = JSON.parse(isEmpty(chkanychange.val()) ? "{}" : chkanychange.val());

				var inputtype = $(this).prop("type").toLowerCase();
				if(inputtype ==  "text" || inputtype == "hidden" || inputtype == "select" || inputtype == "textarea" || nodetype == "select"){

					var newobj=jsonvalues;
					newobj[$(this).prop("name")]=$(this).val();
					chkanychange.val(JSON.stringify(newobj));
//		console.log(JSON.stringify(newobj),"<<<<<<<<<<<<<<<<<<<<<<<<<")
				}else if(inputtype == "radio" || inputtype == "checkbox"){
					if($(this).prop("checked")){
//						vararrGloabal.push(new Array($(this).prop("name"),$(this).val()))

						var newobj=jsonvalues;
						newobj[$(this).prop("name")]=$(this).val();
						chkanychange.val(JSON.stringify(newobj));
//						console.log(chkanychange.val(newobj),"<<<<<<<<<<<<<<<<<<<<<<<<<")
					}
				}
				
				if(!isEmpty(chkanychange.val())) {
					
					showConfrmAfterSignChng($("#btnNextFormUniq"),$("#applNextScreen").val(),false,false);
//					saveCurrentPageData($("#btnNextFormUniq"),$("#applNextScreen").val(),false,false);
				}
			}


		});
		
//	}
	

//});

function showConfrmAfterSignChng(obj,nextscreen,navigateflg,infoflag) {
	
	Swal.fire({
//  	  title: "Clear Signature",
  	  text: " By changing the data, you must capture sign again and redo the approval process!",
  	  icon: "info",
  	  showCancelButton: true,
  	  allowOutsideClick:false,
  	  allowEscapeKey:false,
  	  confirmButtonClass: "btn-danger",
  	  confirmButtonText: "OK",
  	  cancelButtonText: "No,Cancel",
//  	  closeOnConfirm: false,
  	  //showLoaderOnConfirm: true
//  	  closeOnCancel: false
  	}).then((result) => {
		  if (result.isConfirmed) {

			  var fnaId=$("#hTxtFldCurrFNAId").val();
			  Cookies.remove(fnaId, { path: '/' })
			  
			  saveCurrentPageData($("#btnNextFormUniq"),$("#applNextScreen").val(),false,false);
			  
			 
			  $.ajax({
				  url : baseUrl+'/FnaSignature/clearByFnaId/'+fnaId,
					 type: 'DELETE',
					 async:false,
					 success: function () {
						 
//						  doCurrentPageData(obj,nextscreen,navigateflg,infoflag);
			    		 window.location.reload(true);
						  
						  /*setPageKeyInfoFail();
						  setPageSignInfoFail();
						  setToolbarSignBtnFail();
						  
						  setPageManagerSts();
						  setPageAdminSts();
						  setPageComplSts();*/

						  
						 return true;
					 },
					 error: function(xhr,textStatus, errorThrown){
				 			//$('#cover-spin').hide(0);
				 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
					       return false;
			             }
			   });
		  }  else if ( result.dismiss === Swal.DismissReason.cancel) {
			  
			  window.location.reload(true);
		  }


});

}

//clear all errmessages in keyInformation PopUp chat window
function removeAllErrorMsgInChat(){
	$("#ekycValidMessageList").children().remove();
}

function removErrMsgInChat(spanId){
	$("#ekycValidMessageList").find('li#spanerr'+spanId).remove();
}

 
//Vignesh Add On 07-05-2021
function openURLPopup(crtElm){
	
	$('#cover-spin').show(0);
  
    var masterURl = baseUrl +'/viewDashBoardDetls';
	 
		var frame  =  "<iframe id='thedialog' width='100%' height='100%' frameborder='0' scrolling='yes' allowtransparency='true' src='"+masterURl+"'></iframe>"
		
		    $("#advMngrDshbrdDetlsMdl #cloneURLId").html(frame);
		    //$("#advMngrDshbrdDetlsMdl #cloneURLId").find("");
		    $('#advMngrDshbrdDetlsMdl').modal({
			  backdrop: 'static',
			  keyboard: true,
			  show:true
		});
		
		    $('#cover-spin').hide(0);
		 
}
//End


 

//poovathi add on 10-05-2021
function ajaxCommonError (err,errcde,errMsg){
	Swal.fire({
			  icon: 'error',
			  title: errcde +" - "+ err,
			  text: errMsg,
			  footer: '<a href>Please Contact Administrator?</a>'
			});
		}

//poovathi deleted this resetSignStatus function 0n 01-07-2021
//poovathi add on 02-06-21
function resetSignStatus(){
	
	var prevFnaId = $("#prevFnaId").val();
	var fnaIdFlg = $("#createNewFnaFlgs").val();
	
	if((fnaIdFlg == "true")&&(!isEmpty(prevFnaId))){
		
		//Clr Manager Sign Related Details
		 $("#kycSentStatus").val("");
		 $("#mgrApproveStatus").val("");
		 $("#mgrapprstsByadvstf").val("");
		 $("#mgrApproveDate").val("");
		 
		 //Clr Admin Sign Related Details
		 $("#adminKycSentStatus").val("");
		 $("#adminApproveStatus").val("");
		 $("#adminapprstsByadvstf").val("");
		 $("#adminApproveDate").val("");
		
		 //Clr Comp. Sign Related Details
		 $("#compKycSentStatus").val("");
		 $("#compApproveStatus").val("");
	     $("#compapprstsByadvstf").val("");
		 $("#compApproveDate").val("");
	}
}

function bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Byte';
	   var i = parseFloat(Math.floor(Math.log(bytes) / Math.log(1024)));
	   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i] + "(apprx.)";
	}

function showhidePrevArchKycDetls(secDiv){
	
	var advname = $("#hTxtFldLogUserId").val();
	var clntName = $("#hTxtFldCurrCustName").val();
	//var prevFnaId = $("#prevFnaId").val();
	var fnaIdFlg = $("#createNewFnaFlgs").val();
	var PrevFnaIDRemovFlg = $("#hTxtFldPrevFNAIdRemovFlg").val();
	var lat_FnaId = $("#hTxtFldLatFNAId").val();
	
	/*var curnt_FnaId = $("#hTxtFldCurrFNAId").val();*/
	var fullApprvekyc = $("#hTxtFldFullyApprvekyc").val();
	var latstAchvFna =  $("#hTxtFldLatArchvFna").val();
	var fullApprvekycFlg = "";
	
	
	
	/*if(isEmpty(lat_FnaId)){
		fullApprvekycFlg = true; 
	}
	
	if(PrevFnaIDRemovFlg == "Y"){
		fullApprvekycFlg = true; 
	}
	
	if(fnaIdFlg == "true"){
		fullApprvekycFlg = true; 
	}*/
	
	/*if(fullApprvekyc == "optgrpApproved"){
		fullApprvekycFlg = false; 
	}
	*/
	
	/*if(latstAchvFna == "(Last Saved eKYC)" && (fullApprvekyc == null)){
		fullApprvekycFlg = true;
	}*/
	
	
	if( fullApprvekyc == "optgrpApproved"){
		fullApprvekycFlg = true;
	}
	
	//alert(latstAchvFna + " " + fullApprvekyc);
	
	if( latstAchvFna != "(Last Saved eKYC)" && fullApprvekyc == "searchExistFnaList"){
		fullApprvekycFlg = true;
	}
	
	
	//PrevFnaIDRemovFlg == "Y" && fnaIdFlg != "true" && fullApprvekyc != "(Last Saved eKYC)"
	      if(fullApprvekycFlg == true){
		  $("#"+secDiv).addClass("disabledMngrSec");
          $("#btnAppUpload").addClass("hide");
          $("#btntoolbarsign").addClass("hide");
          $("#applValidMessage").addClass("disabledMngrSec");
          
          //poovathi add on 26-10-2021(show polEsub Button)
          //$("#btnPolEsub").addClass("show").removeClass("hide")
          
          $(".oldArchvnoteinfo").removeClass("hide").addClass("show");
          
          var sgnStsMsg = $("#sgnStsMsg").text();
          if(sgnStsMsg.length > 0){
        	  $(".oldArchvnoteinfo").html(' Hi<strong>&nbsp;'+advname+'&nbsp;,</strong>&nbsp;'+OLD_ARCH_TEMPLATE.replace("~STATUS~",sgnStsMsg)+'&nbsp; '); 
          }else{
        	  $(".oldArchvnoteinfo").html(' Hi<strong>&nbsp;'+advname+'&nbsp;,</strong>&nbsp;No further changes will be allowed,you can able to view Only.&nbsp; ');
          }
         
	}
	
/* 	if((fnaIdFlg == "true")&&(!isEmpty(prevFnaId))){
 		
 	}else{
 		//poovathi add on 25-08-2021
 		var lat_FnaId = $("#hTxtFldLatFNAId").val();
 		var curnt_FnaId = $("#hTxtFldCurrFNAId").val();
             //console.log(lat_FnaId,curnt_FnaId)
 		  if(!isEmpty(lat_FnaId) && !isEmpty(curnt_FnaId)){
 			 // console.log("lat_FnaId-----------",lat_FnaId)
 		   	  if(lat_FnaId != curnt_FnaId && lat_FnaId!="CreateNewFNA"){
 		   		  
 		   		  if(PrevFnaIDRemovFlg == "Y"){}else{
 		   			$("#"+secDiv).addClass("disabledMngrSec");
  		           $("#btnAppUpload").addClass("hide");
  		           $("#btntoolbarsign").addClass("hide");
  		           $("#applValidMessage").addClass("disabledMngrSec");
  		           $(".oldArchvnoteinfo").removeClass("hide").addClass("show");
  		           
  		        
  		           $(".oldArchvnoteinfo").html(' Dear<strong>&nbsp;'+advname+'&nbsp;,</strong>&nbsp;'+OLD_ARCH_TEMPLATE.replace("~CLIENT~", clntName)+'&nbsp; ');
 		   		  }
 		   		   
 		      }
 	     }
             
 	}*/
	
}

function chkPrevArchFnaDetls(){
	 
	/*var prevArchvFlg = false;
	var lat_FnaId = $("#hTxtFldLatFNAId").val();
	var curnt_FnaId = $("#hTxtFldCurrFNAId").val();
	var PrevFnaIDRemovFlg = $("#hTxtFldPrevFNAIdRemovFlg").val();
	var prevFnaId = $("#prevFnaId").val();*/
	var PrevFnaIDRemovFlg = $("#hTxtFldPrevFNAIdRemovFlg").val();
	var fullApprvekyc = $("#hTxtFldFullyApprvekyc").val();
	var fullApprvekycNoExistsFlg = true;
	var latstAchvFna =  $("#hTxtFldLatArchvFna").val();
	
	
	/*if(fullApprvekyc != "optgrpApproved"){
		fullApprvekycNoExistsFlg = true; 
	}else{
		fullApprvekycNoExistsFlg = false;
	}
	
	if(PrevFnaIDRemovFlg == "Y" || isEmpty(PrevFnaIDRemovFlg)){
		fullApprvekycNoExistsFlg = true; 
	}*/
	
	if( fullApprvekyc == "optgrpApproved"){
		fullApprvekycNoExistsFlg = false;
	}
	
	//alert(latstAchvFna + " " + fullApprvekyc);
	
	if( latstAchvFna != "(Last Saved eKYC)" && fullApprvekyc == "searchExistFnaList"){
		fullApprvekycNoExistsFlg = false;
	}
	
	 return fullApprvekycNoExistsFlg;
}

//poovathi add on 19-08-2021 to show and hide sps name in sidemenu
   function showSpouseInMenu(newinsured) {
		$("#clnt2Text").addClass("show").removeClass("hide");
		$("#MenuSpsName").text(newinsured).addClass("show").removeClass("hide");
	}

	function hideSpouseInMenu() {
		$("#MenuSpsName").text("").addClass("hide").removeClass("show");
		$("#clnt2Text").addClass("hide").removeClass("show");
	}
	
	//poovathi add on 21-09-2021 to add dynamic opt in businessNatur Combo for self,sps
	function addDynaOptCombo(fldId,OptVal){
		var busNatArr = [];
   	   $("#"+fldId).find("option").each(function() {
   		   busNatArr.push(this.value);
        });
	   
	   if(!busNatArr.includes(OptVal)){
    	 addOption(fldId,OptVal,OptVal)
       }
	}
	
	//poovathi add on 20-10-2021
	function showHideprodRecomAndSwitchScrn(){
		fetchAllPageData();
		if(FNA_TABLE_DATA.ccRepexistinvestflg == "Y"){
			  //$("#divProdRecomSwtchFrm").removeClass("disabledMngrSec");
			  $("#prodSwtchRecommMenu").addClass("d-block").removeClass("d-none");
		}else if(FNA_TABLE_DATA.ccRepexistinvestflg == "N"){
			 //$("#divProdRecomSwtchFrm").addClass("disabledMngrSec");
			 //$(".oldArchvnoteinfo").removeClass("hide").addClass("show");
	         //$(".oldArchvnoteinfo").html('Hi <strong>&nbsp;'+advname+'&nbsp;</strong>,'+' You had select <b>No</b> option for  Is any part of this application intended to switch / replace / surrender / terminate your existing Investment holdings or Life / Health Insurance polices. <b> so you cannot recommend Switch / Replace product Details.</b> ');
		     $("#prodSwtchRecommMenu").addClass("d-none").removeClass("d-block");
		
		}
		//end
	}
	
	
	
