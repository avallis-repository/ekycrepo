//var strGLBL_ACCESS_LEVEL = "";
//var AccessComboList = "";

var clientAutoCompList = [];

$(document).ready(function () {

	
	$('#cover-spin').hide(0);
	
	$('#searchIcon').hide();
    	$('#goPreviousArrow').hide();
    
    getGlblAcessLvlAgnts();
    		
	$('#srchClntModal').on('shown.bs.modal', function () {
		//GetAllData
		triggerClientDets();
		
		//clear preview recordes in Table
		searchtable.clear().draw();
	});

	$("#radNewClient").on("click",function(){
		 window.location.href="clientInfo";
	})
	
	//poovathi add on 08-06-2021
	var $input = $("#txtFldCustName");
	 $input.typeahead({
	  source:clientAutoCompList,
	  autoSelect: false
	});
	// End
	 
	 
	//Poovathi add fnaList select combo Select 2 Initialization on 05-05-2021
	 $('#selFldFnaList').select2({
	 	   templateResult: formatFnaListOpt,
	 	   placeholder: '--Select Archive below--'
	 });
	
});



	

$("#selSrchAdvName").on("change",function(){
	validatecurntAdvsr();
	clientAutoCompList.length=0;
	triggerClientDets();
	//clear preview recordes in Table
	searchtable.clear().draw();
	
})
function triggerClientDets() {
	
	setTimeout(function() { $('#cover-spin').show(0);}, 0);	
	setTimeout(function() {  
		getAllClntNames();
		$("#txtFldCustName").val("");
		$("#txtFldCustName").focus();
		
	}, 1000);
	
}


function getAllClntNames(){
	var custName = ''; 
	  var loggedAdvId = $("#selSrchAdvName").val() ;//$("#hTxtFldLoggedAdvId").val();
	  var globalAcsLvelAdvId = $("#selSrchAdvName option:selected").val();
	  if(!isEmpty(globalAcsLvelAdvId)) {
		

		   $.ajax({
	        url:baseUrl+"/customerdetails/getAllClients/"+globalAcsLvelAdvId,
	        type: "GET",
	        dataType: "json",async:false,
	        contentType: "application/json",
	        data : {"clientname":custName},
	        success: function (data) {
	        	for (var i=0; i<data.length; i++) {
	                 clientAutoCompList.push(data[i].custName);//Add Client Names in Autocomplete List Array
	            }
	        	
	        	
	        },
		        error: function(xhr,textStatus, errorThrown){
		 			$('#cover-spin').hide(0);
		 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
				}
	        });
		   
	  }else {
			$('#cover-spin').hide(0);
				//toastr.clear($('.toast'));
				//toastr.options = {  timeOut: "5000", extendedTimeOut: "1000"};
				//toastr["error"]("Select the Adviser");

	  }
	  
}


	   

var searchtable = $('#srchClntTable').DataTable({
//	 processing: true,
//     language: {
//         'loadingRecords': '&nbsp;',
//         'processing': '<div class="spinner"></div>'
//     } ,
    
	scrollX: true,
	scrollY:"300px",
	scroller: true,
	scrollCollapse:true,
	autoWidth:false,
//	paging:true,
//	pagingType: "simple",
	columnDefs: [
	                {
	                    "targets": [ 5 ],
	                    "visible": false,
	                    "searchable": false
	                }
	            ]
	
});

//commented by poovathi on 09-06-2021
/*$('#srchClntTable tbody').on('click', 'tr', function () {
	//$(this).addClass("bg-success");
	//$(this).css('cursor', 'pointer');
    var data = searchtable.row(this).data();
    
    var custId = data[5];
    
    window.location.href="clientInfo?c="+custId;
} );*/


//poovathi add on 03-02-2021

$('#srchClntTable tbody').on('click', 'tr', function () {
	//$(this).addClass("bg-success");
	//$(this).css('cursor', 'pointer');
    var data = searchtable.row(this).data();
    
    var custName = data[0]
    var custId = data[5];
    $.ajax({
        url:baseUrl+"/customerdetails/getAllFnaList/"+custId,
        type: "GET",
        //dataType: "json",
        async:false,
        contentType: "application/json",
        success: function (data) {
        	
        	$('#cover-spin').hide(0);
        	//$("#fnaArchivList").empty();
        	var fnaList = JSON.parse(data)
        	
        	var fnaListLen = fnaList.length;
        	
        	
        	
        	//var custName = $("#custName").val();
        	/*var fnaListInfo= '<span class="font-sz-level6  text-custom-color-gp" title="Total Archive List : '+fnaListLen+'">Client : <strong>'+custName+'</strong> totally have '
        	+'<span class="badge badge-pill badge-primary" title="'+fnaListLen+'">'+ fnaListLen +'</span>&nbsp;Archive List(s). Select Latest Archive and Click <strong>Proceed</strong> Button.</span>';
        	 $("#fnaListCountInfo").prop("title","Total Archive List : "+ fnaListLen );
        	 */
        	 
        	 var fnaListInfo ='<li class="list-group-item p-1 font-sz-level6 "><i class="fas fa-user text-secondary mx-2"></i><span>Client Name : </span><span><strong>'+custName+'</strong></span></li>'  
		            +'<li class="list-group-item p-1 font-sz-level6 "><i class="fas fa-list text-secondary mx-2" title="'+fnaListLen+'"></i>Total Archive List : '+
		            '<span class="badge badge-primary badge-pill">'+fnaListLen+'</span></li>';
		         
        	if(fnaListLen > 0){
        		$('#srchClntModal').modal('hide');
        		$('#fnaListModal').modal('show');
        		 //if fnaList exists ,have to enable search fnadetls radio button 
        		 $("input:radio[name=radfnaList]:last").prop('checked', true);
        		 $("#optgrpApproved").children().empty();
        		 $("#searchExistFnaList").children().empty();
        		//show notification msg
        		 $("#fnaListCountInfo").html(fnaListInfo).addClass("show").removeClass("hide");
        		 /*$.each(fnaList,function(key,val){
	        		$.each(val,function(key,val){
	        			if((key == "fnaId")&&(val != undefined)){
	        				$("#selFldFnaList").find('optgroup#searchExistFnaList').append('<option value="'+val+'" >'+val+'</option>');
	        			}
		        	});
	        	});*/
        		 
        		 
        		 for (var i=0; i<fnaListLen; i++) {
        			 
        			// var tabRow = '<tr><td>'+(i+1)+'</td><td>'+fnaList[i].fnaId+'</td><td>'+fnaList[i].mgrApproveStatus+'</td><td>'+fnaList[i].adminApproveStatus+'</td><td>'+fnaList[i].compApproveStatus+'</td><td>'+fnaList[i].fnaCreatedDate+'</td></tr>';
        			//$("#fnaArchivList").append(tabRow);
        			     var archivests = '';
        			     if( i == 0){
        			    	 archivests = "Last Saved eKYC";
        			     }else{
        			    	 archivests = "Archive - " + (i+1);
        			     }
        			     
        			     
        			     var mgrsts = fnaList[i].mgrApproveStatus;
        			     var adminsts = fnaList[i].adminApproveStatus;
        			     var compsts = fnaList[i].compApproveStatus;
        			     
        			     if(isEmpty(mgrsts)){
        			    	 mgrsts = "PENDING"
        			     }
        			     
        			     if(isEmpty(adminsts)){
        			    	 adminsts = "PENDING"
        			     }
        			     
        			     if(isEmpty(compsts)){
        			    	 compsts = "PENDING"
        			     }
        			     
        			     var mgrstsClass = '',mngrtxt = "";
        			     if(mgrsts == "APPROVE"){
        			    	 mgrstsClass = "fa fa-check-circle text-success"
        			    		 mngrtxt = "Approved";
        			    	 //archivests = "Last Saved eKYC";
        			    }else if(mgrsts == "REJECT"){
        			    	 mgrstsClass = "fa fa-times-circle text-danger"
        			    		 mngrtxt = "Rejected";
        			     }
        			     else if(mgrsts == "PENDINGREQ"){
        			    	 mgrstsClass = "fa fa-info-circle text-info"
        			    		 mngrtxt = "Pending Req";
        			     }else{
        			    	 mgrstsClass = "fa fa-question-circle text-primary"
        			    		 mngrtxt = "Pending";
        			     }
        			     
        			     var admnstsClass = '',admntxt = "";;
                         if(adminsts == "APPROVE"){
                        	 admnstsClass = "fa fa-check-circle text-success"
                        	 admntxt = 'Approved'
                        	// archivests = "Last Saved eKYC";
        			     }else if(adminsts == "REJECT"){
        			    	 admnstsClass = "fa fa-times-circle text-danger"
        			    		 admntxt = 'Rejected'
        			     }
        			     else if(adminsts == "PENDINGREQ"){
        			    	 admnstsClass = "fa fa-info-circle text-info"
        			    		 admntxt = 'pending Req'
        			     }else{
        			    	 admnstsClass = "fa fa-question-circle text-primary"
        			    		 admntxt = 'Pending' 
        			     }
                         
                         var compstsClass = '',comptxt = '';
                         if(compsts == "APPROVE"){
                        	 compstsClass = "fa fa-check-circle text-success" 
                        		 comptxt = 'Approved'
                        			// archivests = "Last Saved eKYC";
        			     }else if(compsts == "REJECT"){
        			    	 compstsClass = "fa fa-times-circle text-danger"
        			    		 comptxt = 'Rejected'
        			     }
        			     else if(compsts == "PENDINGREQ"){
        			    	 compstsClass = "fa fa-info-circle text-info"
        			    		 comptxt = 'Penging Req'
        			     }else{
        			    	 compstsClass = "fa fa-question-circle text-primary"
        			    		 comptxt = 'Pending'
        			     }
        			     
        			    // compsts.charAt(0) //IMPLEMENT LATER WILL DO FURTHER ENHANCEMETS
                         
                         if(isEmpty(fnaList[i].fnaId)){
                        	 fnaList[i].fnaId = "--NIL--"
                         }
                         
                         if(isEmpty(fnaList[i].fnaCreatedDate)){
                        	 fnaList[i].fnaCreatedDate = "--NIL--"
                         }
                         
                         if(isEmpty(fnaList[i].mgrapprstsByadvstf)){
                        	 fnaList[i].mgrapprstsByadvstf = "--NIL--"
                         }
                         if(isEmpty(fnaList[i].mgrApproveDate)){
                        	 fnaList[i].mgrApproveDate = "--NIL--"
                         }
                         
                         if(isEmpty(fnaList[i].adminapprstsByadvstf)){
                        	 fnaList[i].adminapprstsByadvstf = "--NIL--"
                         }
                         
                         if(isEmpty(fnaList[i].adminApproveDate)){
                        	 fnaList[i].adminApproveDate = "--NIL--"
                         }
                         
                         if(isEmpty(fnaList[i].compapprstsByadvstf)){
                        	 fnaList[i].compapprstsByadvstf = "--NIL--"
                         }
                         
                         if(isEmpty(fnaList[i].compApproveDate)){
                        	 fnaList[i].compApproveDate = "--NIL--"
                         }
        			     
                         
                         var fnaDate= fnaList[i].fnaCreatedDate.split("/")[0]
                         var fnaMonth= fnaList[i].fnaCreatedDate.split("/")[1]
                         var fnaYear= fnaList[i].fnaCreatedDate.split("/")[2]
                        
        			     var fnaArchvList = '<div class="jumbotron p-1 font-sz-level6 mt-2 hide" id="'+fnaList[i].fnaId+'" >'+
      			    	  '<h5 class="text-custom-color-gp font-sz-level6">fna Created Date : </span> <span class=" bold">'+fnaList[i].fnaCreatedDate+'</h5>'+
      			    	   '<div class="row">'+
      			    	        '<div class="col-md-4 col-sm-12 col-12"> <span  class="text-custom-color-gp ">Manager  :</span> <span class="bold"> '+fnaList[i].mgrapprstsByadvstf+'.</span></div>'+
      			    	        '<div class="col-md-4 col-sm-12 col-12"> <span  class="text-custom-color-gp ">Manager Status :</span> <span class="'+mgrstsClass+'"></span></div>'+
      			    	        '<div class="col-md-4 col-sm-12 col-12"> <span  class="text-custom-color-gp ">Manager '+mngrtxt+' Date : </span> <span class="bold"> '+fnaList[i].mgrApproveDate+'</span></div>'+
      			    	   '</div>'+
      			    	  
      			    	  '<div class="row">'+
      			    	    '<div class="col-md-4 col-sm-12 col-12"> <span  class="text-custom-color-gp ">Admin : </span> <span class="bold"> '+fnaList[i].adminapprstsByadvstf+'.</span></div>'+
			    	        '<div class="col-md-4 col-sm-12 col-12"> <span  class="text-custom-color-gp ">Admin Status :</span> <span class="'+admnstsClass+'"></span></div>'+
			    	        '<div class="col-md-4 col-sm-12 col-12"> <span  class="text-custom-color-gp ">Admin '+admntxt+' Date : </span> <span class="bold">'+fnaList[i].adminApproveDate+'</span></div>'+
			    	      '</div>'+
      			    	  
      			    	  '<div class="row">'+
			    	        '<div class="col-md-4 col-sm-12 col-12"> <span class="text-custom-color-gp ">Compliance :</span> <span> <span class="bold">'+fnaList[i].compapprstsByadvstf+'.</span></div>'+
			    	        '<div class="col-md-4 col-sm-12 col-12"> <span  class="text-custom-color-gp ">Compliance  Status :</span> <span class="'+compstsClass+'"></span></div>'+
			    	        '<div class="col-md-4 col-sm-12 col-12"> <span  class="text-custom-color-gp ">Comp '+comptxt+' Date :</span><span class="bold"> '+fnaList[i].compApproveDate+'</span></div>'+
			    	      '</div>'+
      			    	  
      			   '</div>';
        			     
        			     $(".fnaArchhvList").append(fnaArchvList);
        			    
        			     //poovathi add on 25-11-2021
                        var optgrplength = $("#selFldFnaList").find('optgroup#searchExistFnaList').children().length;
        			          if(optgrplength == 0){
        			            archivests = "Last Saved eKYC";
        			          }
        			    	  
                         var stsopt = '<span  title="'+mgrsts+'">M : '+mgrsts+'</span> |<span title="'+adminsts+'"> A : '+adminsts+' </span>|<span title="'+compsts+'"> C : '+compsts+' </span>';// | <span title="'+fnaList[i].fnaCreatedDate+'">Fna Crt. Date : '+fnaList[i].fnaCreatedDate+' </span>
        			     
        			    var seloption = '<option value="'+fnaList[i].fnaId+'">('+archivests+')_'+fnaList[i].fnaId+'_'+stsopt+' </option>';
        			    
                        // var seloption = '<option value="'+fnaList[i].fnaId+'">['+archivests+']_'+'['+fnaYear+"-"+fnaMonth+"-"+fnaDate'] Created - '+fnaList[i].fnaId+'_'+stsopt+ '' ' </option>';
        			     if(mgrsts == "APPROVE" && adminsts == "APPROVE" && compsts == "APPROVE"){
        			    	 archivests = "Last Saved eKYC";
        			    	 var stsopt = '<span  title="'+mgrsts+'">M : '+mgrsts+'</span> |<span title="'+adminsts+'"> A : '+adminsts+' </span>|<span title="'+compsts+'"> C : '+compsts+'</span> ';//(Approved on '+fnaList[i].compApproveDate+') //| <span title="'+fnaList[i].fnaCreatedDate+'">Fna Crt. Date : '+fnaList[i].fnaCreatedDate+' </span>
            			     var seloption = '<option value="'+fnaList[i].fnaId+'">('+archivests+')_'+fnaList[i].fnaId+'_'+stsopt+' </option>';
        			    	// var seloption = '<option value="'+fnaList[i].fnaId+'">['+archivests+']_'+'['+fnaYear+"-"+fnaMonth+"-"+fnaDate'] Created - '+fnaList[i].fnaId+'_'+stsopt+ '' ' </option>';
        			    	 $("#optgrpApproved").append(seloption);
       			         }else{
       			        	 $("#searchExistFnaList").append(seloption);
       			         } 
        			     
        						 
        			 
        			 /*if( i == 0){
        				 
        				 $("#selFldFnaList").find('optgroup#searchExistFnaList').append(seloption);
        			 }else{
        				 $("#selFldFnaList").find('optgroup#searchExistFnaList').append('<option value="'+fnaList[i].fnaId+'" >Archive('+(i+1)+') - '+fnaList[i].fnaId+'  </option>');
        			 }*/
             		
             	 }
        		 
        		 $("#selFldFnaList").val($('#searchExistFnaList > option').eq(0).val());
   			     $('#selFldFnaList').trigger('change.select2');
   			     $("#hdnCustId").val(custId);
   			     $("#hdnCustName").val(custName);
   			      
   			    // $('#searchExistFnaList option:contains("'+selfName+'")').prop("disabled","disabled");
        		/*if(fnaListLen == 1){
        		    	
        		    	var fnaId = $('#searchExistFnaList > option').eq(1).val();
        		    	$('#fnaListModal').modal('hide');
        		    	//console.log("only one fna id"+ fnaId)
        		    	window.location.href="kycIntro?id="+fnaId;
        		    	
        			  //if only one fnalist present,have to set that fnaid in combo
        			  $("#selFldFnaList").val($('#searchExistFnaList > option').eq(1).val());
        			  //$('#selFldFnaList').trigger('change.select2');
	        	}*/
        		
        		/*if(fnaListLen > 1 ){
        			 //if only one fnalist present,have to set that fnaid in combo
        			  $('#finishedModal').modal('show');
        			  $("#selFldFnaList").val($('#searchExistFnaList > option').eq(0).val());
        			  $('#selFldFnaList').trigger('change.select2');
        			  
        		}*/
        		
        	}
        	
        	//if no fnaList exist navigate to create new fna Section
        	if(fnaListLen == 0){
        		$('#fnaListModal').modal('hide');
        		$("#fnaListCountInfo").html("").addClass("hide").removeClass("show");
        		//window.location.href="kycIntro"
        		
        		//poovathi add on 29-06-2021 to encrypt custId
        		custId =  window.btoa(custId);
        		window.location.href="clientInfo?c="+custId;
        	}
        	
         },
         error: function(xhr,textStatus, errorThrown){
	 			$('#cover-spin').hide(0);
	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			}
        });
   
} );

//show Drag and drop Text Content...in File Upload Modal
$('#fnaListModal').on('shown.bs.modal', function () {
	$('#cover-spin').hide(0);
})
//End


/*function validateCrtNewFna(){
	 
	 var curFnaId = "";
	 var custId = $("#hdnCustId").val();
  	   var curOpt =  $('input[name="radfnaList"]:checked').val();
  	   if(curOpt == "SRCHFNA"){
  		   
  		   curFnaId = $("#selFldFnaList option:selected").val();
  		   
  		   if(isEmpty(curFnaId)){
  			    $("#searchExistFnaListError").addClass("show").removeClass("hide")
  			    $("#selFldFnaList").focus();
  			    return false;
  		   }else{
  			    $("#searchExistFnaListError").addClass("hide").removeClass("show") 
  			 }
  		 
		  	  //poovathi add on 29-06-2021 to encrypt custId and FnaID
		   	  custId =  window.btoa(custId);
		   	  curFnaId =  window.btoa(curFnaId);
  		      window.location.href="clientInfo?c="+custId+"&fnaId="+curFnaId;
  		   
  		 }else if(curOpt == "CRTFNA"){
  			 //window.location.href="CreateNewFNA"
  			 //poovathi add on 29-06-2021 to encrypt custId and FnaID
		   	  custId =  window.btoa(custId);
		   	  curOpt =  window.btoa(curOpt);
  			  window.location.href="clientInfo?c="+custId+"&newFna="+curOpt;
  		 }
  	   
  	   
}*/

function validateCrtNewFna(){
	 
	 var curFnaId = "";
	 var curFnaIdTxt = "";
	 
   	   var curOpt =  $('input[name="radfnaList"]:checked').val();
   	   if(curOpt == "SRCHFNA"){
   		   
   		   curFnaId = $("#selFldFnaList option:selected").val();
   		   curFnaIdTxt = $("#selFldFnaList option:selected").text();
   		
   		
   		   var latSavId = curFnaIdTxt.split("_")[0];
   		   
   		   var curntOptGrpId = $("#selFldFnaList option:selected").parent().attr("id")
   		   
   		    if(isEmpty(curFnaId)){
   			    $("#searchExistFnaListError").addClass("show").removeClass("hide")
   			    $("#selFldFnaList").focus();
   			    return false;
   		   }else{
   			    $("#searchExistFnaListError").addClass("hide").removeClass("show") 
   			 }
   		   
   		 callAjaxParam(curFnaId);
   		   
   		 }else if(curOpt == "CRTFNA"){
   			
   			 //poovathi add on 16-09-2021 (ekycdev change)
   			 
   			var custId =  $("#hdnCustId").val();
   			var custName = $("#hdnCustName").val();
   			// console.log("custId-----------",custId,"custName------",custName);
   		    var custeId = window.btoa(custId);
            var custeName = window.btoa(custName);
   			  window.location.href="CreateNewFNA?custId="+custeId+"&custName="+custeName;
   		 }
   	   
   	   
 }


// hide error message of fnaListCombo
function getFnaListVal(selObj){
	
	
	var ids = [],pendingArr = [],aprovedArr = [];
	var selobjGrupId = $(selObj).children().each(function(i,e){
		var grpid = $(this).attr("id");
		ids.push(grpid)
	});
	
	for(var i = 0;i<ids.length;i++){
		var id =  ids[i];
		$("#"+id).children().each(function(i,e){
			
			if(id == "searchExistFnaList"){
				//alert($(this).parent().attr("id"))
				pendingArr.push($(this).attr("value"))
			}else if(id == "optgrpApproved"){
				aprovedArr.push($(this).attr("value"))
			}
			
		});
	}
	
	
	//alert("searchExistFnaList"+pendingArr.length)
	//alert("optgrpApproved"+aprovedArr.length)
	
	
	var  curFnaId = $("#selFldFnaList option:selected").val();
	var  curFnaIdTxt = $("#selFldFnaList option:selected").text();
	
	var optGrpid = $("#selFldFnaList option:selected").parent().attr("id");
	
	
	
	  $(".jumbotron").each(function(i,e){
		   
		   if($(this).attr("id") == curFnaIdTxt.split("_")[1]){
			   $(this).removeClass("hide");
			   $(".jumbotron").not(this).addClass("hide")
		   }
	   });
	
	var latSavId = curFnaIdTxt.split("_")[0];
	
	 $("#hdnselGrpId").val(optGrpid)
	 $("#hdnLatArchFna").val(latSavId)
	
	var  latFnaId = $('#searchExistFnaList > option').eq(0).val();
	var  custName = $("#hdnCustName").val();
	//console.log(curFnaId , latFnaId)
	
	       if(isEmpty(curFnaId)){
			    $("#searchExistFnaListError").addClass("show").removeClass("hide")
			    $("#selFldFnaList").focus();
			    return false;
		   }else{
			    $("#searchExistFnaListError").addClass("hide").removeClass("show");
			    
			   // if(curFnaId != latFnaId){
			    if(aprovedArr.includes(curFnaId)){
			    	     Swal.fire(
			    			 // 'Dear, ' + custName,
			    	    		 '',
			    	    		 (FNA_FINALARCHANYONE_VALMSG),
			    			  'info'
			    			)
			    }
	  }
}


function createNewFnaFun(){
	 $("#searchExistFnaListError").addClass("hide").removeClass("show");
	 $('#selFldFnaList').val('');
	 $('#selFldFnaList').select2({
		   templateResult: formatFnaListOpt,
		   placeholder: '--Select FNA Id below--'
	});

}

function callAjaxParam(curFnaId){
	 
	//alert(curFnaId)
	
	var custId =  $("#hdnCustId").val();
	var custName = $("#hdnCustName").val();
	
	var latSavIds =  $("#hdnselGrpId").val()
	var curntOptGrpIds =  $("#hdnLatArchFna").val()
	
	console.log("custId-----------",custId,"custName------",custName)
	console.log("curntOptGrpId-----------",curntOptGrpIds)
	 
	 $.ajax({
	        url:baseUrl+"/fnadetails/register/Fnadetails/"+curFnaId, 
	        type: "GET",
           dataType: "json",
           async:false,
	        contentType: "application/json",
	        success: function (data) {
	        	 //alert(data.FNA_ID)
	        	  var fnaId = window.btoa(data.FNA_ID);
	        	  var custeId = window.btoa(custId);
	              var custeName = window.btoa(custName);
	              var curntOptGrpId = window.btoa(curntOptGrpIds);
	              var latSavId = window.btoa(latSavIds);
	           
	             // alert(custName)
	              //alert(curntOptGrpIds+" curntOptGrpIds")
	          	  //alert(latSavIds +"latSavIds")
	        	  
	        	  window.location.href="kycIntro?id="+fnaId+"&custId="+custeId+"&custName="+custeName+"&curntOptGrpId="+curntOptGrpId+"&latSavId="+latSavId;
	        	    //window.location.href="kycIntro?id="+fnaId;
	       },
	       error: function(xhr,textStatus, errorThrown){
	 			$('#cover-spin').hide(0);
	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			}
   });
	 
	 }

/*function callAjaxParam(curFnaId){
	 
	var custId =  $("#hdnCustId").val();
	var custName = $("#hdnCustName").val();
	
	console.log("custId-----------",custId,"custName------",custName)
	 
	 $.ajax({
	        url:baseUrl+"/fnadetails/register/Fnadetails/"+curFnaId, 
	        type: "GET",
            dataType: "json",
            async:false,
	        contentType: "application/json",
	        success: function (data) {
	        	 //alert(data.FNA_ID)
	        	  var fnaId = window.btoa(data.FNA_ID);
	                  var custeId = window.btoa(custId);
	                  var custeName = window.btoa(custName);
	                  
	        	 //alert(fnaId)
	        	      window.location.href="kycIntro?id="+fnaId+"&custId="+custeId+"&custName="+custeName;
	       },
	       error: function(xhr,textStatus, errorThrown){
	 			$('#cover-spin').hide(0);
	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			}
   });
	 
	 }*/


//Poovathi add fnaList select combo Select 2 Initialization on 05-05-2021
    function formatFnaListOpt (fnaList) {
		
		  if (!fnaList.id ) {
			 return fnaList.text;
		  }
		  
		  if( fnaList.id == "Select FNA Id/Add New FNA Id"){
			  return fnaList.text;
		  }
		 
		  var image = "vendor/avallis/img/idCard.png";
		  var $fnaList = $(
		         '<span><img style="width:25px" src="' + image +'" class="img-flag" /> ' + fnaList.text + '</span>'
		  );
		  return $fnaList;
		} 
//end of code 09-06-2021

function getAllFnaSpouseDetails(){
	
	//validate Adviser field while click search Button
	if(!validatecurntAdvsr()){return;}
	
	setTimeout(function() { loaderBlock() ;}, 0);	
	setTimeout(function() {  
		
		// show search results table
		$("#srchClntModal").find(".modal-body").find("#srchClntTblSec").removeClass('hide').addClass('show');
		
		$("#srchClntModal").find(".modal-body").find("#noteInfo").text("Dont click anywhere in the window,Until it Searching Your Results !");
		
		//set overlay class for loader
//		$("#srchClntModal").find(".modal-body").find("#secOverlay").addClass("disabledMngrSec");
		
		// get client Name 
		var custName = ''; 
			
		var client = $("#txtFldCustName").val();
		
		if(client.length == 0){
			custName  = client;
		}else{
			custName  = client;
		}
		
	    var loggedAdvId = $("#hTxtFldLoggedAdvId").val();
	    var globalAcsLvelAdvId = $("#selSrchAdvName option:selected").val();
	    
//		var tblId = document.getElementById("srchClntTable");
//		var tbody = tblId.tBodies[0]; 
		
		searchtable.clear().draw();
		var col0="",col1="",col2="",col3="",col4="",col5="",col6="";var col7="";
		
		if(!isEmpty(globalAcsLvelAdvId)) {
			$.ajax({
			    url:baseUrl+"/customerdetails/getAllClients/"+globalAcsLvelAdvId,
	            type: "GET",
	            dataType: "json",async:false,
	            contentType: "application/json",
	            data : {"clientname":custName},
	            success: function (data) {
	            	
	            //poovathi add on 16-08-2021 (client not found means navigate new Client Create Page)
	            	if(( data.length == 0 ) && (!isEmpty(custName))){
	            		
	            		//poovathi add on 16-09-2021 (ekycdev change)
	            		  Swal.fire({
	            			  title: 'Client Not found!',
	            			  html: NEW_CLNT_PROMT_MSG.replace("~NEWCLIENT~", client),
	            			  icon: 'info',
	            			  showCancelButton: true,
	            			  confirmButtonColor: '#5cb85c',
	            			  cancelButtonColor: '#d33',
	            			  confirmButtonText: 'Yes, Create New Client'
	            			}).then((result) => {
	            			  if (result.isConfirmed) {
	            				  //poovathi add on 16-09-2021 (ekycdev change)
	            				  var newClntName = window.btoa(client);
	            					window.location.href="clientInfo?newClnt="+newClntName;
	            					
	            				  //window.location.href="clientInfo";
	            			  }else{
	            				  $("#txtFldCustName").val("");//poovathi add on 21-09-21 clr clnt name when adv not create new client.
	            			  }
	            			})
	            	}//end

	            	
	            	
//	            	console.log("dada--------------------->"+data)
		            for (var i=0; i<data.length; i++) {
		            	
		            	col0=data[i].custName;
		            	
		            	//poovathi comment on 18-06-2021(to avoid duplicate client name in anutocomplete list)
		            	//clientAutoCompList.push(col0);
		            	
//		            	console.log("autocomplete List--------------------->"+clientAutoCompList)
		            	
		            	col1=data[i].custInitials;
		            	col2=data[i].nric;
		            	col3=data[i].dob;
		            	col4=data[i].resHandPhone;
		            	col5=data[i].custid;
		            	col6=data[i].custCateg;
		            	//col7 = '<a href="#" class="btn btn-link" role="">New FNA</a>';
		            	if(col6 != "COMPANY") {
		            		searchtable.row.add( [col0,col1,col2,col3,col4,col5] ).draw( false );
		            	}
		            	
		            	
	              }
	            	
	            	$("#srchClntModal").find(".modal-body").find('#srchClntLoader').removeClass("d-block").addClass("hide");
	            	//remove overlay class for loader
	            	$("#srchClntModal").find(".modal-body").find("#secOverlay").removeClass("disabledMngrSec");
	            	$("#srchClntModal").find(".modal-body").find(".noteInfoMsg").removeClass('hide').addClass('show');
	            	$("#srchClntModal").find(".modal-body").find("#noteInfo").text("Click a row to view the details");
	            },
	            error: function(xhr,textStatus, errorThrown){
		 			$('#cover-spin').hide(0);
		 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
				}
	            });
		}else {
			$('#cover-spin').hide(0);
			
			//toastr.clear($('.toast'));
			//toastr.options = {  timeOut: "5000", extendedTimeOut: "1000"};
			//toastr["error"]("Select the Adviser");
		}
		
		
	}, 1000);
	
	
}



function validatecurntAdvsr(){
	 var advId = $("#selSrchAdvName").val();
	 var Fld = "Error";
	 if(isEmptyFld(advId)){
		  $("#selSrchAdvName"+Fld).removeClass("hide").addClass("show");
		  $("#selSrchAdvName").addClass('err-fld');
		  $("#selSrchAdvName").focus();
	      return;
		}else{
		  $("#selSrchAdvName"+Fld).removeClass("show").addClass("hide");
		  $("#selSrchAdvName").removeClass('err-fld');
		  
	}
	
	 return true;
}


// get Adviser List Cobo based on Access Level
function getGlblAcessLvlAgnts(){
	
	//clear preview recordes in Table
	searchtable.clear().draw();
	//clr cutname field
	$("#txtFldCustName").val("");
	
	clearAdvCombo();
	var globalAcsLvel = $("#selGlblAcessLvl option:selected").val();
	var strloggedAdvStaffId = $("#hTxtFldLoggedAdvId").val();
	var strloggedUserStaffType = $("#hTxtloggedUsrStftype").val();
	var strloggedUsrMgrFlg = $("#hTxtloggedUsrMgrFlg").val();
	var strloggedUsrDistId ='DIS000000001';//$("#hTxtloggedUsrDistId").val();
	
	var advName = $("#hTxtFldLoggedAdvName").val();
	
	
	var param =  {"loggedAdvStaffId":strloggedAdvStaffId,"loggedUserStaffType":strloggedUserStaffType,"loggedUsrDistId":strloggedUsrDistId}
	if( globalAcsLvel == "MANAGER"){
		
		$.ajax({
			
			    url:"customerdetails/getGloblAcessList/",
	            type: "GET",
	            async:false,
	            dataType: "json",
	            contentType: "application/json",
	            data :param,
	            success: function (data) {
                    //poovathi add on 02-12-2021
	            	$('#cover-spin').hide(0);
	           
                for (var i=0; i<data.length; i++) {
	        	    var newOpt = '<option value="'+data[i].advstfId+'">'+data[i].advstfName+'</option>'
	            	$("#selSrchAdvName").append(newOpt);
	        	    $("#selSrchAdvNameGlobal").append(newOpt);
                 } 	
               
               validatecurntAdvsr();
	            	
	         },
	         error: function(xhr,textStatus, errorThrown){
		 			//$('#cover-spin').hide(0);
		 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
				}
            });
	}
	
   if( globalAcsLvel == "ADVISER/STAFF"){
	   
	   if(strloggedUserStaffType == "PARAPLANNER") {
		   
		   var totalpara = paraplanadviser.length;
		   
			for(var para=0;para<totalpara;para++) {
				var jsonObj = paraplanadviser[para];

				var newOpt = '<option value="'+jsonObj.paraAdvStfId+'">'+jsonObj.paraAdvStfName+'</option>';
						$("#selSrchAdvName").append(newOpt);
						$("#selSrchAdvNameGlobal").append(newOpt);
			}
	   }else {
		
		   var newOpt = '<option value="'+strloggedAdvStaffId+'">'+advName+'</option>'
			   	   $("#selSrchAdvName").append(newOpt);
				   $("#selSrchAdvName").val(strloggedAdvStaffId);
				   
				   $("#selSrchAdvNameGlobal").append(newOpt);
				   $("#selSrchAdvNameGlobal").val(strloggedAdvStaffId);
				   
				   //hide Adviser field Error (if exists)
				   validatecurntAdvsr();
				   
				   // focus custname text Field
				   $("#txtFldCustName").val("");
				   $("#txtFldCustName").focus();
		 }
	   validatecurntAdvsr();
	}
   
   if( globalAcsLvel == "COMPANY"){
	   $.ajax({
			
		   url:"customerdetails/getGloblAcessListCompany/",
           type: "GET",
           async:false,
           dataType: "json",
           contentType: "application/json",
           data :param,
           success: function (data) {
        	   
        	
         for (var i=0; i<data.length; i++) {
       	    var newOpt = '<option value="'+data[i].advstfId+'">'+data[i].advstfName+'</option>'
           	$("#selSrchAdvName").append(newOpt);
       	     $("#selSrchAdvNameGlobal").append(newOpt);
            } 	
         
         validatecurntAdvsr();
           	
        },
        error: function(xhr,textStatus, errorThrown){
 			//$('#cover-spin').hide(0);
 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
		}
       });
	}
   
  
}//fun end

//clear combo values
function clearAdvCombo(){
	$("#selSrchAdvName").find('option').not(':first').remove();
	 $("#selSrchAdvNameGlobal").find('option').not(':first').remove();
}

//poovathi add on 15-06-2021
function clrTableData(){
	//clear preview recordes in Table
	searchtable.clear().draw();
}





