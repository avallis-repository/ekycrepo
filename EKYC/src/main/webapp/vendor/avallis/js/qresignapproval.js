3/**
 * 
 */


/*Qr code implementation*/

var signatureDoneArr = [];
var seesselfName="",sessspsname=""

function getQrCode(value,id,loaderId){
    document.getElementById(loaderId).style.display = "block";
    $.ajax({
    	 url : baseUrl+'/FnaSignature/getQrCodeById/'+value,
		 type: 'GET',
		 success: function (byteArray) {
	       $('#'+id).attr("src", 'data:image/png;base64,'+byteArray);
			// console.log("SUCCESS : ", document.getElementById("qrCode").src);
		  document.getElementById(loaderId).style.display = "none";
           startTimer();
           

    
       },
       error: function (e) {
       	console.log("ERROR : ", e);
      }
   });
}

var timesRun = 0;
var timer;
function startTimer(){
	timer = setInterval(getAllSignDatas, 30000);//30sec
	
}
function getAllSignDatas(){
	
	timesRun++;
	var isModalopen = $('#ScanToSignModal').is(':visible');
	
	//isModalopen ||
	if( ((isEmpty(sessspsname) && signatureDoneArr.length < 3) || (!isEmpty(sessspsname) && signatureDoneArr.length < 4))){
		//if( (isEmpty(sessspsname) && signatureDoneArr.length < 2) || (!isEmpty(sessspsname) && signatureDoneArr.length < 3)){
		var fnaId=$("#txtFldFnaId").val();
		getAllSign(fnaId);
		//isEmailPopup();
		genQrOrSign('CLIENT','Client1');
		genQrOrSign('SPS','Client2');
		genQrOrSign('ADVISER','Adviser');
		genQrOrSign('MANAGER','Manager');
		console.log(timesRun,"timesRun")
		clearInterval(timer);
		//}
	}
	
	
	
 		
}


/*Signpad*/
/* Self */
function setClientType(type){
	$("#perType").val(type);
	
}
var count=0;
function selfInit(diagramId,savedDiagram) {
	var valid=document.getElementById("selfLoad").value;
	//
     if(valid == "Y"){
	     if (window.goSamples) goSamples(); 
	 	 var $ = go.GraphObject.make;
	  selfDiagramObj = $(go.Diagram,diagramId); 
      selfDiagramObj.toolManager.mouseDownTools.insertAt(3, new GeometryReshapingTool());
      selfDiagramObj.nodeTemplateMap.add("FreehandDrawing",
        $(go.Part,
          { locationSpot: go.Spot.Center, isLayoutPositioned: false },
          new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
          {
            selectionAdorned: true, selectionObjectName: "SHAPE",
            selectionAdornmentTemplate:  // custom selection adornment: a blue rectangle
              $(go.Adornment, "Auto",
                $(go.Shape, { stroke: "dodgerblue", fill: null }),
                $(go.Placeholder, { margin: -1 }))
          },
          { resizable: true, resizeObjectName: "SHAPE" },
          { rotatable: true, rotateObjectName: "SHAPE" },
          { reshapable: true },  // GeometryReshapingTool assumes nonexistent Part.reshapeObjectName would be "SHAPE"
          $(go.Shape,
            { name: "SHAPE", fill: null, strokeWidth: 1.5 },
            new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
            new go.Binding("angle").makeTwoWay(),
            new go.Binding("geometryString", "geo").makeTwoWay(),
            new go.Binding("fill"),
            new go.Binding("stroke"),
            new go.Binding("strokeWidth"))
        ));
      // create drawing tool for myDiagram, defined in FreehandDrawingTool.js
      var tool = new FreehandDrawingTool();
      // provide the default JavaScript object for a new polygon in the model
      tool.archetypePartData =
        { stroke: "green", strokeWidth: 3, category: "FreehandDrawing" };
      // allow the tool to start on top of an existing Part
      tool.isBackgroundOnly = false;
      // install as first mouse-move-tool
      selfDiagramObj.toolManager.mouseMoveTools.insertAt(0, tool);
      
      selfLoad(savedDiagram);  // 
      document.getElementById("selfLoad").value="N";
     }
    }
   function selfClearDiagram(savedDiagram){
	  
       clearDiagram(savedDiagram,selfDiagramObj);
    }
    function selfLoad(savedDiagram) {
      load(savedDiagram,selfDiagramObj);

    
    }
//Sps

function spsInit(diagramId,savedDiagram) {
	var valid=document.getElementById("spsLoad").value;
	
     if (window.goSamples) goSamples(); 
 	 var $ = go.GraphObject.make;
    if(valid == "Y"){
     spsDiagramObj = $(go.Diagram,diagramId);
      spsDiagramObj.toolManager.mouseDownTools.insertAt(3, new GeometryReshapingTool());
      spsDiagramObj.nodeTemplateMap.add("FreehandDrawing",
        $(go.Part,
          { locationSpot: go.Spot.Center, isLayoutPositioned: false },
          new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
          {
            selectionAdorned: true, selectionObjectName: "SHAPE",
            selectionAdornmentTemplate:  // custom selection adornment: a blue rectangle
              $(go.Adornment, "Auto",
                $(go.Shape, { stroke: "dodgerblue", fill: null }),
                $(go.Placeholder, { margin: -1 }))
          },
          { resizable: true, resizeObjectName: "SHAPE" },
          { rotatable: true, rotateObjectName: "SHAPE" },
          { reshapable: true },  // GeometryReshapingTool assumes nonexistent Part.reshapeObjectName would be "SHAPE"
          $(go.Shape,
            { name: "SHAPE", fill: null, strokeWidth: 1.5 },
            new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
            new go.Binding("angle").makeTwoWay(),
            new go.Binding("geometryString", "geo").makeTwoWay(),
            new go.Binding("fill"),
            new go.Binding("stroke"),
            new go.Binding("strokeWidth"))
        ));
      // create drawing tool for myDiagram, defined in FreehandDrawingTool.js
      var tool = new FreehandDrawingTool();
      // provide the default JavaScript object for a new polygon in the model
      tool.archetypePartData =
        { stroke: "green", strokeWidth: 3, category: "FreehandDrawing" };
      // allow the tool to start on top of an existing Part
      tool.isBackgroundOnly = false;
      // install as first mouse-move-tool
      spsDiagramObj.toolManager.mouseMoveTools.insertAt(0, tool);
      spsLoad(savedDiagram);  //      document.getElementById("spsLoad").value="N";
    }
    }
   function spsClearDiagram(savedDiagram){
       clearDiagram(savedDiagram,spsDiagramObj);
    }
    function spsLoad(savedDiagram) {
      load(savedDiagram,spsDiagramObj);

    
    }
//End
//Advisor

function advInit(diagramId,savedDiagram) {
	var valid=document.getElementById("advLoad").value;
	if(valid == "Y"){
     if (window.goSamples) goSamples(); 
 	 var $ = go.GraphObject.make;
    
     advDiagramObj = $(go.Diagram,diagramId);
      advDiagramObj.toolManager.mouseDownTools.insertAt(3, new GeometryReshapingTool());
      advDiagramObj.nodeTemplateMap.add("FreehandDrawing",
        $(go.Part,
          { locationSpot: go.Spot.Center, isLayoutPositioned: false },
          new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
          {
            selectionAdorned: true, selectionObjectName: "SHAPE",
            selectionAdornmentTemplate:  // custom selection adornment: a blue rectangle
              $(go.Adornment, "Auto",
                $(go.Shape, { stroke: "dodgerblue", fill: null }),
                $(go.Placeholder, { margin: -1 }))
          },
          { resizable: true, resizeObjectName: "SHAPE" },
          { rotatable: true, rotateObjectName: "SHAPE" },
          { reshapable: true },  // GeometryReshapingTool assumes nonexistent Part.reshapeObjectName would be "SHAPE"
          $(go.Shape,
            { name: "SHAPE", fill: null, strokeWidth: 1.5 },
            new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
            new go.Binding("angle").makeTwoWay(),
            new go.Binding("geometryString", "geo").makeTwoWay(),
            new go.Binding("fill"),
            new go.Binding("stroke"),
            new go.Binding("strokeWidth"))
        ));
      // create drawing tool for myDiagram, defined in FreehandDrawingTool.js
      var tool = new FreehandDrawingTool();
      // provide the default JavaScript object for a new polygon in the model
      tool.archetypePartData =
        { stroke: "green", strokeWidth: 3, category: "FreehandDrawing" };
      // allow the tool to start on top of an existing Part
      tool.isBackgroundOnly = true;
      // install as first mouse-move-tool
      advDiagramObj.toolManager.mouseMoveTools.insertAt(0, tool);
      advLoad(savedDiagram);  // 
      document.getElementById("advLoad").value="N";
    }
    }
   function advClearDiagram(savedDiagram){
       clearDiagram(savedDiagram,advDiagramObj);
    	
    }
  
    function advLoad(savedDiagram) {
      load(savedDiagram,advDiagramObj);
    }



function clearDiagram(savedDiagram,diagramObj){
	 var str1 = '{ "position": "-5 -5", "model": { "class": "GraphLinksModel", "nodeDataArray": [], "linkDataArray": []} }';
    	 document.getElementById(savedDiagram).value = str1;

    	 var str = document.getElementById(savedDiagram).value;
         try {
           var json = JSON.parse(str);

           diagramObj.initialPosition = go.Point.parse(json.position || "-5 -5");
           diagramObj.model = go.Model.fromJson(json.model);
           diagramObj.model.undoManager.isEnabled = true;
         } catch (ex) {
           alert(ex);
         }
}
function load(savedDiagram,diagramObj){
var str = document.getElementById(savedDiagram).value;
      try {
        var json = JSON.parse(str);
        diagramObj.initialPosition = go.Point.parse(json.position || "-5 -5");
        diagramObj.model = go.Model.fromJson(json.model);
        diagramObj.model.undoManager.isEnabled = true;
      } catch (ex) {
        alert(ex);
      }
}
//End
//Manager
function mgrInit(diagramId,savedDiagram) {
	var valid=document.getElementById("mgrLoad").value;
	if(valid == "Y"){
     if (window.goSamples) goSamples(); 
 	 var $ = go.GraphObject.make;
     mgrDiagramObj = $(go.Diagram,diagramId);
      mgrDiagramObj.toolManager.mouseDownTools.insertAt(3, new GeometryReshapingTool());
      mgrDiagramObj.nodeTemplateMap.add("FreehandDrawing",
        $(go.Part,
          { locationSpot: go.Spot.Center, isLayoutPositioned: false },
          new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
          {
            selectionAdorned: true, selectionObjectName: "SHAPE",
            selectionAdornmentTemplate:  // custom selection adornment: a blue rectangle
              $(go.Adornment, "Auto",
                $(go.Shape, { stroke: "dodgerblue", fill: null }),
                $(go.Placeholder, { margin: -1 }))
          },
          { resizable: true, resizeObjectName: "SHAPE" },
          { rotatable: true, rotateObjectName: "SHAPE" },
          { reshapable: true },  // GeometryReshapingTool assumes nonexistent Part.reshapeObjectName would be "SHAPE"
          $(go.Shape,
            { name: "SHAPE", fill: null, strokeWidth: 1.5 },
            new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
            new go.Binding("angle").makeTwoWay(),
            new go.Binding("geometryString", "geo").makeTwoWay(),
            new go.Binding("fill"),
            new go.Binding("stroke"),
            new go.Binding("strokeWidth"))
        ));
      // create drawing tool for myDiagram, defined in FreehandDrawingTool.js
      var tool = new FreehandDrawingTool();
      // provide the default JavaScript object for a new polygon in the model
      tool.archetypePartData =
        { stroke: "green", strokeWidth: 3, category: "FreehandDrawing" };
      // allow the tool to start on top of an existing Part
      tool.isBackgroundOnly = false;
      // install as first mouse-move-tool
      mgrDiagramObj.toolManager.mouseMoveTools.insertAt(0, tool);
      mgrLoad(savedDiagram);  // 
      document.getElementById("mgrLoad").value="N";
     }
    }
   function mgrClearDiagram(savedDiagram){
       
    	 clearDiagram(savedDiagram,mgrDiagramObj);
    }
    function mgrLoad(savedDiagram) {
     load(savedDiagram,mgrDiagramObj);

    
    }

  
    function mode(draw) {
      var tool = myDiagram.toolManager.findTool("FreehandDrawing");
      tool.isEnabled = draw;
    }
    function updateAllAdornments() {  // called after checkboxes change Diagram.allow...
      myDiagram.selection.each(function(p) { p.updateAdornments(); });
    }
    
    // save a model to and load a model from Json text, displayed below the Diagram
    function selfSave() {
    	makeBlob(selfDiagramObj);
 	}
    function spsSave() {
    	makeBlob(spsDiagramObj);
 	}
    function advSave() {
    	makeBlob(advDiagramObj);
 	}
    function mgrSave() {
    	makeBlob(mgrDiagramObj);
 	}
  
    function makeBlob(selfDiagramObj) {
        var blob = selfDiagramObj.makeImageData({ background: "white", returnType: "blob", callback: sendSignData });
      }
    function sendSignData(blob) {
    	mgrSign=blob;
       	 var url = URL.createObjectURL(blob);

	    	
	    	var signByte;
	    	var request = new XMLHttpRequest();
	        request.open('GET', url, true);
	        request.responseType = 'blob';
	        request.onload = function() {
	            var reader = new FileReader();
	            reader.readAsDataURL(request.response);
	            reader.onload =  function(e){
	            	signByte=e.target.result;
	            	getImage(signByte);
	            	
	            };
	        };
	        request.send();
 }
   
   function getImage(signByte){
	   var perType=$('#perType').val();
       
       var esignaturestr;
       var esignId;
	    if(perType == "CLIENT"){
	     	esignaturestr = '{ "position": "' + go.Point.stringify(selfDiagramObj.position) + '",\n  "model": ' + selfDiagramObj.model.toJson() + ' }';
			document.getElementById("selfSavedDiagram").value = esignaturestr;
			esignId=document.getElementById('selfSignId').value;
	      //  alert(esignaturestr);
         }else if(perType == "SPOUSE"){
	     	esignaturestr = '{ "position": "' + go.Point.stringify(spsDiagramObj.position) + '",\n  "model": ' + spsDiagramObj.model.toJson() + ' }';
			document.getElementById("spsSavedDiagram").value = esignaturestr;
			esignId=document.getElementById('spsSignId').value;
	       // alert(esignaturestr);
         }
         else if(perType == "ADVISER"){
	     	esignaturestr = '{ "position": "' + go.Point.stringify(advDiagramObj.position) + '",\n  "model": ' + advDiagramObj.model.toJson() + ' }';
			document.getElementById("advSavedDiagram").value = esignaturestr;
			esignId=document.getElementById('advSignId').value;
	       // alert(esignaturestr);
         }else if(perType == "MANAGER"){
	     	esignaturestr = '{ "position": "' + go.Point.stringify(mgrDiagramObj.position) + '",\n  "model": ' + mgrDiagramObj.model.toJson() + ' }';
			document.getElementById("mgrSavedDiagram").value = esignaturestr;
			esignId=document.getElementById('mgrSignId').value;
	       // alert(esignaturestr);
            
         }
	    
	     var signImgString=signByte.split(",")[1];
        
	     
	     var fnaId=$("#txtFldFnaId").val();

	    
	    
	     saveSignature(fnaId,esignaturestr,signImgString,perType,esignId);
   }  

  
    
   function saveSignature(fnaId,esignaturestr,signImgString,perType,esignId){
	var curPage=$("#curPage").val();
	
	 $.ajax({
	    	  url: baseUrl+"FnaSignature/saveFnaSignature",
	    	  type: "POST",
	    	  
	    	  data:{fnaId:fnaId,
	    		  	esign:esignaturestr,
	    		  	signDoc:signImgString,
	    		  	perType:perType,
                  	esignId:esignId,
                  	curPage:curPage
	    	  },
	    	
	    		  
	    	  success : function(data){
		        
                 //After isertion set esign id -Avoid duplicate entries
                 setSignPersonData(data);


	 			getAllSign(fnaId);
	
                 //isEmailPopup();
				 //alert("success");
	    	  },
	    	  error : function(data){
	    		  alert("Error in saving Signature");
	    		 
	    	  }});
} 

function isEmailPopup(){
	var fnaId=$("#txtFldFnaId").val();
	$.ajax({
 		    url: baseUrl+'FnaSignature/checkPopupMail/'+fnaId,
 		    type: 'GET',
 		    success: function(data) {
	          if(data.length >=2){
		        // $("#mailModal").modal("show");
			  }
 		    	
 		    },
 		    error: function(jqXHR, textStatus, errorThrown,data){
 		    	
 		    	console.log('Error: ' + textStatus + ' - ' + errorThrown);
 		       
 		    }
 		});
}

//Get all data by fnaId
function editSignData(){
	var fnaId=$("#txtFldFnaId").val();
	getAllSign(fnaId);
}



//Set signature data
 function getAllSign(fnaId){
	
	fnaId = $("#txtFldFnaId").val()
 	   $.ajax({
 		    url: baseUrl+'/FnaSignature/getAllDataById/'+fnaId,
 		    type: 'GET',
 		    async:false,
 		    success: function(data) {
	          	//var obj=JSON.stringify(data);
	          	for(var i=0;i<data.length;i++){
		alert(data)
$.each(signatureDoneArr,function(){
  if ($.inArray(data[i].signPerson,signatureDoneArr)==-1) signatureDoneArr.push(data[i].signPerson);
});
		console.log(signatureDoneArr,"signatureDoneArr")
		
	         //	signatureDoneArr.push(data[i].signPerson);
					if(data[i].signPerson == "CLIENT"){						
						$("#signmodalclient1qrsign").removeClass("fa-exclamation-triangle").addClass("fa-check-circle");
					}
					if(data[i].signPerson == "SPOUSE"){						
						$("#signmodalclient2qrsign").removeClass("fa-exclamation-triangle").addClass("fa-check-circle");
					}
					if(data[i].signPerson == "ADVISER"){						
						$("#signmodaladviserqrsign").removeClass("fa-exclamation-triangle").addClass("fa-check-circle");
					}
					if(data[i].signPerson == "MANAGER"){						
						$("#signmodalmanagerqrsign").removeClass("fa-exclamation-triangle").addClass("fa-check-circle");
					}
				}
	          	if(data.length > 0){
 		    		setAllSign(data);
 		    	}
 		    	
 		    },
 		    error: function(jqXHR, textStatus, errorThrown,data){
 		    	console.log('Error in loading e-signatures ' );
 		       
 		    }
 		});
 		
    }
    

var selfData;
var spsData;
var advData;
var mgrData;
function setAllSign(data){
	fnaSignData=data;
	var mailtrigger=0;
	for(var i=0;i<data.length;i++){
		setSignPersonData(data[i]);
		
		/*if(data[i].signPerson == "CLIENT"){
			
			toastr.clear($('.toast'));
			
			toastr.options = {
			  "closeButton": true,
			  "debug": false,
			  "newestOnTop": false,
			  "progressBar": true,
			  "positionClass": "toast-top-right",
			  "preventDuplicates": true,
			  "showDuration": "1100",
			  "hideDuration": "1000",
			  "timeOut": "5000",
			  "extendedTimeOut": "1000",
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
			}
		
			toastr["success"]("Client(1) signature is captured successfully!");
				
			mailtrigger+=1;
		}
		
		if(data[i].signPerson == "SPOUSE"){
			
			toastr.clear($('.toast'));
			
			toastr.options = {
			  "closeButton": true,
			  "debug": false,
			  "newestOnTop": false,
			  "progressBar": true,
			  "positionClass": "toast-top-right",
			  "preventDuplicates": true,
			  "showDuration": "1100",
			  "hideDuration": "1000",
			  "timeOut": "5000",
			  "extendedTimeOut": "1000",
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
			}
		
			toastr["success"]("Client(2) signature is captured successfully!");
			
			mailtrigger+=1;
		}
		
		if(data[i].signPerson == "ADVISER"){
			
			toastr.clear($('.toast'));
			
			toastr.options = {
			  "closeButton": true,
			  "debug": false,
			  "newestOnTop": false,
			  "progressBar": true,
			  "positionClass": "toast-top-right",
			  "preventDuplicates": true,
			  "showDuration": "1100",
			  "hideDuration": "1000",
			  "timeOut": "5000",
			  "extendedTimeOut": "1000",
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
			}
		
			toastr["success"]("Adviser signature is captured successfully!");
			
			mailtrigger+=1;
		}*/
		
		
		if(data[i].signPerson == "MANAGER"){
			
			/*toastr.clear($('.toast'));
			
			toastr.options = {
			  "closeButton": true,
			  "debug": false,
			  "newestOnTop": false,
			  "progressBar": true,
			  "positionClass": "toast-top-right",
			  "preventDuplicates": true,
			  "showDuration": "1100",
			  "hideDuration": "1000",
			  "timeOut": "5000",
			  "extendedTimeOut": "1000",
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
			}*/
		
			//toastr["success"]("Manager signature is captured successfully!");
			
			mailtrigger+=1;
		}
		
		
	}
	
	if(mailtrigger >=2){
		//$("#mailModal").modal("show");
	}
}
function setSignPersonData(signData){
	var signPerson=signData.signPerson;
	if(signPerson == "CLIENT"){
		     selfData=signData;
			setViewSign(signData,"client1Qr","selfSignDate");
		}if(signPerson == "SPOUSE"){
			spsData=signData;
			setViewSign(signData,"client2Qr","spsSignDate")
		}if(signPerson == "ADVISER"){
			advData=signData;
			setViewSign(signData,"advQr","advSignDate");
		}if(signPerson == "MANAGER"){
			mgrData=signData;
			setViewSign(signData,"mgrQr","mgrSignDate");
			var supreFlg=$('input[name="suprevMgrFlg"]:checked').val();
			if(supreFlg!=undefined){
			}
		}
}
function setViewSign(signData,roleQr,signDateId){
	var byteArray=signData.signDocBlob;
			if((byteArray != "")&&(byteArray != null)){
 		    	$("."+roleQr).attr('src', 'data:image/png;base64,'+byteArray);
 			 }else{
	             $("."+roleQr).attr('src', 'vendor/avallis/img/contract.png');
			 }
 	 $("#"+signDateId).text(formatDate(signData.signDate));   	
}
function selfSignData(){
	if(selfData != undefined){
		setSignData(selfData,'selfSignId',selfDiagramObj,'selfSavedDiagram');
	}
	
}
function advSignData(){
	if(advData != undefined){
	setSignData(advData,'advSignId',advDiagramObj,'advSavedDiagram');
	}
	
}
function mgrSignData(){
	    if(mgrData!=undefined){
			setSignData(mgrData,'mgrSignId',mgrDiagramObj,'mgrSavedDiagram');
		}
		
}
function spsSignData(){
	    if(spsData!=undefined){
			setSignData(spsData,'spsSignId',spsDiagramObj,'spsSavedDiagram');
		}
	
}
function setSignData(signData,signId,diagramObj,savedDiagram){
	if(signData.esignId!=undefined){
		document.getElementById(signId).value=signData.esignId;
	}
	try {
		   
	        var json =JSON.parse(signData.eSign);
	        diagramObj.initialPosition = go.Point.parse(json.position || "-5 -5");
	        diagramObj.model = go.Model.fromJson(json.model);
	        diagramObj.model.undoManager.isEnabled = true;
	        document.getElementById(savedDiagram).value=signData.eSign;
	    } catch (ex) {
	        alert(ex);
	      }
}

function genQrOrSign(personType,id){
	if(personType == "CLIENT"){
		if(selfData != undefined){
			hideQrCode(id);
			selfInit('selfDiagram','selfSavedDiagram');setClientType('CLIENT');
			setSignData(selfData,'selfSignId',selfDiagramObj,'selfSavedDiagram');
		}else{
			hideSignPad(id);
		    getQrCode('CLIENT','qrCodeClient1','signLoaderClient1');
		}
	}else if(personType == "SPS"){
		if(spsData != undefined){
			hideQrCode(id);
			spsInit('spsDiagram','spsSavedDiagram');setClientType('SPOUSE');
			setSignData(spsData,'spsSignId',spsDiagramObj,'spsSavedDiagram');
		}else{
			hideSignPad(id);
		    getQrCode('SPS','qrCodeClient2','signLoaderClient2');
		}
	}else if(personType == "ADVISER"){
		if(advData != undefined){
			hideQrCode(id);
			advInit('advDiagram','advSavedDiagram');setClientType('ADVISER')
			setSignData(advData,'advSignId',advDiagramObj,'advSavedDiagram');
		}else{
			hideSignPad(id);
		    getQrCode('ADVISER','qrCodeAdviser','signLoaderAdviser');
		}
	}else if(personType == "MANAGER"){
		if(mgrData != undefined){
			hideQrCode(id);
			mgrInit('mgrDiagram','mgrSavedDiagram');setClientType('MANAGER');
			setSignData(mgrData,'mgrSignId',mgrDiagramObj,'mgrSavedDiagram');
		}else{
			hideSignPad(id);
		    getQrCode('MANAGER','qrCodeManager','signLoaderManager');
		}
	}
}
function hideQrCode(id){
	document.getElementById("signSec"+id).style.display="block";
	document.getElementById("qrCodeSec"+id).style.display="none";
}
function hideSignPad(id){
	document.getElementById("signSec"+id).style.display="none";
	document.getElementById("qrCodeSec"+id).style.display="block";
}
function generateQrCode(perType,id){
	    hideSignPad(id);
		getQrCode(perType,'qrCode'+id,'signLoader'+id);
}
//End qr code