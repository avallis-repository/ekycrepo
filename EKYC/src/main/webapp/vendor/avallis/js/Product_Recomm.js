


var jsnDataProdRecomDetls = "",jsnDataFnaDetails ="";
var typeaheaddata = []
		var principalnull="-No Principal Data-"

$( document ).ready(function() {
	
	
	// poovathi add on 17/8/21 enable or disable prod recomm screen based on App Types Value
	
	//showHideprodRecomAndSwitchScrn();
	
	 $("input:radio[name=recomPpBasrid]:first").prop('checked', true);
	 
	//Select 2 Fields Initialization in Page6
    $('#recomPpName').select2({
  	 	 placeholder: "Select a Client Name",
		  width : '100%',
		//  templateResult: loadSelect2ClntNameCombo,
		  allowClear: true,
		  tags: true,
			selectOnClose: true,
			tokenSeparators: [ '\n','\t'],
			createTag: function (params) {
			    var term = $.trim(params.term);

			    if (term === '') {
			      return null;
			    }
			
			    return {
			      text: term.toUpperCase(),
			      newTag: true 
			    }
			  }
		  });


    
   $('#recomPpProdname').select2({
		placeholder: "Keyin the Product Name",
		width : '100%',
		allowClear: true,
		tags: true,
		selectOnClose: true,
		language: {"noResults": function(){ return "--Keyin the Product Name--"; } },
		tokenSeparators: [',', '\n','\t'],
		maximumInputLength : 150,
		createTag: function (params) {
		    var term = $.trim(params.term);

			if(isEmpty($('#recomPpPrin').val())){return null;}

			var prodcode = makeid(12);
			
			//alert("prodcode 1"+prodcode)
			
			// var prodcode = term.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,'_');
			 prodcode = prodcode.replace(/\s+/g, '_');
			 prodcode = prodcode.toUpperCase();
			 
			 
			 //alert("prodcode"+prodcode)
		
		    if (term === '') {
		      return null;
		    }
		
		    return {
		      id: prodcode,
		      text: term,
		      newTag: true // add additional parameters
		    }
		  }
	});
		
	$('#recomPpPrin').select2({
		  placeholder: "Select an Insurance Company / Principal",
		  width : '100%',
		  templateResult: loadSelect2PrinCombo,
		  allowClear: true
		  });
	
	var prodDetsObj = jsnDataProdRecomDetls.length;
    var emptObjFlg = isEmptyObj(jsnDataProdRecomDetls);
	
	if(emptObjFlg == true){
		$("#NoProdDetlsRow").removeClass("hide").addClass("show");
		hidePrimaryAddProdRecommBtn();
	}else if(emptObjFlg == false){
		$("#NoProdDetlsRow").removeClass("show").addClass("hide");
		showPrimaryAddProdRecommBtn();
	}
	
	
	  if(!isEmpty(jsnDataFnaDetails)  && !isJsonObjEmpty(jsnDataFnaDetails)){
		  for(var obj in jsnDataFnaDetails){
			  if (jsnDataFnaDetails.hasOwnProperty(obj)) {
				  var objValue = jsnDataFnaDetails[obj];
				  
				  var nodetype = $("#"+obj).prop('nodeName');
				  var inputtype = $("#"+obj).prop("type");
				  
				  switch(obj){
				  
				  
				  case "prodrecObjectives":
					  var data=JSON.parse(objValue);
                      setJsonObjToElm(obj,data);
                      break;
                      
				  case "ccRepexistinvestflg":
		        	     if(objValue == "Y"){
		        	    	 $("#prodSwtchRecommMenu").removeClass("hide").addClass("show"); 
		        	    	 $("#prodSwtchRecommMenu").addClass("d-block").removeClass("d-none");
		        	    	 $("#prodRecommMenu").removeClass("show").addClass("hide");//prod recomm menu not appear 
						 }else if (objValue == "N"){
							 $("#prodRecommMenu").removeClass("hide").addClass("show");
							 $("#prodSwtchRecommMenu").removeClass("show").addClass("hide"); 
							 $("#prodSwtchRecommMenu").addClass("d-none").removeClass("d-block");
						  }
		    	 break;
                      
                  default:		
					
					 if(inputtype == "radio" || inputtype == "checkbox"){
						  if(objValue == "Y"){
							  $("#"+obj).prop("checked",true);
						  }
					  }
					  
					  $("#"+obj).val(objValue);
				  }
				  
			  }
			  
		  }
	  }
	
	
	
    if(!isEmpty(jsnDataProdRecomDetls)){
    	 for(var prodkey in jsnDataProdRecomDetls){    			  
    		if (jsnDataProdRecomDetls.hasOwnProperty(prodkey)) {

        		var prodVal = jsnDataProdRecomDetls[prodkey];
//        		console.log("prodVal",prodVal)
        	    var  strrecomPpId ="",strClntName = "",strCompName = "";strProdType="";
        		$.each(prodVal,function(key,val){
        			
        			switch(key){
		      		 case "recomPpName":
		      			if(isEmpty(val)) {
		      				strClntName=seesselfName.toUpperCase();
		      			}else {
		      				strClntName = val;	
		      			}
						//typeaheaddata.push(strClntName);
						//if (typeaheaddata.includes(strClntName) === false) typeaheaddata.push(strClntName);
		      			//console.log(strClntName ,"=" ,strClntName,"ekyc")
		      			if(!chkOptionExists("recomPpName",strClntName)) {
		      				addOption("recomPpName",strClntName.toUpperCase(),strClntName.toUpperCase())
		      			}
		      			
		      			break;
		      		 case "recomPpPrin":		      			
		      			strCompName = val;	      			 
 		      			break;
		      		 case "recomPpBasrid":
		      			strProdType = val; 
		      		 break;
		      		 
		      		 case "recomPpId":
		      			strrecomPpId = val;
 		      		 break;    	    		      			
    		      	   }
        		});
        		creteNewTabFun(strClntName);	
        		createPrincipalCards(prodVal,strCompName,strClntName,strrecomPpId); 
        		
        		//always set self Client (1) Tab Active
        		$('#prodTabs').children().eq(0).find("a.nav-link").trigger("click");
   
    			
    		}
    	  }	
    }



	
  //  $("#recomPpName").typeahead({ source:typeaheaddata});
	var fnaId=$("#hTxtFldCurrFNAId").val();
	getAllSign(fnaId);

	setPageManagerSts();
	setPageAdminSts();
	setPageComplSts();
	//poovathi add on 25-08-2021
	showhidePrevArchKycDetls('divProdRecomFrm');
	
});


//----All Page 6 ralated Script Task wrote below------

//Add / Load Client Name Combo Field

function loadSelect2ClntNameCombo (state) {
	if (!state.id) {
	return state.text;
	}
	
   var clnt = state.element.value.toLowerCase() ;
	var icon = "";
	switch(clnt){

	case "add new client":
	icon = "vendor/avallis/img/user.png";
	break;

	default:
    icon = "vendor/avallis/img/profile.png";
     }
	var $state = $('<span><img src="' + icon +'"> | <span class="font-sz-level5">' + state.text + '</span></span>'
	);
	
	return $state ;
	};

	//Add Images and Combo to ProdName Field
	function loadSelect2PrinCombo (state) {
	    if (!state.id) {
	 return state.text;
	}


	var prin = state.element.value;
	var icon = "";
	switch(prin){
	

	case "Aviva Ltd":
	case "Singapore Life Ltd":
	case "Singapore Life Pte.Ltd":
		//icon = "vendor/avallis/img/prinImg/aviva.png";
			icon = "vendor/avallis/img/prinImg/SinglifewithAviva.png";
		break;
		

	case "NTUC Income":
	icon = "vendor/avallis/img/prinImg/ntucincome.png";
	break;

	case "Manulife Financial":
	case "Manulife (Singapore) Pte Ltd":
	icon = "vendor/avallis/img/prinImg/Manulife_logo.png";
	break;

	/*case "AIG Global Investment Corp S Ltd":
	icon = "vendor/avallis/img/prinImg/aia.jpg";
	break;*/
	
	case "Aberdeen Asset Management Asia Ltd":
	icon = "vendor/avallis/img/prinImg/aberdeen.png";
	break;
	
	case "ABN AMRO Asset Management S Ltd":
	icon = "vendor/avallis/img/prinImg/ABN_Armo.png";
	break;
	
	case "ACE Insurance Ltd":
	icon = "vendor/avallis/img/prinImg/Ace_New_Logo.png";
	break;
	
	case "Allianz Dresdner Asset Management":
	case "Allianz Global Corporate & Specialty AG Singapore Branch":
	case "Allianz Global Investors S Ltd":
	icon = "vendor/avallis/img/prinImg/allianz.svg";
	break;
	
	case "American International Group":
	case "AIG Global Investment Corp S Ltd":		
	icon = "vendor/avallis/img/prinImg/AIG.png";
	break;
	
	case "American International Assurance Co. Ltd":
	case "AIA Singapore Private Limited":
	icon = "vendor/avallis/img/prinImg/AIA_Logo.png";
	break;
	
	
    case "Chartis, American Home Assurance":
		icon = "vendor/avallis/img/prinImg/CHARTIS.png";
		break;
		
	case "China Insurance Co (Singapore) Pte Ltd":
	case "China Life Insurance (Singapore) Pte. Ltd":
	case "China Life Insurance (Singapore) Pte. Ltd.":
		icon = "vendor/avallis/img/prinImg/CHINA_LIFE.png";
		break;
		
	case "China Taiping Insurance (Singapore) Pte Ltd":
	case "China Taiping Insurance":
		icon = "vendor/avallis/img/prinImg/CHINATAP.png";
		break;
		
	case "CIGNA International":
		icon = "vendor/avallis/img/prinImg/CIGNA.png";
		break;
		
	case "Commerzbank Asset Management Asia L":
		icon = "vendor/avallis/img/prinImg/COMMERZ.png";
		break;
		
	case "DBS Asset Management":
	case "Development Bank of Singapore":
		icon = "vendor/avallis/img/prinImg/DEVBank.png";
		break;
		
	case "Deutsche Asset Management Asia Ltd":
		icon = "vendor/avallis/img/prinImg/DEUT.png";
		break;
		
	case "EQ Insurance Company Limited":
		icon = "vendor/avallis/img/prinImg/EQINS.png";
		break;
		
	case "Federal Insurance Company":
		icon = "vendor/avallis/img/prinImg/AgeasFederal.jpg";
		break;
		
	case "Fidelity Investments Singapore Ltd":
		icon = "vendor/avallis/img/prinImg/Fidelity.png";
		break;
		
	case "First Capital Insurance Ltd":
		icon = "vendor/avallis/img/prinImg/FIRCap.jpg";
		break;
		
	case "First State Investments Singapore":
		icon = "vendor/avallis/img/prinImg/FSI.png";
		break;
		
	case "Friends Provident International Limited (Singapore Branch)":
		icon = "vendor/avallis/img/prinImg/FPI_Logo.png";
		break;
		
	case "Great Eastern Life Assurance":
	case "Great Eastern Life":
		icon = "vendor/avallis/img/prinImg/GE.png";
		break;
		
	case "Henderson Global Investors S Ltd":
		icon = "vendor/avallis/img/prinImg/HGIS.png";
		break;
		
	case "Hongkong Shanghai Banking Corporation":
	case "HSBC Insurance (Singapore) Pte Ltd":
	case "HSBC Investments":
		icon = "vendor/avallis/img/prinImg/HBSC.png";
		break;
		
	case "iFAST Financial":
		icon = "vendor/avallis/img/prinImg/Ifast.png";
		break;
		
	
		
		
	
	
	case "India International Insurance Pte Ltd":
		icon = "vendor/avallis/img/prinImg/india_International.jpeg";
		break;
		
	case "InterGlobal Insurance Company Limited":
		icon = "vendor/avallis/img/prinImg/Inter-Global.jpg";
		break;
		
		
	case "International SOS Pte Ltd":
		icon = "vendor/avallis/img/prinImg/inter_SOS.png";
		break;
		
		
	case "Keppel Insurance Pte Ltd":
		icon = "vendor/avallis/img/prinImg/swfi-keppal.svg";
		break;
		
		
	case "Legg Mason Asset Management Asia":
		icon = "vendor/avallis/img/prinImg/leg_mason.png";
		break;
		
	case "Liberty Insurance":
		icon = "vendor/avallis/img/prinImg/liberty.png";
		break;
	
	case "AXA Insurance Singapore Pte Ltd":
	case "AXA Life Insurance (S) Pte Ltd":
	icon = "vendor/avallis/img/prinImg/AXA.png";
	break;
		
	case "Catlin Singapore Pte Ltd (Lloyds)":
	icon = "vendor/avallis/img/prinImg/Catlin_Sig.png";
	break;
		
	case "CGU International Insurance plc":
	icon = "vendor/avallis/img/prinImg/CGU_logo.svg";
	break;
	
	
	case "Life Insurance Corporation (Singapore) Pte Ltd":
		icon = "vendor/avallis/img/prinImg/LICS.jpg";
		break;
	
	case "MSIG Insurance (Singapore) Pte Ltd":
	case "Mitsui Sumitomo Insurance (Singapore) Pte Ltd":
		icon = "vendor/avallis/img/prinImg/msig_logo.png";
		break;
	
	case "QBE Insurance International Ltd":
		icon = "vendor/avallis/img/prinImg/QBE.png";
		break;
		
	case "Raffles Health Insurance Pte Ltd":
		icon = "vendor/avallis/img/prinImg/Raffle.png";
		break;
		
	case "Royal & SunAlliance Insurance (Singapore) Limited":
		icon = "vendor/avallis/img/prinImg/Royal_logo.jpeg";
		break;
		
	case "Schroder Investment Mgt Ltd":
		icon = "vendor/avallis/img/prinImg/scholders.png";
		break;
		
	case "SG Asset Management":
		icon = "vendor/avallis/img/prinImg/SG_logo.png";
		break;
		
	case "SHC Insurance Pte Ltd":
		icon = "vendor/avallis/img/prinImg/SHC_Logo.jpg";
		break;
	
	case "Standard Chartered":
		icon = "vendor/avallis/img/prinImg/stand_charted.png";
		break;

	case "Templeton Asset Management Ltd":
		icon = "vendor/avallis/img/prinImg/templetion_logo.jpeg";
		break;
		
	case "Tenet Insurance Company Ltd":
		icon = "vendor/avallis/img/prinImg/tenet_logo.jpeg";
		break;
		
	case "The Overseas Assurance Corporation Limited":
		icon = "vendor/avallis/img/prinImg/OAC.jpg";
		break;
		
	case "Tokio Marine Insurance Singapore Ltd":
	case "TM Asia Life Singapore Ltd":
	case "TM Asia Insurance Singapore Ltd":
	case "Tokio Marine Life Insurance":
	case "The Tokio Marine & Fire Insurance Company (S) Pte Ltd":	
		icon = "vendor/avallis/img/prinImg/tm-logo.png";
		break;
		
	case "Transamerica Occidental Life Insurance Company":
		icon = "vendor/avallis/img/prinImg/transamerica-logo.svg";
		break;
		
	case "United Overseas Bank Ltd":
	case "Overseas Union Insurance Limited":
		icon = "vendor/avallis/img/prinImg/uob-logo.jpg";
		break;
		
	case "United Overseas Insurance Limited":
		icon = "vendor/avallis/img/prinImg/UOI.jpeg";
		break;
	
	case "UOB Asset Management Ltd":
	icon = "vendor/avallis/img/prinImg/uobAsset-logo.jpg";
	break;
	
	case "UOB Life Assurance Ltd":
	icon = "vendor/avallis/img/prinImg/UOB.png";
	break;
	
	case "Walton Internation Group (S) Pte Ltd":
	icon = "vendor/avallis/img/prinImg/Walton_logo.png";
	break;
	
	case "Zurich Insurance (Singapore) Pte Ltd":
	icon = "vendor/avallis/img/prinImg/zurich.png";
	break;
		
	//poovathi add on 14-06-2021
	case "Prudential":
	case "Prudential Asset Management S Ltd":	
	icon = "vendor/avallis/img/prinImg/prudentials.png";
	break;
	
	
//	case "Singapore Life Pte.Ltd":
//	icon = "vendor/avallis/img/prinImg/singapore_life.png";	
//	break;
	
	
	case "Swiss Life (Singapore) Pte Ltd":	  
		icon = "vendor/avallis/img/prinImg/swiss_life.png";	
		break;
	
	default:
	icon="vendor/avallis/img/prinImg/noimage.png";
	}

	var $state = $('<span><img src="' + icon +'" class="img-flag" style="width:100px;height:30px;"/> &nbsp; | &nbsp; <span class="font-sz-level5">' + state.text + '</span></span>'
	);
	return $state;
	};	
	
	

// Add Validation for all Key fields  in page 6 

// Client Name Validation 
  $("#recomPpName").change(function(){

	//Clear All Form Fields Except Client Name 
	  clrProdFrmDetlsExceptClntName(); 
	  clrProdLob();
	  clrProdPpProductCombo();
	  hideAddBscRdrPlnBtn();    
	  enableBscprodType();
	  $("#hdntxtPlntype").text("Basic Plan");
	  clrselFldBscPlanCombo();
	  hideBscPlanCombo();
	  
	 var ClntName = $(this).val();
      $("#hdnBscTextClnt").text(ClntName);
      $("#hdnBscTextComp").text("Select Principal below");
     
      
	if(isEmptyFld(ClntName)){
		$("#txtFldClntNameError").removeClass("hide").addClass("show");
		$("#recomPpName").addClass('err-fld');
		return;
	}else{
		$("#txtFldClntNameError").removeClass("show").addClass("hide");
		$("#recomPpName").removeClass('err-fld');
		  if(ClntName == "Add New Client"){
			        //$("#txtFlddropAddNewClient").val("");
					//$("#dropdwnSec").addClass("show").removeClass("hide");
			      // addNewDepntClnt();
			      
				}
		}
	
	//Add class active to particular client
	var totlClntTabLen = $('#prodTabs').find("a.nav-link").length;
	   
	if(totlClntTabLen > 0){
		
		 $('#prodTabs').find("a.nav-link").each(function(){
			var curtntTabId = $(this).attr("id");
			ClntName = ClntName.replace(/[&\/\\#,+()@!$~%.'":*?<>{}]/g,'_');
			ClntName = ClntName.replace(/[\s+\/]/g, '_');
			 if(curtntTabId == ClntName){
				 var curtntTabIdobj = $(this);
				 $(this).addClass("active");
				 $('#prodTabs').find("a.nav-link").not(this).removeClass("active show");
				 openThisTabContArea(curtntTabIdobj);
				 }
		 });
	}
	
 });	


var basicproducts = [];
var riderproducts = [];
var benefproducts = [];
var fundproducts =  [];


 $("#recomPpPrin").on("change",function(){
	 
	 clrProdRelatedFrmDetls();//clear all prod related fields
	 clrProdLob();
	 enableClrClnt();
	 basicproducts.length=0;
	 riderproducts.length=0;
	 benefproducts.length=0;
	 fundproducts.length=0;
	
	 var prinname = $(this).val();
	 
	
	 var printxt = $("#recomPpPrin option:selected").text();
	 $("#hdnBscTextComp").text(prinname);
	 
	
	 
	 if(isEmptyFld(prinname)){
			$("#recomPpPrinError").removeClass("hide").addClass("show");
			$("#recomPpPrin").addClass('err-fld');
			return;
		}else{
		   $("#recomPpPrinError").removeClass("show").addClass("hide");
		   $("#recomPpPrin").removeClass('err-fld');
		}
	 
	  var optgroup = $('<optgroup>');
      optgroup.attr('label', "Search Prod.Details");

	 $.ajax({
         url:"productsrecommend/products/prinname/"+prinname,
         async:false,
         type: "GET",
         dataType: "json",
         contentType: "application/json",
         
         success: function (data) {

	            for (var i=0; i<data.length; i++) {
	            	
	            	
	            	if(data[i].prodType  == "B"){
	            		basicproducts[basicproducts.length++]=data[i];
	            	}else if(data[i].prodType  == "R"){
	            		riderproducts[riderproducts.length++]=data[i];
	            	}else if(data[i].prodType  == "A"){
	            		benefproducts[benefproducts.length++]=data[i];
	            	}else if(data[i].prodType  == "F"){
	            		fundproducts[fundproducts.length++]=data[i];
	            	}
	            }

         },
         error: function(xhr,textStatus, errorThrown){
	 			//$('#cover-spin').hide(0);
	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			}
         });
	 
	 
	 
	// hide Basic Plan Combo if prod Type is Basic
	 $("#selFldBscPlanComboSec").removeClass("show").addClass("hide");
	 prinname = prinname.replace(/[&\/\\#,+()@!$~%.'":*?<>{}]/g,'_');	 	 
	 prinname = prinname.replace(/\s+/g, '_');
	 
	var strClntName = $("#recomPpName option:selected").val();//$("#recomPpName").val();//$("#recomPpName option:selected").val();
	    
	    strClntName = strClntName.replace(/[&\/\\#,+()@!$~%.'":*?<>{}]/g,'_');
	    strClntName = strClntName.replace(/[\s+\/]/g, '_');
	 // show  Add Rider,Add Basic  Plan Button when selected Principal have one Basic Plan
		 showAddBscRdrPlnBtn();	
	
	var nofBscPlsInPrinCount = $('#prodTabContent').children('#'+strClntName).find("div#"+prinname+"cardDetls").children(".PrinpalCrdBodyCont").find(".RiderPlans").length;
	
	 if(nofBscPlsInPrinCount == 0){
		 enableBscprodType();
		 hideAddBscRdrPlnBtn();    
		 hideBscPlanCombo();
	     $("#hdntxtPlntype").text("Basic Plan");
	 }
	 if(nofBscPlsInPrinCount >= 1){
		 showAddBscRdrPlnBtn(); 
		 clrselFldBscPlanCombo();
		 
		 $('#prodTabContent').children('#'+strClntName).find("div#"+prinname+"cardDetls").children(".PrinpalCrdBodyCont").children("").each(function(){
				
				var prodVal = $(this).find('input:hidden[name=hdnFldProdVal]').val();
				
				var prodtxt = $(this).find('input:hidden[name=BscProdName]').val();
				
				var prodLob = $(this).find("input:hidden[name=hTxtFldProdMProdLob]").val();
//				console.log("prodLob------>"+prodLob)
				 
				var NoofBscPlanIds = $(this).find(".card-body").attr("id");
				var strprodstring = '<option value="'+prodVal+'">'+prodtxt+'</option>';
				var strprodstringLob = '<option value="'+prodVal+'">'+prodLob+'</option>';
					$("#selFldBscPlanCombo").append(strprodstring);
					$("#selFldBscProdLobCombo").append(strprodstringLob);
					
		  });
		 
		 var selectFldId = document.getElementById("selFldBscPlanCombo");
		 var selectOptLen = selectFldId.options.length;
		 
		
		 
		 if(selectOptLen == 2){
			  var selectFldId = document.getElementById("selFldBscPlanCombo");
			  var strBscPlnFrstProdVal =  selectFldId.options[1].value;
			  $("#selFldBscPlanCombo").val(strBscPlnFrstProdVal);
			  $("#recomPpBasicRef").val(strBscPlnFrstProdVal)
			  $("#selFldBscPlanCombo").trigger('change');
			 
		 }
		 
		 if(selectOptLen > 2){
			 $("#selFldBscPlanCombo").val("");
			 $("#selFldBscPlanCombo").trigger('change');
		 }
		 
	 }
	 
	 var strFnaId = $("#fnaDetails").val();
	 var basorrider = $('input[name="recomPpBasrid"]:checked').val();
	 
	
	 if(basorrider == "BASIC"){
		 hideBscPlanCombo();
		 clrProdPpProductCombo();
		 $("#hdntxtPlntype").text("Basic Plan");
		 for (var i=0; i<basicproducts.length; i++) {
		     var option = $("<option></option>");
			 option.val(basicproducts[i].prodCode);
			 option.text(basicproducts[i].planName +" [Plan Code:"+basicproducts[i].planCode+"]");
			 $('#recomPpProdname').append(option);
		 }
		 
	   }else if(basorrider == "RIDER"){
		   
		 showBscPlanCombo(); 
		 clrProdPpProductCombo();
		 $("#hdntxtPlntype").text("Rider Plan");
		  for (var i=0; i<riderproducts.length; i++) {
		     var option = $("<option></option>");
			 option.val(riderproducts[i].prodCode);
			 option.text(riderproducts[i].planName +" [Plan Code:"+riderproducts[i].planCode+"]");
			 $('#recomPpProdname').append(option);
		 }
		 
	 }
	 
	 if($("#hdnAction").val() == "E"){
		  hideAddBscRdrPlnBtn();
		  hideBscPlanCombo(); 
		  disableClrClnt();
		  
		 
	 }
	 
	 
 });
 



		   
		   
//Product LOB Type Validation - Enable product Risk Rate Field Based On Product LOB
$('input[type=radio][name=recomPpProdtype]').change(function() {
	var strProdLob = this.value;
	    if (strProdLob == "ILP"){
	    	$("#prodRiskRate").removeAttr('disabled');
	    }
	    
	    if (strProdLob == 'PA' || strProdLob == "Life" || strProdLob == "H&S"){
	    	$("#prodRiskRate").val('');
	    	$("#prodRiskRate").prop('disabled','true');
	    }
	    
	    if(isEmptyFld(strProdLob)){
			$("#radBtnprodLobError").removeClass("hide").addClass("show");
			return ;
		}else{
			$("#radBtnprodLobError").removeClass("show").addClass("hide");
		}
	});


// Product Name Validation
 $("#recomPpProdname").change(function(){
	 
	var strProdName = $(this).val();
	 //var strProdName = makeid(12);
	 
	 //var prodcode = makeid(12);
		//alert("prodcode 4"+prodcode)
	//alert(strProdName)
	var strProdText = $("#recomPpProdname option:selected").text();
	$("#recomPpPlan").val(strProdText);	
	
	var strCompNameOrg = $("#recomPpPrin").val();
	var strCrntBscPlan =  $("#selFldBscPlanCombo").val();
	$("#recomPpBasicRef").val(strCrntBscPlan);
	
	var strCompName = strCompNameOrg.replace(/[&\/\\#,+()@!$~%.'":*?<>{}]/g,'_');
	strCompName = strCompName.replace(/[\s+\/]/g, '_');
	
	var strClntName =$("#recomPpName option:selected").val();// $("#recomPpName").val();;//$("#recomPpName option:selected").val();
	 strClntName = strClntName.replace(/[&\/\\#,+()@!$~%.'":*?<>{}]/g,'_');
    strClntName = strClntName.replace(/[\s+\/]/g, '_');
	
    
	var strUnqId = $("#hdnUnqId").val();
	
    if(isEmptyFld(strProdName)){
		$("#recomPpProdnameError").removeClass("hide").addClass("show");
		$(this).closest('span').find(".select2-container--default .select2-selection--single").addClass('err-fld');
		return;
    }else{
		$("#recomPpProdnameError").removeClass("show").addClass("hide");
		$(this).closest('span').find(".select2-container--default .select2-selection--single").removeClass('err-fld');
	}
    
   //check Product Name Duplication in principal, when product type is Basic
    var prodTye = $('input[name="recomPpBasrid"]:checked').val();
      
        var ProdArr = [],UnqIDArr = [];
        
       if(prodTye == "BASIC"){
    	   var id = "RiderPlans";
    	   var prodhdnVal = "hdnTxtprodValue";
    	   var prodhdnTxt = "BscProdName";
    	   var hdnQnqId = "hdnUnqdId";
    	   var PrdType = "Basic";
    	   var productmsg = strCompNameOrg+" Principal";
    	}
       
       if(prodTye == "RIDER"){
    	   var BscPlnprdVal;
    	   
    	   if(($("#hdnAction").val() == "I")){
    		   BscPlnprdVal=$("#selFldBscPlanCombo").val(); 
    		   $("#recomPpBasicRef").val(BscPlnprdVal);
    		   enableClrClnt();
    		   
    		   
    	   }
    	   
    	  if(($("#hdnAction").val() == "E")){
    		   var bscsetId = $("#hEdtBscSetId").val();
    		   BscPlnprdVal = $('#prodTabContent').children('#'+strClntName).find("div#"+strCompName+"cardDetls").children(".PrinpalCrdBodyCont").find("div#"+bscsetId).find('input:hidden[name="hdnTxtprodValue"]').val();
    		   $("#recomPpBasicRef").val(BscPlnprdVal);
    		   disableClrClnt();
    	      }
    	  
       var id = BscPlnprdVal+"RdrParentSet";
   	   var prodhdnVal = "hdnprodVal";
   	   var prodhdnTxt = "RdrProdName";
   	   var hdnQnqId = "hdnUnqdId";
   	   var PrdType = "Rider";
   	   var productmsg = "Basic Plan";
   	   
    	}
   
      $('#prodTabContent').children('#'+strClntName).find("div#"+strCompName+"cardDetls").children(".PrinpalCrdBodyCont").find("div."+id).each(function(){
    		        var prodVal = $(this).find('input:hidden[name='+prodhdnVal+']').val();
    				var prodtxt = $(this).find('input:hidden[name='+prodhdnTxt+']').val();
    				var UnqID =  $(this).find('input:hidden[name='+hdnQnqId+']').val();
    				
    				if(!ProdArr.includes(prodVal)){
      					ProdArr.push(prodVal);
      				 }
    				
    				if(!UnqIDArr.includes(UnqID)){
    					UnqIDArr.push(UnqID);
      				 }
    		});
    	 
      
       var strOldPlnName =  $("#hEtdPlnVal").val();
 	   var strUnqIdVal =  $("#hdnUnqId").val();
 	  if(($("#hdnAction").val() == "E")){
    	  hideAddBscRdrPlnBtn();
    	  disableClrClnt();
    	 //(ProdArr.includes(strOldPlnName))&&
    	  if(ProdArr.includes(strProdName)){
    		  //command by vignesh on 26-05-2021
        		//   $("#errInfoSameProd").html("This "+PrdType+" Product  already Recommended in " +strCompName+ " Principal, Please Keyin the different product !!" );
        	    //   $("#errInfoSameProd").addClass('show').removeClass("hide");
        	       setTimeout(function(){
        	    	   //command by vignesh on 26-05-2021
        	    	   //$('#recomPpProdname').val('');
      	           

					/*$('#recomPpProdname').select2({
    				   placeholder: "Select a Product Name  below",
    				   width : '100%',
    				   });*/

   $('#recomPpProdname').select2({
		placeholder: "Keyin the Product Name",
		width : '100%',
		allowClear: true,
		tags: true,selectOnClose: true,
		tokenSeparators: [',', '\n','\t'],
		language: {"noResults": function(){ return "--Keyin the Product Name--"; } },
		maximumInputLength : 150,
		createTag: function (params) {
		    //addNewProdNameToCombo(params);

		var term = $.trim(params.term);

			if(isEmpty($('#recomPpPrin').val())){return null;}
//alert(term)
			var prodcode = makeid(12);
			//alert("prodcode 2"+prodcode)
			//var prodcode = term.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,'_');
			                 			 prodcode = prodcode.replace(/\s+/g, '_');
			                 			 prodcode = prodcode.toUpperCase();
			                 			 
			                 			//alert(prodcode)
		
		    if (term === '') {
		      return null;
		    }
		
		    return {
		      id: prodcode,
		      text: term,
		      newTag: true // add additional parameters
		    }
		  }
	});


					 },800);//$("#errInfoSameProd").addClass('hide').removeClass("show");
        	     }else{
        	    	$("#errInfoSameProd").addClass('hide').removeClass("show");
        	      } 
    	 }
      
      
      
     
      if($("#hdnAction").val() == "I"){
 			var nofBscICount = $('#prodTabContent').children('#'+strClntName).find("div#"+strCompName+"cardDetls").children(".PrinpalCrdBodyCont").find(".RiderPlans").length;
 			 
 			 if(nofBscICount > 0 ){
 				// show  Add Rider Plan Button when selected Principal have more than one Basic Plans
 			     showAddBscRdrPlnBtn();
 			 }
 			 
           if(ProdArr.includes(strProdName)){
        	   //command by vignesh on 26-05-2021
        	// $("#errInfoSameProd").html("This "+PrdType+" Product  already Recommended in " +productmsg+ " , Please keyin the different "+PrdType+" product !!" );
      	    //   $("#errInfoSameProd").addClass('show').removeClass("hide");
      	       setTimeout(function(){
      	    	 //command by vignesh on 26-05-2021
      	    	   //$('#recomPpProdname').val('');

    	           /*$('#recomPpProdname').select2({
  				   placeholder: "Select a Product Name  below",
  				   width : '100%',
  				   });*/

  $('#recomPpProdname').select2({
		placeholder: "Keyin the Product Name",
		width : '100%',
		allowClear: true,
		tags: true,selectOnClose: true,
		tokenSeparators: [',', '\n','\t'],
		language: {"noResults": function(){ return "--Keyin the Product Name--"; } },
		maximumInputLength : 150,
		createTag: function (params) {
		   //addNewProdNameToCombo(params);

var term = $.trim(params.term);

			if(isEmpty($('#recomPpPrin').val())){return null;}

			var prodcode = makeid(12);
			//alert("prodcode 3"+prodcode)
			//var prodcode = term.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,'_');
			                 			 prodcode = prodcode.replace(/\s+/g, '_');
			                 			 prodcode = prodcode.toUpperCase();
		
		    if (term === '') {
		      return null;
		    }
		
		    return {
		      id: prodcode,
		      text: term,
		      newTag: true // add additional parameters
		    }

		  }
	});

 },800);//$("#errInfoSameProd").addClass('hide').removeClass("show");
      	     }else{
      	    	$("#errInfoSameProd").addClass('hide').removeClass("show");
      	      }
        	 
         }
});

//click to trigger Add product Recomm Primary PopUp Form.
 $("#badgeAddProdRecomm").click(function(){
	 openPrimaryRecommPopUp();
 }); 
 
 //click to trigger Add product Recomm Primary PopUp Form in card-body(if no prod detls Available).
 $("#btnAddRecommDetls").click(function(){
	 openPrimaryRecommPopUp();
 }); 
 
 
 function openPrimaryRecommPopUp(){
	 
	 saveCurrentPageData($("#btnNextFormUniq"),$("#applNextScreen").val(),false,false);
	 
		//check checkdb onchange function for Adding prod Details
		 enableReSignIfRequiredforEdtDeltAction();
		 
		 //objective Sec Validation
		  if(!validateObjSec()){return;}
		   
		//clear alert message content
		 clrProdAlertInfoMsg();
		 
		 //enable to add client btn
		 enableClrClnt();
		 
		 
		 //remove attr disabled from clnt and compName
		   RmveDisbleAttrClntCompProdPln();
		  
		//show clear form Btn
		  $("#btnClrFrmFdls").removeClass("hide").addClass("show");
		  
		// change Modal Header img to add image 
		  $("#mdlHdrImg").attr("src","vendor/avallis/img/addins.png");
		  
		// Open Modal PopUp
		   OpenProdRecommMdl();
		 	
		// hide if Add Dependent DropDown Exists  
		 $("#dropdwnSec").addClass("hide").removeClass("show");
		 
		// hide if prod same -->error shows
		 $("#errInfoSameProd").addClass("hide").removeClass("show");
		 
		//hide Noti Box (Green Box)
		 $("#bscRdrNotInfo").removeClass("show").addClass("hide");
		 
		 //clr all frm fields
		 ClrProdFrmDetls(); 
	   
		 //set Active tab name in ClientName Combo
		  setClientNameCombo();
		  
		  //initialy hide Add Bsc ,Rdr plan btn 
		  hideAddBscRdrPlnBtn();
		  
		  // Set  ProdType is Basic
		  enableBscprodType();
		  
		  clrprodFldErrMsg();
		  
		 //Hide Basic Plan Combo Box when Prod Type is Basic
		  hideBscPlanCombo();
	 }
 
 function setClientNameCombo(){
	 
	 //tabcount == 0
	 var totlTabLen = $('#prodTabs li').length;
	 
	   if(totlTabLen == 0){
              $("#recomPpName").val($('#dynaClientAddSec > option').eq(0).val());
			  $('#recomPpName').trigger('change.select2'); //$('#recomPpName').trigger('change'); // $('#recomPpName').trigger('change.select2');
	  }
	   // tabcount >0  
	   if(totlTabLen == 1){
		   var strClnt="";
		   $('#prodTabs li a').each(function(){
			 
			 if($(this).hasClass('active')){
				 strClnt = $(this).text();
				 strClnt = strClnt.trim().toUpperCase();
				 //$("#recomPpName").val(strClnt);
				 //alert("strClnt"+strClnt)
				 $("#recomPpName").val(strClnt);
				 $('#recomPpName').trigger('change.select2');//$('#recomPpName').trigger('change');// $('#recomPpName').trigger('change.select2');
			 }
		 });  
	   }
	   
	
     
 }

 //clear Entire Product Form details Reset Action 
 $("#btnClrFrmFdls").click(function(){
	//initialy hide Add Bsc ,Rdr plan btn 
	hideAddBscRdrPlnBtn();
    // Set  ProdType is Basic
	 enableBscprodType();
     ClrProdFrmDetls(); 
 });
 

 function ClrProdFrmDetls(){
   $('#recomPpName').val('');
   $('#recomPpName').select2({
	    	  placeholder: "Select a Client Name",
	    	  width : '100%',
	  		//  templateResult: loadSelect2ClntNameCombo,
	  		  allowClear: true,
	  		  tags: true,
	  			selectOnClose: true,
	  			tokenSeparators: [ '\n','\t'],
	  			createTag: function (params) {
	  			    var term = $.trim(params.term);
	  			  
	  			    if (term === '') {
	  			      return null;
	  			    }
	  			    
	  			
	  			  return {
				      id: term.toUpperCase(),
				      text: term.toUpperCase(),
				      newTag: true 
				    }
	  			  }
			  });
   $('#recomPpPrin').val('');
   $('#recomPpPrin').select2({
			  placeholder: "Select an Insurance Company / Principal",
			  width : '100%',
			  templateResult: loadSelect2PrinCombo,
			  });
   $('input[name="recomPpProdtype"]').prop('checked', false);
   $('#recomPpProdname').val('');
   /*$('#recomPpProdname').select2({
			   placeholder: "Select a Product Name  below",
			   width : '100%',
			  });*/
	  $('#recomPpProdname').select2({
		placeholder: "Keyin the Product Name",
		width : '100%',
		allowClear: true,
		tags: true,selectOnClose: true,
		language: {"noResults": function(){ return "--Keyin the Product Name--"; } },
		tokenSeparators: [',', '\n','\t'],
		maximumInputLength : 150,
		createTag: function (params) {
		    //addNewProdNameToCombo(params);
var term = $.trim(params.term);

			if(isEmpty($('#recomPpPrin').val())){return null;}

			var prodcode = (12);
			//alert("prodcode 44"+prodcode)
			var prodcode = term.replace(/[&\/\\#,+()@!$~%.'":*?<>{}]/g,'_');
			                 			 prodcode = prodcode.replace(/\s+/g, '_');
			                 			 prodcode = prodcode.toUpperCase();
		
		    if (term === '') {
		      return null;
		    }
		
		    return {
		      id: prodcode,
		      text: term,
		      newTag: true // add additional parameters
		    }
		  }
	});;
   clrProdPpProductCombo();
   
   $("#prodDetlsFrm").find("input[type=text]").val("");
   $('#recomPpPaymentmode').val('');
   $('#prodRiskRate').val('');
   $("#prodRiskRate").removeAttr('disabled');
   $("#recomPpBasicRef").val("");
   $("#recomPpPlan").val("");
   
   //hide if prodCombo field error Exists
   clrprodFldErrMsg();
   //hide isf prin combo has error msg
   $("#recomPpPrinError").removeClass('show').addClass('hide');
   
   //hide green Noti Box
   $("#bscRdrNotInfo").removeClass('show').addClass('hide');
   clrselFldBscPlanCombo();
    hideBscPlanCombo();
   
   }
 
//Clear all Form Fields Except Client Name
   function clrProdFrmDetlsExceptClntName(){
	  $('#recomPpPrin').val('');
       $('#recomPpPrin').select2({
			  placeholder: "Select an Insurance Company / Principal",
			  width : '100%',
			  templateResult: loadSelect2PrinCombo,
		});
       clrProdRelatedFrmDetls();
}

   
   // Click save and Add New Button 
   $("#btnsaveAddNewPrdDetls").click(function(){
	   clrprodFldErrMsg();
	   var ProdType = "";
	    
	   if($("#hdnAction").val() == "I"){
		   
		   enableClrClnt();
	    	
	    	 ProdType = $('input[name="recomPpBasrid"]:checked').val();
	    	
	    	if(ProdType == "BASIC"){
	    		$("#recomPpBasicRef").val("");
	    	}
	    	
	    	$("#recomPpId").val("");
		      //chkProdType();
		   if(!validateProdDetlsForm()){return;}
		 
		   var strecommPpId = "";
		    
		   var prdRecmFrmInsert = $('#prodDetlsFrm :input').serializeObject();
		   var strFnaId  = $("#hTxtFldCurrFNAId").val();
		   $.ajax({
		            url:"productsrecommend/add/"+strFnaId,
		            type: "POST",
		            dataType: "json",async:false,
		            contentType: "application/json",
		            data: JSON.stringify(prdRecmFrmInsert),
		            success: function (response) {
		            	
		            	 for(var prod in response){
		            		 if(response.hasOwnProperty(prod)) {
		            			 
		            			 $.each(response,function(key,val){
		     	        			
		     	        			switch(key){
		         		      		case "recomPpId":
		         		      			strrecomPpId = val;
		          		      		 break;
		          		      		 default:
		          		      			 break;
		         		      		  }
		     	        		});
		            			 strecommPpId = response.recomPpId;
		            	       }
		            	 }
		            
//		       	        console.log("recommPPid-------->"+strecommPpId)
		               
		       	       var strClntName =  $("#recomPpName option:selected").val();//$("#recomPpName").val();// $("#recomPpName option:selected").val();
		       	    var orgClientName =  $("#recomPpName option:selected").val();
		      		   var strCompName = $("#recomPpPrin option:selected").val();
		      		   creteNewTabFun(strClntName);	
//		      		   console.log(strCompName,strClntName)
		      		   strClntName = strClntName.replace(/[&\/\\#,+()@!$~%.'":*?<>{}]/g,'_');
		      		                                     strClntName = strClntName.replace(/\s+/g, '_');
		      		   createPrincipalCards(null,strCompName,orgClientName,strecommPpId); 
						//if (typeaheaddata.includes(strClntName) === false) typeaheaddata.push(strClntName);
						//$("#recomPpName").typeahead({ source:typeaheaddata});
		      		   
//		      		 console.log(strClntName ,"=" ,strClntName,"ekyc11")
		      		 if(!chkOptionExists("recomPpName",orgClientName)) {
		      				addOption("recomPpName",orgClientName,orgClientName)
		      			}
						
						
						chkNextChange()
		            },
		            error: function(xhr,textStatus, errorThrown){
			 			//$('#cover-spin').hide(0);
			 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
					}
		            });
		   
  		  
 		   
 	   }
	   

   	// Edit Process of Basic and Rider plans
       if($("#hdnAction").val() == "E"){
    	   disableClrClnt();
    	   RmveDisbleAttrClntCompProdPln();

           ProdType = $('input[name="recomPpBasrid"]:checked').val();
	    	
	    	if(ProdType == "BASIC"){
	    		$("#recomPpBasicRef").val("");
	    	}
    	   
    	   var strProdPlan = $("#recomPpProdname option:selected").text();
    		$("#recomPpPlan").val(strProdPlan);	
    		
    		
    	   var strRecmPpId = $("#recomPpId").val();
    	   var strFnaId  = $("#fnaDetails").val("");
    	   var prdRecmFrmUpdate = $('#prodDetlsFrm :input').serializeObject();
		   
//		   console.log(JSON.stringify(prdRecmFrmUpdate))
			$.ajax({
		            url:"productsrecommend/update/"+strRecmPpId,
		            type: "POST",
		            dataType: "json",async:false,
		            contentType: "application/json",
		            data: JSON.stringify(prdRecmFrmUpdate),
		            success: function (data) {
		            	
		               
		         	   var nwBscArr = [];
		     		   var strComp = $("#hEdtComp").val(); 
		     		   var strPln  = $("#hEtdPln").val();
		     		   var strPlnVal = $("#hEtdPlnVal").val();
		     		   var strBscSetId =  $("#hEdtBscSetId").val();
		     		    
		     		   nwBscArr.push($("#recomPpProdname option:selected").text());
		     		   var prdtype = $('input[name="recomPpBasrid"]:checked').val();
		     		   nwBscArr.push(prdtype.charAt(0));
		     		   nwBscArr.push($('input[name="recomPpProdtype"]:checked').val());
		     		   
		     		   var ErrVal = "--NIL--"
		     		   var prem = $("#recomPpPremium").val(); 
		     		   if((prem.length == 0)){
		     			   nwBscArr.push(ErrVal);	
		     	 		}else{
		     	 			 nwBscArr.push(prem);
		     	 		}
		     		   
		     		   var sumAss = $("#recomPpSumassr").val(); 
		     		   if((sumAss.length == 0)){
		     			   nwBscArr.push(ErrVal);	
		     	 		}else{
		     	 			 nwBscArr.push(sumAss);
		     	 		}
		     		   
		     		   
		     		   var payType = $("#recomPpPaymentmode option:selected").text(); 
		     		   if((payType.length == 0)){
		     			   nwBscArr.push(ErrVal);	
		     	 		}else{
		     	 			 nwBscArr.push(payType);
		     	 		}
		     		  
		     		   
		     		   var plnTrm = $("#recomPpPlanterm").val(); 
		     		   if((plnTrm.length == 0)){
		     			   nwBscArr.push(ErrVal);	
		     	 		}else{
		     	 			 nwBscArr.push(plnTrm);
		     	 		}
		     		   
		     		   
		     		  var prmtrm = $("#recomPpPayterm").val(); 
		     		   if((prmtrm.length == 0)){
		     			   nwBscArr.push(ErrVal);	
		     	 		}else{
		     	 			 nwBscArr.push(prmtrm);
		     	 		}
		     		   
		     		   
		     		  var prdRiskRate = $("#prodRiskRate").val(); 
		     		   if((prdRiskRate.length == 0)){
		     			   nwBscArr.push(ErrVal);	
		     	 		}else{
		     	 			 nwBscArr.push(prdRiskRate);
		     	 		}
		     		   
		     		//prod Risk Rates values set while editing product Risk rate Values.
		     			var strProdRiskRateClass="",PRRTitle ="";
		     			
		     			    if(prdRiskRate == 1){
		     				      PRRTitle = "Conservative";
		     					  strProdRiskRateClass = "fa fa-star-o text-primary fa-1x";
		     				 } 
		     				 if(prdRiskRate == 2){
		     					 PRRTitle = "Moderatively Conservative";
		     					 strProdRiskRateClass = "fa fa-star-half-o text-info fa-1x";
//		     					 console.log("2"+PRRTitle)
		     				 }
		     				 
		     		          if(prdRiskRate == 3){
		     		        	  PRRTitle = "Moderatively Aggressive";
		     		 			 strProdRiskRateClass = "fa fa-star-half text-secondary fa-1x";
//		     		 			 console.log("3"+PRRTitle)
		     				 }
		     		          
		     		          if(prdRiskRate == 4){
		     		        	  PRRTitle = "Aggressive";
		     		 			  strProdRiskRateClass = "fa fa-star text-danger fa-1x";
//		     		 			 console.log("4"+PRRTitle)
		     		 		 }//end
		     					 
		     		    var clnt =$("#recomPpName").val();// $("#recomPpName option:selected").val();
		     		   var comp = $("#recomPpPrin option:selected").val();
		     		   var prodVal = $("#recomPpProdname option:selected").val();
		     		   var NwArrLen = nwBscArr.length; 
		     		  
		     		   var thisPrntId,thisObj,rdrParentProdVal;
		     		   if(prdtype == "BASIC"){
		     			    thisPrntId = "RiderPlans";
		     		   }if(prdtype == "RIDER"){
		     			    thisPrntId = "RdrProdDetls";
		     		    }
		     		    strComp = strComp.replace(/[&\/\\#,+()@!$~%.'":*?<>{}]/g,'_');
		     		    strComp = strComp.replace(/\s+/g, '_');
		     		    
		     		    clnt = clnt.replace(/[\s+\/]/g, '_');
		     		   clnt = clnt.replace(/[&\/\\#,+()@!$~%.'":*?<>{}]/g,'_');
		     		  
		     		    $('#prodTabContent').children('#'+clnt).find("div#"+strComp+"cardDetls").find("#"+strPlnVal+thisPrntId).each(function(){
		     		    	
		     		    	thisObj =  $(this).find('.prdEdtClass');
		     				  if(prdtype == "BASIC"){
		     					  var bscPlnProdCode = strComp+prodVal+"BscPlnDetls"; 
		     					  $(this).parent().find('input:hidden').eq(0).val(comp);//comp
		     					  $(this).parent().find('input:hidden').eq(1).val(nwBscArr[0]);//pln
		     					  $(this).parent().find('input:hidden').eq(2).val(prodVal);//plnVal
		     					  $(this).parent().attr("id",bscPlnProdCode);
		     					  $(this).attr("id",prodVal+thisPrntId);
		     					  $(this).find('#BscProdDetlsEdit').prop('title','Edit Basic Plan : '+nwBscArr[0]);
		     					  $(this).find('#BscProdDetlsDelte').prop('title','Delete Basic Plan : '+nwBscArr[0]);
		     					  $("div#"+bscPlnProdCode).find(".riderPlnDetls").removeClass(strPlnVal+'RdrParentSet').addClass(prodVal+'RdrParentSet');
		     				   } 
		     				  if(prdtype == "RIDER"){
		     					  $(this).attr("id",prodVal+thisPrntId);
		                          $(this).removeClass(strPlnVal+thisPrntId).addClass(prodVal+thisPrntId);
		                          $(this).find('#RdrProdDetlsEdit').prop('title','Edit Rider Plan : '+nwBscArr[0]);
		     					  $(this).find('#RdrProdDetlsDelte').prop('title','Delete Rider Plan : '+nwBscArr[0]);
		     				 }
		     				  
		     				  for (i = 0; i < NwArrLen; i++) {
		     					    if(i == 0){
		     					    	thisObj.eq(i).text(nwBscArr[i]);
		     					    	thisObj.eq(i).siblings().eq(1).val(nwBscArr[i]);
		     					    	thisObj.eq(i).siblings().eq(2).val(comp);
		     					    	thisObj.eq(i).siblings().eq(3).val(prodVal);
		     						}
		     						 if(i > 0){
		     							 thisObj.eq(i).text(nwBscArr[i]);
		     							
		     							 thisObj.eq(i).siblings().eq(1).val(nwBscArr[i])
		     							}
		     						 
		     						 if(i == 8){
		     							 thisObj.eq(i).text(nwBscArr[i]);
		     							 thisObj.eq(i).siblings().eq(1).attr("class",strProdRiskRateClass)
		     							 thisObj.eq(i).siblings().eq(1).attr("title",PRRTitle)
		     							 thisObj.eq(i).siblings().eq(2).val(nwBscArr[i])
		     						 }
		     						 }
		     				   });
		     		    
		     		     showAlert(strComp,prdtype,nwBscArr[0]);
		     		    //setTimeout(function(){ $("#"+strPlnVal+thisPrntId).css("background-color", "")},2000);
		     		 },
		     		error: function(xhr,textStatus, errorThrown){
			 			//$('#cover-spin').hide(0);
			 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
					}
		            });
		    
		    setTimeout(function(){ $('#AddProdRecommDetlsMdl').modal('toggle')},1000);
		   }
	   
	 });
   
    function SaveProdDetlsData(){
	 var frmElements = $('#prodDetlsFrm :input').serializeObject();
	  
	   var strFnaId  = $("#hTxtFldCurrFNAId").val();
	  
	    
		$.ajax({
	            url:"productsrecommend/add/"+strFnaId,
	            type: "POST",
	            dataType: "json",async:false,
	            contentType: "application/json",
	            data: JSON.stringify(frmElements),
	            success: function (response) {
	            	//alert("Success-->" +response);
	            },
	            error: function(xhr,textStatus, errorThrown){
		 			//$('#cover-spin').hide(0);
		 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
				}
	            });
   } 
   

   function validateProdDetlsForm(){
	   
	    var strClntName = $("#recomPpName option:selected").val();//$("#recomPpName").val();// $("#recomPpName option:selected").val();
	    var strCompName = $("#recomPpPrin option:selected").val();
	 	var strProdLob  = $('input[name="recomPpProdtype"]:checked').val();
	 	var strProdName = $("#recomPpProdname option:selected").val();
	 	var strProdPln = $("#recomPpProdname option:selected").text();
	 	//alert(strProdName)
	 	$("#recomPpPlan").val(strProdPln);
	      if(isEmptyFld(strClntName)){
	    	   
	 			$("#txtFldClntNameError").removeClass("hide").addClass("show");
	 			$("#recomPpName").addClass('err-fld');
	 			return;
	 		}else{
	 			$("#txtFldClntNameError").removeClass("show").addClass("hide");
	 			$("#recomPpName").removeClass('err-fld');
	 		}
	 		
	 		if(isEmptyFld(strCompName)){
	 		
	 			$("#recomPpPrinError").removeClass("hide").addClass("show");
	 			$(this).closest('span').find(".select2-container--default .select2-selection--single").addClass('err-fld');
	 			return;
	 		}else{
	 			$("#recomPpPrinError").removeClass("show").addClass("hide");
	 			$(this).closest('span').find(".select2-container--default .select2-selection--single").removeClass('err-fld');
	 		}
	 	 
	 		if(isEmptyFld(strProdLob)){
	 			$("#radBtnprodLobError").removeClass("hide").addClass("show");
	 			return ;
	 		}else{
	 			$("#radBtnprodLobError").removeClass("show").addClass("hide");
	 		}
	 	
	 	 	if(isEmptyFld(strProdName)){
	 	 		$("#recomPpProdnameError").removeClass("hide").addClass("show");
	 			$(this).closest('span').find(".select2-container--default .select2-selection--single").addClass('err-fld');
	 			return;
	 		}else{
	 			$("#recomPpProdnameError").removeClass("show").addClass("hide");
	 			$(this).closest('span').find(".select2-container--default .select2-selection--single").removeClass('err-fld');
	 		} 
	 	 	
	 	 	if(strProdLob == "RIDER"){
		 		var strBscPlanCombo = $("#selFldBscPlanCombo option:selected").val();
		 		if(isEmptyFld(strBscPlanCombo)){
			    	$("#selFldBscPlanComboError").removeClass("hide").addClass("show");
		 			$("#selFldBscPlanCombo").addClass('err-fld');
		 			return;
		 		}else{
		 			$("#selFldBscPlanComboError").removeClass("show").addClass("hide");
		 			$("#selFldBscPlanCombo").removeClass('err-fld');
		 		}
		 	}
	 	 	
	     return true;
	    }   
  
   
   
   function creteNewTabFun(ClntName){
	   var orgClntName = ClntName;
	   		ClntName = ClntName.replace(/[\s+\/]/g, '_');
	   		
	   		//ClntName = ClntName.replace(',', '');
	   		ClntName = ClntName.replace(/[&\/\\#,+()@!$~%.'":*?<>{}]/g,'_');
	 
		  var totlTabs =  $('#prodTabs li').length;
		  
		     if(totlTabs == 0 ){
		    	 createNewClntTabs(ClntName.toUpperCase(),orgClntName.toUpperCase());
			 }else{
				var tabArr =[];
			      $('#prodTabs li a').each(function(){
					   strTab = $(this).attr("id");
					   tabArr.push(strTab.trim());
				     }); 
			    var clntTabNameExts = tabArr.includes(ClntName.trim().toUpperCase());
			        if(!clntTabNameExts){
			           createNewClntTabs(ClntName.toUpperCase(),orgClntName.toUpperCase());
			          }
		      }
	  }   
   
   
   
   function createNewClntTabs(strClntName,orgClntName){
	   var nextTab = $('#prodTabs li').length+1;
	    
	    // create the tab
	    $('#prodTabs').find("a.nav-link").removeClass("active");
		$('<li class="nav-item"><a class="nav-link active" title="'+orgClntName+'" href="#'+strClntName+'"  id="'+strClntName+'" onclick="openThisTabContArea(this)" data-toggle="tab" role="tab"  aria-controls="#'+strClntName+'">'+orgClntName+' &nbsp;<i class="fa fa-trash-o checkdb delBtnclr pl-3" aria-hidden="true" style="font-size: larger;" id="'+orgClntName+'" title="Delete : '+orgClntName+'" onclick="deleteThisClient(this)"></i> </a></li>').appendTo('#prodTabs');
		
		// create the tab content
		$('#prodTabContent').children().removeClass("active show");
		$('<div class="tab-pane fade active show " id="'+strClntName+'" role="tabpanel" aria-labelledby="'+strClntName+'" > </div>').appendTo('#prodTabContent');
		
		// make the new tab active
		
		$('#prodTabs').find('#'+strClntName).tab('show');
		$('#prodTabs').find('#'+strClntName).trigger('click');
		
		$('#prodTabContent').find('#'+strClntName).show();
		$("#btnAddRdrPlns").removeClass("show").addClass("hide");
		
		}
   
  
  function openThisTabContArea(obj){
	    var tabid = $(obj).attr("id");
		var tabcontAreaId=[];
		$('#prodTabContent div').each(function(){
			var tabContId = $(this).attr("id");
			if(tabid == tabContId) {
				$(this).addClass("active show");
				$('#prodTabContent div').not(this).removeClass("active show");
			 }
		});
	} 
   
function createPrincipalCards(dataset,prinVal,clntVal,recomId){
//	console.log(prinVal,clntVal,dataset);
	
	
	
	var img,id,crdBdyId;
	
	
	switch(prinVal){
	   case "Aviva Ltd":
	   case "Singapore Life Ltd":
	   case "Singapore Life Pte.Ltd":
		   
		   prinVal = "Singapore Life Ltd"; //this is new name - 2022/jan
		   
		      img = '<img src="vendor/avallis/img/prinImg/SinglifewithAviva.png" class="rounded float-left"  style="width:170px;">';
		      id = prinVal+"cardDetls";
		      crdBdyId = prinVal+"crdBdyId";
		     break;
		     
		     
	   case "NTUC Income":
		     img = '<img src="vendor/avallis/img/prinImg/ntucincome.png" class="rounded float-left"  style="width:100px;">';
		     id= prinVal+"cardDetls";
		     crdBdyId = prinVal+"crdBdyId";
		     break;
		     
	   case "Manulife Financial":
	   case "Manulife (Singapore) Pte Ltd":
		   
		   prinVal = "Manulife (Singapore) Pte Ltd";//this is new name - 2022/jan
		   
		     img = '<img src="vendor/avallis/img/prinImg/Manulife_logo.png" class="rounded float-left" style="width:100px;">';
		     id= prinVal+"cardDetls";
		     crdBdyId = prinVal+"crdBdyId";
		     break;
		     
	 /*  case "AIG Global Investment Corp S Ltd":
		   img = '<img src="vendor/avallis/img/prinImg/aia.jpg" class="rounded float-left" style="height:30px">';
		   id= prinVal+"cardDetls";
		   crdBdyId = prinVal+"crdBdyId";
		   break;*/
		   
	   case "Aberdeen Asset Management Asia Ltd":
			//icon = "vendor/avallis/img/prinImg/";
			
			img = '<img src="vendor/avallis/img/prinImg/aberdeen.png" class="rounded float-left" style="height:30px">';
			   id= prinVal+"cardDetls";
			   crdBdyId = prinVal+"crdBdyId";
			break;
			
			case "ABN AMRO Asset Management S Ltd":
			//icon = "vendor/avallis/img/prinImg/";
			img = '<img src="vendor/avallis/img/prinImg/ABN_Armo.png" class="rounded float-left" style="height:30px">';
			   id= prinVal+"cardDetls";
			   crdBdyId = prinVal+"crdBdyId";
			break;
			
			case "ACE Insurance Ltd":
			//icon = "vendor/avallis/img/prinImg/";
			img = '<img src="vendor/avallis/img/prinImg/Ace_New_Logo.png" class="rounded float-left" style="height:30px">';
			   id= prinVal+"cardDetls";
			   crdBdyId = prinVal+"crdBdyId";
			break;
			
			case "India International Insurance Pte Ltd":
				//icon = "vendor/avallis/img/prinImg/india_International.jpeg";
				img = '<img src="vendor/avallis/img/prinImg/india_International.jpeg" class="rounded float-left" style="height:30px">';
				id= prinVal+"cardDetls";
				crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "InterGlobal Insurance Company Limited":
				//icon = "vendor/avallis/img/prinImg/Inter-Global.jpg";
				img = '<img src="vendor/avallis/img/prinImg/Inter-Global.jpg" class="rounded float-left" style="height:30px">';
				id= prinVal+"cardDetls";
				crdBdyId = prinVal+"crdBdyId";
				break;
				
				
			case "International SOS Pte Ltd":
				//icon = "vendor/avallis/img/prinImg/inter_SOS.png";
				img = '<img src="vendor/avallis/img/prinImg/inter_SOS.png" class="rounded float-left" style="height:30px">';
				id= prinVal+"cardDetls";
				crdBdyId = prinVal+"crdBdyId";
				break;
				
				
			case "Keppel Insurance Pte Ltd":
				//icon = "vendor/avallis/img/prinImg/swfi-keppal.svg";
				img = '<img src="vendor/avallis/img/prinImg/swfi-keppal.svg" class="rounded float-left" style="height:30px">';
				id= prinVal+"cardDetls";
				crdBdyId = prinVal+"crdBdyId";
				break;
				
				
			case "Legg Mason Asset Management Asia":
				//icon = "vendor/avallis/img/prinImg/leg_mason.png";
				img = '<img src="vendor/avallis/img/prinImg/leg_mason.png" class="rounded float-left" style="height:30px">';
				id= prinVal+"cardDetls";
				crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "Liberty Insurance":
				//icon = "vendor/avallis/img/prinImg/liberty.png";
				img = '<img src="vendor/avallis/img/prinImg/liberty.png" class="rounded float-left" style="height:30px">';
				id= prinVal+"cardDetls";
				crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "Allianz Dresdner Asset Management":
			case "Allianz Global Corporate & Specialty AG Singapore Branch":
			case "Allianz Global Investors S Ltd":
			//icon = "vendor/avallis/img/prinImg/";
			img = '<img src="vendor/avallis/img/prinImg/allianz.svg" class="rounded float-left" style="height:30px">';
			   id= prinVal+"cardDetls";
			   crdBdyId = prinVal+"crdBdyId";
			break;
			
			case "American International Group":
			case "AIG Global Investment Corp S Ltd":
			//icon = "vendor/avallis/img/prinImg/";
			img = '<img src="vendor/avallis/img/prinImg/AIG.png" class="rounded float-left" style="height:30px">';
			   id= prinVal+"cardDetls";
			   crdBdyId = prinVal+"crdBdyId";
			break;
			
			case "American International Assurance Co. Ltd":
			case "AIA Singapore Private Limited":
			//icon = "vendor/avallis/img/prinImg/";
			img = '<img src="vendor/avallis/img/prinImg/AIA_Logo.png" class="rounded float-left" style="height:30px">';
			   id= prinVal+"cardDetls";
			   crdBdyId = prinVal+"crdBdyId";
			break;
			
			
			
			case "AXA Insurance Singapore Pte Ltd":
			case "AXA Life Insurance (S) Pte Ltd":
			//icon = "vendor/avallis/img/prinImg/";
			img = '<img src="vendor/avallis/img/prinImg/AXA.png" class="rounded float-left" style="height:30px">';
			   id= prinVal+"cardDetls";
			   crdBdyId = prinVal+"crdBdyId";
			break;
				
			case "Catlin Singapore Pte Ltd (Lloyds)":
			//icon = "vendor/avallis/img/prinImg/";
			img = '<img src="vendor/avallis/img/prinImg/Catlin_Sig.png" class="rounded float-left" style="height:30px">';
			   id= prinVal+"cardDetls";
			   crdBdyId = prinVal+"crdBdyId";
			break;
				
			case "CGU International Insurance plc":
			//icon = "vendor/avallis/img/prinImg/";
			img = '<img src="vendor/avallis/img/prinImg/CGU_logo.svg" class="rounded float-left" style="height:30px">';
			   id= prinVal+"cardDetls";
			   crdBdyId = prinVal+"crdBdyId";
			break;
			
			

		    case "Chartis, American Home Assurance":
				//icon = "vendor/avallis/img/prinImg/CHARTIS.png";
		    	img = '<img src="vendor/avallis/img/prinImg/CHARTIS.png" class="rounded float-left" style="height:30px">';
				id= prinVal+"cardDetls";
				crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "China Insurance Co (Singapore) Pte Ltd":
			case "China Life Insurance (Singapore) Pte. Ltd":	
			case "China Life Insurance (Singapore) Pte. Ltd.":
				//icon = "vendor/avallis/img/prinImg/CHINA_LIFE.png";
				img = '<img src="vendor/avallis/img/prinImg/CHINA_LIFE.png" class="rounded float-left" style="height:30px">';
				id= prinVal+"cardDetls";
				crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "China Taiping Insurance (Singapore) Pte Ltd":
			case "China Taiping Insurance":
				//icon = "vendor/avallis/img/prinImg/CHINATAP.png";
				img = '<img src="vendor/avallis/img/prinImg/CHINATAP.png" class="rounded float-left" style="height:30px">';
				id= prinVal+"cardDetls";
				crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "CIGNA International":
				//icon = "vendor/avallis/img/prinImg/CIGNA.png";
				img = '<img src="vendor/avallis/img/prinImg/CIGNA.png" class="rounded float-left" style="height:30px">';
				id= prinVal+"cardDetls";
				crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "Commerzbank Asset Management Asia L":
				//icon = "vendor/avallis/img/prinImg/COMMERZ.png";
				img = '<img src="vendor/avallis/img/prinImg/COMMERZ.png" class="rounded float-left" style="height:30px">';
				id= prinVal+"cardDetls";
				crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "DBS Asset Management":
			case "Development Bank of Singapore":
				//icon = "vendor/avallis/img/prinImg/DEVBank.png";
				img = '<img src="vendor/avallis/img/prinImg/DEVBank.png" class="rounded float-left" style="height:30px">';
				id= prinVal+"cardDetls";
				crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "Deutsche Asset Management Asia Ltd":
				//icon = "vendor/avallis/img/prinImg/DEUT.png";
				img = '<img src="vendor/avallis/img/prinImg/DEUT.png" class="rounded float-left" style="height:30px">';
				id= prinVal+"cardDetls";
				crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "EQ Insurance Company Limited":
				//icon = "vendor/avallis/img/prinImg/EQINS.png";
				img = '<img src="vendor/avallis/img/prinImg/EQINS.png" class="rounded float-left" style="height:30px">';
				id= prinVal+"cardDetls";
				crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "Federal Insurance Company":
				//icon = "vendor/avallis/img/prinImg/AgeasFederal.jpg";
				img = '<img src="vendor/avallis/img/prinImg/AgeasFederal.jpg" class="rounded float-left" style="height:30px">';
				id= prinVal+"cardDetls";
				crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "Fidelity Investments Singapore Ltd":
				//icon = "vendor/avallis/img/prinImg/Fidelity.png";
				img = '<img src="vendor/avallis/img/prinImg/Fidelity.png" class="rounded float-left" style="height:30px">';
				id= prinVal+"cardDetls";
				crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "First Capital Insurance Ltd":
				//icon = "vendor/avallis/img/prinImg/FIRCap.jpg";
				img = '<img src="vendor/avallis/img/prinImg/FIRCap.jpg" class="rounded float-left" style="height:30px">';
				id= prinVal+"cardDetls";
				crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "First State Investments Singapore":
				//icon = "vendor/avallis/img/prinImg/FSI.png";
				img = '<img src="vendor/avallis/img/prinImg/FSI.png" class="rounded float-left" style="height:30px">';
				id= prinVal+"cardDetls";
				crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "Friends Provident International Limited (Singapore Branch)":
				//icon = "vendor/avallis/img/prinImg/FPI_Logo.png";
				img = '<img src="vendor/avallis/img/prinImg/FPI_Logo.png" class="rounded float-left" style="height:30px">';
				id= prinVal+"cardDetls";
				crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "Great Eastern Life Assurance":
			case "Great Eastern Life":
				//icon = "vendor/avallis/img/prinImg/GE.png";
				img = '<img src="vendor/avallis/img/prinImg/GE.png" class="rounded float-left" style="height:30px">';
				id= prinVal+"cardDetls";
				crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "Henderson Global Investors S Ltd":
				//icon = "vendor/avallis/img/prinImg/HGIS.png";
				img = '<img src="vendor/avallis/img/prinImg/HGIS.png" class="rounded float-left" style="height:30px">';
				id= prinVal+"cardDetls";
				crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "Hongkong Shanghai Banking Corporation":
			case "HSBC Insurance (Singapore) Pte Ltd":
			case "HSBC Investments":
				//icon = "vendor/avallis/img/prinImg/HBSC.png";
				img = '<img src="vendor/avallis/img/prinImg/HBSC.png" class="rounded float-left" style="height: 60px;width: 15%;">';
				id= prinVal+"cardDetls";
				crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "iFAST Financial":
				//icon = "vendor/avallis/img/prinImg/Ifast.png";
				img = '<img src="vendor/avallis/img/prinImg/Ifast.png" class="rounded float-left" style="height:30px">';
				id= prinVal+"cardDetls";
				crdBdyId = prinVal+"crdBdyId";
				break;
			
			
			
			
			
			case "Life Insurance Corporation (Singapore) Pte Ltd":
				//icon = "vendor/avallis/img/prinImg/";
				img = '<img src="vendor/avallis/img/prinImg/LICS.jpg" class="rounded float-left" style="height:30px">';
				   id= prinVal+"cardDetls";
				   crdBdyId = prinVal+"crdBdyId";
				break;
			
			case "MSIG Insurance (Singapore) Pte Ltd":
			case "Mitsui Sumitomo Insurance (Singapore) Pte Ltd":
				//icon = "vendor/avallis/img/prinImg/";
				img = '<img src="vendor/avallis/img/prinImg/msig_logo.png" class="rounded float-left" style="height:30px">';
				   id= prinVal+"cardDetls";
				   crdBdyId = prinVal+"crdBdyId";
				break;
			
			case "QBE Insurance International Ltd":
				//icon = "vendor/avallis/img/prinImg/";
				img = '<img src="vendor/avallis/img/prinImg/QBE.png" class="rounded float-left" style="height:30px">';
				   id= prinVal+"cardDetls";
				   crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "Raffles Health Insurance Pte Ltd":
				//icon = "vendor/avallis/img/prinImg/;
				img = '<img src="vendor/avallis/img/prinImg/Raffle.png"" class="rounded float-left" style="height:30px">';
				   id= prinVal+"cardDetls";
				   crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "Royal & SunAlliance Insurance (Singapore) Limited":
				//icon = "vendor/avallis/img/prinImg/";
				img = '<img src="vendor/avallis/img/prinImg/Royal_logo.jpeg" class="rounded float-left" style="height:30px">';
				   id= prinVal+"cardDetls";
				   crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "Schroder Investment Mgt Ltd":
				//icon = "vendor/avallis/img/prinImg/";
				img = '<img src="vendor/avallis/img/prinImg/scholders.png" class="rounded float-left" style="height:30px">';
				   id= prinVal+"cardDetls";
				   crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "SG Asset Management":
				//icon = "vendor/avallis/img/prinImg/";
				img = '<img src="vendor/avallis/img/prinImg/SG_logo.png" class="rounded float-left" style="height:30px">';
				   id= prinVal+"cardDetls";
				   crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "SHC Insurance Pte Ltd":
				//icon = "vendor/avallis/img/prinImg/";
				img = '<img src="vendor/avallis/img/prinImg/SHC_Logo.jpg" class="rounded float-left" style="height:30px">';
				   id= prinVal+"cardDetls";
				   crdBdyId = prinVal+"crdBdyId";
				break;
			
			case "Standard Chartered":
				//icon = "vendor/avallis/img/prinImg/";
				img = '<img src="vendor/avallis/img/prinImg/stand_charted.png" class="rounded float-left" style="height:30px">';
				   id= prinVal+"cardDetls";
				   crdBdyId = prinVal+"crdBdyId";
				break;

			case "Templeton Asset Management Ltd":
				//icon = "vendor/avallis/img/prinImg/";
				img = '<img src="vendor/avallis/img/prinImg/templetion_logo.jpeg" class="rounded float-left" style="height:30px">';
				   id= prinVal+"cardDetls";
				   crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "Tenet Insurance Company Ltd":
				//icon = "vendor/avallis/img/prinImg/";
				img = '<img src="vendor/avallis/img/prinImg/tenet_logo.jpeg" class="rounded float-left" style="height:30px">';
				   id= prinVal+"cardDetls";
				   crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "The Overseas Assurance Corporation Limited":
				//icon = "vendor/avallis/img/prinImg/";
				img = '<img src="vendor/avallis/img/prinImg/OAC.jpg" class="rounded float-left" style="height:30px">';
				   id= prinVal+"cardDetls";
				   crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "Tokio Marine Insurance Singapore Ltd":
			case "TM Asia Life Singapore Ltd":
			case "TM Asia Insurance Singapore Ltd":
			case "Tokio Marine Life Insurance":
			case "The Tokio Marine & Fire Insurance Company (S) Pte Ltd":	
				//icon = "vendor/avallis/img/prinImg/";
				img = '<img src="vendor/avallis/img/prinImg/tm-logo.png" class="rounded float-left" style="height:30px">';
				   id= prinVal+"cardDetls";
				   crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "Transamerica Occidental Life Insurance Company":
				//icon = "vendor/avallis/img/prinImg/";
				img = '<img src="vendor/avallis/img/prinImg/transamerica-logo.svg" class="rounded float-left" style="height:30px">';
				   id= prinVal+"cardDetls";
				   crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "United Overseas Bank Ltd":
			case "Overseas Union Insurance Limited":
				//icon = "vendor/avallis/img/prinImg/";
				img = '<img src="vendor/avallis/img/prinImg/uob-logo.jpg" class="rounded float-left" style="height:30px">';
				   id= prinVal+"cardDetls";
				   crdBdyId = prinVal+"crdBdyId";
				break;
				
			case "United Overseas Insurance Limited":
				//icon = "vendor/avallis/img/prinImg/";
				img = '<img src="vendor/avallis/img/prinImg/UOI.jpeg" class="rounded float-left" style="height:30px">';
				   id= prinVal+"cardDetls";
				   crdBdyId = prinVal+"crdBdyId";
				break;
			
			case "UOB Asset Management Ltd":
			//icon = "vendor/avallis/img/prinImg/";
			img = '<img src="vendor/avallis/img/prinImg/uobAsset-logo.jpg" class="rounded float-left" style="height:30px">';
			   id= prinVal+"cardDetls";
			   crdBdyId = prinVal+"crdBdyId";
			break;
			
			case "UOB Life Assurance Ltd":
			//icon = "vendor/avallis/img/prinImg/";
			img = '<img src="vendor/avallis/img/prinImg/UOB.png" class="rounded float-left" style="height:30px">';
			   id= prinVal+"cardDetls";
			   crdBdyId = prinVal+"crdBdyId";
			break;
			
			case "Walton Internation Group (S) Pte Ltd":
			//icon = "vendor/avallis/img/prinImg/";
			img = '<img src="vendor/avallis/img/prinImg/Walton_logo.png" class="rounded float-left" style="height:30px">';
			   id= prinVal+"cardDetls";
			   crdBdyId = prinVal+"crdBdyId";
			break;
			
			case "Zurich Insurance (Singapore) Pte Ltd":
			
			img = '<img src="vendor/avallis/img/prinImg/zurich.png" class="rounded float-left" style="height:30px">';
			   id= prinVal+"cardDetls";
			   crdBdyId = prinVal+"crdBdyId";
			break;
			
			//poovathi add on 14-06-2021
			case "Prudential":
			case "Prudential Asset Management S Ltd":	
				img = '<img src="vendor/avallis/img/prinImg/prudentials.png" class="rounded float-left" style="height:30px">';
				id= prinVal+"cardDetls";
			    crdBdyId = prinVal+"crdBdyId";
				break;
			
//			case "Singapore Life Pte.Ltd":
//				img = '<img src="vendor/avallis/img/prinImg/singapore_life.png" class="rounded float-left" style="height:30px">';
//				id= prinVal+"cardDetls";
//				crdBdyId = prinVal+"crdBdyId";
//				break;
					
			case "Swiss Life (Singapore) Pte Ltd":	  
				img = '<img src="vendor/avallis/img/prinImg/swiss_life.png" class="rounded float-left" style="height:30px">';
				id= prinVal+"cardDetls";
				crdBdyId = prinVal+"crdBdyId";
				break;
				
		   
		default:
			prinVal = isEmpty(prinVal) ? principalnull : prinVal;
			img = '<span><h6><i class="fa fa-question-circle-o"></i> '+prinVal+' </h6></span>';
			//img = '<img src="vendor/avallis/img/prinImg/noimage.png" class="rounded float-left"  style="width:100px;">';
	      	id = prinVal+"cardDetls";
	      	crdBdyId = prinVal+"crdBdyId";
			break;
		}
	
	
	id = id.replace(/[&\/\\#,+()@!$~%.'":*?<>{}]/g,'_');
	id = id.replace(/[\s+\/]/g, '_');
	
	crdBdyId =crdBdyId.replace(/[&\/\\#,+()@!$~%.'":*?<>{}]/g,'_');
	crdBdyId =crdBdyId.replace(/[\s+\/]/g, '_');
	
	//console.log(img,id, crdBdyId,"recomId"+recomId);
	
	
	
	var strPrinCardDetls = '<div class="card mb-1 p-1 m-1 PrincipalCardSec" id="'+id+'" class="'+id+'" style="border: 1px solid #2a4fb6;">'
        +'<div class="card-header" id="cardHeaderStyle" title="'+prinVal+'">'
              	+'<div class="row"> <input type="hidden" class="hdnFldCompName" name="hdnFldCompName" id="hdnFldCompName" value="'+prinVal+'"><input type="hidden" class="hdnFldClntName" name="hdnFldClntName" id="hdnFldClntName" value="'+clntVal+'">' 
                      +'<div class="col-8" >'+img+'</div>'   
                      +'<div class="col-3"><span class="badge badge-pill badge-success font-sz-level8 py-2" title="Add Basic Plan For this Principal" id="badgeAddBscPln" onclick="addEdtBasicPlans(this);"><i class="fa fa-shield" aria-hidden="true"></i><sub><i class="fa fa-plus" aria-hidden="true"></i></sub>&nbsp;Add Basic Plans</span></div>'  
                      +'<div class="col-1"><i class="fa fa-trash-o checkdb fa-2x" style="color: red;" title="Delete Principal : '+prinVal+'" onclick="deleteThisPrincipal(this)" ></i></div>' 
                 +'</div>'
       +'</div>'
      +'<div class="card-body p-2 PrinpalCrdBodyCont '+crdBdyId+'" style="border:1px solid #ddd;"></div>'
    +'</div>';
	
	 var  strClntName = clntVal;
	
	strClntName = strClntName.replace(/[\s+\/]/g, '_');
	strClntName = strClntName.replace(/[&\/\\#,+()@!$~%.'":*?<>{}]/g,'_');
	                                    
	                                    strClntName = strClntName.toUpperCase()
   
	var totalPrinlen = $('#prodTabContent').children('#'+strClntName).find('#'+id).length;
	
//	console.log(strClntName,"totalPrinlen",totalPrinlen);
	
//        console.log(totalPrinlen);
        if(totalPrinlen == 0){
            $("#prodTabs").removeClass("hide").addClass("show");
    		$("#NoProdDetlsRow").removeClass("show").addClass("hide");
    		showPrimaryAddProdRecommBtn();
        	$('#prodTabContent').children('#'+strClntName).append(strPrinCardDetls); 
       	}  
   addBscPlan(dataset,strClntName,prinVal,id,crdBdyId,recomId);
}

function addBscPlan(dataset,Clnt,prin,prinCrdId,prinCrdBdyId,PpId){
	
//	console.log("dataset,ppid"+Clnt,PpId);
    var prodBgClr;
   switch(prin){
       case "Aviva Ltd":
       case "Singapore Life Ltd":
       case "Singapore Life Pte.Ltd":
    	    //console.log(prin)
    	    prodBgClr ="prod-hglt-aviva";
    	    getBasicPlnDetls(dataset,Clnt,prin,prodBgClr,prinCrdId,PpId);
    	  break;
    	  
      
   	   
	     
       case "NTUC Income":
    	   //console.log(prin)
    	   prodBgClr ="prod-hglt-ntuc";
    	   getBasicPlnDetls(dataset,Clnt,prin,prodBgClr,prinCrdId,PpId);
    	 break;
	     
       case "Manulife Financial":
       case "Manulife (Singapore) Pte Ltd":
    	   //console.log(prin)
    	   prodBgClr ="prod-hglt-mnulfe";
    	   getBasicPlnDetls(dataset,Clnt,prin,prodBgClr,prinCrdId,PpId);
    	 break;
	     
       case "AIG Global Investment Corp S Ltd":
       case "AIA Singapore Private Limited":
    	   //console.log(prin)
    	   prodBgClr ="prod-hglt-aia";
    	   getBasicPlnDetls(dataset,Clnt,prin,prodBgClr,prinCrdId,PpId);
    	  break;
	   
	   default:
		   prodBgClr ="prod-hglt-dedault";
	       getBasicPlnDetls(dataset,Clnt,prin,prodBgClr,prinCrdId,PpId);
		 break;
  
   }

}


function getBasicPlnDetls(dataset,Clnt,prin,prodBgClr,prinCrdId,recomId){
	
	
//	console.log("recomId------------>"+recomId);
	
	var strClntName = "",strCompName="",strProdval="",strProdName="",
	strProdType="",strProdLob="",strPremium="",strSumAssured="",strPayMethod="",
	strPlanTerm="",strPremTerm="",strProdRiskRate="",strProdRiskRateVal="",prodtype="";strProdPln="",strrecomPpBasicRef="";
	strrecomPpId = "",strClntNameOrg = "";
	
	if(dataset == null){
		
		strClntName =$("#recomPpName option:selected").val();//$("#recomPpName").val();// $("#recomPpName option:selected").val();
		
		strClntNameOrg = $("#recomPpName").val();
		
		strCompName = $("#recomPpPrin option:selected").text();
		
	    strProdval =  $("#recomPpProdname option:selected").val();
	    strProdName = $("#recomPpProdname option:selected").text();
	 	
	 	$("#recomPpPlan").val(strProdName);
	 	
	 	strProdPln = $("#recomPpPlan").val();
		
		strProdType = $('input[name="recomPpBasrid"]:checked').val();
		strProdLob  = $('input[name="recomPpProdtype"]:checked').val();
		
		strPremium  = $("#recomPpPremium").val();
		if(isEmptyFld(strPremium)){
	 		 strPremium = "--NIL--";
		       }
		      
		strSumAssured = $("#recomPpSumassr").val();
		    if(isEmptyFld(strSumAssured)){
		    	strSumAssured = "--NIL--";
	       }  
		
		strPayMethod = $("#recomPpPaymentmode option:selected").text();
		   if(isEmptyFld($("#recomPpPaymentmode").val())){
		    	 strPayMethod = "--NIL--";
	       }
		     
		strPlanTerm = $("#recomPpPlanterm").val();
		   if(isEmptyFld(strPlanTerm)){
		    	strPlanTerm = "--NIL--";
	       } 
		
		strPremTerm = $("#recomPpPayterm").val();
		    if(isEmptyFld(strPremTerm)){
		    	 strPremTerm = "--NIL--";
	        } 
		     
		strProdRiskRate = $("#prodRiskRate option:selected").val();
		
		
	    if(isEmptyFld(strProdRiskRate)){
	    	strProdRiskRate = "--NIL--";
        } 
		
		
		strrecomPpBasicRef = $("#selFldBscPlanCombo option:selected").val();
		$("#recomPpBasicRef").val(strrecomPpBasicRef);
		prodtype = strProdType.charAt(0);	
		
		strrecomPpId = recomId;
		
//		console.log("strrecomPpId null dataset------------>"+strrecomPpId);
		
	}else{
		
		strClntName = isEmpty(dataset.recomPpName)?seesselfName.toUpperCase() :dataset.recomPpName.toUpperCase();
		strClntNameOrg = isEmpty(dataset.recomPpName)?seesselfName.toUpperCase() :dataset.recomPpName.toUpperCase();
		strCompName = isEmpty(dataset.recomPpPrin) ? principalnull : dataset.recomPpPrin;
		if(strCompName == principalnull) {
//			console.log("inside principalnull"+dataset.recomPpId)
		}
		
	    strProdval =  dataset.recomPpProdname;
		
		//strProdName = dataset.recomPpProdname;
	    
	    strProdName = dataset.recomPpPlan;
		
		strProdType = dataset.recomPpBasrid;
		
		strProdLob  = dataset.recomPpProdtype;
		
		strPremium  = dataset.recomPpPremium;
		if(isEmptyFld(strPremium)){
		    	 strPremium = "--NIL--";
		       }
		      
		strSumAssured = dataset.recomPpSumassr;
		  if(isEmptyFld(strSumAssured)){
		    	strSumAssured = "--NIL--";
	       }  
		
		strPayMethod =dataset.recomPpPaymentmode;
		  if(isEmptyFld(strPayMethod)){
		    	 strPayMethod = "--NIL--";
	       }
		     
		strPlanTerm = dataset.recomPpPlanterm;
		   if(isEmptyFld(strPlanTerm)){
		    	strPlanTerm = "--NIL--";
	       } 
		
		strPremTerm = dataset.recomPpPayterm;
		    if(isEmptyFld(strPremTerm)){
		    	 strPremTerm = "--NIL--";
	        } 
		     
		strProdRiskRate = dataset.prodRiskRate;
		
	    if(isEmptyFld(strProdRiskRate)){
	    	strProdRiskRate = "--NIL--";
        } 
		
		strrecomPpBasicRef = dataset.recomPpBasicRef;
		
		$("#recomPpBasicRef").val(strrecomPpBasicRef);
		$("#selFldBscPlanCombo").val($("#recomPpBasicRef").val());
		
		strrecomPpId = dataset.recomPpId;
//		console.log("strrecomPpId not null ====>>>>>>>>>>>>>>>>>>>>"+strrecomPpId)
		prodtype = strProdType.charAt(0);
		
	}
	
	strClntName = strClntName.replace(/[\s+\/]/g, '_');
	strClntName = strClntName.replace(/[&\/\\#,+()@!$~%.'":*?<>{}]/g,'_');
    strClntName = strClntName.toUpperCase();
//	console.log("dfgfhfghgfhgfhgfh>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+strProdRiskRate)
	
	
	//prod Risk Rates
	var strProdRiskRateClass="",PRRTitle ="";
	
	    if(strProdRiskRate == 1){
		      PRRTitle = "Conservative";
			  strProdRiskRateClass = "fa fa-star-o text-primary fa-1x";
		 } 
		 if(strProdRiskRate == 2){
			 PRRTitle = "Moderatively Conservative";
			 strProdRiskRateClass = "fa fa-star-half-o text-info fa-1x";
//			 console.log("2"+PRRTitle)
		 }
		 
          if(strProdRiskRate == 3){
        	  PRRTitle = "Moderatively Aggressive";
 			 strProdRiskRateClass = "fa fa-star-half text-secondary fa-1x";
// 			 console.log("3"+PRRTitle)
		 }
          
          if(strProdRiskRate == 4){
        	  PRRTitle = "Aggressive";
 			 strProdRiskRateClass = "fa fa-star text-danger fa-1x";
// 			 console.log("4"+PRRTitle)
 		 }//end
			 
			 
	
	if(strProdType =="B")strProdType="BASIC";
 if(strProdType == "BASIC"){
	//CR-2021
    var strProdTypeClass;

	switch(strProdType){
	 case "BASIC":
		 strProdTypeClass = "badge-success";
		break;
		
	 case "RIDER":
		 strProdTypeClass = "badge-warning amber";
		break;
		
	 default:
		break;
	}
	
	var StrBsicPlnConts ='<div class="card p-1 m-1 BscPlnDetls" style="border: 1px solid #A9B9E6;" id="'+prin+''+strProdval+'BscPlnDetls"><input type="hidden" name="hdnFldCompName" id="hdnFldCompName"  value="'+strCompName+'"><input type="hidden" name="hdnFldProduct" id="hdnFldProduct" value="'+strProdName+'"><input type="hidden" name="hdnFldProdVal" id="hdnFldProdVal" value="'+strProdval+'">'		
	+'<div class="card-body Pdng-Style bsc-pln-bg border-top border-right border-left border-bottom RiderPlans" id="'+strProdval+'RiderPlans">'	
			   +'<div class="row pb-1">'
		                   +'<div class="col-8 '+prodBgClr+'"><span class="font-sz-level5 text-custom-color-gp bold">Product&nbsp;:&nbsp;</span><span class="font-sz-level5 text-custom-color-gp bold prdEdtClass">'+strProdName+'</span><input type="hidden" name="BscProdName"  class="form-control BscProdName" value="'+strProdName+'"><input type="hidden" name="hdnFldComp" id="hdnFldComp"  value="'+strCompName+'"><input type="hidden" name="hdnTxtprodValue"  class="form-control hdnTxtprodValue" value="'+strProdval+'"></div>'                 
		                   +'<div class="col-2"><span class="badge badge-pill badge-warning amber right font-sz-level8 font-normal py-2" title="Add Rider Plan For this Principal" id="badgeAddRdrPln"  onclick="addEdtBasicPlans(this);"><i class="fa fa-shield" aria-hidden="true"></i><sub><i class="fa fa-plus" aria-hidden="true"></i></sub>&nbsp;Add Rider</span></div>'														 
		                   +'<div class="col-1"><i class="fa fa-pencil-square-o" style="color: blue;" title="Edit Basic Plan : '+strProdName+'" id="BscProdDetlsEdit" onclick = "editThisBscRdrPlan(this)"></i>&nbsp;&nbsp;<i class="fa fa-trash-o checkdb" style="color: red;" id="BscProdDetlsDelte" title="Delete Basic Plan : '+strProdName+'" onclick="deleteThisBscPlan(this)"></i></div>' 
		                   +'<div class="col-1"><span class="badge badge-pill '+strProdTypeClass+' font-sz-level8 font-sz-level8 prdEdtClass" style="cursor: default;" id="prodTypeId">'+prodtype+'</span><input type="hidden" name="hdntxtProdType" id ="hdntxtProdType" class="form-control" value="'+strProdType+'" ></div>'
		       +'</div>'	                 
		       +'<div class="row pb-2 rowmarginClass">'
		                    +'<div class="col-1"><span class="font-sz-level8 text-custom-color-gp ">LOB&nbsp;:&nbsp;</span><span class="font-sz-level8 prdEdtClass">'+strProdLob+'</span><input type="hidden" name="hTxtFldProdMProdLob" class="form-control" value="'+strProdLob+'" > </div>'
		                    +'<div class="col-2"><span class="font-sz-level8 text-custom-color-gp ">Premium&nbsp;:&nbsp;</span><span class="font-sz-level8 prdEdtClass">'+strPremium+'</span><input type="hidden" class="form-control" value="'+strPremium+'" > </div>'                                                         
		                    +'<div class="col-2"><span class="font-sz-level8 text-custom-color-gp ">SA&nbsp;:&nbsp;</span><span class="font-sz-level8 prdEdtClass" >'+strSumAssured+'</span><input type="hidden" class="form-control" value="'+strSumAssured+'" > </div>'
		       
                            +'<div class="col-2"><span class="font-sz-level8 text-custom-color-gp ">Pay.Type&nbsp;:&nbsp;</span><span class="font-sz-level8 prdEdtClass">'+strPayMethod+'</span><input type="hidden" class="form-control" value="'+strPayMethod+'" ></div>'
                            +'<div class="col-2"><span class="font-sz-level8 text-custom-color-gp ">Pln.Term&nbsp;:&nbsp;</span><span class="font-sz-level8 prdEdtClass">'+strPlanTerm+'</span><input type="hidden" class="form-control" value="'+strPlanTerm+'" ></div>'
                            +'<div class="col-2"><span class="font-sz-level8 text-custom-color-gp ">Prem.Term&nbsp;:&nbsp;</span><span class="font-sz-level8 prdEdtClass">'+strPremTerm+'</span><input type="hidden" class="form-control" value="'+strPremTerm+'" ></div>'
                            +'<div class="col-1"><span class="font-sz-level8 text-custom-color-gp ">PRR&nbsp;:&nbsp;</span><span class="font-sz-level8 prdEdtClass" id="riskRate" >'+strProdRiskRate+' </span> <i class="'+strProdRiskRateClass+'" title="'+PRRTitle+'" aria-hidden="true"></i><input type="hidden" class="form-control" value="'+strProdRiskRate+'" ><input type="hidden" name="hdnUnqdId" value="'+makeid(20)+'" ><input type="hidden" id="hBscrecommPpId" name="hBscrecommPpId" value="'+strrecomPpId+'"></div>'
			   +'</div>'  
		      
	 +'</div>'
+'</div>';
	
	
   $('#prodTabContent').children('#'+strClntName).find("div#"+prinCrdId).children(".PrinpalCrdBodyCont").append(StrBsicPlnConts);   
   showAlert(prin,strProdType,strProdName);
  
  var nofBscPlsInPrin = $('#prodTabContent').children('#'+strClntName).find("div#"+prinCrdId).children(".PrinpalCrdBodyCont").find(".RiderPlans").length;
   
  if(nofBscPlsInPrin > 0){
	// show  Add Rider Plan Button when selected Principal have more than one Basic Plans
	 showAddBscRdrPlnBtn();
  }
  clrProdRelatedFrmDetls();
  
  // Making Combo Box when  basic plan > 0 in selected Principal
    clrselFldBscPlanCombo();
  
    $('#prodTabContent').children('#'+strClntName).find("div#"+prinCrdId).children(".PrinpalCrdBodyCont").children("").each(function(){
		
		var prodVal = $(this).find('input:hidden[name=hdnFldProdVal]').val();
		
		var prodtxt = $(this).find('input:hidden[name=BscProdName]').val();
		 
		var NoofBscPlanIds = $(this).find(".card-body").attr("id");
		var strprodstring = '<option value="'+prodVal+'">'+prodtxt+'</option>';
			
			$("#selFldBscPlanCombo").append(strprodstring);
	});
  
}

 if(strProdType =="R")strProdType="RIDER";
 if(strProdType == "RIDER"){
	 
	var BscPlnprdVal = $("#selFldBscPlanCombo").val();
	$("#recomPpBasicRef").val(BscPlnprdVal);
	if(strCompName == principalnull) {
//		console.log("------------------------------------>",BscPlnprdVal)
	}
	
	var StrRiderPlnConts = '<div class="card-body Pdng-Style border-top border-right border-left border-bottom  '+BscPlnprdVal+'RdrParentSet  '+strProdval+'RdrProdDetls riderPlnDetls" id="'+strProdval+'RdrProdDetls">'
		
			   +'<div class="row pb-1">'
			               +'<div class="col-8">'
			                      +'<span class="font-sz-level6 text-custom-color-gp bold">Product&nbsp;:&nbsp;</span><span class="font-sz-level6 text-custom-color-gp bold prdEdtClass">'+strProdName+'</span><input type="hidden" name="RdrProdName" id="RdrProdName"  class="form-control RdrProdName " value="'+strProdName+'" ><input type="hidden" name="hdnTextComp" id="hdnTextComp" value="'+prin+'"><input type="hidden" name="hdnprodVal"  class="form-control hdnprodVal" value="'+strProdval+'">'
			               +'</div>'                
						   +'<div class="col-2">&nbsp;</div>'														 
			               +'<div class="col-1"><i class="fa fa-pencil-square-o " style="color: blue;" title="Edit Rider Plan : '+strProdName+'"  id="RdrProdDetlsEdit" onclick = "editThisBscRdrPlan(this)"></i>&nbsp;&nbsp;<i class="fa fa-trash-o checkdb " id="RdrProdDetlsDelte" style="color: red;" title="Delete Rider Plan : '+strProdName+'" onclick="deleteThisRdrPlan(this)"></i></div>'
			               +'<div class="col-1"><span class="badge badge-pill badge-warning amber  font-sz-level8 prdEdtClass" style="cursor: default;" >'+prodtype+'</span><input type="hidden" name="hdnprodType" id="name="hdnprodType" value="'+strProdType+'"></div>'
			   +'</div>' 
          
			   +'<div class="row pb-1">' 
			               +'<div class="col-1"><span class="font-sz-level8 text-custom-color-gp ">LOB&nbsp;:&nbsp;</span><span class="font-sz-level8 prdEdtClass">'+strProdLob+'</span><input type="hidden" class="form-control" value="'+strProdLob+'" > </div>' 
			               +'<div class="col-2"><span class="font-sz-level8 text-custom-color-gp ">Premium&nbsp;:&nbsp;</span><span class="font-sz-level8 prdEdtClass">'+strPremium+'</span><input type="hidden" class="form-control" value="'+strPremium+'" ></div>'                                                         
			               +'<div class="col-2"><span class="font-sz-level8 text-custom-color-gp ">SA&nbsp;:&nbsp;</span><span class="font-sz-level8 prdEdtClass">'+strSumAssured+'</span><input type="hidden" class="form-control" value="'+strSumAssured+'" > </div>'
			               +'<div class="col-2"><span class="font-sz-level8 text-custom-color-gp ">Pay.Type&nbsp;:&nbsp;</span><span class="font-sz-level8 prdEdtClass">'+strPayMethod+'</span><input type="hidden" class="form-control" value="'+strPayMethod+'" > </div>' 
			               +'<div class="col-2"><span class="font-sz-level8 text-custom-color-gp ">Pln.Term&nbsp;:&nbsp;</span><span class="font-sz-level8 prdEdtClass">'+strPlanTerm+'</span><input type="hidden" class="form-control" value="'+strPlanTerm+'" > </div>' 
			               +'<div class="col-2"><span class="font-sz-level8 text-custom-color-gp ">Prem.Term&nbsp;:&nbsp;</span><span class="font-sz-level8 prdEdtClass">'+strPlanTerm+'</span><input type="hidden" class="form-control" value="'+strPlanTerm+'" > </div>' 
			               +'<div class="col-1"><span class="font-sz-level8 text-custom-color-gp ">PRR&nbsp;:&nbsp;</span><span class="font-sz-level8 prdEdtClass" id="riskRate" >'+strProdRiskRate+' </span> <i class="'+strProdRiskRateClass+'" title="'+PRRTitle+'" aria-hidden="true"></i><input type="hidden" class="form-control" value="'+strProdRiskRate+'" ><input type="hidden" name="hdnUnqdId" value="'+makeid(20)+'" ><input type="hidden" id="hRdrrecommPpId" name="hRdrrecommPpId" value="'+strrecomPpId+'"><input type="hidden" id="RdrBscId" name="RdrBscId" value="'+BscPlnprdVal+'"></div>'
			   +'</div>'
          
        +'</div>';
	 
	
	var nofBscPlsInPrinCount = $('#prodTabContent').children('#'+strClntName).find("div#"+prinCrdId).children(".PrinpalCrdBodyCont").find(".RiderPlans").length;
	
	 if(nofBscPlsInPrinCount == 0){
//		 alert(prinCrdId)
		 var strthisBscPlnObj = $('#prodTabContent').children('#'+strClntName).find("div#"+prinCrdId).children(".PrinpalCrdBodyCont");
    	 $(strthisBscPlnObj).removeClass('border-bottom').addClass('border-bottom-0');
      	 $(StrRiderPlnConts).insertAfter($(strthisBscPlnObj));
		 
	 }
	
	 if(nofBscPlsInPrinCount == 1){
    	 
    	 var thisprdVal = $('#prodTabContent').children('#'+strClntName).find("div#"+prinCrdId).children(".PrinpalCrdBodyCont").children().find('input:hidden[name=hdnFldProdVal]').val();
    	 var strthisprdVal =  $("#selFldBscPlanCombo").val();
    	 var strthisprdtxt =  $("#selFldBscPlanCombo").text();
    	 var strthisBscPlnObj = $('#prodTabContent').children('#'+strClntName).find("div#"+prinCrdId).children(".PrinpalCrdBodyCont").find("#"+strthisprdVal+"RiderPlans");
    	 $(strthisBscPlnObj).removeClass('border-bottom').addClass('border-bottom-0');
      	 $(StrRiderPlnConts).insertAfter($(strthisBscPlnObj));
      	 
      	 
      // show  Add Rider,bsc  Plan Button when selected Principal have one Basic Plan
		 showAddBscRdrPlnBtn();
		 
      	//toast msg For Successful prod Details Added in Principal
        //showToastMsg(prin,strProdType,strProdName);
      	 showAlert(prin,strProdType,strProdName);
      	 clrProdRelatedFrmDetls();	
     }
     
    if(nofBscPlsInPrinCount > 1){
    	
    	// show  Add Rider,bsc Plan Button when selected Principal have one Basic Plan
		 showAddBscRdrPlnBtn();
    	
    	var  lftSdeAddRdrprodval  = $("#hdntisBtnId").val();
    	
    	if(isEmptyFld(lftSdeAddRdrprodval)){
    	    	  
    	    	  var strBscplnVal = $("#selFldBscPlanCombo option:selected").val();
 		    	  var strBscplntxt = $("#selFldBscPlanCombo option:selected").text();
    				  if(isEmptyFld(strBscplnVal)){
    					  $("#selFldBscPlanComboError").removeClass("hide").addClass("show");
    					  $("#selFldBscPlanComboError").html("Please Select a Basic Plan to Proceed !").show();
    					  $("#selFldBscPlanCombo").addClass('err-fld');
    					  return;
    		  }else{
    			 $("#selFldBscPlanComboError").removeClass("show").addClass("hide");
    			 $("#selFldBscPlanCombo").removeClass('err-fld');
    				 var strSelBscPlnProdValObj = $('#prodTabContent').children('#'+strClntName).find("div#"+prinCrdId).children(".PrinpalCrdBodyCont").find("#"+strBscplnVal+"RiderPlans");
    					$(strSelBscPlnProdValObj).removeClass('border-bottom').addClass('border-bottom-0');
    					$(StrRiderPlnConts).insertAfter($(strSelBscPlnProdValObj));
    					/*showToastMsg(prin,strProdType,strProdName);*/
    					showAlert(prin,strProdType,strProdName);
    					clrProdRelatedFrmDetls();	
    				 }
    	     }
    	      
    	      if(!isEmptyFld(lftSdeAddRdrprodval)){
    	    	  var thidRdrPlnobj = $("#selFldBscPlanCombo option:selected").val();
    	    	  var thidRdrPlnobjTxt = $("#selFldBscPlanCombo option:selected").text();
    	    	  var strSelBscPlnProdValObj = $('#prodTabContent').children('#'+strClntName).find("div#"+prinCrdId).children(".PrinpalCrdBodyCont").find("#"+thidRdrPlnobj+"RiderPlans");
				  $(strSelBscPlnProdValObj).removeClass('border-bottom').addClass('border-bottom-0');
				  $(StrRiderPlnConts).insertAfter($(strSelBscPlnProdValObj));
				 /* showToastMsg(prin,strProdType,strProdName);*/
				  showAlert(prin,strProdType,strProdName);
				  clrProdRelatedFrmDetls();	
    	    	  
    	      }
    	   
         }
 }// Prod Type rider End---

}




$("#btnAddBscPlns").click(function(){
	//Vignesh Add On 07-05-2021
var strProd = $("#recomPpProdname option:selected").val();



















	
	if(!isEmpty(strProd)){
		    
		const swalWithBootstrapButtons = Swal.mixin({
			  customClass: {
			    confirmButton: 'btn btn-success',
			    cancelButton: 'btn btn-danger mr-3'
			  },
			  buttonsStyling: false
			})

			swalWithBootstrapButtons.fire({
			  title: 'Are you sure?',
			  text: "You already Keyin some Product Details want to Clear Product Details ?",
			  icon: 'info',
			  showCancelButton: true,
			  confirmButtonText: 'Yes, Clear it',
			  cancelButtonText: 'No, Proceed to Save',
			  reverseButtons: true
			}).then((result) => {
			  if (result.isConfirmed) {

				    clrprodFldErrMsg();
					enableBscprodType();
					triggerPrinCombo();
					$("#hdntxtPlntype").text("Basic Plan");
					clrProdRelatedFrmDetls();
					//Hide Basic Plan Combo Box when Prod Type is Basic
					$("#selFldBscPlanComboSec").removeClass("show").addClass("hide");
					//resetSelect2("recomPpProdname");
					$('#recomPpProdname').trigger('change.select2');
				  
				  
			    swalWithBootstrapButtons.fire(
			      'Clear!',
			      'Your Product Details has been cleared.',
			      'success'
			    )
			  } else if (
			    /* Read more about handling dismissals below */
			    result.dismiss === Swal.DismissReason.cancel
			  ) {
			    swalWithBootstrapButtons.fire(
			      'Proceed',
			      'Click Save & Add New Button to Proceed',
			      'error'
			    )
			  }
			})
	 }else{
		 clrprodFldErrMsg();
	     enableBscprodType();
	     triggerPrinCombo(); 
	 }
	//End
	
});

$("#btnAddRdrPlns").click(function(){
	
	//Vignesh Add On 07-05-2021
	var strProd = $("#recomPpProdname option:selected").val();
		
		if(!isEmpty(strProd)){
			    
			const swalWithBootstrapButtons = Swal.mixin({
				  customClass: {
				    confirmButton: 'btn btn-success',
				    cancelButton: 'btn btn-danger  mr-3'
				  },
				  buttonsStyling: false
				})

				swalWithBootstrapButtons.fire({
				  title: 'Are you sure?',
				  text: "You already Keyin some Product Details want to Clear Product Details ?",
				  icon: 'info',
				  showCancelButton: true,
				  confirmButtonText: 'Yes, Clear it',
				  cancelButtonText: 'No, Proceed to Save',
				  reverseButtons: true
				}).then((result) => {
				  if (result.isConfirmed) {

					    clrprodFldErrMsg();
						enableRdrprodType();
						triggerPrinCombo();
						$("#hdntxtPlntype").text("Rider Plan");
						$("#btnAddBscPlns").removeClass("hide").addClass("show");
						clrProdRelatedFrmDetls();
						//show Basic Plan Combo Box when Prod Type is Rider
						$("#selFldBscPlanComboSec").removeClass("hide").addClass("show"); 
						$('#recomPpProdname').trigger('change.select2');
					  
					  
				    swalWithBootstrapButtons.fire(
				      'Clear!',
				      'Your Product Details has been cleared.',
				      'success'
				    )
				  } else if (
				    /* Read more about handling dismissals below */
				    result.dismiss === Swal.DismissReason.cancel
				  ) {
				    swalWithBootstrapButtons.fire(
				      'Proceed',
				      'Click Save & Add New Button to Proceed',
				      'error'
				    )
				  }
				})
		 }else{
			 clrprodFldErrMsg();
			 enableRdrprodType();
		     triggerPrinCombo(); 
		 }
	 
	//End
});


function clrProdRelatedFrmDetls(){
	   //$('input[name="recomPpProdtype"]').prop('checked', false);
	   //$('input[name="recomPpBasrid"]').prop('checked', false);
	   $('#recomPpProdname').val('');
	  /* $('#recomPpProdname').select2({
				   placeholder: "Select a Product Name  below",
				   width : '100%',
				   });*/
	
	  $('#recomPpProdname').select2({
		placeholder: "Keyin the Product Name",
		width : '100%',
		allowClear: true,
		tags: true,
		selectOnClose: true,
		language: {"noResults": function(){ return "--Keyin the Product Name--"; } },
		tokenSeparators: [',', '\n','\t'],
		maximumInputLength : 150,
		createTag: function (params) {
		   //addNewProdNameToCombo(params);
		var term = $.trim(params.term);

			if(isEmpty($('#recomPpPrin').val())){return null;}

			var prodcode = makeid(12);
			//alert("prodcode 4"+prodcode)
			//var prodcode = term.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,'_');
			                 			 prodcode = prodcode.replace(/\s+/g, '_');
			                 			 prodcode = prodcode.toUpperCase();
		
		    if (term === '') {
		      return null;
		    }
		
		    return {
		      id: prodcode,
		      text: term,
		      newTag: true // add additional parameters
		    }
		  }
	});
	
	   $("#recomPpPremium").val("");
	   $("#recomPpSumassr").val("");
	   $("#recomPpPlanterm").val("");
	   $("#recomPpPayterm").val("");
	   
	   $('#recomPpPaymentmode').val('');
	   $('#prodRiskRate').val('');
	   
	   var strProdLob  = $('input[name="recomPpProdtype"]:checked').val();
	 
	   if (strProdLob == "ILP"){
	    	$("#prodRiskRate").removeAttr('disabled');
	    }
	    
	    if (strProdLob == 'PA' || strProdLob == "Life" || strProdLob == "H&S"){
	    	$("#prodRiskRate").val('');
	    	$("#prodRiskRate").prop('disabled','true');
	    }
	    
	   
	   $("#recomPpBasicRef").val("");
	   $("#recomPpPlan").val("");
	   
	
}

//Add Basic and Rider Plan Product Details using inline Add Basic and Add Rider Button Buttons 

function addEdtBasicPlans(bscPlnBtnObj){
	
	 //check checkdb onchange function for Adding prod Details
	 enableReSignIfRequiredforEdtDeltAction();
	
	 //objective Sec Validation
	  if(!validateObjSec()){return;}
	  
	  clrProdAlertInfoMsg();
	  setClientNameCombo();//set active tab client name in Combo
	  clrprodFldErrMsg();
	  RmveDisbleAttrClntCompProdPln();  
	 
	//show clear form Btn
	  $("#btnClrFrmFdls").removeClass("hide").addClass("show");
	  $("#mdlHdrImg").attr("src","vendor/avallis/img/addins.png");
	  
    //Open Modal PopUp
	  OpenProdRecommMdl();	
	  
	//hide if Add Dependent DropDown Exists  
	 $("#dropdwnSec").addClass("hide").removeClass("show");
	 
	 var clntNameTab = $(bscPlnBtnObj).closest(".PrincipalCardSec").find('input:hidden[name=hdnFldClntName]').val().toUpperCase();
//	 alert("clntNameTab"+clntNameTab)
	 $("#recomPpName").val(clntNameTab);
	 $('#recomPpName').trigger('change.select2');// $('#recomPpName').trigger('change');;// $('#recomPpName').trigger('change.select2');
	 
	 var PrinVal = $(bscPlnBtnObj).closest(".PrincipalCardSec").find('input:hidden[name=hdnFldCompName]').val();
      $('#recomPpPrin').val(PrinVal);
	
 
   var thisBtnId = $(bscPlnBtnObj).attr("id");
   //Basic prod Type
    if(thisBtnId == "badgeAddBscPln"){
		var BscPrinId = $(bscPlnBtnObj).closest(".card-header").parent().attr("id");
		 $("#hdnFldPrinCrdId").val(BscPrinId);
		 enableBscprodType();
		 triggerPrinCombo();
		 //hide or show What plan you are Currently adding(green Notification Box]
	     $("#hdntxtPlntype"). text("Basic Plan");
	     $("#hdnBscTextClnt").text(clntNameTab);
	     $("#hdnBscTextComp").text(PrinVal);
	     
	     $("#bscRdrNotInfo").removeClass("hide").addClass("Show");
	     //Hide Basic Plan Combo Box when Prod Type is Basic
	     hideBscPlanCombo();
	}
	
  //Rider Prod Type  
   if(thisBtnId == "badgeAddRdrPln"){
	   enableRdrprodType();
	   triggerPrinCombo();
	   //hide or show What plan you are Currently adding(green Notification Box]
	   $("#hdntxtPlntype").text("Rider Plan");
	   $("#hdnBscTextClnt").text(clntNameTab);
	   $("#hdnBscTextComp").text(PrinVal);
    
	   $("#bscRdrNotInfo").removeClass("hide").addClass("Show");
    
	   //show Basic Plan Combo Box when Prod Type is Rider
	   showBscPlanCombo();
    
    var compid = $(bscPlnBtnObj).closest(".card-body").parent().parent().parent().attr("id");
    $("#hdnRdrCompId").val(compid);
      
    var  rdrBscPrinIn = $(bscPlnBtnObj).closest(".card-body").find('input:hidden[name=hdnTxtprodValue]').val();
          $("#hdnFldrdrBscPrinIn").val(rdrBscPrinIn);
          
    var hdncurntBscPlanCrdId = $(bscPlnBtnObj).closest(".card-body").attr("id");
          $("#hdnFldcurntBscPlnId").val(hdncurntBscPlanCrdId);
          
    var strProdVal  = $(bscPlnBtnObj).closest(".card-body").find('input:hidden[name=hdnTxtprodValue]').val();
          $("#hdntisBtnId").val(strProdVal);
          
     
    var strProdText = $(bscPlnBtnObj).closest(".card-body").find('input:hidden[name=BscProdName]').val();
          $("#hdntxtProdText").val(strProdText);       
          $("#selFldBscPlanCombo").val(strProdVal); 
          $("#selFldBscPlanCombo").trigger('change');
          
   }
        $("#AddProdRecommDetlsMdl").on('shown.bs.modal', function(){
        	 //clear all product Related Details in Primary PopUp Form
			  $(this).find(".modal-footer").children("button:eq(1)").click(function(){
				  ClrProdFrmDetls();  //clear Entire form ,if click Reset btn in modal
			  });
			  
			     $("#alrtInfo").hide();
		       	 $('#alrtNoti').alert('close'); 
		 }); 	
}

//Delete Principal,Basic Plan, Rider Plan Product Details start below
//Delete Each Rider Plans Start Here...
function deleteThisRdrPlan(obj){
	
	
	var Rdrprod = $(obj).closest(".riderPlnDetls").find('input:hidden[name=RdrProdName]').val();
	var strRecomPpId = $(obj).closest(".riderPlnDetls").find('input:hidden[name=hRdrrecommPpId]').val();
	//Delete Individual Rider Plans 
	
	const swalWithBootstrapButtons = Swal.mixin({
		  customClass: {
		    confirmButton: 'btn btn-success',
		    cancelButton: 'btn btn-danger mr-3'
		  },
		  buttonsStyling: false
		})

		swalWithBootstrapButtons.fire({
		  title: 'Are you sure?',
		  text: "You will not be able to recover this Rider Plan : (" +Rdrprod+ ") Product Details Again!",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonText: 'Yes, delete it!',
		  cancelButtonText: 'No, cancel!',
		  reverseButtons: true
		}).then((result) => {
		  if (result.isConfirmed) {
			  
			  $.ajax({
	  	            url:"productsrecommend/deleteRider/"+strRecomPpId,
	  	            type: "POST",async:false,
	  	            success: function (response) {
	  	            	//alert(response+"success")
	  	                 $(obj).closest(".riderPlnDetls").remove();
	  	                 swalWithBootstrapButtons.fire(
	  	 		    		"Deleted!", "Your Rider Plan : (" +Rdrprod+ ") Product Details has been deleted.", "success"
	  	 		    )
	  	 		    
	  	 		//check checkdb onchange function for delete prod Details
	  	 		 enableReSignIfRequiredforEdtDeltAction();
	  	 		    
	  	            },
	  	          error: function(xhr,textStatus, errorThrown){
	  	 			//$('#cover-spin').hide(0);
	  	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
	  			}
	  	            });
			  
		    
		  } else if (
		    /* Read more about handling dismissals below */
		    result.dismiss === Swal.DismissReason.cancel
		  ) {
		    swalWithBootstrapButtons.fire(
		    		"Cancelled", "Your Rider Plan : (" +Rdrprod+ ") Product Details is safe :)", "error"
		    )
		  }
		})
	
	
	
	
}//End of Delete Each Rider Plan Details...


//delete Basic and Corresponding Rider Plans Start here 
function deleteThisBscPlan(obj){
	
	var prin = $(obj).closest(".BscPlnDetls").find('input:hidden[name=hdnFldComp]').val();
	var Bscprod = $(obj).closest(".BscPlnDetls").find('input:hidden[name=BscProdName]').val();
	var strBscProdVal  = $(obj).closest(".BscPlnDetls").find('input:hidden[name=hdnTxtprodValue]').val();
	
	var BscPlanRecommPpId = $(obj).parent().parent().parent().children().eq(1).find('input:hidden[name=hBscrecommPpId]').val();
	
    var client = $(obj).closest(".BscPlnDetls").parent().parent().find('input:hidden[name=hdnFldClntName]').val();
   
	var clntReplaced = client.replace(/[\s+\/]/g, '_');
	    clntReplaced = clntReplaced.replace(/[&\/\\#,+()@!$~%.'":*?<>{}]/g,'_');
	    clntReplaced= clntReplaced.toUpperCase();
	
	var prinReplce = prin.replace(/\s+/g, '_');
        prinReplce = prinReplce.replace(/[&\/\\#,+()@!$~%.'":*?<>{}]/g,'_');
    
	var totlBscPlanInPrin  = $(obj).parents("."+prinReplce+"crdBdyId").find(".BscPlnDetls").length;
    
	
	var totPrinCardInTab =$(obj).parents("#"+clntReplaced).find(".PrincipalCardSec").length;
    
	var totlClntTabLen = $('#prodTabs').find("a.nav-link").length;
	
	
	if(totPrinCardInTab == 1){
		
		if(totlBscPlanInPrin == 1){
		
			const swalWithBootstrapButtons = Swal.mixin({
				  customClass: {
				    confirmButton: 'btn btn-success',
				    cancelButton: 'btn btn-danger mr-3'
				  },
				  buttonsStyling: false
				})

				swalWithBootstrapButtons.fire({
				  title: 'Are you sure?',
				  text: "You will not be able to recover this Principal : (" +prin+ ")  Product Details and Client:("+client+")  Again!",
				  icon: 'warning',
				  showCancelButton: true,
				  confirmButtonText: 'Yes, delete it!',
				  cancelButtonText: 'No, cancel!',
				  reverseButtons: true
				}).then((result) => {
				  if (result.isConfirmed) {
					  $.ajax({
			 	 			 
		 	 			    url:"productsrecommend/deletePrincipalByClntName/",
		 		            type: "POST",async:false,
		 		            data:{"recommPpName":client,"recomPpPrin":prin},
		 		            success: function (response) {
		 		            	
		 		            if(totlClntTabLen == 1){
		 		            	clrAllTabs(clntReplaced);
		 		            	swalWithBootstrapButtons.fire("Deleted!", "Your Client : (" +client+ ") Product Details has been deleted.", "success");
		 		            	//check checkdb onchange function for delete prod Details
		 			  	 		 enableReSignIfRequiredforEdtDeltAction();
		 		            }
		 		            	
		 		            if(totlClntTabLen > 1){
		 		            	delCurntTab(clntReplaced);
		 		            	swalWithBootstrapButtons.fire("Deleted!", "Your Client : (" +client+ ") Product Details has been deleted.", "success");
		 		            	//check checkdb onchange function for delete prod Details
		 			  	 		 enableReSignIfRequiredforEdtDeltAction();
		 		            }
		 		            	
		 		            },
		 		           error: function(xhr,textStatus, errorThrown){
		 			 			//$('#cover-spin').hide(0);
		 			 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
		 					}
		 		            });
					  
					 } else if ( result.dismiss === Swal.DismissReason.cancel) {
				          swalWithBootstrapButtons.fire(
				    		"Cancelled", "Your Principal : (" +prin+ ")  Product Details is safe :)", "error"
				    )
				  }
				})
			
			
			
				
	     }
		
		if(totlBscPlanInPrin > 1){
			delCurntBscPlan(obj,BscPlanRecommPpId,strBscProdVal,Bscprod);
			
		}
	}
	
	if(totPrinCardInTab > 1){
		
        if(totlBscPlanInPrin == 1){
			 const swalWithBootstrapButtons = Swal.mixin({
				  customClass: {
				    confirmButton: 'btn btn-success',
				    cancelButton: 'btn btn-danger mr-3'
				  },
				  buttonsStyling: false
				})

				swalWithBootstrapButtons.fire({
				  title: 'Are you sure?',
				  text: "You will not be able to recover this Basic Plan : (" +Bscprod+ ") and, Corresponding Rider Plan Product Details and whole Principal  Again!",
				  icon: 'warning',
				  showCancelButton: true,
				  confirmButtonText: 'Yes, delete it!',
				  cancelButtonText: 'No, cancel!',
				  reverseButtons: true
				}).then((result) => {
				  if (result.isConfirmed) {
					  
					  $.ajax({
			  	            url:"productsrecommend/deleteBasicPlnSet/"+strBscProdVal,
			  	            type: "POST",async:false,
			  	            success: function (response) {
			  	            	
			  	            	if(totPrinCardInTab == 1){
			  	            		$(obj).closest(".BscPlnDetls").parent().parent().parent().parent().empty();
			  	            		$("#NoProdDetlsRow").removeClass("hide").addClass("show");
			  	            		hidePrimaryAddProdRecommBtn();
			  	            		swalWithBootstrapButtons.fire("Deleted!", "Your Basic Plan : (" +Bscprod+ ") Product Details has been deleted.", "success");
			  	            	     //check checkdb onchange function for delete prod Details
			  	 	  	 		     enableReSignIfRequiredforEdtDeltAction();
			  	            	}else{
			  	            		$(obj).closest(".BscPlnDetls").parent().parent().remove();	
			  	            		swalWithBootstrapButtons.fire("Deleted!", "Your Basic Plan : (" +Bscprod+ ") Product Details has been deleted.", "success");
			  	            	    //check checkdb onchange function for delete prod Details
			  	 	  	 		     enableReSignIfRequiredforEdtDeltAction();
			  	            	}
			  	           },
			  	         error: function(xhr,textStatus, errorThrown){
			 	 			//$('#cover-spin').hide(0);
			 	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			 			}
			  	            });
					   
					 } else if ( result.dismiss === Swal.DismissReason.cancel) {
				          swalWithBootstrapButtons.fire(
				    		"Cancelled", "Your Principal : (" +PrinProd+ ")  Product Details is safe :)", "error"
				    )
				  }
				})
        	  
        	  
        	  
        	  
		}
        
        if(totlBscPlanInPrin > 1){
        	delCurntBscPlan(obj,BscPlanRecommPpId,strBscProdVal,Bscprod);
        }
		
	}
    
}


// Delete whole Principal Card Start Here...

function deleteThisPrincipal(obj){
	var PrinProd = $(obj).closest(".PrincipalCardSec").find('input:hidden[name=hdnFldCompName]').val();
    var totPrinCrdLenInTab = $(obj).parent().parent().parent().parent().parent().children().length;
    var activClnt =$(obj).closest(".PrincipalCardSec").find('input:hidden[name=hdnFldClntName]').val(); 
     
    var orgnlClnt = activClnt;
       activClnt =  activClnt.replace(/[\s+\/]/g, '_');
       activClnt = activClnt.replace(/[&\/\\#,+()@!$~%.'":*?<>{}]/g,'_');
       
                                       activClnt=activClnt.toUpperCase();
   
   var prinreplacd = PrinProd.replace(/[&\/\\#,+()@!$~%.'":*?<>{}]/g,'_');//replace special character by underScore Char
       prinreplacd = prinreplacd.replace(/\s+/g, '_');//replace empty spaces by underScore Char
   var totlClntTabLen = $('#prodTabs').find("a.nav-link").length;
 
       const swalWithBootstrapButtons = Swal.mixin({
			  customClass: {
			    confirmButton: 'btn btn-success',
			    cancelButton: 'btn btn-danger mr-3'
			  },
			  buttonsStyling: false
			})

			swalWithBootstrapButtons.fire({
			  title: 'Are you sure?',
			  text: "You will not be able to recover this Principal : (" +PrinProd+ ")  Product Details Again!",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonText: 'Yes, delete it!',
			  cancelButtonText: 'No, cancel!',
			  reverseButtons: true
			}).then((result) => {
			  if (result.isConfirmed) {
				  
				  $.ajax({
		 	 			 
	 	 			    url:"productsrecommend/deletePrincipalByClntName/",
	 		            type: "POST",async:false,
	 		            data:{"recommPpName":orgnlClnt,"recomPpPrin":PrinProd},
	 		            success: function (response) {
	 		            	
	 		            	if(totlClntTabLen == 1){
	 		            		if(totPrinCrdLenInTab == 1){
	 		            			clrAllTabs(activClnt);
	 		            			swalWithBootstrapButtons.fire("Deleted!", "Your Principal : (" +PrinProd+ ") Product Details has been deleted.", "success");
	 		            			//check checkdb onchange function for delete prod Details
	 		      	  	 		     enableReSignIfRequiredforEdtDeltAction();
	 		            		}
	 		            		
	 		            		if(totPrinCrdLenInTab > 1){
	 		            			$(obj).closest(".PrincipalCardSec").parent("#"+activClnt).find("#"+prinreplacd+"cardDetls").remove();
	 		            			swalWithBootstrapButtons.fire("Deleted!", "Your Principal : (" +PrinProd+ ") Product Details has been deleted.", "success");
	 		            			//check checkdb onchange function for delete prod Details
	 		      	  	 		     enableReSignIfRequiredforEdtDeltAction();
	 		            		
	 		            		}
	 		            	}
	 		            	if(totlClntTabLen > 1){
	 		            	
	 		            		if(totPrinCrdLenInTab == 1){
	 		            			delCurntTab(activClnt);
	 		            			swalWithBootstrapButtons.fire("Deleted!", "Your Principal : (" +PrinProd+ ") Product Details has been deleted.", "success");
	 		            			//check checkdb onchange function for delete prod Details
	 		      	  	 		    enableReSignIfRequiredforEdtDeltAction();
	 		            		}
	 		            		
	 		            		if(totPrinCrdLenInTab > 1){
		 		            		$(obj).closest(".PrincipalCardSec").parent("#"+activClnt).find("#"+prinreplacd+"cardDetls").remove();
		 		            		swalWithBootstrapButtons.fire("Deleted!", "Your Principal : (" +PrinProd+ ") Product Details has been deleted.", "success");
		 		            		//check checkdb onchange function for delete prod Details
		 		  	  	 		    enableReSignIfRequiredforEdtDeltAction();
	 		            		}
	 		            		
	 		            	}
	 		          },
			 		         error: function(xhr,textStatus, errorThrown){
			 		 			//$('#cover-spin').hide(0);
			 		 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			 				}
	 		            });
				   
				 } else if ( result.dismiss === Swal.DismissReason.cancel) {
			          swalWithBootstrapButtons.fire(
			        		  "Cancelled", "Your Principal : (" +PrinProd+ ")  Product Details is safe :)", "error"
			    )
			  }
			})
       
    }


function deleteThisClient(obj){
    var strClient = $(obj).attr("id");
    var clntReplaced = strClient.replace(/[\s+\/]/g, '_');
        clntReplaced = clntReplaced.replace(/[&\/\\#,+()@!$~%.'":*?<>{}]/g,'_');
    var totlClntTabLen = $('#prodTabs').find("a.nav-link").length;
   
  const swalWithBootstrapButtons = Swal.mixin({
		  customClass: {
		    confirmButton: 'btn btn-success',
		    cancelButton: 'btn btn-danger mr-3'
		  },
		  buttonsStyling: false
		})

		swalWithBootstrapButtons.fire({
		  title: 'Are you sure?',
		  text: "You will not be able to recover this Client Name : (" +strClient+ ")  Product Details Again!",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonText: 'Yes, delete it!',
		  cancelButtonText: 'No, cancel!',
		  reverseButtons: true
		}).then((result) => {
		  if (result.isConfirmed) {
			  
			  $.ajax({
		            url:"productsrecommend/deleteClient/"+strClient,
		            type: "POST",async:false,
		             success: function (response) {
		            		if(totlClntTabLen == 1){
		            			$(obj).closest(".ProdDetlsSec").children().eq(1).find("#prodTabContent").empty();
		    	            	hideProdTabSec();
		            		}
		            		
		            		if(totlClntTabLen > 1){
		            		  delCurntTab(clntReplaced);
		            		}
		            		swalWithBootstrapButtons.fire("Deleted!", "Client Name : (" +strClient+ ") has been deleted.", "success");
		            		//check checkdb onchange function for delete prod Details
		   	  	 		     enableReSignIfRequiredforEdtDeltAction();
		            		
		            		
		            		if(chkOptionExists("recomPpName",strClient)) {
//			      				addOption("recomPpName",strClntName.toUpperCase(),strClntName.toUpperCase())
		            			//removeOptionByVal("recomPpName",strClient.toUpperCase());
			      			}
		           },
			           error: function(xhr,textStatus, errorThrown){
				 			//$('#cover-spin').hide(0);
				 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
						}
		            });
			 
			   
			 } else if ( result.dismiss === Swal.DismissReason.cancel) {
		          swalWithBootstrapButtons.fire(
		        		  "Cancelled", "Client Name : (" +strClient+ ")  Product Details is safe :)", "error"
		    )
		  }
		})
 
 }


// onchange Basic Plan Product Field Combo
 $("#selFldBscPlanCombo").change(function(){
	 var prodName = $(this).val();
	 $("#selFldBscProdLobCombo").val(prodName);
	 var selLobTmp =   $("#selFldBscProdLobCombo option:selected").text();
	 $('input[name="recomPpProdtype"][value="' + selLobTmp + '"]').prop('checked', true).trigger("click");
	 if(isEmptyFld(prodName)){
			$("#selFldBscPlanComboError").removeClass("hide").addClass("show");
			$("#selFldBscPlanCombo").addClass('err-fld');
			return;
		}else{
			$("#selFldBscPlanComboError").removeClass("show").addClass("hide");
			$("#selFldBscPlanCombo").removeClass('err-fld');
		}
 });

//Add New Dependent in Prod Recomm screen
 
 $("#btnAddNewDepntClnt").click(function(){
	var strNewClnt =  $("#txtFlddropAddNewClient").val();
	if(isEmptyFld(strNewClnt)){
		$("#errorFld").removeClass("hide").addClass("show");
		$("#txtFlddropAddNewClient").addClass('err-fld');
		return;
	}else{
		$("#errorFld").removeClass("show").addClass("hide");
		$("#txtFlddropAddNewClient").removeClass('err-fld');
		
		$("#dropdwnSec").addClass("hide").removeClass("show");
		
		var data = {
			    id: strNewClnt,
			    text: strNewClnt
			};
		
		 // Create a DOM Option and pre-select by default
	    var newOption = new Option(data.text, data.id, true, true);
	    
	}
		
 });
 
 $("#txtFlddropAddNewClient").keypress(function() {
	    $("#errorFld").removeClass("show").addClass("hide");
		$("#txtFlddropAddNewClient").removeClass('err-fld');
	 
 });
 
 //Show toast Mesage function 
 
 function showToastMsg(prin,strProdType,strProdName){
	 
	 if($("#hdnAction").val() == "I"){
		 var insFlg = "Inserted";
		  enableClrClnt();
	 }
	 if($("#hdnAction").val() == "E"){
		 var insFlg = "Updated";
		  disableClrClnt();
	 }
	 
	  $.toast({
		    heading: "Product "+insFlg+" Successfully",
		    text: [
		        'Principal&nbsp;:&nbsp;'+prin, 
		        'Product Type&nbsp;:&nbsp;'+strProdType, 
		        'Product Name&nbsp;:&nbsp;'+strProdName,
		    ],
		    showHideTransition: 'plain',
		    icon: 'info',
		   
		    position: 'mid-center',
		    loaderBg: '#82d3f7',
		    hideAfter: 1500,
		    allowToastClose: true,
		    stack: 2
		});
 }
 
 
 //show Alert Message 
 
 function showAlert(prin,strProdType,strProdName){
	 
	 var insFlg,bgClass;
	 if($("#hdnAction").val() == "I"){
		  insFlg = "Inserted";
		  bgClass = "alert-success";
		  enableClrClnt();
	 }
	 if($("#hdnAction").val() == "E"){
		  insFlg = "Updated";
		  bgClass = "alert-info";
		  disableClrClnt();
	 }
	 
	   var strAlrtNoti = '<div class="alert '+bgClass+' role="alert" id="alrtNoti">'
                       +'<div class="alert-heading font-sz-level5 mb-1"><i class="fa fa-bell"  aria-hidden="true"></i>&nbsp;Data '+insFlg+' Success!</div>'
			                  +'<hr class="m-2">'
			                 +'<div class="font-sz-level6"><span>Principal : </span><span>'+prin+'.</span>' 
			                 +'</div>' 
			                 +'<div class="font-sz-level6"><span>Prod.Type : </span><span>'+strProdType+'.</span>' 
			                 +'</div>' 
			                 +'<div class="font-sz-level6"><span>Product : </span><span>'+strProdName+'.</span>'  
	                   +'</div>'
	                   +'</div>';
               
      $("#alrtInfo").html(strAlrtNoti);
      
//      $("#alrtInfo").removeClass("d-none");//.addClass("show");
      $("#alrtInfo").show()
    
		
      
      
      setTimeout(function () { 
    	  
          // Closing the alert 
          $('#alrtNoti').alert('close'); 
      }, 3000)
 
 }
 
//open Modal PopUp
 
 function OpenProdRecommMdl(){
	 $("#hdnAction").val("I");
	 $("#prdMdlHdr").text("Add");
	 if (!($('.modal.in').length)) {
		    $('.modal-dialog').css({
		      top: '5%',
		      left: '0',
		      right:'0',
		      margin:'auto',
		      bottom:'0'
		    });
		  }
		  $('#AddProdRecommDetlsMdl').modal({
		    backdrop: 'static',
		    keyboard: false,
		    show: true
		  });

		  $('.modal-dialog').draggable({
		    handle: ".modal-header,.modal-body,.modal-footer"
		  });
		  
		  $("#btnsaveAddNewPrdDetls").removeClass("btn-primary").addClass("btn-success");
		  $("#btnsaveAddNewPrdDetls").text("Save & Add New");
		  
		  $("#taskNotiFlg").removeClass("badge-primary").addClass("badge-success");
		  $("#taskNotiFlg").text("Adding");
 }
 
 
 // edit Basic and Rider Plan Details
 
 function editThisBscRdrPlan(thisObj){
	 $("#recomPpId").val("");
	   
	//hide clear form Btn
	  $("#btnClrFrmFdls").removeClass("show").addClass("hide");
	  
    //Hide basic Plan Combo Box
	 $("#selFldBscPlanComboSec").removeClass("show").addClass("hide");
	 
	
	 //hide if Add Dependent DropDown Exists  
	 $("#dropdwnSec").addClass("hide").removeClass("show");
	 
	// hide if prod same -->error shows
	 $("#errInfoSameProd").addClass("hide").removeClass("show");
	 
	//show Noti Box (Green Box)
	 $("#bscRdrNotInfo").removeClass("hide").addClass("show");

	 
	 // Clear Entire Form
	 ClrProdFrmDetls(); 
	 
	 
	 //Open PopUp Form
	  OpenProdRecommMdl();
	  
	 //change save  and Add New Btn label and Icon for Edit Purpose
	  $("#btnsaveAddNewPrdDetls").removeClass("btn-success").addClass("btn-primary");
	  $("#btnsaveAddNewPrdDetls").text("Edit & Close");
	  $("#btnClose").html("Cancel");
	  
	  $("#taskNotiFlg").removeClass("badge-success").addClass("badge-primary");
	  $("#taskNotiFlg").text("Editing");
	  
	  //show Noti Box (Green Box)
      $("#bscRdrNotInfo").removeClass("hide").addClass("show");
	  
	  
	  $("#hdnAction").val("E");
	  $("#prdMdlHdr").text("Edit");
	  
	  $("#mdlHdrImg").attr("src","vendor/avallis/img/pencil (1).png");//    height: 3.3vh;
	  $("#mdlHdrImg").css('height','3.3vh');
	  
	  hideAddBscRdrPlnBtn();
	  
	 // $(thisObj).parent().parent().parent().not(this).css("background-color", "red");
	 // $(thisObj).parent().parent().parent().css("background-color", "#bcb5da70");
	  
	  var thisPlnObj = $(thisObj).parent().parent().parent().attr("id");
	  var bscSetId = $(thisObj).parent().parent().parent().parent().attr("id");
	
	  
	  ftchProdHdnFldValtoPopUpFrm(thisPlnObj,bscSetId);
}

 function ftchProdHdnFldValtoPopUpFrm(thisPlnObj,bscSetId){
	   var ProdDetlsArr = [];
	    $("#hEdtBscSetId").val(bscSetId);
	   
	    $('#prodTabContent div.active').find("div#"+thisPlnObj).find('input:hidden').each(function(){
			ProdDetlsArr.push($(this).val());
		});
	    
//	    console.log("ProdDetlsArr Array ==>>>>>>>>>>>" + ProdDetlsArr);
	    
	    
	    
	    var totlTabsLen =  $('#prodTabs li').length;
	    if(totlTabsLen > 0 ){
	    	var  strTab;
	    	  $('#prodTabs li a.nav-link').each(function(){
	    		  var flag  = $(this).hasClass('active');
	    		  if(flag){
	    			  strTab = $(this).text(); 
	    		 }
				}); 
	    	 
	    	  $('#recomPpName').val(strTab.trim());
	    	  $('#recomPpName').trigger('change.select2');//	 $('#recomPpName').trigger('change');;// $('#recomPpName').trigger('change.select2');
			  $("#recomPpName").prop('disabled', true);
			  $("#hdnBscTextClnt").text($("#recomPpName").val());
			  $("#hEdtClntName").val($("#recomPpName").val());
			  
	    }
	    
	    var strSel2TriggerClass= "";
		var strprodType = ProdDetlsArr[3];
		if(strprodType =="B")strprodType="BASIC";
		if(strprodType =="R")strprodType="RIDER";
		   if(strprodType == "RIDER"){
			   enableRdrprodType();
			   triggerPrinCombo();
			   $("INPUT[name=recomPpBasrid]").val([strprodType]);
			   $("#hdntxtPlntype").text("Rider Plan");
			    strSel2TriggerClass = "change";
				}
		   
		
		   
		   if(strprodType == "BASIC"){
			   enableBscprodType();
			   triggerPrinCombo();
			    $("INPUT[name=recomPpBasrid]").val([strprodType]);
				$("#hdntxtPlntype").text("Basic Plan");
				strSel2TriggerClass = "change";
				
			}
		   
	    
	    var strprdVal = ProdDetlsArr[0];
	  
	    $("#hEtdPln").val(strprdVal);
	   
	    $("#recomPpProdname").prop('disabled', true);
	    
	    
	   var strcomp = ProdDetlsArr[1];
	   
	   if ($('#recomPpPrin').find("option[value='" + strcomp + "']").length == 0) {
			var newOption = new Option(strcomp, strcomp, true, true);
			$('#recomPpPrin').append(newOption)
		}
	   
			$('#recomPpPrin').val(strcomp);
		    $('#recomPpPrin').trigger(strSel2TriggerClass);
		    $("#recomPpPrin").prop('disabled', true);
		    $("#hdnBscTextComp").text($("#recomPpPrin").val())
		    $("#hEdtComp").val(strcomp); 
			
			var strprodNme = ProdDetlsArr[2];
			
			if ($('#recomPpProdname').find("option[value='" + strprodNme + "']").length == 0) {
				var newOption = new Option(strprdVal, strprodNme, true, true);
    			$('#recomPpProdname').append(newOption)
			}
			
			
			  $('#recomPpProdname').val(strprodNme);
		      $("#recomPpProdname").trigger('change.select2')
			  $("#hEtdPlnVal").val(strprodNme);
		  
		   var strprodLob = ProdDetlsArr[4];
			  if (strprodLob == "ILP"){
			    	$("#prodRiskRate").removeAttr('disabled');
			    }
		       if (strprodLob == 'PA' || strprodLob == "Life" || strprodLob == "H&S"){
		    	    $("#prodRiskRate").val('');
			    	$("#prodRiskRate").prop('disabled','true');
			    }	
		     
		       $('input[name="recomPpProdtype"][value="' + strprodLob + '"]').prop('checked', true).trigger("click");
		    
		 var strprem = ProdDetlsArr[5];
		    if(strprem == "--NIL--"){
		    	$("#recomPpPremium").val('');
		    }else{
		    	$("#recomPpPremium").val(strprem);
		    }
		  
		   
		 var strsumAssrd = ProdDetlsArr[6];
		    if(strsumAssrd == "--NIL--"){
		    	$("#recomPpSumassr").val('');
		    }else{
		    	$("#recomPpSumassr").val(strsumAssrd);
		    }
		  
		   
		   
		 var strPayType = ProdDetlsArr[7];
		     if(strPayType == "--NIL--"){
		      // $("#recomPpPaymentmode").val("");
		       $("#recomPpPaymentmode option[value='']").attr('selected', true);
		     }
		     if(strPayType != "--NIL--"){
			       $("#recomPpPaymentmode").val(strPayType); 
			 }
			   
		   
		 var strPlnTrm =  ProdDetlsArr[8];
		   
		   if(strPlnTrm == "--NIL--"){
			   $("#recomPpPlanterm").val('');
		    }else{
		    	$("#recomPpPlanterm").val(strPlnTrm);
		    }
		   
		   
		 var strPremTrm = ProdDetlsArr[9];
		  
		     if(strPremTrm == "--NIL--"){
		    	 $("#recomPpPayterm").val('');
		    }else{
		    	 $("#recomPpPayterm").val(strPremTrm);
		    }
		     
		     var strPrdRiskRate = ProdDetlsArr[10];
			  
		     if(strPrdRiskRate == "--NIL--"){
		    	 $("#prodRiskRate").val('');
		    }else{
		    	 $("#prodRiskRate").val(strPrdRiskRate);
		    } 
		     
		     
		     
		  $("#hdnUnqId").val(ProdDetlsArr[11])
		  
		  $("#recomPpId").val(ProdDetlsArr[12])
		  
		  $("#recomPpBasicRef").val(ProdDetlsArr[13])
		  
		  
		  
		  
		 
 }
 
 
 
 $("#recomPpProdname").click(function(){
	 var bscPln = $("#selFldBscPlanCombo").val(); 
	 if(isEmptyFld(bscPln)){
			$("#selFldBscPlanComboError").removeClass("hide").addClass("show");
			$("#selFldBscPlanCombo").addClass('err-fld');
			return;
		}else{
			$("#selFldBscPlanComboError").removeClass("show").addClass("hide");
			$("#selFldBscPlanCombo").removeClass('err-fld');
		}
 });
 
 
function RmveDisbleAttrClntCompProdPln(){
 	$("#recomPpPrin").removeAttr('disabled');
	$("#recomPpName").removeAttr('disabled');
	$("#recomPpProdname").removeAttr('disabled');
	
 }
 
 
//Hide  Add Rider, Basic Button when selected Principal have more than one Basic Plans
 function hideAddBscRdrPlnBtn(){
	$("#btnAddBscPlns").addClass("hide").removeClass("show");
	$("#btnAddRdrPlns").addClass("hide").removeClass("show");
 }
 
//show  Add Rider, Basic Button when selected Principal have more than one Basic Plans
 function showAddBscRdrPlnBtn(){
	$("#btnAddBscPlns").removeClass("hide").addClass("show");
	$("#btnAddRdrPlns").removeClass("hide").addClass("show");
  }
 
 function enableBscprodType(){
	$("#prodTypeBasicSec").addClass("show").removeClass("hide");
    $("#prodTypeRiderSec").addClass("hide").removeClass("show");
	$("INPUT[name=recomPpBasrid]").val(['BASIC']);
  }
  
 function enableRdrprodType(){
		$("#prodTypeRiderSec").addClass("show").removeClass("hide");
		$("#prodTypeBasicSec").addClass("hide").removeClass("show");
		$("INPUT[name=recomPpBasrid]").val(['RIDER']);
	}
 
 //trigger principal Change function
 function triggerPrinCombo(){
	 $("#recomPpPrin").trigger('change');
 }
 
 //Add New Dependent or Client in Prod Recomm Screen -23-10-2020
 function addNewDepntClnt(){
	
	var options = {
 		    tags: true,
 		    createTag: function(params) {
 		        return {
 		            id: params.term, //	UTILIZZATO PER INSERIRE NUOVO VALORE NEL DATABASE
 		            text: params.term,
 		            newOption: true,
 		           //closeOnSelect: false
 		        }
 		    },
 		    templateResult: function(data) {
 		    	
 		     var $result = $("<span></span>");
 		    	//var $result  =	$('<span><img src="vendor/avallis/img/profile.png"> | <span class="font-sz-level5">' + data.text + '</span></span>');

 		        $result.text(data.text);

 		        if (data.newOption) {
 		        	var clntNot = '<br><span><small class="font-sz-level8" style="color:#fff;">&nbsp;(Type New Client Name and Click <kbd class="font-sz-level8">&nbsp;Enter&nbsp;</kbd> Button )</small></span>';
 		            $result.append(clntNot);
 		        }

 		       return $result;
 		    }
 		}

 		var $select2 = $('#recomPpName').select2(options);
 		$select2.on('select2:select', function(e) {
 		    debugger;
 		    var data = e.params.data;

 		    var text = data.text;

 		    if (data.newOption) {

               if ($('#recomPpName optgroup[label="Search Client or Add New Client Details"]').length > 0) {

 		            var option = $("<option></option>");
 		            option.val(data.id);
 		            option.text(text);

 		            $('#recomPpName optgroup[label="Search Client or Add New Client Details"]').append(option);
 		            $('#recomPpName').val(data.id).trigger("change");


 		        } else {

 		            var optgroup = $('<optgroup>');
 		            optgroup.attr('label', "Search Client or Add New Client Details");

 		            var option = $("<option></option>");
 		            option.val(data.id);
 		            option.text(text);

 		            optgroup.append(option);

 		            $('#recomPpName').append(optgroup);

 		            $('#recomPpName').val(data.id)

 		        }
 		       $select2.select2(options);
                }

 		});
}
 
 //Fetch Prod Recomm Data 
 
 $("#btnFetchRecomData").click(function(){
	 
	 var fnaId = $("#fnaDetails").val();

	 $.ajax({
         url:"productsrecommend/allRecommend/"+fnaId,
         type: "GET",async:false,
         dataType: "json",
         contentType: "application/json",
         
         success: function (data) {
	            for (var i=0; i<data.length; i++) {
	            	creteNewTabFun(data[i].recomPpName);	
		     		   createPrincipalCards(data[i],data[i].recomPpPrin,data[i].recomPpName,data[i].recomPpId); 
     }
         	
         },
         error: function(xhr,textStatus, errorThrown){
	 			//$('#cover-spin').hide(0);
	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			}
         });
	 
	
	 
 });
 

 function clrProdPpProductCombo() {
		 
	 var select = document.getElementById("recomPpProdname");
	 var length = select.options.length;
	 for (i = length-1; i >= 0; i--) {
	   select.options[i] = null;
	   }
	 
	 $("#recomPpProdname").append('<option value="" selected="true" data-select2-id="select2-data-4-j3jb">Keyin the Product Name</option>')
	 
	 }
		
    function hideProdTabSec() {
		$("#prodTabs").empty();
		$("#prodTabs").addClass("hide").removeClass("show");
		$("#NoProdDetlsRow").removeClass("hide").addClass("show");
		hidePrimaryAddProdRecommBtn();
	}
 
    function clrselFldBscPlanCombo(){
    	var selectFldId = document.getElementById("selFldBscPlanCombo");
		  var selectOptLen = selectFldId.options.length;
		     for (i = selectOptLen-1; i > 0; i--) {
			     selectFldId.options[i] = null;
		     }
	  }

 
  
 
 
 
 
 //objective sec Validation 
    function slctAllObjTypes(obj){
    	
    	if($(obj).is(":checked")){
    		$("#htfarfamincpro").prop("checked",true);
    		$("#htfarret").prop("checked",true);
    		$("#htfarinv").prop("checked",true);
    		$("#htfardisincpro").prop("checked",true);
    		$("#htfarednincpro").prop("checked",true);
    		
    		var prodObjOptObj = {ALLOBJ: "Y",PRO:"Y",RET:"Y",INV:"Y",HIN:"Y",SAV:"Y"};
    		$("#prodrecObjectives").val(JSON.stringify(prodObjOptObj));
    		hideObjSecError();
    		}
        if(!$(obj).is(":checked")){
        	$("#htfarfamincpro").prop("checked",false);
    		$("#htfarret").prop("checked",false);
    		$("#htfarinv").prop("checked",false);
    		$("#htfardisincpro").prop("checked",false);
    		$("#htfarednincpro").prop("checked",false);
    		$("#prodrecObjectives").val("");
    		showObjSecError();
    		
    	}
    	
    }
 
 
 

  //old kyc Reuse functions
    function fnaAssgnFlg(obj, id) {
    	var numberOfChecked = $("#objSec").find('input:checkbox:checked').length;
   		if (id == "chk") {
   			if (obj.checked) {
   				obj.value = "Y";
   				hideObjSecError();
   				if(numberOfChecked == 5){
   					$("#htfarother").prop("checked",true);
   					$("#htfarother").val("Y");
   					var prodObjOptObject = {ALLOBJ: "Y",PRO:"Y",RET:"Y",INV:"Y",HIN:"Y",SAV:"Y"};
   		    		$("#prodrecObjectives").val(JSON.stringify(prodObjOptObject));
   				}
   			
   			} else if (!obj.checked) {
   				obj.value = "";
   				
   				if(numberOfChecked == 0){
   				    showObjSecError();
   			    }
   				
   				var otherval  = $("#htfarother").val();
   				if(otherval == "Y"){
   					$("#htfarother").prop("checked",false);
   					$("#htfarother").val("");
   					
   					var prodObjOptObj = {PRO:"Y",RET:"Y",INV:"Y",HIN:"Y",SAV:"Y"};
   		    		$("#prodrecObjectives").val(JSON.stringify(prodObjOptObj));
   					
   				}
   				
   				
   				
   			}
   			
   		} else {
   			if (obj.checked) {
   				document.getElementById(id).value = obj.value;
   			}
   		}
   	}
    
    
    function chkJsnOptions(chkbox,jsnFld){
   	    var advAppTypeOpt = $("#"+jsnFld+"");
   		var jsonvalues = JSON.parse(isEmpty(advAppTypeOpt.val()) ? "{}" : advAppTypeOpt.val());
   		var chkd = chkbox.checked;
   		var val = $(chkbox).attr("data");

   		var newobj=jsonvalues;
   		newobj[val]=(chkd == true ? "Y" :"N");
   		advAppTypeOpt.val(JSON.stringify(newobj));
   	}
    
   function saveCurrentPageData(obj,nextscreen,navigateflg,infoflag){
//    	var nextPge  = $(obj).attr("href");
//    	alert(nextPge)
    	var frmElements = $('#divProdRecomFrm :input').serializeObject();
    	  
    		$.ajax({
    	            url:"fnadetails/register/productrecom/",
    	            type: "POST",
    	            async:false,
    	            dataType: "json",
    	            contentType: "application/json",
    	            data: JSON.stringify(frmElements),
    	            success: function (response) {
						if(navigateflg){
		   	            	window.location.href=nextscreen;
						}
						
    	            	
    	            },
    	            error: function(xhr,textStatus, errorThrown){
    		 			//$('#cover-spin').hide(0);
    		 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
    				}
    	            });
    	  }
    
     function delCurntTab(clnt){
			$('#prodTabs').find("#"+clnt).parent().remove();
			$('#prodTabContent').find("#"+clnt).remove(); 
			 
			$('#prodTabs').children().eq(0).find("a.nav-link").addClass('active');
			$('#prodTabContent').children().eq(0).addClass('active show');
		}
    
    function clrAllTabs(clnt){
 		$("#"+clnt).parents(".prodContentSec").children().eq(0).find("#prodTabs").empty();
     	$("#"+clnt).parents(".prodContentSec").children().eq(0).find("#prodTabs").addClass("hide");
     	
     	$("#"+clnt).parents(".prodContentSec").children().eq(0).find("#prodTabContent").empty();
     	$("#NoProdDetlsRow").removeClass("hide").addClass("show");
     	hidePrimaryAddProdRecommBtn();
 	}
    
    function delCurntBscPlan(thisObj,ppId,prodval,prod){
		
		const swalWithBootstrapButtons = Swal.mixin({
			  customClass: {
			    confirmButton: 'btn btn-success',
			    cancelButton: 'btn btn-danger mr-3'
			  },
			  buttonsStyling: false
			})

			swalWithBootstrapButtons.fire({
			  title: 'Are you sure?',
			  text: "You will not be able to recover this Basic Plan : (" +prod+ ") and, Corresponding Rider Plan  Product Details Again!",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonText: 'Yes, delete it!',
			  cancelButtonText: 'No, cancel!',
			  reverseButtons: true
			}).then((result) => {
			  if (result.isConfirmed) {
				  
				  $.ajax({
		  	            url:"productsrecommend/deleteBscPlnByRecommPpId/",
		  	            type: "POST",async:false,
		  	            data:{"recommPpId":ppId,"prodVal":prodval},
		  	            success: function (response){
		  	            	$(thisObj).closest(".BscPlnDetls").remove();
		  	            	swalWithBootstrapButtons.fire("Deleted!", "Your Basic Plan : (" +prod+ ") Product Details has been deleted.", "success");
		  	                //check checkdb onchange function for delete prod Details
		  		  	 		 enableReSignIfRequiredforEdtDeltAction();
		  	           },
				  	         error: function(xhr,textStatus, errorThrown){
				 	 			//$('#cover-spin').hide(0);
				 	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
				 			}
		  	            });
				  
				 } else if ( result.dismiss === Swal.DismissReason.cancel) {
			          swalWithBootstrapButtons.fire(
			        		 "Cancelled", "Your Basic Plan : (" +prod+ ") Product Details is safe :)", "error"
			    )
			  }
			})
		
	}
    
    function clrprodFldErrMsg(){
    	$("#txtFldClntNameError").removeClass("show").addClass("hide");
    	$("#recomPpPrinError").removeClass("show").addClass("hide");
    	$("#radBtnprodLobError").removeClass("show").addClass("hide");
    	$("#recomPpProdnameError").removeClass("show").addClass("hide");
    	//$("#selFldBscPlanComboError").removeClass("show").addClass("hide");
        $("#errInfoSameProd").removeClass('show').addClass('hide');
        
        $("#txtFldPremiumError").removeClass('show').addClass('hide');
        $("#txtFldSAError").removeClass('show').addClass('hide');
        
        
    }
    
    function hideBscPlanCombo(){
		  $("#selFldBscPlanComboSec").removeClass('show').addClass('hide'); 
	  }
    function showBscPlanCombo(){
		  $("#selFldBscPlanComboSec").removeClass('hide').addClass('show'); 
	  }
    function loadRecommSumDet(){
    	//clear all table rows
    	$("#prodSummTable").find("#prodSummTblBody").children().remove();


    	
        $.ajax({
            url:"productsrecommend/prodRecomSumDetls/",
            type: "GET",async:false,
            dataType: "json",
            contentType: "application/json",
            success: function (response) {
            	//hide consolidated ppopup if(no summary detls available) show error msg alert
            	if(response.length == 0){
            		
            		$("#btnViewSumDetls").removeAttr("data-toggle","modal");
            		$("#btnViewSumDetls").removeAttr("data-target","#prodConsDetls");
            		$("#btnViewSumDetls").removeAttr("data-backdrop","static");
            		$("#btnViewSumDetls").removeAttr("data-keyboard","false");
            		
            		toastr.clear($('.toast'));
            		toastr.options = {  timeOut: "5000", extendedTimeOut: "1000"};
            		toastr["error"]("No Product Summary details Available !");	
            		
            	}
            	
            	//show consolidated popup window if product recommend record count is > 0
            	if(response.length > 0){
            		
            		$("#btnViewSumDetls").attr("data-toggle","modal");
            		$("#btnViewSumDetls").attr("data-target","#prodConsDetls");
            		$("#btnViewSumDetls").attr("data-backdrop","static");
            		$("#btnViewSumDetls").attr("data-keyboard","false");
            		
            		//hide Loader
                	$("#prodConsDetls").find(".modal-body").find('#srchClntLoader').removeClass("d-block").addClass("hide");
                	console.log(response);
                	
                	
                    var jsnDataProdRecomSumDetls= response;
                    viewProdSummaryDets(jsnDataProdRecomSumDetls);
            	}
             },
	             error: function(xhr,textStatus, errorThrown){
	 	 			//$('#cover-spin').hide(0);
	 	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
	 			}
            });
    	
    }


var prodMap;
var compMap;
var ridMap;
function viewProdSummaryDets(data){
	prodMap = new Map();
	compMap=new Map();
	ridMap=new Map();
	for(var i=0;i<data.length;i++){
		var prod=data[i];
		createMapData(prodMap,prod.recomPpName.toUpperCase(),prod);
	}
	setCompanyMapData();
	setTableData();
}

function createMapData(map,key,value){
	
	
    if (!map.has(key)) {
	   var setVal=new Set();
      
        map.set(key,setVal.add(value));
       
        return;
    }
    map.get(key).add(value);

}
function setCompanyMapData(){
	for(let key of prodMap.keys()){
		var prodValues=prodMap.get(key);
		for(let prod of prodValues){
			var ppName=prod.recomPpName.toUpperCase();
			var ppPrin=prod.recomPpPrin;
			var ppBasicRef=prod.recomPpBasicRef;
			createMapData(compMap,ppName+'='+ppPrin,prod);
			if(prod.recomPpBasrid == "RIDER" || prod.recomPpBasrid == "R"){
				createMapData(ridMap,ppName+'='+ppPrin+'='+ppBasicRef,prod);
			}
	    }
    }
}



//$('#prodConsDetls').on('shown.bs.modal', function (e) {
//    $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
//});

function setTableData(){

	var i=1;
	var sino=1;
	//Key ->client name ,value->prod dets
	for(let key of prodMap.keys()){
		var prodValues=prodMap.get(key);
		var tblRow;
		var clientName="";
		var comNaMap=new Map();
		if(i!=1){
			$("#prodSummTable").append('<tr class=""><td colspan="13" ></td></tr>');
		}
		
		for(let prodPp of prodValues){
		
			if(!comNaMap.has(prodPp.recomPpPrin))	{
				
				comNaMap.set(prodPp.recomPpPrin,prodPp);
				//Get product dets based on company name
				var compValues=compMap.get(key+'='+prodPp.recomPpPrin);
				
			var riderMap=new Map();	
			var compName="";var lob="";
			for(let prod of compValues){
			
			//Check rider or basic plans
			if(!riderMap.has(prod.recomPpProdname)){
				if(prod.recomPpBasrid == "BASIC" || prod.recomPpBasrid == "B"){
					if(clientName == prod.recomPpName){
					
						tblRow='<tr class="bsc-pln-bg"><td colspan="3" ></td>';
			          }else{
			        	  
			        	  var clntnames = '';
			        	  if(seesselfName.toUpperCase() == prod.recomPpName.toUpperCase()){
			        		  clntnames = prod.recomPpName.toUpperCase() + "<span class='text-primary'> - Client (1)</span>";
			        	  }else if(sessspsname.toUpperCase() == prod.recomPpName.toUpperCase()){
			        		  clntnames = prod.recomPpName.toUpperCase() + "<span class='text-primary'> - Client (2)</span>";
			        	  }else{
			        		  clntnames = prod.recomPpName.toUpperCase() + "<span class='text-primary'> - Dep.</span>";
			        	  }
			        	  
				          tblRow='<tr class="bsc-pln-bg"><td><span class="text-custom-color-gp font-sz-level8"></span></td>'
				          +'<td><span class="text-custom-color-gp font-sz-level8">'+seesselfName.toUpperCase()+'</span></td>'
				          +'<td><span class="text-custom-color-gp font-sz-level8">'+clntnames+'</span></td>';
			           }
                       if(compName == prod.recomPpPrin){
							tblRow='<tr>'+'<td colspan="4"></td>';
						}else{
							tblRow+='<td><span class="text-custom-color-gp font-sz-level8">'+prod.recomPpPrin+'</span></td>'
						}
						
						if(lob == prod.recomPpProdtype){
							tblRow='<tr class="bsc-pln-bg">'+'<td colspan="5" ></td>';
						}else{
							tblRow+='<td><span class="text-custom-color-gp font-sz-level8">'+prod.recomPpProdtype+'</span></td>'
						}
		                tblRow+=getTblRow(prod,'basic'); 
		         		$("#prodSummTable").append(tblRow);
                    clientName=prod.recomPpName.toUpperCase();
                    compName=prod.recomPpPrin;
                    lob=prod.recomPpProdtype;
	           	}  
            var ridvalues=ridMap.get(key+'='+prodPp.recomPpPrin+'='+prod.recomPpProdname);
			if(ridvalues!=undefined){
				for(let ridProd of ridvalues){
				 riderMap.set(ridProd.recomPpProdname,ridProd);
				 tblRow='<tr>'+'<td colspan="4" ></td>';
			     if(lob == ridProd.recomPpProdtype){
							tblRow='<tr>'+'<td colspan="5" ></td>';
						}else{
							tblRow+='<td><span class="text-custom-color-gp font-sz-level8">'+ridProd.recomPpProdtype+'</span></td>'
						}
	             tblRow+=getTblRow(ridProd,'rider'); 
                 $("#prodSummTable").append(tblRow);
                  lob=ridProd.recomPpProdtype;
			   }
			 tblRow='<tr class=""><td colspan="13" ></td></tr>';
		     $("#prodSummTable").append(tblRow);
			compName=prod.recomPpPrin;
			
			}
            }
         
         }
			
			tblRow='<tr class=""><td colspan="13" ></td></tr>';	
				$("#prodSummTable").append(tblRow);
        } 
         
             
    } i++; }  
	
	
	
	// highlight basic Product set in prodRecomm summary Table
	 $('#prodSummTable tbody tr').each(function() {
        var tblBdyRowLen = $(this).children().length;
        for(var i = 0; i < tblBdyRowLen; i++){
       	 var prodType = $(this).children().eq(i).text();
       	   if (prodType == "Basic"){
             	$(this).addClass("bsc-pln-bg")
             }
           }
     });//Function End
     
}

function getTblRow(prod,riderCls){
	
	
	var prodClass="";
	if(prod.recomPpBasrid == 'BASIC' || prod.recomPpBasrid == 'B'){
		prodClass = '<span class="badge badge-pill badge-success font-sz-level8 font-sz-level8" style="cursor: default;">Basic</span>';
	}else if(prod.recomPpBasrid == 'RIDER' || prod.recomPpBasrid == 'R'){
		prodClass = '<span class="badge badge-pill badge-warning amber font-sz-level8 font-sz-level8" style="cursor: default;">Rider</span>';
	}
	
	if(isEmpty(prod.recomPpSumassr)){
		prod.recomPpSumassr = "NIL";
	}else{
		prod.recomPpSumassr = prod.recomPpSumassr;
	}
	
	if(isEmpty(prod.recomPpPaymentmode)){
		prod.recomPpPaymentmode = "NIL";
	}else{
		prod.recomPpPaymentmode = prod.recomPpPaymentmode;
	}
	
	if(isEmpty(prod.recomPpPaymentmode)){
		prod.recomPpPaymentmode = "NIL";
	}else{
		prod.recomPpPaymentmode = prod.recomPpPaymentmode;
	}
	
	
	if(isEmpty(prod.recomPpPlanterm)){
		prod.recomPpPlanterm = "NIL";
	}else{
		prod.recomPpPlanterm = prod.recomPpPlanterm;
	}
	
	if(isEmpty(prod.recomPpPayterm)){
		prod.recomPpPayterm = "NIL";
	}else{
		prod.recomPpPayterm = prod.recomPpPayterm;
	}
	
	if(isEmpty(prod.recomPpPremium)){
		prod.recomPpPremium = "NIL";
	}else{
		prod.recomPpPremium = prod.recomPpPremium;
	}
	
	var prodRiskRateType;
	if(isEmpty(prod.prodRiskRate)){
		prodRiskRateType = "NIL";
	}else{
		
		prodRiskRateType = getProdRiskRate(prod.prodRiskRate);
		
	}
	
	
	
	var tblRow='<td><span class="text-custom-color-gp font-sz-level8">'+prod.recomPpPlan+'</span></td>'         
	              +'<td><span class="text-custom-color-gp font-sz-level8">'+prodClass+'</span></td>'
	              +'<td><span class="text-custom-color-gp font-sz-level8">'+prod.recomPpSumassr+'</span></td>'
	              +'<td><span class="text-custom-color-gp font-sz-level8">'+prod.recomPpPaymentmode+'</span></td>'
				  
		          +'<td><span class="text-custom-color-gp font-sz-level8">'+prod.recomPpPlanterm+'</span></td>'
	              +'<td><span class="text-custom-color-gp font-sz-level8">'+prod.recomPpPayterm+'</span></td>'
	              +'<td><span class="text-custom-color-gp font-sz-level8">'+prod.recomPpPremium+'</span></td>'
	              +'<td><span class="text-custom-color-gp font-sz-level8">'+prodRiskRateType+'</span></td></tr>';
	//recomPpPremium
    return tblRow;
}

function getProdRiskRate(prdRiskRate){
	var PRRTitle;
	if(prdRiskRate == 1){
	      PRRTitle = "Conservative";
		 
	 } else	 if(prdRiskRate == 2){
		 PRRTitle = "Moderatively Conservative";
		
	 }else    if(prdRiskRate == 3){
   	  PRRTitle = "Moderatively Aggressive";
		
	 }else     if(prdRiskRate == 4){
   	  PRRTitle = "Aggressive";
		
     }else {
    	 PRRTitle = "NIL";
     }
     return PRRTitle;
}

function addNewProdNameToCombo(params){
	
	var term = $.trim(params.term);

			if(isEmpty($('#recomPpPrin').val())){return null;}

			var prodcode = makeid(12);
			//var prodcode = term.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,'_');
			                 			 prodcode = prodcode.replace(/\s+/g, '_');
			                 			 prodcode = prodcode.toUpperCase();
		
		    if (term === '') {
		      return null;
		    }
		
		    return {
		      id: prodcode,
		      text: term,
		      newTag: true // add additional parameters
		    }

}

//prod screen obj sec validation start
function validateObjSec(){
	var numberOfChecked = $("#objSec").find('input:checkbox:checked').length;
	 if(numberOfChecked == 0){
		 showObjSecError();
		 return; 
	 }else if(numberOfChecked > 0){
		 hideObjSecError();
	}
	  return true;
 }
 
 function showObjSecError(){
	 $("#objSecError").html("Please Select any of the option in objective Section and then Proceed to add Product Details !");
	 $("#objSec").closest(".card-body").css("border","1px solid #ff0000");
	 $("#objSec").closest(".card-body").css("background","#dea8a82b");
 }
 
 function hideObjSecError(){
	 $("#objSecError").html("");
	 $("#objSec").closest(".card-body").css("border","");
	 $("#objSec").closest(".card-body").css("background","");
 } 
//End validation
 
 
 //consolidated ProdSummary Table Print Funcion
 
 function printProdSumDet(){
	      //poovathi commented this below code
	        //generate FNA()
		    //$("#dynaFrame").get(0).contentWindow.print();
	 
	 Swal.fire({
		  title: 'Are you sure?',
		  text: "want to Print Prodcut Summary Details!",
		  icon: 'info',
 allowOutsideClick:false,
		allowEscapeKey:false,
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, Print it!'
		}).then((result) => {
		  if (result.isConfirmed) {
		    //print function start
			  var divToPrint=document.getElementById("prodSummayTbl");
			  var pageTitle = 'Product Summary Details',
			     stylesheet = 'vendor/bootstrap453/css/bootstrap.css',
			     win = window.open('', 'Print', 'width=1200,height=800');
			     win.document.write('<html><head><title>' + pageTitle + '</title>' +
			     '<link rel="stylesheet" href="' + stylesheet + '">' +
			     '</head><body>' + divToPrint.outerHTML + '</body></html>');
			      win.document.close();
			      win.print();
			      win.close();
			      return false;
			  
			//print function end  
		  }
		})
	}
 
 
 
 //Clear RecommPpName
  function clrRecommPpName(objFld){
	 
	// $("#"+objFld).focus();
	 $("#"+objFld).val(""); 
	 $("#"+objFld).select2({
  	 	 placeholder: "Select a Client Name",
		  width : '100%',
		//  templateResult: loadSelect2ClntNameCombo,
		  allowClear: true,
		  tags: true,
			selectOnClose: true,
			tokenSeparators: [ '\n','\t'],
			createTag: function (params) {
			    var term = $.trim(params.term);
			    if (term === '') {
			      return null;
			    }
			
			    return {
				      id: term.toUpperCase(),
				      text: term.toUpperCase(),
				      newTag: true 
				    }
			  }
		  });
	 
	 $("#"+objFld).trigger("change");
 }
  
  //Clear RecommPpLOB
  function clrProdLob(){
	  $('input[name="recomPpProdtype"]').prop('checked', false);
  }
 
  //not allow to edit client when mode is E
  
  function disableClrClnt(){
	  $("#btnClrCnlt").addClass("disabledMngrSec");
	  $("#btnClrCnlt").css("cursor", "not-allowed");
	  
 }
  
  function enableClrClnt(){
	  $("#btnClrCnlt").removeClass("disabledMngrSec");
	  $("#btnClrCnlt").css("cursor", "pointer");
  }
  
 //premium  and sumAssured number field validation 
  
 /* function isNumber(event,errMsgId){
	    
	    event = (event) ? event : window.event;
	    var charCode = (event.which) ? event.which : event.keyCode;
	    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
	      $("#"+errMsgId).removeClass("hide").addClass("show");
	        return false;
	    }
	    $("#"+errMsgId).removeClass("show").addClass("hide");
	    return true;
		
	}*/
  
  

  $('.numberClass').keyup(function(){
      var val = $(this).val();
      if(isNaN(val)){
           val = val.replace(/[^0-9\.]/g,'');
           if(val.split('.').length>2) 
               val =val.replace(/\.+$/,"");
      }
      $(this).val(val); 
  });
  
  // onchange hide error msg of premium and summ assured.
  $(".numberClass").bind("change",function(){
	  $("#txtFldPremiumError").removeClass("show").addClass("hide");
	  $("#txtFldSAError").removeClass("show").addClass("hide");
	})
	
   //remove prodNotications content in prodRecommPopUp Window
   function clrProdAlertInfoMsg(){
	  $("#alrtInfo").html("");
   }
  
  
  //show primary add btn for products in card header
  function showPrimaryAddProdRecommBtn(){
	  $("#badgeAddProdRecomm").removeClass("hide").addClass("show");
  }
  
  //hide primary add btn for products in card header
  function hidePrimaryAddProdRecommBtn(){
	  $("#badgeAddProdRecomm").removeClass("show").addClass("hide");
  }
  
	  
  
  
  