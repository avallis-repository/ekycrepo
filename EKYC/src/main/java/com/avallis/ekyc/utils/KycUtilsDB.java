package com.avallis.ekyc.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.avallis.ekyc.dbinterfaces.DBImplementation;
import com.avallis.ekyc.dbinterfaces.DBIntf;
import com.avallis.ekyc.dto.AdviserStaff;
import com.avallis.ekyc.dto.AdvstfCodes;
import com.avallis.ekyc.dto.CustomerCoreDetails;
import com.avallis.ekyc.dto.CustomerDetails;
import com.avallis.ekyc.dto.FnaNtucEsubmissions;
import com.avallis.ekyc.dto.FnaNtucPolicyDets;
import com.avallis.ekyc.dto.FpmsDistributor;
import com.avallis.ekyc.dto.MasterProduct;
import com.avallis.ekyc.dto.PolInslifebasic;
import com.avallis.ekyc.dto.Policy;



public class KycUtilsDB {

	static Logger kyclog = Logger.getLogger(KycUtilsDB.class.getName());
	
	
	/**
	 * 
	 * @param dbDTO
	 * @param strAdvstfCode
	 * @param strEsubmitId
	 * @return
	 */

	public List<Object> getESubmitOAuthDets(DBIntf dbDTO,String strAdvstfCode,String strEsubmitId,String strFnaId){
		
		if(KycUtils.checkNullVal(dbDTO)){
			
//			WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext();
//			dbDTO = (DBIntf) ctx.getBean("DBDAO");
		}
		
		List<Object> lstPol= new ArrayList<Object>();
		
		String SQL_QRY="SELECT ACCESS_TOKEN,E_SUMBIT_DT,ESUBID,FNA_ID,CASE_ID,CASE_NO,EXPIRES_SECONDS "
				+ " FROM FNA_NTUC_ESUBMISSIONS"
				+ " WHERE ADVSTF_CODE ='"+strAdvstfCode+"'"
				+ " AND EXTRACT(DAY FROM(CAST(SYSDATE AS TIMESTAMP) - E_SUMBIT_DT)) = (0)"
				+ " AND EXTRACT(HOUR FROM (CAST(SYSDATE as TIMESTAMP)-E_SUMBIT_DT))= (0)";
		
		if(!KycUtils.nullOrBlank(strEsubmitId)){
			SQL_QRY += " and ESUBID='"+strEsubmitId+"'";
		}
		
		if(!KycUtils.nullOrBlank(strFnaId)){
			SQL_QRY += " and FNA_ID='"+strFnaId+"'";
		}
		
		lstPol = (List<Object>) dbDTO.searchByNativeSQLQuery(SQL_QRY); 
		
		return lstPol;
	}
	
	
	/**
	 * 
	 * @param dbDTO
	 * @param esubmission
	 * @return
	 */

	public String insertESubmission(DBIntf dbDTO,FnaNtucEsubmissions esubmission){
		
		
		String esubMaxQry = "select (max(substr(ESUBID,length(ESUBID)-14,15))+1) from FNA_NTUC_ESUBMISSIONS";
		int maxVal = Integer.parseInt(dbDTO.fetchMaxSeqVal(esubMaxQry));	
		String esubId = "ESUB" + KycUtils.lpadString(16, String.valueOf(maxVal));	
		esubmission.setEsubid(esubId);
		
		dbDTO.insertDB(esubmission);
		return esubmission.getEsubid();
			
		
	}
	/**
	 * 
	 * @param dbDTO
	 * @param strCaseId
	 * @param strCaseNo
	 * @param strEsubId
	 * @throws HibernateException
	 */
	 public void updateCaseIdDets(DBIntf dbDTO,String strCaseId,String strCaseNo,String strEsubId)throws HibernateException {
		 
		 String UPDATE_QRY ="UPDATE FNA_NTUC_ESUBMISSIONS SET CASE_ID ='"+strCaseId+"' , CASE_NO =  '"+strCaseNo+"'  WHERE ESUBID='"+strEsubId+"'";
		 dbDTO.updateDbByNativeSQLQuery(UPDATE_QRY);


	 }
	 /**
	  * 
	  * @param dbDTO
	  * @param strCaseId
	  * @param strCasests
	  * @param strFnaId
	  * @throws HibernateException
	  */
	 
	 public void updateFnaDetsWithCaseId(DBIntf dbDTO,String strCaseId,String strCasests,String strFnaId)throws HibernateException {
		 
		 String UPDATE_QRY ="UPDATE FNA_DETAILS SET NTUC_CASE_ID ='"+strCaseId+"' , NTUC_CASE_STATUS =  '"+strCasests+"'  WHERE FNA_ID='"+strFnaId+"'";
		 dbDTO.updateDbByNativeSQLQuery(UPDATE_QRY);


	 }
	 /**
	  * 
	  * @param dbDTO
	  * @param strCaseId
	  * @param strCasests
	  * @throws HibernateException
	  */
	 public void updateFnaCaseStsByCaseId(DBIntf dbDTO,ServletContext context,String strCaseId,String strCasests)throws HibernateException {
		 
		 if(KycUtils.checkNullVal(dbDTO)){
				WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(context);
				dbDTO = (DBIntf) ctx.getBean("DBDAO");
			}
		 
		 String UPDATE_QRY ="UPDATE FNA_DETAILS SET  NTUC_CASE_STATUS =  '"+strCasests+"'  WHERE NTUC_CASE_ID ='"+strCaseId+"'";
		 dbDTO.updateDbByNativeSQLQuery(UPDATE_QRY);


	 }
	 
	 public void updateFnaPolCrtFldByCaseId(DBIntf dbDTO,ServletContext context,String strCaseId,String strPolCrtFlg)throws HibernateException {
		 
		 if(KycUtils.checkNullVal(dbDTO)){
				WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(context);
				dbDTO = (DBIntf) ctx.getBean("DBDAO");
			}
		 
		 String UPDATE_QRY ="UPDATE FNA_DETAILS SET  POLICY_CREATE_FLG =  '"+strPolCrtFlg+"'  WHERE NTUC_CASE_ID ='"+strCaseId+"'";
		 dbDTO.updateDbByNativeSQLQuery(UPDATE_QRY);


	 }
	 
	 
	 /**
	  * 
	  * @param dbDTO
	  * @param samlMap
	  * @param strEsubId
	  * @throws HibernateException
	  */
	 
	 public void updateAvaSAMLRespDets(DBIntf dbDTO,Map<String,String> samlMap,String strEsubId)throws HibernateException {
		 
		 String assrid = KycConst.STR_NULL_STRING,
				 responseid = KycConst.STR_NULL_STRING,
				 notbefore =  KycConst.STR_NULL_STRING,
				 notafter =  KycConst.STR_NULL_STRING,
				 authninstnt =  KycConst.STR_NULL_STRING;
		 
		 if(samlMap.containsKey("RESPONSE_ID"))
			 responseid = samlMap.get("RESPONSE_ID");
		 
		 if(samlMap.containsKey("ASSERTION_ID"))
			 assrid = samlMap.get("ASSERTION_ID");
		 
		 if(samlMap.containsKey("NOT_BEFORE"))
			 notbefore = samlMap.get("NOT_BEFORE");
		 
		 if(samlMap.containsKey("NOT_ON_OR_AFTER"))
			 notafter = samlMap.get("NOT_ON_OR_AFTER");
		 
		 if(samlMap.containsKey("AUTHN_INSTANT"))
			 authninstnt = samlMap.get("AUTHN_INSTANT");
		 
		 String UPDATE_QRY ="UPDATE FNA_NTUC_ESUBMISSIONS SET SAML_RESP_ID ='"+responseid+"',"
		 		+ " SAML_ASSR_ID='"+assrid+"',"
		 		+ " SAML_NOTBEFORE='"+notbefore+"',"
		 		+ " SAML_NOTAFTER='"+notafter+"',"
		 		+ " SAML_AUTHINSTANT='"+authninstnt+"'"
		 		+ " WHERE ESUBID='"+strEsubId+"'";
		 
		 dbDTO.updateDbByNativeSQLQuery(UPDATE_QRY);


	 }
	 
	 /**
	  * 
	  * @param dbDTO
	  * @param samlMap
	  * @param strEsubId
	  * @throws HibernateException
	  */
	 
	 
	 public void updateIDPSAMLRespDets(DBIntf dbDTO,Map<String,String> samlMap,String strEsubId)throws HibernateException {
		 
		 String assrid = KycConst.STR_NULL_STRING,
				 responseid = KycConst.STR_NULL_STRING,
				 notbefore =  KycConst.STR_NULL_STRING,
				 notafter =  KycConst.STR_NULL_STRING,
				 authninstnt =  KycConst.STR_NULL_STRING,
				 inresponseto=KycConst.STR_NULL_STRING,
				 istant=KycConst.STR_NULL_STRING,
				 status = KycConst.STR_NULL_STRING,
				 authnQry = KycConst.STR_NULL_STRING,
				 dsfinstant=KycConst.STR_NULL_STRING;
		 
		 String strAdvStfCode = KycConst.STR_NULL_STRING;
		 if(samlMap.containsKey("RESPONSE_ID"))
			 responseid = samlMap.get("RESPONSE_ID");
		 
		 if(samlMap.containsKey("INRESPONSETO_ID"))
			 inresponseto=samlMap.get("INRESPONSETO_ID");
		 
		 if(samlMap.containsKey("ISSUE_INSTANT"))
			 istant=samlMap.get("ISSUE_INSTANT");
		 
		 if(samlMap.containsKey("ASSERTION_ID"))
			 assrid = samlMap.get("ASSERTION_ID");
		 
		 if(samlMap.containsKey("NOT_BEFORE"))
			 notbefore = samlMap.get("NOT_BEFORE");
		 
		 if(samlMap.containsKey("NOT_ON_OR_AFTER"))
			 notafter = samlMap.get("NOT_ON_OR_AFTER");
		 
		 if(samlMap.containsKey("AUTHN_INSTANT"))
			 authninstnt = samlMap.get("AUTHN_INSTANT");
		 
		 if(samlMap.containsKey("IDPRESP_STATUS"))
			 status = samlMap.get("IDPRESP_STATUS");
		 
		 if(samlMap.containsKey("DSF_AUTHNQRY_ID"))
			 authnQry=samlMap.get("DSF_AUTHNQRY_ID");
		 
		 if(samlMap.containsKey("DSF_ISSUEINSTANT"))
			 dsfinstant=samlMap.get("DSF_ISSUEINSTANT");
		 
		 if(samlMap.containsKey("ADVSTF_CODE"))
			 strAdvStfCode=samlMap.get("ADVSTF_CODE");
		
		 
		 
		 
		 String UPDATE_QRY ="UPDATE FNA_NTUC_ESUBMISSIONS SET IDPRESP_RESP_ID ='"+responseid+"',"
		 		+ " IDPRESP_INRESPONSETO='"+inresponseto+"',"
		 		+ " IDPRESP_INSTANT='"+istant+"',"
		 		+ " IDPRESP_ASSR_ID='"+assrid+"',"
		 		+ " IDPRESP_NOTBEFORE='"+notbefore+"',"
		 		+ " IDPRESP_NOTAFTER='"+notafter+"',"
		 		+ " IDPRESP_AUTHN_INSTANT='"+authninstnt+"',"
		 		+ " IDPRESP_STATUS='"+status+"',"
		 		+ " AUTHNQRY_ID='"+authnQry+"',"
		 		+ " AUTHN_ISSUEINSTANT='"+dsfinstant+"'"
		 		+ " WHERE ADVSTF_CODE='"+strAdvStfCode+"' "
		 		+ " AND ESUBID = '"+strEsubId+"'"
//		 				+ " AND ESUBID = (SELECT MAX(ESUBID) FROM FNA_NTUC_ESUBMISSIONS WHERE ADVSTF_CODE='"+strAdvStfCode+"')"
		 				;
		 
		 dbDTO.updateDbByNativeSQLQuery(UPDATE_QRY);


	 }
	 
	 public List<Object> getOriginalAssertionId(DBIntf dbDTO,String samlAssrId){
			
			if(KycUtils.checkNullVal(dbDTO)){
//				WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(ServletActionContext.getServletContext());
//				dbDTO = (DBIntf) ctx.getBean("DBDAO");
			}
			
			List<Object> lstPol= new ArrayList<Object>();
			
			String SQL_QRY="SELECT ACCESS_TOKEN,E_SUMBIT_DT,ESUBID,FNA_ID,CASE_ID,CASE_NO ,SAML_ASSR_ID,ADVSTF_CODE"
					+ " FROM FNA_NTUC_ESUBMISSIONS "
					+ " WHERE SAML_ASSR_ID='"+samlAssrId+"'"
					
//					+ " WHERE ADVSTF_CODE ='"+strAdvstfCode+"'"
//					+ " AND ESUBID = (SELECT MAX(ESUBID) FROM FNA_NTUC_ESUBMISSIONS WHERE ADVSTF_CODE='"+strAdvstfCode+"')"					
//					+ " AND EXTRACT(DAY FROM(CAST(SYSDATE AS TIMESTAMP) - E_SUMBIT_DT)) = (0)"
//					+ " AND EXTRACT(HOUR FROM (CAST(SYSDATE as TIMESTAMP)-E_SUMBIT_DT))= (0)"
					;
			
			
			
			lstPol = (List<Object>) dbDTO.searchByNativeSQLQuery(SQL_QRY); 
			
			return lstPol;
		}
	 
	 /**
	  * 
	  * @param dbDTO
	  * @return
	  */

	public static String genAutoPolicyNo(DBIntf dbDTO){
		
		String policyno=KycConst.STR_NULL_STRING;
		
		String polNumSeq = "SELECT LTRIM(RTRIM(TO_CHAR(AUTO_POLNO_SEQ.NEXTVAL,'00000000'))) MAXAUTOPOLNO FROM DUAL";
		String maxPolNumVal = dbDTO.fetchMaxSeqVal(polNumSeq);
		policyno = maxPolNumVal;
		
		String DATE_FORMAT = "yyyyMMdd";
		SimpleDateFormat dtFormat = new SimpleDateFormat(DATE_FORMAT);
		Calendar c1 = Calendar.getInstance(); // today
		
		policyno = "FPF" + dtFormat.format(c1.getTime()) + policyno.trim();
		
		return policyno;
		
	}
	
	
	/**
	 * 
	 * @param dbDTO
	 * @param policyObj
	 * @return
	 */
	
	public String insertPolicyDets(DBIntf dbDTO,Policy policyObj){
		
		
	
		String strMaxPolNo = genAutoPolicyNo(dbDTO);
	 
		String maxAppId = KycConst.STR_NULL;
	
		String polSeq = " select max(substr(APPID,length(APPID)-16,17))+1 from POLICY ";
		int maxPolVal =Integer.parseInt(dbDTO.fetchMaxSeqVal(polSeq));
		String assignId = String.valueOf(maxPolVal);
		String qry="select lpad(" + assignId + ",17,0) from dual";
		maxAppId = "APP" + dbDTO.fetchMaxSeqVal(qry);
	
	
		policyObj.setAppid(maxAppId);
		policyObj.setPolicyno(strMaxPolNo);
	
	
		dbDTO.insertDB(policyObj);
	
	
		return maxAppId+"~"+strMaxPolNo;
	}
	/**
	 * 
	 * @param dbDTO
	 * @param polBasicPdtIns
	 * @throws Exception
	 */
	public void insertPolInsLifeBasicDets(DBIntf dbDTO,List<PolInslifebasic> polBasicPdtIns) throws Exception{
		
		try{
		Object policyBasicObj=null;
		 PolInslifebasic polBasicObj = new PolInslifebasic();
	     String BasicMaxQuery ="select (max(substr(PRDLIFEBASIC_ID,length(PRDLIFEBASIC_ID)-10,11))+1) from POL_INSLIFEBASIC";
	     int  maxVal =Integer.parseInt(dbDTO.fetchMaxSeqVal(BasicMaxQuery));
	     Iterator itIns = polBasicPdtIns.iterator();
		   while (itIns.hasNext()) {
			   	  polBasicObj = (PolInslifebasic)itIns.next();
			   	  String qry="select lpad(" + maxVal + ",11,0) from dual";
			      String maxPolBasid="BASI"+dbDTO.createSeqIdVal(qry);		     
			      polBasicObj.setTxtFldPrdlifebasicId(maxPolBasid);
			     maxVal++;
		   } // End While (itIns.hasNext())
		   policyBasicObj=polBasicObj;
		dbDTO.insertTableValues(polBasicPdtIns,policyBasicObj);
	
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("Error in Policy Basic Product Register failed for NTUC-DSF");
		}
		
	}
	/**
	 * 
	 * @param dbDTO
	 * @param strNric
	 * @param strAdviserId
	 * @return
	 * @throws Exception
	 */
	public List<CustomerDetails> getCustomerDetails(DBIntf dbDTO, String strNric,String strAdviserId)throws Exception{

		try{
		List<CustomerDetails> custDetList = new ArrayList<CustomerDetails>();

		if(KycUtils.checkNullVal(dbDTO)){
//			WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(ServletActionContext.getServletContext());
//			dbDTO = (DBIntf) ctx.getBean("DBDAO");
		}

		 custDetList =  (List<CustomerDetails>) dbDTO.searchListByQuery("from com.kyc.Dao.CustomerDetails cust left join fetch cust.adviserStaffByAgentIdCurrent left "
		 		+ "join fetch cust.adviserStaffByAgentIdInitial left join fetch cust.masterCustomerStatus left join fetch cust.fpmsDistributor "
		 		+ " where cust.adviserStaffByAgentIdCurrent='"+strAdviserId+"' and (UPPER(cust.nric)=UPPER('"+strNric+"') or UPPER(cust.custPassportNum)=UPPER('"+strNric+"') or UPPER(cust.custFin)=UPPER('"+strNric+"'))");

		 return custDetList;

		}catch(Exception e){
			e.printStackTrace();
				throw new Exception("Error in getCustomerDetails . Ref. :"+strNric + ", Adviser Id :"+strAdviserId);
			}
	}
	
	/**
	 * 
	 * @param dbDTO
	 * @param custPERSDAO
	 * @return
	 * @throws Exception
	 */
	
	
	public String insertCustomerClient(DBIntf dbDTO,CustomerDetails custPERSDAO) throws Exception{

		Object cusObj = KycConst.STR_NULL;
		String maxCustId = KycConst.STR_NULL_STRING;
		Object cusCoreObj = KycConst.STR_NULL;;
		String maxCoreId = KycConst.STR_NULL_STRING;
		
		if(KycUtils.checkNullVal(dbDTO)){
//			WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(ServletActionContext.getServletContext());
//			dbDTO = (DBIntf) ctx.getBean("DBDAO");
		}
		
		String HQL_CUST_NRIC_QUERY = "from com.kyc.Dao.CustomerCoreDetails core " +
				" where (core.coreCustNric =? or core.coreCustFin =? or core.coreCustPasportNum=?) and core.fpmsDistributor=?";
		


		 
		 CustomerCoreDetails coreDet = new CustomerCoreDetails();
  		 List<CustomerCoreDetails> coreList = new ArrayList<CustomerCoreDetails>();


				String firstName = !(KycUtils.nullOrBlank(custPERSDAO.getCustName()))?custPERSDAO.getCustName():"";
				String finNumber = !(KycUtils.nullOrBlank(custPERSDAO.getCustFin()))?custPERSDAO.getCustFin():"";
				String passport = !(KycUtils.nullOrBlank(custPERSDAO.getCustPassportNum()))?custPERSDAO.getCustPassportNum():"";
				String Nric = !(KycUtils.nullOrBlank(custPERSDAO.getNric()))?custPERSDAO.getNric():"";
				Date DOB = custPERSDAO.getDob();
				String gender = !(KycUtils.nullOrBlank(custPERSDAO.getSex()))?custPERSDAO.getSex():"";
				Date createdDate = custPERSDAO.getCreatedDate();
				String createdBy = !(KycUtils.nullOrBlank(custPERSDAO.getCreatedBy()))?custPERSDAO.getCreatedBy():"";
				String modifiedBy = !(KycUtils.nullOrBlank(custPERSDAO.getModifiedBy()))?custPERSDAO.getModifiedBy():"";
				Date modifiedDate = custPERSDAO.getModifiedDate();
				String disId = !(KycUtils.nullOrBlank(custPERSDAO.getFpmsDistributor().getDistributorId()))?custPERSDAO.getFpmsDistributor().getDistributorId():"";
				String disName = !(KycUtils.nullOrBlank(custPERSDAO.getDistributorName()))?custPERSDAO.getDistributorName():"";

				coreList = (List<CustomerCoreDetails>) dbDTO.searchListByFourParam(HQL_CUST_NRIC_QUERY,Nric.trim(),finNumber.trim(),passport.trim(),disId.trim());

				if(coreList.size()==0){
					coreDet.setCoreCustName(firstName);
					coreDet.setCoreCustFin(finNumber);
					coreDet.setCoreCustPasportNum(passport);
					coreDet.setCoreCustNric(Nric);
					coreDet.setCoreCustDob(DOB);
					coreDet.setCoreCustGender(gender);
					coreDet.setCreatedDate(createdDate);
					coreDet.setCreatedBy(createdBy);
					coreDet.setModifiedBy(modifiedBy);
					coreDet.setModifiedDate(modifiedDate);

					FpmsDistributor fpms = new FpmsDistributor();
					fpms.setDistributorId(disId);
					coreDet.setFpmsDistributor(fpms);
					coreDet.setDistributorName(disName);

					String cusSeq = " select (max(substr(CUST_CORE_ID,length(CUST_CORE_ID)-13,14))+1) from CUSTOMER_CORE_DETAILS";
					int maxCusVal =Integer.parseInt(dbDTO.fetchMaxSeqVal(cusSeq));
					String assignId = String.valueOf(maxCusVal);
					String qry="select lpad(" + assignId + ",14,0) from dual";
					maxCoreId = "CRECUS" + dbDTO.fetchMaxSeqVal(qry);
					coreDet.setCustCoreId(maxCoreId);
					cusCoreObj = coreDet;
					dbDTO.insertDB(cusCoreObj);
				}else{
					maxCoreId = coreList.get(0).getCustCoreId();
				}


			String cusSeq = " select (max(substr(CUSTID,length(CUSTID)-15,16))+1) from CUSTOMER_DETAILS";
			int maxCusVal =Integer.parseInt(dbDTO.fetchMaxSeqVal(cusSeq));
			String assignId = String.valueOf(maxCusVal);
			String qry="select lpad(" + assignId + ",16,0) from dual";
			maxCustId = "CUST" + dbDTO.fetchMaxSeqVal(qry);

			custPERSDAO.setCustid(maxCustId);
			//custPERSDAO.setTxtFldClntCustCoreId(maxCoreId);

			CustomerCoreDetails custCoreDet = new CustomerCoreDetails();
			custCoreDet.setCustCoreId(maxCoreId);
			custPERSDAO.setCustomerCoreDetails(custCoreDet);

			cusObj = custPERSDAO;
			dbDTO.insertDB(cusObj);


		 return maxCustId;
	
	}
	
	/**
	 * 
	 * @param dbDTO
	 * @param strProdCode
	 * @param strPrinId
	 * @return
	 * @throws Exception
	 */
	
	public List<MasterProduct> getMasterProductDets(DBIntf dbDTO,String strProdCode,String strPrinId)throws Exception{
		
		
		if(KycUtils.checkNullVal(dbDTO)){
//			WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(ServletActionContext.getServletContext());
//			dbDTO = (DBIntf) ctx.getBean("DBDAO");
		}

		List<MasterProduct> masterPrdtDetTabElems = new ArrayList<MasterProduct>();

		try {

			masterPrdtDetTabElems = (List<MasterProduct>) dbDTO.searchListByQuery("from com.kyc.Dao.MasterProduct bas left join fetch bas.masterProductLine "
					+ " left join fetch bas.masterPrincipal where bas.prodCode='"+strProdCode+"' and bas.masterPrincipal='"+strPrinId+"'");

			return masterPrdtDetTabElems;

		}catch(Exception e){
			throw new Exception("Product Loading Error");
		}


	}

	/**
	 * 
	 * @param strPrinId
	 * @param strDistrId
	 * @param strAgentCode
	 * @return
	 */
	 public List<AdvstfCodes> getAdviserDetailsBasedOnCodes(String strPrinId,String strDistrId,String strAgentCode) {
			List<AdvstfCodes> advstfList = new ArrayList<AdvstfCodes>();
			try{

				String HQL_GET_ADVSTFDETAILS = " from com.kyc.Dao.AdvstfCodes advstfDet ";
				
				 ApplicationContext appCtx = ApplicationContextUtils.getApplicationContext();
				 DBImplementation dbDTO = (DBImplementation) appCtx.getBean("DBBean");

				String strWhrCls = "where advstfDet.prinAdvId='"+strAgentCode+"' and advstfDet.masterPrincipal ='"+strPrinId+"' and"
						+ " advstfDet.fpmsDistributor='"+strDistrId+"'";
				advstfList = (List<AdvstfCodes>) dbDTO.searchListByQuery(HQL_GET_ADVSTFDETAILS +strWhrCls);

			}catch(HibernateException e){
				kyclog.info("----------> getAdviserDetailsBasedOnCodes " , e);
			}
			return advstfList;
	}
	 /**
	  * 
	  * @param strPrinId
	  * @param strDistrId
	  * @param strAgentId
	  * @return
	  */
	 
	 public List<AdvstfCodes> getAdvStfCodeDetsBasedOnAdvId(String strPrinId,String strDistrId,String strAgentId) {
			List<AdvstfCodes> advstfList = new ArrayList<AdvstfCodes>();
			try{

				String HQL_GET_ADVSTFDETAILS = " from com.kyc.Dao.AdvstfCodes advstfDet ";
				
				 ApplicationContext appCtx = ApplicationContextUtils.getApplicationContext();
				 DBImplementation dbDTO = (DBImplementation) appCtx.getBean("DBBean");

				String strWhrCls = "where advstfDet.adviserStaff='"+strAgentId+"' and advstfDet.masterPrincipal ='"+strPrinId+"' and"
						+ " advstfDet.fpmsDistributor='"+strDistrId+"'";
				advstfList = (List<AdvstfCodes>) dbDTO.searchListByQuery(HQL_GET_ADVSTFDETAILS +strWhrCls);

			}catch(HibernateException e){
				kyclog.info("----------> getAdvStfCodeDetsBasedOnAdvId " , e);
			}
			return advstfList;
	}
	 /**
	  * 
	  * @param dbDTO
	  * @param strCaseId
	  * @return
	  */

	public List<Policy> chkPolicyExistSts(DBIntf dbDTO,String strCaseId){		
		 
		String SQL_QRY="from com.kyc.Dao.Policy pol where pol.ntucCaseId='"+strCaseId+"'";
		
		List<Policy> lstPol=(List<Policy>) dbDTO.searchListByQuery(SQL_QRY); 
		
		return lstPol;
	}
	/**
	 * 
	 * @param dbDTO
	 * @param strAppId
	 * @return
	 */
	
	public List<PolInslifebasic> chkPolicyInsLifeBasExistSts(DBIntf dbDTO,String strAppId){		
		 
		String SQL_QRY="from com.kyc.Dao.PolInslifebasic bas "
				+ " left join fetch bas.selPrdPolicy pol where pol.appid='"+strAppId+"'";
		
		List<PolInslifebasic> lstPol=(List<PolInslifebasic>) dbDTO.searchListByQuery(SQL_QRY); 
		
		return lstPol;
	}
	/**
	 * 
	 * @param dbDTO
	 * @param strAdviserId
	 * @return
	 * @throws Exception
	 */

	public List<AdviserStaff> getAdviserStaffDetails(DBIntf dbDTO,String strAdviserId)throws Exception{

		
		List<AdviserStaff> advStfList = new ArrayList<AdviserStaff>();


		if(KycUtils.checkNullVal(dbDTO)){
			ApplicationContext appCtx = ApplicationContextUtils.getApplicationContext();
			dbDTO = (DBImplementation) appCtx.getBean("DBBean");
		}

		advStfList =  (List<AdviserStaff>) dbDTO.searchListByQuery("from com.kyc.Dao.AdviserStaff advstf where advstf.advstfId='"+strAdviserId+"'");

		return advStfList;

		
	}
	
	/**
	 * 
	 * @param strAdviserId
	 * @return
	 */
	public List getPrxyAdviserStaffDetails(String strAdviserId){
		List prxyAdvStfList = new ArrayList();
		try{

		 ApplicationContext appCtx = ApplicationContextUtils.getApplicationContext();
		 DBImplementation dbDTO = (DBImplementation) appCtx.getBean("DBBean");

		
		 prxyAdvStfList = dbDTO.searchByNativeSQLQuery("SELECT ADVSTF_ID,ADVSTF_NAME,EMAIL_ID from ADVISER_STAFF advstf where advstf.ADVSTF_ID IN "
		 		+ "( SELECT MANAGER_ID FROM ADVSTF_PROXY_MGR_DETS PRXMGR WHERE  ADVSTF_ID ='"+strAdviserId+"'"
		 				+ " AND TO_DATE(PRXMGR.START_DATE,'DD/MM/YYYY')<= TO_DATE(SYSDATE,'DD/MM/YYYY')"
		 				+ " AND TO_DATE(PRXMGR.END_DATE,'DD/MM/YYYY')  >=TO_DATE(SYSDATE,'DD/MM/YYYY')"
		 				+ ")");
		 
		 

		 

		}catch(Exception e){
			e.printStackTrace();
			//throw new Exception(KycConst.WS_SUCCESSMESSAGE);
		}
		return prxyAdvStfList;
	}
	
	/**
	 * 
	 * @param dbDTO
	 * @param context
	 * @param strCaseId
	 * @param strPolCrtFlg
	 * @throws HibernateException
	 */
	
	public void updatePolDetsByCaseId(DBIntf dbDTO,ServletContext context,String strCaseId,String ntucPolicyNo)throws HibernateException {
		 
		 if(KycUtils.checkNullVal(dbDTO)){
				WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(context);
				dbDTO = (DBIntf) ctx.getBean("DBDAO");
			}
		 
		 String UPDATE_QRY ="UPDATE POLICY SET POLICYNO =  '"+ntucPolicyNo+"' WHERE NTUC_CASE_ID ='"+strCaseId+"'";
		 dbDTO.updateDbByNativeSQLQuery(UPDATE_QRY);


	 }
	
	public void insertNTUCDocs(DBIntf dbDTO,List<String> insertQry){
		
		
		for(String query : insertQry){
			kyclog.info("Document insert Query ->"+query);
			dbDTO.updateDbByNativeSQLQuery(query);
		}
		
	}
	
	
	public List<Object> getCaseStatusByCaseId(DBIntf dbDTO,ServletContext context,String strCaseId){
		
		if(KycUtils.checkNullVal(dbDTO)){
			WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(context);
			dbDTO = (DBIntf) ctx.getBean("DBDAO");
		}
		
		List<Object> lstPol= new ArrayList<Object>();
		
		String SQL_QRY="SELECT FNA_ID,NTUC_CASE_ID,NTUC_CASE_STATUS FROM FNA_DETAILS"
				+ " WHERE NTUC_CASE_ID ='"+strCaseId+"'";
		
		lstPol = (List<Object>) dbDTO.searchByNativeSQLQuery(SQL_QRY); 
		
		return lstPol;
	}

	
	public void updateFnaMailSentFlgByCaseId(DBIntf dbDTO,ServletContext context,String strCaseId,String strFnaId,String strMailSentFlg)throws HibernateException {
		 
		 if(KycUtils.checkNullVal(dbDTO)){
				WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(context);
				dbDTO = (DBIntf) ctx.getBean("DBDAO");
			}
		 
		 String UPDATE_QRY ="UPDATE FNA_DETAILS SET  MGR_EMAIL_SENT_FLG =  '"+strMailSentFlg+"'  WHERE NTUC_CASE_ID ='"+strCaseId+"' AND FNA_ID='"+strFnaId+"'";
		 dbDTO.updateDbByNativeSQLQuery(UPDATE_QRY);


	 }
	
	
	public void insertNtucPolDets(DBIntf dbDTO,List<FnaNtucPolicyDets> polBasicPdtIns) throws Exception{
		
		try{
		Object policyBasicObj=null;
		FnaNtucPolicyDets polBasicObj = new FnaNtucPolicyDets();
	     String BasicMaxQuery ="select NVL((max(TO_NUMBER(NP_ID))+1),1)NP_ID from FNA_NTUC_POLICY_DETS";
	     int  maxVal =Integer.parseInt(dbDTO.fetchMaxSeqVal(BasicMaxQuery));
	     Iterator itIns = polBasicPdtIns.iterator();
		   while (itIns.hasNext()) {
			   	  polBasicObj = (FnaNtucPolicyDets)itIns.next();
//			   	  String qry="select lpad(" + maxVal + ",11,0) from dual";
//			      String maxPolBasid="BASI"+dbDTO.createSeqIdVal(qry);		     
			      polBasicObj.setNpId(String.valueOf(maxVal));
			     maxVal++;
		   } // End While (itIns.hasNext())
		   policyBasicObj=polBasicObj;
		dbDTO.insertTableValues(polBasicPdtIns,policyBasicObj);
	
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("Error in insertNtucPolDets for NTUC-DSF");
		}
		
	}
}
