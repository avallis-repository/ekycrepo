package com.avallis.ekyc.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.avallis.ekyc.dto.FnaDetails;
import com.avallis.ekyc.dto.FnaExpenditureDets;
import com.avallis.ekyc.dto.FnaOtherassetDets;
import com.avallis.ekyc.dto.FnaSelfspouseDets;
import com.avallis.ekyc.services.FNAService;
import com.avallis.ekyc.services.FpmsService;
import com.avallis.ekyc.services.KycService;

public class KycUtils {

	public static ResourceBundle resource = ResourceBundle.getBundle("AppResource");

	static Logger kyclog = Logger.getLogger(KycUtils.class.getName());
	private static SimpleDateFormat standardDateFormat = new SimpleDateFormat("dd/MM/yyyy");
	private static SimpleDateFormat stdDateXMLFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//	private static SimpleDateFormat targetXMLFormat = new SimpleDateFormat("yyyy-MM-ddTHH:mm:ss");
	private static DateFormat stdDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
	private static SimpleDateFormat standardDateTimeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
	private static SimpleDateFormat standardDateTimeFormatFile = new SimpleDateFormat("ddMMyyyy_hh_mm_ss");
	private static SimpleDateFormat fileNameFormat = new SimpleDateFormat("ddMMyyyyhhmmssS");
	
	private static SimpleDateFormat dbFormat = new SimpleDateFormat("yyyy-MM-dd");

	
	public static String convertDateToDateDateString(Date date) {
		return (checkNullVal(date) ? KycConst.STR_NULL : standardDateFormat.format(date));
	}
	
	public static String convertDateToDate(Date dateStr) throws ParseException {
		return (!checkNullVal(dateStr) ? stdDateFormat.format(dateStr)
				: "");
	}
	
	public static Date convertDateStringToDate(String dateStr) {
		try {
			// return standardDateFormat.parse(dateStr);
			return (nullOrBlank(dateStr) ? null : standardDateFormat
					.parse(dateStr));
		} catch (ParseException pe) {
//			pe.printStackTrace();
			kyclog.error("error in convertDateStringToDate :",pe);
			return null;
		}
	}
	
	public static Date convertStringToDate(String dateStr) {
//		SimpleDateFormat originalFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		Date date = null;
		try {
			date = standardDateFormat.parse(dateStr);

		} catch (ParseException ex) {
//			ex.printStackTrace();
			kyclog.error("error in convertStringToDate :",ex);
		}
		return date;
	}
	
	public static String formatFileDate(Date date) {
		String formatdate = "";

		try {
			if (date != null) {

				formatdate = standardDateTimeFormatFile.format(date);
			} else
				formatdate = null;

		} catch (Exception ex) {
//			ex.printStackTrace();
			kyclog.error("error in formatFileDate :",ex);
		}

		return formatdate;
	}

	public static String toXMLGregorianCalendar(Date date) {

		try {
			return (checkNullVal(date) ? null : stdDateXMLFormat.format(date));
		} catch (Exception ex) {
//			ex.printStackTrace();
			kyclog.error("error in toXMLGregorianCalendar :",ex);
		}
		return null;
	}

	public static String convertDateToDateString(Date date) {
		return (checkNullVal(date) ? KycConst.STR_NULL : standardDateFormat
				.format(date));
	}
	
	public static String formatDate(Date date) {
		String formatdate = "";

		try {
			if (date != null) {

				
				formatdate = standardDateFormat.format(date);
			} else
				formatdate = null;

		} catch (Exception ex) {
//			ex.printStackTrace();
			kyclog.error("error in formatDate :",ex);
		}

		return formatdate;
	}

	public static String formatDateString(String date, String flag) {

		
		String formatdate = KycConst.STR_NULL_STRING;

		try {

			Date dateobj = date != null ? dbFormat.parse(date) : null;

			if (dateobj != null) {
				if (flag.equalsIgnoreCase("Q")) {
//					SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
					formatdate = standardDateFormat.format(dateobj);
				} else if (flag.equalsIgnoreCase("I")) {
//					SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
					formatdate = stdDateFormat.format(date);

				}
			}

		} catch (Exception ex) {
//			ex.printStackTrace();
			kyclog.error("error in formatDateString :",ex);
		}

		return formatdate;
	}

	public static String convertDateToDateTimeString(Date date) {
		return (checkNullVal(date) ? KycConst.STR_NULL_STRING : standardDateTimeFormat.format(date));
	}

	
	public static String convertObjToString(Object pObj) {
		if (pObj == null) {
			return KycConst.STR_NULL_STRING;
		} else {
			return (pObj.toString());
		}
	} // end convertObjToString
	
	public static boolean checkNullVal(Object obj) {
		if (obj == null) {
			return true;
		} else {
			return false;
		}
	} // end checkNullVal
	
	public static boolean nullOrBlank(String str) {
		return ((str == null) || (str.length() == 0));
	}

	public static boolean nullObj(Object obj) {
		return ((obj == null));
	}

	public static double convertDouble(String strVal) {

		if (!nullOrBlank(strVal) && !nullObj(strVal))
			return Double.parseDouble(strVal);
		else
			return Double.parseDouble("0");
	}

	public static String getObjValue(Object obj) {

		String retStr = "";
		if (obj != null)
			// System.out.println("@ object value -->"+obj.getClass().getName());

			if (obj != null && obj.getClass().equals(Date.class)) {
				// System.out.println("inside date cond>>>>>>>>>");
				Date dte = (Date) obj;
				// retStr = formatDate(dte);
				// retStr = convertDateToDateString(dte);
				retStr = convertObjToString(convertDateToDateDateString(dte));
			} else if (obj != null
					&& obj.getClass().equals(java.util.Date.class)) {

				java.util.Date sqlDte = (java.util.Date) obj;
				Date dte = sqlDte;
				// retStr = formatDate(dte);
				// retStr = convertDateToDateString(dte);
				retStr = convertObjToString(convertDateToDateDateString(dte));
			} else if (obj != null
					&& obj.getClass().equals(java.sql.Date.class)) {
				java.sql.Date sqlDte = (java.sql.Date) obj;
				Date dte = sqlDte;
				retStr = convertObjToString(convertDateToDateDateString(dte));
			} else if (obj != null
					&& obj.getClass().equals(java.lang.Double.class)) {

				String pattern = "######00.###";
				DecimalFormat decimalFormat = new DecimalFormat(pattern);
				// NumberFormat format = NumberFormat.getInstance();
				retStr = decimalFormat.format(Double.parseDouble(obj.toString()));

			} else {
				retStr = !KycUtils.nullObj(obj) ? obj.toString() : "";
			}

		return retStr;
	}

	public static String[] splitSplChars(String str, String splChar) {

		StringTokenizer strtoken = new StringTokenizer(str, splChar);
		String[] strArr = new String[strtoken.countTokens()];

		try {
			int count = 0;
			while (strtoken.hasMoreTokens()) {
				strArr[count] = strtoken.nextToken();
				count++;
			}
		} catch (Exception ex) {
			kyclog.error(" splitSplChars-->" , ex);
		}
		return strArr;
	}

	public static Date formatStringToDate(String dateStr) {

//		SimpleDateFormat stdFormat = new SimpleDateFormat("dd/MM/yyyy");

		try {
			// return standardDateFormat.parse(dateStr);
			return (nullOrBlank(dateStr) ? null : standardDateFormat.parse(dateStr));
		} catch (ParseException pe) {
//			pe.printStackTrace();
			kyclog.error("error formatStringToDate-->" , pe);
			return null;
		}

	}

	public static Map<String, Object> getRequestMapping(HttpServletRequest request) {

		Map<String, Object> objMapping = new HashMap<String, Object>();

		FnaDetails fnaDet = new FnaDetails();
		FnaDetails fnaDetDTO = new FnaDetails();

		FnaSelfspouseDets fnaSSDet = new FnaSelfspouseDets();
		FnaSelfspouseDets fnaSSDetDTO = new FnaSelfspouseDets();

		FnaExpenditureDets fnaExpDet = new FnaExpenditureDets();
		FnaExpenditureDets fnaExpDetDTO = new FnaExpenditureDets();

		FnaOtherassetDets fnaOtherAsset = new FnaOtherassetDets();
		FnaOtherassetDets fnaOtherAssetDTO = new FnaOtherassetDets();

		Enumeration paramNames = request.getParameterNames();
		boolean exp = false, othasset = false;

		while (paramNames.hasMoreElements()) {

			Object obj = paramNames.nextElement();
			String paramName = obj.toString();

			try {

				BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
				beanUtilsBean.getConvertUtils().register(new org.apache.commons.beanutils.converters.DoubleConverter(null), Double.class);

				beanUtilsBean
						.getConvertUtils()
						.register(
								new org.apache.commons.beanutils.converters.DoubleConverter(
										null), Double.TYPE);

				beanUtilsBean
						.getConvertUtils()
						.register(
								new org.apache.commons.beanutils.converters.IntegerConverter(
										null), Integer.class);
				beanUtilsBean
						.getConvertUtils()
						.register(
								new org.apache.commons.beanutils.converters.IntegerConverter(
										null), Integer.TYPE);

				beanUtilsBean
						.getConvertUtils()
						.register(
								new org.apache.commons.beanutils.converters.SqlDateConverter(
										null), java.util.Date.class);

				// both
				if (PropertyUtils.isReadable(fnaDet, paramName)
						&& PropertyUtils.isWriteable(fnaDet, paramName)) {
					// BeanUtilsBean beanUtilsBean =
					// BeanUtilsBean.getInstance();
					beanUtilsBean.setProperty(fnaDet, paramName,
							request.getParameter(paramName));
				}
				// both
				if (PropertyUtils.isReadable(fnaSSDet, paramName)
						&& PropertyUtils.isWriteable(fnaSSDet, paramName)) {
					// BeanUtilsBean beanUtilsBean =
					// BeanUtilsBean.getInstance();
					beanUtilsBean.setProperty(fnaSSDet, paramName,
							request.getParameter(paramName));
				}
				// both
				if (PropertyUtils.isReadable(fnaExpDet, paramName)
						&& PropertyUtils.isWriteable(fnaExpDet, paramName)) {
					exp = true;

					beanUtilsBean.setProperty(fnaExpDet, paramName,
							request.getParameter(paramName));
				}
				
				if (PropertyUtils.isReadable(fnaOtherAsset, paramName)
						&& PropertyUtils.isWriteable(fnaOtherAsset, paramName)) {
					if (!KycUtils.nullObj(request.getParameter(paramName))
							&& !KycUtils.nullOrBlank(request.getParameter(
									paramName).toString()))
						othasset = true;
					beanUtilsBean.setProperty(fnaOtherAsset, paramName,
							request.getParameter(paramName));
				}


			} catch (Exception e) {
				kyclog.error("-------> while setting prop " + paramName);
			}
		}

		try {

			BeanUtils.copyProperties(fnaDetDTO, fnaDet);
			objMapping.put("FNA_DETAILS", fnaDetDTO);

			BeanUtils.copyProperties(fnaSSDetDTO, fnaSSDet);
			objMapping.put("FNA_SELFSPSDET", fnaSSDetDTO);

			if (exp) {
				BeanUtils.copyProperties(fnaExpDetDTO, fnaExpDet);
				objMapping.put("FNA_EXPENDITURE", fnaExpDetDTO);
			}

			if (othasset) {
				BeanUtils.copyProperties(fnaOtherAssetDTO, fnaOtherAsset);
				objMapping.put("FNA_OTHASSETDET", fnaOtherAssetDTO);
			}

		} catch (Exception e) {
			kyclog.error(" ---------> while copying prop  bean : ");
		}

		return objMapping;
	}

	public static String lpadString(int zerocnt, String num) {

		if (nullObj(num))
			num = "1";
		return org.apache.commons.lang.StringUtils.leftPad(num, zerocnt, '0');
	}



	public static String replaceSplChars(String regexChars, String formattedStr) {

		try {

			String[] strArrRegexChars = regexChars.split(",");
			int totChars = strArrRegexChars.length;

			for (int sp = 0; sp < totChars; sp++) {

				String strEachKey = strArrRegexChars[sp];

				String[] splCharKeyVal = strEachKey.split("=");
				// kyclog.info("Before replace-->"+formattedStr);

				if (formattedStr.toUpperCase().contains(
						splCharKeyVal[0].toUpperCase())) {
					formattedStr = formattedStr.replace(splCharKeyVal[0],
							splCharKeyVal[1].toUpperCase());
				}
				// kyclog.info("After replace-->"+formattedStr);

			}

		} catch (Exception ex) {
			// ex.printStackTrace();
			kyclog.error("Error in Utility replaceSplChars---> " + ex);
		}

		return formattedStr;
	}
	
	

	public static File createPhysicalDir(String strScreen, String strAdvName,
			String strClntName, String strDestFileName) {

		StringBuffer mainDirPath = new StringBuffer();
		String strDestFileNameWithPath = "";// FPMSConstants.STR_NULL_STRING;
		String strSplChars = "";// FPMSConstants.STR_NULL_STRING;
		File destfile = null;

		try {

			ResourceBundle resource = ResourceBundle.getBundle("AppResource");


			mainDirPath.append(resource.getString("attach.bfile.desitMachine"));
			if (!(KycUtils.nullOrBlank(resource
					.getString("attach.bfile.desitMachine.drive")))) {
				mainDirPath
						.append(File.separator
								+ resource
										.getString("attach.bfile.desitMachine.drive"));
			}
			mainDirPath
					.append(File.separator
							+ resource
									.getString("attach.bfile.desitMachine.drive.folder"));
			strSplChars = resource.getString("attach.bfile.SplCharVal");

			if (strScreen.equalsIgnoreCase(KycConst.GLBL_MODULE_CLNT)) {
				mainDirPath.append(File.separator
						+ resource.getString("attach.bfile.attachscreen.clnt"));
			}
			if (strScreen.equalsIgnoreCase(KycConst.GLBL_MODULE_NTUC)) {
				mainDirPath.append(File.separator
						+ resource.getString("attach.bfile.attachscreen.ntuc"));
			}

			if (!KycUtils.nullOrBlank(strAdvName)) {
				mainDirPath.append(File.separator
						+ replaceSplChars(strSplChars, strAdvName));
			}
			if (!KycUtils.nullOrBlank(strClntName)) {
				mainDirPath.append(File.separator
						+ replaceSplChars(strSplChars, strClntName));
			}

			if (!KycUtils.nullOrBlank(strDestFileName)) {
				strDestFileNameWithPath = mainDirPath + File.separator
						+ strDestFileName;
			} else {
				strDestFileNameWithPath = mainDirPath.toString();
			}
             

			destfile = new File(strDestFileNameWithPath);
			

		} catch (Exception ex) {
			kyclog.error("Exception in creating Desitnation file, -->" + ex);
		}
		return destfile;

	}
	
	
	

	public static Map sortByValue(Map map) {
		List list = new LinkedList(map.entrySet());
		Collections.sort(list, new Comparator() {
			public int compare(Object o1, Object o2) {
				return ((Comparable) ((Map.Entry) (o1)).getValue())
						.compareTo(((Map.Entry) (o2)).getValue());
			}
		});

		Map result = new LinkedHashMap();
		for (Iterator it = list.iterator(); it.hasNext();) {
			Map.Entry entry = (Map.Entry) it.next();
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}

	public static boolean isValidUSession(HttpServletRequest requestObj) {

		HttpSession sessObj = requestObj.getSession(false);
		if (nullObj(sessObj)) {
			kyclog.info("---------->SESSSION HAS BEEN EXPIRED");
			return false;
		} else {
			return true;
		}
	} // end isValidUser()
	
	public static boolean chkValidDB(){
		FpmsService fpmsser = new FpmsService();
		return fpmsser.chkValidDB();
	}
	

	public static String getCommonSessValue(HttpServletRequest req,
			String sessKey) {

		String strSessValue = "";

		Map<String,String> sessMap = new HashMap<String,String>();	
		try {

			HttpSession session = req.getSession();
			
			sessMap = (HashMap<String, String>)session.getAttribute(KycConst.LOGGED_USER_INFO);
			 

			strSessValue = sessMap.get(sessKey);

		} catch (Exception ex) {
			kyclog.error("Error in Utility getCommonSessValue : ---->" + ex.getMessage());
		}
		return strSessValue;
	}

	public static String getCommonSessValueWithSess(HttpSession session,
			String sessKey) {

		String strSessValue = "";
		Map<String,String> sessMap = new HashMap<String,String>();	

		try {

			sessMap = (HashMap<String, String>)session.getAttribute(KycConst.LOGGED_USER_INFO);
			sessMap = (HashMap<String, String>)session.getAttribute(KycConst.LOGGED_USER_INFO);

		} catch (Exception ex) {
			kyclog.error("Error in utility getCommonSessValueWithSess: ---->" + ex.getMessage());
		}
		return strSessValue;
	}

	public static String getBFILEDirPath(String strModule) {

		StringBuffer dirPath = new StringBuffer();

		try {


			dirPath.append(resource.getString("attach.bfile.desitMachine")
					.toString() + File.separator);

			if (!KycUtils.nullOrBlank(resource
					.getString("attach.bfile.desitMachine.drive"))) {
				dirPath.append(resource.getString(
						"attach.bfile.desitMachine.drive").toString()
						+ File.separator);
			}

			dirPath.append(resource.getString(
					"attach.bfile.desitMachine.drive.folder").toString()
					+ File.separator);

			if (strModule.equalsIgnoreCase(KycConst.GLBL_MODULE_CLNT)) {
				dirPath.append(resource.getString(
						"attach.bfile.attachscreen.clnt").toString()
						+ File.separator);
			}

			if (strModule.equalsIgnoreCase(KycConst.GLBL_MODULE_NTUC)) {
				dirPath.append(resource.getString(
						"attach.bfile.attachscreen.ntuc").toString()
						+ File.separator);
			}

		} catch (Exception ex) {
			kyclog.error("error in getBFILEDirPath-->" + ex);
		}

		return dirPath.toString();

	}

	



	

	public static String removeCommas(String strInput) {
		String strRetStr = "0";
		for (int m = 0; m < strInput.length(); m++) {
			String strIndStr = strInput.substring(m, m + 1);
			if (strIndStr == "," || strIndStr.equals(",")) {
				strIndStr = "";
			}
			strRetStr = strRetStr + strIndStr;
		}
		return strRetStr;
	}



	public static JSONArray getManagerFnaDetails(String strLogUserAdvId,	String strFnaId) {
		JSONArray jsnArr = new JSONArray();

		List lstFnaDets = new ArrayList();

		FNAService serv = new FNAService();
		JSONObject fnaJsnObj = new JSONObject();
		JSONObject jsnObj = new JSONObject();
		JSONArray fnaJsnArrObj = new JSONArray();
		try {

			lstFnaDets = serv.getManagerFnaDetails(null, strLogUserAdvId,
					strFnaId);

			if (lstFnaDets.size() > 0) {

				fnaJsnArrObj = getUnApprovalColumns(lstFnaDets);

				jsnObj.put("MANAGER_FNA_DETAILS", fnaJsnArrObj);
				jsnArr.put(jsnObj);

			} else {

				fnaJsnObj.put("NO_RECORDS_FOUND",
						"No records found for manager to approve/reject");
				fnaJsnArrObj.put(fnaJsnObj);
				jsnObj.put("MANAGER_NO_RECORDS_FNA_DETAILS", fnaJsnArrObj);
				
				
				jsnArr.put(jsnObj);
			}

		} catch (Exception e) {
//			e.printStackTrace();
			kyclog.error("error in getManagerFnaDetails-->" , e);
		}

		return jsnArr;
	}

	public static JSONArray getAdminFnaDetails(String strFnaId) {
		JSONArray jsnArr = new JSONArray();

		List lstFnaDets = new ArrayList();

		FNAService serv = new FNAService();
		JSONObject fnaJsnObj = new JSONObject();
		JSONObject jsnObj = new JSONObject();
		JSONArray fnaJsnArrObj = new JSONArray();
		try {

			lstFnaDets = serv.getAdminFnaDetails(null, strFnaId);
			if (lstFnaDets.size() > 0) {

				fnaJsnArrObj = getUnApprovalColumns(lstFnaDets);

				jsnObj.put("ADMIN_FNA_DETAILS", fnaJsnArrObj);
				jsnArr.put(jsnObj);

			} else {

				fnaJsnObj.put("NO_RECORDS_FOUND",
						"No records found for admin to approve/reject");
				fnaJsnArrObj.put(fnaJsnObj);
				jsnObj.put("ADMIN_NO_RECORDS_FNA_DETAILS", fnaJsnArrObj);
				
				
				jsnArr.put(jsnObj);
			}

		} catch (Exception e) {
//			e.printStackTrace();
			kyclog.error("error in getAdminFnaDetails-->" , e);
		}

		return jsnArr;
	}

	public static JSONArray getComplianceFnaDetails(String strFnaId) {
		JSONArray jsnArr = new JSONArray();

		List lstFnaDets = new ArrayList();

		FNAService serv = new FNAService();
		JSONObject fnaJsnObj = new JSONObject();
		JSONObject jsnObj = new JSONObject();
		JSONArray fnaJsnArrObj = new JSONArray();
		try {

			lstFnaDets = serv.getComplianceFnaDetails(null, strFnaId);
			if (lstFnaDets.size() > 0) {

				fnaJsnArrObj = getUnApprovalColumns(lstFnaDets);

				jsnObj.put("COMPLIANCE_FNA_DETAILS", fnaJsnArrObj);
				jsnArr.put(jsnObj);

			} else {

				fnaJsnObj.put("NO_RECORDS_FOUND","No records found for compliance staff to approve/reject");
				
				fnaJsnArrObj.put(fnaJsnObj);
				
				jsnObj.put("COMPLIANCE_NO_RECORDS_FNA_DETAILS", fnaJsnArrObj);
				jsnArr.put(jsnObj);
			}

		} catch (Exception e) {
//			e.printStackTrace();
			kyclog.error("error in getComplianceFnaDetails-->" , e);
		}

		return jsnArr;
	}

	public static JSONArray getCompAndAdminFnaDets(String strFnaId) {
		JSONArray jsnArr = new JSONArray();

		List lstFnaDets = new ArrayList();

		FNAService serv = new FNAService();
		JSONObject fnaJsnObj = new JSONObject();
		JSONObject jsnObj = new JSONObject();
		JSONArray fnaJsnArrObj = new JSONArray();
		try {

			lstFnaDets = serv.getCompAndAdminFnaDets(null, strFnaId);
			if (lstFnaDets.size() > 0) {

				fnaJsnArrObj = getUnApprovalColumns(lstFnaDets);

				jsnObj.put("COMPLIANCE_FNA_DETAILS", fnaJsnArrObj);
				jsnArr.put(jsnObj);

			} else {

				fnaJsnObj.put("NO_RECORDS_FOUND","No records found for compliance staff to approve/reject");
				fnaJsnArrObj.put(fnaJsnObj);
				
				jsnObj.put("COMPLIANCE_NO_RECORDS_FNA_DETAILS", fnaJsnArrObj);
				jsnArr.put(jsnObj);
			}

		} catch (Exception e) {
//			e.printStackTrace();
			kyclog.error("error in getCompAndAdminFnaDets-->" , e);
		}

		return jsnArr;
	}

	public static JSONArray getUnApprovalColumns(List lstFnaDets) {

		
		JSONObject jsnObj = new JSONObject();
		JSONArray fnaJsnArrObj = new JSONArray();

		try {

			if (lstFnaDets.size() > 0) {

				Iterator it = lstFnaDets.iterator();

				while (it.hasNext()) {

					Object[] row = (Object[]) it.next();
					JSONObject fnaJsnObj = new JSONObject();

					fnaJsnObj
							.put("txtFldFnaId",
									checkNullVal(row[0]) ? KycConst.STR_NULL_STRING
											: row[0]);

					fnaJsnObj
							.put("txtFldCustId",
									checkNullVal(row[1]) ? KycConst.STR_NULL_STRING
											: row[1]);
					fnaJsnObj.put("txtFldCustName", checkNullVal(row[2]) ? KycConst.STR_NULL_STRING
							: row[2]);

					fnaJsnObj.put("txtFldAdvStfId", checkNullVal(row[3]) ? KycConst.STR_NULL_STRING
							: row[3]);
					fnaJsnObj.put("txtFldAdvStfName", checkNullVal(row[4]) ? KycConst.STR_NULL_STRING
							: row[4]);
					fnaJsnObj.put("txtFldAdvStfEmailId", checkNullVal(row[5]) ? KycConst.STR_NULL_STRING
							: row[5]);

					fnaJsnObj
							.put("txtFldMgrId",
									checkNullVal(row[6]) ? KycConst.STR_NULL_STRING
											: row[6]);
					fnaJsnObj.put("txtFldMgrApproveStatus", checkNullVal(row[7]) ? KycConst.STR_NULL_STRING
							: row[7]);
					fnaJsnObj
							.put("txtFldMgrApproveDate",
									checkNullVal(row[8]) ? KycConst.STR_NULL_STRING
											: convertObjToString(convertDateToDateDateString((Date) row[8])));
					fnaJsnObj.put("txtFldMgrApproveRemarks", checkNullVal(row[9]) ? KycConst.STR_NULL_STRING
							: row[9]);

					fnaJsnObj.put("txtFldAdminApproveStatus", checkNullVal(row[10]) ? KycConst.STR_NULL_STRING
							: row[10]);
					fnaJsnObj
							.put("txtFldAdminApproveDate",
									checkNullVal(row[11]) ? KycConst.STR_NULL_STRING
											: convertObjToString(convertDateToDateDateString((Date) row[11])));
					fnaJsnObj.put("txtFldAdminApproveRemarks", checkNullVal(row[12]) ? KycConst.STR_NULL_STRING
							: row[12]);

					fnaJsnObj.put("txtFldCompApproveStatus", checkNullVal(row[13]) ? KycConst.STR_NULL_STRING
							: row[13]);
					fnaJsnObj
							.put("txtFldCompApproveDate",
									checkNullVal(row[14]) ? KycConst.STR_NULL_STRING
											: convertObjToString(convertDateToDateDateString((Date) row[14])));
					fnaJsnObj.put("txtFldCompApproveRemarks", checkNullVal(row[15]) ? KycConst.STR_NULL_STRING
							: row[15]);

					fnaJsnObj.put("txtFldNTUCPolId", checkNullVal(row[16]) ? KycConst.STR_NULL_STRING
							: row[16]);
					fnaJsnObj.put("txtFldNTUCPolNum", checkNullVal(row[17]) ? KycConst.STR_NULL_STRING
							: row[17]);

					fnaJsnObj.put("txtFldMgrName", checkNullVal(row[18]) ? KycConst.STR_NULL_STRING
							: row[18]);
					fnaJsnObj
							.put("mgrName",
									checkNullVal(row[18]) ? KycConst.STR_NULL_STRING
											: row[18]);

					fnaJsnObj.put("txtFldMgrEmailId", checkNullVal(row[19]) ? KycConst.STR_NULL_STRING
							: row[19]);

					fnaJsnObj.put("txtFldMgrStsApprBy", checkNullVal(row[20]) ? KycConst.STR_NULL_STRING
							: row[20]);
					fnaJsnObj.put("txtFldAdminStsApprBy", 
							checkNullVal(row[21]) ? KycConst.STR_NULL_STRING
							: row[21]);
					fnaJsnObj.put("txtFldCompStsApprBy", checkNullVal(row[22]) ? KycConst.STR_NULL_STRING
							: row[22]);

					fnaJsnObj.put("txtFldCustIcDets", checkNullVal(row[23]) ? KycConst.STR_NULL_STRING
							: row[23]);
					fnaJsnObj.put("txtFldCustFnaIc", checkNullVal(row[24]) ? KycConst.STR_NULL_STRING
							: row[24]);

					fnaJsnObj.put("txtFldFnaNtucCaseId",checkNullVal(row[25]) ? KycConst.STR_NULL_STRING
							: row[25]);
					fnaJsnObj.put("txtFldFnaNtucCasests", checkNullVal(row[26]) ? KycConst.STR_NULL_STRING
							: row[26]);
					fnaJsnObj.put("txtFldFnaPolCrtFlg",checkNullVal(row[27]) ? KycConst.STR_NULL_STRING
							: row[27]);

					fnaJsnObj.put("txtFldAdvStfCode", checkNullVal(row[28]) ? KycConst.STR_NULL_STRING
							: row[28]);

					fnaJsnObj.put("txtFldFnaPrin", checkNullVal(row[29]) ? KycConst.STR_NULL_STRING
							: row[29]);
					fnaJsnObj.put("txtFldFnaClientSign", checkNullVal(row[30]) ? KycConst.STR_NULL_STRING
							: row[30]);
					fnaJsnObj.put("txtFldFnaAdviserSign", checkNullVal(row[31]) ? KycConst.STR_NULL_STRING
							: row[31]);
					fnaJsnObj.put("txtFldFnaManagerSign", checkNullVal(row[32]) ? KycConst.STR_NULL_STRING
							: row[32]);

					fnaJsnArrObj.put(fnaJsnObj);
				}
			}

		} catch (Exception ex) {
//			ex.printStackTrace();
			kyclog.error("error in getUnApprovalColumns-->" , ex);
		}

		return fnaJsnArrObj;

	}


	

	/**
	 * This method used to Read an Input Stream returns into a byte array
	 * 
	 * @param inputStream
	 *            FileInputStream
	 * @return content byte of given given Input Stream
	 */
	public static byte[] fpmsReadInputStream(FileInputStream inputStream)
			throws IOException {
		int bufSize = 1024 * 1024;
		byte[] content;

		List<byte[]> parts = new LinkedList<byte[]>();
		InputStream in = new BufferedInputStream(inputStream);

		byte[] readBuffer = new byte[bufSize];
		byte[] part = null;
		int bytesRead = 0;

		// read everyting into a list of byte arrays
		while ((bytesRead = in.read(readBuffer, 0, bufSize)) != -1) {
			part = new byte[bytesRead];
			System.arraycopy(readBuffer, 0, part, 0, bytesRead);
			parts.add(part);
		} // End of while(bytesRead)

		// calculate the total size
		int totalSize = 0;
		for (byte[] partBuffer : parts) {
			totalSize += partBuffer.length;
		}

		// allocate the array
		content = new byte[totalSize];
		int offset = 0;
		for (byte[] partBuffer : parts) {
			System.arraycopy(partBuffer, 0, content, offset, partBuffer.length);
			offset += partBuffer.length;
		}// End of for(partBuffer)

		return content;
	} // End of fpmsReadInputStream()

	public static String getFileExtension(File file) {
		String fileName = file.getName();
		if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
			return fileName.substring(fileName.lastIndexOf(".") + 1);
		else
			return "";
	}

	public static String formatFileName(String strAtmtFileName) {
		String strFormatFileName = KycConst.STR_NULL_STRING;
		try {
			int lastIndex = strAtmtFileName.lastIndexOf(".");
			String strprefix = strAtmtFileName.substring(0, lastIndex);
			String strsuffix = strAtmtFileName.substring(lastIndex);

			

			strprefix = strprefix + "_" + fileNameFormat.format(new Date());

			strFormatFileName = strprefix + strsuffix;
		} catch (Exception ex) {
			kyclog.error("Exception in formatFileName -------------->" + ex);

		}
		return strFormatFileName;
	}

	public static String createPhysicalFile(File attachSrcFile, File destBFile) {

		File destfile = destBFile;
		String strReturnMsg = KycConst.STR_NULL_STRING;

		try {
			// System.out.print("File Exist--->"+destfile.exists());

			if (!destfile.getParentFile().exists()) {
				destfile.getParentFile().mkdirs();
			}

			FileInputStream srcStream = new FileInputStream(attachSrcFile);
			FileOutputStream destStream = new FileOutputStream(destfile);

			int len = (int) attachSrcFile.length();
			byte[] buffer = new byte[len];
			// kyclog.info("Source File Len-->"+len);

			int length;
			while ((length = srcStream.read(buffer)) > 0) {
				destStream.write(buffer, 0, length);
			}

			// kyclog.info("Desitnation File Exist -->"+destfile.exists());
			// kyclog.info("Desitnation File Len-->"+(int)(long)destfile.length());

			destfile = null;

			srcStream.close();
			destStream.close();

			kyclog.info("File is copied successful  ");

		} catch (Exception ex) {

			kyclog.error("Error in Utility fpmsCreatePhysicalFile Method-->"
					,ex);
			strReturnMsg = "Exception in file creation";
		}

		return strReturnMsg;

	}
	
	public static String createPhysicalFile(byte[] attachSrcFile, File destBFile) {

		File destfile = destBFile;
		String strReturnMsg = KycConst.STR_NULL_STRING;

		try {
			// System.out.print("File Exist--->"+destfile.exists());

			if (!destfile.getParentFile().exists()) {
				destfile.getParentFile().mkdirs();
			}

//			FileInputStream srcStream = new FileInputStream(attachSrcFile);
			FileOutputStream destStream = new FileOutputStream(destfile);

//			int len =   attachSrcFile.length;
//			byte[] buffer = new byte[len];
			// kyclog.info("Source File Len-->"+len);

//			int length;
//			while ((length = srcStream.read(buffer)) > 0) {
//				destStream.write(buffer, 0, length);
//			}
			destStream.write(attachSrcFile);

			// kyclog.info("Desitnation File Exist -->"+destfile.exists());
			// kyclog.info("Desitnation File Len-->"+(int)(long)destfile.length());

			destfile = null;

//			srcStream.close();
			destStream.close();

//			System.out.println("File is copied successful  ");

		} catch (Exception ex) {

			// ex.printStackTrace();
			// String strExcep = ExceptionMsgLibrary.throwExcep(ex);
			kyclog.error("Error in Utility fpmsCreatePhysicalFile Method-->", ex);
			strReturnMsg = "Exception in file creation";
		}

		return strReturnMsg;

	}
	/**
	 * 
	 * @param base64String
	 * @param strFilePath
	 * @throws IOException
	 */
	public static void convertBase64ToPDF(String base64String,
			String strFilePath) throws IOException {

		FileOutputStream fos = new FileOutputStream(strFilePath);
		fos.write(com.sun.jersey.core.util.Base64.decode(base64String));
		fos.close();

	}

	public static String toCamelCase(String args) {
		String result = Character.toUpperCase(args.charAt(0))
				+ (args.substring(1).toLowerCase());
		return result;

		// return result.toString();

	}

	 public static String getParamValue(HttpServletRequest request,String param){
		 	
			return ((request.getParameter(param) != KycConst.STR_NULL_STRING) ? 
					request.getParameter(param) : KycConst.STR_NULL_STRING );
			
			
		}//end of getParamValue
		
		public static double getParamdoubleValue(HttpServletRequest request,String strVal) {
			if (!nullOrBlank(strVal) && !nullObj(strVal))
				return Double.parseDouble(strVal);
			else
				return Double.parseDouble("0");
		}
		

		public static<K> Map<K,Integer> incrementValue(Map<K,Integer> map, K key){
			Integer count = map.get(key);
			
			if (count == null) {
				map.put(key, 1);
			}
			else {
				map.put(key, count + 1);
			}
			
			return map;
		}

	public static String convertToBase64(File file) {

		String base64Code = "";

		try {

			byte[] filedata = null;

			FileInputStream fin = new FileInputStream(file);

			filedata = fpmsReadInputStream(fin);

			// base64Code = Base64.encodeBase64String(filedata);
			base64Code = new String(
					com.sun.jersey.core.util.Base64.encode(filedata));

		} catch (Exception e) {
//			e.printStackTrace();
			kyclog.error("Error in Utility convertToBase64 Method-->", e);
		}

		return base64Code;
	}

	

	public static String sendMailToItTeam(String strMailFor, String strCaseID) throws UnsupportedEncodingException {

		String mailStatus = "";

		String[] toReceipent = new String[1];
		String[] toReceipentCC = new String[1];

		toReceipent[0] = resource.getString("appln.it.admin.To").trim();
		toReceipentCC[0] = resource.getString("appln.it.admin.cc").trim();

		String ItEmailContent = KycConst.STR_NULL_STRING, ItEmailSubject = KycConst.STR_NULL_STRING, strSub = KycConst.STR_NULL_STRING;

		if (strMailFor.equalsIgnoreCase("SFTP")) {
			ItEmailContent = resource.getString("appln.it.admin.content.sftp")
					.trim();
			ItEmailSubject = resource.getString("appln.it.admin.subject.sftp")
					.trim();
			strSub = ItEmailSubject.replace("~CASEID~", strCaseID);
		}

		if (strMailFor.equalsIgnoreCase("POLICYNO")) {
			ItEmailContent = resource.getString(
					"appln.it.admin.content.policyno").trim();
			ItEmailSubject = resource.getString(
					"appln.it.admin.subject.policyno").trim();
			strSub = ItEmailSubject.replace("~CASEID~", strCaseID);
		}

		mailStatus = KycMail.sendMail("", toReceipent, toReceipentCC,ItEmailContent, strSub);

		return mailStatus;
	}

	public static void sendNricAttachmentMail(HttpSession sess,
			String strClntId, String strFNAID, String strCustomerName) {
	}


	




public static JSONArray getAdvtoMngrKycSendList(String strLogUserAdvId) {
	
    
	JSONArray jsnArr = new JSONArray();

	List lstFnaDets = new ArrayList();

	FNAService serv = new FNAService();
	JSONObject fnaJsnObj = new JSONObject();
	JSONObject jsnObj = new JSONObject();
	JSONArray fnaJsnArrObj = new JSONArray();
	try {

		lstFnaDets = serv.getAdvtoMngrKycSendList(null, strLogUserAdvId);

		if (lstFnaDets.size() > 0) {
			

			fnaJsnArrObj = getUnApprovalColumns(lstFnaDets);

			jsnObj.put("MANAGER_FNA_DETAILS", fnaJsnArrObj);
			jsnArr.put(jsnObj);

		} else {

			fnaJsnObj.put("NO_RECORDS_FOUND","No records found for manager to approve/reject");
			fnaJsnArrObj.put(fnaJsnObj);
			jsnObj.put("MANAGER_NO_RECORDS_FNA_DETAILS", fnaJsnArrObj);
			jsnObj.put("MANAGER_FNA_DETAILS", fnaJsnArrObj);
			jsnArr.put(jsnObj);
		}

	} catch (Exception e) {
//		e.printStackTrace();
		kyclog.error("Error in Utility getAdvtoMngrKycSendList -->", e);
	}

	return jsnArr;
}
        //poovathi add common fun to decrypt all url param values on 29-06-2021	
	public String decodeURLParam(String urlEncParam) {
	    String UrlDecParam = new String(DatatypeConverter.parseBase64Binary(urlEncParam));
	    return UrlDecParam;
	}
	
	public String encodeURLParam(String urlParam) throws UnsupportedEncodingException {
	    String urlEcParam = DatatypeConverter.printBase64Binary(urlParam.getBytes("UTF-8"));
	    return urlEcParam;
	}
	
	
}
