package com.avallis.ekyc.utils;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import javax.activation.MimetypesFileTypeMap;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;




import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import jcifs.smb.SmbFileOutputStream;
import jcifs.smb.SmbRandomAccessFile;
import net.lingala.zip4j.model.FileHeader;

public class Utility {
	private static SimpleDateFormat standardDateFormat = new SimpleDateFormat("dd/MM/yyyy");
	private static SimpleDateFormat standardDateTimeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
//	static DateFormat stdDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private static String OS = System.getProperty("os.name").toLowerCase();

	public static void copyFileUsingJcifs(final String userName, final String password, final String sourcePath,
			final String destinationPath) throws IOException {

		final String user = userName + ":" + password;
		final NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(user);
		final SmbFile sFile = new SmbFile(destinationPath, auth);
		final SmbFileOutputStream smbFileOutputStream = new SmbFileOutputStream(sFile);
		final FileInputStream fileInputStream = new FileInputStream(new File(sourcePath));

		final byte[] buf = new byte[16 * 1024 * 1024];
		int len;
		while ((len = fileInputStream.read(buf)) > 0) {
			smbFileOutputStream.write(buf, 0, len);
		}
		fileInputStream.close();
		smbFileOutputStream.close();
	}

	public static boolean isWindows() {

		return (OS.indexOf("win") >= 0);

	}

	public static boolean isUnix() {

		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);

	}

	public static File copyFile(final byte[] buf, final String destinationPath) throws  IOException {

		File fileObj = new File(destinationPath);
		if (!fileObj.exists()) {
			fileObj.createNewFile();
		}
		
		//Commented for Password Protection
		/*final FileOutputStream fileOutputStream = new FileOutputStream(destinationPath);
		fileOutputStream.write(buf);
		fileOutputStream.close();*/
		
		FileUtils.writeByteArrayToFile(fileObj, buf);

		return fileObj;
	}

	public static String identifyFileTypeUsingMimetypesFileTypeMap(final String fileName) {
		final MimetypesFileTypeMap fileTypeMap = new MimetypesFileTypeMap();
		return fileTypeMap.getContentType(fileName);
	}

	public static List<File> readZipFiles(String destinationPath, String Filename, String ext, String adviserName,
			String customerName) throws Exception {

		ResourceBundle resource = ResourceBundle.getBundle("AppResource");
		String destmachine = resource.getString("attach.bfile.desitMachine").trim();

		List<File> listFileObj = new ArrayList<File>();
		try {

			ZipInputStream zipinputstream = null;
			ZipEntry zipentry;
			zipinputstream = new ZipInputStream(new FileInputStream(destinationPath));

			zipentry = zipinputstream.getNextEntry();
			File filedir = new File(
					destmachine + File.separator + resource.getString("attach.bfile.desitMachine.drive.folder")
							+ File.separator + resource.getString("attach.bfile.attachscreen.policy") + File.separator
							+ adviserName + File.separator + customerName + File.separator + Filename);
			if (!filedir.exists()) {
				filedir.mkdirs();
			}
			while (zipentry != null) {
				// for each entry to be extracted
				String entryName = zipentry.getName();
				File filedirObj = null;

				String strFolder = destmachine + File.separator
						+ resource.getString("attach.bfile.desitMachine.drive.folder") + File.separator
						+ resource.getString("attach.bfile.attachscreen.policy") + File.separator + adviserName
						+ File.separator + customerName + File.separator + Filename.trim();

				if (zipentry.isDirectory()) {
					// filedirObj = new
					// File(strFolder+File.separator+entryName);
					filedirObj = new File(strFolder + "/" + entryName);

					if (!filedirObj.exists()) {
						filedirObj.mkdirs();
					}
				} else {
					// filedirObj = new
					// File(strFolder+File.separator+entryName);
					entryName = entryName.replace("/", "");
					filedirObj = new File(strFolder + File.separator + entryName);

					if (!filedirObj.exists()) {
						filedirObj.createNewFile();
					}

					/*
					 * rf = new RandomAccessFile(fileObj, "r"); String line;
					 * 
					 * byte[] document = new byte[(int) rf.length()];
					 * rf.readFully(document);
					 */
					byte[] buf = new byte[1024];
					final FileOutputStream fileOutputStream = new FileOutputStream(filedirObj);

					int length;
					while ((length = zipinputstream.read(buf, 0, buf.length)) >= 0) {
						fileOutputStream.write(buf, 0, length);
					}

					fileOutputStream.close();

					listFileObj.add(filedirObj);

				}

				zipinputstream.closeEntry();
				zipentry = zipinputstream.getNextEntry();
			}

			zipinputstream.close();
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}

		return listFileObj;
	}

	public static List<SmbFile> readLinuxZipFiles(String strPath, String filename, String ext, String adviserName,
			String customerName) throws Exception {

		ResourceBundle resource = ResourceBundle.getBundle("AppResource");
		final NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication("WORKGROUP",
				resource.getString("userid"), resource.getString("password"));
		String destmachine = resource.getString("attach.bfile.linux.destMachine").trim();

		List<SmbFile> listFileObj = new ArrayList<SmbFile>();
		try {

			ZipInputStream zipinputstream = null;
			ZipEntry zipentry;

			SmbFile srcDir = new SmbFile(strPath + File.separator + filename.trim() + "." + ext, auth);
			zipinputstream = new ZipInputStream(new SmbFileInputStream(srcDir));

			zipentry = zipinputstream.getNextEntry();

			SmbFile dirObj = new SmbFile(
					destmachine + File.separator + resource.getString("attach.bfile.desitMachine.drive.folder")
							+ File.separator + resource.getString("attach.bfile.attachscreen.policy") + File.separator
							+ adviserName + File.separator + customerName + File.separator + filename.trim(),
					auth);

			if (!dirObj.exists()) {
				dirObj.mkdirs();
			}

			while (zipentry != null) {
				// for each entry to be extracted
				String entryName = zipentry.getName();

				SmbFile dirFileObj = null;
				String strFolder = destmachine + File.separator
						+ resource.getString("attach.bfile.desitMachine.drive.folder") + File.separator
						+ resource.getString("attach.bfile.attachscreen.policy") + File.separator + adviserName
						+ File.separator + customerName + File.separator + filename.trim();

				if (zipentry.isDirectory()) {
					dirFileObj = new SmbFile(strFolder + File.separator + entryName, auth);
					strFolder = strFolder + File.separator + entryName;

					if (!dirFileObj.exists()) {
						dirFileObj.mkdirs();
					}
				} else {

					dirFileObj = new SmbFile(strFolder + File.separator + entryName, auth);
					if (!dirFileObj.exists()) {
						dirFileObj.createNewFile();
					}

					byte[] buf = new byte[1024];

					final SmbFileOutputStream fileOutputStream = new SmbFileOutputStream(dirFileObj);
					int length;
					while ((length = zipinputstream.read(buf, 0, buf.length)) >= 0) {
						fileOutputStream.write(buf, 0, length);
					}

					fileOutputStream.close();

					listFileObj.add(dirFileObj);
					// rf.close();
				}

				zipinputstream.closeEntry();
				zipentry = zipinputstream.getNextEntry();

			} // while

			zipinputstream.close();
		} catch (Exception e) {
			throw new Exception("File Not Found in Specified Path");
		}

		return listFileObj;
	}

	public static List<String> getFilePath(String destinationPath, String Filename, String ext) {

		ResourceBundle resource = ResourceBundle.getBundle("AppResource");
		String destmachine = resource.getString("attach.bfile.desitMachine").trim();
		String filePath = "";
		List<String> listStringObj = new ArrayList<String>();
		try {
			byte[] buf = new byte[1024];
			ZipInputStream zipinputstream = null;
			ZipEntry zipentry;
			zipinputstream = new ZipInputStream(
					new FileInputStream(destinationPath + File.separator + Filename + "." + ext));

			zipentry = zipinputstream.getNextEntry();
			String path = "";

			while (zipentry != null) {
				// for each entry to be extracted
				String entryName = zipentry.getName();

				if (zipentry.isDirectory()) {
					path = path + Filename;
					path = path + File.separator + entryName;
					
				} else {
					filePath = "";
					filePath = Filename + File.separator + entryName;
					listStringObj.add(filePath);
				}

				zipinputstream.closeEntry();
				zipentry = zipinputstream.getNextEntry();
			}

			zipinputstream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return listStringObj;
	}

	public static boolean nullOrBlank(String str) {
		return ((str == null) || (str.length() == 0));
	}

	public static Date convertDateStringToDate(String dateStr) {
		try {
			// return standardDateFormat.parse(dateStr);
			return (nullOrBlank(dateStr) ? null : standardDateFormat.parse(dateStr));
		} catch (ParseException pe) {
			pe.printStackTrace();
			return null;
		}
	}

	public static String convertDateToDateString(Date date) {
		return (checkNullVal(date) ? KycConst.STR_NULL : standardDateFormat.format(date));
	}
	
	public static String convertDateToDateDateString(Date date) {
		return (checkNullVal(date) ? KycConst.STR_NULL : standardDateFormat.format(date));
	}

	public static boolean checkNullVal(Object obj) {
		if (obj == null) {
			return true;
		} else {
			return false;
		}
	} // end checkNullVal

	public static String formatDateString(String date, String flag) {

		SimpleDateFormat dbFormat = new SimpleDateFormat("yyyy-MM-dd");

		String formatdate = "";

		try {

			Date dateobj = date != null ? dbFormat.parse(date) : null;

			if (dateobj != null) {
				if (flag.equalsIgnoreCase("Q")) {
					SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
					formatdate = format.format(dateobj);
				} else if (flag.equalsIgnoreCase("I")) {
					SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
					formatdate = format.format(date);

				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return formatdate;
	}

	static boolean isValidforLinux(final String file) {
		ZipFile zipfile = null;
		try {
			zipfile = new ZipFile(file);
			return true;
		} catch (IOException e) {
			return false;
		} finally {
			try {
				if (zipfile != null) {
					zipfile.close();
					zipfile = null;
				}
			} catch (IOException e) {
			}
		}
	}

	public static boolean isValidforWindows(final File file) {
		ZipFile zipfile = null;
		try {
			zipfile = new ZipFile(file);
			return true;
		} catch (IOException e) {
			return false;
		} finally {
			try {
				if (zipfile != null) {
					zipfile.close();
					zipfile = null;
				}
			} catch (IOException e) {
			}
		}
	}

	public static List<SmbFile> readZipPasswordProtectFileLinux(String strPath, String filename, String ext) throws Exception {
		Map<String, byte[]> inMemoryFiles = new LinkedHashMap<String, byte[]>();

		ResourceBundle resource = ResourceBundle.getBundle("AppResource");
		final NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication("WORKGROUP",
				resource.getString("userid"), resource.getString("password"));
		String destmachine = resource.getString("attach.bfile.linux.destMachine").trim();
		String password = resource.getString("attach.import.zipfile.password");

		List<SmbFile> listFileObj = new ArrayList<SmbFile>();
		try {
			SmbFile smbFile = new SmbFile(strPath + filename.trim() + "." + ext, auth);

			InputStream smbis = new SmbFileInputStream(smbFile);
			byte[] fileBytes = IOUtils.toByteArray(smbis);
		 
			File fileObj = new File(strPath + filename.trim() + "." + ext);
			FileUtils.writeByteArrayToFile(fileObj, fileBytes);
			
			
			net.lingala.zip4j.core.ZipFile zipFile = new net.lingala.zip4j.core.ZipFile(fileObj);
			if (zipFile.isEncrypted()) {
				zipFile.setPassword(password);
			}

			ArrayList fileHeaderList = (ArrayList) zipFile.getFileHeaders();
			for (int i = 0; i < fileHeaderList.size(); i++) {
				FileHeader fileHeader = (FileHeader) fileHeaderList.get(i);
				if (fileHeader.isDirectory()) {
					byte[] uncompressedBytes = new byte[4096];
					inMemoryFiles.put(fileHeader.getFileName() + "^" + fileHeader.isDirectory(), uncompressedBytes);
				}
			}

			for (int i = 0; i < fileHeaderList.size(); i++) {
				FileHeader fileHeader = (FileHeader) fileHeaderList.get(i);
				if (!fileHeader.isDirectory()) {
					net.lingala.zip4j.io.ZipInputStream is = zipFile.getInputStream(fileHeader);
					int uncompressedSize = (int) fileHeader.getUncompressedSize();
					OutputStream os = new ByteArrayOutputStream(uncompressedSize);
					int bytesRead;
					byte[] buffer = new byte[4096];
					while ((bytesRead = is.read(buffer)) != -1) {
						os.write(buffer, 0, bytesRead);
					}
					byte[] uncompressedBytes = ((ByteArrayOutputStream) os).toByteArray();
					inMemoryFiles.put(fileHeader.getFileName() + "^" + fileHeader.isDirectory(), uncompressedBytes);
					is.close();
				}
			}
			
			String strFileFolder = "";
			
			
				strFileFolder = destmachine + File.separator
						+ resource.getString("attach.bfile.desitMachine.drive.folder") + File.separator
						+ resource.getString("attach.bfile.attachscreen.policy") + File.separator + filename.trim();
			 
			
			
			
			
			SmbFile fil = new SmbFile(strFileFolder, auth);
			if (!fil.exists()) {
				fil.mkdirs();
				
			}
			
			for (String fileNameDirFile : inMemoryFiles.keySet()) {
				StringTokenizer strToken = new StringTokenizer(fileNameDirFile, "^");

				String fileName = strToken.nextToken().toString();
				// boolean dirFlg = false;
				
				boolean dirFlg = strToken.nextToken().equalsIgnoreCase("true") ? true : false;
				
				byte[] buffer = inMemoryFiles.get(fileNameDirFile);

				String strFolder="";
				
			 
					strFolder = destmachine + File.separator
							+ resource.getString("attach.bfile.desitMachine.drive.folder") + File.separator
							+ resource.getString("attach.bfile.attachscreen.policy") + File.separator  + filename.trim();
			 
				
				
				fil = new SmbFile(strFolder + File.separator, auth);
				
				String foldername = "";
				
				
				
				if (dirFlg) {
					foldername = fileName;
					
					fil = new SmbFile(strFolder + File.separator+foldername, auth);
					if (!fil.exists()) {
						fil.mkdirs();
						
					}
				} else {
					fileName = fileName.replaceAll(foldername, "");
					
					if(fileName.trim().equalsIgnoreCase("KYC.pdf")){
						String fileNameStr = fileName;
						strFolder = strFolder.replaceAll(resource.getString("attach.bfile.attachscreen.policy"),resource.getString("attach.bfile.attachscreen.clnt"));
						SmbFile filObj = new SmbFile(strFolder + File.separator, auth);
						if (!filObj.exists()) {
							filObj.mkdirs();
							
						}
					}else{
						strFolder = strFolder.replaceAll(resource.getString("attach.bfile.attachscreen.clnt"),resource.getString("attach.bfile.attachscreen.policy"));
					}
					 
					fil = new SmbFile(strFolder + File.separator+fileName, auth);
					if (!fil.exists()) {
						fil.createNewFile();
					}

					final SmbFileOutputStream fileOutputStream = new SmbFileOutputStream(fil);
					fileOutputStream.write(buffer);
					fileOutputStream.close();
					listFileObj.add(fil);

				}

			}

			return listFileObj;

		} catch (Exception e) {
			e.printStackTrace();
			 throw new Exception(KycConst.WS_SUCCESSMESSAGE);
		}
 

	}
	
	public static List<File> readZipPasswordProtectFileWindows(String strPath, String filename, String ext
			) throws Exception {
		Map<String, byte[]> inMemoryFiles = new LinkedHashMap<String, byte[]>();

		ResourceBundle resource = ResourceBundle.getBundle("AppResource");
		String destmachine = resource.getString("attach.bfile.desitMachine").trim();
		String password = resource.getString("attach.import.zipfile.password");

		List<File> listFileObj = new ArrayList<File>();
		try {
			
			File fileObj = new File(strPath);			
			
			net.lingala.zip4j.core.ZipFile zipFile = new net.lingala.zip4j.core.ZipFile(fileObj);
			if (zipFile.isEncrypted()) {
				zipFile.setPassword(password);
			}

			ArrayList fileHeaderList = (ArrayList) zipFile.getFileHeaders();
			for (int i = 0; i < fileHeaderList.size(); i++) {
				FileHeader fileHeader = (FileHeader) fileHeaderList.get(i);
				if (fileHeader.isDirectory()) {
					byte[] uncompressedBytes = new byte[4096];
					inMemoryFiles.put(fileHeader.getFileName() + "^" + fileHeader.isDirectory(), uncompressedBytes);
				}
			}

			for (int i = 0; i < fileHeaderList.size(); i++) {
				FileHeader fileHeader = (FileHeader) fileHeaderList.get(i);
				if (!fileHeader.isDirectory()) {
					net.lingala.zip4j.io.ZipInputStream is = zipFile.getInputStream(fileHeader);
					int uncompressedSize = (int) fileHeader.getUncompressedSize();
					OutputStream os = new ByteArrayOutputStream(uncompressedSize);
					int bytesRead;
					byte[] buffer = new byte[4096];
					while ((bytesRead = is.read(buffer)) != -1) {
						os.write(buffer, 0, bytesRead);
					}
					byte[] uncompressedBytes = ((ByteArrayOutputStream) os).toByteArray();
					inMemoryFiles.put(fileHeader.getFileName() + "^" + fileHeader.isDirectory(), uncompressedBytes);
					is.close();
				}
			}
			
			String strFileFolder = destmachine + File.separator
					+ resource.getString("attach.bfile.desitMachine.drive.folder") + File.separator
					+ resource.getString("attach.bfile.attachscreen.policy") + File.separator + filename.trim();
			File fil = new File(strFileFolder);
			if (!fil.exists()) {
				fil.mkdirs();
				
			}
			
			for (String fileNameDirFile : inMemoryFiles.keySet()) {
				StringTokenizer strToken = new StringTokenizer(fileNameDirFile, "^");

				String fileName = strToken.nextToken().toString();
				// boolean dirFlg = false;
				boolean dirFlg = strToken.nextToken().equalsIgnoreCase("true") ? true : false;

				byte[] buffer = inMemoryFiles.get(fileNameDirFile);

				String strFolder = destmachine + File.separator
						+ resource.getString("attach.bfile.desitMachine.drive.folder") + File.separator
						+ resource.getString("attach.bfile.attachscreen.policy") + File.separator  + filename.trim();
				
				fil = new File(strFolder+File.separator);
				
				String foldername = "";
				if (dirFlg) {
					foldername = fileName;
					fil = new File(strFolder + File.separator+foldername);
					if (!fil.exists()) {
						fil.mkdirs();
						
					}
				} else {
					fileName = fileName.replaceAll(foldername, "");
					
					
					if(fileName.trim().equalsIgnoreCase("KYC.pdf")){
						strFolder = strFolder.replaceAll(resource.getString("attach.bfile.attachscreen.policy"),resource.getString("attach.bfile.attachscreen.clnt"));
						File filObj = new File(strFolder + File.separator);
						if (!filObj.exists()) {
							filObj.mkdirs();
							
						}
					}else{
						strFolder = strFolder.replaceAll(resource.getString("attach.bfile.attachscreen.clnt"),resource.getString("attach.bfile.attachscreen.policy"));
					}
					fil = new File(strFolder + File.separator+fileName);
					if (!fil.exists()) {
						fil.createNewFile();
					}

					final FileOutputStream fileOutputStream = new FileOutputStream(fil);
					fileOutputStream.write(buffer);
					fileOutputStream.close();
					listFileObj.add(fil);

				}

			}

			return listFileObj;

		} catch (Exception e) {
			e.printStackTrace();
			 throw new Exception(KycConst.WS_SUCCESSMESSAGE);
		}

		 

	}

	public static void closeFileHandlers(net.lingala.zip4j.io.ZipInputStream is, OutputStream os) throws IOException {
		// Close output stream
		if (os != null) {
			os.close();
			os = null;
		}
	}

	//Another Utility

	
 
	 
	  
	  public static String replaceSplChars(String regexChars,String formattedStr){
		  
		  try{
			  
			  String[] strArrRegexChars = regexChars.split(",");
			  int totChars = strArrRegexChars.length;
				
			  for(int sp=0;sp<totChars;sp++){	
					
				  String strEachKey = strArrRegexChars[sp];
				  	
				  String[] splCharKeyVal =strEachKey.split("=") ;	
				  //fplog.info("Before replace-->"+formattedStr);
					
				  if(formattedStr.toUpperCase().contains(splCharKeyVal[0].toUpperCase())){
					  formattedStr = formattedStr.replace(splCharKeyVal[0], splCharKeyVal[1].toUpperCase());
				  }						
				  //fplog.info("After replace-->"+formattedStr);
					
				}
			  
		  }catch(Exception ex){
			  ex.printStackTrace();
			  //fplog.info("Error in Utility replaceSplChars---> "+ex);
		  }
			
			
			return formattedStr;
		}
	  
		public static String getFileExtension (String item) {
	        String formatName = "";
	        String fileName = item;
	        if (fileName != null && fileName.length() >0) {
	            int index = -1;
	             for (int i = fileName.length()-1 ; i >=0; i --) {
	                 if (fileName.charAt(i)=='.') {
	                     index = i;
	                     break;
	                 }
	             }
	            if (index >=0 && index < fileName.length() -1) {
	                 formatName = fileName.substring(index+1);
	            }
	        }
	        return formatName ;
	    }
		
		
		//Another Utility
		
		public static void copySmallArraysToBigArray(final byte[][] smallArrays,
			    final byte[] bigArray){
			    int currentOffset = 0;
			    for(final byte[] currentArray : smallArrays){
			        System.arraycopy(
			            currentArray, 0,
			            bigArray, currentOffset,
			            currentArray.length
			        );
			        currentOffset += currentArray.length;
			    }
			}
		

		public static boolean isNumeric(String str)
		{
		  NumberFormat formatter = NumberFormat.getInstance();
		  ParsePosition pos = new ParsePosition(0);
		  formatter.parse(str, pos);
		  return str.length() == pos.getIndex();
		}
		 public static String convertObjToString(Object pObj) {
		        if (pObj == null) {
		            return KycConst.STR_NULL_STRING;
		        } else {
		            return (String)(pObj.toString());
		        }
		    }


public static String convertDateToDateTimeString(Date date) {
    return (checkNullVal(date) ? KycConst.STR_NULL_STRING : 
            standardDateTimeFormat.format(date));
}

public static String encode(String url)  
{  
          try {  
               String encodeURL=URLEncoder.encode( url, "UTF-8" );  
               return encodeURL;  
          } catch (UnsupportedEncodingException e) {  
               return "Issue while encoding" +e.getMessage();  
          }  
}  
}