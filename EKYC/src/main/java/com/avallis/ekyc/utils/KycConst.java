package com.avallis.ekyc.utils;

public class KycConst {
	
	public static final String DB_BEAN= "dbImplBean";
	
	public final static String LOGGED_USER_DISTRIBUTOR = "LOGGED_USER_DISTRIBUTOR";
	public final static String LOGGED_USER_DISTNAME="LOGGED_USER_DISTNAME";
	public final static String LOGGED_USER_DISTMNEMONIC="LOGGED_USER_DISTMNEMONIC";
	public final static String LOGGED_USER_ID = "LOGGED_USER_ID";
	public final static String LOGGED_USER_ADVSTFID = "LOGGED_USER_ADVSTFID";
	public final static String LOGGED_USER_MGRADVSTFID = "LOGGED_USER_MGRADVSTFID";
	public final static String LOGGED_USER_MGRADVSTFNAME="LOGGED_USER_MGRADVSTFNAME";
	public final static String LOGGED_USER_ADVSTFNAME="LOGGED_USER_ADVSTFNAME";
	public final static String FNA_ADVSTFNAME="FNA_ADVSTFNAME";
	public final static String LOGGED_USER_ADVSTF_MGRID = "LOGGED_USER_ADVSTF_MGRID";
	public final static String LOGGED_USER_ADVSTF_MGRNAME = "LOGGED_USER_ADVSTF_MGRNAME";
	public final static String FNA_ADVSTF_MGRNAME = "FNA_ADVSTF_MGRNAME";
	public final static String LOGGED_USER_ROLEID="LOGGED_USER_ROLEID";
	public final static String LOGGED_USER_ROLENAME="LOGGED_USER_ROLENAME";
	public final static String LOGGED_USER_COMPACS="LOGGED_USER_COMPACS";
	public final static String LOGGED_USER_MGRFLG="LOGGED_USER_MGRFLG";
	public final static String LOGGED_USER_STFTYPE="LOGGED_USER_STFTYPE";
	public final static String LOGGED_USER_STFTYPE_MNEM="LOGGED_USER_STFTYPE_MNEM";
	public final static String LOGGED_USER_INFO="LOGGED_USER_INFO";
	public final static String LOGGED_USER_ADVSTFINITIAL="LOGGED_USER_ADVSTFINITIAL";
	public final static String LOGGED_USER_MGRFLAG = "LOGGED_USER_MGRFLAG"; 
	public final static String LOGGED_DESIGNATION = "LOGGED_DESIGNATION"; 
	public final static String LOGGED_DIST_ID = "LOGGED_DIST_ID";
	public final static String LOGGED_DIST_NAME = "LOGGED_DIST_NAME";
	public final static String LOGGED_SYSDATE = "LOGGED_SYSDATE";
	public final static String FNA_SELF_NAME = "FNA_SELF_NAME";
	public final static String FNA_SPOUSE_NAME = "FNA_SPOUSE_NAME";
	public final static String NTUC_POLICY_ACCESS="NTUC_POLICY_ACCESS";
	public final static String NTUC_SFTP_ACCESS="NTUC_SFTP_ACCESS";
	
	
	
	public static final String GLBL_INSERT_MODE = "I";
	public static final String GLBL_UPDATE_MODE = "U";
	public static final String GLBL_DELETE_MODE = "D";
	public static final String GLBL_QUERY_MODE = "Q";
	public static String YUIPATH=null;
	
	public static final String DESIG_GMR="GENERAL MANAGER";
	public static String YUI_PATH = "";
	public static String BIRT_PATH="";

	public static String GLBL_PAYMENTMODE_ANNUALLY="ANNUALLY";
	public static String GLBL_PAYMENTMODE_HALFYEARLY="HALF-YEARLY";
	public static String GLBL_PAYMENTMODE_QUARTELY="QUARTERLY";
	public static String GLBL_PAYMENTMODE_MONTHLY="MONTHLY";
	
	//added by gokul -16/09/2015
	public static String GLBL_MODULE_CLNT="CLIENT";
	public static String GLBL_MODULE_NTUC="NTUC";
	public static final String FILE_NOT_FOUND = "filenotfound";
	public final static String STR_NULL = null;
	
	
	public final static String WS_SUCCESSMESSAGE = "1";
	public final static String WS_FAILUREMESSAGE = "0";
	public final static String STR_NULL_STRING = "";
	public final static String TARGETNAMESPACE_URI= "http://schemas.datacontract.org/2004/07/FirmApprovalLib";
	public final static String WS_APPROVAL_REMARK_ARGUMENT="ApprovalRemark";
	public final static String WS_APPROVAL_APPROVALSTATUS_ARGUMENT="ApprovalStatus";
	public final static String WS_APPROVAL_APPROVER_ARGUMENTNAME="Approver";
	public final static String WS_APPROVAL_FIRMCODE_ARGUMENTNAME="FirmCode";
	public final static String WS_APPROVAL_FIRMREFERENCEID_ARGUMENTNAME="FirmReferenceID";
	public final static String WS_APPROVAL_STEPPOLICYID_ARGUMENTNAME="STEPPolicyID";
	public final static String USER_LOGGED_USER = "STR_LOGGEDUSER";
	public final static String USER_SCR_PRIVS = "STRSCREENPRIVILIGIES";
	public final static String COMPLIANCE_KYC_APPROVAL = "COMP_KYC_APPROVAL";
	public final static String ADMIN_KYC_APPROVAL = "ADMIN_KYC_APPROVAL";
	
	public final static String STR_CLNTSTS_CLNT="C-CLIENT";
	public final static String SIMPLIFIED_VER = "SIMPLIFIED";
	public final static String ALL_VER = "ALL";
	public final static String STR_CNTRY_SINGAPORE = "SINGAPORE";
	
	public static final String INVALID_TOKEN = "invalid.token";
	
	public static String GLBL_MODULE_POLICY="POLICY";
	public static String GLBL_MODULE_AGNT="AGENT";
	
	 //LOGIN
    public final static String USER_ROLE_PRIVS = "C_USER_ROLE_PRIVS";
    public final static String USER_LOGGED_MAIL = "C_USER_MAIL";
    public final static String LOGGED_USER_STAFFID = "C_LOGUSER_STAFFID";
    public final static String LOGGED_USER_GLBL_ACCESSLVL="C_LOGUSER_ACCESS_LEVEL";
    public final static String LOGGED_USER_STAT="C_LOGUSER_EMP_STAT";    
    public final static String GLBL_STR_ACCLVL_COMP ="COMPANY";
    public final static String GLBL_STR_ACCLVL_MGR ="MANAGER";
    public final static String GLBL_STR_ACCLVL_ADVSTF ="ADVISER/STAFF";
    
    
    public final static String GI_AGENT_REG_BROKID = "C_GIAGENT_STAFFID";
    public final static String GI_AGENT_REG_BRKTYPE = "C_GIAGENT_BRK_TYPE";
    
    // AGENT RELATED
    public final static String AGENT_REG_STAFFID = "C_AGENT_STAFFID";
    public final static String AGENT_REG_STAFFNAME = "C_AGENT_STAFF_NAME";
    public final static String AGENT_REG_STAFFTYPE = "C_AGENT_STAFF_TYPE";
    public final static String AGENT_REG_COMP_ACCESS = "C_AGENT_STAFF_COMP_ACCESS";
    public final static String AGENT_REG_MGR_ACCESS = "C_AGENT_STAFF_MGR_ACCESS";
    
    
    public final static String CURRENT_CUSTID = "CURRENT_CUSTID";
    public final static String CURRENT_CUSTNAME = "CURRENT_CUSTNAME";
    
    public final static String  CURRENT_SPSNAME = " CURRENT_SPSNAME";
    
    
    public final static String  CREATE_NEW_FNA_FLG = "CREATE_NEW_FNA_FLG";
    
    public final static String  PREV_FNAID = "PREV_FNAID"; 
    
    //poovathi add on 10-9-2021
    public final static String  LAT_ARCHV_FNAID = "LAT_ARCHV_FNAID"; 
    
    //poovathi add on 27-08-2021
    public final static String  PREV_FNA_REMOV_FLG = "PREV_FNA_REMOV_FLG"; 
    
    
    //poovathi add on 24-10-2021
    public final static String  FULLY_APPR_EKYC = "FULLY_APPR_EKYC"; 
    
    
    //poovathi add on 24-11-2021
    public final static String  SESS_SWTCH_FLG = "SESS_SWTCH_FLG"; 
    
    public final static String  LAT_ARCHV_FNA = "LAT_ARCHV_FNA";
    
    public final static String  ADDNEWCLIENT2 = "ADDNEWCLIENT2";
    
    //poovathi add on 20-01-21 for product screen validations
    public final static String  PROD_SCRN_VALI_FLG = "PROD_SCRN_VALI_FLG";
    
    
    public final static String SIGN_CLIENT="CLIENT";
	public final static String SIGN_SPS="SPOUSE";
	public final static String SIGN_ADVISOR="ADVISER";
	public final static String SIGN_MANAGER="MANAGER";
}
