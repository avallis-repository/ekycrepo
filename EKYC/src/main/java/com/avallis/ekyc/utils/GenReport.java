package com.avallis.ekyc.utils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;



public class GenReport  {

	private static final long serialVersionUID = 1L;
	static Logger kyclog = Logger.getLogger(GenReport.class.getName());
	ResourceBundle resource = ResourceBundle.getBundle("AppResource");

	public static void saveFileFromUrl(String fileName, InputStream in)
			throws MalformedURLException, IOException {

//		BufferedInputStream in = null;
		FileOutputStream fout = null;
		try {
//			in = new BufferedInputStream(new URL(fileUrl).openStream());
			fout = new FileOutputStream(fileName);

			byte data[] = new byte[2048];
			int count = -1;
			
			while ((count = in.read(data)) > -1) {
			    fout.write(data, 0, count);
			}

			kyclog.info(fileName + "-----------> File writing....");
		} finally {
			if (in != null)
				in.close();
			if (fout != null)
				fout.close();

			kyclog.info("-----------> File written success to "+fileName);
		}
	}

		public String genPdfFromUrl(String strFnaId,String strCustId,
									String strRptLoc,String strPdfLoc,
									String strGenFileName,String strBirtUrl,
									String strCustName,String strNric,
									String strCustLob,
									String strDistId,
									String strFormType,
									boolean strSpcificPage) throws Exception {

			String retFileName = "";
			retFileName = strPdfLoc + strGenFileName;

			String param = "&P_CUSTID="+strCustId+"&P_FNAID="+strFnaId+
					"&P_CUSTOMERNAME="+strCustName+"&P_CUSTNRIC="+strNric+
					"&P_PRDTTYPE="+strCustLob+"&P_DISTID="+strDistId+"&P_FORMTYPE="+strFormType;
			
			strRptLoc=java.net.URLEncoder.encode(strRptLoc,"UTF-8");
			String fileurl = strBirtUrl+"/frameset?__report="+strRptLoc+"&__format=pdf"+param;
			kyclog.info("fileURL --->"+fileurl);

			String exportZipType = resource.getString("fna.report.exportzip.generate.type");
			String allOptPageRange = resource.getString("fna.report.specific.pageno.all");
			String simOptPageRange = resource.getString("fna.report.specific.pageno.simplified");

			if(strSpcificPage){

				if(!exportZipType.equalsIgnoreCase("ALL")){
					strSpcificPage = true;

					String pages = "";

					if(strFormType.equalsIgnoreCase("SIMPLIFIED")){
						pages = simOptPageRange;

					}

					if(strFormType.equalsIgnoreCase("ALL")){
						pages = allOptPageRange;
					}

					fileurl = fileurl + "&__pagerange="+pages;
				}

			}




			URL obj = new URL(fileurl);
			InputStream in = obj.openStream();
			

			saveFileFromUrl(retFileName , in);
			kyclog.info("file is downloaded....to " + retFileName);

			return retFileName ;

		}


}