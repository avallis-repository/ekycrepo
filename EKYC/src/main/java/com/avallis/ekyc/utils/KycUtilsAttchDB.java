package com.avallis.ekyc.utils;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.json.JSONArray;
import org.json.JSONObject;

import com.avallis.ekyc.dbinterfaces.DBIntf;


public class KycUtilsAttchDB {
	static Logger kyclog = Logger.getLogger(KycUtilsAttchDB.class.getName());
	ResourceBundle resource = ResourceBundle.getBundle("AppResource");


	public String saveNRICDocument(DBIntf dbDTO,HttpServletRequest req,ArrayList<File> DocLst){
		HttpSession sess = req.getSession();

		String regexchars = resource.getString("attach.bfile.SplCharVal");
		String custdir =  resource.getString("attach.bfile.clnt.dir");
		String retMsg="";

		JSONObject jsnObj=new JSONObject();

		JSONArray sessMapArr=(JSONArray) sess.getAttribute("COMMON_SESS_OBJ");

		jsnObj=(JSONObject) sessMapArr.get(0);

		String strCurrAdvName =jsnObj.getString("CURR_ADVSTF_NAME");
		String strCustName=jsnObj.getString("CUST_NAME");
		String strAtmtId = null;
		String strCustId=jsnObj.getString("STR_CUSTID");
		String strAtmtCreatedBy=jsnObj.getString("STR_LOGGEDUSER");
		String strDistId=jsnObj.getString("LOGGED_DISTID");;
		String strDistrName=jsnObj.getString("STR_DISTRIBUTORNAME");;

		String[] strAtmtAttachMode=req.getParameterValues("txtFldClntDocMode") != null ? req.getParameterValues("txtFldClntDocMode") :null;
		String[] strAttachFor=req.getParameterValues("selClntDocFor") != null ? req.getParameterValues("selClntDocFor") :null;
		String[] strAtmtAttachCategName=req.getParameterValues("txtFldClntDocChkList") != null ? req.getParameterValues("txtFldClntDocChkList") :null;
		String[] strAtmtTitle=req.getParameterValues("txtFldClntDocTitle") != null ? req.getParameterValues("txtFldClntDocTitle") :null;
		String[] strAtmtPageNo=req.getParameterValues("txtFldClntDocPgNo") != null ? req.getParameterValues("txtFldClntDocPgNo") :null;
		String[] strAtmtRemarks=req.getParameterValues("txtFldClntDocRmk") != null ? req.getParameterValues("txtFldClntDocRmk") :null;
		String[] strAtmtAttachCateg=req.getParameterValues("txtFldPopUpDocCode") != null ? req.getParameterValues("txtFldPopUpDocCode") :null;
		String[] strAtmtAttchApplName = req.getParameterValues("txtFldDocApplcntName") != null ? req.getParameterValues("txtFldDocApplcntName") : null;
		
		/*if(KycUtils.nullOrBlank(strAttachFor)){
		strAtmtAttachCategName=req.getParameter("txtFldPopUpChkList") != null ? req.getParameter("txtFldPopUpChkList") :"";
		strAtmtTitle=req.getParameter("txtFldPopUpDocTitle") != null ? req.getParameter("txtFldPopUpDocTitle") :"";
		strAtmtPageNo=req.getParameter("txtFldPopupPageNo") != null ? req.getParameter("txtFldPopupPageNo") :"";
		strAtmtRemarks=req.getParameter("txtFldPopUpAttRem") != null ? req.getParameter("txtFldPopUpAttRem") :"";
		strAtmtAttachCateg=req.getParameter("txtFldPopUpDocCode") != null ? req.getParameter("txtFldPopUpDocCode") :"";
		strAttachFor="";
		}else{
			strAtmtAttachCategName=req.getParameter("txtFldSpsPopUpChkList") != null ? req.getParameter("txtFldSpsPopUpChkList") :"";
			strAtmtTitle=req.getParameter("txtFldSpsPopUpDocTitle") != null ? req.getParameter("txtFldSpsPopUpDocTitle") :"";
			strAtmtPageNo=req.getParameter("txtFldSpsPopupPageNo") != null ? req.getParameter("txtFldSpsPopupPageNo") :"";
			strAtmtRemarks=req.getParameter("txtFldSpsPopUpAttRem") != null ? req.getParameter("txtFldSpsPopUpAttRem") :"";
			strAtmtAttachCateg=req.getParameter("txtFldSpsPopUpDocCode") != null ? req.getParameter("txtFldSpsPopUpDocCode") :"";
		}*/

		//String strAtmtCreatedDate=KycUtils.convertDateToDateString(new Date());
		for(int count=0;count<strAtmtAttachMode.length;count++){

		try {
			if(strAtmtAttachMode[count].equals(KycConst.GLBL_INSERT_MODE)){
			byte[] filedata = null;

			filedata = KycUtils.fpmsReadInputStream(new FileInputStream(DocLst.get(count)));
			String strApplcntName = strAtmtAttchApplName[count];
			strApplcntName = Utility.checkNullVal(strApplcntName) ? ""  : strApplcntName.replace("\'", "\''");

			String strAtmtFileName = DocLst.get(count).getName();
			String strAtmtFileType = KycUtils.getFileExtension(DocLst.get(count));
			String strAtmtFileSize = Integer.toString(filedata.length);

			strCurrAdvName = KycUtils.replaceSplChars(regexchars, strCurrAdvName);
			strCustName  = KycUtils.replaceSplChars(regexchars, strCustName);
			strAtmtFileName = KycUtils.formatFileName(strAtmtFileName);

			String strBFILEName = strCurrAdvName+File.separator+strCustName+File.separator+strAtmtFileName;
			strBFILEName = strBFILEName.replace("\'", "\''");

			File srcFile  = KycUtils.createPhysicalDir(KycConst.GLBL_MODULE_CLNT,strCurrAdvName,strCustName,strAtmtFileName);
			retMsg = KycUtils.createPhysicalFile(DocLst.get(count),srcFile);

			if(KycUtils.nullOrBlank(retMsg)){

				if(strAtmtFileType.length()>40){
	    			strAtmtFileType = "document/text";
	    		}

	    	strAtmtId = formatAttachIdQuartz(KycConst.GLBL_MODULE_CLNT, "CUSTATT", 11,dbDTO);


	    	String ATTCH_INSERT_QRY = "INSERT INTO CUSTOMER_ATTACHMENTS"
	    	+ "(CUST_ATTACH_ID,CUSTID,TITLE,PAGE_NUM,REMARKS,FILENAME,FILESIZE,FILETYPE,ATTACH_CATEG_ID,"
	    	+ "ATTACH_CATEG_NAME,CREATED_BY,CREATED_DATE,MODIFIED_BY,MODIFIED_DATE,DISTRIBUTOR_ID,DISTRIBUTOR_NAME,DOCUMENT,ATTACH_FOR) " +
	    	" VALUES('"+strAtmtId+"','"+strCustId+"','"+strAtmtTitle[count]+"','"+strAtmtPageNo[count]+"'," +
	    	"'"+strAtmtRemarks[count]+"','"+strAtmtFileName+"','"+strAtmtFileSize+"','"+strAtmtFileType+"'," +
	    	"'"+strAtmtAttachCateg[count]+"','"+strAtmtAttachCategName[count]+"','"+strAtmtCreatedBy+"',SYSDATE," +
	    	"'','','"+strDistId+"','"+strDistrName+"',BFILENAME('"+custdir+"','"+strBFILEName+"'),'"+strAttachFor[count].toUpperCase()+"')";


	    		dbDTO.updateDbByNativeSQLQuery(ATTCH_INSERT_QRY);

	    		retMsg="Successfully NRIC Document saved.";
			}
			}
		} catch (Exception e) {
			e.printStackTrace();
			retMsg=e.getMessage();
		}

		}

		return retMsg;
	}

//	public String fetchSaveAttch(DBIntf dbDTO,HttpServletRequest req,String polDet){}

	public static String formatAttachIdQuartz(String strScreen,String prefix,int padding,DBIntf dbDTO){

		  String strAttachId = KycConst.STR_NULL_STRING;


		  try{

			  if(strScreen.equalsIgnoreCase(KycConst.GLBL_MODULE_CLNT)){

					String maxCustAttchQry ="SELECT LTRIM(RTRIM(TO_CHAR(CUST_ATT_SEQ.NEXTVAL,'00000000000'))) MAXUWID FROM DUAL";
					strAttachId = prefix + dbDTO.fetchMaxSeqVal(maxCustAttchQry);
					kyclog.info("strAttachId --> " + strAttachId);
			  }else if(strScreen.equalsIgnoreCase(KycConst.GLBL_MODULE_AGNT)){

					String maxCustAttchQry =" SELECT LTRIM(RTRIM(TO_CHAR(ADV_ATT_SEQ.NEXTVAL,'00000000000000'))) MAXUWID FROM DUAL";
					strAttachId = prefix + dbDTO.fetchMaxSeqVal(maxCustAttchQry);
					kyclog.info("strAttachId --> " + strAttachId);
			  }else if(strScreen.equalsIgnoreCase(KycConst.GLBL_MODULE_POLICY)){

					String maxCustAttchQry ="SELECT LTRIM(RTRIM(TO_CHAR(POL_ATT_SEQ.NEXTVAL,'0000000000000'))) MAXUWID FROM DUAL";
					strAttachId = prefix + dbDTO.fetchMaxSeqVal(maxCustAttchQry);
					kyclog.info("strAttachId --> " + strAttachId);
			  }



		  }catch(Exception ex){
			  kyclog.info("Error in formatAttachId "+ex.getMessage());
		  }
		  return strAttachId;
	  }

	public List  downloadAttachments(DBIntf dto,String strClntId,String strDistId) throws HibernateException{
		 List attachDocList = null;
		  try{
			  //String strWhrcondQry = " from CUSTOMER_ATTACHMENTS attch  where (attch.CUSTID)='"+strClntId+"' and attch.TITLE='NRIC' and  (attch.DISTRIBUTOR_ID) = '"+strDistId+"' and attch.CUST_ATTACH_ID=(SELECT MAX(CUST_ATTACH_ID) FROM CUSTOMER_ATTACHMENTS where CUSTID='"+strClntId+"' and TITLE='NRIC' and  DISTRIBUTOR_ID = '"+strDistId+"')  ";
			 String strcondQry = "select ATTACH_CATEG_ID,TITLE,ATTACH_FOR,DISTRIBUTOR_ID,DISTRIBUTOR_NAME from CUSTOMER_ATTACHMENTS attch  where (attch.CUSTID)='"+strClntId+"' AND attch.attach_for IS NOT NULL and  (attch.DISTRIBUTOR_ID) = '"+strDistId+"' order by ATTACH_FOR asc NULLS FIRST,CUST_ATTACH_ID";
			  attachDocList = dto.searchByNativeSQLQuery(strcondQry);//,strAtchId, strCliId,strDistID);
			  kyclog.info("DB attachDocList downloadAttachments---->"+attachDocList);
		  }catch (HibernateException he) {
			  kyclog.info(" -- > downloadAttachments error " + he);
		}


		  return  attachDocList;
	}

	public List fetchNricAttch(DBIntf dto,String strClntId,String strDistId) throws HibernateException{
		 List attachDocList = null;
		  try{
			  //String strWhrcondQry = " from CUSTOMER_ATTACHMENTS attch  where (attch.CUSTID)='"+strClntId+"' and attch.TITLE='NRIC' and  (attch.DISTRIBUTOR_ID) = '"+strDistId+"' and attch.CUST_ATTACH_ID=(SELECT MAX(CUST_ATTACH_ID) FROM CUSTOMER_ATTACHMENTS where CUSTID='"+strClntId+"' and TITLE='NRIC' and  DISTRIBUTOR_ID = '"+strDistId+"')  ";
			 String strcondQry = "select ATTACH_CATEG_ID,TITLE,ATTACH_FOR,DISTRIBUTOR_ID,DISTRIBUTOR_NAME,ATTACH_CATEG_NAME,FILENAME,FILETYPE,PAGE_NUM,REMARKS,CREATED_BY,To_char(CREATED_DATE,'dd/mm/yyyy') ATTACH_DATE1,'' AS ATTACH_APPLICANT_NAME from CUSTOMER_ATTACHMENTS attch  where (attch.CUSTID)='"+strClntId+"'  AND attch.attach_for IS NOT NULL and  (attch.DISTRIBUTOR_ID) = '"+strDistId+"' order by ATTACH_FOR asc NULLS FIRST ,CUST_ATTACH_ID";
			 attachDocList = dto.searchByNativeSQLQuery(strcondQry);//,strAtchId, strCliId,strDistID);
			  kyclog.info("DB attachDocList downloadAttachments---->"+attachDocList);
		  }catch (HibernateException he) {
			  kyclog.info(" -- > downloadAttachments error " + he);
		}


		  return  attachDocList;
	}


public String sftpPolAttachInsert(DBIntf dbDTO,HttpSession sess,String filePath,String strAppId){

		try {

		JSONObject jsnObj=new JSONObject();

		JSONArray sessMapArr=(JSONArray) sess.getAttribute("COMMON_SESS_OBJ");

		jsnObj=(JSONObject) sessMapArr.get(0);

	    String strUsrName =jsnObj.getString("STR_LOGGEDUSER");
//	    String[] polDetArr=polDet.split("~");

	    String retMsg = KycConst.STR_NULL_STRING;
//	    String strPolNo =KycConst.STR_NULL_STRING;
//	    String strCurrADv = KycConst.STR_NULL_STRING;

//	    	String regexchars = resource.getString("attach.bfile.SplCharVal");
	    	String policydir = resource.getString("attach.bfile.policy.dir");

	        String strDistId = jsnObj.getString("LOGGED_DISTID");
	        String strDistName =jsnObj.getString("STR_DISTRIBUTORNAME");
//	        String strAdvName =jsnObj.getString("CURR_ADVSTF_NAME");


//	        String strPolAppId  = polDetArr[0];
//	        strPolNo = polDetArr[1];


	        String strpolAttachId = null;
	        String strPolAttachMode = KycConst.GLBL_INSERT_MODE;
	        String strPolAttMasterAttachCateg = resource.getString("attachment.catagory.id");
	        String strPolAttachMasCategName =resource.getString("attachment.catagory.name");
	        String strPolAttachTitle = resource.getString("attachment.catagory.name");
//			String strPolAttachDate =KycUtils.convertDateToDateString(new Date());
	        String strPolAttachPageNum = resource.getString("attachment.catagory.pgno");
	        String strPolAttachRemarks = "";
//	        String strFileName = "";
	        String strModRefid = "";

	        Date dteAttachCreatedDate = new Date();
	        String strAtmtCreatedDate = KycUtils.convertDateToDate(dteAttachCreatedDate);
	        String strAttachCreatedBy = strUsrName.toUpperCase();
//	        Date dteAttachModifyDate = null;
//	        String strAtmtModifyBy = null;

	        String strDistrName = strDistName;

	        if(strPolAttachMode.equalsIgnoreCase(KycConst.GLBL_INSERT_MODE)){


    		byte[] filedata = null;

    		File file=new File(filePath);//File to be added from property file
    		FileInputStream fin=new FileInputStream(file);

    		try{
		    			filedata = KycUtils.fpmsReadInputStream(fin);
    		}catch(Exception ex){
		    			ex.printStackTrace();

		   			  	retMsg = "strExcep";
    		}

    		retMsg = KycConst.STR_NULL_STRING;
    		String strAtmtFileName =file.getName();
    		String strAtmtFileType =KycUtils.getFileExtension(file);
    		String strAtmtFileSize = Integer.toString(filedata.length);;


//    		strPolNo = KycUtils.replaceSplChars(regexchars, strPolNo);
//    		strAdvName = KycUtils.replaceSplChars(regexchars, strAdvName);
//    		strAtmtFileName = KycUtils.formatFileName(strAtmtFileName);
    		String strBFILEName =strAtmtFileName;
    		/*strAdvName.trim()+File.separator+strPolNo+File.separator+strAtmtFileName;
    		strBFILEName = strBFILEName.replace("\'", "\''");*/

//    		File srcFile  = KycUtils.createPhysicalDir(KycConst.GLBL_MODULE_POLICY,strAdvName.trim(),strPolNo,strAtmtFileName);
//    		retMsg = KycUtils.createPhysicalFile(file, srcFile);

			strpolAttachId = formatAttachIdQuartz(KycConst.GLBL_MODULE_POLICY, "POLATT", 13,dbDTO);

			String ATTCH_INSERT_QRY = "INSERT INTO POL_ATTACHMENTS (POL_ATTACH_ID,APPID,TITLE,PAGE_NUM, REMARKS, FILENAME,FILESIZE,FILETYPE,ATTACH_CATEG_ID,ATTACH_CATEG_NAME,CREATED_BY,CREATED_DATE,MODIFIED_BY,MODIFIED_DATE,DISTRIBUTOR_ID,DISTRIBUTOR_NAME,DOCUMENT,MODULE_REF_ID)" +
					" VALUES('"+strpolAttachId+"','"+strAppId+"','"+strPolAttachTitle+"','"+strPolAttachPageNum+"'," +
					"'"+strPolAttachRemarks+"','"+strAtmtFileName+"','"+strAtmtFileSize+"','"+strAtmtFileType+"'," +
					"'"+strPolAttMasterAttachCateg+"','"+strPolAttachMasCategName+"','"+strAttachCreatedBy+"','"+strAtmtCreatedDate+"'," +
					"'','','"+strDistId+"','"+strDistrName+"',BFILENAME('"+policydir+"','"+strBFILEName+"'),'"+strModRefid+"')";


			dbDTO.updateDbByNativeSQLQuery(ATTCH_INSERT_QRY);

			kyclog.info("sftpPolAttachInsert ----> Successfully saved attachment");
	        }

	    }catch (Exception e) {
	    	e.printStackTrace();
		}


		return "Successfully saved attachment";
	}

  public List fetchNtucDocument(DBIntf dbDTO,HttpServletRequest req){

	  List lstDoc=null;
	  String strFnaId=req.getParameter("strFnaId")!=null?req.getParameter("strFnaId"):"";

	  String Sql="select ND_ID,FNA_ID,NTUC_CASE_ID,DOC_AVA_DOCTITILE,DOC_AVA_FILENAME,DOC_AVA_FILETYPE,DOC_AVA_FILESIZE,"+
			  	 "DOC_AVA_CATEGID,DOC_CRTD_BY,TO_CHAR(DOC_CRTD_DATE,'dd/mm/yyyy') FROM FNA_NTUC_DOCUMENTS WHERE FNA_ID='"+strFnaId+"'";

	  lstDoc=dbDTO.searchByNativeSQLQuery(Sql);

	  return lstDoc;
  }
  
  public String uploadAdvApprovalDocs(DBIntf dbDTO,HttpServletRequest req,ArrayList<File> DocLst){
		HttpSession sess = req.getSession();

		String regexchars = resource.getString("attach.bfile.SplCharVal");
		String retMsg="";

		JSONObject jsnObj=new JSONObject();
		JSONArray sessMapArr=(JSONArray) sess.getAttribute("COMMON_SESS_OBJ");
		jsnObj=(JSONObject) sessMapArr.get(0);

		String strCurrAdvName =jsnObj.getString("CURR_ADVSTF_NAME");
		String strCustName=jsnObj.getString("CUST_NAME");
		 String strUsrName =jsnObj.getString("STR_LOGGEDUSER");
		 
		String[] strAtmtAttachMode=req.getParameterValues("txtFldKycApprAdvDocmode") != null ? req.getParameterValues("txtFldKycApprAdvDocmode") :null;
		String[] strChkListArr=req.getParameterValues("selKycApprAdvChkList") != null ? req.getParameterValues("selKycApprAdvChkList") :null;
		String[] strDocTitleArrCode=req.getParameterValues("selKycApprAdvDocTitle") != null ? req.getParameterValues("selKycApprAdvDocTitle") :null;
		String[] strDocTitleTxtArr=req.getParameterValues("txtFldPopUpDocText") != null ? req.getParameterValues("txtFldPopUpDocText") :null;
		String[] strAtmtPageNo=req.getParameterValues("txtFldKycApprAdvDocPgNo") != null ? req.getParameterValues("txtFldKycApprAdvDocPgNo") :null;
		String[] strAtmtRemarks=req.getParameterValues("txtFldKycApprAdvDocRem") != null ? req.getParameterValues("txtFldKycApprAdvDocRem") :null;
	
		String strFnaId = req.getParameter("txtFldFNAID") != null ? req.getParameter("txtFldFNAID"):"";

		for(int count=0;count<strAtmtAttachMode.length;count++){
			

		try {
			if(strAtmtAttachMode[count].equals(KycConst.GLBL_INSERT_MODE)){
			byte[] filedata = null;

			filedata = KycUtils.fpmsReadInputStream(new FileInputStream(DocLst.get(count)));
			String strApplcntName = "";
			strApplcntName = Utility.checkNullVal(strApplcntName) ? ""  : strApplcntName.replace("\'", "\''");

			String strAtmtFileName = DocLst.get(count).getName();
			String strAtmtFileType = KycUtils.getFileExtension(DocLst.get(count));
			String strAtmtFileSize = Integer.toString(filedata.length);

			strCurrAdvName = KycUtils.replaceSplChars(regexchars, strCurrAdvName);
			strCustName  = KycUtils.replaceSplChars(regexchars, strCustName);
			strAtmtFileName = KycUtils.formatFileName(strAtmtFileName);

//			String strBFILEName = strCurrAdvName+File.separator+strCustName+File.separator+strAtmtFileName;
//			strBFILEName = strBFILEName.replace("\'", "\''");
			
			String strBFILEName = strFnaId + File.separator+File.separator+ strAtmtFileName;

			File srcFile  = KycUtils.createPhysicalDir(KycConst.GLBL_MODULE_NTUC,strFnaId,"",strAtmtFileName);
			retMsg = KycUtils.createPhysicalFile(DocLst.get(count),srcFile);
			
			String strDataKey = "",
					strdesc = "",
					strfilepath = "",
					strkey= "",
					strfiletype = "",
					encryptfilelen="",
					decryptfilename =strAtmtFileName,
					filetype= strAtmtFileType,
					doctitle = strDocTitleArrCode[count], //strDocTitleTxtArr[count],
					decryptfilelen = strAtmtFileSize,
					ntuc_orcl_dir = resource.getString("attach.bfile.ntuc.dir");
			

			if(KycUtils.nullOrBlank(retMsg)){

				if(strAtmtFileType.length()>40){
	    			strAtmtFileType = "document/text";
	    		}
				
				
				 String ATTCH_INSERT_QRY = "INSERT INTO FNA_NTUC_DOCUMENTS(ND_ID,FNA_ID,NTUC_CASE_ID,"
				       		+ "DOC_DATA_KEY,DOC_DESC,DOC_SFTP_FILEPATH,DOC_DECRYPT_KEY,DOC_FILETYPE,DOC_FILESIZE,"
				       		+ "DOC_AVA_DOCTITILE,DOC_AVA_FILENAME,DOC_AVA_FILETYPE,DOC_AVA_FILESIZE,DOC_AVA_CATEGID,"
				       		+ "DOC_CRTD_BY,DOC_CRTD_DATE,"
				       		+ "DOCUMENT)"
				   	    	+" VALUES(KYC_NTUC_DOCID.nextval,'"+strFnaId+"','',"
				   	    	+ "'"+strDataKey+"'," +"'"+strdesc+"','"+strfilepath+"','"+strkey+"','"+strfiletype+"','"+encryptfilelen+"',"
				   	    	+ "'"+doctitle+"','"+decryptfilename+"','"+filetype+"','"+decryptfilelen+"',"
				   	    	+ "null,'"+strUsrName.toUpperCase()+"',SYSDATE,"+"BFILENAME('"+ntuc_orcl_dir+"','"+strBFILEName+"'))";

	    	

	    		dbDTO.updateDbByNativeSQLQuery(ATTCH_INSERT_QRY);

	    		retMsg="Documents are successfully uploaded";
			}
			}
		} catch (Exception e) {
			e.printStackTrace();
			retMsg="Document upload failed";
		}

		}

		return retMsg;
	}
  
//poovathi add on 02-04-2020
  public List fetchNRICDocument(DBIntf dbDTO,HttpServletRequest request){

	  List lstDoc=null;
	  String strCustId = request.getParameter("strcustId")!=null?request.getParameter("strcustId"):"";
      //System.out.println("custId===>>>>>>>>>>>>"+ strCustId);
	  String Sql = " SELECT CUST_ATTACH_ID, CUSTID,TITLE,FILENAME,FILESIZE,FILETYPE, NTUC_POLICY_ID,ATTACH_FOR "
	  		+ " FROM CUSTOMER_ATTACHMENTS WHERE CUSTID='"+strCustId+"'"
			+" AND TITLE='NRIC/PASSPORT' AND ( ATTACH_FOR='CLIENT' OR ATTACH_FOR='SPOUSE/CLIENT') ";

	  lstDoc=dbDTO.searchByNativeSQLQuery(Sql);
       //System.out.println("List===>>>>>"+ lstDoc);
	  return lstDoc;
  }
  
}
