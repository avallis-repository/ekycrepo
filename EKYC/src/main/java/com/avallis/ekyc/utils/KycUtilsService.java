package com.avallis.ekyc.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.avallis.ekyc.dbinterfaces.DBIntf;
import com.avallis.ekyc.dto.AdviserStaff;
import com.avallis.ekyc.dto.AdvstfCodes;
import com.avallis.ekyc.dto.CustomerDetails;
import com.avallis.ekyc.dto.FPMSLabelValueDTO;
import com.avallis.ekyc.dto.FnaNtucEsubmissions;
import com.avallis.ekyc.dto.FnaNtucPolicyDets;
import com.avallis.ekyc.dto.FpmsDistributor;
import com.avallis.ekyc.dto.MasterDesignation;
import com.avallis.ekyc.dto.MasterPolicyStatus;
import com.avallis.ekyc.dto.MasterPrincipal;
import com.avallis.ekyc.dto.MasterProduct;
import com.avallis.ekyc.dto.MasterProductLine;
import com.avallis.ekyc.dto.PolInslifebasic;
import com.avallis.ekyc.dto.Policy;
import com.avallis.ekyc.services.FpmsService;
import com.avallis.ekyc.services.KycService;


public class KycUtilsService {
	ResourceBundle resource = ResourceBundle.getBundle("AppResource");
	
	public String saveNRICDocument(DBIntf dbDTO,HttpServletRequest req,ArrayList<File> DocLst){
		KycUtilsAttchDB cDB = new KycUtilsAttchDB();
		return cDB.saveNRICDocument(dbDTO,req,DocLst);
	}

	public String insertPolicyDets(DBIntf dbDTO,Policy policyObj){
		KycUtilsDB db=new KycUtilsDB();
		return db.insertPolicyDets(dbDTO,policyObj);	
	}
	
	public void insertPolInsLifeBasicDets(DBIntf dbDTO,List<PolInslifebasic> polBasicPdtIns) throws Exception{
		KycUtilsDB db=new KycUtilsDB();
		db.insertPolInsLifeBasicDets(dbDTO,polBasicPdtIns);	
	}
	
	public List<CustomerDetails> getCustomerDetails(DBIntf dbDTO, String strNric,String strAdvId)throws Exception{
		KycUtilsDB cDB = new KycUtilsDB();
		return cDB.getCustomerDetails(dbDTO, strNric,strAdvId);
	}
	
	public String insertCustomerClient(DBIntf dbDTO,CustomerDetails custObj)throws Exception{
		KycUtilsDB db=new KycUtilsDB();
		return db.insertCustomerClient(dbDTO,custObj);	
	}
	
	/*public String fetchSaveAttch(DBIntf dbDTO,HttpServletRequest req,String polDet){
		KycUtilsAttchDB db=new KycUtilsAttchDB();
		return db.fetchSaveAttch(dbDTO, req, polDet);
	}*/
	
	public List<AdvstfCodes> getAdviserDetailsBasedOnCodes(String strPrinId,String strDistrId,String strAgentCode){
		KycUtilsDB db=new KycUtilsDB();
		return db.getAdviserDetailsBasedOnCodes(strPrinId,strDistrId,strAgentCode);
	}
	
	public List<AdvstfCodes> getAdvStfCodeDetsBasedOnAdvId(String strPrinId,String strDistrId,String strAgentId){
		KycUtilsDB db=new KycUtilsDB();
		return db.getAdvStfCodeDetsBasedOnAdvId(strPrinId,strDistrId,strAgentId);
	}
	
	public List<AdviserStaff> getAdviserStaffDetails(DBIntf dbDTO,String strAdviserId)throws Exception{
		KycUtilsDB db=new KycUtilsDB();
		return db.getAdviserStaffDetails(dbDTO, strAdviserId);
	}
	
	public List getPrxyAdviserStaffDetails(String strAdviserId) throws Exception{
		KycUtilsDB db=new KycUtilsDB();
		return db.getPrxyAdviserStaffDetails(strAdviserId);
	}
	
	public List<MasterProduct> getMasterProductDets(DBIntf dbDTO,String strProdCode,String strPrinId)throws Exception{
		KycUtilsDB db=new KycUtilsDB();
		return db.getMasterProductDets(dbDTO,strProdCode,strPrinId);	
	}
	
	public List downloadAttachments(DBIntf dbDTO, String strClntId,String strDistId){
		KycUtilsAttchDB cDB = new KycUtilsAttchDB();
		return cDB.downloadAttachments(dbDTO, strClntId,strDistId);
	}
	
	public List fetchNricAttch(DBIntf dbDTO, String strClntId,String strDistId){
		KycUtilsAttchDB cDB = new KycUtilsAttchDB();
		return cDB.fetchNricAttch(dbDTO, strClntId,strDistId);
	}
	
	public List<Policy> chkPolicyExistSts(DBIntf dbDTO,String strCaseId){
		KycUtilsDB db=new KycUtilsDB();
		return db.chkPolicyExistSts(dbDTO,strCaseId);	
	}
	
	public List<PolInslifebasic> chkPolicyInsLifeBasExistSts(DBIntf dbDTO,String strAppId){
		KycUtilsDB db=new KycUtilsDB();
		return db.chkPolicyInsLifeBasExistSts(dbDTO,strAppId);	
	}
	
	public List<Object> getESubmitOAuthDets(DBIntf dbDTO,String strAdvStfCode,String strEubId,String strFnaId){
		KycUtilsDB db=new KycUtilsDB();
		return db.getESubmitOAuthDets(dbDTO,strAdvStfCode,strEubId,strFnaId);	
	}
	
	public List<Object> getOriginalAssertionId(DBIntf dbDTO,String samlAssrId){
		KycUtilsDB db=new KycUtilsDB();
		return db.getOriginalAssertionId(dbDTO,samlAssrId);	
	}
	
	public String insertESubmission(DBIntf dbDTO,FnaNtucEsubmissions fnaESub){
		KycUtilsDB db=new KycUtilsDB();
		return db.insertESubmission(dbDTO,fnaESub);	
	}
	
	public void updateCaseIdDets(DBIntf dbDTO,String strCaseId,String strCaseNo,String strEsubId){
		KycUtilsDB db=new KycUtilsDB();
		db.updateCaseIdDets(dbDTO,strCaseId,strCaseNo,strEsubId);	
	}
	
	public void updateFnaDetsWithCaseId(DBIntf dbDTO,String strCaseId,String strCasests,String strFnaId){
		KycUtilsDB db=new KycUtilsDB();
		db.updateFnaDetsWithCaseId(dbDTO,strCaseId,strCasests,strFnaId);	
	}
	
	public void updateFnaCaseStsByCaseId(DBIntf dbDTO,ServletContext context,String strCaseId,String strCasests){
		KycUtilsDB db=new KycUtilsDB();
		db.updateFnaCaseStsByCaseId(dbDTO,context,strCaseId,strCasests);	
	}
	
	public void updateFnaPolCrtFldByCaseId(DBIntf dbDTO,ServletContext context,String strCaseId,String strPolCrtFlg){
		KycUtilsDB db=new KycUtilsDB();
		db.updateFnaPolCrtFldByCaseId(dbDTO,context,strCaseId,strPolCrtFlg);	
	}
	
	public void updatePolDetsByCaseId(DBIntf dbDTO,ServletContext context,String strCaseId,String ntucPolNo){
		KycUtilsDB db=new KycUtilsDB();
		db.updatePolDetsByCaseId(dbDTO,context,strCaseId,ntucPolNo);	
	}
	
	public void updateAvaSAMLRespDets(DBIntf dbDTO,Map<String,String> samlMap,String strEsubId){
		KycUtilsDB db=new KycUtilsDB();
		db.updateAvaSAMLRespDets(dbDTO,samlMap,strEsubId);	
	}
	
	public void updateIDPSAMLRespDets(DBIntf dbDTO,Map<String,String> samlMap,String strEsubId){
		KycUtilsDB db=new KycUtilsDB();
		db.updateIDPSAMLRespDets(dbDTO,samlMap,strEsubId);	
	}
	
	/*public String sftpPolAttachInsert(DBIntf dbDTO,HttpSession sess,String filePath,String strAppId){
		KycUtilsAttchDB db=new KycUtilsAttchDB();
		return db.sftpPolAttachInsert(dbDTO,sess,filePath,strAppId);	
	}*/
	
	public Policy setPolicyDetails(
			String strAssurerNRIC,
			String strProposerNRIC,
			String policyNum,
			Map<String,String> paymentModeMap,
			Map<String,String> paymentMethodMap,
			
			
			
			String strCustProposerId,
			String strCustAssurerId,
			String strPlanName,
			String strAgentCode){
		
		Policy policyObj = new Policy();
		
		  MasterProductLine mastPrdtLineObj  = new MasterProductLine(); 
		  MasterPrincipal mastPrinObj = new MasterPrincipal();

		  FpmsDistributor fpmsDisObj = new FpmsDistributor();
	      fpmsDisObj.setDistributorId(resource.getString("policy.distributor.id"));
	      mastPrdtLineObj.setProductLineId(resource.getString("policy.pl.lifetrad"));

	      CustomerDetails custDetProObj = new CustomerDetails();
	      CustomerDetails custDetAssrObj= new CustomerDetails();

	      custDetProObj.setCustid(strCustProposerId);
	      custDetAssrObj.setCustid(strCustAssurerId);

	      String strAdviserId = "";
     	 KycService kycServObj = new KycService();
     	 List<AdvstfCodes> lstAdvCodesDetails = kycServObj.getAdviserDetailsBasedOnCodes(resource.getString("policy.ntuc.principalid"),resource.getString("policy.distributor.id"),strAgentCode);
     	 if(lstAdvCodesDetails.size()>0){
     		for(AdvstfCodes advCodeObj:lstAdvCodesDetails) {
     			strAdviserId = advCodeObj.getAdviserStaff().getAdvstfId();
     		}
     	 }

	   
	         AdviserStaff advStfObj = new AdviserStaff();
	      
	         if(!KycUtils.nullOrBlank(strAdviserId)){
	        	 advStfObj.setAdvstfId(strAdviserId);
	         }else{
	        	 advStfObj.setAdvstfId(resource.getString("policy.advstaff.id"));
	         }


	    	 MasterPolicyStatus mastPolStatusObj = new MasterPolicyStatus();
	    	 mastPolStatusObj.setPolStatusId(resource.getString("policy.status.inforce"));

	    	 policyObj.setPolType((strAssurerNRIC.equalsIgnoreCase(strProposerNRIC))?resource.getString("policy.type.first"):resource.getString("policy.type.third"));
	    	 policyNum= "";
	         policyObj.setPolicyno(policyNum);
	         policyObj.setRegistrationDate(null);
	         policyObj.setEffDate(new Date());
	         policyObj.setRenewalDate(null);
	         policyObj.setRenewalReminderDate(null);
	         policyObj.setSubmDate(null);
	         policyObj.setTermDate(null);
	         policyObj.setEndDate(null);
	         policyObj.setCounterStopDate(null);
	         policyObj.setPolStatusDate(null);
	         policyObj.setPrevPolnoChngeDate(null);
	         policyObj.setSpTopupFlg("");
	         policyObj.setCpfFlg("");
	         policyObj.setGstFlg("");
	         policyObj.setAutoTranfFlg("");
	         policyObj.setAutoTranfAge((short) 0);
	         policyObj.setPaymentMode(paymentModeMap.get(" "));
	         policyObj.setPaymentMethod(paymentMethodMap.get(" "));
	         policyObj.setCurrencyType("");
	         policyObj.setCpfAccNo(null);
	         policyObj.setGiroFirstDeductDate(null);
	         policyObj.setGiroSecondDeductDate(null);
	         policyObj.setTermsAndCond(null);
	         policyObj.setRemarks("Policy Registered by NTUC-DSF on [ "+Utility.convertDateToDateTimeString(new Date())+" ] ");
	         policyObj.setCreatedDate(new Date());
	         policyObj.setCreatedBy("NTUC-STEP");
	         policyObj.setModifiedBy("");
//	         policyObj.setModifiedDate(null);
	      //   policyObj.setDistributorName("NTUC");
	         policyObj.setTotalSa(new Double(KycUtils.removeCommas("0")));
	         policyObj.setTotalPrem(new Double(KycUtils.removeCommas("0")));//1,000.00
	         
	        /* policyObj.setPolCorAddr1("");
	         policyObj.setPolCorAddr2("");
	         policyObj.setPolCorAddr3("");
	         policyObj.setPolCorCity("");
	         policyObj.setPolCorState("");
	         policyObj.setPolCorCountry("");
	         policyObj.setPolCorPostCde("");*/
	         policyObj.setDocDispatchDate(null);
	         policyObj.setTempAppidFpms06Dm("");
	         policyObj.setAvivaSplit("");
	         policyObj.setNtucPolicyId(null);//caseid
	         policyObj.setSubmDate(null);
	         policyObj.setIssueDate(null);
	         policyObj.setSourceData("");
	         policyObj.setDistributorName(resource.getString("policy.distributor.name"));
	         policyObj.setPaymentMethod(paymentMethodMap.get(" "));
	         mastPrinObj = new MasterPrincipal();
	         mastPrinObj.setPrinId(resource.getString("policy.ntuc.principalid"));
		     mastPrinObj.setPrinCode(resource.getString("policy.ntuc.principalcode"));
	         policyObj.setAdviserStaffByAdviserInitial(advStfObj);
	         policyObj.setAdviserStaffByAdviserCurrent(advStfObj);
	         policyObj.setAdviserStaffByAdviserComm(advStfObj);
	         policyObj.setFpmsDistributor(fpmsDisObj);
	         policyObj.setMasterProductLine(mastPrdtLineObj);
	         policyObj.setMasterPrincipal(mastPrinObj);
	         policyObj.setCustomerDetailsByPolicyOwner(custDetProObj);
	         policyObj.setCustomerDetailsByCustidProposed(custDetProObj);
	         policyObj.setCustomerDetailsByCustidAssured(custDetAssrObj);
	         policyObj.setMasterPolicyStatus(mastPolStatusObj);

	     
	     
	     

		return policyObj;
	}
	
	
	public void insertNTUCDocs(DBIntf dbDTO,List<String> insertQry){
		KycUtilsDB db=new KycUtilsDB();
		db.insertNTUCDocs(dbDTO,insertQry);	
	}
	
	

	public List<Object> getCaseStatusByCaseId(DBIntf dbDTO,ServletContext context,String strCaseId){
		KycUtilsDB db=new KycUtilsDB();
		return db.getCaseStatusByCaseId(dbDTO,context,strCaseId);	
	}
	
	public void updateFnaMailSentFlgByCaseId(DBIntf dbDTO,ServletContext context,String strCaseId,String strFnaId,String strMailSentFlg){
		KycUtilsDB db=new KycUtilsDB();
		db.updateFnaMailSentFlgByCaseId(dbDTO,context,strCaseId,strFnaId,strMailSentFlg);	
	}


	public List fetchNtucDocument(DBIntf dbDTO,HttpServletRequest req){
		KycUtilsAttchDB db=new KycUtilsAttchDB();
		return db.fetchNtucDocument(dbDTO, req);
	}
	
	public void insertNtucPolDets(DBIntf dbDTO,List<FnaNtucPolicyDets> polNtucDets) throws Exception{
		KycUtilsDB db=new KycUtilsDB();
		db.insertNtucPolDets(dbDTO,polNtucDets);	
	}
	
	public String uploadAdvApprovalDocs(DBIntf dbDTO,HttpServletRequest req,ArrayList<File> DocLst){
		KycUtilsAttchDB cDB = new KycUtilsAttchDB();
		return cDB.uploadAdvApprovalDocs(dbDTO,req,DocLst);
	}
	
//poovathi add on 02-04-2020
	
	public List fetchNRICDocument(DBIntf dbDTO,HttpServletRequest request){
		KycUtilsAttchDB db=new KycUtilsAttchDB();
		return db.fetchNRICDocument(dbDTO, request);
	}
	
	
	// get access level list
	
			 public List getGlobalAccessLvl(HttpServletRequest request) {
			    	HttpSession fpsess = request.getSession(false);
			    	 ArrayList lstAccessLvl = new ArrayList();
			    	 
			    	 Map<String,String> sessMap = (Map<String, String>) fpsess.getAttribute(KycConst.LOGGED_USER_INFO);
					 
//			        String strLoggedUser =	KycUtils.getCommonSessValue(request, KycConst.LOGGED_USER_ID) ;
			        
			        String strAgentStaffId = KycUtils.getCommonSessValue(request, KycConst.LOGGED_USER_ADVSTFID) ;
			        
//			        String strAgentStaffName =	KycUtils.getCommonSessValue(request, KycConst.LOGGED_USER_ADVSTFNAME) ;
			        
			        String strAgentStaffTyp = KycUtils.getCommonSessValue(request, KycConst.LOGGED_USER_STFTYPE) ;
			        
			        String strAgtStfCmpAccess = KycUtils.getCommonSessValue(request, KycConst.AGENT_REG_COMP_ACCESS) ;
			       
			        //int accessFlg=0;
			        FpmsService aserv = new FpmsService();
			        if(strAgentStaffTyp.equalsIgnoreCase("ADVISER")){
			        	
			        List advStfElem = aserv.srchGlblAccessLevel(strAgentStaffId);
			    	int accesLvlSize=advStfElem.size();
			    	String accessLvl=KycConst.STR_NULL_STRING;
			    	String mgrAccessFlg=KycConst.STR_NULL_STRING;
			    	String mgrQryVal=KycConst.STR_NULL_STRING;
			    	//fplog.info("ACCESS LEVL SIZE "+accesLvlSize);
			    	if(accesLvlSize>0){
			    		Iterator itr = advStfElem.iterator();
			    		 while (itr.hasNext()) {
			    			  Object[] row = (Object[])itr.next();
			    			  mgrAccessFlg=(String)row[0];
			    			 // fplog.info("mgrAccessFlgsss"+mgrAccessFlg);
			    			  accessLvl=(String)row[1];
			    		 }
			    	}
			   		//fplog.info("Acces level Object "+accessLvl);
			    	if(!Utility.nullOrBlank(accessLvl)){
			    	    String strArrMngrAcess[] =accessLvl.split(",");
			            int mngrLen = strArrMngrAcess.length;
			            for (int mngr = 0; mngr<mngrLen; mngr++) {
			            //	fplog.info("strArrMngrAcess[mngr]"+strArrMngrAcess[mngr]);
			                FPMSLabelValueDTO fpDto = new FPMSLabelValueDTO(strArrMngrAcess[mngr],strArrMngrAcess[mngr]);
			                lstAccessLvl.add(fpDto);
			    	 }
			       }
			    	
//			    	System.out.println(mgrAccessFlg+"mgrAccessFlg");
			    	if(!Utility.nullOrBlank(mgrAccessFlg)){
			    		if(mgrAccessFlg.equalsIgnoreCase("Y")){
			    			mgrQryVal=" Select ADVISERSTAFFID from ADVSTF_MANAGER CONNECT BY PRIOR ADVISERSTAFFID = MANAGER_ID START WITH ADVISERSTAFFID='"+strAgentStaffId+"' ";	
			    		}else{
			    		//	fplog.info("mgrAccessFlg"+mgrAccessFlg);
			    			mgrQryVal=" where brok.txtFldadvstfId='" + strAgentStaffId + "' ";
			    		}
			    		fpsess.setAttribute("GLBL_MGR_QRY", mgrQryVal);	
			    	}
			    }
			    if(strAgentStaffTyp.equalsIgnoreCase("STAFF")){
			    	String strcmpAccess = resource.getString("appln.glblAccessLvl.comp");
			    	FPMSLabelValueDTO fpDto = new FPMSLabelValueDTO(strcmpAccess,strcmpAccess);
			    	lstAccessLvl.add(fpDto);
			    }
			    if(!(strAgentStaffTyp.equalsIgnoreCase("STAFF")||strAgentStaffTyp.equalsIgnoreCase("ADVISER"))){
			    	//fplog.info(" INSIDE INTRODUCER OR PARAPLANNER ");
			    	String strAgntStfAccess = resource.getString("appln.glblAccessLvl.agnt");
			    	FPMSLabelValueDTO fpDto = new FPMSLabelValueDTO(strAgntStfAccess,strAgntStfAccess);
			    	lstAccessLvl.add(fpDto);
			    }
			    if(strAgentStaffTyp.equalsIgnoreCase("ADVISER")){
			    	//if((strAgtStfCmpAccess.equalsIgnoreCase("View.Change")||(strAgtStfCmpAccess.equalsIgnoreCase("View.Only")))&&(!(strAgentStaffTyp.equalsIgnoreCase("STAFF")))){
			    if((strAgtStfCmpAccess.equalsIgnoreCase("View.Change")||(strAgtStfCmpAccess.equalsIgnoreCase("View.Only")))){
			    	//fplog.info(" INSIDE COMPANY LEVEL ACCESS ");
			    	String strcmpAccess = resource.getString("appln.glblAccessLvl.comp");
			    	FPMSLabelValueDTO fpDto = new FPMSLabelValueDTO(strcmpAccess,strcmpAccess);
			    	lstAccessLvl.add(fpDto);
			    }
			    } 
			 	return lstAccessLvl;
			  }// End getAccessLvl
	
}
