package com.avallis.ekyc.dto;

import java.io.Serializable;


public class KeyValueDTO implements Serializable {
    String Key;
    String Value;

    public KeyValueDTO() {

    }

    public KeyValueDTO(String strKey, String strValue) {
        this.Key = strKey;
        this.Value = strValue;
    }

    public String getKey() {
        return this.Key;
    }

    public String getValue() {
        return this.Value;
    }

    public void setKey(String strKey) {
        this.Key = strKey;
    }

    public void setValue(String strValue) {
        this.Value = strValue;
    }


}
