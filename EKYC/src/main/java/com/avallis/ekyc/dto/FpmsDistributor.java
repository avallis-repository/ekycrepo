package com.avallis.ekyc.dto;

// Generated Mar 13, 2018 9:17:21 PM by Hibernate Tools 4.0.0

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreType;

/**
 * FpmsDistributor generated by hbm2java
 */
@JsonIgnoreType
public class FpmsDistributor implements java.io.Serializable {

	private String distributorId;
	private String mnemonic;
	private String distrbutorName;
	private String addr1;
	private String addr2;
	private String addr3;
	private String city;
	private String state;
	private String country;
	private String pincode;
	private String telephoneOff;
	private String telephoneRes;
	private String faxNo;
	private String emailId;
	private String contactRef;
	private String registrationNo;
	private String type;
	private Date createdDate;
	private String createdBy;
	private String modifiedBy;
	private Date modifiedDate;
	private String parentYn;
	private String parentName;
	private String previousName;
	private String namePeriodFrom;
	private String namePeriodTo;
	private String loginAppear;
	private String gstFlg;
	private String website;
	/*private Set<ProdLobTransDets> prodLobTransDetses = new HashSet<ProdLobTransDets>(
			0);
	private Set<PolMedTest> polMedTests = new HashSet<PolMedTest>(0);
	private Set<MasterCommSchedule> masterCommSchedules = new HashSet<MasterCommSchedule>(
			0);
	private Set<PolPaymentTracking> polPaymentTrackings = new HashSet<PolPaymentTracking>(
			0);
	private Set<PolLobDistDets> polLobDistDetsesForLobDistributorId = new HashSet<PolLobDistDets>(
			0);
	private Set<AdvstfIntroWrkwith> advstfIntroWrkwiths = new HashSet<AdvstfIntroWrkwith>(
			0);
	private Set<PolUnderwrite> polUnderwrites = new HashSet<PolUnderwrite>(0);
	private Set<Policy> policies = new HashSet<Policy>(0);
	private Set<AdvstfDistWrkwith> advstfDistWrkwithsForWrkwithDistId = new HashSet<AdvstfDistWrkwith>(
			0);
	private Set<CustomerNotes> customerNoteses = new HashSet<CustomerNotes>(0);
	private Set<AdvstfProxyMgrDets> advstfProxyMgrDetses = new HashSet<AdvstfProxyMgrDets>(
			0);
	private Set<CommOtherIncome> commOtherIncomes = new HashSet<CommOtherIncome>(
			0);
	private Set<CommRecoverable> commRecoverables = new HashSet<CommRecoverable>(
			0);
	private Set<AdvstfPrdRecDets> advstfPrdRecDetses = new HashSet<AdvstfPrdRecDets>(
			0);
	private Set<KpiTrack> kpiTracks = new HashSet<KpiTrack>(0);
	private Set<CommScreeningDets> commScreeningDetses = new HashSet<CommScreeningDets>(
			0);
	private Set<PolLoanIntrstDets> polLoanIntrstDetses = new HashSet<PolLoanIntrstDets>(
			0);
	private Set<AdvstfCodes> advstfCodeses = new HashSet<AdvstfCodes>(0);
	private Set<PolLandbanking> polLandbankings = new HashSet<PolLandbanking>(0);
	private Set<PolBenefDets> polBenefDetses = new HashSet<PolBenefDets>(0);
	private Set<KpiTargtMembers> kpiTargtMemberses = new HashSet<KpiTargtMembers>(
			0);
	private Set<PolTrusteeMgrDets> polTrusteeMgrDetses = new HashSet<PolTrusteeMgrDets>(
			0);
	private Set<Commstmt> commstmts = new HashSet<Commstmt>(0);
	private Set<DocHandling> docHandlings = new HashSet<DocHandling>(0);
	private Set<AdvstfDistWrkwith> advstfDistWrkwithsForDistributorId = new HashSet<AdvstfDistWrkwith>(
			0);
	private Set<AdvstfIntroPotarea> advstfIntroPotareas = new HashSet<AdvstfIntroPotarea>(
			0);
	private Set<PolLoanDets> polLoanDetses = new HashSet<PolLoanDets>(0);
	private Set<AdvstfIntroPerfTrgt> advstfIntroPerfTrgts = new HashSet<AdvstfIntroPerfTrgt>(
			0);
	private Set<FpuserTest> fpuserTests = new HashSet<FpuserTest>(0);
	private Set<AdviserStaff> adviserStaffsForDistributorId = new HashSet<AdviserStaff>(
			0);
	private Set<CustomerLobDets> customerLobDetses = new HashSet<CustomerLobDets>(
			0);
	private Set<ProductDistributor> productDistributors = new HashSet<ProductDistributor>(
			0);
	private Set<PolAdviserShare> polAdviserShares = new HashSet<PolAdviserShare>(
			0);
	private Set<CustomerAmlHistDets> customerAmlHistDetses = new HashSet<CustomerAmlHistDets>(
			0);
	private Set<CustomerFamilyparticulars> customerFamilyparticularses = new HashSet<CustomerFamilyparticulars>(
			0);
	private Set<PolAttachments> polAttachmentses = new HashSet<PolAttachments>(
			0);
	private Set<Fpuser> fpusers = new HashSet<Fpuser>(0);
	private Set<MasterDesignation> masterDesignations = new HashSet<MasterDesignation>(
			0);
	private Set<KpiTarget> kpiTargets = new HashSet<KpiTarget>(0);
	private Set<PolLobDistDets> polLobDistDetsesForDistributorId = new HashSet<PolLobDistDets>(
			0);
	private Set<AdvstfVestInfo> advstfVestInfos = new HashSet<AdvstfVestInfo>(0);
	private Set<CustomerFinancialServices> customerFinancialServiceses = new HashSet<CustomerFinancialServices>(
			0);
	private Set<PolWillstrustsPlanDets> polWillstrustsPlanDetses = new HashSet<PolWillstrustsPlanDets>(
			0);
	private Set<CustomerAlterations> customerAlterationses = new HashSet<CustomerAlterations>(
			0);
	private Set<CpdHrsScheduler> cpdHrsSchedulers = new HashSet<CpdHrsScheduler>(
			0);
	private Set<KpiTargetTrans> kpiTargetTranses = new HashSet<KpiTargetTrans>(
			0);
	private Set<AdvstfEduDets> advstfEduDetses = new HashSet<AdvstfEduDets>(0);
	private Set<PolAssignment> polAssignments = new HashSet<PolAssignment>(0);
	private Set<PolIlpfundredeem> polIlpfundredeems = new HashSet<PolIlpfundredeem>(
			0);
	private Set<AdviserStaff> adviserStaffsForBaseDistId = new HashSet<AdviserStaff>(
			0);
	private Set<AdvstfIntroCommsplit> advstfIntroCommsplits = new HashSet<AdvstfIntroCommsplit>(
			0);
	private Set<FnaHistoryDetails> fnaHistoryDetailses = new HashSet<FnaHistoryDetails>(
			0);
	private Set<PolInsliferider> polInsliferiders = new HashSet<PolInsliferider>(
			0);
	private Set<CustomerCoreDetails> customerCoreDetailses = new HashSet<CustomerCoreDetails>(
			0);
	private Set<CommAmmendments> commAmmendmentses = new HashSet<CommAmmendments>(
			0);
	private Set<PolInslifebasic> polInslifebasics = new HashSet<PolInslifebasic>(
			0);
	private Set<FpuserDistWrkwith> fpuserDistWrkwiths = new HashSet<FpuserDistWrkwith>(
			0);
	private Set<PolAlterations> polAlterationses = new HashSet<PolAlterations>(
			0);
	private Set<PolIlpfunds> polIlpfundses = new HashSet<PolIlpfunds>(0);
	private Set<CommstmtRiders> commstmtRiderses = new HashSet<CommstmtRiders>(
			0);
	private Set<AdvstfLicenseDets> advstfLicenseDetses = new HashSet<AdvstfLicenseDets>(
			0);
	private Set<AdvstfAttachments> advstfAttachmentses = new HashSet<AdvstfAttachments>(
			0);
	private Set<CustomerAttachments> customerAttachmentses = new HashSet<CustomerAttachments>(
			0);
	private Set<CustomerMktAreaOfInterests> customerMktAreaOfInterestses = new HashSet<CustomerMktAreaOfInterests>(
			0);
	private Set<CustomerDetails> customerDetailses = new HashSet<CustomerDetails>(
			0);
	private Set<AdvstfEmplymntHistory> advstfEmplymntHistories = new HashSet<AdvstfEmplymntHistory>(
			0);
	private Set<KpiTrackTrans> kpiTrackTranses = new HashSet<KpiTrackTrans>(0);
	private Set<CommGenDets> commGenDetses = new HashSet<CommGenDets>(0);
	private Set<PolTrusteeDets> polTrusteeDetses = new HashSet<PolTrusteeDets>(
			0);
	private Set<AdvstfIntroDets> advstfIntroDetses = new HashSet<AdvstfIntroDets>(
			0);
	private Set<PolLndBnkUntDets> polLndBnkUntDetses = new HashSet<PolLndBnkUntDets>(
			0);
	private Set<PolIlptopup> polIlptopups = new HashSet<PolIlptopup>(0);
	private Set<FeedbackDetails> feedbackDetailses = new HashSet<FeedbackDetails>(
			0);
	private Set<CustomerKeyperson> customerKeypersons = new HashSet<CustomerKeyperson>(
			0);
	private Set<PolWillstrustsDets> polWillstrustsDetses = new HashSet<PolWillstrustsDets>(
			0);
	private Set<CustomerRelDets> customerRelDetses = new HashSet<CustomerRelDets>(
			0);*/

	public FpmsDistributor() {
	}

	public FpmsDistributor(String distributorId, String mnemonic,
			String distrbutorName, String loginAppear) {
		this.distributorId = distributorId;
		this.mnemonic = mnemonic;
		this.distrbutorName = distrbutorName;
		this.loginAppear = loginAppear;
	}

	public FpmsDistributor(String distributorId, String mnemonic,
			String distrbutorName, String addr1, String addr2, String addr3,
			String city, String state, String country, String pincode,
			String telephoneOff, String telephoneRes, String faxNo,
			String emailId, String contactRef, String registrationNo,
			String type, Date createdDate, String createdBy, String modifiedBy,
			Date modifiedDate, String parentYn, String parentName,
			String previousName, String namePeriodFrom, String namePeriodTo,
			String loginAppear, String gstFlg, String website
			/*,Set<ProdLobTransDets> prodLobTransDetses,
			Set<PolMedTest> polMedTests,
			Set<MasterCommSchedule> masterCommSchedules,
			Set<PolPaymentTracking> polPaymentTrackings,
			Set<PolLobDistDets> polLobDistDetsesForLobDistributorId,
			Set<AdvstfIntroWrkwith> advstfIntroWrkwiths,
			Set<PolUnderwrite> polUnderwrites, Set<Policy> policies,
			Set<AdvstfDistWrkwith> advstfDistWrkwithsForWrkwithDistId,
			Set<CustomerNotes> customerNoteses,
			Set<AdvstfProxyMgrDets> advstfProxyMgrDetses,
			Set<CommOtherIncome> commOtherIncomes,
			Set<CommRecoverable> commRecoverables,
			Set<AdvstfPrdRecDets> advstfPrdRecDetses, Set<KpiTrack> kpiTracks,
			Set<CommScreeningDets> commScreeningDetses,
			Set<PolLoanIntrstDets> polLoanIntrstDetses,
			Set<AdvstfCodes> advstfCodeses,
			Set<PolLandbanking> polLandbankings,
			Set<PolBenefDets> polBenefDetses,
			Set<KpiTargtMembers> kpiTargtMemberses,
			Set<PolTrusteeMgrDets> polTrusteeMgrDetses,
			Set<Commstmt> commstmts, Set<DocHandling> docHandlings,
			Set<AdvstfDistWrkwith> advstfDistWrkwithsForDistributorId,
			Set<AdvstfIntroPotarea> advstfIntroPotareas,
			Set<PolLoanDets> polLoanDetses,
			Set<AdvstfIntroPerfTrgt> advstfIntroPerfTrgts,
			Set<FpuserTest> fpuserTests,
			Set<AdviserStaff> adviserStaffsForDistributorId,
			Set<CustomerLobDets> customerLobDetses,
			Set<ProductDistributor> productDistributors,
			Set<PolAdviserShare> polAdviserShares,
			Set<CustomerAmlHistDets> customerAmlHistDetses,
			Set<CustomerFamilyparticulars> customerFamilyparticularses,
			Set<PolAttachments> polAttachmentses, Set<Fpuser> fpusers,
			Set<MasterDesignation> masterDesignations,
			Set<KpiTarget> kpiTargets,
			Set<PolLobDistDets> polLobDistDetsesForDistributorId,
			Set<AdvstfVestInfo> advstfVestInfos,
			Set<CustomerFinancialServices> customerFinancialServiceses,
			Set<PolWillstrustsPlanDets> polWillstrustsPlanDetses,
			Set<CustomerAlterations> customerAlterationses,
			Set<CpdHrsScheduler> cpdHrsSchedulers,
			Set<KpiTargetTrans> kpiTargetTranses,
			Set<AdvstfEduDets> advstfEduDetses,
			Set<PolAssignment> polAssignments,
			Set<PolIlpfundredeem> polIlpfundredeems,
			Set<AdviserStaff> adviserStaffsForBaseDistId,
			Set<AdvstfIntroCommsplit> advstfIntroCommsplits,
			Set<FnaHistoryDetails> fnaHistoryDetailses,
			Set<PolInsliferider> polInsliferiders,
			Set<CustomerCoreDetails> customerCoreDetailses,
			Set<CommAmmendments> commAmmendmentses,
			Set<PolInslifebasic> polInslifebasics,
			Set<FpuserDistWrkwith> fpuserDistWrkwiths,
			Set<PolAlterations> polAlterationses,
			Set<PolIlpfunds> polIlpfundses,
			Set<CommstmtRiders> commstmtRiderses,
			Set<AdvstfLicenseDets> advstfLicenseDetses,
			Set<AdvstfAttachments> advstfAttachmentses,
			Set<CustomerAttachments> customerAttachmentses,
			Set<CustomerMktAreaOfInterests> customerMktAreaOfInterestses,
			Set<CustomerDetails> customerDetailses,
			Set<AdvstfEmplymntHistory> advstfEmplymntHistories,
			Set<KpiTrackTrans> kpiTrackTranses, Set<CommGenDets> commGenDetses,
			Set<PolTrusteeDets> polTrusteeDetses,
			Set<AdvstfIntroDets> advstfIntroDetses,
			Set<PolLndBnkUntDets> polLndBnkUntDetses,
			Set<PolIlptopup> polIlptopups,
			Set<FeedbackDetails> feedbackDetailses,
			Set<CustomerKeyperson> customerKeypersons,
			Set<PolWillstrustsDets> polWillstrustsDetses,
			Set<CustomerRelDets> customerRelDetses*/) {
		this.distributorId = distributorId;
		this.mnemonic = mnemonic;
		this.distrbutorName = distrbutorName;
		this.addr1 = addr1;
		this.addr2 = addr2;
		this.addr3 = addr3;
		this.city = city;
		this.state = state;
		this.country = country;
		this.pincode = pincode;
		this.telephoneOff = telephoneOff;
		this.telephoneRes = telephoneRes;
		this.faxNo = faxNo;
		this.emailId = emailId;
		this.contactRef = contactRef;
		this.registrationNo = registrationNo;
		this.type = type;
		this.createdDate = createdDate;
		this.createdBy = createdBy;
		this.modifiedBy = modifiedBy;
		this.modifiedDate = modifiedDate;
		this.parentYn = parentYn;
		this.parentName = parentName;
		this.previousName = previousName;
		this.namePeriodFrom = namePeriodFrom;
		this.namePeriodTo = namePeriodTo;
		this.loginAppear = loginAppear;
		this.gstFlg = gstFlg;
		this.website = website;
		/*this.prodLobTransDetses = prodLobTransDetses;
		this.polMedTests = polMedTests;
		this.masterCommSchedules = masterCommSchedules;
		this.polPaymentTrackings = polPaymentTrackings;
		this.polLobDistDetsesForLobDistributorId = polLobDistDetsesForLobDistributorId;
		this.advstfIntroWrkwiths = advstfIntroWrkwiths;
		this.polUnderwrites = polUnderwrites;
		this.policies = policies;
		this.advstfDistWrkwithsForWrkwithDistId = advstfDistWrkwithsForWrkwithDistId;
		this.customerNoteses = customerNoteses;
		this.advstfProxyMgrDetses = advstfProxyMgrDetses;
		this.commOtherIncomes = commOtherIncomes;
		this.commRecoverables = commRecoverables;
		this.advstfPrdRecDetses = advstfPrdRecDetses;
		this.kpiTracks = kpiTracks;
		this.commScreeningDetses = commScreeningDetses;
		this.polLoanIntrstDetses = polLoanIntrstDetses;
		this.advstfCodeses = advstfCodeses;
		this.polLandbankings = polLandbankings;
		this.polBenefDetses = polBenefDetses;
		this.kpiTargtMemberses = kpiTargtMemberses;
		this.polTrusteeMgrDetses = polTrusteeMgrDetses;
		this.commstmts = commstmts;
		this.docHandlings = docHandlings;
		this.advstfDistWrkwithsForDistributorId = advstfDistWrkwithsForDistributorId;
		this.advstfIntroPotareas = advstfIntroPotareas;
		this.polLoanDetses = polLoanDetses;
		this.advstfIntroPerfTrgts = advstfIntroPerfTrgts;
		this.fpuserTests = fpuserTests;
		this.adviserStaffsForDistributorId = adviserStaffsForDistributorId;
		this.customerLobDetses = customerLobDetses;
		this.productDistributors = productDistributors;
		this.polAdviserShares = polAdviserShares;
		this.customerAmlHistDetses = customerAmlHistDetses;
		this.customerFamilyparticularses = customerFamilyparticularses;
		this.polAttachmentses = polAttachmentses;
		this.fpusers = fpusers;
		this.masterDesignations = masterDesignations;
		this.kpiTargets = kpiTargets;
		this.polLobDistDetsesForDistributorId = polLobDistDetsesForDistributorId;
		this.advstfVestInfos = advstfVestInfos;
		this.customerFinancialServiceses = customerFinancialServiceses;
		this.polWillstrustsPlanDetses = polWillstrustsPlanDetses;
		this.customerAlterationses = customerAlterationses;
		this.cpdHrsSchedulers = cpdHrsSchedulers;
		this.kpiTargetTranses = kpiTargetTranses;
		this.advstfEduDetses = advstfEduDetses;
		this.polAssignments = polAssignments;
		this.polIlpfundredeems = polIlpfundredeems;
		this.adviserStaffsForBaseDistId = adviserStaffsForBaseDistId;
		this.advstfIntroCommsplits = advstfIntroCommsplits;
		this.fnaHistoryDetailses = fnaHistoryDetailses;
		this.polInsliferiders = polInsliferiders;
		this.customerCoreDetailses = customerCoreDetailses;
		this.commAmmendmentses = commAmmendmentses;
		this.polInslifebasics = polInslifebasics;
		this.fpuserDistWrkwiths = fpuserDistWrkwiths;
		this.polAlterationses = polAlterationses;
		this.polIlpfundses = polIlpfundses;
		this.commstmtRiderses = commstmtRiderses;
		this.advstfLicenseDetses = advstfLicenseDetses;
		this.advstfAttachmentses = advstfAttachmentses;
		this.customerAttachmentses = customerAttachmentses;
		this.customerMktAreaOfInterestses = customerMktAreaOfInterestses;
		this.customerDetailses = customerDetailses;
		this.advstfEmplymntHistories = advstfEmplymntHistories;
		this.kpiTrackTranses = kpiTrackTranses;
		this.commGenDetses = commGenDetses;
		this.polTrusteeDetses = polTrusteeDetses;
		this.advstfIntroDetses = advstfIntroDetses;
		this.polLndBnkUntDetses = polLndBnkUntDetses;
		this.polIlptopups = polIlptopups;
		this.feedbackDetailses = feedbackDetailses;
		this.customerKeypersons = customerKeypersons;
		this.polWillstrustsDetses = polWillstrustsDetses;
		this.customerRelDetses = customerRelDetses;*/
	}

	public String getDistributorId() {
		return this.distributorId;
	}

	public void setDistributorId(String distributorId) {
		this.distributorId = distributorId;
	}

	public String getMnemonic() {
		return this.mnemonic;
	}

	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	public String getDistrbutorName() {
		return this.distrbutorName;
	}

	public void setDistrbutorName(String distrbutorName) {
		this.distrbutorName = distrbutorName;
	}

	public String getAddr1() {
		return this.addr1;
	}

	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}

	public String getAddr2() {
		return this.addr2;
	}

	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}

	public String getAddr3() {
		return this.addr3;
	}

	public void setAddr3(String addr3) {
		this.addr3 = addr3;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPincode() {
		return this.pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getTelephoneOff() {
		return this.telephoneOff;
	}

	public void setTelephoneOff(String telephoneOff) {
		this.telephoneOff = telephoneOff;
	}

	public String getTelephoneRes() {
		return this.telephoneRes;
	}

	public void setTelephoneRes(String telephoneRes) {
		this.telephoneRes = telephoneRes;
	}

	public String getFaxNo() {
		return this.faxNo;
	}

	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}

	public String getEmailId() {
		return this.emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getContactRef() {
		return this.contactRef;
	}

	public void setContactRef(String contactRef) {
		this.contactRef = contactRef;
	}

	public String getRegistrationNo() {
		return this.registrationNo;
	}

	public void setRegistrationNo(String registrationNo) {
		this.registrationNo = registrationNo;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getParentYn() {
		return this.parentYn;
	}

	public void setParentYn(String parentYn) {
		this.parentYn = parentYn;
	}

	public String getParentName() {
		return this.parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getPreviousName() {
		return this.previousName;
	}

	public void setPreviousName(String previousName) {
		this.previousName = previousName;
	}

	public String getNamePeriodFrom() {
		return this.namePeriodFrom;
	}

	public void setNamePeriodFrom(String namePeriodFrom) {
		this.namePeriodFrom = namePeriodFrom;
	}

	public String getNamePeriodTo() {
		return this.namePeriodTo;
	}

	public void setNamePeriodTo(String namePeriodTo) {
		this.namePeriodTo = namePeriodTo;
	}

	public String getLoginAppear() {
		return this.loginAppear;
	}

	public void setLoginAppear(String loginAppear) {
		this.loginAppear = loginAppear;
	}

	public String getGstFlg() {
		return this.gstFlg;
	}

	public void setGstFlg(String gstFlg) {
		this.gstFlg = gstFlg;
	}

	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}
	/*
	public Set<ProdLobTransDets> getProdLobTransDetses() {
		return this.prodLobTransDetses;
	}

	public void setProdLobTransDetses(Set<ProdLobTransDets> prodLobTransDetses) {
		this.prodLobTransDetses = prodLobTransDetses;
	}

	public Set<PolMedTest> getPolMedTests() {
		return this.polMedTests;
	}

	public void setPolMedTests(Set<PolMedTest> polMedTests) {
		this.polMedTests = polMedTests;
	}

	public Set<MasterCommSchedule> getMasterCommSchedules() {
		return this.masterCommSchedules;
	}

	public void setMasterCommSchedules(
			Set<MasterCommSchedule> masterCommSchedules) {
		this.masterCommSchedules = masterCommSchedules;
	}

	public Set<PolPaymentTracking> getPolPaymentTrackings() {
		return this.polPaymentTrackings;
	}

	public void setPolPaymentTrackings(
			Set<PolPaymentTracking> polPaymentTrackings) {
		this.polPaymentTrackings = polPaymentTrackings;
	}

	public Set<PolLobDistDets> getPolLobDistDetsesForLobDistributorId() {
		return this.polLobDistDetsesForLobDistributorId;
	}

	public void setPolLobDistDetsesForLobDistributorId(
			Set<PolLobDistDets> polLobDistDetsesForLobDistributorId) {
		this.polLobDistDetsesForLobDistributorId = polLobDistDetsesForLobDistributorId;
	}

	public Set<AdvstfIntroWrkwith> getAdvstfIntroWrkwiths() {
		return this.advstfIntroWrkwiths;
	}

	public void setAdvstfIntroWrkwiths(
			Set<AdvstfIntroWrkwith> advstfIntroWrkwiths) {
		this.advstfIntroWrkwiths = advstfIntroWrkwiths;
	}

	public Set<PolUnderwrite> getPolUnderwrites() {
		return this.polUnderwrites;
	}

	public void setPolUnderwrites(Set<PolUnderwrite> polUnderwrites) {
		this.polUnderwrites = polUnderwrites;
	}

	public Set<Policy> getPolicies() {
		return this.policies;
	}

	public void setPolicies(Set<Policy> policies) {
		this.policies = policies;
	}

	public Set<AdvstfDistWrkwith> getAdvstfDistWrkwithsForWrkwithDistId() {
		return this.advstfDistWrkwithsForWrkwithDistId;
	}

	public void setAdvstfDistWrkwithsForWrkwithDistId(
			Set<AdvstfDistWrkwith> advstfDistWrkwithsForWrkwithDistId) {
		this.advstfDistWrkwithsForWrkwithDistId = advstfDistWrkwithsForWrkwithDistId;
	}

	public Set<CustomerNotes> getCustomerNoteses() {
		return this.customerNoteses;
	}

	public void setCustomerNoteses(Set<CustomerNotes> customerNoteses) {
		this.customerNoteses = customerNoteses;
	}

	public Set<AdvstfProxyMgrDets> getAdvstfProxyMgrDetses() {
		return this.advstfProxyMgrDetses;
	}

	public void setAdvstfProxyMgrDetses(
			Set<AdvstfProxyMgrDets> advstfProxyMgrDetses) {
		this.advstfProxyMgrDetses = advstfProxyMgrDetses;
	}

	public Set<CommOtherIncome> getCommOtherIncomes() {
		return this.commOtherIncomes;
	}

	public void setCommOtherIncomes(Set<CommOtherIncome> commOtherIncomes) {
		this.commOtherIncomes = commOtherIncomes;
	}

	public Set<CommRecoverable> getCommRecoverables() {
		return this.commRecoverables;
	}

	public void setCommRecoverables(Set<CommRecoverable> commRecoverables) {
		this.commRecoverables = commRecoverables;
	}

	public Set<AdvstfPrdRecDets> getAdvstfPrdRecDetses() {
		return this.advstfPrdRecDetses;
	}

	public void setAdvstfPrdRecDetses(Set<AdvstfPrdRecDets> advstfPrdRecDetses) {
		this.advstfPrdRecDetses = advstfPrdRecDetses;
	}

	public Set<KpiTrack> getKpiTracks() {
		return this.kpiTracks;
	}

	public void setKpiTracks(Set<KpiTrack> kpiTracks) {
		this.kpiTracks = kpiTracks;
	}

	public Set<CommScreeningDets> getCommScreeningDetses() {
		return this.commScreeningDetses;
	}

	public void setCommScreeningDetses(
			Set<CommScreeningDets> commScreeningDetses) {
		this.commScreeningDetses = commScreeningDetses;
	}

	public Set<PolLoanIntrstDets> getPolLoanIntrstDetses() {
		return this.polLoanIntrstDetses;
	}

	public void setPolLoanIntrstDetses(
			Set<PolLoanIntrstDets> polLoanIntrstDetses) {
		this.polLoanIntrstDetses = polLoanIntrstDetses;
	}

	public Set<AdvstfCodes> getAdvstfCodeses() {
		return this.advstfCodeses;
	}

	public void setAdvstfCodeses(Set<AdvstfCodes> advstfCodeses) {
		this.advstfCodeses = advstfCodeses;
	}

	public Set<PolLandbanking> getPolLandbankings() {
		return this.polLandbankings;
	}

	public void setPolLandbankings(Set<PolLandbanking> polLandbankings) {
		this.polLandbankings = polLandbankings;
	}

	public Set<PolBenefDets> getPolBenefDetses() {
		return this.polBenefDetses;
	}

	public void setPolBenefDetses(Set<PolBenefDets> polBenefDetses) {
		this.polBenefDetses = polBenefDetses;
	}

	public Set<KpiTargtMembers> getKpiTargtMemberses() {
		return this.kpiTargtMemberses;
	}

	public void setKpiTargtMemberses(Set<KpiTargtMembers> kpiTargtMemberses) {
		this.kpiTargtMemberses = kpiTargtMemberses;
	}

	public Set<PolTrusteeMgrDets> getPolTrusteeMgrDetses() {
		return this.polTrusteeMgrDetses;
	}

	public void setPolTrusteeMgrDetses(
			Set<PolTrusteeMgrDets> polTrusteeMgrDetses) {
		this.polTrusteeMgrDetses = polTrusteeMgrDetses;
	}

	public Set<Commstmt> getCommstmts() {
		return this.commstmts;
	}

	public void setCommstmts(Set<Commstmt> commstmts) {
		this.commstmts = commstmts;
	}

	public Set<DocHandling> getDocHandlings() {
		return this.docHandlings;
	}

	public void setDocHandlings(Set<DocHandling> docHandlings) {
		this.docHandlings = docHandlings;
	}

	public Set<AdvstfDistWrkwith> getAdvstfDistWrkwithsForDistributorId() {
		return this.advstfDistWrkwithsForDistributorId;
	}

	public void setAdvstfDistWrkwithsForDistributorId(
			Set<AdvstfDistWrkwith> advstfDistWrkwithsForDistributorId) {
		this.advstfDistWrkwithsForDistributorId = advstfDistWrkwithsForDistributorId;
	}

	public Set<AdvstfIntroPotarea> getAdvstfIntroPotareas() {
		return this.advstfIntroPotareas;
	}

	public void setAdvstfIntroPotareas(
			Set<AdvstfIntroPotarea> advstfIntroPotareas) {
		this.advstfIntroPotareas = advstfIntroPotareas;
	}

	public Set<PolLoanDets> getPolLoanDetses() {
		return this.polLoanDetses;
	}

	public void setPolLoanDetses(Set<PolLoanDets> polLoanDetses) {
		this.polLoanDetses = polLoanDetses;
	}

	public Set<AdvstfIntroPerfTrgt> getAdvstfIntroPerfTrgts() {
		return this.advstfIntroPerfTrgts;
	}

	public void setAdvstfIntroPerfTrgts(
			Set<AdvstfIntroPerfTrgt> advstfIntroPerfTrgts) {
		this.advstfIntroPerfTrgts = advstfIntroPerfTrgts;
	}

	public Set<FpuserTest> getFpuserTests() {
		return this.fpuserTests;
	}

	public void setFpuserTests(Set<FpuserTest> fpuserTests) {
		this.fpuserTests = fpuserTests;
	}

	public Set<AdviserStaff> getAdviserStaffsForDistributorId() {
		return this.adviserStaffsForDistributorId;
	}

	public void setAdviserStaffsForDistributorId(
			Set<AdviserStaff> adviserStaffsForDistributorId) {
		this.adviserStaffsForDistributorId = adviserStaffsForDistributorId;
	}

	public Set<CustomerLobDets> getCustomerLobDetses() {
		return this.customerLobDetses;
	}

	public void setCustomerLobDetses(Set<CustomerLobDets> customerLobDetses) {
		this.customerLobDetses = customerLobDetses;
	}

	public Set<ProductDistributor> getProductDistributors() {
		return this.productDistributors;
	}

	public void setProductDistributors(
			Set<ProductDistributor> productDistributors) {
		this.productDistributors = productDistributors;
	}

	public Set<PolAdviserShare> getPolAdviserShares() {
		return this.polAdviserShares;
	}

	public void setPolAdviserShares(Set<PolAdviserShare> polAdviserShares) {
		this.polAdviserShares = polAdviserShares;
	}

	public Set<CustomerAmlHistDets> getCustomerAmlHistDetses() {
		return this.customerAmlHistDetses;
	}

	public void setCustomerAmlHistDetses(
			Set<CustomerAmlHistDets> customerAmlHistDetses) {
		this.customerAmlHistDetses = customerAmlHistDetses;
	}

	public Set<CustomerFamilyparticulars> getCustomerFamilyparticularses() {
		return this.customerFamilyparticularses;
	}

	public void setCustomerFamilyparticularses(
			Set<CustomerFamilyparticulars> customerFamilyparticularses) {
		this.customerFamilyparticularses = customerFamilyparticularses;
	}

	public Set<PolAttachments> getPolAttachmentses() {
		return this.polAttachmentses;
	}

	public void setPolAttachmentses(Set<PolAttachments> polAttachmentses) {
		this.polAttachmentses = polAttachmentses;
	}

	public Set<Fpuser> getFpusers() {
		return this.fpusers;
	}

	public void setFpusers(Set<Fpuser> fpusers) {
		this.fpusers = fpusers;
	}

	public Set<MasterDesignation> getMasterDesignations() {
		return this.masterDesignations;
	}

	public void setMasterDesignations(Set<MasterDesignation> masterDesignations) {
		this.masterDesignations = masterDesignations;
	}

	public Set<KpiTarget> getKpiTargets() {
		return this.kpiTargets;
	}

	public void setKpiTargets(Set<KpiTarget> kpiTargets) {
		this.kpiTargets = kpiTargets;
	}

	public Set<PolLobDistDets> getPolLobDistDetsesForDistributorId() {
		return this.polLobDistDetsesForDistributorId;
	}

	public void setPolLobDistDetsesForDistributorId(
			Set<PolLobDistDets> polLobDistDetsesForDistributorId) {
		this.polLobDistDetsesForDistributorId = polLobDistDetsesForDistributorId;
	}

	public Set<AdvstfVestInfo> getAdvstfVestInfos() {
		return this.advstfVestInfos;
	}

	public void setAdvstfVestInfos(Set<AdvstfVestInfo> advstfVestInfos) {
		this.advstfVestInfos = advstfVestInfos;
	}

	public Set<CustomerFinancialServices> getCustomerFinancialServiceses() {
		return this.customerFinancialServiceses;
	}

	public void setCustomerFinancialServiceses(
			Set<CustomerFinancialServices> customerFinancialServiceses) {
		this.customerFinancialServiceses = customerFinancialServiceses;
	}

	public Set<PolWillstrustsPlanDets> getPolWillstrustsPlanDetses() {
		return this.polWillstrustsPlanDetses;
	}

	public void setPolWillstrustsPlanDetses(
			Set<PolWillstrustsPlanDets> polWillstrustsPlanDetses) {
		this.polWillstrustsPlanDetses = polWillstrustsPlanDetses;
	}

	public Set<CustomerAlterations> getCustomerAlterationses() {
		return this.customerAlterationses;
	}

	public void setCustomerAlterationses(
			Set<CustomerAlterations> customerAlterationses) {
		this.customerAlterationses = customerAlterationses;
	}

	public Set<CpdHrsScheduler> getCpdHrsSchedulers() {
		return this.cpdHrsSchedulers;
	}

	public void setCpdHrsSchedulers(Set<CpdHrsScheduler> cpdHrsSchedulers) {
		this.cpdHrsSchedulers = cpdHrsSchedulers;
	}

	public Set<KpiTargetTrans> getKpiTargetTranses() {
		return this.kpiTargetTranses;
	}

	public void setKpiTargetTranses(Set<KpiTargetTrans> kpiTargetTranses) {
		this.kpiTargetTranses = kpiTargetTranses;
	}

	public Set<AdvstfEduDets> getAdvstfEduDetses() {
		return this.advstfEduDetses;
	}

	public void setAdvstfEduDetses(Set<AdvstfEduDets> advstfEduDetses) {
		this.advstfEduDetses = advstfEduDetses;
	}

	public Set<PolAssignment> getPolAssignments() {
		return this.polAssignments;
	}

	public void setPolAssignments(Set<PolAssignment> polAssignments) {
		this.polAssignments = polAssignments;
	}

	public Set<PolIlpfundredeem> getPolIlpfundredeems() {
		return this.polIlpfundredeems;
	}

	public void setPolIlpfundredeems(Set<PolIlpfundredeem> polIlpfundredeems) {
		this.polIlpfundredeems = polIlpfundredeems;
	}

	public Set<AdviserStaff> getAdviserStaffsForBaseDistId() {
		return this.adviserStaffsForBaseDistId;
	}

	public void setAdviserStaffsForBaseDistId(
			Set<AdviserStaff> adviserStaffsForBaseDistId) {
		this.adviserStaffsForBaseDistId = adviserStaffsForBaseDistId;
	}

	public Set<AdvstfIntroCommsplit> getAdvstfIntroCommsplits() {
		return this.advstfIntroCommsplits;
	}

	public void setAdvstfIntroCommsplits(
			Set<AdvstfIntroCommsplit> advstfIntroCommsplits) {
		this.advstfIntroCommsplits = advstfIntroCommsplits;
	}

	public Set<FnaHistoryDetails> getFnaHistoryDetailses() {
		return this.fnaHistoryDetailses;
	}

	public void setFnaHistoryDetailses(
			Set<FnaHistoryDetails> fnaHistoryDetailses) {
		this.fnaHistoryDetailses = fnaHistoryDetailses;
	}

	public Set<PolInsliferider> getPolInsliferiders() {
		return this.polInsliferiders;
	}

	public void setPolInsliferiders(Set<PolInsliferider> polInsliferiders) {
		this.polInsliferiders = polInsliferiders;
	}

	public Set<CustomerCoreDetails> getCustomerCoreDetailses() {
		return this.customerCoreDetailses;
	}

	public void setCustomerCoreDetailses(
			Set<CustomerCoreDetails> customerCoreDetailses) {
		this.customerCoreDetailses = customerCoreDetailses;
	}

	public Set<CommAmmendments> getCommAmmendmentses() {
		return this.commAmmendmentses;
	}

	public void setCommAmmendmentses(Set<CommAmmendments> commAmmendmentses) {
		this.commAmmendmentses = commAmmendmentses;
	}

	public Set<PolInslifebasic> getPolInslifebasics() {
		return this.polInslifebasics;
	}

	public void setPolInslifebasics(Set<PolInslifebasic> polInslifebasics) {
		this.polInslifebasics = polInslifebasics;
	}

	public Set<FpuserDistWrkwith> getFpuserDistWrkwiths() {
		return this.fpuserDistWrkwiths;
	}

	public void setFpuserDistWrkwiths(Set<FpuserDistWrkwith> fpuserDistWrkwiths) {
		this.fpuserDistWrkwiths = fpuserDistWrkwiths;
	}

	public Set<PolAlterations> getPolAlterationses() {
		return this.polAlterationses;
	}

	public void setPolAlterationses(Set<PolAlterations> polAlterationses) {
		this.polAlterationses = polAlterationses;
	}

	public Set<PolIlpfunds> getPolIlpfundses() {
		return this.polIlpfundses;
	}

	public void setPolIlpfundses(Set<PolIlpfunds> polIlpfundses) {
		this.polIlpfundses = polIlpfundses;
	}

	public Set<CommstmtRiders> getCommstmtRiderses() {
		return this.commstmtRiderses;
	}

	public void setCommstmtRiderses(Set<CommstmtRiders> commstmtRiderses) {
		this.commstmtRiderses = commstmtRiderses;
	}

	public Set<AdvstfLicenseDets> getAdvstfLicenseDetses() {
		return this.advstfLicenseDetses;
	}

	public void setAdvstfLicenseDetses(
			Set<AdvstfLicenseDets> advstfLicenseDetses) {
		this.advstfLicenseDetses = advstfLicenseDetses;
	}

	public Set<AdvstfAttachments> getAdvstfAttachmentses() {
		return this.advstfAttachmentses;
	}

	public void setAdvstfAttachmentses(
			Set<AdvstfAttachments> advstfAttachmentses) {
		this.advstfAttachmentses = advstfAttachmentses;
	}

	public Set<CustomerAttachments> getCustomerAttachmentses() {
		return this.customerAttachmentses;
	}

	public void setCustomerAttachmentses(
			Set<CustomerAttachments> customerAttachmentses) {
		this.customerAttachmentses = customerAttachmentses;
	}

	public Set<CustomerMktAreaOfInterests> getCustomerMktAreaOfInterestses() {
		return this.customerMktAreaOfInterestses;
	}

	public void setCustomerMktAreaOfInterestses(
			Set<CustomerMktAreaOfInterests> customerMktAreaOfInterestses) {
		this.customerMktAreaOfInterestses = customerMktAreaOfInterestses;
	}

	public Set<CustomerDetails> getCustomerDetailses() {
		return this.customerDetailses;
	}

	public void setCustomerDetailses(Set<CustomerDetails> customerDetailses) {
		this.customerDetailses = customerDetailses;
	}

	public Set<AdvstfEmplymntHistory> getAdvstfEmplymntHistories() {
		return this.advstfEmplymntHistories;
	}

	public void setAdvstfEmplymntHistories(
			Set<AdvstfEmplymntHistory> advstfEmplymntHistories) {
		this.advstfEmplymntHistories = advstfEmplymntHistories;
	}

	public Set<KpiTrackTrans> getKpiTrackTranses() {
		return this.kpiTrackTranses;
	}

	public void setKpiTrackTranses(Set<KpiTrackTrans> kpiTrackTranses) {
		this.kpiTrackTranses = kpiTrackTranses;
	}

	public Set<CommGenDets> getCommGenDetses() {
		return this.commGenDetses;
	}

	public void setCommGenDetses(Set<CommGenDets> commGenDetses) {
		this.commGenDetses = commGenDetses;
	}

	public Set<PolTrusteeDets> getPolTrusteeDetses() {
		return this.polTrusteeDetses;
	}

	public void setPolTrusteeDetses(Set<PolTrusteeDets> polTrusteeDetses) {
		this.polTrusteeDetses = polTrusteeDetses;
	}

	public Set<AdvstfIntroDets> getAdvstfIntroDetses() {
		return this.advstfIntroDetses;
	}

	public void setAdvstfIntroDetses(Set<AdvstfIntroDets> advstfIntroDetses) {
		this.advstfIntroDetses = advstfIntroDetses;
	}

	public Set<PolLndBnkUntDets> getPolLndBnkUntDetses() {
		return this.polLndBnkUntDetses;
	}

	public void setPolLndBnkUntDetses(Set<PolLndBnkUntDets> polLndBnkUntDetses) {
		this.polLndBnkUntDetses = polLndBnkUntDetses;
	}

	public Set<PolIlptopup> getPolIlptopups() {
		return this.polIlptopups;
	}

	public void setPolIlptopups(Set<PolIlptopup> polIlptopups) {
		this.polIlptopups = polIlptopups;
	}

	public Set<FeedbackDetails> getFeedbackDetailses() {
		return this.feedbackDetailses;
	}

	public void setFeedbackDetailses(Set<FeedbackDetails> feedbackDetailses) {
		this.feedbackDetailses = feedbackDetailses;
	}

	public Set<CustomerKeyperson> getCustomerKeypersons() {
		return this.customerKeypersons;
	}

	public void setCustomerKeypersons(Set<CustomerKeyperson> customerKeypersons) {
		this.customerKeypersons = customerKeypersons;
	}

	public Set<PolWillstrustsDets> getPolWillstrustsDetses() {
		return this.polWillstrustsDetses;
	}

	public void setPolWillstrustsDetses(
			Set<PolWillstrustsDets> polWillstrustsDetses) {
		this.polWillstrustsDetses = polWillstrustsDetses;
	}

	public Set<CustomerRelDets> getCustomerRelDetses() {
		return this.customerRelDetses;
	}

	public void setCustomerRelDetses(Set<CustomerRelDets> customerRelDetses) {
		this.customerRelDetses = customerRelDetses;
	}*/

}
