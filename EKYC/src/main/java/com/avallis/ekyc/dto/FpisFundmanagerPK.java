package com.avallis.ekyc.dto;

import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class FpisFundmanagerPK implements Serializable {

    /** identifier field */
    private String fmCode;

    /** identifier field */
    private String status;

    /** full constructor */
    public FpisFundmanagerPK(String fmCode, String status) {
        this.fmCode = fmCode;
        this.status = status;
    }

    /** default constructor */
    public FpisFundmanagerPK() {
    }

    public String getFmCode() {
        return this.fmCode;
    }

    public void setFmCode(String fmCode) {
        this.fmCode = fmCode;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("fmCode", getFmCode())
            .append("status", getStatus())
            .toString();
    }

    public boolean equals(Object other) {
        if ( (this == other ) ) return true;
        if ( !(other instanceof FpisFundmanagerPK) ) return false;
        FpisFundmanagerPK castOther = (FpisFundmanagerPK) other;
        return new EqualsBuilder()
            .append(this.getFmCode(), castOther.getFmCode())
            .append(this.getStatus(), castOther.getStatus())
            .isEquals();
    }

    public int hashCode() {
        return new HashCodeBuilder()
            .append(getFmCode())
            .append(getStatus())
            .toHashCode();
    }

}
