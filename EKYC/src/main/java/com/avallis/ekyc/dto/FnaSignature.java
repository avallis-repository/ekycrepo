package com.avallis.ekyc.dto;

import java.util.Date;
//import java.util.UUID;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.FetchType;
//import javax.persistence.GeneratedValue;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.Table;
//
//import org.hibernate.annotations.Type;
//import org.hibernate.annotations.TypeDef;
//import org.json.simple.JSONObject;

import com.fasterxml.jackson.annotation.JsonFormat;

//import com.vladmihalcea.hibernate.type.json.JsonBinaryType;

//import oracle.sql.RAW;



public class FnaSignature implements java.io.Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	private byte[] esignId;

	
	private FnaDetails fnaDetails;

	
	private String signPerson;

	
	private String eSign;


	private byte[] signDocBlob;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy",timezone = "Asia/Singapore")
	private Date signDate;


	private String pageSecRef;

	

	public byte[] getEsignId() {
		return esignId;
	}

	public void setEsignId(byte[] esignId) {
		this.esignId = esignId;
	}

	public FnaDetails getFnaDetails() {
		return fnaDetails;
	}

	public void setFnaDetails(FnaDetails fnaDetails) {
		this.fnaDetails = fnaDetails;
	}

	public String getSignPerson() {
		return signPerson;
	}

	public void setSignPerson(String signPerson) {
		this.signPerson = signPerson;
	}

	public String geteSign() {
		return eSign;
	}

	public void seteSign(String eSign) {
		this.eSign = eSign;
	}

	public byte[] getSignDocBlob() {
		return signDocBlob;
	}

	public void setSignDocBlob(byte[] signDocBlob) {
		this.signDocBlob = signDocBlob;
	}

	public Date getSignDate() {
		return signDate;
	}

	public void setSignDate(Date signDate) {
		this.signDate = signDate;
	}

	public String getPageSecRef() {
		return pageSecRef;
	}

	public void setPageSecRef(String pageSecRef) {
		this.pageSecRef = pageSecRef;
	}

}
