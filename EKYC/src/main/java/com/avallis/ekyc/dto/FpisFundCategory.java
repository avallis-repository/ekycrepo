package com.avallis.ekyc.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class FpisFundCategory implements Serializable {

    /** identifier field */
    private com.avallis.ekyc.dto.FpisFundCategoryPK comp_id;

    /** persistent field */
    private String categoryName;

    /** persistent field */
    private String inputter;

    /** persistent field */
    private Date inpDtTm;

    /** nullable persistent field */
    private String modifier;

    /** nullable persistent field */
    private Date modDtTm;

    /** nullable persistent field */
    private String authoriser;

    /** persistent field */
    private Set fpisFunds;

    /** full constructor */
    public FpisFundCategory(com.avallis.ekyc.dto.FpisFundCategoryPK comp_id, String categoryName, String inputter, Date inpDtTm, String modifier, Date modDtTm, String authoriser, Set fpisFunds) {
        this.comp_id = comp_id;
        this.categoryName = categoryName;
        this.inputter = inputter;
        this.inpDtTm = inpDtTm;
        this.modifier = modifier;
        this.modDtTm = modDtTm;
        this.authoriser = authoriser;
        this.fpisFunds = fpisFunds;
    }

    /** default constructor */
    public FpisFundCategory() {
    }

    /** minimal constructor */
    public FpisFundCategory(com.avallis.ekyc.dto.FpisFundCategoryPK comp_id, String categoryName, String inputter, Date inpDtTm, Set fpisFunds) {
        this.comp_id = comp_id;
        this.categoryName = categoryName;
        this.inputter = inputter;
        this.inpDtTm = inpDtTm;
        this.fpisFunds = fpisFunds;
    }

    public com.avallis.ekyc.dto.FpisFundCategoryPK getComp_id() {
        return this.comp_id;
    }

    public void setComp_id(com.avallis.ekyc.dto.FpisFundCategoryPK comp_id) {
        this.comp_id = comp_id;
    }

    public String getCategoryName() {
        return this.categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getInputter() {
        return this.inputter;
    }

    public void setInputter(String inputter) {
        this.inputter = inputter;
    }

    public Date getInpDtTm() {
        return this.inpDtTm;
    }

    public void setInpDtTm(Date inpDtTm) {
        this.inpDtTm = inpDtTm;
    }

    public String getModifier() {
        return this.modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public Date getModDtTm() {
        return this.modDtTm;
    }

    public void setModDtTm(Date modDtTm) {
        this.modDtTm = modDtTm;
    }

    public String getAuthoriser() {
        return this.authoriser;
    }

    public void setAuthoriser(String authoriser) {
        this.authoriser = authoriser;
    }

    public Set getFpisFunds() {
        return this.fpisFunds;
    }

    public void setFpisFunds(Set fpisFunds) {
        this.fpisFunds = fpisFunds;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("comp_id", getComp_id())
            .toString();
    }

    public boolean equals(Object other) {
        if ( (this == other ) ) return true;
        if ( !(other instanceof FpisFundCategory) ) return false;
        FpisFundCategory castOther = (FpisFundCategory) other;
        return new EqualsBuilder()
            .append(this.getComp_id(), castOther.getComp_id())
            .isEquals();
    }

    public int hashCode() {
        return new HashCodeBuilder()
            .append(getComp_id())
            .toHashCode();
    }

}
