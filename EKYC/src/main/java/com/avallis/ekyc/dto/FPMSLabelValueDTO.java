package com.avallis.ekyc.dto;

import java.io.Serializable;


public class FPMSLabelValueDTO implements Serializable {
    String Label;
    String Value;

    public FPMSLabelValueDTO() {

    }

    public FPMSLabelValueDTO(String strLabel, String strValue) {
        this.Label = strLabel;
        this.Value = strValue;
    }

    public String getLabel() {
        return this.Label;
    }

    public String getValue() {
        return this.Value;
    }

    public void setLabel(String strLabel) {
        this.Label = strLabel;
    }

    public void setValue(String strValue) {
        this.Value = strValue;
    }


}
