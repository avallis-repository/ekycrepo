package com.avallis.ekyc.dto;

// Generated Sep 21, 2018 4:00:47 PM by Hibernate Tools 4.0.0

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreType;

/**
 * FnaNtucEsubmissions generated by hbm2java
 */
@JsonIgnoreType
public class FnaNtucEsubmissions implements java.io.Serializable {

	private String esubid;
	private FnaDetails fnaDetails;
	private String advstfCode;
	private String accessToken;
	private Timestamp ESumbitDt;
	private String expiresSeconds;
	private String caseId;
	private String caseNo;
	private String dsfServerDt;
	private String samlRespId;
	private String samlAssrId;
	private String samlNotbefore;
	private String samlNotafter;
	private String samlAuthinstant;
	private String authnqryId;
	private String authnIssueinstant;
	private String idprespRespId;
	private String idprespInresponseto;
	private String idprespInstant;
	private String idprespAssrId;
	private String idprespNotbefore;
	private String idprespNotafter;
	private String idprespAuthnInstant;
	private String esubCrtdBy;
	private Date esubCrtdDate;
	private String idprespStatus;

	public FnaNtucEsubmissions() {
	}

	public FnaNtucEsubmissions(String esubid) {
		this.esubid = esubid;
	}

	public FnaNtucEsubmissions(String esubid, FnaDetails fnaDetails,
			String advstfCode, String accessToken, Timestamp ESumbitDt,
			String expiresSeconds, String caseId, String caseNo,
			String dsfServerDt, String samlRespId, String samlAssrId,
			String samlNotbefore, String samlNotafter, String samlAuthinstant,
			String authnqryId, String authnIssueinstant, String idprespRespId,
			String idprespInresponseto, String idprespInstant,
			String idprespAssrId, String idprespNotbefore,
			String idprespNotafter, String idprespAuthnInstant,
			String esubCrtdBy, Date esubCrtdDate,String idprespStatus) {
		this.esubid = esubid;
		this.fnaDetails = fnaDetails;
		this.advstfCode = advstfCode;
		this.accessToken = accessToken;
		this.ESumbitDt = ESumbitDt;
		this.expiresSeconds = expiresSeconds;
		this.caseId = caseId;
		this.caseNo = caseNo;
		this.dsfServerDt = dsfServerDt;
		this.samlRespId = samlRespId;
		this.samlAssrId = samlAssrId;
		this.samlNotbefore = samlNotbefore;
		this.samlNotafter = samlNotafter;
		this.samlAuthinstant = samlAuthinstant;
		this.authnqryId = authnqryId;
		this.authnIssueinstant = authnIssueinstant;
		this.idprespRespId = idprespRespId;
		this.idprespInresponseto = idprespInresponseto;
		this.idprespInstant = idprespInstant;
		this.idprespAssrId = idprespAssrId;
		this.idprespNotbefore = idprespNotbefore;
		this.idprespNotafter = idprespNotafter;
		this.idprespAuthnInstant = idprespAuthnInstant;
		this.esubCrtdBy = esubCrtdBy;
		this.esubCrtdDate = esubCrtdDate;
		this.idprespStatus=idprespStatus;
	}

	public String getEsubid() {
		return this.esubid;
	}

	public void setEsubid(String esubid) {
		this.esubid = esubid;
	}

	public FnaDetails getFnaDetails() {
		return this.fnaDetails;
	}

	public void setFnaDetails(FnaDetails fnaDetails) {
		this.fnaDetails = fnaDetails;
	}

	public String getAdvstfCode() {
		return this.advstfCode;
	}

	public void setAdvstfCode(String advstfCode) {
		this.advstfCode = advstfCode;
	}

	public String getAccessToken() {
		return this.accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public Timestamp getESumbitDt() {
		return this.ESumbitDt;
	}

	public void setESumbitDt(Timestamp ESumbitDt) {
		this.ESumbitDt = ESumbitDt;
	}

	public String getExpiresSeconds() {
		return this.expiresSeconds;
	}

	public void setExpiresSeconds(String expiresSeconds) {
		this.expiresSeconds = expiresSeconds;
	}

	public String getCaseId() {
		return this.caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public String getCaseNo() {
		return this.caseNo;
	}

	public void setCaseNo(String caseNo) {
		this.caseNo = caseNo;
	}

	public String getDsfServerDt() {
		return this.dsfServerDt;
	}

	public void setDsfServerDt(String dsfServerDt) {
		this.dsfServerDt = dsfServerDt;
	}

	public String getSamlRespId() {
		return this.samlRespId;
	}

	public void setSamlRespId(String samlRespId) {
		this.samlRespId = samlRespId;
	}

	public String getSamlAssrId() {
		return this.samlAssrId;
	}

	public void setSamlAssrId(String samlAssrId) {
		this.samlAssrId = samlAssrId;
	}

	public String getSamlNotbefore() {
		return this.samlNotbefore;
	}

	public void setSamlNotbefore(String samlNotbefore) {
		this.samlNotbefore = samlNotbefore;
	}

	public String getSamlNotafter() {
		return this.samlNotafter;
	}

	public void setSamlNotafter(String samlNotafter) {
		this.samlNotafter = samlNotafter;
	}

	public String getSamlAuthinstant() {
		return this.samlAuthinstant;
	}

	public void setSamlAuthinstant(String samlAuthinstant) {
		this.samlAuthinstant = samlAuthinstant;
	}

	public String getAuthnqryId() {
		return this.authnqryId;
	}

	public void setAuthnqryId(String authnqryId) {
		this.authnqryId = authnqryId;
	}

	public String getAuthnIssueinstant() {
		return this.authnIssueinstant;
	}

	public void setAuthnIssueinstant(String authnIssueinstant) {
		this.authnIssueinstant = authnIssueinstant;
	}

	public String getIdprespRespId() {
		return this.idprespRespId;
	}

	public void setIdprespRespId(String idprespRespId) {
		this.idprespRespId = idprespRespId;
	}

	public String getIdprespInresponseto() {
		return this.idprespInresponseto;
	}

	public void setIdprespInresponseto(String idprespInresponseto) {
		this.idprespInresponseto = idprespInresponseto;
	}

	public String getIdprespInstant() {
		return this.idprespInstant;
	}

	public void setIdprespInstant(String idprespInstant) {
		this.idprespInstant = idprespInstant;
	}

	public String getIdprespAssrId() {
		return this.idprespAssrId;
	}

	public void setIdprespAssrId(String idprespAssrId) {
		this.idprespAssrId = idprespAssrId;
	}

	public String getIdprespNotbefore() {
		return this.idprespNotbefore;
	}

	public void setIdprespNotbefore(String idprespNotbefore) {
		this.idprespNotbefore = idprespNotbefore;
	}

	public String getIdprespNotafter() {
		return this.idprespNotafter;
	}

	public void setIdprespNotafter(String idprespNotafter) {
		this.idprespNotafter = idprespNotafter;
	}

	public String getIdprespAuthnInstant() {
		return this.idprespAuthnInstant;
	}

	public void setIdprespAuthnInstant(String idprespAuthnInstant) {
		this.idprespAuthnInstant = idprespAuthnInstant;
	}

	public String getEsubCrtdBy() {
		return this.esubCrtdBy;
	}

	public void setEsubCrtdBy(String esubCrtdBy) {
		this.esubCrtdBy = esubCrtdBy;
	}

	public Date getEsubCrtdDate() {
		return this.esubCrtdDate;
	}

	public void setEsubCrtdDate(Date esubCrtdDate) {
		this.esubCrtdDate = esubCrtdDate;
	}

	public String getIdprespStatus() {
		return idprespStatus;
	}

	public void setIdprespStatus(String idprespStatus) {
		this.idprespStatus = idprespStatus;
	}

}
