package com.avallis.ekyc.dto;

public class FnaOthPersTpp {
	
	private String txtFldCateg="TPP";
	private String txtFldCDTppId;
	private String txtFldCDTppName;
	private String txtFldCDTppNric;
	private String txtFldCDTppIncNo;
	private String txtFldCDTppAddr;
	private String radFldCDTppJob;
	
	private String txtFldCDTppJobCont;
	private String txtFldCDTppRel;
	private String txtFldCDTppCcontact;
	private String txtFldCDTppBankName;
	private String txtFldCDTppChqNo;
	private String radCDTppPayMode;
	
	//poovathi add posheld field  on 18-08-2021
	private String txtFldCDTppPosheld;
	
	
	public String getRadCDTppPayMode() {
		return radCDTppPayMode;
	}
	public void setRadCDTppPayMode(String radCDTppPayMode) {
		this.radCDTppPayMode = radCDTppPayMode;
	}
	public String getTxtFldCateg() {
		return txtFldCateg;
	}
	public void setTxtFldCateg(String txtFldCateg) {
		this.txtFldCateg = txtFldCateg;
	}
	
	public String getTxtFldCDTppId() {
		return txtFldCDTppId;
	}
	public void setTxtFldCDTppId(String txtFldCDTppId) {
		this.txtFldCDTppId = txtFldCDTppId;
	}
	public String getTxtFldCDTppName() {
		return txtFldCDTppName;
	}
	public void setTxtFldCDTppName(String txtFldCDTppName) {
		this.txtFldCDTppName = txtFldCDTppName;
	}
	public String getTxtFldCDTppNric() {
		return txtFldCDTppNric;
	}
	public void setTxtFldCDTppNric(String txtFldCDTppNric) {
		this.txtFldCDTppNric = txtFldCDTppNric;
	}
	public String getTxtFldCDTppIncNo() {
		return txtFldCDTppIncNo;
	}
	public void setTxtFldCDTppIncNo(String txtFldCDTppIncNo) {
		this.txtFldCDTppIncNo = txtFldCDTppIncNo;
	}
	public String getTxtFldCDTppAddr() {
		return txtFldCDTppAddr;
	}
	public void setTxtFldCDTppAddr(String txtFldCDTppAddr) {
		this.txtFldCDTppAddr = txtFldCDTppAddr;
	}
	public String getTxtFldCDTppJobCont() {
		return txtFldCDTppJobCont;
	}
	public void setTxtFldCDTppJobCont(String txtFldCDTppJobCont) {
		this.txtFldCDTppJobCont = txtFldCDTppJobCont;
	}
	public String getTxtFldCDTppRel() {
		return txtFldCDTppRel;
	}
	public void setTxtFldCDTppRel(String txtFldCDTppRel) {
		this.txtFldCDTppRel = txtFldCDTppRel;
	}
	public String getTxtFldCDTppCcontact() {
		return txtFldCDTppCcontact;
	}
	public void setTxtFldCDTppCcontact(String txtFldCDTppCcontact) {
		this.txtFldCDTppCcontact = txtFldCDTppCcontact;
	}
	public String getTxtFldCDTppBankName() {
		return txtFldCDTppBankName;
	}
	public void setTxtFldCDTppBankName(String txtFldCDTppBankName) {
		this.txtFldCDTppBankName = txtFldCDTppBankName;
	}
	public String getTxtFldCDTppChqNo() {
		return txtFldCDTppChqNo;
	}
	public void setTxtFldCDTppChqNo(String txtFldCDTppChqNo) {
		this.txtFldCDTppChqNo = txtFldCDTppChqNo;
	}
	public String getRadFldCDTppJob() {
		return radFldCDTppJob;
	}
	public void setRadFldCDTppJob(String radFldCDTppJob) {
		this.radFldCDTppJob = radFldCDTppJob;
	}
	
	//poovathi add on 18-08-2021
	public String getTxtFldCDTppPosheld() {
	    return txtFldCDTppPosheld;
	}
	public void setTxtFldCDTppPosheld(String txtFldCDTppPosheld) {
	    this.txtFldCDTppPosheld = txtFldCDTppPosheld;
	}

}
