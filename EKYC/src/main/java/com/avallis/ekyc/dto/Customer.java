package com.avallis.ekyc.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreType;

@JsonIgnoreType
public class Customer {

	public String CustId;
	public String getCustId() {
		return CustId;
	}
	public void setCustId(String custId) {
		CustId = custId;
	}
	public String NricName;
	public String NricNum;
	public String resAddr1;
	public String resAddr2;
	public String resAddr3;
	public String resCity;
	public String resState;
	public String resCountry;
	public String resPostalCode;
	public String getNricName() {
		return NricName;
	}
	public void setNricName(String nricName) {
		NricName = nricName;
	}
	public String getNricNum() {
		return NricNum;
	}
	public void setNricNum(String nricNum) {
		NricNum = nricNum;
	}
	public String getResAddr1() {
		return resAddr1;
	}
	public void setResAddr1(String resAddr1) {
		this.resAddr1 = resAddr1;
	}
	public String getResAddr2() {
		return resAddr2;
	}
	public void setResAddr2(String resAddr2) {
		this.resAddr2 = resAddr2;
	}
	public String getResAddr3() {
		return resAddr3;
	}
	public void setResAddr3(String resAddr3) {
		this.resAddr3 = resAddr3;
	}
	public String getResCity() {
		return resCity;
	}
	public void setResCity(String resCity) {
		this.resCity = resCity;
	}
	public String getResState() {
		return resState;
	}
	public void setResState(String resState) {
		this.resState = resState;
	}
	public String getResCountry() {
		return resCountry;
	}
	public void setResCountry(String resCountry) {
		this.resCountry = resCountry;
	}
	public String getResPostalCode() {
		return resPostalCode;
	}
	public void setResPostalCode(String resPostalCode) {
		this.resPostalCode = resPostalCode;
	}
}
