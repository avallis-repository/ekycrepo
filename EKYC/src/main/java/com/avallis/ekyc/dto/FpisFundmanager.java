package com.avallis.ekyc.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class FpisFundmanager implements Serializable {

    /** identifier field */
    private com.avallis.ekyc.dto.FpisFundmanagerPK comp_id;

    /** nullable persistent field */
    private String mnemonic;

    /** persistent field */
    private String name;

    /** nullable persistent field */
    private String amcRegno;

    /** nullable persistent field */
    private String address;

    /** nullable persistent field */
    private String state;

    /** nullable persistent field */
    private String pincode;

    /** nullable persistent field */
    private String city;

    /** nullable persistent field */
    private String country;

    /** nullable persistent field */
    private String region;

    /** nullable persistent field */
    private String emailId;

    /** nullable persistent field */
    private String faxNo;

    /** nullable persistent field */
    private String telephone1;

    /** nullable persistent field */
    private String telephone2;

    /** nullable persistent field */
    private String contactRef;

    /** persistent field */
    private String currency;

    /** persistent field */
    private String fmNumber;

    /** persistent field */
    private String inputter;

    /** persistent field */
    private Date inpDtTm;

    /** nullable persistent field */
    private String authoriser;

    /** nullable persistent field */
    private Integer minAge;

    /** nullable persistent field */
    private Integer stlmtCycleT;

    /** nullable persistent field */
    private String tranCutoffTime;

    /** nullable persistent field */
    private String aggrCutoffTime;

    /** nullable persistent field */
    private String allocCutoffTime;

    /** nullable persistent field */
    private String modifier;

    /** nullable persistent field */
    private Date modDtTm;

    /** nullable persistent field */
    private String namePeriodFrom;

    /** nullable persistent field */
    private String namePeriodTo;

    /** nullable persistent field */
    private String previousName;

    /** nullable persistent field */
    private String distributor;

    /** nullable persistent field */
    private String fmCommReqd;

//    /** persistent field */
//    private Set fpisPurchaseReqs;
//
//    /** persistent field */
//    private Set fpisNavs;
//
//    /** persistent field */
//    private Set fpisMsNavs;
//
//    /** persistent field */
//    private Set fpisDividendDeclarations;
//
//    /** persistent field */
//    private Set fpisDivUnitReconciliates;
//
//    /** persistent field */
//    private Set fpisFunds;
//
//    /** persistent field */
//    private Set fpisRepurchaseReqs;

    /** full constructor */
    public FpisFundmanager(com.avallis.ekyc.dto.FpisFundmanagerPK comp_id, String mnemonic, String name, String amcRegno, String address, String state, String pincode, String city, String country, String region, String emailId, String faxNo, String telephone1, String telephone2, String contactRef, String currency, String fmNumber, String inputter, Date inpDtTm, String authoriser, Integer minAge, Integer stlmtCycleT, String tranCutoffTime, String aggrCutoffTime, String allocCutoffTime, String modifier, Date modDtTm, String namePeriodFrom, String namePeriodTo, String previousName, String distributor, String fmCommReqd, Set fpisPurchaseReqs, Set fpisNavs, Set fpisMsNavs, Set fpisDividendDeclarations, Set fpisDivUnitReconciliates, Set fpisFunds, Set fpisRepurchaseReqs) {
        this.comp_id = comp_id;
        this.mnemonic = mnemonic;
        this.name = name;
        this.amcRegno = amcRegno;
        this.address = address;
        this.state = state;
        this.pincode = pincode;
        this.city = city;
        this.country = country;
        this.region = region;
        this.emailId = emailId;
        this.faxNo = faxNo;
        this.telephone1 = telephone1;
        this.telephone2 = telephone2;
        this.contactRef = contactRef;
        this.currency = currency;
        this.fmNumber = fmNumber;
        this.inputter = inputter;
        this.inpDtTm = inpDtTm;
        this.authoriser = authoriser;
        this.minAge = minAge;
        this.stlmtCycleT = stlmtCycleT;
        this.tranCutoffTime = tranCutoffTime;
        this.aggrCutoffTime = aggrCutoffTime;
        this.allocCutoffTime = allocCutoffTime;
        this.modifier = modifier;
        this.modDtTm = modDtTm;
        this.namePeriodFrom = namePeriodFrom;
        this.namePeriodTo = namePeriodTo;
        this.previousName = previousName;
        this.distributor = distributor;
        this.fmCommReqd = fmCommReqd;
//        this.fpisPurchaseReqs = fpisPurchaseReqs;
//        this.fpisNavs = fpisNavs;
//        this.fpisMsNavs = fpisMsNavs;
//        this.fpisDividendDeclarations = fpisDividendDeclarations;
//        this.fpisDivUnitReconciliates = fpisDivUnitReconciliates;
//        this.fpisFunds = fpisFunds;
//        this.fpisRepurchaseReqs = fpisRepurchaseReqs;
    }

    /** default constructor */
    public FpisFundmanager() {
    }

    /** minimal constructor */
    public FpisFundmanager(com.avallis.ekyc.dto.FpisFundmanagerPK comp_id, String name, String currency, String fmNumber, String inputter, Date inpDtTm, Set fpisPurchaseReqs, Set fpisNavs, Set fpisMsNavs, Set fpisDividendDeclarations, Set fpisDivUnitReconciliates, Set fpisFunds, Set fpisRepurchaseReqs) {
        this.comp_id = comp_id;
        this.name = name;
        this.currency = currency;
        this.fmNumber = fmNumber;
        this.inputter = inputter;
        this.inpDtTm = inpDtTm;
//        this.fpisPurchaseReqs = fpisPurchaseReqs;
//        this.fpisNavs = fpisNavs;
//        this.fpisMsNavs = fpisMsNavs;
//        this.fpisDividendDeclarations = fpisDividendDeclarations;
//        this.fpisDivUnitReconciliates = fpisDivUnitReconciliates;
//        this.fpisFunds = fpisFunds;
//        this.fpisRepurchaseReqs = fpisRepurchaseReqs;
    }

    public com.avallis.ekyc.dto.FpisFundmanagerPK getComp_id() {
        return this.comp_id;
    }

    public void setComp_id(com.avallis.ekyc.dto.FpisFundmanagerPK comp_id) {
        this.comp_id = comp_id;
    }

    public String getMnemonic() {
        return this.mnemonic;
    }

    public void setMnemonic(String mnemonic) {
        this.mnemonic = mnemonic;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmcRegno() {
        return this.amcRegno;
    }

    public void setAmcRegno(String amcRegno) {
        this.amcRegno = amcRegno;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getState() {
        return this.state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return this.pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getRegion() {
        return this.region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getEmailId() {
        return this.emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getFaxNo() {
        return this.faxNo;
    }

    public void setFaxNo(String faxNo) {
        this.faxNo = faxNo;
    }

    public String getTelephone1() {
        return this.telephone1;
    }

    public void setTelephone1(String telephone1) {
        this.telephone1 = telephone1;
    }

    public String getTelephone2() {
        return this.telephone2;
    }

    public void setTelephone2(String telephone2) {
        this.telephone2 = telephone2;
    }

    public String getContactRef() {
        return this.contactRef;
    }

    public void setContactRef(String contactRef) {
        this.contactRef = contactRef;
    }

    public String getCurrency() {
        return this.currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getFmNumber() {
        return this.fmNumber;
    }

    public void setFmNumber(String fmNumber) {
        this.fmNumber = fmNumber;
    }

    public String getInputter() {
        return this.inputter;
    }

    public void setInputter(String inputter) {
        this.inputter = inputter;
    }

    public Date getInpDtTm() {
        return this.inpDtTm;
    }

    public void setInpDtTm(Date inpDtTm) {
        this.inpDtTm = inpDtTm;
    }

    public String getAuthoriser() {
        return this.authoriser;
    }

    public void setAuthoriser(String authoriser) {
        this.authoriser = authoriser;
    }

    public Integer getMinAge() {
        return this.minAge;
    }

    public void setMinAge(Integer minAge) {
        this.minAge = minAge;
    }

    public Integer getStlmtCycleT() {
        return this.stlmtCycleT;
    }

    public void setStlmtCycleT(Integer stlmtCycleT) {
        this.stlmtCycleT = stlmtCycleT;
    }

    public String getTranCutoffTime() {
        return this.tranCutoffTime;
    }

    public void setTranCutoffTime(String tranCutoffTime) {
        this.tranCutoffTime = tranCutoffTime;
    }

    public String getAggrCutoffTime() {
        return this.aggrCutoffTime;
    }

    public void setAggrCutoffTime(String aggrCutoffTime) {
        this.aggrCutoffTime = aggrCutoffTime;
    }

    public String getAllocCutoffTime() {
        return this.allocCutoffTime;
    }

    public void setAllocCutoffTime(String allocCutoffTime) {
        this.allocCutoffTime = allocCutoffTime;
    }

    public String getModifier() {
        return this.modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public Date getModDtTm() {
        return this.modDtTm;
    }

    public void setModDtTm(Date modDtTm) {
        this.modDtTm = modDtTm;
    }

    public String getNamePeriodFrom() {
        return this.namePeriodFrom;
    }

    public void setNamePeriodFrom(String namePeriodFrom) {
        this.namePeriodFrom = namePeriodFrom;
    }

    public String getNamePeriodTo() {
        return this.namePeriodTo;
    }

    public void setNamePeriodTo(String namePeriodTo) {
        this.namePeriodTo = namePeriodTo;
    }

    public String getPreviousName() {
        return this.previousName;
    }

    public void setPreviousName(String previousName) {
        this.previousName = previousName;
    }

    public String getDistributor() {
        return this.distributor;
    }

    public void setDistributor(String distributor) {
        this.distributor = distributor;
    }

    public String getFmCommReqd() {
        return this.fmCommReqd;
    }

    public void setFmCommReqd(String fmCommReqd) {
        this.fmCommReqd = fmCommReqd;
    }

//    public Set getFpisPurchaseReqs() {
//        return this.fpisPurchaseReqs;
//    }
//
//    public void setFpisPurchaseReqs(Set fpisPurchaseReqs) {
//        this.fpisPurchaseReqs = fpisPurchaseReqs;
//    }
//
//    public Set getFpisNavs() {
//        return this.fpisNavs;
//    }
//
//    public void setFpisNavs(Set fpisNavs) {
//        this.fpisNavs = fpisNavs;
//    }
//
//    public Set getFpisMsNavs() {
//        return this.fpisMsNavs;
//    }
//
//    public void setFpisMsNavs(Set fpisMsNavs) {
//        this.fpisMsNavs = fpisMsNavs;
//    }
//
//    public Set getFpisDividendDeclarations() {
//        return this.fpisDividendDeclarations;
//    }
//
//    public void setFpisDividendDeclarations(Set fpisDividendDeclarations) {
//        this.fpisDividendDeclarations = fpisDividendDeclarations;
//    }
//
//    public Set getFpisDivUnitReconciliates() {
//        return this.fpisDivUnitReconciliates;
//    }
//
//    public void setFpisDivUnitReconciliates(Set fpisDivUnitReconciliates) {
//        this.fpisDivUnitReconciliates = fpisDivUnitReconciliates;
//    }
//
//    public Set getFpisFunds() {
//        return this.fpisFunds;
//    }
//
//    public void setFpisFunds(Set fpisFunds) {
//        this.fpisFunds = fpisFunds;
//    }
//
//    public Set getFpisRepurchaseReqs() {
//        return this.fpisRepurchaseReqs;
//    }
//
//    public void setFpisRepurchaseReqs(Set fpisRepurchaseReqs) {
//        this.fpisRepurchaseReqs = fpisRepurchaseReqs;
//    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("comp_id", getComp_id())
            .toString();
    }

    public boolean equals(Object other) {
        if ( (this == other ) ) return true;
        if ( !(other instanceof FpisFundmanager) ) return false;
        FpisFundmanager castOther = (FpisFundmanager) other;
        return new EqualsBuilder()
            .append(this.getComp_id(), castOther.getComp_id())
            .isEquals();
    }

    public int hashCode() {
        return new HashCodeBuilder()
            .append(getComp_id())
            .toHashCode();
    }

}
