package com.avallis.ekyc.dto;

import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class FpisFundPK implements Serializable {

    /** identifier field */
    private String status;

    /** identifier field */
    private String fmCode;

    /** identifier field */
    private String fundCode;

    /** full constructor */
    public FpisFundPK(String status, String fmCode, String fundCode) {
        this.status = status;
        this.fmCode = fmCode;
        this.fundCode = fundCode;
    }

    /** default constructor */
    public FpisFundPK() {
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFmCode() {
        return this.fmCode;
    }

    public void setFmCode(String fmCode) {
        this.fmCode = fmCode;
    }

    public String getFundCode() {
        return this.fundCode;
    }

    public void setFundCode(String fundCode) {
        this.fundCode = fundCode;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("status", getStatus())
            .append("fmCode", getFmCode())
            .append("fundCode", getFundCode())
            .toString();
    }

    public boolean equals(Object other) {
        if ( (this == other ) ) return true;
        if ( !(other instanceof FpisFundPK) ) return false;
        FpisFundPK castOther = (FpisFundPK) other;
        return new EqualsBuilder()
            .append(this.getStatus(), castOther.getStatus())
            .append(this.getFmCode(), castOther.getFmCode())
            .append(this.getFundCode(), castOther.getFundCode())
            .isEquals();
    }

    public int hashCode() {
        return new HashCodeBuilder()
            .append(getStatus())
            .append(getFmCode())
            .append(getFundCode())
            .toHashCode();
    }

}
