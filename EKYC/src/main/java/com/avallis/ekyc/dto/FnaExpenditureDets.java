package com.avallis.ekyc.dto;

// Generated Mar 13, 2018 9:17:21 PM by Hibernate Tools 4.0.0

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreType;

/**
 * FnaExpenditureDets generated by hbm2java
 */
@JsonIgnoreType
public class FnaExpenditureDets implements java.io.Serializable {

	private String expdId;
	private FnaDetails fnaDetails;
	private Double expdSelfRent;
	private Double expdSelfUtil;
	private Double expdSelfGrocery;
	private Double expdSelfEatingout;
	private Double expdSelfClothing;
	private Double expdSelfTransport;
	private Double expdSelfMedical;
	private Double expdSelfPersonal;
	private Double expdSelfDepntcontr;
	private Double expdSelfTaxes;
	private Double expdSelfEntertain;
	private Double expdSelfFestiv;
	private Double expdSelfVacations;
	private Double expdSelfCharity;
	private Double expdSelfProploan;
	private Double expdSelfVehiloan;
	private Double expdSelfInsurance;
	private Double expdSelfOthers;
	private Double expdSpsRent;
	private Double expdSpsUtil;
	private Double expdSpsGrocery;
	private Double expdSpsEatingout;
	private Double expdSpsClothing;
	private Double expdSpsTransport;
	private Double expdSpsMedical;
	private Double expdSpsPersonal;
	private Double expdSpsDepntcontr;
	private Double expdSpsTaxes;
	private Double expdSpsEntertain;
	private Double expdSpsFestiv;
	private Double expdSpsVacations;
	private Double expdSpsCharity;
	private Double expdSpsProploan;
	private Double expdSpsVehiloan;
	private Double expdSpsInsurance;
	private Double expdSpsOthers;
	private Double expdFamilyRent;
	private Double expdFamilyUtil;
	private Double expdFamilyGrocery;
	private Double expdFamilyEatingout;
	private Double expdFamilyClothing;
	private Double expdFamilyTransport;
	private Double expdFamilyMedical;
	private Double expdFamilyPersonal;
	private Double expdFamilyHousehold;
	private Double expdFamilyDomestic;
	private Double expdFamilyEnhance;
	private Double expdFamilyTaxes;
	private Double expdFamilyEntertain;
	private Double expdFamilyFestiv;
	private Double expdFamilyVacations;
	private Double expdFamilyCharity;
	private Double expdFamilyProploan;
	private Double expdFamilyVehiloan;
	private Double expdFamilyInsurance;
	private Double expdFamilyOthers;
	private String expdRemarks;
	private String createdBy;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	private Date createdDate;
	private Double cfSelfAnnlinc;
	private Double cfSpsAnnlinc;
	private Double cfFamilyAnnlinc;
	private Double cfSelfCpfcontib;
	private Double cfSpsCpfcontib;
	private Double cfFamilyCpfcontib;
	private Double cfSelfEstexpense;
	private Double cfSpsEstexpense;
	private Double cfFamilyEstexpense;
	private Double cfSelfSurpdef;
	private Double cfSpsSurpdef;
	private Double cfFamilySurpdef;
	private Double alSelfTotasset;
	private Double alSpsTotasset;
	private Double alFamilyTotasset;
	private Double alSelfTotliab;
	private Double alSpsTotliab;
	private Double alFamilyTotliab;
	private Double alSelfNetasset;
	private Double alSpsNetasset;
	private Double alFamilyNetasset;

	public FnaExpenditureDets() {
	}

	public FnaExpenditureDets(String expdId) {
		this.expdId = expdId;
	}

	public FnaExpenditureDets(String expdId, FnaDetails fnaDetails,
			Double expdSelfRent, Double expdSelfUtil,
			Double expdSelfGrocery, Double expdSelfEatingout,
			Double expdSelfClothing, Double expdSelfTransport,
			Double expdSelfMedical, Double expdSelfPersonal,
			Double expdSelfDepntcontr, Double expdSelfTaxes,
			Double expdSelfEntertain, Double expdSelfFestiv,
			Double expdSelfVacations, Double expdSelfCharity,
			Double expdSelfProploan, Double expdSelfVehiloan,
			Double expdSelfInsurance, Double expdSelfOthers,
			Double expdSpsRent, Double expdSpsUtil,
			Double expdSpsGrocery, Double expdSpsEatingout,
			Double expdSpsClothing, Double expdSpsTransport,
			Double expdSpsMedical, Double expdSpsPersonal,
			Double expdSpsDepntcontr, Double expdSpsTaxes,
			Double expdSpsEntertain, Double expdSpsFestiv,
			Double expdSpsVacations, Double expdSpsCharity,
			Double expdSpsProploan, Double expdSpsVehiloan,
			Double expdSpsInsurance, Double expdSpsOthers,
			Double expdFamilyRent, Double expdFamilyUtil,
			Double expdFamilyGrocery, Double expdFamilyEatingout,
			Double expdFamilyClothing, Double expdFamilyTransport,
			Double expdFamilyMedical, Double expdFamilyPersonal,
			Double expdFamilyHousehold, Double expdFamilyDomestic,
			Double expdFamilyEnhance, Double expdFamilyTaxes,
			Double expdFamilyEntertain, Double expdFamilyFestiv,
			Double expdFamilyVacations, Double expdFamilyCharity,
			Double expdFamilyProploan, Double expdFamilyVehiloan,
			Double expdFamilyInsurance, Double expdFamilyOthers,
			String expdRemarks, String createdBy, Date createdDate,
			Double cfSelfAnnlinc, Double cfSpsAnnlinc, Double cfFamilyAnnlinc,
			Double cfSelfCpfcontib, Double cfSpsCpfcontib,
			Double cfFamilyCpfcontib, Double cfSelfEstexpense,
			Double cfSpsEstexpense, Double cfFamilyEstexpense,
			Double cfSelfSurpdef, Double cfSpsSurpdef, Double cfFamilySurpdef,
			Double alSelfTotasset, Double alSpsTotasset,
			Double alFamilyTotasset, Double alSelfTotliab, Double alSpsTotliab,
			Double alFamilyTotliab, Double alSelfNetasset,
			Double alSpsNetasset, Double alFamilyNetasset) {
		this.expdId = expdId;
		this.fnaDetails = fnaDetails;
		this.expdSelfRent = expdSelfRent;
		this.expdSelfUtil = expdSelfUtil;
		this.expdSelfGrocery = expdSelfGrocery;
		this.expdSelfEatingout = expdSelfEatingout;
		this.expdSelfClothing = expdSelfClothing;
		this.expdSelfTransport = expdSelfTransport;
		this.expdSelfMedical = expdSelfMedical;
		this.expdSelfPersonal = expdSelfPersonal;
		this.expdSelfDepntcontr = expdSelfDepntcontr;
		this.expdSelfTaxes = expdSelfTaxes;
		this.expdSelfEntertain = expdSelfEntertain;
		this.expdSelfFestiv = expdSelfFestiv;
		this.expdSelfVacations = expdSelfVacations;
		this.expdSelfCharity = expdSelfCharity;
		this.expdSelfProploan = expdSelfProploan;
		this.expdSelfVehiloan = expdSelfVehiloan;
		this.expdSelfInsurance = expdSelfInsurance;
		this.expdSelfOthers = expdSelfOthers;
		this.expdSpsRent = expdSpsRent;
		this.expdSpsUtil = expdSpsUtil;
		this.expdSpsGrocery = expdSpsGrocery;
		this.expdSpsEatingout = expdSpsEatingout;
		this.expdSpsClothing = expdSpsClothing;
		this.expdSpsTransport = expdSpsTransport;
		this.expdSpsMedical = expdSpsMedical;
		this.expdSpsPersonal = expdSpsPersonal;
		this.expdSpsDepntcontr = expdSpsDepntcontr;
		this.expdSpsTaxes = expdSpsTaxes;
		this.expdSpsEntertain = expdSpsEntertain;
		this.expdSpsFestiv = expdSpsFestiv;
		this.expdSpsVacations = expdSpsVacations;
		this.expdSpsCharity = expdSpsCharity;
		this.expdSpsProploan = expdSpsProploan;
		this.expdSpsVehiloan = expdSpsVehiloan;
		this.expdSpsInsurance = expdSpsInsurance;
		this.expdSpsOthers = expdSpsOthers;
		this.expdFamilyRent = expdFamilyRent;
		this.expdFamilyUtil = expdFamilyUtil;
		this.expdFamilyGrocery = expdFamilyGrocery;
		this.expdFamilyEatingout = expdFamilyEatingout;
		this.expdFamilyClothing = expdFamilyClothing;
		this.expdFamilyTransport = expdFamilyTransport;
		this.expdFamilyMedical = expdFamilyMedical;
		this.expdFamilyPersonal = expdFamilyPersonal;
		this.expdFamilyHousehold = expdFamilyHousehold;
		this.expdFamilyDomestic = expdFamilyDomestic;
		this.expdFamilyEnhance = expdFamilyEnhance;
		this.expdFamilyTaxes = expdFamilyTaxes;
		this.expdFamilyEntertain = expdFamilyEntertain;
		this.expdFamilyFestiv = expdFamilyFestiv;
		this.expdFamilyVacations = expdFamilyVacations;
		this.expdFamilyCharity = expdFamilyCharity;
		this.expdFamilyProploan = expdFamilyProploan;
		this.expdFamilyVehiloan = expdFamilyVehiloan;
		this.expdFamilyInsurance = expdFamilyInsurance;
		this.expdFamilyOthers = expdFamilyOthers;
		this.expdRemarks = expdRemarks;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.cfSelfAnnlinc = cfSelfAnnlinc;
		this.cfSpsAnnlinc = cfSpsAnnlinc;
		this.cfFamilyAnnlinc = cfFamilyAnnlinc;
		this.cfSelfCpfcontib = cfSelfCpfcontib;
		this.cfSpsCpfcontib = cfSpsCpfcontib;
		this.cfFamilyCpfcontib = cfFamilyCpfcontib;
		this.cfSelfEstexpense = cfSelfEstexpense;
		this.cfSpsEstexpense = cfSpsEstexpense;
		this.cfFamilyEstexpense = cfFamilyEstexpense;
		this.cfSelfSurpdef = cfSelfSurpdef;
		this.cfSpsSurpdef = cfSpsSurpdef;
		this.cfFamilySurpdef = cfFamilySurpdef;
		this.alSelfTotasset = alSelfTotasset;
		this.alSpsTotasset = alSpsTotasset;
		this.alFamilyTotasset = alFamilyTotasset;
		this.alSelfTotliab = alSelfTotliab;
		this.alSpsTotliab = alSpsTotliab;
		this.alFamilyTotliab = alFamilyTotliab;
		this.alSelfNetasset = alSelfNetasset;
		this.alSpsNetasset = alSpsNetasset;
		this.alFamilyNetasset = alFamilyNetasset;
	}

	public String getExpdId() {
		return this.expdId;
	}

	public void setExpdId(String expdId) {
		this.expdId = expdId;
	}

	public FnaDetails getFnaDetails() {
		return this.fnaDetails;
	}

	public void setFnaDetails(FnaDetails fnaDetails) {
		this.fnaDetails = fnaDetails;
	}

	public Double getExpdSelfRent() {
		return this.expdSelfRent;
	}

	public void setExpdSelfRent(Double expdSelfRent) {
		this.expdSelfRent = expdSelfRent;
	}

	public Double getExpdSelfUtil() {
		return this.expdSelfUtil;
	}

	public void setExpdSelfUtil(Double expdSelfUtil) {
		this.expdSelfUtil = expdSelfUtil;
	}

	public Double getExpdSelfGrocery() {
		return this.expdSelfGrocery;
	}

	public void setExpdSelfGrocery(Double expdSelfGrocery) {
		this.expdSelfGrocery = expdSelfGrocery;
	}

	public Double getExpdSelfEatingout() {
		return this.expdSelfEatingout;
	}

	public void setExpdSelfEatingout(Double expdSelfEatingout) {
		this.expdSelfEatingout = expdSelfEatingout;
	}

	public Double getExpdSelfClothing() {
		return this.expdSelfClothing;
	}

	public void setExpdSelfClothing(Double expdSelfClothing) {
		this.expdSelfClothing = expdSelfClothing;
	}

	public Double getExpdSelfTransport() {
		return this.expdSelfTransport;
	}

	public void setExpdSelfTransport(Double expdSelfTransport) {
		this.expdSelfTransport = expdSelfTransport;
	}

	public Double getExpdSelfMedical() {
		return this.expdSelfMedical;
	}

	public void setExpdSelfMedical(Double expdSelfMedical) {
		this.expdSelfMedical = expdSelfMedical;
	}

	public Double getExpdSelfPersonal() {
		return this.expdSelfPersonal;
	}

	public void setExpdSelfPersonal(Double expdSelfPersonal) {
		this.expdSelfPersonal = expdSelfPersonal;
	}

	public Double getExpdSelfDepntcontr() {
		return this.expdSelfDepntcontr;
	}

	public void setExpdSelfDepntcontr(Double expdSelfDepntcontr) {
		this.expdSelfDepntcontr = expdSelfDepntcontr;
	}

	public Double getExpdSelfTaxes() {
		return this.expdSelfTaxes;
	}

	public void setExpdSelfTaxes(Double expdSelfTaxes) {
		this.expdSelfTaxes = expdSelfTaxes;
	}

	public Double getExpdSelfEntertain() {
		return this.expdSelfEntertain;
	}

	public void setExpdSelfEntertain(Double expdSelfEntertain) {
		this.expdSelfEntertain = expdSelfEntertain;
	}

	public Double getExpdSelfFestiv() {
		return this.expdSelfFestiv;
	}

	public void setExpdSelfFestiv(Double expdSelfFestiv) {
		this.expdSelfFestiv = expdSelfFestiv;
	}

	public Double getExpdSelfVacations() {
		return this.expdSelfVacations;
	}

	public void setExpdSelfVacations(Double expdSelfVacations) {
		this.expdSelfVacations = expdSelfVacations;
	}

	public Double getExpdSelfCharity() {
		return this.expdSelfCharity;
	}

	public void setExpdSelfCharity(Double expdSelfCharity) {
		this.expdSelfCharity = expdSelfCharity;
	}

	public Double getExpdSelfProploan() {
		return this.expdSelfProploan;
	}

	public void setExpdSelfProploan(Double expdSelfProploan) {
		this.expdSelfProploan = expdSelfProploan;
	}

	public Double getExpdSelfVehiloan() {
		return this.expdSelfVehiloan;
	}

	public void setExpdSelfVehiloan(Double expdSelfVehiloan) {
		this.expdSelfVehiloan = expdSelfVehiloan;
	}

	public Double getExpdSelfInsurance() {
		return this.expdSelfInsurance;
	}

	public void setExpdSelfInsurance(Double expdSelfInsurance) {
		this.expdSelfInsurance = expdSelfInsurance;
	}

	public Double getExpdSelfOthers() {
		return this.expdSelfOthers;
	}

	public void setExpdSelfOthers(Double expdSelfOthers) {
		this.expdSelfOthers = expdSelfOthers;
	}

	public Double getExpdSpsRent() {
		return this.expdSpsRent;
	}

	public void setExpdSpsRent(Double expdSpsRent) {
		this.expdSpsRent = expdSpsRent;
	}

	public Double getExpdSpsUtil() {
		return this.expdSpsUtil;
	}

	public void setExpdSpsUtil(Double expdSpsUtil) {
		this.expdSpsUtil = expdSpsUtil;
	}

	public Double getExpdSpsGrocery() {
		return this.expdSpsGrocery;
	}

	public void setExpdSpsGrocery(Double expdSpsGrocery) {
		this.expdSpsGrocery = expdSpsGrocery;
	}

	public Double getExpdSpsEatingout() {
		return this.expdSpsEatingout;
	}

	public void setExpdSpsEatingout(Double expdSpsEatingout) {
		this.expdSpsEatingout = expdSpsEatingout;
	}

	public Double getExpdSpsClothing() {
		return this.expdSpsClothing;
	}

	public void setExpdSpsClothing(Double expdSpsClothing) {
		this.expdSpsClothing = expdSpsClothing;
	}

	public Double getExpdSpsTransport() {
		return this.expdSpsTransport;
	}

	public void setExpdSpsTransport(Double expdSpsTransport) {
		this.expdSpsTransport = expdSpsTransport;
	}

	public Double getExpdSpsMedical() {
		return this.expdSpsMedical;
	}

	public void setExpdSpsMedical(Double expdSpsMedical) {
		this.expdSpsMedical = expdSpsMedical;
	}

	public Double getExpdSpsPersonal() {
		return this.expdSpsPersonal;
	}

	public void setExpdSpsPersonal(Double expdSpsPersonal) {
		this.expdSpsPersonal = expdSpsPersonal;
	}

	public Double getExpdSpsDepntcontr() {
		return this.expdSpsDepntcontr;
	}

	public void setExpdSpsDepntcontr(Double expdSpsDepntcontr) {
		this.expdSpsDepntcontr = expdSpsDepntcontr;
	}

	public Double getExpdSpsTaxes() {
		return this.expdSpsTaxes;
	}

	public void setExpdSpsTaxes(Double expdSpsTaxes) {
		this.expdSpsTaxes = expdSpsTaxes;
	}

	public Double getExpdSpsEntertain() {
		return this.expdSpsEntertain;
	}

	public void setExpdSpsEntertain(Double expdSpsEntertain) {
		this.expdSpsEntertain = expdSpsEntertain;
	}

	public Double getExpdSpsFestiv() {
		return this.expdSpsFestiv;
	}

	public void setExpdSpsFestiv(Double expdSpsFestiv) {
		this.expdSpsFestiv = expdSpsFestiv;
	}

	public Double getExpdSpsVacations() {
		return this.expdSpsVacations;
	}

	public void setExpdSpsVacations(Double expdSpsVacations) {
		this.expdSpsVacations = expdSpsVacations;
	}

	public Double getExpdSpsCharity() {
		return this.expdSpsCharity;
	}

	public void setExpdSpsCharity(Double expdSpsCharity) {
		this.expdSpsCharity = expdSpsCharity;
	}

	public Double getExpdSpsProploan() {
		return this.expdSpsProploan;
	}

	public void setExpdSpsProploan(Double expdSpsProploan) {
		this.expdSpsProploan = expdSpsProploan;
	}

	public Double getExpdSpsVehiloan() {
		return this.expdSpsVehiloan;
	}

	public void setExpdSpsVehiloan(Double expdSpsVehiloan) {
		this.expdSpsVehiloan = expdSpsVehiloan;
	}

	public Double getExpdSpsInsurance() {
		return this.expdSpsInsurance;
	}

	public void setExpdSpsInsurance(Double expdSpsInsurance) {
		this.expdSpsInsurance = expdSpsInsurance;
	}

	public Double getExpdSpsOthers() {
		return this.expdSpsOthers;
	}

	public void setExpdSpsOthers(Double expdSpsOthers) {
		this.expdSpsOthers = expdSpsOthers;
	}

	public Double getExpdFamilyRent() {
		return this.expdFamilyRent;
	}

	public void setExpdFamilyRent(Double expdFamilyRent) {
		this.expdFamilyRent = expdFamilyRent;
	}

	public Double getExpdFamilyUtil() {
		return this.expdFamilyUtil;
	}

	public void setExpdFamilyUtil(Double expdFamilyUtil) {
		this.expdFamilyUtil = expdFamilyUtil;
	}

	public Double getExpdFamilyGrocery() {
		return this.expdFamilyGrocery;
	}

	public void setExpdFamilyGrocery(Double expdFamilyGrocery) {
		this.expdFamilyGrocery = expdFamilyGrocery;
	}

	public Double getExpdFamilyEatingout() {
		return this.expdFamilyEatingout;
	}

	public void setExpdFamilyEatingout(Double expdFamilyEatingout) {
		this.expdFamilyEatingout = expdFamilyEatingout;
	}

	public Double getExpdFamilyClothing() {
		return this.expdFamilyClothing;
	}

	public void setExpdFamilyClothing(Double expdFamilyClothing) {
		this.expdFamilyClothing = expdFamilyClothing;
	}

	public Double getExpdFamilyTransport() {
		return this.expdFamilyTransport;
	}

	public void setExpdFamilyTransport(Double expdFamilyTransport) {
		this.expdFamilyTransport = expdFamilyTransport;
	}

	public Double getExpdFamilyMedical() {
		return this.expdFamilyMedical;
	}

	public void setExpdFamilyMedical(Double expdFamilyMedical) {
		this.expdFamilyMedical = expdFamilyMedical;
	}

	public Double getExpdFamilyPersonal() {
		return this.expdFamilyPersonal;
	}

	public void setExpdFamilyPersonal(Double expdFamilyPersonal) {
		this.expdFamilyPersonal = expdFamilyPersonal;
	}

	public Double getExpdFamilyHousehold() {
		return this.expdFamilyHousehold;
	}

	public void setExpdFamilyHousehold(Double expdFamilyHousehold) {
		this.expdFamilyHousehold = expdFamilyHousehold;
	}

	public Double getExpdFamilyDomestic() {
		return this.expdFamilyDomestic;
	}

	public void setExpdFamilyDomestic(Double expdFamilyDomestic) {
		this.expdFamilyDomestic = expdFamilyDomestic;
	}

	public Double getExpdFamilyEnhance() {
		return this.expdFamilyEnhance;
	}

	public void setExpdFamilyEnhance(Double expdFamilyEnhance) {
		this.expdFamilyEnhance = expdFamilyEnhance;
	}

	public Double getExpdFamilyTaxes() {
		return this.expdFamilyTaxes;
	}

	public void setExpdFamilyTaxes(Double expdFamilyTaxes) {
		this.expdFamilyTaxes = expdFamilyTaxes;
	}

	public Double getExpdFamilyEntertain() {
		return this.expdFamilyEntertain;
	}

	public void setExpdFamilyEntertain(Double expdFamilyEntertain) {
		this.expdFamilyEntertain = expdFamilyEntertain;
	}

	public Double getExpdFamilyFestiv() {
		return this.expdFamilyFestiv;
	}

	public void setExpdFamilyFestiv(Double expdFamilyFestiv) {
		this.expdFamilyFestiv = expdFamilyFestiv;
	}

	public Double getExpdFamilyVacations() {
		return this.expdFamilyVacations;
	}

	public void setExpdFamilyVacations(Double expdFamilyVacations) {
		this.expdFamilyVacations = expdFamilyVacations;
	}

	public Double getExpdFamilyCharity() {
		return this.expdFamilyCharity;
	}

	public void setExpdFamilyCharity(Double expdFamilyCharity) {
		this.expdFamilyCharity = expdFamilyCharity;
	}

	public Double getExpdFamilyProploan() {
		return this.expdFamilyProploan;
	}

	public void setExpdFamilyProploan(Double expdFamilyProploan) {
		this.expdFamilyProploan = expdFamilyProploan;
	}

	public Double getExpdFamilyVehiloan() {
		return this.expdFamilyVehiloan;
	}

	public void setExpdFamilyVehiloan(Double expdFamilyVehiloan) {
		this.expdFamilyVehiloan = expdFamilyVehiloan;
	}

	public Double getExpdFamilyInsurance() {
		return this.expdFamilyInsurance;
	}

	public void setExpdFamilyInsurance(Double expdFamilyInsurance) {
		this.expdFamilyInsurance = expdFamilyInsurance;
	}

	public Double getExpdFamilyOthers() {
		return this.expdFamilyOthers;
	}

	public void setExpdFamilyOthers(Double expdFamilyOthers) {
		this.expdFamilyOthers = expdFamilyOthers;
	}

	public String getExpdRemarks() {
		return this.expdRemarks;
	}

	public void setExpdRemarks(String expdRemarks) {
		this.expdRemarks = expdRemarks;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Double getCfSelfAnnlinc() {
		return this.cfSelfAnnlinc;
	}

	public void setCfSelfAnnlinc(Double cfSelfAnnlinc) {
		this.cfSelfAnnlinc = cfSelfAnnlinc;
	}

	public Double getCfSpsAnnlinc() {
		return this.cfSpsAnnlinc;
	}

	public void setCfSpsAnnlinc(Double cfSpsAnnlinc) {
		this.cfSpsAnnlinc = cfSpsAnnlinc;
	}

	public Double getCfFamilyAnnlinc() {
		return this.cfFamilyAnnlinc;
	}

	public void setCfFamilyAnnlinc(Double cfFamilyAnnlinc) {
		this.cfFamilyAnnlinc = cfFamilyAnnlinc;
	}

	public Double getCfSelfCpfcontib() {
		return this.cfSelfCpfcontib;
	}

	public void setCfSelfCpfcontib(Double cfSelfCpfcontib) {
		this.cfSelfCpfcontib = cfSelfCpfcontib;
	}

	public Double getCfSpsCpfcontib() {
		return this.cfSpsCpfcontib;
	}

	public void setCfSpsCpfcontib(Double cfSpsCpfcontib) {
		this.cfSpsCpfcontib = cfSpsCpfcontib;
	}

	public Double getCfFamilyCpfcontib() {
		return this.cfFamilyCpfcontib;
	}

	public void setCfFamilyCpfcontib(Double cfFamilyCpfcontib) {
		this.cfFamilyCpfcontib = cfFamilyCpfcontib;
	}

	public Double getCfSelfEstexpense() {
		return this.cfSelfEstexpense;
	}

	public void setCfSelfEstexpense(Double cfSelfEstexpense) {
		this.cfSelfEstexpense = cfSelfEstexpense;
	}

	public Double getCfSpsEstexpense() {
		return this.cfSpsEstexpense;
	}

	public void setCfSpsEstexpense(Double cfSpsEstexpense) {
		this.cfSpsEstexpense = cfSpsEstexpense;
	}

	public Double getCfFamilyEstexpense() {
		return this.cfFamilyEstexpense;
	}

	public void setCfFamilyEstexpense(Double cfFamilyEstexpense) {
		this.cfFamilyEstexpense = cfFamilyEstexpense;
	}

	public Double getCfSelfSurpdef() {
		return this.cfSelfSurpdef;
	}

	public void setCfSelfSurpdef(Double cfSelfSurpdef) {
		this.cfSelfSurpdef = cfSelfSurpdef;
	}

	public Double getCfSpsSurpdef() {
		return this.cfSpsSurpdef;
	}

	public void setCfSpsSurpdef(Double cfSpsSurpdef) {
		this.cfSpsSurpdef = cfSpsSurpdef;
	}

	public Double getCfFamilySurpdef() {
		return this.cfFamilySurpdef;
	}

	public void setCfFamilySurpdef(Double cfFamilySurpdef) {
		this.cfFamilySurpdef = cfFamilySurpdef;
	}

	public Double getAlSelfTotasset() {
		return this.alSelfTotasset;
	}

	public void setAlSelfTotasset(Double alSelfTotasset) {
		this.alSelfTotasset = alSelfTotasset;
	}

	public Double getAlSpsTotasset() {
		return this.alSpsTotasset;
	}

	public void setAlSpsTotasset(Double alSpsTotasset) {
		this.alSpsTotasset = alSpsTotasset;
	}

	public Double getAlFamilyTotasset() {
		return this.alFamilyTotasset;
	}

	public void setAlFamilyTotasset(Double alFamilyTotasset) {
		this.alFamilyTotasset = alFamilyTotasset;
	}

	public Double getAlSelfTotliab() {
		return this.alSelfTotliab;
	}

	public void setAlSelfTotliab(Double alSelfTotliab) {
		this.alSelfTotliab = alSelfTotliab;
	}

	public Double getAlSpsTotliab() {
		return this.alSpsTotliab;
	}

	public void setAlSpsTotliab(Double alSpsTotliab) {
		this.alSpsTotliab = alSpsTotliab;
	}

	public Double getAlFamilyTotliab() {
		return this.alFamilyTotliab;
	}

	public void setAlFamilyTotliab(Double alFamilyTotliab) {
		this.alFamilyTotliab = alFamilyTotliab;
	}

	public Double getAlSelfNetasset() {
		return this.alSelfNetasset;
	}

	public void setAlSelfNetasset(Double alSelfNetasset) {
		this.alSelfNetasset = alSelfNetasset;
	}

	public Double getAlSpsNetasset() {
		return this.alSpsNetasset;
	}

	public void setAlSpsNetasset(Double alSpsNetasset) {
		this.alSpsNetasset = alSpsNetasset;
	}

	public Double getAlFamilyNetasset() {
		return this.alFamilyNetasset;
	}

	public void setAlFamilyNetasset(Double alFamilyNetasset) {
		this.alFamilyNetasset = alFamilyNetasset;
	}

}
