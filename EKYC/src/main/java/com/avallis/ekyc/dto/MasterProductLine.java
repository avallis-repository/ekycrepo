package com.avallis.ekyc.dto;

// Generated Mar 13, 2018 9:17:21 PM by Hibernate Tools 4.0.0

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreType;

/**
 * MasterProductLine generated by hbm2java
 */
@JsonIgnoreType
public class MasterProductLine implements java.io.Serializable {

	private String productLineId;
	private String productLineMain;
	private String productLineDesc;
	private String createdBy;
	private Date createdDate;
	private String modifiedBy;
	private Date modifiedDate;
	private String productLineSub;
	/*private Set<CustomerMktAreaOfInterests> customerMktAreaOfInterestses = new HashSet<CustomerMktAreaOfInterests>(
			0);
	private Set<MasterProduct> masterProducts = new HashSet<MasterProduct>(0);
	private Set<CustomerLobDets> customerLobDetses = new HashSet<CustomerLobDets>(
			0);
	private Set<Policy> policies = new HashSet<Policy>(0);
	private Set<MasterCourseDetails> masterCourseDetailses = new HashSet<MasterCourseDetails>(
			0);
	private Set<CpdHrsScheduler> cpdHrsSchedulers = new HashSet<CpdHrsScheduler>(
			0);
	private Set<ProdLobTransDets> prodLobTransDetses = new HashSet<ProdLobTransDets>(
			0);
*/
	public MasterProductLine() {
	}

	public MasterProductLine(String productLineId, String createdBy,
			Date createdDate) {
		this.productLineId = productLineId;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
	}

	public MasterProductLine(String productLineId, String productLineMain,
			String productLineDesc, String createdBy, Date createdDate,
			String modifiedBy, Date modifiedDate, String productLineSub/*,
			Set<CustomerMktAreaOfInterests> customerMktAreaOfInterestses,
			Set<MasterProduct> masterProducts,
			Set<CustomerLobDets> customerLobDetses, Set<Policy> policies,
			Set<MasterCourseDetails> masterCourseDetailses,
			Set<CpdHrsScheduler> cpdHrsSchedulers,
			Set<ProdLobTransDets> prodLobTransDetses*/) {
		this.productLineId = productLineId;
		this.productLineMain = productLineMain;
		this.productLineDesc = productLineDesc;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.modifiedBy = modifiedBy;
		this.modifiedDate = modifiedDate;
		this.productLineSub = productLineSub;
		/*this.customerMktAreaOfInterestses = customerMktAreaOfInterestses;
		this.masterProducts = masterProducts;
		this.customerLobDetses = customerLobDetses;
		this.policies = policies;
		this.masterCourseDetailses = masterCourseDetailses;
		this.cpdHrsSchedulers = cpdHrsSchedulers;
		this.prodLobTransDetses = prodLobTransDetses;*/
	}

	public String getProductLineId() {
		return this.productLineId;
	}

	public void setProductLineId(String productLineId) {
		this.productLineId = productLineId;
	}

	public String getProductLineMain() {
		return this.productLineMain;
	}

	public void setProductLineMain(String productLineMain) {
		this.productLineMain = productLineMain;
	}

	public String getProductLineDesc() {
		return this.productLineDesc;
	}

	public void setProductLineDesc(String productLineDesc) {
		this.productLineDesc = productLineDesc;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getProductLineSub() {
		return this.productLineSub;
	}

	public void setProductLineSub(String productLineSub) {
		this.productLineSub = productLineSub;
	}

	/*public Set<CustomerMktAreaOfInterests> getCustomerMktAreaOfInterestses() {
		return this.customerMktAreaOfInterestses;
	}

	public void setCustomerMktAreaOfInterestses(
			Set<CustomerMktAreaOfInterests> customerMktAreaOfInterestses) {
		this.customerMktAreaOfInterestses = customerMktAreaOfInterestses;
	}

	public Set<MasterProduct> getMasterProducts() {
		return this.masterProducts;
	}

	public void setMasterProducts(Set<MasterProduct> masterProducts) {
		this.masterProducts = masterProducts;
	}

	public Set<CustomerLobDets> getCustomerLobDetses() {
		return this.customerLobDetses;
	}

	public void setCustomerLobDetses(Set<CustomerLobDets> customerLobDetses) {
		this.customerLobDetses = customerLobDetses;
	}

	public Set<Policy> getPolicies() {
		return this.policies;
	}

	public void setPolicies(Set<Policy> policies) {
		this.policies = policies;
	}

	public Set<MasterCourseDetails> getMasterCourseDetailses() {
		return this.masterCourseDetailses;
	}

	public void setMasterCourseDetailses(
			Set<MasterCourseDetails> masterCourseDetailses) {
		this.masterCourseDetailses = masterCourseDetailses;
	}

	public Set<CpdHrsScheduler> getCpdHrsSchedulers() {
		return this.cpdHrsSchedulers;
	}

	public void setCpdHrsSchedulers(Set<CpdHrsScheduler> cpdHrsSchedulers) {
		this.cpdHrsSchedulers = cpdHrsSchedulers;
	}

	public Set<ProdLobTransDets> getProdLobTransDetses() {
		return this.prodLobTransDetses;
	}

	public void setProdLobTransDetses(Set<ProdLobTransDets> prodLobTransDetses) {
		this.prodLobTransDetses = prodLobTransDetses;
	}*/

}
