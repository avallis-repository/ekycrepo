package com.avallis.ekyc.dto;

// Generated Mar 13, 2018 9:17:21 PM by Hibernate Tools 4.0.0

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreType;

/**
 * MasterCustomerStatus generated by hbm2java
 */
@JsonIgnoreType
public class MasterCustomerStatus implements java.io.Serializable {

	private String customerStatusId;
	private String customerStatusDesc;
	private String customerStatusName;
	private String createdBy;
	private Date createdDate;
	private String modifiedBy;
	private Date modifiedDate;
//	private Set<CustomerDetails> customerDetailses = new HashSet<CustomerDetails>(0);

	public MasterCustomerStatus() {
	}

	public MasterCustomerStatus(String customerStatusId, String createdBy,
			Date createdDate) {
		this.customerStatusId = customerStatusId;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
	}

	public MasterCustomerStatus(String customerStatusId,
			String customerStatusDesc, String customerStatusName,
			String createdBy, Date createdDate, String modifiedBy,
			Date modifiedDate/*, Set<CustomerDetails> customerDetailses*/) {
		this.customerStatusId = customerStatusId;
		this.customerStatusDesc = customerStatusDesc;
		this.customerStatusName = customerStatusName;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.modifiedBy = modifiedBy;
		this.modifiedDate = modifiedDate;
//		this.customerDetailses = customerDetailses;
	}

	public String getCustomerStatusId() {
		return this.customerStatusId;
	}

	public void setCustomerStatusId(String customerStatusId) {
		this.customerStatusId = customerStatusId;
	}

	public String getCustomerStatusDesc() {
		return this.customerStatusDesc;
	}

	public void setCustomerStatusDesc(String customerStatusDesc) {
		this.customerStatusDesc = customerStatusDesc;
	}

	public String getCustomerStatusName() {
		return this.customerStatusName;
	}

	public void setCustomerStatusName(String customerStatusName) {
		this.customerStatusName = customerStatusName;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/*public Set<CustomerDetails> getCustomerDetailses() {
		return this.customerDetailses;
	}

	public void setCustomerDetailses(Set<CustomerDetails> customerDetailses) {
		this.customerDetailses = customerDetailses;
	}*/

}
