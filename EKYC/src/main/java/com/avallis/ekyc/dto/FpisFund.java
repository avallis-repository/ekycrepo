package com.avallis.ekyc.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class FpisFund implements Serializable {

    /** identifier field */
    private com.avallis.ekyc.dto.FpisFundPK comp_id;

    /** persistent field */
    private String inputter;

    /** persistent field */
    private Date inpDtTm;

    /** nullable persistent field */
    private String authoriser;

    /** persistent field */
    private String fundName;

    /** nullable persistent field */
    private String mnemonic;

    /** nullable persistent field */
    private String fundNumber;

    /** persistent field */
    private String fundType;

    /** persistent field */
    private Date fundStartDate;

    /** nullable persistent field */
    private Date fundEndDate;

    /** nullable persistent field */
    private Date repsStartDt;

    /** persistent field */
    private String currency;

    /** persistent field */
    private Double faceValue;

    /** nullable persistent field */
    private Integer lockPeriod;

    /** nullable persistent field */
    private Double approvedFundUnits;

    /** nullable persistent field */
    private Double minCreatLimit;

    /** nullable persistent field */
    private Double minCanclLimit;

    /** nullable persistent field */
    private Double minBalCirculation;

    /** nullable persistent field */
    private String pricingMethod;

    /** persistent field */
    private int unitDecimals;

    /** nullable persistent field */
    private String unitRoundoffFlag;

    /** nullable persistent field */
    private String amountRoundoffFlag;

    /** nullable persistent field */
    private String schemeSrlNo;

    /** persistent field */
    private int priceDecimals;

    /** nullable persistent field */
    private Double maxUnitsHolding;

    /** persistent field */
    private String distributor;

    /** persistent field */
    private String allocationUnitsRoundOff;

    /** persistent field */
    private String nomAccFm;

    /** persistent field */
    private String cpfInvAllowYn;

    /** persistent field */
    private String cpfAccType;

    /** persistent field */
    private String schemeLoc;

    /** persistent field */
    private String srsInvAllowYn;

    /** nullable persistent field */
    private String benchmarkCode;

    /** nullable persistent field */
    private String paymentGrossNet;

    /** nullable persistent field */
    private String aggregationRqrd;

    /** nullable persistent field */
    private String lipperCode;

    /** nullable persistent field */
    private String navFreq;

    /** nullable persistent field */
    private Double minUnitsRedem;

    /** nullable persistent field */
    private Double minInvestAmount;

    /** nullable persistent field */
    private Double minRedemAmount;

    /** nullable persistent field */
    private String cutoffTime;

    /** nullable persistent field */
    private String growthDividendFlag;

    /** nullable persistent field */
    private Double minBalUnits;

    /** nullable persistent field */
    private Double maxUnitsRedem;

    /** nullable persistent field */
    private String businessStartTime;

    /** nullable persistent field */
    private String businessEndTime;

    /** persistent field */
    private String fatalOverwrite;

    /** nullable persistent field */
    private Double minAddInvestAmount;

    /** nullable persistent field */
    private Double minTransferUnits;

    /** nullable persistent field */
    private Double minTransferAmount;

    /** nullable persistent field */
    private Double minSsAmount;

    /** nullable persistent field */
    private Double minSsUnits;

    /** nullable persistent field */
    private Double minBalAmount;

    /** nullable persistent field */
    private Double minShAmount;

    /** nullable persistent field */
    private Double minShUnits;

    /** nullable persistent field */
    private String reinvestFlag;

    /** nullable persistent field */
    private String swpFixedDate;

    /** nullable persistent field */
    private String swpDayOne;

    /** nullable persistent field */
    private String swpDayTwo;

    /** nullable persistent field */
    private Double swpMMinUnits;

    /** nullable persistent field */
    private Double swpQMinUnits;

    /** nullable persistent field */
    private Double swpHMinUnits;

    /** nullable persistent field */
    private Double swpYMinUnits;

    /** nullable persistent field */
    private Double minUnitsSwp;

    /** nullable persistent field */
    private Double swpMinBalEnroll;

    /** nullable persistent field */
    private String salesChargeAllow;

    /** nullable persistent field */
    private String switchingOption;

    /** nullable persistent field */
    private String shiftingOption;

    /** nullable persistent field */
    private Integer freeSwitches;

    /** nullable persistent field */
    private String freeSwitchesFreq;

    /** nullable persistent field */
    private String freeSwitchesChargeYn;

    /** nullable persistent field */
    private String switchShiftCharge;

    /** nullable persistent field */
    private String shexitFeeYn;

    /** nullable persistent field */
    private String swshexitFee;

    /** nullable persistent field */
    private String redemFlag;

    /** nullable persistent field */
    private Integer coolOffDays;

    /** nullable persistent field */
    private String redemExitFeeYn;

    /** nullable persistent field */
    private Integer redemfreeExit;

    /** nullable persistent field */
    private String redemexitFee;

    /** nullable persistent field */
    private String transferOption;

    /** nullable persistent field */
    private String transferexitFeeOption;

    /** nullable persistent field */
    private Integer transferfreeExit;

    /** nullable persistent field */
    private String transferexitFee;

    /** nullable persistent field */
    private String shareType;

    /** nullable persistent field */
    private String priceOption;

    /** nullable persistent field */
    private Integer settlementDaysPur;

    /** nullable persistent field */
    private Integer settlementDaysRedm;

    /** persistent field */
    private String planCode;

    /** persistent field */
    private String planName;

    /** nullable persistent field */
    private String modifier;

    /** nullable persistent field */
    private Date modDtTm;

    /** nullable persistent field */
    private Double salesCharge;

    /** nullable persistent field */
    private Double distributorRetention;

    /** nullable persistent field */
    private Double cpfDistributorRetention;

    /** nullable persistent field */
    private Double cpfSalesCharge;

    /** nullable persistent field */
    private String fndCommReqd;

    /** nullable persistent field */
    private Double srsSalesCharge;

    /** nullable persistent field */
    private Double srsDistributorRetention;

    /** nullable persistent field */
    private String isin;

    /** nullable persistent field */
    private String morningstarId;

    /** persistent field */
    private com.avallis.ekyc.dto.FpisFundCategory fpisFundCategory;

    /** persistent field */
    private com.avallis.ekyc.dto.FpisFundmanager fpisFundmanager;

//    /** persistent field */
//    private Set fpisPurchaseReqs;
//
//    /** persistent field */
//    private Set fpisInvestorHoldingsBals;
//
//    /** persistent field */
//    private Set fpisRspSetups;
//
//    /** persistent field */
//    private Set fpisDividendReprocessReqs;
//
//    /** persistent field */
//    private Set fpisInvestorAgentBals;
//
//    /** persistent field */
//    private Set fpisNavs;
//
//    /** persistent field */
//    private Set fpisDividendDeclarations;
//
//    /** persistent field */
//    private Set fpisMsNavs;
//
//    /** persistent field */
//    private Set fpisDivUnitReconciliates;
//
//    /** persistent field */
//    private Set fpisTransferToInvestors;
//
//    /** persistent field */
//    private Set fpisTransferFromDistributors;
//
//    /** persistent field */
//    private Set fpisSwitchOutReqs;
//
//    /** persistent field */
//    private Set fpisAllocations;
//
//    /** persistent field */
//    private Set fpisFundStatusHists;
//
//    /** persistent field */
//    private Set fpisTransferToDistributors;
//
//    /** persistent field */
//    private Set fpisAggregations;
//
//    /** persistent field */
//    private Set fpisTransferFromInvestors;
//
//    /** persistent field */
//    private Set fpisInvestorHoldingsTrans;
//
//    /** persistent field */
//    private Set fpisReinvestOptionReqs;
//
//    /** persistent field */
//    private Set fpisSwitchInReqs;
//
//    /** persistent field */
//    private Set fpisRepurchaseReqs;

    /** full constructor */
    public FpisFund(com.avallis.ekyc.dto.FpisFundPK comp_id, String inputter, Date inpDtTm, String authoriser, String fundName, String mnemonic, String fundNumber, String fundType, Date fundStartDate, Date fundEndDate, Date repsStartDt, String currency, Double faceValue, Integer lockPeriod, Double approvedFundUnits, Double minCreatLimit, Double minCanclLimit, Double minBalCirculation, String pricingMethod, int unitDecimals, String unitRoundoffFlag, String amountRoundoffFlag, String schemeSrlNo, int priceDecimals, Double maxUnitsHolding, String distributor, String allocationUnitsRoundOff, String nomAccFm, String cpfInvAllowYn, String cpfAccType, String schemeLoc, String srsInvAllowYn, String benchmarkCode, String paymentGrossNet, String aggregationRqrd, String lipperCode, String navFreq, Double minUnitsRedem, Double minInvestAmount, Double minRedemAmount, String cutoffTime, String growthDividendFlag, Double minBalUnits, Double maxUnitsRedem, String businessStartTime, String businessEndTime, String fatalOverwrite, Double minAddInvestAmount, Double minTransferUnits, Double minTransferAmount, Double minSsAmount, Double minSsUnits, Double minBalAmount, Double minShAmount, Double minShUnits, String reinvestFlag, String swpFixedDate, String swpDayOne, String swpDayTwo, Double swpMMinUnits, Double swpQMinUnits, Double swpHMinUnits, Double swpYMinUnits, Double minUnitsSwp, Double swpMinBalEnroll, String salesChargeAllow, String switchingOption, String shiftingOption, Integer freeSwitches, String freeSwitchesFreq, String freeSwitchesChargeYn, String switchShiftCharge, String shexitFeeYn, String swshexitFee, String redemFlag, Integer coolOffDays, String redemExitFeeYn, Integer redemfreeExit, String redemexitFee, String transferOption, String transferexitFeeOption, Integer transferfreeExit, String transferexitFee, String shareType, String priceOption, Integer settlementDaysPur, Integer settlementDaysRedm, String planCode, String planName, String modifier, Date modDtTm, Double salesCharge, Double distributorRetention, Double cpfDistributorRetention, Double cpfSalesCharge, String fndCommReqd, Double srsSalesCharge, Double srsDistributorRetention, String isin, String morningstarId, com.avallis.ekyc.dto.FpisFundCategory fpisFundCategory, com.avallis.ekyc.dto.FpisFundmanager fpisFundmanager, Set fpisPurchaseReqs, Set fpisInvestorHoldingsBals, Set fpisRspSetups, Set fpisDividendReprocessReqs, Set fpisInvestorAgentBals, Set fpisNavs, Set fpisDividendDeclarations, Set fpisMsNavs, Set fpisDivUnitReconciliates, Set fpisTransferToInvestors, Set fpisTransferFromDistributors, Set fpisSwitchOutReqs, Set fpisAllocations, Set fpisFundStatusHists, Set fpisTransferToDistributors, Set fpisAggregations, Set fpisTransferFromInvestors, Set fpisInvestorHoldingsTrans, Set fpisReinvestOptionReqs, Set fpisSwitchInReqs, Set fpisRepurchaseReqs) {
        this.comp_id = comp_id;
        this.inputter = inputter;
        this.inpDtTm = inpDtTm;
        this.authoriser = authoriser;
        this.fundName = fundName;
        this.mnemonic = mnemonic;
        this.fundNumber = fundNumber;
        this.fundType = fundType;
        this.fundStartDate = fundStartDate;
        this.fundEndDate = fundEndDate;
        this.repsStartDt = repsStartDt;
        this.currency = currency;
        this.faceValue = faceValue;
        this.lockPeriod = lockPeriod;
        this.approvedFundUnits = approvedFundUnits;
        this.minCreatLimit = minCreatLimit;
        this.minCanclLimit = minCanclLimit;
        this.minBalCirculation = minBalCirculation;
        this.pricingMethod = pricingMethod;
        this.unitDecimals = unitDecimals;
        this.unitRoundoffFlag = unitRoundoffFlag;
        this.amountRoundoffFlag = amountRoundoffFlag;
        this.schemeSrlNo = schemeSrlNo;
        this.priceDecimals = priceDecimals;
        this.maxUnitsHolding = maxUnitsHolding;
        this.distributor = distributor;
        this.allocationUnitsRoundOff = allocationUnitsRoundOff;
        this.nomAccFm = nomAccFm;
        this.cpfInvAllowYn = cpfInvAllowYn;
        this.cpfAccType = cpfAccType;
        this.schemeLoc = schemeLoc;
        this.srsInvAllowYn = srsInvAllowYn;
        this.benchmarkCode = benchmarkCode;
        this.paymentGrossNet = paymentGrossNet;
        this.aggregationRqrd = aggregationRqrd;
        this.lipperCode = lipperCode;
        this.navFreq = navFreq;
        this.minUnitsRedem = minUnitsRedem;
        this.minInvestAmount = minInvestAmount;
        this.minRedemAmount = minRedemAmount;
        this.cutoffTime = cutoffTime;
        this.growthDividendFlag = growthDividendFlag;
        this.minBalUnits = minBalUnits;
        this.maxUnitsRedem = maxUnitsRedem;
        this.businessStartTime = businessStartTime;
        this.businessEndTime = businessEndTime;
        this.fatalOverwrite = fatalOverwrite;
        this.minAddInvestAmount = minAddInvestAmount;
        this.minTransferUnits = minTransferUnits;
        this.minTransferAmount = minTransferAmount;
        this.minSsAmount = minSsAmount;
        this.minSsUnits = minSsUnits;
        this.minBalAmount = minBalAmount;
        this.minShAmount = minShAmount;
        this.minShUnits = minShUnits;
        this.reinvestFlag = reinvestFlag;
        this.swpFixedDate = swpFixedDate;
        this.swpDayOne = swpDayOne;
        this.swpDayTwo = swpDayTwo;
        this.swpMMinUnits = swpMMinUnits;
        this.swpQMinUnits = swpQMinUnits;
        this.swpHMinUnits = swpHMinUnits;
        this.swpYMinUnits = swpYMinUnits;
        this.minUnitsSwp = minUnitsSwp;
        this.swpMinBalEnroll = swpMinBalEnroll;
        this.salesChargeAllow = salesChargeAllow;
        this.switchingOption = switchingOption;
        this.shiftingOption = shiftingOption;
        this.freeSwitches = freeSwitches;
        this.freeSwitchesFreq = freeSwitchesFreq;
        this.freeSwitchesChargeYn = freeSwitchesChargeYn;
        this.switchShiftCharge = switchShiftCharge;
        this.shexitFeeYn = shexitFeeYn;
        this.swshexitFee = swshexitFee;
        this.redemFlag = redemFlag;
        this.coolOffDays = coolOffDays;
        this.redemExitFeeYn = redemExitFeeYn;
        this.redemfreeExit = redemfreeExit;
        this.redemexitFee = redemexitFee;
        this.transferOption = transferOption;
        this.transferexitFeeOption = transferexitFeeOption;
        this.transferfreeExit = transferfreeExit;
        this.transferexitFee = transferexitFee;
        this.shareType = shareType;
        this.priceOption = priceOption;
        this.settlementDaysPur = settlementDaysPur;
        this.settlementDaysRedm = settlementDaysRedm;
        this.planCode = planCode;
        this.planName = planName;
        this.modifier = modifier;
        this.modDtTm = modDtTm;
        this.salesCharge = salesCharge;
        this.distributorRetention = distributorRetention;
        this.cpfDistributorRetention = cpfDistributorRetention;
        this.cpfSalesCharge = cpfSalesCharge;
        this.fndCommReqd = fndCommReqd;
        this.srsSalesCharge = srsSalesCharge;
        this.srsDistributorRetention = srsDistributorRetention;
        this.isin = isin;
        this.morningstarId = morningstarId;
        this.fpisFundCategory = fpisFundCategory;
        this.fpisFundmanager = fpisFundmanager;
//        this.fpisPurchaseReqs = fpisPurchaseReqs;
//        this.fpisInvestorHoldingsBals = fpisInvestorHoldingsBals;
//        this.fpisRspSetups = fpisRspSetups;
//        this.fpisDividendReprocessReqs = fpisDividendReprocessReqs;
//        this.fpisInvestorAgentBals = fpisInvestorAgentBals;
//        this.fpisNavs = fpisNavs;
//        this.fpisDividendDeclarations = fpisDividendDeclarations;
//        this.fpisMsNavs = fpisMsNavs;
//        this.fpisDivUnitReconciliates = fpisDivUnitReconciliates;
//        this.fpisTransferToInvestors = fpisTransferToInvestors;
//        this.fpisTransferFromDistributors = fpisTransferFromDistributors;
//        this.fpisSwitchOutReqs = fpisSwitchOutReqs;
//        this.fpisAllocations = fpisAllocations;
//        this.fpisFundStatusHists = fpisFundStatusHists;
//        this.fpisTransferToDistributors = fpisTransferToDistributors;
//        this.fpisAggregations = fpisAggregations;
//        this.fpisTransferFromInvestors = fpisTransferFromInvestors;
//        this.fpisInvestorHoldingsTrans = fpisInvestorHoldingsTrans;
//        this.fpisReinvestOptionReqs = fpisReinvestOptionReqs;
//        this.fpisSwitchInReqs = fpisSwitchInReqs;
//        this.fpisRepurchaseReqs = fpisRepurchaseReqs;
        
    }

    /** default constructor */
    public FpisFund() {
    }

    /** minimal constructor */
    public FpisFund(com.avallis.ekyc.dto.FpisFundPK comp_id, String inputter, Date inpDtTm, String fundName, String fundType, Date fundStartDate, String currency, Double faceValue, int unitDecimals, int priceDecimals, String distributor, String allocationUnitsRoundOff, String nomAccFm, String cpfInvAllowYn, String cpfAccType, String schemeLoc, String srsInvAllowYn, String fatalOverwrite, String planCode, String planName, com.avallis.ekyc.dto.FpisFundCategory fpisFundCategory, com.avallis.ekyc.dto.FpisFundmanager fpisFundmanager, Set fpisPurchaseReqs, Set fpisInvestorHoldingsBals, Set fpisRspSetups, Set fpisDividendReprocessReqs, Set fpisInvestorAgentBals, Set fpisNavs, Set fpisDividendDeclarations, Set fpisMsNavs, Set fpisDivUnitReconciliates, Set fpisTransferToInvestors, Set fpisTransferFromDistributors, Set fpisSwitchOutReqs, Set fpisAllocations, Set fpisFundStatusHists, Set fpisTransferToDistributors, Set fpisAggregations, Set fpisTransferFromInvestors, Set fpisInvestorHoldingsTrans, Set fpisReinvestOptionReqs, Set fpisSwitchInReqs, Set fpisRepurchaseReqs) {
        this.comp_id = comp_id;
        this.inputter = inputter;
        this.inpDtTm = inpDtTm;
        this.fundName = fundName;
        this.fundType = fundType;
        this.fundStartDate = fundStartDate;
        this.currency = currency;
        this.faceValue = faceValue;
        this.unitDecimals = unitDecimals;
        this.priceDecimals = priceDecimals;
        this.distributor = distributor;
        this.allocationUnitsRoundOff = allocationUnitsRoundOff;
        this.nomAccFm = nomAccFm;
        this.cpfInvAllowYn = cpfInvAllowYn;
        this.cpfAccType = cpfAccType;
        this.schemeLoc = schemeLoc;
        this.srsInvAllowYn = srsInvAllowYn;
        this.fatalOverwrite = fatalOverwrite;
        this.planCode = planCode;
        this.planName = planName;
        this.fpisFundCategory = fpisFundCategory;
        this.fpisFundmanager = fpisFundmanager;
//        this.fpisPurchaseReqs = fpisPurchaseReqs;
//        this.fpisInvestorHoldingsBals = fpisInvestorHoldingsBals;
//        this.fpisRspSetups = fpisRspSetups;
//        this.fpisDividendReprocessReqs = fpisDividendReprocessReqs;
//        this.fpisInvestorAgentBals = fpisInvestorAgentBals;
//        this.fpisNavs = fpisNavs;
//        this.fpisDividendDeclarations = fpisDividendDeclarations;
//        this.fpisMsNavs = fpisMsNavs;
//        this.fpisDivUnitReconciliates = fpisDivUnitReconciliates;
//        this.fpisTransferToInvestors = fpisTransferToInvestors;
//        this.fpisTransferFromDistributors = fpisTransferFromDistributors;
//        this.fpisSwitchOutReqs = fpisSwitchOutReqs;
//        this.fpisAllocations = fpisAllocations;
//        this.fpisFundStatusHists = fpisFundStatusHists;
//        this.fpisTransferToDistributors = fpisTransferToDistributors;
//        this.fpisAggregations = fpisAggregations;
//        this.fpisTransferFromInvestors = fpisTransferFromInvestors;
//        this.fpisInvestorHoldingsTrans = fpisInvestorHoldingsTrans;
//        this.fpisReinvestOptionReqs = fpisReinvestOptionReqs;
//        this.fpisSwitchInReqs = fpisSwitchInReqs;
//        this.fpisRepurchaseReqs = fpisRepurchaseReqs;
    }

    public com.avallis.ekyc.dto.FpisFundPK getComp_id() {
        return this.comp_id;
    }

    public void setComp_id(com.avallis.ekyc.dto.FpisFundPK comp_id) {
        this.comp_id = comp_id;
    }

    public String getInputter() {
        return this.inputter;
    }

    public void setInputter(String inputter) {
        this.inputter = inputter;
    }

    public Date getInpDtTm() {
        return this.inpDtTm;
    }

    public void setInpDtTm(Date inpDtTm) {
        this.inpDtTm = inpDtTm;
    }

    public String getAuthoriser() {
        return this.authoriser;
    }

    public void setAuthoriser(String authoriser) {
        this.authoriser = authoriser;
    }

    public String getFundName() {
        return this.fundName;
    }

    public void setFundName(String fundName) {
        this.fundName = fundName;
    }

    public String getMnemonic() {
        return this.mnemonic;
    }

    public void setMnemonic(String mnemonic) {
        this.mnemonic = mnemonic;
    }

    public String getFundNumber() {
        return this.fundNumber;
    }

    public void setFundNumber(String fundNumber) {
        this.fundNumber = fundNumber;
    }

    public String getFundType() {
        return this.fundType;
    }

    public void setFundType(String fundType) {
        this.fundType = fundType;
    }

    public Date getFundStartDate() {
        return this.fundStartDate;
    }

    public void setFundStartDate(Date fundStartDate) {
        this.fundStartDate = fundStartDate;
    }

    public Date getFundEndDate() {
        return this.fundEndDate;
    }

    public void setFundEndDate(Date fundEndDate) {
        this.fundEndDate = fundEndDate;
    }

    public Date getRepsStartDt() {
        return this.repsStartDt;
    }

    public void setRepsStartDt(Date repsStartDt) {
        this.repsStartDt = repsStartDt;
    }

    public String getCurrency() {
        return this.currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Double getFaceValue() {
        return this.faceValue;
    }

    public void setFaceValue(Double faceValue) {
        this.faceValue = faceValue;
    }

    public Integer getLockPeriod() {
        return this.lockPeriod;
    }

    public void setLockPeriod(Integer lockPeriod) {
        this.lockPeriod = lockPeriod;
    }

    public Double getApprovedFundUnits() {
        return this.approvedFundUnits;
    }

    public void setApprovedFundUnits(Double approvedFundUnits) {
        this.approvedFundUnits = approvedFundUnits;
    }

    public Double getMinCreatLimit() {
        return this.minCreatLimit;
    }

    public void setMinCreatLimit(Double minCreatLimit) {
        this.minCreatLimit = minCreatLimit;
    }

    public Double getMinCanclLimit() {
        return this.minCanclLimit;
    }

    public void setMinCanclLimit(Double minCanclLimit) {
        this.minCanclLimit = minCanclLimit;
    }

    public Double getMinBalCirculation() {
        return this.minBalCirculation;
    }

    public void setMinBalCirculation(Double minBalCirculation) {
        this.minBalCirculation = minBalCirculation;
    }

    public String getPricingMethod() {
        return this.pricingMethod;
    }

    public void setPricingMethod(String pricingMethod) {
        this.pricingMethod = pricingMethod;
    }

    public int getUnitDecimals() {
        return this.unitDecimals;
    }

    public void setUnitDecimals(int unitDecimals) {
        this.unitDecimals = unitDecimals;
    }

    public String getUnitRoundoffFlag() {
        return this.unitRoundoffFlag;
    }

    public void setUnitRoundoffFlag(String unitRoundoffFlag) {
        this.unitRoundoffFlag = unitRoundoffFlag;
    }

    public String getAmountRoundoffFlag() {
        return this.amountRoundoffFlag;
    }

    public void setAmountRoundoffFlag(String amountRoundoffFlag) {
        this.amountRoundoffFlag = amountRoundoffFlag;
    }

    public String getSchemeSrlNo() {
        return this.schemeSrlNo;
    }

    public void setSchemeSrlNo(String schemeSrlNo) {
        this.schemeSrlNo = schemeSrlNo;
    }

    public int getPriceDecimals() {
        return this.priceDecimals;
    }

    public void setPriceDecimals(int priceDecimals) {
        this.priceDecimals = priceDecimals;
    }

    public Double getMaxUnitsHolding() {
        return this.maxUnitsHolding;
    }

    public void setMaxUnitsHolding(Double maxUnitsHolding) {
        this.maxUnitsHolding = maxUnitsHolding;
    }

    public String getDistributor() {
        return this.distributor;
    }

    public void setDistributor(String distributor) {
        this.distributor = distributor;
    }

    public String getAllocationUnitsRoundOff() {
        return this.allocationUnitsRoundOff;
    }

    public void setAllocationUnitsRoundOff(String allocationUnitsRoundOff) {
        this.allocationUnitsRoundOff = allocationUnitsRoundOff;
    }

    public String getNomAccFm() {
        return this.nomAccFm;
    }

    public void setNomAccFm(String nomAccFm) {
        this.nomAccFm = nomAccFm;
    }

    public String getCpfInvAllowYn() {
        return this.cpfInvAllowYn;
    }

    public void setCpfInvAllowYn(String cpfInvAllowYn) {
        this.cpfInvAllowYn = cpfInvAllowYn;
    }

    public String getCpfAccType() {
        return this.cpfAccType;
    }

    public void setCpfAccType(String cpfAccType) {
        this.cpfAccType = cpfAccType;
    }

    public String getSchemeLoc() {
        return this.schemeLoc;
    }

    public void setSchemeLoc(String schemeLoc) {
        this.schemeLoc = schemeLoc;
    }

    public String getSrsInvAllowYn() {
        return this.srsInvAllowYn;
    }

    public void setSrsInvAllowYn(String srsInvAllowYn) {
        this.srsInvAllowYn = srsInvAllowYn;
    }

    public String getBenchmarkCode() {
        return this.benchmarkCode;
    }

    public void setBenchmarkCode(String benchmarkCode) {
        this.benchmarkCode = benchmarkCode;
    }

    public String getPaymentGrossNet() {
        return this.paymentGrossNet;
    }

    public void setPaymentGrossNet(String paymentGrossNet) {
        this.paymentGrossNet = paymentGrossNet;
    }

    public String getAggregationRqrd() {
        return this.aggregationRqrd;
    }

    public void setAggregationRqrd(String aggregationRqrd) {
        this.aggregationRqrd = aggregationRqrd;
    }

    public String getLipperCode() {
        return this.lipperCode;
    }

    public void setLipperCode(String lipperCode) {
        this.lipperCode = lipperCode;
    }

    public String getNavFreq() {
        return this.navFreq;
    }

    public void setNavFreq(String navFreq) {
        this.navFreq = navFreq;
    }

    public Double getMinUnitsRedem() {
        return this.minUnitsRedem;
    }

    public void setMinUnitsRedem(Double minUnitsRedem) {
        this.minUnitsRedem = minUnitsRedem;
    }

    public Double getMinInvestAmount() {
        return this.minInvestAmount;
    }

    public void setMinInvestAmount(Double minInvestAmount) {
        this.minInvestAmount = minInvestAmount;
    }

    public Double getMinRedemAmount() {
        return this.minRedemAmount;
    }

    public void setMinRedemAmount(Double minRedemAmount) {
        this.minRedemAmount = minRedemAmount;
    }

    public String getCutoffTime() {
        return this.cutoffTime;
    }

    public void setCutoffTime(String cutoffTime) {
        this.cutoffTime = cutoffTime;
    }

    public String getGrowthDividendFlag() {
        return this.growthDividendFlag;
    }

    public void setGrowthDividendFlag(String growthDividendFlag) {
        this.growthDividendFlag = growthDividendFlag;
    }

    public Double getMinBalUnits() {
        return this.minBalUnits;
    }

    public void setMinBalUnits(Double minBalUnits) {
        this.minBalUnits = minBalUnits;
    }

    public Double getMaxUnitsRedem() {
        return this.maxUnitsRedem;
    }

    public void setMaxUnitsRedem(Double maxUnitsRedem) {
        this.maxUnitsRedem = maxUnitsRedem;
    }

    public String getBusinessStartTime() {
        return this.businessStartTime;
    }

    public void setBusinessStartTime(String businessStartTime) {
        this.businessStartTime = businessStartTime;
    }

    public String getBusinessEndTime() {
        return this.businessEndTime;
    }

    public void setBusinessEndTime(String businessEndTime) {
        this.businessEndTime = businessEndTime;
    }

    public String getFatalOverwrite() {
        return this.fatalOverwrite;
    }

    public void setFatalOverwrite(String fatalOverwrite) {
        this.fatalOverwrite = fatalOverwrite;
    }

    public Double getMinAddInvestAmount() {
        return this.minAddInvestAmount;
    }

    public void setMinAddInvestAmount(Double minAddInvestAmount) {
        this.minAddInvestAmount = minAddInvestAmount;
    }

    public Double getMinTransferUnits() {
        return this.minTransferUnits;
    }

    public void setMinTransferUnits(Double minTransferUnits) {
        this.minTransferUnits = minTransferUnits;
    }

    public Double getMinTransferAmount() {
        return this.minTransferAmount;
    }

    public void setMinTransferAmount(Double minTransferAmount) {
        this.minTransferAmount = minTransferAmount;
    }

    public Double getMinSsAmount() {
        return this.minSsAmount;
    }

    public void setMinSsAmount(Double minSsAmount) {
        this.minSsAmount = minSsAmount;
    }

    public Double getMinSsUnits() {
        return this.minSsUnits;
    }

    public void setMinSsUnits(Double minSsUnits) {
        this.minSsUnits = minSsUnits;
    }

    public Double getMinBalAmount() {
        return this.minBalAmount;
    }

    public void setMinBalAmount(Double minBalAmount) {
        this.minBalAmount = minBalAmount;
    }

    public Double getMinShAmount() {
        return this.minShAmount;
    }

    public void setMinShAmount(Double minShAmount) {
        this.minShAmount = minShAmount;
    }

    public Double getMinShUnits() {
        return this.minShUnits;
    }

    public void setMinShUnits(Double minShUnits) {
        this.minShUnits = minShUnits;
    }

    public String getReinvestFlag() {
        return this.reinvestFlag;
    }

    public void setReinvestFlag(String reinvestFlag) {
        this.reinvestFlag = reinvestFlag;
    }

    public String getSwpFixedDate() {
        return this.swpFixedDate;
    }

    public void setSwpFixedDate(String swpFixedDate) {
        this.swpFixedDate = swpFixedDate;
    }

    public String getSwpDayOne() {
        return this.swpDayOne;
    }

    public void setSwpDayOne(String swpDayOne) {
        this.swpDayOne = swpDayOne;
    }

    public String getSwpDayTwo() {
        return this.swpDayTwo;
    }

    public void setSwpDayTwo(String swpDayTwo) {
        this.swpDayTwo = swpDayTwo;
    }

    public Double getSwpMMinUnits() {
        return this.swpMMinUnits;
    }

    public void setSwpMMinUnits(Double swpMMinUnits) {
        this.swpMMinUnits = swpMMinUnits;
    }

    public Double getSwpQMinUnits() {
        return this.swpQMinUnits;
    }

    public void setSwpQMinUnits(Double swpQMinUnits) {
        this.swpQMinUnits = swpQMinUnits;
    }

    public Double getSwpHMinUnits() {
        return this.swpHMinUnits;
    }

    public void setSwpHMinUnits(Double swpHMinUnits) {
        this.swpHMinUnits = swpHMinUnits;
    }

    public Double getSwpYMinUnits() {
        return this.swpYMinUnits;
    }

    public void setSwpYMinUnits(Double swpYMinUnits) {
        this.swpYMinUnits = swpYMinUnits;
    }

    public Double getMinUnitsSwp() {
        return this.minUnitsSwp;
    }

    public void setMinUnitsSwp(Double minUnitsSwp) {
        this.minUnitsSwp = minUnitsSwp;
    }

    public Double getSwpMinBalEnroll() {
        return this.swpMinBalEnroll;
    }

    public void setSwpMinBalEnroll(Double swpMinBalEnroll) {
        this.swpMinBalEnroll = swpMinBalEnroll;
    }

    public String getSalesChargeAllow() {
        return this.salesChargeAllow;
    }

    public void setSalesChargeAllow(String salesChargeAllow) {
        this.salesChargeAllow = salesChargeAllow;
    }

    public String getSwitchingOption() {
        return this.switchingOption;
    }

    public void setSwitchingOption(String switchingOption) {
        this.switchingOption = switchingOption;
    }

    public String getShiftingOption() {
        return this.shiftingOption;
    }

    public void setShiftingOption(String shiftingOption) {
        this.shiftingOption = shiftingOption;
    }

    public Integer getFreeSwitches() {
        return this.freeSwitches;
    }

    public void setFreeSwitches(Integer freeSwitches) {
        this.freeSwitches = freeSwitches;
    }

    public String getFreeSwitchesFreq() {
        return this.freeSwitchesFreq;
    }

    public void setFreeSwitchesFreq(String freeSwitchesFreq) {
        this.freeSwitchesFreq = freeSwitchesFreq;
    }

    public String getFreeSwitchesChargeYn() {
        return this.freeSwitchesChargeYn;
    }

    public void setFreeSwitchesChargeYn(String freeSwitchesChargeYn) {
        this.freeSwitchesChargeYn = freeSwitchesChargeYn;
    }

    public String getSwitchShiftCharge() {
        return this.switchShiftCharge;
    }

    public void setSwitchShiftCharge(String switchShiftCharge) {
        this.switchShiftCharge = switchShiftCharge;
    }

    public String getShexitFeeYn() {
        return this.shexitFeeYn;
    }

    public void setShexitFeeYn(String shexitFeeYn) {
        this.shexitFeeYn = shexitFeeYn;
    }

    public String getSwshexitFee() {
        return this.swshexitFee;
    }

    public void setSwshexitFee(String swshexitFee) {
        this.swshexitFee = swshexitFee;
    }

    public String getRedemFlag() {
        return this.redemFlag;
    }

    public void setRedemFlag(String redemFlag) {
        this.redemFlag = redemFlag;
    }

    public Integer getCoolOffDays() {
        return this.coolOffDays;
    }

    public void setCoolOffDays(Integer coolOffDays) {
        this.coolOffDays = coolOffDays;
    }

    public String getRedemExitFeeYn() {
        return this.redemExitFeeYn;
    }

    public void setRedemExitFeeYn(String redemExitFeeYn) {
        this.redemExitFeeYn = redemExitFeeYn;
    }

    public Integer getRedemfreeExit() {
        return this.redemfreeExit;
    }

    public void setRedemfreeExit(Integer redemfreeExit) {
        this.redemfreeExit = redemfreeExit;
    }

    public String getRedemexitFee() {
        return this.redemexitFee;
    }

    public void setRedemexitFee(String redemexitFee) {
        this.redemexitFee = redemexitFee;
    }

    public String getTransferOption() {
        return this.transferOption;
    }

    public void setTransferOption(String transferOption) {
        this.transferOption = transferOption;
    }

    public String getTransferexitFeeOption() {
        return this.transferexitFeeOption;
    }

    public void setTransferexitFeeOption(String transferexitFeeOption) {
        this.transferexitFeeOption = transferexitFeeOption;
    }

    public Integer getTransferfreeExit() {
        return this.transferfreeExit;
    }

    public void setTransferfreeExit(Integer transferfreeExit) {
        this.transferfreeExit = transferfreeExit;
    }

    public String getTransferexitFee() {
        return this.transferexitFee;
    }

    public void setTransferexitFee(String transferexitFee) {
        this.transferexitFee = transferexitFee;
    }

    public String getShareType() {
        return this.shareType;
    }

    public void setShareType(String shareType) {
        this.shareType = shareType;
    }

    public String getPriceOption() {
        return this.priceOption;
    }

    public void setPriceOption(String priceOption) {
        this.priceOption = priceOption;
    }

    public Integer getSettlementDaysPur() {
        return this.settlementDaysPur;
    }

    public void setSettlementDaysPur(Integer settlementDaysPur) {
        this.settlementDaysPur = settlementDaysPur;
    }

    public Integer getSettlementDaysRedm() {
        return this.settlementDaysRedm;
    }

    public void setSettlementDaysRedm(Integer settlementDaysRedm) {
        this.settlementDaysRedm = settlementDaysRedm;
    }

    public String getPlanCode() {
        return this.planCode;
    }

    public void setPlanCode(String planCode) {
        this.planCode = planCode;
    }

    public String getPlanName() {
        return this.planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getModifier() {
        return this.modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public Date getModDtTm() {
        return this.modDtTm;
    }

    public void setModDtTm(Date modDtTm) {
        this.modDtTm = modDtTm;
    }

    public Double getSalesCharge() {
        return this.salesCharge;
    }

    public void setSalesCharge(Double salesCharge) {
        this.salesCharge = salesCharge;
    }

    public Double getDistributorRetention() {
        return this.distributorRetention;
    }

    public void setDistributorRetention(Double distributorRetention) {
        this.distributorRetention = distributorRetention;
    }

    public Double getCpfDistributorRetention() {
        return this.cpfDistributorRetention;
    }

    public void setCpfDistributorRetention(Double cpfDistributorRetention) {
        this.cpfDistributorRetention = cpfDistributorRetention;
    }

    public Double getCpfSalesCharge() {
        return this.cpfSalesCharge;
    }

    public void setCpfSalesCharge(Double cpfSalesCharge) {
        this.cpfSalesCharge = cpfSalesCharge;
    }

    public String getFndCommReqd() {
        return this.fndCommReqd;
    }

    public void setFndCommReqd(String fndCommReqd) {
        this.fndCommReqd = fndCommReqd;
    }

    public Double getSrsSalesCharge() {
        return this.srsSalesCharge;
    }

    public void setSrsSalesCharge(Double srsSalesCharge) {
        this.srsSalesCharge = srsSalesCharge;
    }

    public Double getSrsDistributorRetention() {
        return this.srsDistributorRetention;
    }

    public void setSrsDistributorRetention(Double srsDistributorRetention) {
        this.srsDistributorRetention = srsDistributorRetention;
    }

    public String getIsin() {
        return this.isin;
    }

    public void setIsin(String isin) {
        this.isin = isin;
    }

    public String getMorningstarId() {
        return this.morningstarId;
    }

    public void setMorningstarId(String morningstarId) {
        this.morningstarId = morningstarId;
    }

    public com.avallis.ekyc.dto.FpisFundCategory getFpisFundCategory() {
        return this.fpisFundCategory;
    }

    public void setFpisFundCategory(com.avallis.ekyc.dto.FpisFundCategory fpisFundCategory) {
        this.fpisFundCategory = fpisFundCategory;
    }

    public com.avallis.ekyc.dto.FpisFundmanager getFpisFundmanager() {
        return this.fpisFundmanager;
    }

    public void setFpisFundmanager(com.avallis.ekyc.dto.FpisFundmanager fpisFundmanager) {
        this.fpisFundmanager = fpisFundmanager;
    }

//    public Set getFpisPurchaseReqs() {
//        return this.fpisPurchaseReqs;
//    }
//
//    public void setFpisPurchaseReqs(Set fpisPurchaseReqs) {
//        this.fpisPurchaseReqs = fpisPurchaseReqs;
//    }
//
//    public Set getFpisInvestorHoldingsBals() {
//        return this.fpisInvestorHoldingsBals;
//    }
//
//    public void setFpisInvestorHoldingsBals(Set fpisInvestorHoldingsBals) {
//        this.fpisInvestorHoldingsBals = fpisInvestorHoldingsBals;
//    }
//
//    public Set getFpisRspSetups() {
//        return this.fpisRspSetups;
//    }
//
//    public void setFpisRspSetups(Set fpisRspSetups) {
//        this.fpisRspSetups = fpisRspSetups;
//    }
//
//    public Set getFpisDividendReprocessReqs() {
//        return this.fpisDividendReprocessReqs;
//    }
//
//    public void setFpisDividendReprocessReqs(Set fpisDividendReprocessReqs) {
//        this.fpisDividendReprocessReqs = fpisDividendReprocessReqs;
//    }
//
//    public Set getFpisInvestorAgentBals() {
//        return this.fpisInvestorAgentBals;
//    }
//
//    public void setFpisInvestorAgentBals(Set fpisInvestorAgentBals) {
//        this.fpisInvestorAgentBals = fpisInvestorAgentBals;
//    }
//
//    public Set getFpisNavs() {
//        return this.fpisNavs;
//    }
//
//    public void setFpisNavs(Set fpisNavs) {
//        this.fpisNavs = fpisNavs;
//    }
//
//    public Set getFpisDividendDeclarations() {
//        return this.fpisDividendDeclarations;
//    }
//
//    public void setFpisDividendDeclarations(Set fpisDividendDeclarations) {
//        this.fpisDividendDeclarations = fpisDividendDeclarations;
//    }
//
//    public Set getFpisMsNavs() {
//        return this.fpisMsNavs;
//    }
//
//    public void setFpisMsNavs(Set fpisMsNavs) {
//        this.fpisMsNavs = fpisMsNavs;
//    }
//
//    public Set getFpisDivUnitReconciliates() {
//        return this.fpisDivUnitReconciliates;
//    }
//
//    public void setFpisDivUnitReconciliates(Set fpisDivUnitReconciliates) {
//        this.fpisDivUnitReconciliates = fpisDivUnitReconciliates;
//    }
//
//    public Set getFpisTransferToInvestors() {
//        return this.fpisTransferToInvestors;
//    }
//
//    public void setFpisTransferToInvestors(Set fpisTransferToInvestors) {
//        this.fpisTransferToInvestors = fpisTransferToInvestors;
//    }
//
//    public Set getFpisTransferFromDistributors() {
//        return this.fpisTransferFromDistributors;
//    }
//
//    public void setFpisTransferFromDistributors(Set fpisTransferFromDistributors) {
//        this.fpisTransferFromDistributors = fpisTransferFromDistributors;
//    }
//
//    public Set getFpisSwitchOutReqs() {
//        return this.fpisSwitchOutReqs;
//    }
//
//    public void setFpisSwitchOutReqs(Set fpisSwitchOutReqs) {
//        this.fpisSwitchOutReqs = fpisSwitchOutReqs;
//    }
//
//    public Set getFpisAllocations() {
//        return this.fpisAllocations;
//    }
//
//    public void setFpisAllocations(Set fpisAllocations) {
//        this.fpisAllocations = fpisAllocations;
//    }
//
//    public Set getFpisFundStatusHists() {
//        return this.fpisFundStatusHists;
//    }
//
//    public void setFpisFundStatusHists(Set fpisFundStatusHists) {
//        this.fpisFundStatusHists = fpisFundStatusHists;
//    }
//
//    public Set getFpisTransferToDistributors() {
//        return this.fpisTransferToDistributors;
//    }
//
//    public void setFpisTransferToDistributors(Set fpisTransferToDistributors) {
//        this.fpisTransferToDistributors = fpisTransferToDistributors;
//    }
//
//    public Set getFpisAggregations() {
//        return this.fpisAggregations;
//    }
//
//    public void setFpisAggregations(Set fpisAggregations) {
//        this.fpisAggregations = fpisAggregations;
//    }
//
//    public Set getFpisTransferFromInvestors() {
//        return this.fpisTransferFromInvestors;
//    }
//
//    public void setFpisTransferFromInvestors(Set fpisTransferFromInvestors) {
//        this.fpisTransferFromInvestors = fpisTransferFromInvestors;
//    }
//
//    public Set getFpisInvestorHoldingsTrans() {
//        return this.fpisInvestorHoldingsTrans;
//    }
//
//    public void setFpisInvestorHoldingsTrans(Set fpisInvestorHoldingsTrans) {
//        this.fpisInvestorHoldingsTrans = fpisInvestorHoldingsTrans;
//    }
//
//    public Set getFpisReinvestOptionReqs() {
//        return this.fpisReinvestOptionReqs;
//    }
//
//    public void setFpisReinvestOptionReqs(Set fpisReinvestOptionReqs) {
//        this.fpisReinvestOptionReqs = fpisReinvestOptionReqs;
//    }
//
//    public Set getFpisSwitchInReqs() {
//        return this.fpisSwitchInReqs;
//    }
//
//    public void setFpisSwitchInReqs(Set fpisSwitchInReqs) {
//        this.fpisSwitchInReqs = fpisSwitchInReqs;
//    }
//
//    public Set getFpisRepurchaseReqs() {
//        return this.fpisRepurchaseReqs;
//    }
//
//    public void setFpisRepurchaseReqs(Set fpisRepurchaseReqs) {
//        this.fpisRepurchaseReqs = fpisRepurchaseReqs;
//    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("comp_id", getComp_id())
            .toString();
    }

    public boolean equals(Object other) {
        if ( (this == other ) ) return true;
        if ( !(other instanceof FpisFund) ) return false;
        FpisFund castOther = (FpisFund) other;
        return new EqualsBuilder()
            .append(this.getComp_id(), castOther.getComp_id())
            .isEquals();
    }

    public int hashCode() {
        return new HashCodeBuilder()
            .append(getComp_id())
            .toHashCode();
    }

}
