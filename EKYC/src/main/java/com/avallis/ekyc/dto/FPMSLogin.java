package com.avallis.ekyc.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreType;

@JsonIgnoreType
public class FPMSLogin {
	
	private String txtFldUserName;
	private String txtFldpassword;
	public String getTxtFldUserName() {
		return txtFldUserName;
	}
	public void setTxtFldUserName(String txtFldUserName) {
		this.txtFldUserName = txtFldUserName;
	}
	public String getTxtFldpassword() {
		return txtFldpassword;
	}
	public void setTxtFldpassword(String txtFldpassword) {
		this.txtFldpassword = txtFldpassword;
	}
	
	
	
	

}
