package com.avallis.ekyc.restservice;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.avallis.ekyc.dto.CustomerAttachments;
import com.avallis.ekyc.dto.CustomerDetails;
import com.avallis.ekyc.dto.FnaDependantDets;
import com.avallis.ekyc.dto.FnaDetails;
import com.avallis.ekyc.dto.FnaExpenditureDets;
import com.avallis.ekyc.dto.FnaFatcaTaxdet;
import com.avallis.ekyc.dto.FnaOthPersBenfDets;
import com.avallis.ekyc.dto.FnaOthPersPep;
import com.avallis.ekyc.dto.FnaOthPersTpp;
import com.avallis.ekyc.dto.FnaPendingRequest;
import com.avallis.ekyc.dto.FnaSelfspouseDets;
import com.avallis.ekyc.services.CustomerAttachService;
import com.avallis.ekyc.services.FNAService;
import com.avallis.ekyc.services.FpmsService;
import com.avallis.ekyc.utils.KycUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.avallis.ekyc.utils.ErrorMsg;


@RestController
@RequestMapping("/fnadetails")
public class FnaDetailsRest {
	//poovathi add on 06-05-2021
        @PostMapping("/registerNew/{fnaIdFlg}")
	public String regFnaDets(@PathVariable("fnaIdFlg") String strFnaIdFlg,@RequestBody  Map<String,Object> map,HttpServletRequest request) throws JsonProcessingException {
		

		FNAService fs = new FNAService();
		
		ObjectMapper mapper = new ObjectMapper();		
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		
		FnaDetails fnaDetDTO = mapper.convertValue(map, FnaDetails.class);
		String fnaid  = fnaDetDTO.getFnaId();
		
                String strFNAId = fs.insertFnaDetails(strFnaIdFlg,fnaDetDTO, request);
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("FNA_ID", strFNAId);
		//System.out.println("FNA_ID---------->"+strFNAId);
        return jsonObject.toString();

	}
    
    
    @GetMapping("/register/Fnadetails/{FnaId}")
    public String regFnaDetss(@PathVariable("FnaId") String strFnaId,HttpServletRequest request) throws JsonProcessingException {
		 
		FNAService fs = new FNAService();
		HttpSession session = request.getSession(false);
		ObjectMapper mapper = new ObjectMapper();		
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		
		String strFNAId="";
		
		List fnaList = new ArrayList();
		fnaList  =fs.findFNADetailsByFnaId(strFnaId);
		int totalFna = fnaList.size();
		
                if (totalFna > 0){
			FnaDetails fnaDet = (FnaDetails) fnaList.get(0);
			strFNAId = fnaDet.getFnaId();
		}
       
       session.setAttribute("CURRENT_FNAID", strFNAId);
		 JSONObject jsonObject = new JSONObject();
		 jsonObject.put("FNA_ID", strFNAId);
       return jsonObject.toString();

	}//end
	
	@PostMapping("/register/personaldetails")
	public String regPersonalDetails(@RequestBody Map<String,Object> map,HttpServletRequest request) throws IllegalAccessException, InvocationTargetException {

		FNAService fs = new FNAService();
		

		HttpSession session = request.getSession(false);
		String strFNAId = (String) session.getAttribute("CURRENT_FNAID");
		
		ObjectMapper mapper = new ObjectMapper();		
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		
		FnaDetails fnaDetDTO = mapper.convertValue(map, FnaDetails.class);
		FnaSelfspouseDets fnaSelfspouseDets = mapper.convertValue(map, FnaSelfspouseDets.class);
		FnaOthPersBenfDets fnaOthBenDTO = mapper.convertValue(map, FnaOthPersBenfDets.class);
		FnaOthPersPep fnaOthPepDTO = mapper.convertValue(map, FnaOthPersPep.class);
		FnaOthPersTpp fnaOthTppDTO = mapper.convertValue(map,FnaOthPersTpp.class);

		List fnaDet = new ArrayList();
		fnaDet.add(0,fnaOthBenDTO);
		fnaDet.add(1,fnaOthPepDTO);
		fnaDet.add(2,fnaOthTppDTO);	
		fnaDetDTO.setFnaId(strFNAId);
				
		String strDataFrmId = fs.insertPersonalDetails(fnaDetDTO,fnaSelfspouseDets,fnaDet,strFNAId, request);
		JSONObject jsnObj = new JSONObject();
		jsnObj.put("DATA_FORM_ID", strDataFrmId);
		
		return jsnObj.toString();

	}

	
	@GetMapping(value="/taxdetails/{fnaId}")
	public List<FnaFatcaTaxdet> getAllDataByFnaId(@PathVariable("fnaId")String fnaId,HttpServletRequest request) {
		FNAService serv = new FNAService();
		return serv.findTaxDetsByFnaId(fnaId);
	}
	
	@GetMapping(value="/taxdetails/taxid/{taxId}")
	public  FnaFatcaTaxdet  getAllDataByTaxId(@PathVariable("taxId")String taxId,HttpServletRequest request) {
		FNAService serv = new FNAService();
		return serv.findTaxDetsByTaxId(taxId);
		
	}
	
	@PostMapping(value = "/taxdetails/register/{fnaId}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public FnaFatcaTaxdet saveFnaData(@RequestBody FnaFatcaTaxdet fnaTax, @PathVariable("fnaId")String fnaId,HttpServletRequest request) {
		FNAService serv = new FNAService();		
		return serv.addTaxDetails(fnaTax,fnaId,request);
	}

	@DeleteMapping(value = "/taxdetails/delete/{taxId}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ErrorMsg> delete(@PathVariable("taxId")String taxId,HttpServletRequest request) {
		ErrorMsg errMsg=new ErrorMsg();
		try {
			FNAService serv = new FNAService();
			FnaFatcaTaxdet tax=new FnaFatcaTaxdet();
			tax.setTaxId(taxId);
			
			serv.deleteTaxDetails(tax);
			errMsg.setErrMsg("Deleted your details!!!");
		}catch(Exception e) {
			errMsg.setErrMsg(e.getMessage());
			return ResponseEntity.status(400).body(errMsg);
		}
		return ResponseEntity.status(200).body(errMsg);
		
	}
	
	@PostMapping("/register/taxform")
	public FnaDetails regFnaTaxDets(@RequestBody  Map<String,Object> map,HttpServletRequest request) throws JsonProcessingException, IllegalArgumentException, IllegalAccessException {

		
		
		FNAService fs = new FNAService();

		ObjectMapper mapper = new ObjectMapper();		
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		
		FnaDetails fnaDetDTO = mapper.convertValue(map, FnaDetails.class);
		FnaSelfspouseDets fnaSSDTO = mapper.convertValue(map,FnaSelfspouseDets.class);
		
		String strUsnotify =  fnaSSDTO.getDfSelfUsnotifyflg();
		String strUsperson = fnaSSDTO.getDfSelfUspersonflg();
		String strUsnotifySps = fnaSSDTO.getDfSpsUsnotifyflg();
		String strUspersonSps = fnaSSDTO.getDfSpsUspersonflg();
		
		/*if(!KycUtils.nullOrBlank(strUsnotify) && strUsnotify.equalsIgnoreCase("Y")) {
			fnaSSDTO.setDfSelfUspersonflg("N");
			fnaSSDTO.setDfSelfUstaxno("");
		}
		
		if(!KycUtils.nullOrBlank(strUsperson) && strUsperson.equalsIgnoreCase("Y")) {
			fnaSSDTO.setDfSelfUsnotifyflg("N");
		}
		
		if(!KycUtils.nullOrBlank(strUsnotifySps) && strUsnotifySps.equalsIgnoreCase("Y")) {
			fnaSSDTO.setDfSpsUspersonflg("N");
			fnaSSDTO.setDfSpsUstaxno("");
		}
		
		if(!KycUtils.nullOrBlank(strUspersonSps) && strUspersonSps.equalsIgnoreCase("Y")) {
			fnaSSDTO.setDfSpsUsnotifyflg("N");
		}*/
		
		
		
		
		FnaDetails fnaDets = fs.updateTaxDets(fnaDetDTO,fnaSSDTO, request);
        return fnaDets;

	}
	
	
	@PostMapping("/register/financewell")
	public FnaExpenditureDets regFnaFinanceWellDets(@RequestBody Map<String,Object> map,HttpServletRequest request) throws IllegalArgumentException, IllegalAccessException  {
		  
		FNAService fs = new FNAService();
		ObjectMapper mapper = new ObjectMapper();
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		
		FnaExpenditureDets fnaExpDTO = mapper.convertValue(map, FnaExpenditureDets.class);
		FnaDetails fnaDetDTO = mapper.convertValue(map, FnaDetails.class);
//		FnaSelfspouseDets fnaSSDTO = mapper.convertValue(map,FnaSelfspouseDets.class);
		return fs.updateFinanceWellDets(fnaDetDTO,fnaExpDTO, request);

	}
	
	@PostMapping("/register/productrecom/")
	public String regProdRecom(@RequestBody  Map<String,Object> map,HttpServletRequest request) throws JsonProcessingException, IllegalArgumentException, IllegalAccessException {

		FNAService fs = new FNAService();

		ObjectMapper mapper = new ObjectMapper();		
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);		
		FnaDetails fnaDetDTO = mapper.convertValue(map, FnaDetails.class);
		FnaDetails fnaObj = fs.updateFNADetails(request,fnaDetDTO);
		
		if (!KycUtils.nullObj(fnaObj) ) {
			
			ObjectMapper fnamapper = new ObjectMapper();
			String strFnaJson = fnamapper.writeValueAsString(fnaObj);
//			request.setAttribute("CURRENT_FNA_DETAILS", strFnaJson);
			return strFnaJson.toString();
		}else{
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("error", "error");
			return jsonObject.toString();	
		}
	}
	
	@PostMapping("/register/switchprod/")
	public String regSwitchProdRecom(@RequestBody  Map<String,Object> map,HttpServletRequest request) throws JsonProcessingException, IllegalArgumentException, IllegalAccessException {

		FNAService fs = new FNAService();

		ObjectMapper mapper = new ObjectMapper();		
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		
		FnaDetails fnaDetDTO = mapper.convertValue(map, FnaDetails.class);
		FnaDetails fnaObj = fs.updateFNADetails(request,fnaDetDTO);
		
		if (!KycUtils.nullObj(fnaObj) ) {
			
			ObjectMapper fnamapper = new ObjectMapper();
			String strFnaJson = fnamapper.writeValueAsString(fnaObj);
//			request.setAttribute("CURRENT_FNA_DETAILS", strFnaJson);
			return strFnaJson.toString();
		}else{
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("error", "error");
			return jsonObject.toString();	
		}
	}
	
	@PostMapping("/register/advicerecomm/")
	public String regAdviceRecom(@RequestBody  Map<String,Object> map,HttpServletRequest request) throws JsonProcessingException, IllegalArgumentException, IllegalAccessException {

		FNAService fs = new FNAService();

		ObjectMapper mapper = new ObjectMapper();		
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		
		FnaDetails fnaDetDTO = mapper.convertValue(map, FnaDetails.class);
		FnaDetails fnaObj = fs.updateFNADetails(request,fnaDetDTO);
		
		if (!KycUtils.nullObj(fnaObj) ) {
			ObjectMapper fnamapper = new ObjectMapper();
			String strFnaJson = fnamapper.writeValueAsString(fnaObj);
//			request.setAttribute("CURRENT_FNA_DETAILS", strFnaJson);
			return strFnaJson.toString();
		}else{
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("error", "error");
			return jsonObject.toString();	
		}
	}
	
	@PostMapping("/register/clientdecl")
	public String regClientDecl(@RequestBody  Map<String,Object> map,HttpServletRequest request) throws JsonProcessingException, IllegalArgumentException, IllegalAccessException {

		FNAService fs = new FNAService();

		ObjectMapper mapper = new ObjectMapper();		
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		
		FnaDetails fnaDetDTO = mapper.convertValue(map, FnaDetails.class);
		FnaDetails fnaObj = fs.updateFNADetails(request,fnaDetDTO);

		if (!KycUtils.nullObj(fnaObj) ) {
			ObjectMapper fnamapper = new ObjectMapper();
			String strFnaJson = fnamapper.writeValueAsString(fnaObj);
//			request.setAttribute("CURRENT_FNA_DETAILS", strFnaJson);
			return strFnaJson.toString();
		}else{
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("error", "error");
			return jsonObject.toString();	
		}
	}
	
	@PostMapping("/register/managerdecl")
	public String regManagerDecl(@RequestBody  Map<String,Object> map,HttpServletRequest request) throws JsonProcessingException, IllegalArgumentException, IllegalAccessException {

		FNAService fs = new FNAService();

		ObjectMapper mapper = new ObjectMapper();		
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		
		FnaDetails fnaDetDTO = mapper.convertValue(map, FnaDetails.class);		 
		FnaDetails fnaObj = fs.updateFNADetails(request,fnaDetDTO);		
		
		if (!KycUtils.nullObj(fnaObj) ) {			
			ObjectMapper fnamapper = new ObjectMapper();
			String strFnaJson = fnamapper.writeValueAsString(fnaObj);
//			request.setAttribute("CURRENT_FNA_DETAILS", strFnaJson);
			return strFnaJson.toString();
		}else{
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("error", "error");
			return jsonObject.toString();	
		}
		
	}
	
	

	@PostMapping(value = "/dependant/register/{fnaId}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public FnaDependantDets saveDependantForm(@RequestBody FnaDependantDets fnaTax, @PathVariable("fnaId")String fnaId,
			HttpServletRequest request) {
		FNAService serv = new FNAService();
		return  serv.addDependantDetails(fnaTax,fnaId,request);
	}
	
	//update dependent data
	
		@PostMapping(value ="/dependant/update/{depnId}", consumes = MediaType.APPLICATION_JSON_VALUE)
		public FnaDependantDets updateDepnDetls(@PathVariable("depnId") String strdepnId,
			   HttpServletRequest request, @RequestBody FnaDependantDets fnaDepn) throws IllegalArgumentException, IllegalAccessException {
			FNAService fs = new FNAService();
			HttpSession session = request.getSession(false);
			String strFNAId = (String) session.getAttribute("CURRENT_FNAID");
			return  fs.updateDepnDetls(fnaDepn,strdepnId,strFNAId,request);

		}

		
		//get Sign Page Related Data
		@GetMapping("/getSignPageData/{fnaId}")
		public String  getClientNricLists(@PathVariable(value = "fnaId")String strFNAId) throws JsonProcessingException {
			FpmsService fs = new FpmsService();
			FNAService fnaserv = new FNAService();
			FnaSelfspouseDets fnaSSObj = fnaserv.findFNASelfSpsDetailsByFnaId(strFNAId);
			ObjectMapper mapper = new ObjectMapper();
			String custJson = mapper.writeValueAsString(fnaSSObj);
			//System.out.println("custJson-------------------->"+custJson);
			return custJson;
		}
		
		//getLatest FnaDetls
		@GetMapping("/getLatestFnaDetls/{fnaId}")
		public String  getLatestFnaDetls(@PathVariable(value = "fnaId")String strFNAId) throws JsonProcessingException {
			FpmsService fs = new FpmsService();
			FNAService fnaserv = new FNAService();
			
//			FnaDetails fnaObj = fnaserv.findFnaLatestDetlsByFnaId(strFNAId);	
			FnaDetails fnaObj = fnaserv.findFnaDetlsJoinByFnaId(strFNAId);
			
			//System.out.println("strFnaJson-------------------->"+fnaObj.getAdviserStaffByMgrId().getAdvstfName());
			//System.out.println("strFnaJson-------------------->"+fnaObj.getAdviserStaffByAdvstfId().getAdvstfName());
			ObjectMapper fnamapper = new ObjectMapper();
			
			
			
			String strFnaJson = fnamapper.writeValueAsString(fnaObj);
			//System.out.println("strFnaJson-------------------->"+strFnaJson);
			
			JSONObject jsonFna = new JSONObject(strFnaJson);
			jsonFna.put("manager_name", fnaObj.getAdviserStaffByMgrId().getAdvstfName());
			jsonFna.put("adviser_name", fnaObj.getAdviserStaffByAdvstfId().getAdvstfName());
			
			//System.out.println("jsonFna-------------------->"+jsonFna);
			
			return jsonFna.toString();
		}
		
		
	/*	//poovathi add on 05-07-2021
		@GetMapping(value="/getAllPendReqMsg/{fnaId}")
		public String  getAllPendReqMsgByFnaId(@PathVariable("fnaId")String fnaId,HttpServletRequest request) {
		        System.out.println("FnaId-------------------->"+fnaId);
			FNAService serv = new FNAService();
			List pendReqList = serv.getAllPendReqMsgByFnaId(fnaId);
                        ObjectMapper fnamapper = new ObjectMapper();
			
			
			
			
			JSONArray retVal = new JSONArray();
			JSONObject jsnObj = new JSONObject();
			
			int pendReqListSize = pendReqList.size();
			
			if(pendReqListSize > 0) {
			    java.util.Iterator ii = pendReqList.iterator();
			    while(ii.hasNext()){
				
				Object[] rows = (Object[])ii.next();
				JSONObject jsn = new JSONObject();
				
				jsn.put("REQID", rows[0]);
				jsn.put("FNA_ID", rows[1]);
				jsn.put("PENREQ_MSG", rows[2]);
				jsn.put("REQ_BY", rows[3]);
				jsn.put("CREATED_BY", rows[4]);
				jsn.put("CREATED_DATE", rows[5]);
				
				retVal.add(jsn);
				
			        
			    }
			   
			}
			
			 //retVal.add(jsnObj);
			System.out.println("retVal------------->"+retVal);
			
			return null;
				//serv.getAllPendReqMsgByFnaId(fnaId);
		}*/
		
		@GetMapping(value="/getAllPendReqMsg/{fnaId}")
		public List<FnaPendingRequest> getAllPendReqMsgByFnaId(@PathVariable("fnaId")String fnaId,HttpServletRequest request) throws JsonProcessingException {
		    //System.out.println("retVal------------->"+fnaId);
			FNAService serv = new FNAService();
			List<FnaPendingRequest> fnaPendReq = serv.getAllPendReqMsgByFnaId(fnaId);
			
			ObjectMapper mapper = new ObjectMapper();
			String custJson = mapper.writeValueAsString(fnaPendReq);
                        return fnaPendReq;
                 }
		
		@GetMapping("/AddNewClient2")
		public String addNewClient2(HttpServletRequest request){
		        HttpSession session = request.getSession();
			session.setAttribute("ADDNEWCLIENT2", "Y");
			
			//poovathi add on 25-11-2021
			session.removeAttribute("CREATE_NEW_FNA_FLG");
			session.removeAttribute("PREV_FNAID");
			
			//poovathi add on 30-12-2021 for prod recomm scrn validation.
			String strProdScrnValidateFlg =  session.getAttribute("PROD_SCRN_VALI_FLG")!= null ? (String) session.getAttribute("PROD_SCRN_VALI_FLG") :""; 
			if("Y".equalsIgnoreCase(strProdScrnValidateFlg)) {
			    session.removeAttribute("PROD_SCRN_VALI_FLG");
			}
			    
			session.removeAttribute("PREV_FNAID");
			
			return "SUCCESS";
		} //end
		
		@GetMapping("/prodScreenValidation")
		public String doprodScreenValidation(HttpServletRequest request){
		        HttpSession session = request.getSession();
		        System.out.println("prodScreenValidation");
			session.setAttribute("PROD_SCRN_VALI_FLG", "Y");
			return "SUCCESS";
		} //end
	
}
