package com.avallis.ekyc.restservice;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.avallis.ekyc.dto.FnaDetails;
import com.avallis.ekyc.dto.FnaFatcaTaxdet;
import com.avallis.ekyc.dto.FnaSelfspouseDets;
import com.avallis.ekyc.services.FNAService;
import com.avallis.ekyc.utils.KycUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class FnaRest {}
