package com.avallis.ekyc.restservice;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.avallis.ekyc.dto.FnaDependantDets;
import com.avallis.ekyc.dto.FnaExpenditureDets;
import com.avallis.ekyc.dto.FnaFatcaTaxdet;
import com.avallis.ekyc.dto.FnaRecomPrdtplanDet;
import com.avallis.ekyc.services.FNAService;
import com.avallis.ekyc.services.FpmsService;
import com.avallis.ekyc.utils.KycUtils;

@RestController
@RequestMapping("/fnaDependent")
public class FnaDependentForm {
	
	
	@SuppressWarnings("unchecked")
	@PostMapping(value="/depnAdd/{FNAID}")		
	public String insertFnaDepntDet(@RequestBody FnaDependantDets fnaDependantDets,@PathVariable("FNAID") String strFNAId){
		FNAService service = new FNAService();
		String strDepnId = service.insertFnaDepntDet(fnaDependantDets,strFNAId);
		return strDepnId;
		
	}
	
	
	
	@PostMapping(value="/addCashFlwAssetLiabDetls/{FNAID}")		
	public String insertDepnCashAssetLiabDetls(@RequestBody FnaExpenditureDets fnaExpnDets,@PathVariable("FNAID") String strFNAId){
		FNAService service = new FNAService();
		String strexpdId = service.insertDepnCashAssetLiabDetls(fnaExpnDets,strFNAId);
		return strexpdId;
		
	}
	
	// delete Dependent Details
	
	@PostMapping(value="/delDepn/{DEPNID}")	
	public void deleteDepnDetls(@PathVariable("DEPNID") String strDepnId,HttpServletRequest request){
		
		FNAService service = new FNAService();
		service.deleteDepnDetls(strDepnId);
		
	}
	
	// edit dependent data
	
	@GetMapping(value="/getDataById/{depnId}")
	public List<?> edtDepnData(@PathVariable("depnId")String strdepnId,HttpServletRequest request) {
		FNAService serv = new FNAService();
		return serv.edtDepnData(strdepnId);
		
	}
	
	

}
