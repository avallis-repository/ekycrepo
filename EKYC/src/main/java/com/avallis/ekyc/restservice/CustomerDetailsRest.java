package com.avallis.ekyc.restservice;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.avallis.ekyc.dto.AdviserStaff;
import com.avallis.ekyc.dto.CustomerDetails;
import com.avallis.ekyc.dto.FnaDetails;
import com.avallis.ekyc.dto.MasterDesignation;
import com.avallis.ekyc.services.AdviserService;
import com.avallis.ekyc.services.CustomerDetsService;
import com.avallis.ekyc.services.FNAService;
import com.avallis.ekyc.services.FpmsService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/customerdetails")
public class CustomerDetailsRest {

	@GetMapping("/getAllClients/{adviserId}")
	public List<CustomerDetails> getAllClientsByAdvId(@PathVariable("adviserId") String strAdvId,@RequestParam(value = "clientname")String strClntName) {
		
		
		CustomerDetsService fs = new CustomerDetsService();
		return fs.getAllClientsByAdvId(strAdvId,strClntName);
	}

	@GetMapping("/getClientsById/{custId}")
	public CustomerDetails getClientsByCustId(
			@PathVariable("custId") String strCustId) {
		CustomerDetsService fs = new CustomerDetsService();
		return fs.getClientsByCustId(strCustId);
	}

	@PostMapping("/register/client")
	public CustomerDetails regClientDets(@RequestBody CustomerDetails customerDetails,
			HttpServletRequest request) {

		CustomerDetsService fs = new CustomerDetsService();

		CustomerDetails custDets = fs.insertCompanyClient(customerDetails, request);

		return custDets;

	}
	
	//get all Nric List of Client
	
	@GetMapping("/getAllNricList/{Nric}")
	public String  getClientNricLists(@PathVariable("Nric") String strClntNric,
			@RequestParam(value = "CustId")String strCustId,@RequestParam(value="adviserId")String strAdvId
			) throws JsonProcessingException {
		CustomerDetsService cs = new CustomerDetsService();
		List<CustomerDetails> custDets = cs.getClientNricLists(strClntNric,strCustId,strAdvId);
		ObjectMapper mapper = new ObjectMapper();
		String custJson = mapper.writeValueAsString(custDets);
		return custJson;
	}
	@GetMapping("/getGloblAcessList")
		@ResponseBody
		public List<AdviserStaff>  getGloblAcessList(@RequestParam(value = "loggedAdvStaffId")String strloggedAdvStaffId,
				@RequestParam(value = "loggedUserStaffType")String strloggedUserStaffType,
		        @RequestParam(value = "loggedUsrDistId")String strloggedUsrDistId) throws JsonProcessingException {
			FpmsService fs = new FpmsService();
			List advDets = fs.getGloblAcessList(strloggedAdvStaffId,strloggedUserStaffType,strloggedUsrDistId);
			List<AdviserStaff> advstaffList = new ArrayList<AdviserStaff>();
			Iterator it = advDets.iterator();
			
			AdviserStaff aa = new AdviserStaff();
			while(it.hasNext()) {
				Object[] row = (Object[]) it.next();
				aa = new AdviserStaff();
				aa.setAdvstfId((String) row[0]);
				aa.setAdvstfName((String) row[1]);
				advstaffList.add(aa);
			}
			return advstaffList;
		}
		
	// company 
		
		@GetMapping("/getGloblAcessListCompany")
		@ResponseBody
		public List<AdviserStaff>  getGloblAcessListCompany(@RequestParam(value = "loggedAdvStaffId")String strloggedAdvStaffId,
				@RequestParam(value = "loggedUserStaffType")String strloggedUserStaffType,
		        @RequestParam(value = "loggedUsrDistId")String strloggedUsrDistId) throws JsonProcessingException {
			AdviserService as = new AdviserService();
			List advDets = as.getActiveAdvisers(strloggedAdvStaffId,strloggedUserStaffType,strloggedUsrDistId);
			List<AdviserStaff> advstaffList = new ArrayList<AdviserStaff>();
			Iterator it = advDets.iterator();
			
			AdviserStaff aa = new AdviserStaff();
			while(it.hasNext()) {
				Object[] row = (Object[]) it.next();
				aa = new AdviserStaff();
				aa.setAdvstfId((String) row[0]);
				aa.setAdvstfName((String) row[1]);
				advstaffList.add(aa);
			}
			return advstaffList;
		}
		
		@GetMapping("/getGloblAcessListComoValues")
		@ResponseBody
		public List<MasterDesignation>  getGloblAcessListCompany(@RequestParam(value = "loggedAdvStaffId")String strloggedAdvStaffId){
			FpmsService fs = new FpmsService();
			List advDets = fs.getGloblAcessListCombo(strloggedAdvStaffId);
			List<MasterDesignation> advstaffList = new ArrayList<MasterDesignation>();
			Iterator it = advDets.iterator();
			
			MasterDesignation masterDeg = new MasterDesignation();
			while(it.hasNext()) {
				Object[] row = (Object[]) it.next();
				masterDeg = new MasterDesignation();
				masterDeg.setMgrFlg((String) row[0]);
				masterDeg.setDesigType((String) row[1]);
				//aa.setAdvstfId((String) row[0]);
				//aa.setAdvstfName((String) row[1]);
				advstaffList.add(masterDeg);
			}
			return advstaffList;
		}
		
		
		
		//poovathi get fnaList on 05-05-2021
	
		@GetMapping("/getAllFnaList/{custId}")
		public String getAllFnaListBycustId(@PathVariable("custId") String strcustId,HttpServletRequest request) throws JsonProcessingException {
			
			FNAService ekycServ = new FNAService();
			List fnaList = new ArrayList();
			fnaList = ekycServ.findFNADetailsByCustId(strcustId);
			ObjectMapper mapper = new ObjectMapper();
			
			//poovathi add on 24-08-2021
			
			HttpSession session = request.getSession(false);
			session.removeAttribute("LAT_ARCHV_FNAID");
			String strLatestFNAId = "";
			int totalFna = fnaList.size();
			
	                if (totalFna > 0){
				FnaDetails fnaDet = (FnaDetails) fnaList.get(0);
				strLatestFNAId = fnaDet.getFnaId();
			}
	       
	              session.setAttribute("LAT_ARCHV_FNAID", strLatestFNAId);
			
//			System.out.println("LAT_ARCHV_FNAID----------->"+strLatestFNAId);
			return mapper.writeValueAsString(fnaList);
		}

		
	

}
