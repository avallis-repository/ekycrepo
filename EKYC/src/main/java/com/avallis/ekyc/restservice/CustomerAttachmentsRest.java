package com.avallis.ekyc.restservice;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.avallis.ekyc.dto.CustomerAttachments;
import com.avallis.ekyc.dto.MasterAttachCateg;
import com.avallis.ekyc.dto.MasterPrincipal;
import com.avallis.ekyc.services.CustomerAttachService;
import com.avallis.ekyc.services.FpmsService;

@RestController
@RequestMapping("/CustAttach")
public class CustomerAttachmentsRest {

	static Logger kyclog = Logger.getLogger(CustomerAttachmentsRest.class.getName());

	@GetMapping("/getAllCustCategList")
	public List<MasterAttachCateg> fileUpload() {

		FpmsService fnaserv = new FpmsService();
		List<MasterAttachCateg> attachCategList = fnaserv.getattachCategList();

		return attachCategList;
	}
	
	@GetMapping("/getAllCustDocs/{custId}")
	public List<CustomerAttachments> getAllCustDocs(@PathVariable("custId")String strCustId) {
		CustomerAttachService fnaserv = new CustomerAttachService();
		List<CustomerAttachments> attachCategList = fnaserv.getCustAttachByCustId(strCustId);
		return attachCategList;
	}
	
	
	//POOVATHI ADD ON 28-05-2021  
        @RequestMapping(value="/getAllNricCustDocs",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)	
        @ResponseBody
	public List<CustomerAttachments> getAllNricCustDocs(@RequestParam("custid")String strCustId,@RequestParam("fnaId")String strFnaId,HttpServletRequest request ) {
        	CustomerAttachService fnaserv = new CustomerAttachService();
			List<CustomerAttachments> attachCategList = fnaserv.getCustAttachByCustIdFnaId(strCustId,strFnaId);
			return attachCategList;
		}

	@PostMapping(value = "/uploaddata", consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
	public String fileUpload(
			@RequestParam("custAttachFileList") CommonsMultipartFile[] fileList,
			@RequestParam("custAttachments") JSONObject[] customerAttachments,
			HttpServletRequest req) throws IOException {
		
		CustomerAttachService fnaserv = new CustomerAttachService();
		fnaserv.uploadCustDocs(fileList, customerAttachments, req);

		return "uploaddone";

	}
	
	//GeAllPrincipalNames for Doc Upload Screen Insurar Combo
	@GetMapping("/getAllInsurPrinList")
	public List<MasterPrincipal> getAllPrincipalList() {
		FpmsService kycser = new FpmsService();
		List<MasterPrincipal> principalList = kycser.populatePrincipal();
        return principalList;
	}
    //End
	
	@PostMapping(value="/delete/{custAttachId}")	
	public void deleteDepnDetls(@PathVariable("custAttachId") String custAttachId,HttpServletRequest request){
		
		CustomerAttachService serivce = new CustomerAttachService();
		serivce.deleteCustAttachemtDetails(custAttachId);
		
	}
	
}
