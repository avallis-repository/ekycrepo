package com.avallis.ekyc.restservice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
//import java.util.Base64;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.QueryParam;
import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
//import org.json.simple.parser.JSONParser;
//import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.avallis.ekyc.controller.LoginController;
import com.avallis.ekyc.dbinterfaces.DBImplementation;
import com.avallis.ekyc.dto.FnaDetails;
import com.avallis.ekyc.dto.FnaSelfspouseDets;
import com.avallis.ekyc.dto.FnaSignature;
import com.avallis.ekyc.services.AdviserService;
import com.avallis.ekyc.services.FNAService;
import com.avallis.ekyc.services.FnaSignatureService;
import com.avallis.ekyc.services.FpmsService;
import com.avallis.ekyc.services.KycService;
//import com.avallis.ekyc.services.MailService;
import com.avallis.ekyc.utils.ApplicationContextUtils;
import com.avallis.ekyc.utils.KycConst;
import com.avallis.ekyc.utils.KycUtils;
import com.avallis.ekyc.utils.QrCode;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;




@RestController
@RequestMapping("/FnaSignature")
public class FnaSignatureRestService {
        //poovathi add on 15-06-2021
       static Logger kyclog = Logger.getLogger(FnaSignatureRestService.class.getName());
    
	
	
	
	
	@GetMapping(value = "/setSignData")
	public List<FnaSignature> setSignData(@PathVariable("fnaId") String fnaId,HttpServletRequest request) {
		
		HttpSession session = request.getSession(false);
		FNAService fnaservice = new FNAService();
		AdviserService as = new AdviserService();
		
		ApplicationContext appCtx = ApplicationContextUtils.getApplicationContext();
	    DBImplementation dbDTO = (DBImplementation)appCtx.getBean("dbImplBean");
	    Map<String,String> sessMap = new HashMap<String,String>();	 
	    String strMgrAdvStfId = "",
				 strMgrEmailId ="",strEmailId="",
				strMgrAdvName= "",
				strMgrAdvInitial="",
				strCustId = "",strAdvName="",strCustName = "";
		
		List advMgrList = as.getFNAAdvMgrDets(null, fnaId);
		 Iterator fnaite = advMgrList.iterator();
		 while(fnaite.hasNext()){
			 FnaDetails fnaDets = (FnaDetails)fnaite.next();
			 
			 strAdvName = KycUtils.nullOrBlank(fnaDets.getAdviserStaffByAdvstfId().getAdvstfName())?"":fnaDets.getAdviserStaffByAdvstfId().getAdvstfName();
			 strEmailId = KycUtils.checkNullVal(fnaDets.getAdviserStaffByAdvstfId())?"":fnaDets.getAdviserStaffByAdvstfId().getEmailId();
			 strMgrAdvName = KycUtils.checkNullVal(fnaDets.getAdviserStaffByMgrId()) ? "" : fnaDets.getAdviserStaffByMgrId().getAdvstfName();
			 
		 }
		 
		 FnaSelfspouseDets lstFnaSelfSps = fnaservice.findFNASelfSpsDetailsByFnaId( fnaId);
		 String strSelfName =  KycUtils.checkNullVal(lstFnaSelfSps.getDfSelfName()) ? "" : lstFnaSelfSps.getDfSelfName();
			String strSpsName =  KycUtils.checkNullVal(lstFnaSelfSps.getDfSpsName()) ? null : lstFnaSelfSps.getDfSpsName();
	    
	        sessMap = (Map<String, String>) session.getAttribute(KycConst.LOGGED_USER_INFO);
		sessMap.put(KycConst.FNA_ADVSTFNAME, strAdvName);
		sessMap.put(KycConst.FNA_ADVSTF_MGRNAME,strMgrAdvName);
		sessMap.put(KycConst.FNA_SELF_NAME,strSelfName);
		sessMap.put(KycConst.FNA_SPOUSE_NAME,strSpsName);
		
		sessMap.put("CURR_ADVSTF_EMAIL", strEmailId);
		sessMap.put("CURR_ADVSTF_NAME", strAdvName);
		sessMap.put("CUST_NAME", strSelfName);
		
		session.setAttribute(KycConst.LOGGED_USER_INFO, sessMap);
		session.setAttribute(KycConst.FNA_SELF_NAME, strSelfName);
		session.setAttribute(KycConst.FNA_SPOUSE_NAME, strSpsName);
		session.setAttribute("CURRENT_FNAID", fnaId);
	    
		return null;
	}

	@GetMapping(value = "/getAllData", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<FnaSignature> getAllFnaSignature() {
		//return signService.getAllData(KycConst.FNA_SIGNATURE);
		return null;
	}

	@GetMapping(value = "/getAllDataById/{fnaId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<FnaSignature> getDataByFnaId(@PathVariable("fnaId") String fnaId) throws IOException, SQLException, JSONException, ParseException {
		//ObjectMapper mapper = new ObjectMapper();
		//String custJson = mapper.writeValueAsString(custDets);
		ApplicationContext appCtx = ApplicationContextUtils.getApplicationContext();
	    DBImplementation dbDTO = (DBImplementation)appCtx.getBean("dbImplBean");
	    

//		KycService kycService=new KycService();
		
		FnaSignatureService signService=new FnaSignatureService();
		List signList=signService.getClientSignAllCol(dbDTO,fnaId,null,null);
		
		
		JSONArray retjsnResultArr = new JSONArray();
		List allSign=new ArrayList();
		int strProdRecomLstLen = signList.size();
                if(strProdRecomLstLen > 0) {
        	 Iterator it = signList.iterator();
			 JSONObject jsnProdTabObj = new JSONObject();
			 while (it.hasNext()) {
				 
				  JSONObject jsnProdRecommObj = new JSONObject();
			        Object[] row = (Object[]) it.next(); 
			        byte[] esignid=(byte[]) row[0];
			        String esign=new String(esignid);
			        //jsnProdRecommObj.put("esignId", Utility.checkNullVal(esign) ? KycConst.STR_NULL_STRING: esign);
    				jsnProdRecommObj.put("fnaDetails", KycUtils.checkNullVal(row[1]) ? KycConst.STR_NULL_STRING: row[1]);
			        jsnProdRecommObj.put("signPerson", KycUtils.checkNullVal(row[2]) ? KycConst.STR_NULL_STRING: row[2]);
			       // jsnProdRecommObj.put("signDate", Utility.checkNullVal(row[5]) ? KycConst.STR_NULL_STRING: row[5]);
			        Clob clb = (Clob)row[3];
					
					BufferedReader bufferRead = null;
					try {
						bufferRead = new BufferedReader(clb.getCharacterStream());
					} catch (SQLException e) {
						kyclog.error("------------>getDataByFnaId ",e);
					}
					StringBuffer str = new StringBuffer();
				       String strng;
				     
					 while ((strng=bufferRead .readLine())!=null) {
					        str.append(strng);
					 }
					 bufferRead.close();
			        jsnProdRecommObj.put("eSign",str.toString());
			        
			        Blob blob = (Blob)row[4];
			        int blobLength = (int) blob.length();  
			        byte[] blobAsBytes = blob.getBytes(1, blobLength);

			        blob.free();
			        jsnProdRecommObj.put("signDocBlob",blobAsBytes);
			        jsnProdRecommObj.put("signDate", KycUtils.checkNullVal(row[5]) ? KycConst.STR_NULL_STRING: row[5]);
			        jsnProdRecommObj.put("pageSecRef", KycUtils.checkNullVal(row[6]) ? KycConst.STR_NULL_STRING: row[6]);
			        
			        ObjectMapper mapp = new ObjectMapper();
		        	FnaSignature signDets=mapp.readValue(jsnProdRecommObj.toString(), FnaSignature.class);
		        	allSign.add(signDets);
			        retjsnResultArr.put(jsnProdRecommObj);
			 }
			 
        }
		
		return allSign;
		
	}

	

	@PutMapping(value = "/updateData", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void updateFnaData(@RequestBody FnaSignature fna) {
		//signService.update(fna);
	}

	
	
	
	
	@GetMapping("/getQrCodeById/{signPerson}")
	public byte[] getQrCodeById(@PathVariable(value="signPerson") String signPerson,
			HttpServletRequest request) throws UnsupportedEncodingException {
		
		String strDelFlag = KycUtils.getParamValue(request, "isDelete");
		FnaSignatureService signService=new FnaSignatureService();
//		
		String fnaId=(String)request.getSession().getAttribute("CURRENT_FNAID");
//		
		//poovathi commented this line on 14-06-2021
		//String url=request.getRequestURL().toString().replaceAll("FnaSignature/getQrCodeById/"+signPerson,"") + "sign"+signPerson+"="+fnaId;
		
		//poovathi add on 15-06-2021
		  String url = request.getRequestURL().toString().replaceAll("FnaSignature/getQrCodeById/"+signPerson,"") ;
		  String strSignPerData = signPerson+"="+fnaId;
		  String strUrlOrg = url+strSignPerData;
		// Encode strSignPerData using BASE64
		  KycUtils kycutil = new KycUtils();
		  String strSgnPerDataEnc = kycutil.encodeURLParam(strSignPerData);
		  String strUrlEncoded = url+"Sign"+strSgnPerDataEnc;    
                  
                  kyclog.info(signPerson+","+fnaId+", "  +strUrlEncoded);
                  
//                  System.out.println(strUrlEncoded);
		  //End 
			
		if(strDelFlag.equalsIgnoreCase("true")) {
			FnaSignature fnaSign=new FnaSignature();
			FnaDetails fd = new FnaDetails();
			fd.setFnaId(fnaId);
			fnaSign.setFnaDetails(fd);
			fnaSign.setSignPerson(signPerson);
			signService.clearSignature(fnaSign);	
		}
		
		QrCode qrCode=new QrCode();
		return qrCode.getQRCodeImage(strUrlEncoded,fnaId,220,220);
	}
	
	@PostMapping(value="/saveFnaSignature")
	public FnaSignature saveFnaSignature(@RequestParam("fnaId")String fnaId,@RequestParam("esign")String esign
			,@RequestParam("signDoc")String signDoc,
			@RequestParam("perType")String perType,
			@RequestParam("curPage")String curPage,@RequestParam("esignId")String esignId,HttpServletRequest request) throws Exception {
		
		//Save fnasignature
		FnaSignature fnaSign=new FnaSignature();
		FnaSignatureService signService=new FnaSignatureService();
		FnaDetails fd = new FnaDetails();
		fd.setFnaId(fnaId);
		fnaSign.setFnaDetails(fd);
        
		JSONParser parser = new JSONParser();
		org.json.simple.JSONObject json = (org.json.simple.JSONObject) parser.parse(esign);
		fnaSign.seteSign(json.toString());
		fnaSign.setSignPerson(perType);
		fnaSign.setSignDate(new Date(System.currentTimeMillis()));
		fnaSign.setPageSecRef(curPage);
		byte[] imageBytes = DatatypeConverter.parseBase64Binary(signDoc);
		fnaSign.setSignDocBlob(imageBytes);
       
       if(!esignId.isEmpty() && esignId !=null) {
    	   //fnaSign.setEsignId(UUID.fromString(esignId));
    	   //signService.(fnaSign);
       }else {
    	   signService.saveSignature(fnaSign);
       }
       
      return fnaSign;
	}
	
	
	@DeleteMapping("/clearByFnaId/{fnaId}")
	public void clearSignByFnaId(@PathVariable(value="fnaId") String strFnaId,HttpServletRequest request) {
		FnaSignatureService signService=new FnaSignatureService();
                signService.clearSignByFnaId(strFnaId);	
	}
	
	

}
