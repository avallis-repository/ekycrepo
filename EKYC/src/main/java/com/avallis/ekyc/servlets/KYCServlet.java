package com.avallis.ekyc.servlets;


import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Blob;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Iterator;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import oracle.jdbc.OracleResultSet;
import oracle.sql.BFILE;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpMethod;

import com.avallis.ekyc.dbinterfaces.DBIntf;
import com.avallis.ekyc.dto.AdviserStaff;
import com.avallis.ekyc.dto.CustomerDetails;
import com.avallis.ekyc.dto.FnaDependantDets;
import com.avallis.ekyc.dto.FnaDetails;
import com.avallis.ekyc.dto.FnaExpenditureDets;
import com.avallis.ekyc.dto.FnaFatcaTaxdet;
import com.avallis.ekyc.dto.FnaNtucPolicyDets;
import com.avallis.ekyc.dto.FnaOtherpersonDets;
import com.avallis.ekyc.dto.FnaRecomFundDet;
import com.avallis.ekyc.dto.FnaRecomPrdtplanDet;
import com.avallis.ekyc.dto.FnaSelfspouseDets;
import com.avallis.ekyc.dto.FnaSwtchrepFundDet;
import com.avallis.ekyc.dto.FnaSwtchrepPlanDet;
import com.avallis.ekyc.services.AdviserService;
import com.avallis.ekyc.services.CustomerAttachService;
import com.avallis.ekyc.services.CustomerDetsService;
import com.avallis.ekyc.services.FNAService;
import com.avallis.ekyc.services.FnaSignatureService;
import com.avallis.ekyc.services.KycService;
import com.avallis.ekyc.utils.GenReport;
import com.avallis.ekyc.utils.KycConst;
import com.avallis.ekyc.utils.KycMail;
import com.avallis.ekyc.utils.KycUtils;
import com.avallis.ekyc.utils.ApplicationContextUtils;


public class KYCServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//private static final String FNA_PENREQ_SEQ = null;
	static Logger kyclog = Logger.getLogger(KYCServlet.class.getName());
	ResourceBundle resource = ResourceBundle.getBundle("AppResource");
	public static ResourceBundle ntucprop = ResourceBundle.getBundle("ntuc");
	private ServletFileUpload uploader = null;
	
	

    public KYCServlet() {
    }
    

    public void init() throws ServletException{
		DiskFileItemFactory fileFactory = new DiskFileItemFactory();
		File filesDir = (File) getServletContext().getAttribute("FILES_DIRS_FILE");
		fileFactory.setRepository(filesDir);
		this.uploader = new ServletFileUpload(fileFactory);
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String dbcall = request.getParameter("dbcall");
		JSONArray retval = new JSONArray();
		PrintWriter out = response.getWriter();


		if(!KycUtils.isValidUSession(request)){
			kyclog.info("----->SESSION_EXPIRY--->");
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("SESSION_EXPIRY", "SESSION_EXPIRY");
			retval.put(jsonObj);
			dbcall = "";
		}


		if(dbcall.equalsIgnoreCase("VER")){
			 retval =  getFnaVersionDets(request);
		}


		if(dbcall.equalsIgnoreCase("INSERT_CUSTOMER_ATTACHMENTS")){
			retval = insertCustAttachDetails(request);
		}


		if(dbcall.equalsIgnoreCase("MANAGER_APPROVE_STATUS")){
			retval = updateManagerApproveDets(request);
		}
		if(dbcall.equalsIgnoreCase("ADMIN_APPROVE_STATUS")){
			retval = adminManagerApproveDets(request);
		}
		if(dbcall.equalsIgnoreCase("COMP_APPROVE_STATUS")){
			retval = compManagerApproveDets(request,response);
		}
		
		
		if(dbcall.equalsIgnoreCase("NONNTUCMGRMAIL")){
			 try {
			    retval = sendMailToManagerNonNtuc(request);
			} catch (Exception e) {
			    // TODO Auto-generated catch block
			    e.printStackTrace();
			}
		}
		
		// vignesh add on 20-05-2021
		if(dbcall.equalsIgnoreCase("GETAPPROVALMANAGERURL")){
			retval = getapprovelmanager(request);
		}
		
		
		//poovathi add on 30-06-2021
		if(dbcall.equalsIgnoreCase("STATUSMAIL")){
			retval = sendStatusMailToAdviser(request);
		}
		
		//poovathi add on 30-06-2021
		/*if(dbcall.equalsIgnoreCase("GETPENDREQDETLS")){
		       retval = getPendReqDetls(request);
		}
		*/
		
		
		out.print(retval);

	}

	public JSONArray getFnaVersionDets(HttpServletRequest request){

		JSONArray retValues = new JSONArray();

		JSONObject jsnFnaTblData = new JSONObject();

		try{

			

			ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
			DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

			String strFnaID  = request.getParameter("FNA_ID")!= null ? request.getParameter("FNA_ID") : "";



			JSONObject jsnFnaSSTblData = new JSONObject();
			JSONObject jsnFnaDepTblData = new JSONObject();
			JSONObject jsnFnaExpntrData = new JSONObject();
			JSONObject jsnFnaBenfTppPepData = new JSONObject();
			JSONObject jsnFnaPrdtPlanDetData =new JSONObject();
			JSONObject jsnFnaFundPlanDetData =new JSONObject();
			JSONObject jsnFnaFatcaTaxDetData = new JSONObject();
			JSONObject jsnFnaFatcaTaxDetDataSps = new JSONObject();
			JSONObject jsnFnaSwRepPlanDetData = new JSONObject();
			JSONObject jsnFnaSwRepFundDetData = new JSONObject();
			JSONObject jsnFnaFbCustRefData = new JSONObject();

			JSONArray jsnFnaDepArr = new JSONArray();
			JSONArray jsnFnaBenfTppPepArr = new JSONArray();
			JSONArray jsnFnaPrdtPlanArr = new JSONArray();
			JSONArray jsnFnaFundPlanArr = new JSONArray();
			JSONArray jsnFnaFatcaTaxArr = new JSONArray();
			JSONArray jsnFnaFatcaTaxArrSps = new JSONArray();
			JSONArray jsnFnaSwRepPlanArr=new JSONArray();
			JSONArray jsnFnaSwRepFundArr=new JSONArray();

			KycService serv = new KycService();
			FnaSignatureService esignserv = new FnaSignatureService();
			CustomerDetsService cs = new CustomerDetsService();
			
			List latestFpmsCustData = new ArrayList();
			latestFpmsCustData = cs.getLatestFpmsCustData(dbDTO, strFnaID);
			Iterator custIte = latestFpmsCustData.iterator();
			String strCustName=KycConst.STR_NULL_STRING,strCustDob=KycConst.STR_NULL_STRING,strCustNric=KycConst.STR_NULL_STRING,
					strCustFin=KycConst.STR_NULL_STRING,strCustPP=KycConst.STR_NULL_STRING,strCustAddr1=KycConst.STR_NULL_STRING,
					strCustAddr2=KycConst.STR_NULL_STRING,strCustAddr3=KycConst.STR_NULL_STRING,strCustCity=KycConst.STR_NULL_STRING,
					strCustState=KycConst.STR_NULL_STRING,strCustCountry=KycConst.STR_NULL_STRING,strCustPostal=KycConst.STR_NULL_STRING,
					strCustNatly=KycConst.STR_NULL_STRING,strCustHeight=KycConst.STR_NULL_STRING,strCustWeight=KycConst.STR_NULL_STRING,
					strCustRace=KycConst.STR_NULL_STRING,strCustSmoker=KycConst.STR_NULL_STRING;
			
			StringBuffer homeaddr=new StringBuffer();
			String strICDetails = KycConst.STR_NULL_STRING;
			String strClientStatus=KycConst.STR_NULL_STRING;
			
			try {
				
			
			while(custIte.hasNext()){
				Object[] objcustfna = (Object[])custIte.next();
				
				CustomerDetails custData = (CustomerDetails)objcustfna[1];
				
				strCustName = KycUtils.getObjValue(custData.getCustName());
				strCustDob = KycUtils.convertObjToString(KycUtils.convertDateToDateDateString(custData.getDob())); ;
				
				strCustNric = KycUtils.getObjValue(custData.getNric());
				strCustFin = KycUtils.getObjValue(custData.getCustFin());
				strCustPP = KycUtils.getObjValue(custData.getCustPassportNum());
				
				strCustAddr1 = KycUtils.getObjValue(custData.getResAddr1());
				strCustAddr2 = KycUtils.getObjValue(custData.getResAddr2());
				strCustAddr3 = KycUtils.getObjValue(custData.getResAddr3());
				strCustCity = KycUtils.getObjValue(custData.getResCity());
				strCustState = KycUtils.getObjValue(custData.getResState());
				strCustCountry = KycUtils.getObjValue(custData.getResCountry());
				strCustPostal = KycUtils.getObjValue(custData.getResPostalcode());
				
				strClientStatus = KycUtils.getObjValue(custData.getCustStatus());
				strCustNatly = KycUtils.getObjValue(custData.getNationality());
				
				strCustHeight=KycUtils.getObjValue(custData.getHeight());
				strCustWeight=KycUtils.getObjValue(custData.getWeight());
				strCustRace=KycUtils.getObjValue(custData.getRace());
				strCustSmoker=KycUtils.getObjValue(custData.getSmokerFlg());
			}
			
			} catch (Exception e) {
				e.printStackTrace();
			}
			strICDetails= !KycUtils.nullOrBlank(strCustNric) ? strCustNric : !KycUtils.nullOrBlank(strCustFin) ? strCustFin : strCustPP;// !KycUtils.checkNullVal(strCustPP) ? strCustPP :"";

			if(!strCustAddr1.isEmpty())homeaddr.append(strCustAddr1).append(",");
			if(!strCustAddr2.isEmpty())homeaddr.append(strCustAddr2).append(",");
			if(!strCustAddr3.isEmpty())homeaddr.append(strCustAddr3).append(",");
			if(!strCustCity.isEmpty())homeaddr.append(strCustCity).append(",");
			if(!strCustState.isEmpty())homeaddr.append(strCustState).append(",");
			if(!strCustCountry.isEmpty())homeaddr.append(strCustCountry).append(",");
			if(!strCustPostal.isEmpty())homeaddr.append(strCustPostal).append(".");
			
			
			List alClassDet = (List) serv.getFnaArchDets(dbDTO, strFnaID);
			Iterator ite = alClassDet.iterator();

			List<FnaDetails> lstFna = new ArrayList<FnaDetails>();
			List<FnaSelfspouseDets> lstFnaSS = new ArrayList<FnaSelfspouseDets>();
			List<FnaExpenditureDets> lstAnnlExpDet = new ArrayList<FnaExpenditureDets>();
			
			while(ite.hasNext()){

				Object[] objcls = (Object[]) ite.next();

				FnaDetails fnadettmp = (FnaDetails) objcls[0];
				lstFna .add(fnadettmp);

				FnaSelfspouseDets ssdtmp=(FnaSelfspouseDets)objcls[1];
				lstFnaSS.add(ssdtmp);

				FnaExpenditureDets expdtmp = (FnaExpenditureDets)objcls[2];
				lstAnnlExpDet.add(expdtmp);

			}

			if(lstFna.size() > 0){

				JSONObject jsnDetailsDataObj = new JSONObject();


				for(FnaDetails fna : lstFna){

					jsnDetailsDataObj.put("txtFldFnaId",!KycUtils.nullOrBlank(fna.getFnaId()) ? fna.getFnaId() : "");
					jsnDetailsDataObj.put("txtFldClntServAdv",!KycUtils.nullObj(fna.getAdviserStaffByAdvstfId())?fna.getAdviserStaffByAdvstfId().getAdvstfId():"" );
					jsnDetailsDataObj.put("txtFldMgrId",!KycUtils.nullObj(fna.getAdviserStaffByMgrId())?fna.getAdviserStaffByMgrId().getAdvstfId() :"");
					jsnDetailsDataObj.put("selFnaSelectedMgrId",!KycUtils.nullObj(fna.getAdviserStaffByMgrId())?fna.getAdviserStaffByMgrId().getAdvstfId() :"");
					
					jsnDetailsDataObj.put("txtFldCustId",!KycUtils.nullObj(fna.getCustId())?fna.getCustId():"" );
//					jsnDetailsDataObj.put("txtFldKycForm",KycUtils.getObjValue(fna.getFnaType()));
					jsnDetailsDataObj.put("ccRepexistinvestflg",KycUtils.getObjValue(fna.getCcRepexistinvestflg()) );
					jsnDetailsDataObj.put("comments",KycUtils.getObjValue(fna.getComments()) );
					jsnDetailsDataObj.put("selKycArchStatus",KycUtils.getObjValue(fna.getArchStatus()) );
					jsnDetailsDataObj.put("selKycSentStatus",KycUtils.getObjValue(fna.getKycSentStatus()) );
					jsnDetailsDataObj.put("selKycMgrApprStatus", KycUtils.getObjValue(fna.getMgrApproveStatus()));
					jsnDetailsDataObj.put("txtFldMgrApprDate",KycUtils.getObjValue(fna.getMgrApproveDate()));
					jsnDetailsDataObj.put("txtFldMgrStsRemarks",KycUtils.getObjValue(fna.getMgrStatusRemarks()));
					jsnDetailsDataObj.put("chkArchClntConsent",KycUtils.getObjValue(fna.getClientConsent()));
					jsnDetailsDataObj.put("chkCustAttachFlg",KycUtils.getObjValue(fna.getCustAttachFlg()));

					//page4
					jsnDetailsDataObj.put("selFatcaBirthPlace",KycUtils.getObjValue(fna.getFatcaBirthplace()));
					jsnDetailsDataObj.put("chkFatcaNoContFlg",KycUtils.getObjValue(fna.getFatcaNocontryFlg()));

					//page12
					jsnDetailsDataObj.put("crAdeqfund",KycUtils.getObjValue(fna.getCrAdeqfund()));
					jsnDetailsDataObj.put("crInvstobj",KycUtils.getObjValue(fna.getCrInvstobj()));
					jsnDetailsDataObj.put("crInvsttimehorizon",KycUtils.getObjValue(fna.getCrInvsttimehorizon()));
					jsnDetailsDataObj.put("crOthconcern",KycUtils.getObjValue(fna.getCrOthconcern()));
					jsnDetailsDataObj.put("crRiskclass",KycUtils.getObjValue(fna.getCrRiskclass()));
					jsnDetailsDataObj.put("crRiskpref",KycUtils.getObjValue(fna.getCrRiskpref()));
					jsnDetailsDataObj.put("crInvstamt",KycUtils.getObjValue(fna.getCrInvstamt()));
					jsnDetailsDataObj.put("crRoi",KycUtils.getObjValue(fna.getCrRoi()));
					jsnDetailsDataObj.put("cdRemarks",KycUtils.getObjValue(fna.getCdRemarks()));
					jsnDetailsDataObj.put("crRiskclassILP",KycUtils.getObjValue(fna.getCrRiskclassILP()));

					//page14
//					MAR_2018 - this prop are moved to self spouse dets
					jsnDetailsDataObj.put("txtFldCDClntAnnlIncm",KycUtils.getObjValue(fna.getCdCustAnnlincome()));
					jsnDetailsDataObj.put("txtFldCDClntEmplr",KycUtils.getObjValue(fna.getCdCustEmplyr()));
					jsnDetailsDataObj.put("txtFldCDSpsAnnlIncm",KycUtils.getObjValue(fna.getCdSpsAnnlincome()));
					jsnDetailsDataObj.put("txtFldCDSpsEmplr",KycUtils.getObjValue(fna.getCdSpsEmplyr()));
					jsnDetailsDataObj.put("txtFldCdMailAddrSelf",KycUtils.getObjValue(fna.getCdCustMailaddr()));
					jsnDetailsDataObj.put("txtFldCdMailAddrSps",KycUtils.getObjValue(fna.getCdSpsMailaddr()));
					jsnDetailsDataObj.put("radCDClntAddr",KycUtils.getObjValue(fna.getCdMailaddrflg()));
					jsnDetailsDataObj.put("txtFldCDMailAddOth",KycUtils.getObjValue(fna.getCdMailaddrOthdet()));
					jsnDetailsDataObj.put("radCDClntMailngAddr",KycUtils.getObjValue(fna.getCdMailaddrresflg()));


					//page15
					jsnDetailsDataObj.put("cdIntrprtflg",KycUtils.getObjValue(fna.getCdIntrprtflg()));
					jsnDetailsDataObj.put("cdBenfownflg",KycUtils.getObjValue(fna.getCdBenfownflg()));
					jsnDetailsDataObj.put("cdTppflg",KycUtils.getObjValue(fna.getCdTppflg()));
					jsnDetailsDataObj.put("cdPepflg",KycUtils.getObjValue(fna.getCdPepflg()));

					//page17
					jsnDetailsDataObj.put("advrecRemarks",KycUtils.getObjValue(fna.getAdvrecRemarks()));

					//page18
					jsnDetailsDataObj.put("swrepConflg",KycUtils.getObjValue(fna.getSwrepConflg()));
					jsnDetailsDataObj.put("swrepConfDets",KycUtils.getObjValue(fna.getSwrepConfDets()));
					jsnDetailsDataObj.put("swrepAdvbyflg",KycUtils.getObjValue(fna.getSwrepAdvbyflg()));
					jsnDetailsDataObj.put("swrepDisadvgflg",KycUtils.getObjValue(fna.getSwrepDisadvgflg()));
					jsnDetailsDataObj.put("swrepProceedflg",KycUtils.getObjValue(fna.getSwrepProceedflg()));
					jsnDetailsDataObj.put("swrepRemarks",KycUtils.getObjValue(fna.getSwrepRemarks()));

					//page19
					jsnDetailsDataObj.put("swrepPpFfRemarks",KycUtils.getObjValue(fna.getSwrepPpFfRemarks()));
					
					jsnDetailsDataObj.put("mgrName",KycUtils.getObjValue(fna.getmgrName()));
					
					
					//page20
					jsnDetailsDataObj.put("advrecReason",KycUtils.getObjValue(fna.getAdvrecReason()));
					jsnDetailsDataObj.put("advrecReason1",KycUtils.getObjValue(fna.getAdvrecReason1()));
					jsnDetailsDataObj.put("advrecReason2",KycUtils.getObjValue(fna.getAdvrecReason2()));
					jsnDetailsDataObj.put("advrecReason3",KycUtils.getObjValue(fna.getAdvrecReason3()));

					//page21
					jsnDetailsDataObj.put("cdackAdvrecomm",KycUtils.getObjValue(fna.getCdackAdvrecomm()));
					jsnDetailsDataObj.put("cdackAgree",KycUtils.getObjValue(fna.getCdackAgree()));

					jsnDetailsDataObj.put("advrecRemarks",KycUtils.getObjValue(fna.getAdvrecRemarks()));
					jsnDetailsDataObj.put("advrecFnanumber",KycUtils.getObjValue(fna.getAdvrecFnanumber()));
					jsnDetailsDataObj.put("cdMrktmatPostalflg",KycUtils.getObjValue(fna.getCdMrktmatPostalflg()));
					jsnDetailsDataObj.put("cdMrktmatEmailflg",KycUtils.getObjValue(fna.getCdMrktmatEmailflg()));

					//page22
					jsnDetailsDataObj.put("advrecRemarks1",KycUtils.getObjValue(fna.getAdvrecRemarks1()));
					jsnDetailsDataObj.put("advrecFnanumber1",KycUtils.getObjValue(fna.getAdvrecFnanumber1()));

					jsnDetailsDataObj.put("advrecRemarks2",KycUtils.getObjValue(fna.getAdvrecRemarks2()));
					jsnDetailsDataObj.put("advrecFnanumber2",KycUtils.getObjValue(fna.getAdvrecFnanumber2()));


					jsnDetailsDataObj.put("ackmgrComments",KycUtils.getObjValue(fna.getAckmgrComments()));
					jsnDetailsDataObj.put("ackmgrFnanumber",KycUtils.getObjValue(fna.getAckmgrFnanumber()));

					jsnDetailsDataObj.put("ackmgrComments1",KycUtils.getObjValue(fna.getAckmgrComments1()));
					jsnDetailsDataObj.put("ackmgrFnanumber1",KycUtils.getObjValue(fna.getAckmgrFnanumber1()));

					jsnDetailsDataObj.put("suprevMgrFlg",KycUtils.getObjValue(fna.getSuprevMgrFlg()));
					jsnDetailsDataObj.put("suprevFollowReason",KycUtils.getObjValue(fna.getSuprevFollowReason()));
					
					jsnDetailsDataObj.put("suprevMgrFlg",KycUtils.getObjValue(fna.getSuprevMgrFlg()));
					jsnDetailsDataObj.put("clientFeedbackFlag",KycUtils.getObjValue(fna.getClientFeedbackFlag()));
					
					jsnDetailsDataObj.put("intprtContact",KycUtils.getObjValue(fna.getIntprtContact()));
					jsnDetailsDataObj.put("intprtName",KycUtils.getObjValue(fna.getIntprtName()));
					jsnDetailsDataObj.put("intprtNames",KycUtils.getObjValue(fna.getIntprtName()));
					jsnDetailsDataObj.put("intprtNric",KycUtils.getObjValue(fna.getIntprtNric()));
					jsnDetailsDataObj.put("intprtNrics",KycUtils.getObjValue(fna.getIntprtNric()));
					jsnDetailsDataObj.put("intprtRelat",KycUtils.getObjValue(fna.getIntprtRelat()));
//				}
					jsnDetailsDataObj.put("advdecOptions",KycUtils.getObjValue(fna.getAdvdecOptions()).equals("\"\"")?"":KycUtils.getObjValue(fna.getAdvdecOptions()));//added by kavi 16/03/2018
					
					jsnDetailsDataObj.put("cdLanguages",KycUtils.getObjValue(fna.getCdLanguages()).equals("\"\"")?"":KycUtils.getObjValue(fna.getCdLanguages()));	
					jsnDetailsDataObj.put("fwrDepntflg",KycUtils.getObjValue(fna.getFwrDepntflg()).equals("\"\"")?"":KycUtils.getObjValue(fna.getFwrDepntflg()));
					jsnDetailsDataObj.put("fwrFinassetflg",KycUtils.getObjValue(fna.getFwrFinassetflg()).equals("\"\"")?"":KycUtils.getObjValue(fna.getFwrFinassetflg()));
					jsnDetailsDataObj.put("prodrecObjectives",KycUtils.getObjValue(fna.getProdrecObjectives()).equals("\"\"")?"":KycUtils.getObjValue(fna.getProdrecObjectives()));
					jsnDetailsDataObj.put("apptypesClient",KycUtils.getObjValue(fna.getApptypesClient()).equals("\"\"")?"":KycUtils.getObjValue(fna.getApptypesClient()));
					jsnDetailsDataObj.put("cdLanguageOth",KycUtils.getObjValue(fna.getCdLanguageOth()).equals("\"\"")?"":KycUtils.getObjValue(fna.getCdLanguageOth()));
					jsnDetailsDataObj.put("callbackCustph",KycUtils.getObjValue(fna.getCallbackCustph()));
					jsnDetailsDataObj.put("callbackBy",KycUtils.getObjValue(fna.getCallbackBy()));
					jsnDetailsDataObj.put("callbackClient",KycUtils.getObjValue(fna.getCallbackClient()));
					jsnDetailsDataObj.put("callbackAdviser",KycUtils.getObjValue(fna.getCallbackAdviser()));
					jsnDetailsDataObj.put("callbackApptdates",KycUtils.convertObjToString(KycUtils.convertDateToDateDateString(fna.getCallbackApptdate())));
					jsnDetailsDataObj.put("callbakOnDates",KycUtils.convertObjToString(KycUtils.convertDateToDateDateString(fna.getCallbakOnDate())));
					jsnDetailsDataObj.put("disclosureFollowups",KycUtils.getObjValue(fna.getDisclosureFollowups()));
					jsnDetailsDataObj.put("callbackFeedbacks",KycUtils.getObjValue(fna.getCallbackFeedbacks()));
					
					
					jsnDetailsDataObj.put("ntucCaseLock",KycUtils.getObjValue(fna.getNtucCaseLock()));
					jsnDetailsDataObj.put("ntucLockedCase",KycUtils.getObjValue(fna.getNtucLockedCase()));
					jsnDetailsDataObj.put("ntucCaseId",KycUtils.getObjValue(fna.getNtucCaseId()));
					jsnDetailsDataObj.put("ntucCaseStatus",KycUtils.getObjValue(fna.getNtucCaseStatus()));
					jsnDetailsDataObj.put("policyCreateFlg",KycUtils.getObjValue(fna.getPolicyCreateFlg()));
					
					jsnDetailsDataObj.put("txtFldCrtdUser",KycUtils.getObjValue(fna.getFnaCreatedBy()));
					
					jsnDetailsDataObj.put("mgrEmailSentFlg",KycUtils.getObjValue(fna.getMgrEmailSentFlg()));
					jsnDetailsDataObj.put("kycSentStatus",KycUtils.getObjValue(fna.getKycSentStatus()));
					jsnDetailsDataObj.put("mgrApproveStatus",KycUtils.getObjValue(fna.getMgrApproveStatus()));					
					jsnDetailsDataObj.put("adminApproveStatus",KycUtils.getObjValue(fna.getAdminApproveStatus()));
					jsnDetailsDataObj.put("compApproveStatus",KycUtils.getObjValue(fna.getCompApproveStatus()));
					
					jsnDetailsDataObj.put("mgrStatusRemarks",KycUtils.getObjValue(fna.getMgrStatusRemarks()));
					jsnDetailsDataObj.put("adminRemarks",KycUtils.getObjValue(fna.getAdminRemarks()));
					jsnDetailsDataObj.put("compRemarks",KycUtils.getObjValue(fna.getCompRemarks()));
					jsnDetailsDataObj.put("cdConsentApprch",KycUtils.getObjValue(fna.getCdConsentApprch()));
					
					
					
					jsnFnaTblData.put("ARCH_FNA_TABLE_DATA", jsnDetailsDataObj);
			}
			}
			if(lstFnaSS.size() > 0){
				JSONObject jsnDetailsDataObj = new JSONObject();
				
				String strFnaCustName=KycConst.STR_NULL_STRING;
				String strFnaCustDOB =KycConst.STR_NULL_STRING; 
				String strFnaCustNric=KycConst.STR_NULL_STRING;
				String strFnaCustResiAddr = KycConst.STR_NULL_STRING;
				String strFnaCustAddr1 = KycConst.STR_NULL_STRING;String strFnaCustAddr2 = KycConst.STR_NULL_STRING;
				String strFnaCustAddr3 = KycConst.STR_NULL_STRING;String strFnaCustCity = KycConst.STR_NULL_STRING;
				String strFnaCustState = KycConst.STR_NULL_STRING;String strFnaCustCountry = KycConst.STR_NULL_STRING;
				String strFnaCustPostal = KycConst.STR_NULL_STRING;
				String strFnaCustNatly = KycConst.STR_NULL_STRING;
				

				for(FnaSelfspouseDets fna : lstFnaSS){
					strFnaCustName  = KycUtils.getObjValue(fna.getDfSelfName());
//					strFnaCustDOB = KycUtils.getObjValue(fna.getTxtFldFnaClntDOB());
					strFnaCustDOB = KycUtils.convertObjToString(KycUtils.convertDateToDateDateString(fna.getDfSelfDob())); 
					strFnaCustNric= KycUtils.getObjValue(fna.getDfSelfNric());
//					System.out.println("strFnaCustNric->"+strFnaCustNric);
					strFnaCustResiAddr = KycUtils.getObjValue(fna.getDfSelfHomeaddr());
					
					strFnaCustAddr1 =KycUtils.getObjValue(fna.getResAddr1());
					strFnaCustAddr2 = KycUtils.getObjValue(fna.getResAddr2());
					strFnaCustAddr3=KycUtils.getObjValue(fna.getResAddr3());
					strFnaCustCity=KycUtils.getObjValue(fna.getResCity());
					strFnaCustState=KycUtils.getObjValue(fna.getResState());
					strFnaCustCountry=KycUtils.getObjValue(fna.getResCountry());
					strFnaCustPostal=KycUtils.getObjValue(fna.getResPostalcode());
					strFnaCustNatly  = KycUtils.getObjValue(fna.getDfSelfNationality());
					
					if(!strClientStatus.equalsIgnoreCase(KycConst.STR_CLNTSTS_CLNT)){
					
						strFnaCustName = strCustName.equalsIgnoreCase(strFnaCustName) ? strFnaCustName : strCustName;
						strFnaCustDOB = KycUtils.nullOrBlank(strCustDob)?strFnaCustDOB:
							strCustDob.equalsIgnoreCase(strFnaCustDOB) ? strFnaCustDOB : strCustDob;
						strFnaCustNric = KycUtils.nullOrBlank(strICDetails)? strFnaCustNric :
							strICDetails.equalsIgnoreCase(strFnaCustNric) ? strFnaCustNric : strICDetails;
						strFnaCustResiAddr = homeaddr.toString().equalsIgnoreCase(strFnaCustResiAddr) ? strFnaCustResiAddr : homeaddr.toString();
						
						strFnaCustAddr1 = strCustAddr1.equalsIgnoreCase(strFnaCustAddr1) ? strFnaCustAddr1 : strCustAddr1;
						strFnaCustAddr2 = strCustAddr2.equalsIgnoreCase(strFnaCustAddr2) ? strFnaCustAddr2 : strCustAddr2;
						strFnaCustAddr3 = strCustAddr3.equalsIgnoreCase(strFnaCustAddr3) ? strFnaCustAddr3 : strCustAddr3;
						strFnaCustCity = strCustCity.equalsIgnoreCase(strFnaCustCity) ? strFnaCustCity : strCustCity;
						strFnaCustState = strCustState.equalsIgnoreCase(strFnaCustState) ? strFnaCustState : strCustState;
						strFnaCustCountry = strCustCountry.equalsIgnoreCase(strFnaCustCountry) ? strFnaCustCountry : strCustCountry;
						strFnaCustPostal = strCustPostal.equalsIgnoreCase(strFnaCustPostal) ? strFnaCustPostal : strCustPostal;
						strFnaCustNatly = strCustNatly.equalsIgnoreCase(strFnaCustNatly) ?strCustNatly : strFnaCustNatly;
					}
//					jsnDetailsDataObj.put("txtFldFnaId",KycUtils.getObjValue(fna.getFnaDetails().getFnaId()));

					jsnDetailsDataObj.put("txtFldFnaDatId",!KycUtils.nullOrBlank(fna.getDataformId()) ? fna.getDataformId() : "");

					jsnDetailsDataObj.put("dfSelfName",strFnaCustName);
					jsnDetailsDataObj.put("txtFldFnaClntName",strFnaCustName);
					jsnDetailsDataObj.put("htxtFldFnaClntName",strFnaCustName);
					
					jsnDetailsDataObj.put("dfSelfDob",strFnaCustDOB);
					jsnDetailsDataObj.put("txtFldFnaClntDOB",strFnaCustDOB);

//					jsnDetailsDataObj.put("selFnaClntNatly",KycUtils.getObjValue(fna.getSelFnaClntNatly()));
					
					if(strFnaCustNatly.equalsIgnoreCase("SG") || strFnaCustNatly.equalsIgnoreCase("SGPR") ||strFnaCustNatly.equalsIgnoreCase("OTH")){
						jsnDetailsDataObj.put("dfSelfNationality",strFnaCustNatly);
						jsnDetailsDataObj.put("selFnaClntNatly",strFnaCustNatly);
	
					}else{
						String strCUstNatlyCond = strFnaCustNatly.equalsIgnoreCase("Singaporean") ? "SG" 
								: strCustNatly.equalsIgnoreCase("Singapore-PR") ? "SGPR" :	"OTH";
						jsnDetailsDataObj.put("dfSelfNationality",strCUstNatlyCond);
						jsnDetailsDataObj.put("selFnaClntNatly",strCUstNatlyCond);
						
					}

					jsnDetailsDataObj.put("dfSelfAge",KycUtils.getObjValue(fna.getDfSelfAge()));
					jsnDetailsDataObj.put("txtFldFnaClntAge",KycUtils.getObjValue(fna.getDfSelfAge()));

					jsnDetailsDataObj.put("dfSelfNric",strFnaCustNric);
					jsnDetailsDataObj.put("txtFldFnaClntNric",strFnaCustNric);
					jsnDetailsDataObj.put("htxtFldFnaClntNric",strFnaCustNric);

					jsnDetailsDataObj.put("dfSelfMartsts",KycUtils.getObjValue(fna.getDfSelfMartsts()));
					jsnDetailsDataObj.put("selFnaClntMartSts",KycUtils.getObjValue(fna.getDfSelfMartsts()));

					jsnDetailsDataObj.put("dfSelfOccpn",KycUtils.getObjValue(fna.getDfSelfOccpn()) );
					jsnDetailsDataObj.put("txtFldFnaClntOccp",KycUtils.getObjValue(fna.getDfSelfOccpn()) );

					jsnDetailsDataObj.put("dfSelfGender",KycUtils.getObjValue(fna.getDfSelfGender()));
					jsnDetailsDataObj.put("selFnaClntSex",KycUtils.getObjValue(fna.getDfSelfGender()));

					jsnDetailsDataObj.put("dfSelfEmpsts",KycUtils.getObjValue(fna.getDfSelfEmpsts()));
					jsnDetailsDataObj.put("txtFldFnaClntEmpSts",KycUtils.getObjValue(fna.getDfSelfEmpsts()));

					jsnDetailsDataObj.put("chkFnaClntSmoke",KycUtils.getObjValue(fna.getDfSelfSmoking()));

					jsnDetailsDataObj.put("dfSelfHomeaddr",strFnaCustResiAddr);
					jsnDetailsDataObj.put("txtFldFnaClntHomeAddr",strFnaCustResiAddr);
					
					jsnDetailsDataObj.put("dfSelfOffaddr",KycUtils.getObjValue(fna.getDfSelfOffaddr()));
					jsnDetailsDataObj.put("txtFldFnaClntoffAddr",KycUtils.getObjValue(fna.getDfSelfOffaddr()));

					jsnDetailsDataObj.put("dfSelfPersemail",KycUtils.getObjValue(fna.getDfSelfPersemail()));
					jsnDetailsDataObj.put("txtFldFnaClntEmail",KycUtils.getObjValue(fna.getDfSelfPersemail()));
					jsnDetailsDataObj.put("htxtFldFnaClntEmail",KycUtils.getObjValue(fna.getDfSelfPersemail()));
					
					jsnDetailsDataObj.put("dfSelfMobile",KycUtils.getObjValue(fna.getDfSelfMobile()));
					jsnDetailsDataObj.put("txtFldFnaClntMobile",KycUtils.getObjValue(fna.getDfSelfMobile()));

					jsnDetailsDataObj.put("dfSelfOffice",KycUtils.getObjValue(fna.getDfSelfOffice()));
					jsnDetailsDataObj.put("txtFldFnaClntOffice",KycUtils.getObjValue(fna.getDfSelfOffice()));
					jsnDetailsDataObj.put("htxtFldFnaClntMobile",KycUtils.getObjValue(fna.getDfSelfOffice()));

					jsnDetailsDataObj.put("dfSelfOffemail",KycUtils.getObjValue(fna.getDfSelfOffemail()));
					jsnDetailsDataObj.put("txtFldFnaClntOffMail",KycUtils.getObjValue(fna.getDfSelfOffemail()));

					jsnDetailsDataObj.put("dfSelfHome",KycUtils.getObjValue(fna.getDfSelfHome()));
					jsnDetailsDataObj.put("txtFldFnaClntHome",KycUtils.getObjValue(fna.getDfSelfHome()));

					jsnDetailsDataObj.put("txtFldFnaClntFax",KycUtils.getObjValue(fna.getDfSelfFax()));
					
					jsnDetailsDataObj.put("dfSpsName",KycUtils.getObjValue(fna.getDfSpsName()));
					jsnDetailsDataObj.put("txtFldFnaSpsName",KycUtils.getObjValue(fna.getDfSpsName()));

//					jsnDetailsDataObj.put("txtFldFnaSpsDob",KycUtils.getObjValue(fna.getTxtFldFnaSpsDob()));
					jsnDetailsDataObj.put("dfSpsDob",KycUtils.convertObjToString(KycUtils.convertDateToDateDateString(fna.getDfSpsDob())));
					jsnDetailsDataObj.put("txtFldFnaSpsDob",KycUtils.convertObjToString(KycUtils.convertDateToDateDateString(fna.getDfSpsDob())));

					jsnDetailsDataObj.put("dfSpsNationality",KycUtils.getObjValue(fna.getDfSpsNationality()));
					jsnDetailsDataObj.put("selFnaSpsNatly",KycUtils.getObjValue(fna.getDfSpsNationality()));

					jsnDetailsDataObj.put("dfSpsAge",KycUtils.getObjValue(fna.getDfSpsAge()));
					jsnDetailsDataObj.put("txtFldFnaSpsAge",KycUtils.getObjValue(fna.getDfSpsAge()));

					jsnDetailsDataObj.put("dfSpsNric",KycUtils.getObjValue(fna.getDfSpsNric()));
					jsnDetailsDataObj.put("txtFldFnaSpsNric",KycUtils.getObjValue(fna.getDfSpsNric()));

					jsnDetailsDataObj.put("dfSpsMartsts",KycUtils.getObjValue(fna.getDfSpsMartsts()));
					jsnDetailsDataObj.put("selFnaSpsMartSts",KycUtils.getObjValue(fna.getDfSpsMartsts()));

					jsnDetailsDataObj.put("dfSpsOccpn",KycUtils.getObjValue(fna.getDfSpsOccpn()));
					jsnDetailsDataObj.put("txtFldFnaSpsOccpn",KycUtils.getObjValue(fna.getDfSpsOccpn()));

					jsnDetailsDataObj.put("dfSpsGender",KycUtils.getObjValue(fna.getDfSpsGender()));
					jsnDetailsDataObj.put("selFnaSpsSex",KycUtils.getObjValue(fna.getDfSpsGender()));

					jsnDetailsDataObj.put("dfSpsEmpsts",KycUtils.getObjValue(fna.getDfSpsEmpsts()));
					jsnDetailsDataObj.put("selFnaSpsEmpSts",KycUtils.getObjValue(fna.getDfSpsEmpsts()));
					
					jsnDetailsDataObj.put("chkFnaSpsSmoke",KycUtils.getObjValue(fna.getDfSpsSmoking()));

					jsnDetailsDataObj.put("dfSpsPersemail",KycUtils.getObjValue(fna.getDfSpsPersemail()));
					jsnDetailsDataObj.put("txtFldFnaSpsEmail",KycUtils.getObjValue(fna.getDfSpsPersemail()));

					jsnDetailsDataObj.put("dfSpsHp",KycUtils.getObjValue(fna.getDfSpsHp()));
					jsnDetailsDataObj.put("txtFldFnaSpsHP",KycUtils.getObjValue(fna.getDfSpsHp()));

					jsnDetailsDataObj.put("dfSpsOffice",KycUtils.getObjValue(fna.getDfSpsOffice()));
					jsnDetailsDataObj.put("txtFldFnaSpsOffice",KycUtils.getObjValue(fna.getDfSpsOffice()));

					jsnDetailsDataObj.put("dfFingoals",KycUtils.getObjValue(fna.getDfFingoals()));
					jsnDetailsDataObj.put("txtFldFnaFinGoal",KycUtils.getObjValue(fna.getDfFingoals()));

					jsnDetailsDataObj.put("dfSpsHome",KycUtils.getObjValue(fna.getDfSpsHome() ));
					jsnDetailsDataObj.put("txtFldFnaSpsHome",KycUtils.getObjValue(fna.getDfSpsHome() ));
					
					jsnDetailsDataObj.put("dfSpsHomeaddr",KycUtils.getObjValue(fna.getDfSpsHomeaddr()));
					jsnDetailsDataObj.put("txtFldFnaSpsHomeAddr",KycUtils.getObjValue(fna.getDfSpsHomeaddr()));

					jsnDetailsDataObj.put("dfSelfCompname",KycUtils.getObjValue(fna.getDfSelfCompname()));
					jsnDetailsDataObj.put("txtFldCDClntEmplr",KycUtils.getObjValue(fna.getDfSelfCompname()));
					
					jsnDetailsDataObj.put("dfSpsCompname",KycUtils.getObjValue(fna.getDfSpsCompname()));
				    jsnDetailsDataObj.put("txtFldCDSpsEmplr",KycUtils.getObjValue(fna.getDfSpsCompname()));
				        
				    jsnDetailsDataObj.put("dfSelfBusinatr",KycUtils.getObjValue(fna.getDfSelfBusinatr()));
				    jsnDetailsDataObj.put("txtFldCDClntNatrOfBusinss",KycUtils.getObjValue(fna.getDfSelfBusinatr()));
				    
				    jsnDetailsDataObj.put("dfSpsBusinatr",KycUtils.getObjValue(fna.getDfSpsBusinatr()));
				    jsnDetailsDataObj.put("txtFldCDSpsNatrOfBusinss",KycUtils.getObjValue(fna.getDfSpsBusinatr()));
				        
				    jsnDetailsDataObj.put("dfSelfBusinatrDets",KycUtils.getObjValue(fna.getDfSelfBusinatrDets()));
			        jsnDetailsDataObj.put("txtFldCDClntNatrOfBusinssOthrs",KycUtils.getObjValue(fna.getDfSelfBusinatrDets()));
			        
			        jsnDetailsDataObj.put("dfSpsBusinatrDets",KycUtils.getObjValue(fna.getDfSpsBusinatrDets()));
			        jsnDetailsDataObj.put("txtFldCDSpsNatrOfBusinssOthrs",KycUtils.getObjValue(fna.getDfSpsBusinatrDets()));
			        
			        jsnDetailsDataObj.put("dfSelfUsnotifyflg",KycUtils.getObjValue(fna.getDfSelfUsnotifyflg()));
			        jsnDetailsDataObj.put("chkCDUSPrsnTaxClnt",KycUtils.getObjValue(fna.getDfSelfUsnotifyflg()));
			        
			        jsnDetailsDataObj.put("dfSpsUsnotifyflg",KycUtils.getObjValue(fna.getDfSpsUsnotifyflg()));
			        jsnDetailsDataObj.put("chkCDUSPrsnTaxSps",KycUtils.getObjValue(fna.getDfSpsUsnotifyflg()));
			        
			        jsnDetailsDataObj.put("dfSelfUspersonflg",KycUtils.getObjValue(fna.getDfSelfUspersonflg()));
			        jsnDetailsDataObj.put("chkCDNotUSPrsnTaxClnt",KycUtils.getObjValue(fna.getDfSelfUspersonflg()));
			        
			        jsnDetailsDataObj.put("dfSpsUspersonflg",KycUtils.getObjValue(fna.getDfSpsUspersonflg()));
			        jsnDetailsDataObj.put("chkCDNotUSPrsnTaxSps",KycUtils.getObjValue(fna.getDfSpsUspersonflg()));
			        
			        jsnDetailsDataObj.put("dfSelfUstaxno",KycUtils.getObjValue(fna.getDfSelfUstaxno()));
			        jsnDetailsDataObj.put("txtFldClntUSTaxNum",KycUtils.getObjValue(fna.getDfSelfUstaxno()));
			        
			        jsnDetailsDataObj.put("dfSpsUstaxno",KycUtils.getObjValue(fna.getDfSpsUstaxno()));
			        jsnDetailsDataObj.put("txtFldSpsUSTaxNum",KycUtils.getObjValue(fna.getDfSpsUstaxno()));
			        
			        jsnDetailsDataObj.put("dfSelfRescountry",KycUtils.getObjValue(fna.getDfSelfRescountry()));
			        jsnDetailsDataObj.put("dfSpsRescountry",KycUtils.getObjValue(fna.getDfSpsRescountry()));

					//page4 -- RE-USE
					jsnDetailsDataObj.put("txtFldSelfRegAddr",strFnaCustResiAddr);
					jsnDetailsDataObj.put("txtFldCDClntName",strFnaCustName);
					jsnDetailsDataObj.put("txtFldCDClntDOB",strFnaCustDOB);
					jsnDetailsDataObj.put("txtFldCDClntNRIC",strFnaCustNric);
					jsnDetailsDataObj.put("txtFldCDClntMrtSts",KycUtils.getObjValue(fna.getDfSelfMartsts()));
					jsnDetailsDataObj.put("txtFldCDClntOccptn",KycUtils.getObjValue(fna.getDfSelfOccpn()));
					jsnDetailsDataObj.put("txtFldCDClntSex",KycUtils.getObjValue(fna.getDfSelfGender()) );
					jsnDetailsDataObj.put("txtFldCDClntEmail",KycUtils.getObjValue(fna.getDfSelfPersemail()));
					jsnDetailsDataObj.put("txtFldCDClntHndPhne",KycUtils.getObjValue(fna.getDfSelfMobile()));
					jsnDetailsDataObj.put("txtFldCDClntOfc",KycUtils.getObjValue(fna.getDfSelfOffice()));
					jsnDetailsDataObj.put("txtFldCDClntCntDets",KycUtils.getObjValue(fna.getDfSelfHome()));
					jsnDetailsDataObj.put("txtFldClntRegAddr",strFnaCustResiAddr);
					jsnDetailsDataObj.put("txtFldCDSpsName",KycUtils.getObjValue(fna.getDfSpsName()));
//					jsnDetailsDataObj.put("txtFldCDSpsDOB",KycUtils.getObjValue(fna.getTxtFldFnaSpsDob()));
					jsnDetailsDataObj.put("txtFldCDSpsDOB",KycUtils.convertObjToString(KycUtils.convertDateToDateDateString(fna.getDfSpsDob())));
					jsnDetailsDataObj.put("txtFldCDSpsNRIC",KycUtils.getObjValue(fna.getDfSpsNric()));
					jsnDetailsDataObj.put("txtFldCDSpsMrtSts",KycUtils.getObjValue(fna.getDfSpsMartsts()));
					jsnDetailsDataObj.put("txtFldCDSpsOccptn",KycUtils.getObjValue(fna.getDfSpsOccpn()));
					jsnDetailsDataObj.put("txtFldCDSpsSex",KycUtils.getObjValue(fna.getDfSpsGender()));
					jsnDetailsDataObj.put("txtFldCDSpsEmail",KycUtils.getObjValue(fna.getDfSpsPersemail()));
					jsnDetailsDataObj.put("txtFldCDSpsHndPhne",KycUtils.getObjValue(fna.getDfSpsHp()));
					jsnDetailsDataObj.put("txtFldCDSpsOfc",KycUtils.getObjValue(fna.getDfSpsOffice()));
					jsnDetailsDataObj.put("txtFldCDSpsCntDets", "");
					jsnDetailsDataObj.put("txtFldSpsRegAddr", strFnaCustResiAddr);

					//page 23 -- RE-USE
					//2015_05_25
					jsnDetailsDataObj.put("txtFldClientName",strFnaCustName);
					jsnDetailsDataObj.put("txtFldSpouseName",KycUtils.getObjValue(fna.getDfSpsName()));

//					Interpreter details
//					jsnDetailsDataObj.put("txtFldCDIntrContNo",KycUtils.getObjValue(fna.get()));
//					jsnDetailsDataObj.put("txtFldCDIntrName",KycUtils.getObjValue(fna.getTxtFldCDIntrName()));
//					jsnDetailsDataObj.put("txtFldCDIntrNric",KycUtils.getObjValue(fna.getTxtFldCDIntrNric()));
//					jsnDetailsDataObj.put("selCDIntrRel",KycUtils.getObjValue(fna.getSelCDIntrRel()));
					
					
					//added by johnson 20160229 for country of residence
					jsnDetailsDataObj.put("txtFldSelfResCountry",strFnaCustCountry);
					jsnDetailsDataObj.put("txtFldSpsResCountry",KycUtils.getObjValue(fna.getDfSpsRescountry()));
					
					jsnDetailsDataObj.put("htxtFldClntResAddr1",strFnaCustAddr1);
					jsnDetailsDataObj.put("htxtFldClntResAddr2",strFnaCustAddr2);
					jsnDetailsDataObj.put("htxtFldClntResAddr3",strFnaCustAddr3);
					jsnDetailsDataObj.put("htxtFldClntResCity",strFnaCustCity);
					jsnDetailsDataObj.put("htxtFldClntResState",strFnaCustState);
					jsnDetailsDataObj.put("htxtFldClntResCountry",strFnaCustCountry);
					jsnDetailsDataObj.put("htxtFldClntResPostalCode",strFnaCustPostal);
					
					jsnDetailsDataObj.put("resAddr1",strFnaCustAddr1);
			        jsnDetailsDataObj.put("resAddr2",strFnaCustAddr2);
			        jsnDetailsDataObj.put("resAddr3",strFnaCustAddr3);
			        jsnDetailsDataObj.put("resCity",strFnaCustCity);				        
			        jsnDetailsDataObj.put("resState",strFnaCustState);
			        jsnDetailsDataObj.put("resCountry",strFnaCustCountry);
			        jsnDetailsDataObj.put("resPostalcode",strFnaCustPostal);
					
					String strCstnatOth =  KycUtils.getObjValue(fna.getDfSelfNationality());					
					String strCustNatOth = (strFnaCustNatly.equalsIgnoreCase("Singaporean") || strFnaCustNatly.equalsIgnoreCase("Singapore-PR"))
							?(KycUtils.nullOrBlank(strCstnatOth) ? "" : strCstnatOth):strFnaCustNatly;//strCstnatOth;//MAR_2018				
					
					String strFnaSpsNatly = KycUtils.getObjValue(fna.getDfSpsNationality());
					String strSsnatOth  = KycUtils.getObjValue(fna.getDfSpsNatyDets());
					
//					String strSpsNatOth = (strFnaSpsNatly.equalsIgnoreCase("Singaporean") || strFnaSpsNatly.equalsIgnoreCase("Singapore-PR"))
//							?(KycUtils.nullOrBlank(strSsnatOth) ? "" : strSsnatOth):strSsnatOth;	
//										
//					strSpsNatOth =  strCustNatly.equalsIgnoreCase("Singaporean")?
//							"":strCustNatly.equalsIgnoreCase("Singapore-PR")?"":strCustNatly;
					
					jsnDetailsDataObj.put("dfSelfAgenext",KycUtils.getObjValue(fna.getDfSelfAgenext()));
					jsnDetailsDataObj.put("txtFldDfSelfAgenext",KycUtils.getObjValue(fna.getDfSelfAgenext()));
					
					jsnDetailsDataObj.put("dfSpsAgenext",KycUtils.getObjValue(fna.getDfSpsAgenext()));
					jsnDetailsDataObj.put("txtFldDfSpsAgenext",KycUtils.getObjValue(fna.getDfSpsAgenext()));					
					
//					jsnDetailsDataObj.put("dfSelfNatyDets",strCustNatOth);
					
					jsnDetailsDataObj.put("txtFldDfSelfNatyDets",strCustNatOth);
					jsnDetailsDataObj.put("dfSelfNatyDets", KycUtils.getObjValue(fna.getDfSelfNatyDets()));
					
					jsnDetailsDataObj.put("dfSpsNatyDets",strSsnatOth);
					jsnDetailsDataObj.put("dfSpsNatyDets",fna.getDfSpsNatyDets());
					jsnDetailsDataObj.put("txtFldDfSpsNatyDets",strSsnatOth);
					
					jsnDetailsDataObj.put("dfSelfEngSpoken",KycUtils.getObjValue(fna.getDfSelfEngSpoken()));					
					jsnDetailsDataObj.put("dfSelfEngWritten",KycUtils.getObjValue(fna.getDfSelfEngWritten()));
					
					jsnDetailsDataObj.put("dfSpsEngSpoken",KycUtils.getObjValue(fna.getDfSpsEngSpoken()));
					jsnDetailsDataObj.put("dfSpsEngWritten",KycUtils.getObjValue(fna.getDfSpsEngWritten()));
					
					jsnDetailsDataObj.put("dfSelfEdulevel",KycUtils.getObjValue(fna.getDfSelfEdulevel()));
					jsnDetailsDataObj.put("dfSpsEdulevel",KycUtils.getObjValue(fna.getDfSpsEdulevel()));
					
					jsnDetailsDataObj.put("dfSelfReplExistpol",KycUtils.getObjValue(fna.getDfSelfReplExistpol()));
					jsnDetailsDataObj.put("dfSpsReplExistpol",KycUtils.getObjValue(fna.getDfSpsReplExistpol()));
					
					jsnDetailsDataObj.put("dfSelfBirthcntry",KycUtils.getObjValue(fna.getDfSelfBirthcntry()));
					jsnDetailsDataObj.put("dfSpsBirthcntry",KycUtils.getObjValue(fna.getDfSpsBirthcntry()));
					
					jsnDetailsDataObj.put("dfSelfOnecontflg",KycUtils.getObjValue(fna.getDfSelfOnecontflg()));
					jsnDetailsDataObj.put("dfSelfSameasregadd",KycUtils.getObjValue(fna.getDfSelfSameasregadd()));
					
					jsnDetailsDataObj.put("dfSelfMailaddrflg",KycUtils.getObjValue(fna.getDfSelfMailaddrflg()));
					jsnDetailsDataObj.put("dfSpsMailaddrflg",KycUtils.getObjValue(fna.getDfSpsMailaddrflg()));
					
					jsnDetailsDataObj.put("dfSelfMailaddr",KycUtils.getObjValue(fna.getDfSelfMailaddr()));
					jsnDetailsDataObj.put("dfSpsMailaddr",KycUtils.getObjValue(fna.getDfSpsMailaddr()));
					
					jsnDetailsDataObj.put("dfSelfAnnlincome",KycUtils.getObjValue(fna.getDfSelfAnnlincome()));
					jsnDetailsDataObj.put("dfSpsAnnlincome",KycUtils.getObjValue(fna.getDfSpsAnnlincome()));
					
					jsnDetailsDataObj.put("dfSelfFundsrc",KycUtils.getObjValue(fna.getDfSelfFundsrc()).equals("\"\"")?"":KycUtils.getObjValue(fna.getDfSelfFundsrc()));
					jsnDetailsDataObj.put("dfSpsFundsrc",KycUtils.getObjValue(fna.getDfSpsFundsrc()).equals("\"\"")?"":KycUtils.getObjValue(fna.getDfSpsFundsrc()));
					jsnDetailsDataObj.put("dfSelfFundsrcDets",KycUtils.getObjValue(fna.getDfSelfFundsrcDets()).equals("\"\"")?"":KycUtils.getObjValue(fna.getDfSelfFundsrcDets()));
					jsnDetailsDataObj.put("dfSpsFundsrcDets",KycUtils.getObjValue(fna.getDfSpsFundsrcDets()).equals("\"\"")?"":KycUtils.getObjValue(fna.getDfSpsFundsrcDets()));
					
					jsnDetailsDataObj.put("dfSelfHeight",KycUtils.getObjValue(strCustHeight));
					jsnDetailsDataObj.put("dfSpsHeight",KycUtils.getObjValue(fna.getDfSpsHeight()));
					jsnDetailsDataObj.put("dfSelfWeight",KycUtils.getObjValue(strCustWeight));
					jsnDetailsDataObj.put("dfSpsWeight",KycUtils.getObjValue(fna.getDfSpsWeight()));
					
					jsnDetailsDataObj.put("dfSelfRace",strCustRace);
					jsnDetailsDataObj.put("dfSelfSmoker",strCustSmoker);
					
					jsnDetailsDataObj.put("dataformId",KycUtils.getObjValue(fna.getDataformId()));
					
					jsnDetailsDataObj.put("dfSelfTaxres",KycUtils.getObjValue(fna.getDfSelfTaxres()).equals("\"\"")?"":KycUtils.getObjValue(fna.getDfSelfTaxres()));
					jsnDetailsDataObj.put("dfSpsTaxres",KycUtils.getObjValue(fna.getDfSpsTaxres()).equals("\"\"")?"":KycUtils.getObjValue(fna.getDfSpsTaxres()));
					jsnDetailsDataObj.put("dfSelfTaxresOth",KycUtils.getObjValue(fna.getDfSelfTaxresOth()).equals("\"\"")?"":KycUtils.getObjValue(fna.getDfSelfTaxresOth()));
					jsnDetailsDataObj.put("dfSpsfTaxresOth",KycUtils.getObjValue(fna.getDfSpsfTaxresOth()).equals("\"\"")?"":KycUtils.getObjValue(fna.getDfSpsfTaxresOth()));
				}

				jsnFnaSSTblData.put("ARCH_FNASSDET_TABLE_DATA", jsnDetailsDataObj);
			}

			List<FnaDependantDets> lstFnaDep = serv.getFnaArchDepntDets(dbDTO, strFnaID);
			if(lstFnaDep.size()>0){

				JSONObject jsnDetailsDataObj = new JSONObject();

				for(FnaDependantDets fna : lstFnaDep){

					jsnDetailsDataObj.put("txtFldDepId",!KycUtils.nullOrBlank(fna.getDepnId()) ? fna.getDepnId() : "");
					jsnDetailsDataObj.put("txtFldFnaDepntName",!KycUtils.nullOrBlank(fna.getDepnName()) ? fna.getDepnName() : "");
					jsnDetailsDataObj.put("selFnaDepntRel",!KycUtils.nullOrBlank(fna.getDepnRelationship()) ? fna.getDepnRelationship() : "");
					jsnDetailsDataObj.put("txtFldFnaDepntAge",!KycUtils.nullOrBlank(fna.getDepnAge()) ? fna.getDepnAge() : "");
					jsnDetailsDataObj.put("selFnaDepntSex",!KycUtils.nullOrBlank(fna.getDepnGender()) ? fna.getDepnGender() : "");
					jsnDetailsDataObj.put("txtFldFnaDepntYr2Sup",!KycUtils.nullOrBlank(fna.getDepnYrssupport()) ? fna.getDepnYrssupport() : "");
					jsnDetailsDataObj.put("txtFldFnaDepntMonthContr",!KycUtils.nullObj(fna.getDepnMonthcontr()) ? fna.getDepnMonthcontr() : "");
					jsnDetailsDataObj.put("txtFldFnaDepntMonthSelf",!KycUtils.nullObj(fna.getDepnProvSelf()) ? fna.getDepnProvSelf() : "");
					jsnDetailsDataObj.put("txtFldFnaDepntMonthSps",!KycUtils.nullObj(fna.getDepnProvSpouse()) ? fna.getDepnProvSpouse() : "");					
					jsnDetailsDataObj.put("txtFldFnaDepntOccp",!KycUtils.nullObj(fna.getDepnOccp()) ? fna.getDepnOccp() : "");
					jsnFnaDepArr.put(jsnDetailsDataObj);
				}

				jsnFnaDepTblData.put("ARCH_FNADEPNT_TABLE_DATA", jsnFnaDepArr);
			}else {
				jsnFnaDepTblData.put("ARCH_FNADEPNT_TABLE_DATA", jsnFnaDepArr);
			}

			if(lstAnnlExpDet.size()>0){
				JSONObject jsnDetailsDataObj = new JSONObject();

				for(FnaExpenditureDets fna : lstAnnlExpDet){

					if(!KycUtils.nullObj(fna)){

					jsnDetailsDataObj.put("txtFldFnaId",!KycUtils.nullObj(fna.getFnaDetails()) ? fna.getFnaDetails().getFnaId() : "");
					jsnDetailsDataObj.put("txtFldFnaExpId",!KycUtils.nullOrBlank(fna.getExpdId()) ? fna.getExpdId() : "");
					jsnDetailsDataObj.put("txtFldFnaExpRemarks",KycUtils.getObjValue(fna.getExpdRemarks()));
					
					jsnDetailsDataObj.put("cfSelfAnnlinc",KycUtils.getObjValue(fna.getCfSelfAnnlinc()));
					jsnDetailsDataObj.put("cfSpsAnnlinc",KycUtils.getObjValue(fna.getCfSpsAnnlinc()));
					jsnDetailsDataObj.put("cfFamilyAnnlinc",KycUtils.getObjValue(fna.getCfFamilyAnnlinc()));
					jsnDetailsDataObj.put("cfSelfCpfcontib",KycUtils.getObjValue(fna.getCfSelfCpfcontib()));
					jsnDetailsDataObj.put("cfSpsCpfcontib",KycUtils.getObjValue(fna.getCfSpsCpfcontib()));
					jsnDetailsDataObj.put("cfFamilyCpfcontib",KycUtils.getObjValue(fna.getCfFamilyCpfcontib()));
					jsnDetailsDataObj.put("cfSelfEstexpense",KycUtils.getObjValue(fna.getCfSelfEstexpense()));
					jsnDetailsDataObj.put("cfSpsEstexpense",KycUtils.getObjValue(fna.getCfSpsEstexpense()));
					jsnDetailsDataObj.put("cfFamilyEstexpense",KycUtils.getObjValue(fna.getCfFamilyEstexpense()));
					jsnDetailsDataObj.put("cfSelfSurpdef",KycUtils.getObjValue(fna.getCfSelfSurpdef()));
					jsnDetailsDataObj.put("cfSpsSurpdef",KycUtils.getObjValue(fna.getCfSpsSurpdef()));
					jsnDetailsDataObj.put("cfFamilySurpdef",KycUtils.getObjValue(fna.getCfFamilySurpdef()));
					jsnDetailsDataObj.put("alSelfTotasset",KycUtils.getObjValue(fna.getAlSelfTotasset()));
					jsnDetailsDataObj.put("alSpsTotasset",KycUtils.getObjValue(fna.getAlSpsTotasset()));
					jsnDetailsDataObj.put("alFamilyTotasset",KycUtils.getObjValue(fna.getAlFamilyTotasset()));
					jsnDetailsDataObj.put("alSelfTotliab",KycUtils.getObjValue(fna.getAlSelfTotliab()));
					jsnDetailsDataObj.put("alSpsTotliab",KycUtils.getObjValue(fna.getAlSpsTotliab()));
					jsnDetailsDataObj.put("alFamilyTotliab",KycUtils.getObjValue(fna.getAlFamilyTotliab()));
					jsnDetailsDataObj.put("alSelfNetasset",KycUtils.getObjValue(fna.getAlSelfNetasset()));
					jsnDetailsDataObj.put("alSpsNetasset",KycUtils.getObjValue(fna.getAlSpsNetasset()));
					jsnDetailsDataObj.put("alFamilyNetasset",KycUtils.getObjValue(fna.getAlFamilyNetasset()));


					}
				}

				jsnFnaExpntrData.put("ARCH_EXPNTRDET_DATA", jsnDetailsDataObj);
			}

			List<FnaOtherpersonDets> lstOthPersDet = serv.getFnaArchOthPersDet(dbDTO, strFnaID);
			if(lstOthPersDet.size()>0){
				JSONObject jsnDetailsDataObj = new JSONObject();
				for(FnaOtherpersonDets fna  : lstOthPersDet){

					if(fna.getOthperCateg().equalsIgnoreCase("BENF")){
						jsnDetailsDataObj=new JSONObject();
						jsnDetailsDataObj.put("txtFldCDBenfId",!KycUtils.nullOrBlank(fna.getOthperId()) ? fna.getOthperId() : "");
						jsnDetailsDataObj.put("txtFldCDBenfName",!KycUtils.nullOrBlank(fna.getOthperName()) ? fna.getOthperName() : "");
						jsnDetailsDataObj.put("txtFldCDBenfNric",!KycUtils.nullOrBlank(fna.getOthperNric()) ? fna.getOthperNric() : "");
						jsnDetailsDataObj.put("txtFldCDBenfIncNo",!KycUtils.nullOrBlank(fna.getOthperIncorpno()) ? fna.getOthperIncorpno() : "");
						jsnDetailsDataObj.put("txtFldCDBenfAddr",!KycUtils.nullOrBlank(fna.getOthperRegaddr()) ? fna.getOthperRegaddr() : "");
						jsnDetailsDataObj.put("txtFldCDBenfRel",!KycUtils.nullOrBlank(fna.getOthperRelation()) ? fna.getOthperRelation() : "");
						jsnDetailsDataObj.put("txtFldCDBenfJob",!KycUtils.nullOrBlank(fna.getOthperWork()) ? fna.getOthperWork() : "");
						jsnDetailsDataObj.put("txtFldCDBenfJobCont",!KycUtils.nullOrBlank(fna.getOthperWorkCountry()) ? fna.getOthperWorkCountry() : "");
						jsnFnaBenfTppPepArr.put(jsnDetailsDataObj);
					}
					if(fna.getOthperCateg().equalsIgnoreCase("TPP")){
						jsnDetailsDataObj=new JSONObject();
						jsnDetailsDataObj.put("txtFldCDTppId",!KycUtils.nullOrBlank(fna.getOthperId()) ? fna.getOthperId() : "");
						jsnDetailsDataObj.put("txtFldCDTppName",!KycUtils.nullOrBlank(fna.getOthperName()) ? fna.getOthperName() : "");
						jsnDetailsDataObj.put("txtFldCDTppNric",!KycUtils.nullOrBlank(fna.getOthperNric()) ? fna.getOthperNric() : "");
						jsnDetailsDataObj.put("txtFldCDTppIncNo",!KycUtils.nullOrBlank(fna.getOthperIncorpno()) ? fna.getOthperIncorpno() : "");
						jsnDetailsDataObj.put("txtFldCDTppAddr",!KycUtils.nullOrBlank(fna.getOthperRegaddr()) ? fna.getOthperRegaddr() : "");
						jsnDetailsDataObj.put("txtFldCDTppRel",!KycUtils.nullOrBlank(fna.getOthperRelation()) ? fna.getOthperRelation() : "");
						jsnDetailsDataObj.put("txtFldCDTppJob",!KycUtils.nullOrBlank(fna.getOthperWork()) ? fna.getOthperWork() : "");
						jsnDetailsDataObj.put("txtFldCDTppJobCont",!KycUtils.nullOrBlank(fna.getOthperWorkCountry()) ? fna.getOthperWorkCountry() : "");

						jsnDetailsDataObj.put("txtFldCDTppBankName",!KycUtils.nullOrBlank(fna.getOthperBankname()) ? fna.getOthperBankname() : "");
						jsnDetailsDataObj.put("txtFldCDTppChqNo",!KycUtils.nullOrBlank(fna.getOthperChqOrderno()) ? fna.getOthperChqOrderno() : "");
						jsnDetailsDataObj.put("txtFldCDTppCcontact",!KycUtils.nullOrBlank(fna.getOthperContactno()) ? fna.getOthperContactno() : "");
						jsnDetailsDataObj.put("htfcdpaymentmode",!KycUtils.nullOrBlank(fna.getOthperPaymentmode()) ? fna.getOthperPaymentmode() : "");

						jsnFnaBenfTppPepArr.put(jsnDetailsDataObj);
					}

					if(fna.getOthperCateg().equalsIgnoreCase("PEP")){
						jsnDetailsDataObj=new JSONObject();
						jsnDetailsDataObj.put("txtFldCDPepId",!KycUtils.nullOrBlank(fna.getOthperId()) ? fna.getOthperId(): "");
						jsnDetailsDataObj.put("txtFldCDPepName",!KycUtils.nullOrBlank(fna.getOthperName()) ? fna.getOthperName() : "");
						jsnDetailsDataObj.put("txtFldCDPepNric",!KycUtils.nullOrBlank(fna.getOthperNric()) ? fna.getOthperNric() : "");
						jsnDetailsDataObj.put("txtFldCDPepIncNo",!KycUtils.nullOrBlank(fna.getOthperIncorpno()) ? fna.getOthperIncorpno() : "");
						jsnDetailsDataObj.put("txtFldCDPepAddr",!KycUtils.nullOrBlank(fna.getOthperRegaddr()) ? fna.getOthperRegaddr() : "");
						jsnDetailsDataObj.put("txtFldCDPepRel",!KycUtils.nullOrBlank(fna.getOthperRelation()) ? fna.getOthperRelation() : "");
						jsnDetailsDataObj.put("txtFldCDPepJob",!KycUtils.nullOrBlank(fna.getOthperWork()) ? fna.getOthperWork() : "");
						jsnDetailsDataObj.put("txtFldCDPepJobCont",!KycUtils.nullOrBlank(fna.getOthperWorkCountry()) ? fna.getOthperWorkCountry() : "");
						jsnFnaBenfTppPepArr.put(jsnDetailsDataObj);
					}
				}

				jsnFnaBenfTppPepData.put("ARCH_BENFTPPPEP_DATA", jsnFnaBenfTppPepArr);
			}

			List<FnaRecomPrdtplanDet> lstProdPlanDet = serv.getFnaArchRecProdPlanDet(dbDTO, strFnaID);
			if(lstProdPlanDet.size()>0){
				JSONObject jsnDetailsDataObj = null;
//				String strPrevClientName = "";
//				int index = 1;
				for(FnaRecomPrdtplanDet fna  : lstProdPlanDet){
				    jsnDetailsDataObj = new JSONObject();
					//if(fna.getRecomPpCateg().equalsIgnoreCase("ADVREC")){
					//ADVREC
//					jsnDetailsDataObj.put("txtFldFnaId",!KycUtils.nullObj(fna.getFnaDetails()) ? fna.getFnaDetails().getFnaId() : "");
					jsnDetailsDataObj.put("txtFldFnaAdvRecId",!KycUtils.nullOrBlank(fna.getRecomPpId()) ? fna.getRecomPpId() : "");
					jsnDetailsDataObj.put("selFnaAdvRecPayment",!KycUtils.nullOrBlank(fna.getRecomPpPaymentmode()) ? fna.getRecomPpPaymentmode() : "");
					jsnDetailsDataObj.put("selFnaAdvRecRel",!KycUtils.nullOrBlank(fna.getRecomPpPerson()) ? fna.getRecomPpPerson() : "");
					jsnDetailsDataObj.put("txtFldFnaAdvRecName",!KycUtils.nullOrBlank(fna.getRecomPpPersonname()) ? fna.getRecomPpPersonname() : "");
					jsnDetailsDataObj.put("selFnaAdvRecPlanName",!KycUtils.nullOrBlank(fna.getRecomPpProdname()) ? fna.getRecomPpProdname() : "");
					jsnDetailsDataObj.put("selFnaAdvRecCompName",!KycUtils.nullOrBlank(fna.getRecomPpPrin()) ? fna.getRecomPpPrin() : "");
					jsnDetailsDataObj.put("selFnaAdvRecPrdtPlan",!KycUtils.nullOrBlank(fna.getRecomPpPlan()) ? fna.getRecomPpPlan() : "");
					jsnDetailsDataObj.put("selFnaAdvRecPrdtType",!KycUtils.nullOrBlank(fna.getRecomPpProdtype()) ? fna.getRecomPpProdtype() : "");
					jsnDetailsDataObj.put("txtFldFnaAdvRecPremTerm",!KycUtils.nullObj(fna.getRecomPpPayterm()) ? fna.getRecomPpPayterm() : "");
					jsnDetailsDataObj.put("txtFldFnaAdvRecPlanTerm",!KycUtils.nullObj(fna.getRecomPpPlanterm()) ? fna.getRecomPpPlanterm() : "");
					jsnDetailsDataObj.put("txtFldFnaAdvRecPrem",!KycUtils.nullObj(fna.getRecomPpPremium()) ? fna.getRecomPpPremium() : "");
					jsnDetailsDataObj.put("txtFldFnaAdvRecSumAssr",!KycUtils.nullObj(fna.getRecomPpSumassr()) ? fna.getRecomPpSumassr() : "");
					jsnDetailsDataObj.put("txtFldFnaScreen",!KycUtils.nullObj(fna.getRecomPpCateg()) ? fna.getRecomPpCateg() : "");
					jsnDetailsDataObj.put("txtFldRecomPpBasRid",!KycUtils.nullObj(fna.getRecomPpBasrid()) ? fna.getRecomPpBasrid() : "");
					jsnDetailsDataObj.put("txtFldRecomPpOpt",!KycUtils.nullObj(fna.getRecomPpOpt()) ? fna.getRecomPpOpt() : "");
					jsnDetailsDataObj.put("txtFldRecomPpName",!KycUtils.nullObj(fna.getRecomPpName()) ? fna.getRecomPpName() : "");
				//    		Poovathi Add on 14 Oct 2019.
					jsnDetailsDataObj.put("selRecomProdRisk",!KycUtils.nullObj(fna.getProdRiskRate()) ? ( fna.getProdRiskRate() == 0  ? "" :fna.getProdRiskRate() ) : "");
					

					
					jsnFnaPrdtPlanArr.put(jsnDetailsDataObj);
					//}

				}
				jsnFnaPrdtPlanDetData.put("ARCH_ADVRECPRDTPLAN_DATA", jsnFnaPrdtPlanArr);
			}

			List<FnaRecomFundDet> lstFundDet = serv.getFnaArchRecFundDet(dbDTO, strFnaID);
			if(lstFundDet.size()>0){
				JSONObject jsnDetailsDataObj = new JSONObject();
				for(FnaRecomFundDet fna  : lstFundDet){
//					jsnDetailsDataObj.put("txtFldFnaId",!KycUtils.nullObj(fna.getFnaDetails()) ? fna.getFnaDetails().getFnaId() : "");
					jsnDetailsDataObj.put("txtFldFnaArtuFundId",!KycUtils.nullOrBlank(fna.getRecomFfId()) ? fna.getRecomFfId() : "");
					jsnDetailsDataObj.put("selFnaArtuFundPayment",!KycUtils.nullOrBlank(fna.getRecomFfPaymentmode()) ? fna.getRecomFfPaymentmode() : "");
					jsnDetailsDataObj.put("selFnaArtuFundPrdtType",!KycUtils.nullOrBlank(fna.getRecomFfProdtype()) ? fna.getRecomFfProdtype() : "");
					jsnDetailsDataObj.put("selFnaArtuFundCompName",!KycUtils.nullOrBlank(fna.getRecomFfPrin()) ? fna.getRecomFfPrin() : "");
					jsnDetailsDataObj.put("selFnaArtuFundPlanCode",!KycUtils.nullOrBlank(fna.getRecomFfProname()) ? fna.getRecomFfProname() : "");
					jsnDetailsDataObj.put("selFnaArtuFundPlanName",!KycUtils.nullOrBlank(fna.getRecomFfPlan()) ? fna.getRecomFfPlan() : "");
					jsnDetailsDataObj.put("txtFldFnaArtuFundRisk",!KycUtils.nullObj(fna.getRecomFfFundriskrate()) ? fna.getRecomFfFundriskrate() : "");
					jsnDetailsDataObj.put("txtFldArtuFundIaf",!KycUtils.nullObj(fna.getRecomFfIaffee()) ? fna.getRecomFfIaffee() : "");
					jsnDetailsDataObj.put("txtFldFnaArtuFundPurAmt",!KycUtils.nullObj(fna.getRecomFfPuramount()) ? fna.getRecomFfPuramount() : "");
					jsnDetailsDataObj.put("txtFldFnaArtuFundPurPrct",!KycUtils.nullObj(fna.getRecomFfPurprcnt()) ? fna.getRecomFfPurprcnt() : "");
					jsnDetailsDataObj.put("txtFldFnaArtuFundPurSale",!KycUtils.nullObj(fna.getRecomFfSaleschrg()) ? fna.getRecomFfSaleschrg() : "");
					jsnDetailsDataObj.put("txtFldFnaArtuFundBasRid",!KycUtils.nullObj(fna.getRecomFfBasrid()) ? fna.getRecomFfBasrid() : "");
					jsnDetailsDataObj.put("txtFldFnaArtuFundOpt",!KycUtils.nullObj(fna.getRecomFfOpt()) ? fna.getRecomFfOpt() : "");

					jsnFnaFundPlanArr.put(jsnDetailsDataObj);

				}

				jsnFnaFundPlanDetData.put("ARCH_ADVRECFUNDPLAN_DATA", jsnFnaFundPlanArr);
			}


			List<FnaFatcaTaxdet> lstFatca = serv.getFnaFatcaTaxDet(dbDTO, strFnaID);

			if(lstFatca.size()>0){
				JSONObject jsnDetailsDataObj = new JSONObject();
				JSONObject jsnDetailsDataObjSps = new JSONObject();
				for(FnaFatcaTaxdet fna  : lstFatca){
					String strTaxFor = !KycUtils.nullOrBlank(fna.getTaxFor()) ? fna.getTaxFor() : "";
					if(strTaxFor.equalsIgnoreCase("SPOUSE")){
						
//						jsnDetailsDataObj.put("txtFldFnaId",!KycUtils.nullObj(fna.getFnaDetails()) ? fna.getFnaDetails().getFnaId() : "");
						jsnDetailsDataObjSps.put("txtFldFnaTaxId",!KycUtils.nullOrBlank(fna.getTaxId()) ? fna.getTaxId() : "");
						jsnDetailsDataObjSps.put("selFnaTaxCntry",!KycUtils.nullOrBlank(fna.getTaxCountry()) ? fna.getTaxCountry() : "");
						jsnDetailsDataObjSps.put("txtFldFnaTaxRefNo",!KycUtils.nullOrBlank(fna.getTaxRefno()) ? fna.getTaxRefno() : "");
						jsnDetailsDataObjSps.put("txtFldFnaNoTaxReason",!KycUtils.nullOrBlank(fna.getReasonToNotax()) ? fna.getReasonToNotax() : "");
						jsnDetailsDataObjSps.put("txtFldFnaReasonDets",!KycUtils.nullOrBlank(fna.getReasonbDetails()) ? fna.getReasonbDetails() : "");
						jsnDetailsDataObjSps.put("txtFldFnaTaxFor",!KycUtils.nullOrBlank(fna.getTaxFor()) ? fna.getTaxFor() : "");
						jsnFnaFatcaTaxArrSps.put(jsnDetailsDataObjSps);

						
					}else {
						
//						jsnDetailsDataObj.put("txtFldFnaId",!KycUtils.nullObj(fna.getFnaDetails()) ? fna.getFnaDetails().getFnaId() : "");
						jsnDetailsDataObj.put("txtFldFnaTaxId",!KycUtils.nullOrBlank(fna.getTaxId()) ? fna.getTaxId() : "");
						jsnDetailsDataObj.put("selFnaTaxCntry",!KycUtils.nullOrBlank(fna.getTaxCountry()) ? fna.getTaxCountry() : "");
						jsnDetailsDataObj.put("txtFldFnaTaxRefNo",!KycUtils.nullOrBlank(fna.getTaxRefno()) ? fna.getTaxRefno() : "");
						jsnDetailsDataObj.put("txtFldFnaNoTaxReason",!KycUtils.nullOrBlank(fna.getReasonToNotax()) ? fna.getReasonToNotax() : "");
						jsnDetailsDataObj.put("txtFldFnaReasonDets",!KycUtils.nullOrBlank(fna.getReasonbDetails()) ? fna.getReasonbDetails() : "");
						jsnDetailsDataObj.put("txtFldFnaTaxFor",!KycUtils.nullOrBlank(fna.getTaxFor()) ? fna.getTaxFor() : "");
						jsnFnaFatcaTaxArr.put(jsnDetailsDataObj);

						
					}
				}

				jsnFnaFatcaTaxDetData.put("ARCH_FATCATAXDET_DATA", jsnFnaFatcaTaxArr);
				jsnFnaFatcaTaxDetDataSps.put("ARCH_FATCATAXDETSPS_DATA", jsnFnaFatcaTaxArrSps);
			}else {
				jsnFnaFatcaTaxDetData.put("ARCH_FATCATAXDET_DATA", jsnFnaFatcaTaxArr);
				jsnFnaFatcaTaxDetDataSps.put("ARCH_FATCATAXDETSPS_DATA", jsnFnaFatcaTaxArrSps);
			}
			
			


			List<FnaSwtchrepPlanDet> lstSwRepPlanDet = serv.getFnaArchSwRepPlanDet(dbDTO, strFnaID);

			if(lstSwRepPlanDet.size()>0){

				JSONObject jsnDetailsDataObj = null;

				for(FnaSwtchrepPlanDet fna : lstSwRepPlanDet){
				        jsnDetailsDataObj = new JSONObject();
//					jsnDetailsDataObj.put("txtFldFnaId",!KycUtils.nullObj(fna.getFnaDetails()) ? fna.getFnaDetails().getFnaId() : "");				
					jsnDetailsDataObj.put("txtFldFnaSwrepPlanId",KycUtils.getObjValue(fna.getSwrepPpId())) ;
					jsnDetailsDataObj.put("selFnaSwrepPlanPrdtType",KycUtils.getObjValue(fna.getSwrepPpProdtype()));
					jsnDetailsDataObj.put("selFnaSwrepPlanName",KycUtils.getObjValue(fna.getSwrepPpProdname()));//PROD CODE
					jsnDetailsDataObj.put("selFnaSwrepPlan",KycUtils.getObjValue(fna.getSwrepPpPlan()));
					jsnDetailsDataObj.put("selFnaSwrepPlanCompName",KycUtils.getObjValue(fna.getSwrepPpPrin()));
					jsnDetailsDataObj.put("txtFldFnaSwrepPlanSA",KycUtils.getObjValue(fna.getSwrepPpSumassr()));
					jsnDetailsDataObj.put("selFnaSwrepPlanPayment",KycUtils.getObjValue(fna.getSwrepPpPaymentmode()) );
					jsnDetailsDataObj.put("selFnaSwrepPlanSwitch",KycUtils.getObjValue(fna.getSwrepPpTransind()));
					jsnDetailsDataObj.put("txtFldFnaSwrepPlanTerm",KycUtils.getObjValue(fna.getSwrepPpPlanterm()));
					jsnDetailsDataObj.put("txtFldFnaSwrepPlanPayTerm",KycUtils.getObjValue(fna.getSwrepPpPayterm()));
					jsnDetailsDataObj.put("txtFldFnaSwrepPlanPrem",KycUtils.getObjValue(fna.getSwrepPpPremium()) );
					jsnDetailsDataObj.put("txtFldFnaSwrepBasRid",KycUtils.getObjValue(fna.getSwrepPpBasrid()) );
					jsnDetailsDataObj.put("txtFldFnaSwrepPlanOpt",KycUtils.getObjValue(fna.getSwrepPpOpt()) );
					jsnDetailsDataObj.put("txtFldSwrepPpName",KycUtils.getObjValue(fna.getSwrepPpName()) );
//    		Poovathi Add on 14 Oct 2019.
					jsnDetailsDataObj.put("selSwrepPpRiskRate",!KycUtils.nullObj(fna.getSwrepProdRiskRate()) ? ( fna.getSwrepProdRiskRate() == 0  ? "" :fna.getSwrepProdRiskRate() ) : "");
					
					jsnFnaSwRepPlanArr.put(jsnDetailsDataObj);


				}
				jsnFnaSwRepPlanDetData.put("ARCH_SWREPPLANDET_DATA", jsnFnaSwRepPlanArr);
			}


			List<FnaSwtchrepFundDet> lstSwRepFundDet = serv.getFnaArchSwRepFundDet(dbDTO, strFnaID);

			if(lstSwRepFundDet.size()>0){

				JSONObject jsnDetailsDataObj = new JSONObject();

				for(FnaSwtchrepFundDet fna : lstSwRepFundDet){

//					jsnDetailsDataObj.put("txtFldFnaId",!KycUtils.nullObj(fna.getFnaDetails()) ? fna.getFnaDetails().getFnaId() : "");
					jsnDetailsDataObj.put("txtFldSwrepFundIaf",KycUtils.getObjValue(fna.getSwrepFfIaffee())) ;
					jsnDetailsDataObj.put("txtFldFnaSwrepFundId",KycUtils.getObjValue(fna.getSwrepFfId())) ;
					jsnDetailsDataObj.put("selFnaSwrepFundPrdtType",KycUtils.getObjValue(fna.getSwrepFfProdtype()));
					jsnDetailsDataObj.put("selFnaSwrepFundCompName",KycUtils.getObjValue(fna.getSwrepPpPrin()));
					jsnDetailsDataObj.put("selFnaSwrepFundProdCode",KycUtils.getObjValue(fna.getSwrepFfProdname()));
					jsnDetailsDataObj.put("selFnaSwrepFundPlan",KycUtils.getObjValue(fna.getSwrepPpPlan()));
					jsnDetailsDataObj.put("txtFldFnaSwrepFundRisk",KycUtils.getObjValue(fna.getSwrepFfFundriskrate()));
					jsnDetailsDataObj.put("selFnaSwrepFundPayment",KycUtils.getObjValue(fna.getSwrepFfPaymentmode()) );
					jsnDetailsDataObj.put("txtFldFnaSwrepFundSoUnit",KycUtils.getObjValue(fna.getSwrepFfSoredmunits()));
					jsnDetailsDataObj.put("txtFldFnaSwrepFundSoPrc",KycUtils.getObjValue(fna.getSwrepFfSoredprcnt()));
					jsnDetailsDataObj.put("txtFldFnaSwrepFundSiChrg",KycUtils.getObjValue(fna.getSwrepFfSirepsaleschrg()));
					jsnDetailsDataObj.put("txtFldFnaSwrepFundSiAmt",KycUtils.getObjValue(fna.getSwrepFfSirepamt()) );
					jsnDetailsDataObj.put("txtFldFnaSwrepFundSiPrc",KycUtils.getObjValue(fna.getSwrepFfSirepprcnt())) ;
					jsnDetailsDataObj.put("txtFldFnaSwrepFundBasRid",KycUtils.getObjValue(fna.getSwrepFfBsrid())) ;
					jsnDetailsDataObj.put("txtFldFnaSwrepFundOpt",KycUtils.getObjValue(fna.getSwrepFfOpt())) ;
					jsnFnaSwRepFundArr.put(jsnDetailsDataObj);
				}
				jsnFnaSwRepFundDetData.put("ARCH_SWREPFUNDDET_DATA", jsnFnaSwRepFundArr);
			}


			
			JSONObject jsnClientImgObj = new JSONObject();
			JSONObject jsnAdviserImgObj = new JSONObject();
			JSONObject jsnManagerImgObj = new JSONObject();
			JSONObject jsnSpouseImgObj = new JSONObject();
			JSONArray jsnAllTabImgArr = new JSONArray();
			JSONObject jsnSignatureData = new JSONObject();
			JSONObject jsnClientImgTab = new JSONObject();
			
			List signDocList = esignserv.getClientSign(dbDTO, strFnaID,"ALL","ALL");
			
			if(signDocList.size()>0){
				 
				java.util.Iterator ii = signDocList.iterator();			       
				while(ii.hasNext()){
					Object[] rows = (Object[])ii.next();
//					Clob clb = (Clob)rows[0];
					String strFor = KycUtils.nullObj(rows[1]) ? null :(String)rows[1];
					Blob signimage = (Blob)rows[5];
					String date = KycUtils.convertObjToString(KycUtils.convertDateToDateDateString((Date)rows[3]));
					String pageref =  KycUtils.checkNullVal(rows[4])?"":(String)rows[4];
					byte[] imgbyte = signimage.getBytes(1l,(int)signimage.length());
					
					
					if(strFor.equalsIgnoreCase("CLIENT")){
						
						String imageBase64 = DatatypeConverter.printBase64Binary(imgbyte);
						
						jsnClientImgObj.put("CLIENT_SIGNED", "Y");
						jsnClientImgObj.put("CLIENT_SIGN", imageBase64);
						jsnClientImgObj.put("CLIENT_SIGN_DATE", date);
						jsnClientImgObj.put("CLIENT_SIGN_PAGEREF", pageref);
						jsnAllTabImgArr.put(jsnClientImgObj);
					}
					
					
					if(strFor.equalsIgnoreCase("ADVISER")){
						
						String imageBase64 = DatatypeConverter.printBase64Binary(imgbyte);
						
						jsnAdviserImgObj.put("ADVISER_SIGNED", "Y");
						jsnAdviserImgObj.put("ADVISER_SIGN", imageBase64);
						jsnAdviserImgObj.put("ADVISER_SIGN_DATE", date);
						jsnAdviserImgObj.put("ADVISER_SIGN_PAGEREF", pageref);
						jsnAllTabImgArr.put(jsnAdviserImgObj);
					}
					
					
					if(strFor.equalsIgnoreCase("MANAGER")){
						
						String imageBase64 = DatatypeConverter.printBase64Binary(imgbyte);
						
						jsnManagerImgObj.put("MANAGER_SIGNED", "Y");
						jsnManagerImgObj.put("MANAGER_SIGN", imageBase64);
						jsnManagerImgObj.put("MANAGER_SIGN_DATE", date);
						jsnManagerImgObj.put("MANAGER_SIGN_PAGEREF", pageref);
						jsnAllTabImgArr.put( jsnManagerImgObj );
					}
					
					if(strFor.equalsIgnoreCase("SPOUSE")){
						
						String imageBase64 = DatatypeConverter.printBase64Binary(imgbyte);
						
						jsnSpouseImgObj.put("SPOUSE_SIGNED", "Y");
						jsnSpouseImgObj.put("SPOUSE_SIGN", imageBase64);
						jsnSpouseImgObj.put("SPOUSE_SIGN_DATE", date);
						jsnSpouseImgObj.put("SPOUSE_SIGN_PAGEREF", pageref);
						jsnAllTabImgArr.put( jsnSpouseImgObj );
					}

				}
				
				
				
				
				
				
			}else{
				jsnClientImgObj.put("SIGNED", "NO");
				jsnClientImgTab.put("CLIENT_SIGN_DETS", jsnClientImgObj);
				jsnAllTabImgArr.put(jsnClientImgTab);
			}
//			kyclog.info(jsnAllTabImgArr);
			
			jsnSignatureData.put("ALL_SIGNATURE_DETS", jsnAllTabImgArr);
			
			
			retValues.put(jsnFnaTblData);
			retValues.put(jsnFnaSSTblData);
			retValues.put(jsnFnaDepTblData);
			retValues.put(jsnFnaExpntrData);
			retValues.put(jsnFnaBenfTppPepData);
			retValues.put(jsnFnaPrdtPlanDetData);
			retValues.put(jsnFnaFundPlanDetData);
			retValues.put(jsnFnaFatcaTaxDetData);
			retValues.put(jsnFnaFatcaTaxDetDataSps);			
			retValues.put(jsnFnaSwRepPlanDetData);
			retValues.put(jsnFnaSwRepFundDetData);
			retValues.put(jsnFnaFbCustRefData);
			retValues.put(jsnSignatureData);
			

//			kyclog.info(retValues);
//			System.out.println(retValues);
			
		}catch(Exception ex){
			
			ex.printStackTrace();
		}

		return retValues;
	}


	protected JSONArray insertCustAttachDetails(HttpServletRequest request){


		 JSONArray sendEmail = new JSONArray();
//		 JSONArray mailContJsonArr = new JSONArray();
//		 JSONObject jsonObj = new JSONObject();
//		 JSONObject sendMailJsonObj = new JSONObject();
//		 JSONObject nricAttachJsnObj= new JSONObject();
//		 JSONArray nricjsnArray = new JSONArray();
//		 JSONObject nricJsnObj = new JSONObject();

		 String strFnaId = request.getParameter("paramFnaId") != null ?  request.getParameter("paramFnaId"):null;
		 String strCustId = request.getParameter("paramcustId") != null ?  request.getParameter("paramcustId"):null;
		 String strAdvName = request.getParameter("paramAdviserName") != null ? request.getParameter("paramAdviserName") : null;
		 String strClntName = request.getParameter("paramCustName") != null ?  request.getParameter("paramCustName"):null;

		 String strBirtUrl= KycConst.BIRT_PATH;
		 String strFormType = KycConst.SIMPLIFIED_VER;
		 
		 String strDistId= KycConst.STR_NULL_STRING;//KycUtils.getCommonSessValue(request, "LOGGED_DISTID");
//		 ServletContext servlContext = getServletContext();
//		 WebApplicationContext context = WebApplicationContextUtils.getRequiredWebApplicationContext(servlContext);
		 ApplicationContext context = ApplicationContextUtils.getApplicationContext();

		 DBIntf dbDTO = (DBIntf) context.getBean("DBDAO");

		 CustomerAttachService serv = new CustomerAttachService();

		 String regexchars = resource.getString("attach.bfile.SplCharVal");
		 String custdir =  resource.getString("attach.bfile.clnt.dir");
		 String strRptLoc = resource.getString("fna.rpt.fileloc");
		 String pdfFileName ="";

			 pdfFileName = resource.getString("fna.pdf.generalfileName.simplified");
			 pdfFileName = pdfFileName  + resource.getString("fna.pdf.fileformat");

//		kyclog.info("pdfFileName------>"+pdfFileName);

		 String strGenFileName = pdfFileName;
		 String strAtmtCreatedBy="";
		 String strAtmtFileSize = "";

		StringBuffer destFile = new StringBuffer();
		String destFileName ="";


		GenReport gr = new GenReport();

		String strPdfLoc = resource.getString("fna.pdf.generatedloc") ;
		String strCustLob = "ALL";
		strDistId = KycUtils.getCommonSessValue(request, KycConst.LOGGED_DIST_ID) ;
		strAtmtCreatedBy = KycUtils.getCommonSessValue(request, KycConst.LOGGED_USER_ID) ;
//		String strFormType = KycUtils.getCommonSessValue(request, "FNA_FORM_TYPE");
		JSONObject custJsnObj = new JSONObject();
		try{

		   JSONObject custAttachJsnObj = new JSONObject();
		   JSONArray custjsnArray = new JSONArray();
		   

		   pdfFileName = KycUtils.replaceSplChars(regexchars, pdfFileName);

		   String fileName = gr.genPdfFromUrl(strFnaId,strCustId,strRptLoc,strPdfLoc,pdfFileName,strBirtUrl,"","",strCustLob,strDistId,strFormType,true);
		   kyclog.info("Generated file to the customer attachments -------->  "+fileName);


		   if(!KycUtils.nullOrBlank(fileName)){

					kyclog.info(fileName +" ,attachment is generated success!");

					destFile.append(resource.getString("attach.bfile.desitMachine"));

					if(!(KycUtils.nullOrBlank(resource.getString("attach.bfile.desitMachine.drive")))){
						destFile.append(File.separator + resource.getString("attach.bfile.desitMachine.drive"));
					}

					destFile.append(File.separator + resource.getString("attach.bfile.desitMachine.drive.folder"));

					destFile.append(File.separator + resource.getString("attach.bfile.attachscreen.clnt"));

					if(!KycUtils.nullOrBlank(strAdvName)){
						destFile.append(File.separator + KycUtils.replaceSplChars(regexchars, strAdvName)) ;
					}

					if(!KycUtils.nullOrBlank(strClntName)){
						destFile.append(File.separator + KycUtils.replaceSplChars(regexchars, strClntName)) ;
					}

					if(!KycUtils.nullOrBlank(strGenFileName)){
						destFileName = destFile +  File.separator + strGenFileName;
					}else{
						destFileName = destFile.toString();
					}

					String strBFILEName = strAdvName+File.separator+strClntName+File.separator+strGenFileName;
			    	strBFILEName = strBFILEName.replace("\'", "\''");

			    	kyclog.info("destFileName-->" + destFileName+",strBFILEName-->"+strBFILEName);

					File source = new File(fileName);
					File desitnation = new File(destFileName);
					strAtmtFileSize = String.valueOf(source.length());

					FileUtils.copyFile(source, desitnation);

					String custAttachId = serv.insertCustAttForFnaAdmin(dbDTO,strCustId,strGenFileName,strAtmtFileSize,strAtmtCreatedBy,custdir,strBFILEName,strDistId,KycConst.STR_NULL_STRING);
					custAttachJsnObj.put("txtFlsCustAttachId",custAttachId);
					custjsnArray.put(custAttachJsnObj);
					custJsnObj.put("CUST_ATTACH_ID", custjsnArray);
					sendEmail.put(custJsnObj);
				}



			///---------------END OF CLIENT ATTACHMENT


		}catch(Exception ex){
//			ex.printStackTrace();
			kyclog.error(ex);
			custJsnObj.put("CUST_ATTACH_ID", "FAIL");
		}


		return sendEmail;

	}


	public JSONArray updateManagerApproveDets(HttpServletRequest request) {

		
		
	    JSONObject jsonObj = new JSONObject();
	    JSONArray jsonArr = new JSONArray();

	    JSONObject mngrjsonObj = new JSONObject();
	    JSONArray mngrjsonArr = new JSONArray();

	    
		try {
			
			FNAService kycServObj = new FNAService();
			 

//			ServletContext context = getServletContext();
//			WebApplicationContext contexts = WebApplicationContextUtils.getRequiredWebApplicationContext(context);
//			DBIntf dbDTO = (DBIntf) contexts.getBean("DBDAO");
			ApplicationContext ctx = ApplicationContextUtils
					.getApplicationContext();
			DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);
			String strFNAID = request.getParameter("txtFldFnaId")!= null ? request.getParameter("txtFldFnaId") : "";
			
			String strMgrId = request.getParameter("txtFldManagerId") != null ? request.getParameter("txtFldManagerId") : "";
			String strMgrEmailId = request.getParameter("txtFldManagerEmailId") != null ? request.getParameter("txtFldManagerEmailId") : "";
			String strMangrApproveStatus = request.getParameter("txtFldMgrAppStatus")!= null ? request.getParameter("txtFldMgrAppStatus") : "";
			
			String strRemarks = request.getParameter("txtFldManagerRemarks")!= null ? request.getParameter("txtFldManagerRemarks") : "";
			String hNTUCPolicyId = request.getParameter("hNTUCPolicyId")!= null ? request.getParameter("hNTUCPolicyId") : "";
			String strPolicyNum = request.getParameter("txtFldPolNum")!= null ? request.getParameter("txtFldPolNum") : "";
			String strCustomerName = request.getParameter("txtFldCustName")!= null ? request.getParameter("txtFldCustName") : "";
			
			String strAdviserName = request.getParameter("txtFldAdviserName")!= null ? request.getParameter("txtFldAdviserName") : "";			
			String strAdvEmailId = request.getParameter("txtFldAdvEmailId")!= null ? request.getParameter("txtFldAdvEmailId") : "";
			
			String strApproverUserId = request.getParameter("txtFldApprUserId")!= null ? request.getParameter("txtFldApprUserId") : "";
			String strApproverAdvStfId = request.getParameter("txtFldApprAdvId")!= null ? request.getParameter("txtFldApprAdvId") : "";
			String strApprverEmailId =  request.getParameter("txtFldApprUserEmailId") != null ? request.getParameter("txtFldApprUserEmailId") : "";
			String strFnaPrinc = request.getParameter("txtFldFnaPrin") != null ? request.getParameter("txtFldFnaPrin") : "";
					
//			System.out.println("CUST : " +strCustomerName + ",ADV : "+strAdviserName + "["+strAdvEmailId+"]" + "," + "MGR : " + strMgrId +"[" +strMgrEmailId + "]" + ", APPR : "+strApproverAdvStfId + "[" + strApprverEmailId + "]");
		

		String strAdvName ="";
		String strCustName = "";
		String strAdvstfEmailId = "";
		 
		
		if(KycUtils.nullOrBlank(strAdviserName)){
//			14/06/2021
//			strAdvName = KycUtils.getCommonSessValue(request, "CURR_ADVSTF_NAME");
//			strCustName = KycUtils.getCommonSessValue(request, "CUST_NAME");
			
			strAdvName = strAdviserName;
			strCustName = strCustomerName;
			
			strAdvstfEmailId = KycUtils.getCommonSessValue(request, "CURR_ADVSTF_EMAIL");
			strMgrId = KycUtils.getCommonSessValue(request, KycConst.LOGGED_USER_MGRADVSTFID);
		}else{
			strAdvName = strAdviserName;
			strCustName = strCustomerName;
			strAdvstfEmailId = strAdvEmailId;
		}
		
		if(KycUtils.nullOrBlank(strApproverUserId)){
			strApproverUserId = KycUtils.getCommonSessValue(request, "LOGGED_USERID");
		}

		String mailStatus = "";
		
		
		
		if(strMangrApproveStatus.equalsIgnoreCase("APPROVE")){

			String managerEmailContent = "";

			String adminEmailId = resource.getString("appln.manager2admin.email").trim();
			String managerEmailSubject = resource.getString("appln.manager.subject").trim();
			managerEmailSubject = managerEmailSubject.replace("~ADVISER_NAME~", strAdvName);
			managerEmailSubject = managerEmailSubject.replace("~CLIENT_NAME~", strCustName);
			managerEmailSubject = managerEmailSubject.replace("~FNA_ID~", strFNAID);

			if(!KycUtils.nullOrBlank(hNTUCPolicyId)){
//			if(!KycUtils.nullOrBlank(strFnaPrinc) && strFnaPrinc.equalsIgnoreCase("NTUC") ){
				
				String logadvid = "",mgrflg="",distid="";
//				String url=resource.getString("kyc.link.approve")+"KycApproveLoginAction.action?";
//				String urlParam="txtFldAdvId="+logadvid+"&txtFldMgrAccsFlg="+mgrflg+"&txtFldDistId="+distid+"&txtFldFnaId="+strFNAID;
////				String encodedParam=Utility.encode(urlParam);
//				String encodedParam = " <a href='"+url+""+urlParam+"'>KYC-APPROVAL</a>";
				
				
				String url=resource.getString("kyc.link.approve")+"approval/admin?";
				String urlParam=logadvid+"&"+mgrflg+"&"+distid+"&"+strFNAID+"&a";
//				String encodedParam=Utility.encode(urlParam);
				
				byte[] urlenc = urlParam.getBytes("UTF-8");
				String encodedurlparam = DatatypeConverter.printBase64Binary(urlenc);
				
				String encodedParam = " <a href='"+url+""+encodedurlparam+"'>KYC-APPROVAL</a>";
				
				managerEmailContent = resource.getString("appln.manager.approve.email.ntuc.content").trim() ;
//				managerEmailContent = managerEmailContent + resource.getString("appln.reject.actionrequired");
				
				managerEmailContent = managerEmailContent + resource.getString("appln.approve.email.table.ntuc.Content.Start");
//						resource.getString("appln.approve.email.table.ntuc.Content.polno")+strPolicyNum+
//						resource.getString("appln.approve.email.table.ntuc.Content.date")+new SimpleDateFormat("dd/MM/yyyy").format(new Date())+
				managerEmailContent = managerEmailContent + resource.getString("appln.approve.email.table.ntuc.Content.advisername").replace("~ADVISER_NAME~", strAdvName);
				managerEmailContent = managerEmailContent + resource.getString("appln.approve.email.table.ntuc.Content.clientname").replace("~CLIENT_NAME~", strCustName);
				managerEmailContent = managerEmailContent + resource.getString("appln.approve.email.table.ntuc.Content.fnaid").replace("~FNA_ID~", strFNAID);
//						resource.getString("appln.approve.email.table.ntuc.Content.approvedby")+strApproverUserId+						
//						resource.getString("appln.approve.email.table.ntuc.Content.approvestatus")+strMangrApproveStatus+
//						resource.getString("appln.approve.email.table.ntuc.Content.fnaId")+strFNAID+
				managerEmailContent = managerEmailContent + 	resource.getString("appln.approve.email.table.ntuc.Content.End");
				managerEmailContent = managerEmailContent + resource.getString("appln.approve.email.ntuc.Content.message").replace("~APPROVAL_LINK~", encodedParam);
				managerEmailContent = managerEmailContent + resource.getString("appln.approve.email.ntuc.Content.appendix");
				
//			}else if(!KycUtils.nullOrBlank(strFnaPrinc) && strFnaPrinc.equalsIgnoreCase("NON-NTUC")){
			}else if(KycUtils.nullOrBlank(hNTUCPolicyId)){
				
				String logadvid = "",mgrflg="",distid="";
//				String url=resource.getString("kyc.link.approve")+"KycApproveLoginAction.action?";
//				String urlParam="txtFldAdvId="+logadvid+"&txtFldMgrAccsFlg="+mgrflg+"&txtFldDistId="+distid+"&txtFldFnaId="+strFNAID;
////				String encodedParam=Utility.encode(urlParam);
//				String encodedParam = " <a href='"+url+""+urlParam+"'>KYC-APPROVAL</a>";
				
				String url=resource.getString("kyc.link.approve")+"approval/admin?";
				String urlParam=logadvid+"&"+mgrflg+"&"+distid+"&"+strFNAID+"&a";
//				String encodedParam=Utility.encode(urlParam);
				
				byte[] urlenc = urlParam.getBytes("UTF-8");
				String encodedurlparam = DatatypeConverter.printBase64Binary(urlenc);
				
				String encodedParam = " <a href='"+url+""+encodedurlparam+"'>KYC-APPROVAL</a>";
				
				managerEmailContent = resource.getString("appln.manager.approve.email.content").trim();
//				managerEmailContent = managerEmailContent+ resource.getString("appln.reject.actionrequired");
				managerEmailContent = managerEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.Start");
				
				managerEmailContent = managerEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.advisername").replace("~ADVISER_NAME~", strAdvName);
				managerEmailContent = managerEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.clientname").replace("~CLIENT_NAME~", strCustName);				
				managerEmailContent = managerEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.fnaId").replace("~FNA_ID~", strFNAID);
				
//				String strMangrApproveStsTmp = KycUtils.toCamelCase(strMangrApproveStatus);
//				managerEmailContent = managerEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.approvestatus").replace("~MANAGER_STATUS~",strMangrApproveStsTmp);
//				managerEmailContent = managerEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.approvedby").replace("~MANAGER_STATUS~", strMangrApproveStsTmp).replace("~MANAGER_USERID~", strApproverUserId);
//				managerEmailContent = managerEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.date").replace("~MANAGER_STATUS~", strMangrApproveStsTmp).replace("~MANAGER_STATUSON~", new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
				
				managerEmailContent = managerEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.End");	
				managerEmailContent = managerEmailContent + resource.getString("appln.approve.email.nonntuc.Content.message").replace("~APPROVAL_LINK~", encodedParam);
				managerEmailContent = managerEmailContent + resource.getString("appln.approve.email.nonntuc.Content.appendix");
				
						
			}
			
			kyclog.info(managerEmailContent+"\n"+managerEmailSubject);
			


			String[] toReceipent = new String[1];
			toReceipent[0] = adminEmailId.trim();

			String[] toReceipentCC =  new String[2];			
			toReceipentCC[0] = strAdvstfEmailId; //strManagerEmailId
			
			if(strMgrId.equalsIgnoreCase(strApproverAdvStfId)){toReceipentCC[1]=";";}
			
			if(!strMgrId.equalsIgnoreCase(strApproverAdvStfId)){				
				toReceipentCC[1] = ";"+strMgrEmailId;
			}
			
			

//			managerEmailContent = managerEmailContent + resource.getString("approval.enquiry.msg").trim()+"";
			managerEmailContent = managerEmailContent + resource.getString("donotreplyemail").trim()+"<br/>";


			mailStatus = KycMail.sendMail("", toReceipent, toReceipentCC, managerEmailContent, managerEmailSubject);
			jsonObj.put("MANAGER_POLICY_STATUS", "FNA Form has been Approved");

		}else if(strMangrApproveStatus.equalsIgnoreCase("REJECT")){

			//strAdvstfEmailId = "sathish@avallis.com";

			String managerEmailSubject = resource.getString("appln.manager.reject.subject");
			managerEmailSubject = managerEmailSubject.replace("~ADVISER_NAME~", strAdvName);
			managerEmailSubject = managerEmailSubject.replace("~CLIENT_NAME~", strCustName);
			managerEmailSubject = managerEmailSubject.replace("~FNA_ID~", strFNAID);

			String managerEmailContent ="";

			if(!KycUtils.nullOrBlank(hNTUCPolicyId)){
				managerEmailContent = resource.getString("appln.manager.email.reject.content").trim();
//				managerEmailContent = managerEmailContent + resource.getString("appln.reject.actionrequired");
				managerEmailContent = managerEmailContent + resource.getString("appln.reject.email.table.nonntuc.Content.Start");
				managerEmailContent = managerEmailContent + resource.getString("appln.reject.email.table.nonntuc.Content.clientname").replace("~CLIENT_NAME~", strCustName);
				managerEmailContent = managerEmailContent + resource.getString("appln.reject.email.table.nonntuc.Content.fnaId").replace("~FNA_ID~", strFNAID);
				managerEmailContent = managerEmailContent + resource.getString("appln.reject.email.table.nonntuc.Content.End");
				managerEmailContent = managerEmailContent + resource.getString("appln.reject.email.nonntuc.Content.message");
				managerEmailContent = managerEmailContent + resource.getString("appln.reject.email.nonntuc.Content.comments.manager").replace("~MANAGER_COMMENTS~",strRemarks);				
				managerEmailContent = managerEmailContent + resource.getString("appln.reject.email.nonntuc.Content.appendix");

			}else if(KycUtils.nullOrBlank(hNTUCPolicyId)){
				
				managerEmailContent = resource.getString("appln.manager.email.reject.content").trim();
//				managerEmailContent = managerEmailContent + resource.getString("appln.reject.actionrequired");
				managerEmailContent = managerEmailContent + resource.getString("appln.reject.email.table.nonntuc.Content.Start");
				managerEmailContent = managerEmailContent + resource.getString("appln.reject.email.table.nonntuc.Content.clientname").replace("~CLIENT_NAME~", strCustName);
				managerEmailContent = managerEmailContent + resource.getString("appln.reject.email.table.nonntuc.Content.fnaId").replace("~FNA_ID~", strFNAID);
				managerEmailContent = managerEmailContent + resource.getString("appln.reject.email.table.nonntuc.Content.End");
				managerEmailContent = managerEmailContent + resource.getString("appln.reject.email.nonntuc.Content.message");
				managerEmailContent = managerEmailContent + resource.getString("appln.reject.email.nonntuc.Content.comments.manager").replace("~MANAGER_COMMENTS~",strRemarks);				
				managerEmailContent = managerEmailContent + resource.getString("appln.reject.email.nonntuc.Content.appendix.manager");
				
						  

			}


			String[] toReceipent = new String[1];
			toReceipent[0] = strAdvstfEmailId.trim();
			String[] toReceipentCC =  new String[1];
			
			if(strMgrId.equalsIgnoreCase(strApproverAdvStfId)){toReceipentCC=new String[0];}
			
			if(!strMgrId.equalsIgnoreCase(strApproverAdvStfId)){
				
				toReceipentCC[0] = strMgrEmailId;
			}


//			managerEmailContent = managerEmailContent + resource.getString("approval.enquiry.msg").trim();
			managerEmailContent = managerEmailContent + resource.getString("donotreplyemail").trim()+"<br/><br/>";
			kyclog.info("updateManagerApproveDets----> toReceipent-->"+toReceipent+",toReceipentCC--->"+toReceipentCC+",managerEmailContent-->"+managerEmailContent+",managerEmailSubject-->"+managerEmailSubject);

			mailStatus = KycMail.sendMail("", toReceipent, toReceipentCC, managerEmailContent, managerEmailSubject);
			
			
			kyclog.info("updateManagerApproveDets----> mailStatus----------->"+mailStatus);
			
			jsonObj.put("MANAGER_POLICY_STATUS", "FNA Form has been Rejected");
		}

		if(mailStatus.equalsIgnoreCase("SUCCESS")){
			
			kyclog.info("INSIDE MAIL SUCCESS .. GOING TO UPDATE THE STATUS INTO FNA "+ strFNAID +  strMangrApproveStatus + strRemarks );
//			SUPREV_MGR_FLG
//			SUPREV_FOLLOW_REASON
			String strMgrFlag = strMangrApproveStatus.equalsIgnoreCase("APPROVE")?"agree":(strMangrApproveStatus.equalsIgnoreCase("REJECT")?"disagree":"");
			String UPDATE_QRY = " update FNA_DETAILS set "
					+ "MGR_APPROVE_STATUS='"+strMangrApproveStatus+"'"
					+ ",MGR_STATUS_REMARKS='"+strRemarks.replace("\'", "\''")+"'"
					+ ",KYC_SENT_STATUS='YES'"
					+ ",MGR_APPROVE_DATE = SYSDATE"
					+ ",MGRAPPRSTS_BYADVSTF='"+strApproverUserId+"'"
					+ ",SUPREV_MGR_FLG='"+strMgrFlag+"'"
					+ ",SUPREV_FOLLOW_REASON='"+strRemarks.replace("\'", "\''")+"'"
					+ " WHERE FNA_ID='"+strFNAID+"'";
			
			kycServObj.updateManagerApproveStatus(dbDTO, UPDATE_QRY);
			

			
			jsonObj.put("MANAGER_KYC_SENT_STATUS", "YES");
			jsonObj.put("MANAGER_APPROVE_STATUS", strMangrApproveStatus);
			jsonObj.put("MANAGER_APPROVE_REMARKS", strRemarks);

			//kycServObj.updateManagerSentStatus(dbDTO, strFNAID, "YES");
		}else{
			jsonObj.put("MANAGER_KYC_SENT_STATUS", "");
			jsonObj.put("MANAGER_APPROVE_STATUS", "");
			jsonObj.put("MANAGER_APPROVE_REMARKS", "");
		}
		 

		jsonObj.put("MAIL_STATUS", mailStatus);
		jsonArr.put(jsonObj);

		mngrjsonObj.put("TAB_MANAGER_APPROVE_DETAILS", jsonArr);
		mngrjsonArr.put(mngrjsonObj);
		}catch (Exception e) {
			e.printStackTrace();
		}
 
		
		return mngrjsonArr;
	}


	public JSONArray adminManagerApproveDets(HttpServletRequest request) {

		JSONObject jsonObj = new JSONObject();
	    JSONArray jsonArr = new JSONArray();

	    JSONObject adminjsonObj = new JSONObject();
	    JSONArray adminjsonArr = new JSONArray();

		HttpSession sessObj = request.getSession(false);
		try {

		String strFNAID = request.getParameter("txtFldFnaId")!= null ? request.getParameter("txtFldFnaId") : "";
		String strAdminApproveStatus = request.getParameter("txtFldAdminAppStatus")!= null ? request.getParameter("txtFldAdminAppStatus") : "";
		String strManagerEmailId = request.getParameter("txtFldCompAppStatus")!= null ? request.getParameter("txtFldCompAppStatus") : "";
		String strRemarks = request.getParameter("txtFldAdminRemarks")!= null ? request.getParameter("txtFldAdminRemarks") : "";
		String hNTUCPolicyId = request.getParameter("hNTUCPolicyId")!= null ? request.getParameter("hNTUCPolicyId") : "";
		String strPolicyNum = request.getParameter("txtFldPolNum")!= null ? request.getParameter("txtFldPolNum") : "";
		String strApprAdvStfId = request.getParameter("txtFldApprAdvStfId")!= null ? request.getParameter("txtFldApprAdvStfId") : "";
		String strApprUserId = request.getParameter("txtFldApprUserId")!= null ? request.getParameter("txtFldApprUserId") : "";
		String strFnaPrinc = request.getParameter("txtFldFnaPrin") != null ? request.getParameter("txtFldFnaPrin") : "";
		
		FNAService kycServObj = new FNAService();

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);
		
		String strAdviserName = request.getParameter("txtFldAdviserName")!= null ? request.getParameter("txtFldAdviserName") : "";
		String strCustomerName = request.getParameter("txtFldCustName")!= null ? request.getParameter("txtFldCustName") : "";
		String strAdvEmailId = request.getParameter("txtFldAdvEmailId")!= null ? request.getParameter("txtFldAdvEmailId") : "";
		String strCustId= request.getParameter("txtFldCustId")!= null ? request.getParameter("txtFldCustId") : "";
		
		String strAdvName ="";
		String strCustName = "";
		String strAdvstfEmailId = "";
		String strDistId = KycUtils.getCommonSessValue(request,"LOGGED_DISTID");
		
		if(KycUtils.nullOrBlank(strAdviserName)){
			strAdvName = KycUtils.getCommonSessValue(request, "CURR_ADVSTF_NAME");
			strCustName = KycUtils.getCommonSessValue(request, "CUST_NAME");
			strAdvstfEmailId = KycUtils.getCommonSessValue(request, "CURR_ADVSTF_EMAIL");
			strCustId = KycUtils.getCommonSessValue(request, "STR_CUSTID");
		}else{
			strAdvName = strAdviserName;
			strCustName = strCustomerName;
			strAdvstfEmailId = strAdvEmailId;
			
		}

		if(KycUtils.nullOrBlank(strApprUserId)){
			strApprUserId = KycUtils.getCommonSessValue(request, "LOGGED_USERID");
		}
		
		
		String mailStatus = "";
	
		if(strAdminApproveStatus.equalsIgnoreCase("APPROVE")){

			String CompilanceEmailId = resource.getString("appln.admin2comp.email").trim();
			String adminEmailSubject = resource.getString("appln.admin.subject").trim();
			adminEmailSubject = adminEmailSubject.replace("~ADVISER_NAME~", strAdvName);
			adminEmailSubject = adminEmailSubject.replace("~CLIENT_NAME~", strCustName);
			adminEmailSubject = adminEmailSubject.replace("~FNA_ID~", strFNAID);
			
			String adminEmailContent = "";

			String[] toReceipent = new String[1];
			toReceipent[0] = CompilanceEmailId.trim();

			if(!KycUtils.nullOrBlank(strFnaPrinc) && strFnaPrinc.equalsIgnoreCase("NTUC")){
				
				String mgrflg= KycUtils.getCommonSessValue(request,"MANAGER_ACCESSFLG");
				String distid = KycUtils.getCommonSessValue(request,"LOGGED_DISTID");
				String loggeduserid = KycUtils.getCommonSessValue(request,"STR_LOGGEDUSER");
				String custname = KycUtils.getCommonSessValue(request,"CUST_NAME");
				String logadvid = KycUtils.getCommonSessValue(request,"LOGGED_ADVSTFID");
				
//				String url=resource.getString("kyc.link.approve")+"KycApproveLoginAction.action?";
//				String urlParam="txtFldAdvId="+logadvid+"&txtFldMgrAccsFlg="+mgrflg+"&txtFldDistId="+distid+"&txtFldFnaId="+strFNAID;
//				String encodedParam = " <a href='"+url+""+urlParam+"'>KYC-APPROVAL</a>";
				
				
				String url=resource.getString("kyc.link.approve")+"approval/compliance?";
				String urlParam=logadvid+"&"+mgrflg+"&"+distid+"&"+strFNAID+"&c";
//				String encodedParam=Utility.encode(urlParam);
				
				byte[] urlenc = urlParam.getBytes("UTF-8");
				String encodedurlparam = DatatypeConverter.printBase64Binary(urlenc);
				String encodedParam = " <a href='"+url+""+encodedurlparam+"'>KYC-APPROVAL</a>";
				
				adminEmailContent = resource.getString("appln.admin.approve.email.ntuc.content").trim();
//				adminEmailContent = adminEmailContent+ resource.getString("appln.reject.actionrequired");
				
				adminEmailContent = adminEmailContent + resource.getString("appln.approve.email.table.ntuc.Content.Start");
				
//						resource.getString("appln.approve.email.table.ntuc.Content.polno")+strPolicyNum+
//						resource.getString("appln.approve.email.table.ntuc.Content.date")+new SimpleDateFormat("dd/MM/yyyy").format(new Date())+
				adminEmailContent = adminEmailContent + resource.getString("appln.approve.email.table.ntuc.Content.advisername").replace("~ADVISER_NAME~", strAdvName) ;
				adminEmailContent = adminEmailContent + resource.getString("appln.approve.email.table.ntuc.Content.clientname").replace("~CLIENT_NAME~", strCustName);
				adminEmailContent = adminEmailContent + resource.getString("appln.approve.email.table.ntuc.Content.fnaid").replace("~FNA_ID~", strFNAID);				
				adminEmailContent = adminEmailContent + resource.getString("appln.approve.email.table.ntuc.Content.End");
				adminEmailContent = adminEmailContent + resource.getString("appln.admin.approve.email.ntuc.Content.message").replace("~APPROVAL_LINK~",encodedParam);
				adminEmailContent = adminEmailContent + resource.getString("appln.admmin.approve.email.ntuc.Content.appendix");

			}else if(!KycUtils.nullOrBlank(strFnaPrinc) && strFnaPrinc.equalsIgnoreCase("NON-NTUC")){
				
				String mgrflg= KycUtils.getCommonSessValue(request,"MANAGER_ACCESSFLG");
				String distid = KycUtils.getCommonSessValue(request,"LOGGED_DISTID");
				String loggeduserid = KycUtils.getCommonSessValue(request,"STR_LOGGEDUSER");
				String custname = KycUtils.getCommonSessValue(request,"CUST_NAME");
				String logadvid = KycUtils.getCommonSessValue(request,"LOGGED_ADVSTFID");
				
//				String url=resource.getString("kyc.link.approve")+"KycApproveLoginAction.action?";
//				String urlParam="txtFldAdvId="+logadvid+"&txtFldMgrAccsFlg="+mgrflg+"&txtFldDistId="+distid+"&txtFldFnaId="+strFNAID;
////				String encodedParam=Utility.encode(urlParam);
//				String encodedParam = " <a href='"+url+""+urlParam+"'>KYC-APPROVAL</a>";
				
				String url=resource.getString("kyc.link.approve")+"approval/compliance?";
				String urlParam=logadvid+"&"+mgrflg+"&"+distid+"&"+strFNAID+"&c";
//				String encodedParam=Utility.encode(urlParam);
				
				byte[] urlenc = urlParam.getBytes("UTF-8");
				String encodedurlparam = DatatypeConverter.printBase64Binary(urlenc);
				String encodedParam = " <a href='"+url+""+encodedurlparam+"'>KYC-APPROVAL</a>";
				
				adminEmailContent = resource.getString("appln.admin.approve.email.content").trim() ;
//				adminEmailContent = adminEmailContent+ resource.getString("appln.reject.actionrequired");
				
				adminEmailContent = adminEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.Start") ;
				adminEmailContent = adminEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.advisername").replace("~ADVISER_NAME~", strAdvName) ;
				adminEmailContent = adminEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.clientname").replace("~CLIENT_NAME~", strCustName);
				adminEmailContent = adminEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.fnaId").replace("~FNA_ID~", strFNAID);
				
				adminEmailContent = adminEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.End");
				adminEmailContent = adminEmailContent + resource.getString("appln.admin.approve.email.nonntuc.Content.message").replace("~APPROVAL_LINK~",encodedParam);
				adminEmailContent = adminEmailContent + resource.getString("appln.approve.email.nonntuc.Content.appendix");
				
				
			}

			String[] toReceipentCC =  new String[1];
			toReceipentCC[0] = strAdvstfEmailId;

//			adminEmailContent = adminEmailContent + resource.getString("approval.enquiry.msg").trim();
			adminEmailContent = adminEmailContent + resource.getString("donotreplyemail").trim()+"<br/><br/>";
			
			kyclog.info(adminEmailSubject +"\n"+adminEmailSubject);


			mailStatus = KycMail.sendMail("", toReceipent, toReceipentCC, adminEmailContent, adminEmailSubject );
			jsonObj.put("ADMIN_POLICY_STATUS", "FNA Form has been Approved By Admin.");

		}else if(strAdminApproveStatus.equalsIgnoreCase("REJECT")){

			String adminEmailSubject = resource.getString("appln.admin.reject.subject");
			adminEmailSubject = adminEmailSubject.replace("~ADVISER_NAME~", strAdvName);
			adminEmailSubject = adminEmailSubject.replace("~CLIENT_NAME~", strCustName);
			adminEmailSubject = adminEmailSubject.replace("~FNA_ID~", strFNAID);
			String adminEmailContent = "";

			String[] toReceipent = new String[1];
			toReceipent[0] = resource.getString("appln.manager2admin.email");

			String[] toReceipentCC =  new String[1];
			toReceipentCC[0] = strAdvstfEmailId.trim() ;

			if(!KycUtils.nullOrBlank(strFnaPrinc) && strFnaPrinc.equalsIgnoreCase("NTUC")){
				
				
				adminEmailContent = resource.getString("appln.admin.email.reject.ntuc.content").trim() ;
				
				adminEmailContent = adminEmailContent + resource.getString("appln.reject.email.table.nonntuc.Content.Start") ;
				adminEmailContent = adminEmailContent + resource.getString("appln.reject.email.table.nonntuc.Content.advisername").replace("~ADVISER_NAME~",strAdvName) ;
				adminEmailContent = adminEmailContent + resource.getString("appln.reject.email.table.nonntuc.Content.clientname").replace("~CLIENT_NAME~",strCustName) ;
				adminEmailContent = adminEmailContent + resource.getString("appln.reject.email.table.nonntuc.Content.fnaId").replace("~FNA_ID~",strFNAID) ;
				adminEmailContent = adminEmailContent + resource.getString("appln.reject.email.table.nonntuc.Content.End");
				
				adminEmailContent = adminEmailContent + resource.getString("appln.reject.email.nonntuc.Content.message");
				adminEmailContent = adminEmailContent + resource.getString("appln.reject.email.nonntuc.Content.comments.admin").replace("~MANAGER_COMMENTS~",strRemarks);
				adminEmailContent = adminEmailContent + resource.getString("appln.reject.email.nonntuc.Content.appendix");
				
				
			}else if(!KycUtils.nullOrBlank(strFnaPrinc) && strFnaPrinc.equalsIgnoreCase("NON-NTUC")){
				
				adminEmailContent = resource.getString("appln.admin.email.reject.content").trim() ;
				
				adminEmailContent = adminEmailContent + resource.getString("appln.reject.email.table.nonntuc.Content.Start") ;
				adminEmailContent = adminEmailContent + resource.getString("appln.reject.email.table.nonntuc.Content.advisername").replace("~ADVISER_NAME~",strAdvName) ;
				adminEmailContent = adminEmailContent + resource.getString("appln.reject.email.table.nonntuc.Content.clientname").replace("~CLIENT_NAME~",strCustName) ;
				adminEmailContent = adminEmailContent + resource.getString("appln.reject.email.table.nonntuc.Content.fnaId").replace("~FNA_ID~",strFNAID) ;
				adminEmailContent = adminEmailContent + resource.getString("appln.reject.email.table.nonntuc.Content.End");
				
				adminEmailContent = adminEmailContent + resource.getString("appln.reject.email.nonntuc.Content.message");
				adminEmailContent = adminEmailContent + resource.getString("appln.reject.email.nonntuc.Content.comments.admin").replace("~MANAGER_COMMENTS~",strRemarks);
				adminEmailContent = adminEmailContent + resource.getString("appln.reject.email.nonntuc.Content.appendix");
//				adminEmailContent = adminEmailContent + resource.getString("appln.reject.eamil.nonntuc.content.staffname").replace("~OPS_STAFF_NAME~","Nor");
				
			}

			adminEmailContent = adminEmailContent + resource.getString("donotreplyemail").trim()+"<br/><br/>";


			kyclog.info(adminEmailContent +"\n"+adminEmailSubject);
			
			mailStatus = KycMail.sendMail("", toReceipent, toReceipentCC, adminEmailContent, adminEmailSubject );
			jsonObj.put("ADMIN_POLICY_STATUS", "FNA Form has been Rejected By Admin");
		}

		if(mailStatus.equalsIgnoreCase("SUCCESS")){
			
			String UPDATE_QRY =" update FNA_DETAILS set ADMIN_APPROVE_STATUS='"+strAdminApproveStatus+"',"
					+ " ADMIN_REMARKS='"+strRemarks.replace("\'", "\''")+"',"
					+ " ADMIN_KYC_SENT_STATUS='YES' , "
					+ " ADMIN_APPROVE_DATE = SYSDATE , "
					+ " ADMINAPPRSTS_BYADVSTF = '"+strApprUserId+"'"
					+ " WHERE FNA_ID='"+strFNAID+"'";
			
			
			kycServObj.updateAdminApproveStatus(dbDTO, UPDATE_QRY);
			
			jsonObj.put("ADMIN_KYC_SENT_STATUS", "YES");
			jsonObj.put("ADMIN_APPROVE_STATUS", strAdminApproveStatus);
			jsonObj.put("ADMIN_APPROVE_REMARKS", strRemarks);
		}else{
			jsonObj.put("ADMIN_KYC_SENT_STATUS", "");
			jsonObj.put("ADMIN_APPROVE_STATUS", "");
			jsonObj.put("ADMIN_APPROVE_REMARKS", "");
		}

		jsonObj.put("MAIL_STATUS", mailStatus);
		jsonArr.put(jsonObj);

		adminjsonObj.put("TAB_ADMIN_APPROVE_DETAILS", jsonArr);
		adminjsonArr.put(adminjsonObj);
		}catch (Exception e) {
			e.printStackTrace();
		}


		return adminjsonArr;
	}

	public JSONArray compManagerApproveDets(HttpServletRequest request,HttpServletResponse response) {
		
		kyclog.info("inside compManagerApproveDets-->");

		JSONObject jsonObj = new JSONObject();
	    JSONArray jsonArr = new JSONArray();

	    JSONObject compjsonObj = new JSONObject();
	    JSONArray compjsonArr = new JSONArray();
	    JSONObject jsnDsfStatus = new JSONObject();
	    
	    org.json.JSONArray jsnArrPolData = new org.json.JSONArray();
		org.json.JSONObject polJson = new org.json.JSONObject();
		org.json.JSONObject polDetailsJsn = new org.json.JSONObject();
		org.json.JSONObject jsnTab = new org.json.JSONObject();
		
		List<String> polDets = new ArrayList<String>();
		String strPolNo = KycConst.STR_NULL_STRING,strPlanName=KycConst.STR_NULL_STRING,
				strPolNoCont = KycConst.STR_NULL_STRING;
		
		String strStatus=KycConst.STR_NULL_STRING,
				errorMsg=KycConst.STR_NULL_STRING;
	

		HttpSession sessObj = request.getSession(false);
		String base64PDFFile=KycConst.STR_NULL_STRING;
		String faCode= resource.getString("attach.firmCode");
		try {
			Enumeration paramNames = request.getParameterNames();
			while(paramNames.hasMoreElements()) {

				Object obj = paramNames.nextElement();
				String paramName = obj.toString();
				kyclog.info(paramName + "=" + request.getParameter(paramName));
			}

		String strFNAID = request.getParameter("txtFldFnaId")!= null ? request.getParameter("txtFldFnaId") : "";
		String strCompApproveStatus = request.getParameter("txtFldCompAppStatus")!= null ? request.getParameter("txtFldCompAppStatus") : "";
		String strManagerEmailId = request.getParameter("txtFldManagerEmailId")!= null ? request.getParameter("txtFldManagerEmailId") : "";
		String strRemarks = request.getParameter("txtFldCompRemarks")!= null ? request.getParameter("txtFldCompRemarks") : "";
		String hNTUCPolicyId = request.getParameter("hNTUCPolicyId")!= null ? request.getParameter("hNTUCPolicyId") : "";
		String strPolicyNum = request.getParameter("txtFldPolNum")!= null ? request.getParameter("txtFldPolNum") : "";
		String strAdviserName = request.getParameter("txtFldAdviserName")!= null ? request.getParameter("txtFldAdviserName") : "";
		String strCustomerName = request.getParameter("txtFldCustName")!= null ? request.getParameter("txtFldCustName") : "";
		String strAdvEmailId = request.getParameter("txtFldAdvEmailId")!= null ? request.getParameter("txtFldAdvEmailId") : "";
		String strApprAdvStfId = request.getParameter("txtFldApprAdvStfId")!= null ? request.getParameter("txtFldApprAdvStfId") : "";
		String strApprUserId = request.getParameter("txtFldApprUserId")!= null ? request.getParameter("txtFldApprUserId") : "";
		String strNtucCaseId = request.getParameter("txtFldNtucCaseId")!= null ? request.getParameter("txtFldNtucCaseId") : "";
		String strNtucCaseSts = request.getParameter("txtFldNtucCasests")!= null ? request.getParameter("txtFldNtucCasests") : "";
		String strNtucPolCrtdFlg = request.getParameter("txtFldNtucPolCrtdFlg")!= null ? request.getParameter("txtFldNtucPolCrtdFlg") : "";
//		String strAdvStfId = request.getParameter("ADVSTFID")!= null ? request.getParameter("ADVSTFID") : "";
		String strServAdvId = request.getParameter("SERVADVID") != null ? request.getParameter("SERVADVID")  :"";
		String strClntId = request.getParameter("txtFldCustId")!= null ? request.getParameter("txtFldCustId") : "";
//		System.out.println("strServAdvId------------->"+strServAdvId);
		String strDistId = "";
		String strFnaPrinc = request.getParameter("txtFldFnaPrin") != null ? request.getParameter("txtFldFnaPrin") : "";
		
		FNAService kycServObj = new FNAService();
		AdviserService as = new AdviserService();

//		ServletContext context = getServletContext();
//		WebApplicationContext contexts = WebApplicationContextUtils.getRequiredWebApplicationContext(context);
//		DBIntf dbDTO = (DBIntf) contexts.getBean("DBDAO");
		
		ApplicationContext ctx = ApplicationContextUtils
				.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);
		
		String strAdvName = KycUtils.getCommonSessValue(request, "CURR_ADVSTF_NAME");
		String strCustName = KycUtils.getCommonSessValue(request, "CUST_NAME");
		String mailStatus = "";
		String strAdvstfEmailId = KycUtils.getCommonSessValue(request, "CURR_ADVSTF_EMAIL");
		
		
		if(KycUtils.nullOrBlank(strAdviserName)){
			strAdvName = KycUtils.getCommonSessValue(request, "CURR_ADVSTF_NAME");
			strCustName = KycUtils.getCommonSessValue(request, "CUST_NAME");
			strAdvstfEmailId = KycUtils.getCommonSessValue(request, "CURR_ADVSTF_EMAIL");
		}else{
			strAdvName = strAdviserName;
			strCustName = strCustomerName;
			strAdvstfEmailId = strAdvEmailId;
		}
		
		if(KycUtils.nullOrBlank(strApprUserId)){
			strApprUserId = KycUtils.getCommonSessValue(request, "LOGGED_USERID");
		}
		
		JSONArray jsnArrUpdateKYC2 = new JSONArray();
		org.json.JSONObject jsnAccessTokenResponse = new org.json.JSONObject();
		
		if(strFnaPrinc.equalsIgnoreCase("NTUC")){
//			jsnAccessTokenResponse = oauth.getAccessToken(request);
		}
		String access_token = jsnAccessTokenResponse.has("access_token")  ? jsnAccessTokenResponse.getString("access_token") : "";
		
		kyclog.info("inside compManagerApproveDets ,for DSF link access_token-->"+access_token);
		kyclog.info("inside compManagerApproveDets ,Compliance selected status -->"+strCompApproveStatus);
		String strDSFStatus = "";
		
		if(strCompApproveStatus.equalsIgnoreCase("REJECT")){
			
			String complianceEmailSubject = resource.getString("appln.compliance.reject.subject");
			complianceEmailSubject = complianceEmailSubject.replace("~ADVISER_NAME~", strAdvName);
			complianceEmailSubject = complianceEmailSubject.replace("~CLIENT_NAME~", strCustName);
			complianceEmailSubject = complianceEmailSubject.replace("~FNA_ID~", strFNAID);
			
			String complianceEmailContent = "";
			
			if(strFnaPrinc.equalsIgnoreCase("NTUC")){
				
//				jsnArrUpdateKYC2 = oauth.updateKYC2toDSF(request, response,sessObj,null);
				
				jsnTab = jsnArrUpdateKYC2.getJSONObject(0);
				String strSts = jsnTab.getString("UPDATEKYC2_DSF_STATUS");
				
				kyclog.info("inside REJECT status, the updateKYC2 status  ->"+strSts);
				strDSFStatus = strSts;
				
				
				if(strSts.equalsIgnoreCase("success")){
					
					//strAdvstfEmailId = "sathish@avallis.com";

					String[] toReceipent = new String[1];
					toReceipent[0] = strAdvstfEmailId.trim();//resource.getString("appln.admin2comp.email").trim()+";";

					String[] toReceipentCC =  new String[1];
					toReceipentCC[0] = strManagerEmailId+";";


					


//					if(!KycUtils.nullOrBlank(hNTUCPolicyId)){
					if(!KycUtils.nullOrBlank(strNtucCaseId)){
//						complianceEmailContent = resource.getString("appln.compliance.email.reject.ntuc.content").trim() + " "+
//															new SimpleDateFormat("dd/MM/yyyy").format(new Date());
//						complianceEmailContent = complianceEmailContent+ resource.getString("appln.reject.actionrequired");
//						complianceEmailContent = complianceEmailContent +  resource.getString("appln.reject.email.table.ntuc.Content.Start")
//						+ strCustName + resource.getString("appln.reject.email.table.ntuc.Content.polNo")
//						+strPolicyNum+ resource.getString("appln.reject.email.table.ntuc.Content.date") +new SimpleDateFormat("dd/MM/yyyy").format(new Date())+
//						resource.getString("appln.reject.email.table.ntuc.Content.advisername")	+strAdvName+
//						resource.getString("appln.reject.email.table.ntuc.Content.approvedby")	+strApprUserId+
//						resource.getString("appln.reject.email.table.ntuc.Content.approvestatus")+strCompApproveStatus+
//						resource.getString("appln.reject.email.table.ntuc.Content.comments")+strRemarks+
//						resource.getString("appln.reject.email.table.ntuc.Content.fnaId")+strFNAID+
//						resource.getString("appln.reject.email.table.ntuc.Content.End")+"<br/>";
						
						
						complianceEmailContent = resource.getString("appln.compliance.email.reject.ntuc.content").trim();
						
						complianceEmailContent = complianceEmailContent + resource.getString("appln.reject.email.table.nonntuc.Content.Start") ;
						complianceEmailContent = complianceEmailContent + resource.getString("appln.reject.email.table.nonntuc.Content.advisername").replace("~ADVISER_NAME~",strAdvName) ;
						complianceEmailContent = complianceEmailContent + resource.getString("appln.reject.email.table.nonntuc.Content.clientname").replace("~CLIENT_NAME~",strCustName) ;
						complianceEmailContent = complianceEmailContent + resource.getString("appln.reject.email.table.nonntuc.Content.fnaId").replace("~FNA_ID~",strFNAID) ;
						complianceEmailContent = complianceEmailContent + resource.getString("appln.reject.email.table.nonntuc.Content.End");
						
						complianceEmailContent = complianceEmailContent + resource.getString("appln.compliance.reject.ntuc.Content.message");
						complianceEmailContent = complianceEmailContent + resource.getString("appln.compliance.reject.ntuc.Content.appendix");
						
					}
					
//					aug_2019
//					else{
//						complianceEmailContent = resource.getString("appln.compliance.email.reject.content").trim() + " "+ new SimpleDateFormat("dd/MM/yyyy").format(new Date());
//						complianceEmailContent = complianceEmailContent+ resource.getString("appln.reject.actionrequired");
//						complianceEmailContent = complianceEmailContent + resource.getString("appln.reject.email.table.nonntuc.Content.Start")
//								+ strCustName +	resource.getString("appln.reject.email.table.nonntuc.Content.date") + new SimpleDateFormat("dd/MM/yyyy").format(new Date())+
//								  resource.getString("appln.reject.email.table.nonntuc.Content.advisername")+strAdvName+
//								  resource.getString("appln.reject.email.table.nonntuc.Content.approvedby")+strApprUserId+
//								  resource.getString("appln.reject.email.table.nonntuc.Content.approvestatus")+strCompApproveStatus+
//								  resource.getString("appln.reject.email.table.nonntuc.Content.comments")+strRemarks+
//								  resource.getString("appln.reject.email.table.nonntuc.Content.fnaId")+strFNAID+
//								  resource.getString("appln.reject.email.table.nonntuc.Content.End");
//					}

//					complianceEmailContent = complianceEmailContent + resource.getString("approval.enquiry.msg").trim();
					complianceEmailContent = complianceEmailContent + resource.getString("donotreplyemail").trim()+"<br/><br/>";

					kyclog.info("inside REJECT status, mail going to send...  To->"+strAdvstfEmailId.trim()+",CC->"+strManagerEmailId+",mailcontent->"+complianceEmailContent);
					mailStatus = KycMail.sendMail("", toReceipent, toReceipentCC, complianceEmailContent, complianceEmailSubject + " " +new SimpleDateFormat("dd/MM/yyyy").format(new Date()));

//					jsonObj.put("COMP_POLICY_STATUS", "Policy has been rejected by Compliance");
//					compjsonObj.put("TAB_COMPILANCE_REJECT_DETAILS", jsonArr);
					
				}else{
					
					mailStatus ="FAIL";
					
					jsonObj.put("COMP_POLICY_STATUS", jsnTab.getString("UPDATEKYC2_STATUS")+"\n\nCase status updation failed at DSF system. Please Contact System Administrator.");
					
//					compjsonObj.put("TAB_COMPILANCE_REJECT_DETAILS", jsonArr);
					
				}
			}else{
				
				String[] toReceipent = new String[1];
				toReceipent[0] = strAdvstfEmailId.trim();//resource.getString("appln.admin2comp.email").trim()+";";

				String[] toReceipentCC =  new String[1];
				toReceipentCC[0] = strManagerEmailId+";";
				
					complianceEmailContent = resource.getString("appln.compliance.email.reject.content").trim();
					
//					complianceEmailContent = complianceEmailContent+ resource.getString("appln.reject.actionrequired");
					complianceEmailContent = complianceEmailContent + resource.getString("appln.reject.email.table.nonntuc.Content.Start");
					complianceEmailContent = complianceEmailContent + resource.getString("appln.reject.email.table.nonntuc.Content.clientname").replace("~CLIENT_NAME~",strCustName);
					complianceEmailContent = complianceEmailContent + resource.getString("appln.reject.email.table.nonntuc.Content.fnaId").replace("~FNA_ID~",strFNAID);
					complianceEmailContent = complianceEmailContent + resource.getString("appln.reject.email.table.nonntuc.Content.End");
					complianceEmailContent = complianceEmailContent + resource.getString("appln.reject.email.nonntuc.Content.message");
					complianceEmailContent = complianceEmailContent + resource.getString("appln.reject.email.nonntuc.Content.comments.compliance").replace("~MANAGER_COMMENTS~", strRemarks);
					complianceEmailContent = complianceEmailContent + resource.getString("appln.reject.email.nonntuc.Content.appendix.compliance");
//					complianceEmailContent = complianceEmailContent + resource.getString("appln.reject.eamil.nonntuc.content.staffname").replace("~OPS_STAFF_NAME~","Nor");
				

//					complianceEmailContent = complianceEmailContent + resource.getString("approval.enquiry.msg").trim();
					complianceEmailContent = complianceEmailContent + resource.getString("donotreplyemail").trim()+"<br/><br/>";

				kyclog.info("inside REJECT status, mail going to send...  To->"+strAdvstfEmailId.trim()+",CC->"+strManagerEmailId+",mailcontent->"+complianceEmailContent);
				mailStatus = KycMail.sendMail("", toReceipent, toReceipentCC, complianceEmailContent, complianceEmailSubject + " " +new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
			}
			
			
		}else if(strCompApproveStatus.equalsIgnoreCase("APPROVE")){
			
			if(strFnaPrinc.equalsIgnoreCase("NTUC")){
				

				kyclog.info("inside APPROVE status-->");
				if(!ServletFileUpload.isMultipartContent(request)){
					kyclog.error("While KYC approval , PDF uploading is failure ----------------->",new Exception());
//					throw new ServletException("Content type is not multipart/form-data");
				}
				
				File file = null;
				String filePath = "";
				String fileDispName = "";
				List<FileItem> fileItemsList = uploader.parseRequest(request);
				Iterator<FileItem> fileItemsIterator = fileItemsList.iterator();
				
				while(fileItemsIterator.hasNext()){
					
					FileItem fileItem = fileItemsIterator.next();
					
					if (fileItem.getFieldName().equals("fileKycCompApprove")) {
						
//						file = new File(ctx.getAttribute("FILES_DIR")+File.separator+fileItem.getName());
						file = new File(File.separator+fileItem.getName());
						filePath = file.getAbsolutePath();
						fileItem.write(file);
						fileDispName = fileItem.getName();
						
//						System.out.println("For Approval,Uploaded File Name is --->"+fileDispName);
//						kyclog.info("For Approval, Uploaded File Name is --->"+fileDispName);
						
						base64PDFFile=KycUtils.convertToBase64(file);
						request.setAttribute("BASE64PDFFILE", base64PDFFile);
						
//						System.out.println("base64 converted file is------------>"+base64PDFFile);
						kyclog.info("For Approval, Uploaded File Name is --->"+fileDispName + " >> base64 converted file is------------>"+base64PDFFile);
						
					}
				}
				
//				jsnArrUpdateKYC2 = oauth.updateKYC2toDSF(request, response,sessObj,null);
				
				
				jsnTab = jsnArrUpdateKYC2.getJSONObject(0);
				String strSts = jsnTab.getString("UPDATEKYC2_DSF_STATUS");
				
				strDSFStatus = strSts;
				
//				System.out.println("strDSFStatus ->"+strDSFStatus);
				kyclog.info("strDSFStatus ->"+strDSFStatus);

				if(strDSFStatus.equalsIgnoreCase("success")){
					
//					String CompilanceEmailId1 = resource.getString("appln.admin2comp.email").trim();
					String complianceEmailSubject = resource.getString("appln.compliance.approve.subject").trim();
					complianceEmailSubject = complianceEmailSubject.replace("~ADVISER_NAME~", strAdvName);
					complianceEmailSubject = complianceEmailSubject.replace("~CLIENT_NAME~", strCustName);
					complianceEmailSubject = complianceEmailSubject.replace("~FNA_ID~", strFNAID);
					
					String complianceEmailContent = "";
					String compITEmailCC = resource.getString("appln.it.admin.cc").trim();

					String[] toReceipent = new String[1];
					toReceipent[0] = strAdvstfEmailId;//CompilanceEmailId.trim();

					String[] toReceipentCC =  new String[1];
					toReceipentCC[0]=compITEmailCC;

//					if(!KycUtils.nullOrBlank(hNTUCPolicyId)){
					if(!KycUtils.nullOrBlank(strNtucCaseId)){
//						complianceEmailContent = resource.getString("appln.compliance.approve.email.ntuc.content").trim() + " "+
//															new SimpleDateFormat("dd/MM/yyyy").format(new Date());
//						complianceEmailContent = complianceEmailContent+ resource.getString("appln.reject.actionrequired");
//						complianceEmailContent = complianceEmailContent +resource.getString("appln.approve.email.table.ntuc.Content.Start")+strCustName+
//								resource.getString("appln.approve.email.table.ntuc.Content.polno")+strPolicyNum+
//								resource.getString("appln.approve.email.table.ntuc.Content.date")+new SimpleDateFormat("dd/MM/yyyy").format(new Date())+
//								resource.getString("appln.approve.email.table.ntuc.Content.advisername")+strAdvName+
//								resource.getString("appln.reject.email.table.ntuc.Content.approvedby")+strApprUserId+
//								resource.getString("appln.approve.email.table.ntuc.Content.approvestatus")+strCompApproveStatus+
//								resource.getString("appln.approve.email.table.ntuc.Content.fnaId")+strFNAID+
//								resource.getString("appln.approve.email.table.ntuc.Content.End")+"<br/>";
						
						complianceEmailContent = resource.getString("appln.compliance.approve.email.ntuc.content").trim();
						
						complianceEmailContent = complianceEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.Start");						
						complianceEmailContent =complianceEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.Start") ;
						complianceEmailContent = complianceEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.clientname").replace("~CLIENT_NAME~", strCustName);
						complianceEmailContent = complianceEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.fnaId").replace("~FNA_ID~", strFNAID);
						
						complianceEmailContent = complianceEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.End");
						complianceEmailContent = complianceEmailContent + resource.getString("appln.compliance.approve.email.ntuc.content.message");
						complianceEmailContent = complianceEmailContent + resource.getString("appln.compliance.approve.email.ntuc.content.appendix");
						
						
					}else{
						complianceEmailContent = resource.getString("appln.compliance.approve.email.content").trim();
//						complianceEmailContent = complianceEmailContent+ resource.getString("appln.reject.actionrequired");
						complianceEmailContent = complianceEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.Start");
						
						complianceEmailContent =complianceEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.Start") ;
						complianceEmailContent = complianceEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.clientname").replace("~CLIENT_NAME~", strCustName);
						complianceEmailContent = complianceEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.fnaId").replace("~FNA_ID~", strFNAID);
						
						complianceEmailContent = complianceEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.End");
						complianceEmailContent = complianceEmailContent + resource.getString("appln.approve.email.nonntuc.Content.status.msg");
						complianceEmailContent = complianceEmailContent + resource.getString("appln.approve.email.nonntuc.Content.appendix");
						
					}

//					complianceEmailContent = complianceEmailContent + resource.getString("approval.enquiry.msg").trim();
					complianceEmailContent = complianceEmailContent + resource.getString("donotreplyemail").trim()+"<br/><br/>";

					kyclog.info("Inside Approved mail is  going to send To ->"+strAdvstfEmailId +",CC->"+compITEmailCC +",mail->"+complianceEmailContent);
					mailStatus = KycMail.sendMail("", toReceipent, toReceipentCC, complianceEmailContent, complianceEmailSubject + " " +new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
					
				}else{
					
					mailStatus ="FAIL";
					
					jsonObj.put("COMP_POLICY_STATUS", jsnTab.getString("UPDATEKYC2_STATUS")+"\n\nCase status updation failed at DSF system. Please Contact System Administrator.");
//					compjsonObj.put("TAB_COMPILANCE_REJECT_DETAILS", jsonArr);
					
				}
				
			}else{//NON-NTUC
				String complianceEmailSubject = resource.getString("appln.compliance.approve.subject").trim();
				complianceEmailSubject = complianceEmailSubject.replace("~ADVISER_NAME~", strAdvName);
				complianceEmailSubject = complianceEmailSubject.replace("~CLIENT_NAME~", strCustName);
				complianceEmailSubject = complianceEmailSubject.replace("~FNA_ID~", strFNAID);
				String complianceEmailContent = "";
				String compITEmailCC = resource.getString("appln.it.admin.cc").trim();

				String[] toReceipent = new String[1];
				toReceipent[0] = strAdvstfEmailId;//CompilanceEmailId.trim();

				String[] toReceipentCC =  new String[1];
				toReceipentCC[0]=compITEmailCC;
				
				 
				complianceEmailContent = resource.getString("appln.compliance.approve.email.content").trim();
//				complianceEmailContent = complianceEmailContent+ resource.getString("appln.reject.actionrequired");
				complianceEmailContent = complianceEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.Start");
				
				complianceEmailContent =complianceEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.Start") ;
				complianceEmailContent = complianceEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.clientname").replace("~CLIENT_NAME~", strCustName);
				complianceEmailContent = complianceEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.fnaId").replace("~FNA_ID~", strFNAID);
				
				complianceEmailContent = complianceEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.End");
				complianceEmailContent = complianceEmailContent + resource.getString("appln.approve.email.nonntuc.Content.status.msg");
				complianceEmailContent = complianceEmailContent + resource.getString("appln.approve.email.nonntuc.Content.appendix");
				
					/*complianceEmailContent = resource.getString("appln.compliance.approve.email.content").trim() ;
					complianceEmailContent = complianceEmailContent+ resource.getString("appln.reject.actionrequired");
					complianceEmailContent = complianceEmailContent + resource.getString("appln.approve.email.table.nonntuc.Content.Start") +strCustName+
							resource.getString("appln.approve.email.table.nonntuc.Content.date")
					+new SimpleDateFormat("dd/MM/yyyy").format(new Date())+
					resource.getString("appln.approve.email.table.nonntuc.Content.advisername")+strAdvName+
					resource.getString("appln.reject.email.table.nonntuc.Content.approvedby")+strApprUserId+
					resource.getString("appln.approve.email.table.nonntuc.Content.approvestatus")+strCompApproveStatus+
					resource.getString("appln.approve.email.table.nonntuc.Content.fnaId")+strFNAID+
					resource.getString("appln.approve.email.table.nonntuc.Content.End")+"<br/>";*/
				 

//				complianceEmailContent = complianceEmailContent + resource.getString("approval.enquiry.msg").trim();
				complianceEmailContent = complianceEmailContent + resource.getString("donotreplyemail").trim()+"<br/><br/>";

				kyclog.info("Inside Approved mail is  going to send To ->"+strAdvstfEmailId +",CC->"+compITEmailCC +",mail->"+complianceEmailContent);
				mailStatus = KycMail.sendMail("", toReceipent, toReceipentCC, complianceEmailContent, complianceEmailSubject + " " +new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
				
				
			}
			
			
		}
		
//		System.out.println("Mail Sent Status to Complaince -->"+mailStatus);
		kyclog.info("Mail Sent Status to Complaince -->"+mailStatus);

		if(mailStatus.equalsIgnoreCase("SUCCESS")){
			
			 String UPDATE_QRY=" update FNA_DETAILS set COMP_APPROVE_STATUS='"+strCompApproveStatus+"',"
			 		+ " COMP_REMARKS='"+strRemarks.replace("\'", "\''")+"',"
			 		+ "COMP_KYC_SENT_STATUS='YES', "
			 		+ "COMP_APPROVE_DATE = SYSDATE ,"
			 		+ "COMPAPPRSTS_BYADVSTF='"+strApprUserId+"'"
			 		+ "WHERE FNA_ID='"+strFNAID+"'";
			 
			 kyclog.info(UPDATE_QRY);
			 
			 kycServObj.updateCompApproveStatus(dbDTO, UPDATE_QRY);
			 
			 if(strFnaPrinc.equalsIgnoreCase("NTUC")){
				 
				 if(strCompApproveStatus.equalsIgnoreCase("APPROVE")){
					 
					 jsonObj.put("COMP_POLICY_STATUS", "Policy has been Approved by Compliance.");
					 	
					 	polJson.put("caseId", strNtucCaseId);
						polJson.put("faCode",faCode);	
						
						kyclog.info("Prepared to get the policy no and policy product details params --->"+polJson +",access_token->"+access_token);
						
						String strSchdAt = ntucprop.getString("ntuc.retrive.pol.after.min");
						
						long ONE_MINUTE_IN_MILLIS=60000;
						Calendar date = Calendar.getInstance();
						long t= date.getTimeInMillis();
						Date aftermin=new Date(t + (Long.parseLong(strSchdAt)* ONE_MINUTE_IN_MILLIS));
						
//						System.out.println("-----> Policy No API will be Trigger at --->"+KycUtils.convertDateToDateTimeString(date.getTime()));
						kyclog.info("-----> Policy No API will be  Trigger at --->"+KycUtils.convertDateToDateTimeString(date.getTime()));
//						System.out.println("-----> Policy No API will be   start at --->"+ KycUtils.convertDateToDateTimeString(aftermin));		
						kyclog.info("-----> Policy No API will be  start at --->"+ KycUtils.convertDateToDateTimeString(aftermin));
						
						
						
//						Scheduler scheduler = new StdSchedulerFactory().getScheduler();
						
//						JobDetail job = newJob(RetrievePolicy2SJob.class).withIdentity("job1", "group1").build();
						
//						Trigger trigger = newTrigger()
//							      .withIdentity("trigger", "group")
//							      .startAt(aftermin)            
//							      .build(); 
						
//						JobDataMap jobDataMap=  job.getJobDataMap();
//						jobDataMap.put(RetrievePolicy2SJob.dAccess_token,access_token);
//						jobDataMap.put(RetrievePolicy2SJob.dPolJson,polJson.toString());
//						jobDataMap.put(RetrievePolicy2SJob.dMailStatus,mailStatus);
//						jobDataMap.put(RetrievePolicy2SJob.dStrFNAID,strFNAID);
//						jobDataMap.put(RetrievePolicy2SJob.dStrNtucCaseId,strNtucCaseId);
//						jobDataMap.put(RetrievePolicy2SJob.dSession,request.getSession());
//						jobDataMap.put(RetrievePolicy2SJob.dStrServAdvId,strServAdvId);
//						jobDataMap.put(RetrievePolicy2SJob.dStrAdviserName,strAdviserName);
//						jobDataMap.put(RetrievePolicy2SJob.dStrCustomerName,strCustomerName);
//						jobDataMap.put(RetrievePolicy2SJob.dStrCompApproveStatus,strCompApproveStatus);
//						jobDataMap.put(RetrievePolicy2SJob.dStrManagerEmailId,strManagerEmailId);
//						jobDataMap.put(RetrievePolicy2SJob.dStrRemarks,strRemarks);
//						jobDataMap.put(RetrievePolicy2SJob.dHNTUCPolicyId,hNTUCPolicyId);
//						jobDataMap.put(RetrievePolicy2SJob.dStrPolicyNum,strPolicyNum);
//						jobDataMap.put(RetrievePolicy2SJob.dStrAdvEmailId,strAdvEmailId);
//						jobDataMap.put(RetrievePolicy2SJob.dStrApprAdvStfId,strApprAdvStfId);
//						jobDataMap.put(RetrievePolicy2SJob.dStrApprUserId,strApprUserId);
//						jobDataMap.put(RetrievePolicy2SJob.dStrClntId,strClntId);
						
//						scheduler.scheduleJob(job, trigger);
//						 kyclog.info("Retrieve Policy  ---------------->"+scheduler.getMetaData().getSummary());
						
//						polDetailsJsn = policyinfo.retrievePolicy2PolNo(access_token, polJson.toString());  // commented on 05/11/2018-kavi
						
						kyclog.info(polDetailsJsn);
						
						
						if(polDetailsJsn.has("status")){
							strStatus = polDetailsJsn.getString("status");
							
							if(strStatus.equalsIgnoreCase("success")){
								if(polDetailsJsn.has("data")){
									jsnArrPolData = polDetailsJsn.getJSONArray("data");
								}
								errorMsg = KycConst.STR_NULL_STRING;
								
							}else if(strStatus.equalsIgnoreCase("failed")){				
								jsnArrPolData=new org.json.JSONArray();		 
								errorMsg = polDetailsJsn.getString("errorMsg");
								jsonObj.put("POLICY_FETCH_FAIL", mailStatus);
							}	
						}
						
						
						int totalprod = jsnArrPolData.length();
						if(totalprod>0){
							
							List<FnaNtucPolicyDets> polInsBasic = new ArrayList<FnaNtucPolicyDets>();

							
							for(int count=0;count<totalprod;count++){
								
								org.json.JSONObject jsnPolInfo = jsnArrPolData.getJSONObject(count);
								
								String strprodid = jsnPolInfo.has("productId") ? jsnPolInfo.getString("productId") : "";
//								String strstatus = jsnPolInfo.getString("status");
								String strpolicyno = jsnPolInfo.has("policyNo") ? jsnPolInfo.getString("policyNo") : "";
								
								strPolNo += strpolicyno+",";
								strPlanName += strprodid+",";
								strPolNoCont += "<b>"+strpolicyno +"</b> [Plan Name :" +strprodid+"]"+",";
								
								FnaNtucPolicyDets fnaNtucPol = new FnaNtucPolicyDets(null,strFNAID,strNtucCaseId,strpolicyno,strprodid,"AVALLIS-DSF",new Date());
								
								
								polInsBasic.add(fnaNtucPol);
							}
							
//							if(!KycUtils.nullOrBlank(strPolNo)){
//								strFNAID,strNtucCaseId,strpolicyno,strprodid,"AVALLIS-DSF",SYSDATE
//							Since Policy is not created automatically, no need to update
//								utilserv.updatePolDetsByCaseId(dbDTO, context, strNtucCaseId, strPolNo);
//							utilserv.insertNtucPolDets(dbDTO, polInsBasic);
							
								
								String servAdvEmailId1="",advMgrId="",servAdvStfName="";
								
//								String servAdvstaffId= KycUtils.getCommonSessValue(request,"CUST_SERVADVID");	
								String mgrflg= KycUtils.getCommonSessValue(request,"MANAGER_ACCESSFLG");
								String distid = KycUtils.getCommonSessValue(request,"LOGGED_DISTID");
								String loggeduserid = KycUtils.getCommonSessValue(request,"STR_LOGGEDUSER");
//								String custname = KycUtils.getCommonSessValue(request,"CUST_NAME");
								String logadvid = KycUtils.getCommonSessValue(request,"LOGGED_ADVSTFID");
								
								String strhtmlstart=resource.getString("appln.email.html.style");
								String strhtmlend=resource.getString("appln.email.end.html");	
								
								List<AdviserStaff>  lstAdvStfDets = new ArrayList<AdviserStaff>();
								lstAdvStfDets = as.getAdviserStaffDetails(dbDTO, strServAdvId);
								
								if(lstAdvStfDets.size()>0){
									Iterator advite = lstAdvStfDets.iterator();
									while(advite.hasNext()){
										AdviserStaff advstf = (AdviserStaff)advite.next();
										servAdvEmailId1 = advstf.getEmailId();
										advMgrId = advstf.getManagerId();
										servAdvStfName =advstf.getAdvstfName();
									}
								}
								

//								String url=resource.getString("kyc.link.approve")+"KycApproveLoginAction.action?";
//								String urlParam="txtFldAdvId="+logadvid+"&txtFldMgrAccsFlg="+mgrflg+"&txtFldDistId="+distid+"&txtFldFnaId="+strFNAID;
////								String encodedParam=KycUtils.encode(urlParam);
//								String encodedParam = " <a href='"+url+""+urlParam+"'>KYC-APPROVAL</a>";
								
								
								String url=resource.getString("kyc.link.approve")+"approval/compliance?";
								String urlParam=logadvid+"&"+mgrflg+"&"+distid+"&"+strFNAID+"&c";
//								String encodedParam=KycUtils.encode(urlParam);
								
								byte[] urlenc = urlParam.getBytes("UTF-8");
								String encodedurlparam = DatatypeConverter.printBase64Binary(urlenc);
//								System.out.println(encodedurlparam);
								String encodedParam = " <a href='"+url+""+encodedurlparam+"'>KYC-APPROVAL</a>";
								
								String stradminToEmailId = resource.getString("attach.ntuc.admin.toemailid");
								String stradminCCEmailId = resource.getString("attach.ntuc.compliance.ccemailid");
								
								if(!KycUtils.nullOrBlank(strPolNo)){
									strPolNo = strPolNo.substring(0, strPolNo.length()-1);
									strPolNoCont = strPolNoCont.substring(0, strPolNoCont.length()-1);
								}
								
								
								if(!KycUtils.nullOrBlank(strPlanName)){
									strPlanName= strPlanName.substring(0, strPlanName.length()-1);
								}
								
								String strAdminMailContent = resource.getString("attach.ntuc.admin.mailContent");
								String strAdminMailContent1 = strAdminMailContent.replace("~ADVISER_NAME~", strAdviserName);
								String strAdminMailContent2 = strAdminMailContent1.replace("~CUSTOMER_NAME~", strCustomerName);
								String strAdminMailContent3 = strAdminMailContent2.replace("~FNA_ID~", strFNAID);
								String strAdminMailContent4 = strAdminMailContent3.replace("~APPROVAL_LINK~", encodedParam);
								
								String strAdminMailContent5 = strAdminMailContent4.replace("~ADVISER_NAME~", strAdviserName);
								String strAdminMailContent6 = strAdminMailContent5.replace("~CUSTOMER_NAME~", strCustomerName);
								String strAdminMailContent7 = strAdminMailContent6.replace("~FNA_ID~", strFNAID);
								strAdminMailContent7 = strAdminMailContent6.replace("~POLICYNO~",strPolNoCont );
								
								
								strAdminMailContent7 = strhtmlstart + strAdminMailContent7 + resource.getString("donotreplyemail").trim()+"<br/>"+ strhtmlend;;
								
								String compMailSubject = resource.getString("attach.ntuc.admin.subject");
								String compMailSubject1 = compMailSubject.replace("~ADVISER_NAME~", strAdviserName);
								String compMailSubject2 = compMailSubject1.replace("~CUSTOMER_NAME~", strCustomerName);
								String compMailSubject3 = compMailSubject2.replace("~POLICYNO~", strPolNo);		
								String compMailSubject4 = compMailSubject3.replace("~PLAN_NAME~", strPlanName);
								
								String[] adminToReceipent=new String[1];//{servAdvMgrEmailId};
								String[] adminToReceipentCC=new String[1];//{servAdvEmailId};
								
								adminToReceipent[0]=stradminToEmailId;
								adminToReceipentCC[0]=stradminCCEmailId;
								
								kyclog.info("-----Sending Mail for admin approval: ----");
								
//								System.out.println("To:"+adminToReceipent+",Content : "+strAdminMailContent7+",Subject:"+compMailSubject4);
								kyclog.info("To:"+stradminToEmailId+"CC:"+stradminCCEmailId+",Content : "+strAdminMailContent7+",Subject:"+compMailSubject4);		
								
								String compEmailAckMsg=KycMail.sendMail("",adminToReceipent, adminToReceipentCC,strAdminMailContent7,compMailSubject4);
								mailStatus=compEmailAckMsg;
								
								kyclog.info("admin approval mail sent status ->"+mailStatus);		
								
								
								sendNricAttachmentMail(request);//to send mail
								
//							}
							
						}
					 
				 }else if(strCompApproveStatus.equalsIgnoreCase("REJECT")){
					 
					 jsonObj.put("COMP_POLICY_STATUS", "FNA Form has been Rejected by Compliance.");
					 
				 }
				 
			 }else{
				 
				 if(strCompApproveStatus.equalsIgnoreCase("APPROVE")){					 
					 jsonObj.put("COMP_POLICY_STATUS", "APPROVED");
				 }else if(strCompApproveStatus.equalsIgnoreCase("REJECT")){
					 
					 jsonObj.put("COMP_POLICY_STATUS", "REJECTED");
					 
				 }
				 
			 }
			 
				
			 	
				
				
			 if(strFnaPrinc.equalsIgnoreCase("NTUC")){
//				org.json.JSONObject dsfupdatests = jsnArrUpdateKYC2.getJSONObject(0);				
				jsnDsfStatus.put("UPDATEKYC2_STATUS", jsnTab.getString("UPDATEKYC2_STATUS"));
				jsnDsfStatus.put("UPDATEKYC2_DSF_STATUS", strDSFStatus);
			 }

			jsonObj.put("COMP_KYC_SENT_STATUS", "YES");
			jsonObj.put("COMP_APPROVE_STATUS", strCompApproveStatus);
			jsonObj.put("COMP_APPROVE_REMARKS", strRemarks);			
//			jsonObj.put("COMP_POLICY_STATUS", "Policy has been Approved by Compliance.");
			compjsonObj.put("TAB_COMPILANCE_REJECT_DETAILS", jsonArr);
			jsonObj.put("MAIL_STATUS", mailStatus);	
			
			jsonArr.put(jsonObj);

		} else{
			jsonObj.put("COMP_KYC_SENT_STATUS", "");
			jsonObj.put("COMP_APPROVE_STATUS", "");
			jsonObj.put("COMP_APPROVE_REMARKS", "");
			jsonObj.put("MAIL_STATUS", mailStatus);
			
			if(strFnaPrinc.equalsIgnoreCase("NTUC")){
				jsnDsfStatus.put("UPDATEKYC2_STATUS", jsnTab.getString("UPDATEKYC2_STATUS"));
				jsnDsfStatus.put("UPDATEKYC2_DSF_STATUS", strDSFStatus);
			}

			
			jsonArr.put(jsonObj);
		}

		
		compjsonObj.put("TAB_COMPILANCE_REJECT_DETAILS", jsonArr);
				
		compjsonArr.put(compjsonObj);
		compjsonArr.put(jsnDsfStatus);
		
		
//		System.out.println(compjsonArr);
		kyclog.info(compjsonArr);
		
		
		return compjsonArr;
		}catch (Exception e) {
			e.printStackTrace();
			compjsonObj.put("TAB_COMPILANCE_REJECT_DETAILS_ERROR", "Error in Compliance Update->"+e.getMessage());
			compjsonArr.put(compjsonObj);
			return compjsonArr;
		}


		
	}


private void sendNricAttachmentMail(HttpServletRequest request){
//	ServletContext context = getServletContext();
//	WebApplicationContext contexts = WebApplicationContextUtils.getRequiredWebApplicationContext(context);
//	DBIntf dbDTO = (DBIntf) contexts.getBean("DBDAO");
	ApplicationContext ctx = ApplicationContextUtils
			.getApplicationContext();
	DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);
	
	HttpSession sess = request.getSession(false);
	ResultSet attachmentNRICList =null;
	
	JSONObject jsnObj=new JSONObject();
	JSONArray sessMapArr=(JSONArray) sess.getAttribute("COMMON_SESS_OBJ");
	jsnObj=(JSONObject) sessMapArr.get(0);
	
	
	String strFNAID = request.getParameter("txtFldFnaId")!= null ? request.getParameter("txtFldFnaId") : "";
	String strCompApproveStatus = request.getParameter("txtFldCompAppStatus")!= null ? request.getParameter("txtFldCompAppStatus") : "";
	String strManagerEmailId = request.getParameter("txtFldManagerEmailId")!= null ? request.getParameter("txtFldManagerEmailId") : "";
	String strRemarks = request.getParameter("txtFldCompRemarks")!= null ? request.getParameter("txtFldCompRemarks") : "";
	String hNTUCPolicyId = request.getParameter("hNTUCPolicyId")!= null ? request.getParameter("hNTUCPolicyId") : "";
	String strPolicyNum = request.getParameter("txtFldPolNum")!= null ? request.getParameter("txtFldPolNum") : "";
	String strAdviserName = request.getParameter("txtFldAdviserName")!= null ? request.getParameter("txtFldAdviserName") : "";
	String strCustomerName = request.getParameter("txtFldCustName")!= null ? request.getParameter("txtFldCustName") : "";
	String strAdvEmailId = request.getParameter("txtFldAdvEmailId")!= null ? request.getParameter("txtFldAdvEmailId") : "";
	String strApprAdvStfId = request.getParameter("txtFldApprAdvStfId")!= null ? request.getParameter("txtFldApprAdvStfId") : "";
	String strApprUserId = request.getParameter("txtFldApprUserId")!= null ? request.getParameter("txtFldApprUserId") : "";
	String strClntId = request.getParameter("txtFldCustId")!= null ? request.getParameter("txtFldCustId") : "";
	
	
	String strDistId=jsnObj.getString("LOGGED_DISTID");
	
	CustomerAttachService serv = new CustomerAttachService();
	
	attachmentNRICList = serv.downloadNRICAttachments(dbDTO, strClntId,strDistId,null,strFNAID);
	
	try{
	BFILE attachDoc = null;
	ArrayList<File> fileList=new ArrayList<File>();
	
	String attachFileName = null;
    String attachFileSize = null;
    String attachFileType = null;
    String filename = "";
    int count=0;
    Boolean recordSts=true;
    
//    ArrayList<File> files=new ArrayList<File>();//temporary for testing start
    
//    if (KycUtils.nullObj(attachmentNRICList) || !attachmentNRICList.isBeforeFirst() ) {
//    	recordSts=false;
//    }
    
    if(recordSts){
	 while(attachmentNRICList.next()){
		 
		 
			attachDoc= ((OracleResultSet )attachmentNRICList).getBFILE(1);
			attachFileName =  attachmentNRICList.getString(2);
			attachFileSize = attachmentNRICList.getString(3);
			attachFileType = attachmentNRICList.getString(4);

			kyclog.info("attachDoc-->"+attachDoc + ",attachDoc name-->"+attachDoc.getName() + ",attachDoc name-->"+attachDoc.getDirAlias() +",attachFileSize-->"+attachFileSize+",attachFileName-->"+attachFileName+",attachFileType-->"+attachFileType );
			
			
			if(attachDoc.fileExists()){

				filename = KycUtils.getBFILEDirPath(KycConst.GLBL_MODULE_CLNT)+attachDoc.getName();
				
				kyclog.info("filename-------->"+filename);

				File attchFile=new File(filename);
				fileList.add(attchFile);
				
			}
			
     }
    }
	
  
  
//	 File file1= new File(resource.getString("attachment.bfile.temp.path")+"AngularJS.pdf");
//	 File file2= new File(resource.getString("attachment.bfile.temp.path")+"dasffdaf.txt");
//	
//	 files.put(file1);
//	 files.put(file2);//end
	 
	 String[] toReceipent={resource.getString("attachment.to.receipent")};
	 String[] toReceipentCC={};
	 String[] toReceipentBCC={resource.getString("attachment.bcc.receipent")};
	 String msgContent =resource.getString("attachment.mailcontent");
	 String Subj=resource.getString("attachment.mailsubject");
	 String Subj1 = Subj.replace("~POLICY_HOLDER_NAME~", strCustomerName);
	 
	
	 //need to pass fileList
	 KycMail.sendMailWithAttachAsFile(toReceipent, toReceipentCC,toReceipentBCC, msgContent, Subj1,fileList);
	 
	}catch(Exception e){
//		e.printStackTrace();
		kyclog.error("Error in NRIC document mail sending---->",e);
	}
}



	
	
	
	//vignesh add on 20-05-2021
	private JSONArray getapprovelmanager(HttpServletRequest request) throws UnsupportedEncodingException {
		
		 JSONArray urlApprovrlmge = new JSONArray();
		 String logadvid = request.getParameter("strLogAdvID") != null ? request.getParameter("strLogAdvID") : "";
		 String mgrflg = request.getParameter("strMgrFlg") != null ? request.getParameter("strMgrFlg") : "";
		 String distid = request.getParameter("strDistID") != null ? request.getParameter("strDistID") : "";
		 String strFnaId = request.getParameter("strFnaID") != null ? request.getParameter("strFnaID") : "";
		 String strLoggedUserId=request.getParameter("strUid") != null ? request.getParameter("strUid") : "";
		 
		 
		 String url=resource.getString("kyc.link.approve")+"approval/directmanager?";
		 String urlParam=logadvid+"&"+mgrflg+"&"+distid+"&"+strFnaId+"&direct&"+strLoggedUserId;
			
		 byte[] urlenc = urlParam.getBytes("UTF-8");
		 String encodedurlparam = DatatypeConverter.printBase64Binary(urlenc);
		 
		 String urlparam=url+""+encodedurlparam;
		 urlApprovrlmge.put(urlparam);
 
		 return urlApprovrlmge;
	
	}
	//End
	
	
	public JSONArray sendMailToManagerNonNtuc(HttpServletRequest request) throws Exception{

		 JSONArray sendEmail = new JSONArray();
		 JSONArray mailContJsonArr = new JSONArray();
		 JSONObject jsonObj = new JSONObject();
		 JSONObject sendMailJsonObj = new JSONObject();
		 
		AdviserService serv = new AdviserService();
		FNAService fnaServ =new FNAService();
//		ServletContext context = getServletContext();
//		WebApplicationContext contexts = WebApplicationContextUtils.getRequiredWebApplicationContext(context);
//		DBIntf dbDTO = (DBIntf) contexts.getBean("DBDAO");
		ApplicationContext ctx = ApplicationContextUtils
				.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN); 
		HttpSession session = request.getSession(false);
		 
		 String strFnaId = request.getParameter("strFNAId") != null ? request.getParameter("strFNAId") : null;
		 String strntucCaseId = request.getParameter("ntuccaseid") != null ? request.getParameter("ntuccaseid") : null;
		 
		 List advMgrList = serv.getFNAAdvMgrDets(dbDTO, strFnaId);
		 Iterator ite = advMgrList.iterator();
		 
		 String strMgrAdvStfId = "",
				 strMgrEmailId ="",
				strMgrAdvName= "",
				strMgrAdvInitial="",
				strCustId = "",strAdvName="",strCustName = "";
		 
		 //poovathi add on 18-08-2021
		 String prxyMgrDetsEmailId=KycConst.STR_NULL_STRING;
		 
		 String mailReport = "";
		 
		 while(ite.hasNext()){
			 FnaDetails fnaDets = (FnaDetails)ite.next();
			 
			 strCustId =  KycUtils.nullOrBlank(fnaDets.getCustId()) ? "" : fnaDets.getCustId();
//			 strCustName = KycUtils.nullOrBlank(fnaDets.getCustomerDetails().getCustName())?"":fnaDets.getCustomerDetails().getCustName();
			 strAdvName = KycUtils.nullOrBlank(fnaDets.getAdviserStaffByAdvstfId().getAdvstfName())?"":fnaDets.getAdviserStaffByAdvstfId().getAdvstfName();
			 strMgrAdvStfId = KycUtils.checkNullVal(fnaDets.getAdviserStaffByMgrId())  ? "" : fnaDets.getAdviserStaffByMgrId().getAdvstfId();
			 strMgrEmailId = KycUtils.checkNullVal(fnaDets.getAdviserStaffByMgrId()) ? "" : fnaDets.getAdviserStaffByMgrId().getEmailId();
			 strMgrAdvName = KycUtils.checkNullVal(fnaDets.getAdviserStaffByMgrId()) ? "" : fnaDets.getAdviserStaffByMgrId().getAdvstfName();
			 strMgrAdvInitial = KycUtils.checkNullVal(fnaDets.getAdviserStaffByMgrId()) ? "" : fnaDets.getAdviserStaffByMgrId().getAdvstfInitials();
			 
		 }
		 
		 kyclog.info("sendMailToManagerNonNtuc : strMgrEmailId--------->"+strMgrEmailId + ",strMgrAdvStfId--------->"+strMgrAdvStfId );
		 
		// System.out.println("sendMailToManagerNonNtuc : strMgrEmailId--------->"+strMgrEmailId + ",strMgrAdvName--------->"+strMgrAdvName + strMgrAdvStfId);
		 
		 //poovathi add proxy manager  emailcc configuration on 18-08-2021
		      List lstPrxyMgr = new ArrayList();
		      AdviserService advser = new AdviserService();
			    lstPrxyMgr =  advser.getPrxyAdviserStaffDetails(strMgrAdvStfId);
//			    System.out.println("lstPrxyMgr------------------>"+lstPrxyMgr.size());
			if(lstPrxyMgr.size()>0){
				Iterator advite = lstPrxyMgr.iterator();
			   	while(advite.hasNext()){
					Object[] emailId = (Object[])advite.next();				
					prxyMgrDetsEmailId = !KycUtils.nullObj(emailId[2])? emailId[2].toString():"";
				}
			 }
		 
		 
        		 String[] strEmailToArr = {strMgrEmailId};
        		 String[] strEmailToCCArr = new String[2];
        		 
		         strEmailToCCArr[0] = resource.getString("appln.mgr.email.cc");
			
			 if(!KycUtils.nullOrBlank(prxyMgrDetsEmailId)){
			    strEmailToCCArr[1] = ";"+prxyMgrDetsEmailId;
		         }else{
			    strEmailToCCArr[1] = ";";
		         }
		 
		      //End of proxy manager  emailcc configuration on 18-08-2021
		 
		        String mgrflg= KycUtils.getCommonSessValue(request,KycConst.LOGGED_USER_MGRFLAG);
			String distid = KycUtils.getCommonSessValue(request,KycConst.LOGGED_DIST_ID);
			String loggeduserid = KycUtils.getCommonSessValue(request,KycConst.LOGGED_USER_ID);
			String custname = (String) session.getAttribute(KycConst.CURRENT_CUSTNAME);//KycUtils.getCommonSessValue(request,"CUST_NAME");
			String logadvid = KycUtils.getCommonSessValue(request,KycConst.LOGGED_USER_ADVSTFID);
		 

//			String url=resource.getString("kyc.link.approve")+"KycApproveLoginAction.action?";
			String url=resource.getString("kyc.link.approve")+"approval/manager?";
			String urlParam=logadvid+"&"+mgrflg+"&"+distid+"&"+strFnaId+"&m";
//			String encodedParam=KycUtils.encode(urlParam);
			
			byte[] urlenc = urlParam.getBytes("UTF-8");
			String encodedurlparam = DatatypeConverter.printBase64Binary(urlenc);
			
			String encodedParam = " <a href='"+url+""+encodedurlparam+"'>KYC-APPROVAL</a>";

		 
			String strMailContent = "";
		 String strMailSubj =  resource.getString("appln.nonntuc.advtomgr.subject");
		 strMailSubj = strMailSubj.replace("~ADVISER_NAME~", strAdvName);
		 strMailSubj = strMailSubj.replace("~CLIENT_NAME~", custname);
		 strMailSubj = strMailSubj.replace("~FNA_ID~", strFnaId);
		 
		 
		 strMailContent = resource.getString("appln.nonntuc.advtomgr.content").trim();
//		 strMailContent = strMailContent+ resource.getString("appln.reject.actionrequired");
		 strMailContent = strMailContent + resource.getString("appln.nonntuc.advtomgr.Content.Sart");
			
		 strMailContent = strMailContent + resource.getString("appln.nonntuc.advtomgr.Content.advisername").replace("~ADVISER_NAME~", strAdvName);
		 strMailContent = strMailContent + resource.getString("appln.nonntuc.advtomgr.Content.clientname").replace("~CLIENT_NAME~", custname);	 
		 strMailContent = strMailContent + resource.getString("appln.nonntuc.advtomgr.Content.fnaid").replace("~FNA_ID~", strFnaId);
		 strMailContent = strMailContent + resource.getString("appln.nonntuc.advtomgr.Content.End");
		 strMailContent = strMailContent + resource.getString("appln.nonntuc.advtomgr.Content.message").replace("~APPROVAL_LINK~", encodedParam);	
		 strMailContent = strMailContent + resource.getString("appln.nonntuc.advtomgr.Content.appendix");
		 strMailContent = strMailContent + resource.getString("donotreplyemail");
			 
		 
		 kyclog.info("sendMailToManagerNonNtuc : sendEmail ---->"+strMgrEmailId+","+strMailContent+","+strMailSubj);

		try{
			
			

//			mailReport=KycMail.sendMailWithAttach(strEmailToArr, strEmailToCCArr, strMailContent,strMailSubj,strMailAttach,strDispAttachName);
			mailReport=KycMail.sendMail("",strEmailToArr, strEmailToCCArr, strMailContent,strMailSubj);
			
			kyclog.info("mail status upon non-ntuc manager ----------->"+mailReport);

			kyclog.info("logged user advstf id ----------->"+logadvid);
			if(mailReport.equalsIgnoreCase("SUCCESS")){
				
				fnaServ.kycSentStsUpdate(dbDTO, strFnaId, strCustId,"Y",logadvid);
				
				sendMailJsonObj.put("SEND_MAIL_SUCCESS", "Mail has been Sent Successfully");
				
				sendMailJsonObj.put("MGR_EMAIL_SENT_FLG","Yes");
				
				
			}else{
				sendMailJsonObj.put("SEND_MAIL_FAILURE", "Error in Sending the Mail. Contact Administrator");
			}
			
			
			sendEmail.put(sendMailJsonObj);

		}catch(Exception ex){
			ex.printStackTrace();
		}

		return sendEmail;

	}
	
	//poovathi add on 30-06-2021
	
	public JSONArray sendStatusMailToAdviser(HttpServletRequest request) throws UnsupportedEncodingException{

		 JSONArray sendEmail = new JSONArray();
		 JSONObject sendMailJsonObj = new JSONObject();
		 
		ApplicationContext ctx = ApplicationContextUtils
				.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN); 
		HttpSession session = request.getSession(false);
		 
		 String strFnaId = request.getParameter("strFNAId") != null ? request.getParameter("strFNAId") : null;
		 String strCustName = request.getParameter("strCustName") != null ? request.getParameter("strCustName") : null;
		 String strStsMsg = request.getParameter("StrStsMsg") != null ? request.getParameter("StrStsMsg") : null;
		 String strPerson = request.getParameter("StrPerson") != null ? request.getParameter("StrPerson") : null; 
		 String StrAdvname = request.getParameter("StrAdvname") != null ? request.getParameter("StrAdvname") : null; 
		 
		 
		  //System.out.println("fnaid----->" + strFnaId );
		  //System.out.println("strCustName----->" + strCustName );
		  //System.out.println("strStsMsg----->" + strStsMsg );
		  //System.out.println("strPerson----->" + strPerson );
		  //System.out.println("StrAdvname----->" + StrAdvname );
		
		  String strMailStatusContent = "";
		  
		         //email subject
	                 String strMailStatusSubj =  resource.getString("appln.status.subject");
	                 strMailStatusSubj = strMailStatusSubj.replace("~ADVISER_NAME~", StrAdvname);
	                 strMailStatusSubj = strMailStatusSubj.replace("~CLIENT_NAME~", strCustName);
	                 strMailStatusSubj = strMailStatusSubj.replace("~FNA_ID~", strFnaId);
	          
	                 strMailStatusContent = resource.getString("appln.status.content").trim();
                       
			 strMailStatusContent = strMailStatusContent + resource.getString("appln.status.Start") ;
	                 strMailStatusContent = strMailStatusContent + resource.getString("appln.status.clientname").replace("~CLIENT_NAME~", strCustName);
	                 strMailStatusContent = strMailStatusContent + resource.getString("appln.status.fnaId").replace("~FNA_ID~", strFnaId);
			 strMailStatusContent = strMailStatusContent + resource.getString("appln.status.End");
			 
	                 strMailStatusContent = strMailStatusContent + resource.getString("appln.status.Content.message");
	                 strMailStatusContent = strMailStatusContent + resource.getString("appln.status.comment.person").replace("~STATUS_PERSON~", strPerson);	
	                 
	                 strMailStatusContent = strMailStatusContent + resource.getString("appln.status.comment.message").replace("~STATUS_MESSAGE~", strStsMsg);	 
	                 strMailStatusContent = strMailStatusContent + resource.getString("appln.status.end.message");
	                 strMailStatusContent = strMailStatusContent + resource.getString("donotreplyemail");
	                 
	                 String mailReport = "";
	               
	                 if(strPerson.equalsIgnoreCase("Manager")) {
	                     String[] strEmailToArr  = {resource.getString("appln.mgr.email.cc")};
	                     String[] strEmailToCCArr = { resource.getString("appln.mgr.email.cc")};
	                     mailReport = KycMail.sendMail("",strEmailToArr, strEmailToCCArr, strMailStatusContent,strMailStatusSubj);
	                  }else if(strPerson.equalsIgnoreCase("Admin")) {
	                     String[] strEmailToArr  = {resource.getString("appln.it.admin.To")};
	                     String[] strEmailToCCArr = { resource.getString("appln.it.admin.cc")};
	                     mailReport = KycMail.sendMail("",strEmailToArr, strEmailToCCArr, strMailStatusContent,strMailStatusSubj);
	                 }else {
	                     String[] strEmailToArr  = {resource.getString("attach.ntuc.compliance.toemailid")};
	                     String[] strEmailToCCArr = { resource.getString("attach.ntuc.compliance.ccemailid")};
	                     mailReport = KycMail.sendMail("",strEmailToArr, strEmailToCCArr, strMailStatusContent,strMailStatusSubj);
	                 }
	                 
	                 String loggeduserid =KycUtils.getCommonSessValue(request,KycConst.LOGGED_USER_ID);
	                 //System.out.println("loggeduserid----->" + loggeduserid );
	                 
	                 String PEND_REQ_QRY = "INSERT INTO FNA_PENDING_REQUEST (REQID, FNA_ID, PENREQ_MSG, REQ_BY, CREATED_BY, CREATED_DATE)"
	                                	+ "VALUES (FNA_PENREQ_SEQ.NEXTVAL,'"+strFnaId+"','"+strStsMsg.replace("\'", "\''")+"', '"+strPerson+"', '"+loggeduserid+"',"
	                                	+ "'"+new SimpleDateFormat("dd/MM/yyyy").format(new Date())+"')";
	                 
	                       dbDTO.updateDbByNativeSQLQuery(PEND_REQ_QRY);
	                  
	                 if(mailReport.equalsIgnoreCase("SUCCESS")){
				sendMailJsonObj.put("SEND_MAIL_SUCCESS", "Mail has been Sent Successfully");
				
				String FldToUpdate = "";
				    if ( strPerson.equalsIgnoreCase("Manager") ) {
					FldToUpdate = "MGR_APPROVE_STATUS";
				    } else if( strPerson.equalsIgnoreCase("Admin") ) {
				        FldToUpdate = "ADMIN_APPROVE_STATUS";
				    } else {
					FldToUpdate = "COMP_APPROVE_STATUS";
				    }
				    
				    String UPDATE_QRY = "update FNA_DETAILS set "+FldToUpdate+" = 'PENDINGREQ' WHERE FNA_ID='"+strFnaId+"'";
				    dbDTO.updateDbByNativeSQLQuery(UPDATE_QRY);
			 }else{
				sendMailJsonObj.put("SEND_MAIL_FAILURE", "Error in Sending the Mail. Contact Administrator");
			      }
	                 
//	                 System.out.println("sendMailJsonObj --------->"+ sendMailJsonObj);
	                 sendEmail.put(sendMailJsonObj);
	                 
		return sendEmail;

	}

	
	

}