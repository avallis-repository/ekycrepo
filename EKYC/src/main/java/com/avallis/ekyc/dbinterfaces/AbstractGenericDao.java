package com.avallis.ekyc.dbinterfaces;


import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import org.springframework.orm.hibernate5.support.HibernateDaoSupport;



public abstract class AbstractGenericDao<T> extends HibernateDaoSupport {

	

	

	private Class<T> entityClass;
	private String className;

	public AbstractGenericDao() {
		
//		  this.entityClass = (Class<T>) ((ParameterizedType)
//		  this.getClass().getGenericSuperclass()) .getActualTypeArguments()[0];
//		  className = this.entityClass.getSimpleName();
//		  System.out.println("Class name---"+className);
		 
	}

	public void saveMethod(T entity) throws Exception {
		Session session = getHibernateTemplate().getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		try {
		
			//session = session.getSession();
			session.save(entity);
			tx=session.getTransaction();
			
			tx.commit();
		}
		catch(Exception ex) {
//			if(tx!=null) {
//				tx.rollback();
//			}
			throw ex;
		}
		finally {
		session.close();}
	}

	public List<T> getAllData(String className) {
		Session session = getHibernateTemplate().getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		List<T> data = session.createQuery("from " +className ).list();
		tx.commit();
		session.close();
		return data;
	}

	public List<T> getAllDataById(String colName, String id,String className) {
		Session session = getHibernateTemplate().getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		SQLQuery query = session.createSQLQuery("from " + className + " where " + colName + "= :id");
		query.setParameter("id", id);
		
		List<T> data = query.list();
		tx.commit();
		session.close();
		return data;
	}

	public void update(T entity) {
		Session session = getHibernateTemplate().getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		session.saveOrUpdate(entity);
		tx.commit();
		session.close();

	}

	public void delete(String id, String colName,String className) {
		Session session = getHibernateTemplate().getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		SQLQuery  query = session.createSQLQuery("DELETE FROM " + className + " WHERE " + colName + "=:id");
		query.setParameter("id", id);
		query.executeUpdate();
		tx.commit();
		session.close();

	}

	public T getDataById(String id, String colName,String className) {
		Session session = getHibernateTemplate().getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		SQLQuery query = session.createSQLQuery("from " + className + " where " + colName + "= :id");
		query.setParameter("id", id);
		T fnaData = (T) query.uniqueResult();
		session.close();
		tx.commit();
		return fnaData;
	}
	
	public T getDataByCol(String val1,String colName1,String val2,String colName2,String className) {
		Session session = getHibernateTemplate().getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		SQLQuery query = session.createSQLQuery("from " + className + " where " + colName1 + "= :col1 and "+ colName2 + "= :col2");
		query.setParameter("col1", val1);
		query.setParameter("col2", val2);
		T fnaData = (T) query.uniqueResult();
		session.close();
		tx.commit();
		return fnaData;
	}
}
