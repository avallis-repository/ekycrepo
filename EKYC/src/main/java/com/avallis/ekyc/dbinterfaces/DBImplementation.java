package com.avallis.ekyc.dbinterfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import org.springframework.web.context.WebApplicationContext;

import com.avallis.ekyc.dto.FnaDetails;
import com.avallis.ekyc.dto.MasterProduct;
import com.avallis.ekyc.utils.ApplicationContextUtils;
import com.avallis.ekyc.utils.KycUtils;


@SuppressWarnings("unchecked")

public class DBImplementation extends HibernateDaoSupport implements DBIntf {

	static Logger kyclog = Logger.getLogger(DBImplementation.class.getName());

	/** Insert Method */
	public void insertDB(Object insObj) throws DataAccessException {
		
		kyclog.info("Inside Insert DTO Class Obj " + insObj);
		
		Session session = getHibernateTemplate().getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		try {
			session.save(insObj);
			//session.getTransaction().commit();
			tx.commit();
			//getHibernateTemplate().save(insObj);
		} catch (Exception ee) {
			kyclog.error("error in insertDB-->"+ ee );
			tx.rollback();
		}finally{
			session.clear();
			session.flush();
			session.close();
		}
		
		 
	}
 

	public List<?> searchListByQuery(String srchQuery) {
		
		SessionFactory sessionFactory = getHibernateTemplate().getSessionFactory();		
		Session session = sessionFactory.openSession();
		List retList = new ArrayList();
		
		try{
			
			kyclog.info("Inside No Parameter Srch DTO Class Obj " + srchQuery);
			retList=(List)session.createQuery(srchQuery).list();
		
		}catch (Exception ee) {
			kyclog.error("error in searchListByQuery-->"+ ee );
		}finally{
			session.clear();
			session.flush();
			session.close();
		}
		
		return retList;
	}
	
	
	public List searchListByJoin(String srchQuery,Map<String, Class> entityObj) {
		
		kyclog.info(" Inside Native searchListByJoin query.getQueryString---->" + srchQuery);
		
		SessionFactory sessionFactory = getHibernateTemplate().getSessionFactory();		
		Session session = sessionFactory.openSession();
		
		List retList = new ArrayList();
		
		try{
			
			SQLQuery query = session.createSQLQuery(srchQuery);
			
			for (Map.Entry entry : entityObj.entrySet()) {				
				String key = (String) entry.getKey();
				query.addEntity(key,entityObj.get(key));
			}
			retList=(List)query.list();
			
		
		}catch (Exception ee) {
			kyclog.error("error in searchListByJoin-->"+ ee );
		}finally{
			session.clear();
			session.flush();
			session.close();
		}
		
		return retList;
	}
	
	public List searchByNativeSQLQuery(String srchQuery) {
		kyclog.info(" Inside Native SQL Query Search " + srchQuery);
		List clntSrchLst = new ArrayList();
		Session session = null;
		try {
			session = getHibernateTemplate().getSessionFactory()
					.openSession();
			clntSrchLst = session.createSQLQuery(srchQuery).list();
		} catch (Exception ee) {
			kyclog.error("error in searchByNativeSQLQuery-->"+ ee );
		}finally{
			session.clear();
			session.flush();
			session.close();
		}
		return clntSrchLst;
	}
	public FnaDetails getUniqueData(String srchQuery) {
		kyclog.info(" Inside Native SQL Query Search " + srchQuery);
		FnaDetails obj = new FnaDetails();
		Session session = null;
		try {
			session = getHibernateTemplate().getSessionFactory()
					.openSession();
			obj = (FnaDetails) session.createSQLQuery(srchQuery).uniqueResult();
		} catch (Exception ee) {
			kyclog.error("error in getUniqueData-->"+ ee );
		}finally{
			session.clear();
			session.flush();
			session.close();
		}
		return obj;
	}
	
	
	
	public List srchCollNativeSQLQuery(String srchQuery,Vector vectObj) {
		
		kyclog.info(" searchByNativeSQLQuery " +vectObj +","+srchQuery);
		
		List srchList = new ArrayList();
		Session session = null;
		try {
			session = getHibernateTemplate().getSessionFactory()
					.openSession();
			srchList = session.createSQLQuery(srchQuery).setParameterList("paramA",vectObj).list();
		} catch (Exception ee) {
			kyclog.error("error in srchCollNativeSQLQuery-->"+ ee );
		}finally {
			session.clear();
			session.flush();
			session.close();
		}
		return srchList;
	}//End srchCollNativeSQLQuery
	 
	
	
	public String fetchMaxSeqVal(String seqObj) throws DataAccessException {
		
		
		String retKeyId = "";
		List srchLst = new ArrayList();
		Session session = getHibernateTemplate().getSessionFactory().openSession();
		try {			
			Query query = session.createSQLQuery(seqObj.toString());
			srchLst = query.list();
			retKeyId = String.valueOf(srchLst.get(0));
		} catch (Exception e) {
			kyclog.error("Error in fetchMaxSeqVal Function",e);
			throw new HibernateException( "Error!! Unable to fetch your record(S) !!");
		}finally{
			session.clear();
			session.flush();
			session.close();			
		}
		return retKeyId.equals("null")?"1":retKeyId;
		//return retKeyId;
	}

	public void insertTableValues(List insObj, Object daoClsObj)
			throws DataAccessException {

		kyclog.info("Inside insertTableValues " + insObj + "," + daoClsObj);
		Session session = getHibernateTemplate().getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		
		try {		
			
			int insSize = insObj.size();
			
			if (insSize > 0) {
				int i=0;
				Iterator itIns = insObj.iterator();
				while (itIns.hasNext()) {
					daoClsObj = itIns.next();
					session.save(daoClsObj);
				}
			}
			tx.commit();
		} catch (DataAccessException he) {			
			kyclog.error("Error in insertTableValues Function",he);
//			he.printStackTrace();
			if (tx != null)
				tx.rollback();
			
			throw new HibernateException("Insertion / Updation Failure.");
		} finally {
		
			session.clear();
			session.flush();
			session.close();
		}
	}
	
	/** Method Used to Update Table Row Values */
	public void updateTableValues(List updObj, Object daoClsObj)
			throws DataAccessException {
		kyclog.info("INSIDE UPDATE DTO" + updObj + "," + daoClsObj);
		Session session = getHibernateTemplate().getSessionFactory()
				.openSession();
			
		Transaction tx = session.beginTransaction();
		try {
			int updSize = updObj.size();
			if (updSize > 0) {
				
				Iterator itUpd = updObj.iterator();
				while (itUpd.hasNext()) {
					daoClsObj = itUpd.next();
					session.update(daoClsObj);			
					
				}
			}
			tx.commit();
		} catch (Exception he) {
			kyclog.error("Error in updateTableValues Function",he);
			if (tx != null)
				tx.rollback();
			throw new HibernateException("Insertion / Updation Failure.");
		} finally {
			session.clear();
			session.flush();
			session.close();
//			kyclog.info("Session Closed");
		}
	}
	
	/** Method Used to Update Table Row Values */
	public void deleteTableValues(List delObj, Object daoClsObj)
			throws DataAccessException {
		kyclog.info("INSIDE DELETE DTO" + delObj + "," + daoClsObj);
		Session session = getHibernateTemplate().getSessionFactory()
				.openSession();
		Transaction tx = session.beginTransaction();
		try {
			int delSize = delObj.size();
			if (delSize > 0) {
				Iterator itDel = delObj.iterator();
				while (itDel.hasNext()) {
					daoClsObj = itDel.next();
					session.delete(daoClsObj);
				}
			}
			tx.commit();
		} catch (Exception he) {
			kyclog.info("Error in deleteTableValues Function"+he.getStackTrace());
			kyclog.info("---------> TABLE CRUD ", he);
			if (tx != null)
				tx.rollback();
			throw new HibernateException("Deletion  Failure.");
		} finally {
			
			session.clear();
			session.flush();
			session.close();			
			kyclog.info("Session Closed");
		}
	}
	
	public void updateDbByNativeSQLQuery(String updQry){
		Session session=getHibernateTemplate().getSessionFactory().openSession();
		
		Transaction tx = session.beginTransaction();
		
		try {
			Query query = session.createSQLQuery(updQry);
			query.executeUpdate();
			tx.commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (tx != null)
				tx.rollback();
			throw new HibernateException("Insertion / Updation / Deletion Failure.");
		} finally {
			
			session.flush();
			session.clear();
			session.close();
		}
	}// End updateDbByNativeSQLQuery
	
	
	public List searchByNativeSQLQueryParam(String srchQuery,String strObj) {
		
		List srchList = new ArrayList();
		Session session = null;
		try {
			
			session = getHibernateTemplate().getSessionFactory().openSession();
			SQLQuery sqlquery = session.createSQLQuery(srchQuery);			
			srchList = sqlquery.setParameter("param", strObj).list();
			
		} catch (Exception e) {			
			e.printStackTrace();
		} finally {
				session.clear();
				session.flush();
				session.close();
		}
		return srchList;
	}
	
public List srchListByQryParamList(String srchQuery,String param) {
		
		SessionFactory sessionFactory = getHibernateTemplate().getSessionFactory();		
		Session session = sessionFactory.openSession();
		
			
		List retList = new ArrayList();
		
		try{
			
			kyclog.info("Inside srchListByQryParamList DTO Class Obj " + srchQuery); 
			retList=(List)session.createQuery(srchQuery).setParameter("paramA", param).list();
		
		}finally{
			session.clear();
			session.flush();
			session.close();
		}

		return retList;
	}

/** Method Used to create the ID Generator's(Sequence Id's From Dual */
public String createSeqIdVal(String seqObj) {
	kyclog.info(" Inside createSeqIdVal Method ");
	Session session = null;
	String retKeyId = "";
	List srchLst = new ArrayList();
	try {
		session = getHibernateTemplate().getSessionFactory().openSession();
		Query query = session.createSQLQuery(seqObj);
		srchLst = query.list();
		retKeyId = String.valueOf(srchLst.get(0));
	} catch (Exception e) {
		kyclog.info("Error in createSeqIdVal Function"+e.getStackTrace());
	} finally{
		session.clear();
		session.flush();
		session.close();
	}
	return retKeyId;
}

//Method to get Temporary Session
	public Session getTempSession(){
		return getHibernateTemplate().getSessionFactory().openSession();
    }
	
	public Object getSessionState(Class daoObjs, Serializable daoClsId,Session sessObj) {
		kyclog.info(" Inside getSessionState Method ");
		return sessObj.get(daoObjs, daoClsId);
//		return getHibernateTemplate().getSessionFactory().openSession().get(
//				daoObjs, daoClsId);
	}
	
	/** Update Method */
	public void updateDB(Object updObj) throws DataAccessException {
		kyclog.info("Inside Update DTO Class Obj " + updObj);
		Session session = getHibernateTemplate().getSessionFactory().openSession();
		Transaction tx= session.beginTransaction();
		try {
			session.update(updObj);
			tx.commit();
		} catch (DataAccessException ee) {
			ee.printStackTrace();
			kyclog.info("Error in updateDB Function"+ee.getStackTrace()); 
			
			throw new HibernateException("Error!! Unable to update your record(S) !!"+ee);
		}finally{
		session.clear();
			session.flush();
			session.close();
		}
	}/** Delete Method */
	public void deleteDB(Object delObj) throws DataAccessException {
		kyclog.info("Inside Delete DTO Class Obj " + delObj);
		Session session = getHibernateTemplate().getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		
		try {
			session.delete(delObj);
			tx.commit();
		} catch (DataAccessException ee) {
			kyclog.info("Error in deleteDB Function"+ee.getStackTrace());
			//ee.printStackTrace();
			//throw new HibernateException("Error!! Unable to delete your record(S) !!");
			kyclog.info("error-->"+ ee );
		}finally{
			session.clear();
			session.flush();
			session.close();
		}
	}

//added by gokul -16/09/2015	
	public ResultSet downloadAllBFILE(BasicDataSource dm,String srchQuery) {
		
		kyclog.info(" Inside downloadAllBFILE " + srchQuery);
		//Object clntSrchLst = new Object();
//		Session session = null;
		//BFILE bfileobj = null;
		
		ResultSet result = null;
		try {
			
			
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection conn =  DriverManager.getConnection(dm.getUrl(),dm.getUsername(),dm.getPassword());	
			//fplog.info("conn -->"+conn);	
			
			Statement stmt = conn.createStatement();
		//	fplog.info("BFILE srchQuery object---------->"+srchQuery);
			
			result = stmt.executeQuery(srchQuery);
			//fplog.info("BFILE result object---------->"+result);
			
//			while(result.next()){			
//				bfileobj= ((OracleResultSet )result).getBFILE(1);				
//			}
		
			
//			session = getHibernateTemplate().getSessionFactory()
//					.openSession();
//			clntSrchLst = session.createSQLQuery(srchQuery).addScalar("DOCUMENT",Hibernate.BYTE);
			
		} catch (Exception e) {
			kyclog.info("Error in downloadBFILE Function"+e.getStackTrace());
			kyclog.info("Exception at downloadBFILE " + e);
		} finally {
			//session.close();
		}
		return result;
	}   
  
public ResultSet downloadBFILE(String srchQuery,String appMode,WebApplicationContext context) {
		
//		Session session = null;
		ResultSet result = null;
		BasicDataSource dm =null;
		try {
			if(KycUtils.checkNullVal(context)){
//				context = WebApplicationContextUtils.getRequiredWebApplicationContext(ServletActionContext.getServletContext());
				context = (WebApplicationContext) ApplicationContextUtils.getApplicationContext();
			}
			
			//if(appMode.equalsIgnoreCase("LIFE")){
				 dm =(BasicDataSource)context.getBean("dataSource");
//			}else if(appMode.equalsIgnoreCase("GI")){
//				 dm =(BasicDataSource)context.getBean("declareGIDataSource");
//			}
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection conn =  DriverManager.getConnection(dm.getUrl(),dm.getUsername(),dm.getPassword());	
			Statement stmt = conn.createStatement();
			result = stmt.executeQuery(srchQuery);
		} catch (Exception e) {
			e.printStackTrace();
			//System.out.println("Error in downloadBFILE Function"+e.getStackTrace());
			kyclog.error("Exception at downloadBFILE " + e);
		} finally {
			//session.close();
		}
		return result;
	}   



public List<?> searchListByCriteria(String strPlanCode) {
	
	SessionFactory sessionFactory = getHibernateTemplate().getSessionFactory();		
	Session session = sessionFactory.openSession();
	
	List retList = new ArrayList();
	
	try{
		
		Criteria cr = session.createCriteria(MasterProduct.class);
		cr.createAlias("masterProductLine", "masterProductLine");
		cr.createAlias("masterPrincipal", "masterPrincipal");
		// To get records having salary more than 2000
		cr.add(Restrictions.eq("planCode", strPlanCode));
		
 
		 
		
		retList=(List)cr.list();
		
		kyclog.info(" Inside Native searchListByJoin query.retList---->" + retList);

		
//		retList=(List)session.createSQLQuery(srchQuery)
//				.addEntity("fna",FnaDetail.class)
//				.addEntity("ssd",FnaSelfspouseDet.class)					
//				.list();
		
	
	}finally{
		session.clear();
		session.flush();
		session.close();
	}
	
//	return (List) getHibernateTemplate().find(srchQuery);
	return retList;
}

/** Method used for Checking DB Socket Connection */
public Date ChkValidDB(String qryObj) {
	kyclog.info("Inside Valid DB DTO Class Obj " + qryObj);
	Date dateObj = null;
	Session session = getHibernateTemplate().getSessionFactory().openSession();
	try {
		Query query = session.createSQLQuery(qryObj.toString());
		dateObj = (Date) query.uniqueResult();
	} catch (Exception e) {
		kyclog.info("Error in ChkValidDB Function"+e.getStackTrace());
		throw new HibernateException(
				"Error!! Unable to fetch your record(S) !!");
	}finally{
		session.clear();
		session.flush();
		session.close();
	}
	return dateObj;
}

///** Four Parameter Search Method */
//public List searchListByFourParam(String srchQuery, String srchParam1,
//		String srchParam2,String srchParam3,String srchParam4) {
//	kyclog.info("Inside Four Param Srch DTO Class Obj " + srchQuery + ", "
//			+srchParam1+ ", " +srchParam2+ ", " +srchParam3+","+srchParam4);
//	return (List) getHibernateTemplate().find(srchQuery,srchParam1.trim(),
//			srchParam2.trim(),srchParam3.trim(),srchParam4.trim());
//}// End searchListByFourParam
//
///** Multi Parameter Search Method */
//public List searchListByMultipleParam(String srchQuery, String srchParam1,
//		String srchParam2) {
//	kyclog.info("Inside Multi Param Srch DTO Class Obj " + srchQuery + ", "
//			+ srchParam1 + ", " + srchParam2);
//	return (List) getHibernateTemplate().find(srchQuery, srchParam1,
//			srchParam2);
//}
//
public List searchList(String srchQuery, String...srchParam) {
	kyclog.info("Inside Srch DTO Class Obj " + srchQuery + ", "	+ srchParam);
	List srchElm= new ArrayList();
	Session session = null;
	
	session = getHibernateTemplate().getSessionFactory().openSession();
	
	try {
		if(srchParam.length == 1) {
//			srchElm=getHibernateTemplate().find(srchQuery, srchParam[0]);
			srchElm=session.createQuery(srchQuery).setParameter(0, srchParam[0]).list();
		}else if (srchParam.length == 2) {
//			srchElm=getHibernateTemplate().find(srchQuery, srchParam[0],srchParam[1]);
			srchElm=session.createQuery(srchQuery).setParameter(0, srchParam[0]).setParameter(1, srchParam[1]).list();
		}else if (srchParam.length == 3) {
//			srchElm=getHibernateTemplate().find(srchQuery, srchParam[0],srchParam[1],srchParam[2]);
			srchElm=session.createQuery(srchQuery).setParameter(0, srchParam[0]).setParameter(1, srchParam[1]).setParameter(2, srchParam[2]).list();
		}
		
	}catch (DataAccessException ee) {
		kyclog.info("Error in searchList Function"+ee);
	}finally {
		session.clear();
		session.flush();
		session.close();
	}
	
	return srchElm;
}


public List searchNativeQueryList(String srchQuery, String...srchParam) {
	kyclog.info("Inside searchNativeQueryList " + srchQuery + ", "	+ srchParam);
	List srchElm= new ArrayList();
	
	Session session = null;
	
	session = getHibernateTemplate().getSessionFactory().openSession();
	
	try {
		if(srchParam.length == 1) {
			srchElm=session.createSQLQuery(srchQuery).setParameter(0, srchParam[0]).list();
		}else if (srchParam.length == 2) {
			srchElm=session.createSQLQuery(srchQuery).setParameter(0, srchParam[0])
					.setParameter(1, srchParam[1]).list();
		}else if (srchParam.length == 3) {
			srchElm=session.createSQLQuery(srchQuery).setParameter(0, srchParam[0])
					.setParameter(1, srchParam[1])
					.setParameter(2, srchParam[2]).list();
		}
		
		
		
	}catch (DataAccessException ee) {
		kyclog.info("Error in searchList Function"+ee);
	}finally{
		session.clear();
		session.flush();
		session.close();
	}
	return srchElm;
}

public Connection getDBConnection(WebApplicationContext context) {
	BasicDataSource dm = null;
	Connection conn = null;
	
	if(KycUtils.checkNullVal(context)){
		context = (WebApplicationContext) ApplicationContextUtils.getApplicationContext();
	}
	
		 dm =(BasicDataSource)context.getBean("declareDataSource");
	try {
		Class.forName("oracle.jdbc.driver.OracleDriver");
	} catch (ClassNotFoundException e1) {
		kyclog.error("error in getDBConnection-->",e1);
	}
	try {
		conn =  DriverManager.getConnection(dm.getUrl(),dm.getUsername(),dm.getPassword());
	} catch (SQLException e) {
		kyclog.error("error in getDBConnection-->",e);
	}	
	return conn;
}


public void insOrUpdTableValues(List<?> insObj, Object daoClsObj) {
	Session session = getHibernateTemplate().getSessionFactory().openSession();
	Transaction tx = session.beginTransaction();
	
	try {		
		
		int insSize = insObj.size();
		
		if (insSize > 0) {
			int i=0;
			Iterator itIns = insObj.iterator();
			while (itIns.hasNext()) {
				daoClsObj = itIns.next();
				session.saveOrUpdate(daoClsObj);
			}
		}
		tx.commit();
	} catch (DataAccessException he) {			
		kyclog.info(he.getMessage());
		kyclog.info("Error in insOrUpdTableValues Function"+he.getLocalizedMessage());
		he.printStackTrace();
		if (tx != null)
			tx.rollback();
		
		throw new HibernateException("Insertion / Updation Failure.");
	} finally {
	
		session.clear();
		session.flush();
		session.close();
	}

	
}

}// End Class
