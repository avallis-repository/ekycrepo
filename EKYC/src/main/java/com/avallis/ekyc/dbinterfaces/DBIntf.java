package com.avallis.ekyc.dbinterfaces;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.Session;
import org.springframework.web.context.WebApplicationContext;

import com.avallis.ekyc.dto.FnaDetails;

public interface DBIntf {

	public void insertDB(Object insObj);

	public List<?> searchListByQuery(String srchQuery);

	public List<?> searchByNativeSQLQuery(String srchQuery);

	public String fetchMaxSeqVal(String strQry);

	public void insOrUpdTableValues(List<?> insObj, Object daoClsObj);
	
	public void insertTableValues(List<?> insObj, Object daoClsObj);

	public void updateTableValues(List<?> updObj, Object daoClsObj);

	public void deleteTableValues(List<?> delObj, Object daoClsObj);

	public void updateDbByNativeSQLQuery(String updQry);

	@SuppressWarnings("rawtypes")
	
	public List<?> searchListByJoin(String srchQuery,
			Map<String, Class> entityObj);

	public List<?> searchByNativeSQLQueryParam(String srchQuery, String strObj);

	public List<?> srchCollNativeSQLQuery(String srchQuery, Vector<?> param);

	public List<?> srchListByQryParamList(String srchQuery, String param);

	public String createSeqIdVal(String seqObj);

	public Session getTempSession();

	@SuppressWarnings("rawtypes")
	public Object getSessionState(Class daoObjs, Serializable daoClsId,
			Session sessObjt);

	public void updateDB(Object updObj);
	
	public void deleteDB(Object updObj);

	public ResultSet downloadAllBFILE(BasicDataSource dm, String srchQuery);

	public ResultSet downloadBFILE(String srchQuery, String appMode,
			WebApplicationContext context);

	public List<?> searchListByCriteria(String strPlanCode);

	public Date ChkValidDB(String qryObj);

//	public List<?> searchListByFourParam(String srchQuery, String srchParam1,String srchParam2, String srchParam3, String srchParam4);

//	public List<?> searchListByMultipleParam(String srchQuery,	String srchParam1, String srchParam2);

	public List<?> searchList(String srchQuery, String...srchParam);
	
	public List<?> searchNativeQueryList(String srchQuery, String...srchParam);

	public Connection getDBConnection(WebApplicationContext context);

	public FnaDetails getUniqueData(String qry);

}// End Interface
