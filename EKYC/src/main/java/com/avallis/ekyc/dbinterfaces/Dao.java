package com.avallis.ekyc.dbinterfaces;

import java.util.List;



public interface Dao<T> {
	public void save(T fna);

	public void update(T fna);

	public void delete(String id);

	public List<T> getAllData();

	//public FnaDetails getDataById(String id);
}
