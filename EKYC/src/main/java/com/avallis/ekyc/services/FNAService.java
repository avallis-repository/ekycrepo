package com.avallis.ekyc.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import oracle.jdbc.OracleResultSet;
import oracle.sql.BFILE;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.avallis.ekyc.db.CustomerAttachDB;
import com.avallis.ekyc.db.FNADb;
import com.avallis.ekyc.db.FpmsDB;
import com.avallis.ekyc.db.KycDb;
import com.avallis.ekyc.dbinterfaces.DBIntf;
import com.avallis.ekyc.dto.AdviserStaff;
import com.avallis.ekyc.dto.CustomerAttachments;
import com.avallis.ekyc.dto.CustomerDetails;
import com.avallis.ekyc.dto.FnaDependantDets;
import com.avallis.ekyc.dto.FnaDetails;
import com.avallis.ekyc.dto.FnaExpenditureDets;
import com.avallis.ekyc.dto.FnaFatcaTaxdet;
import com.avallis.ekyc.dto.FnaOthPersBenfDets;
import com.avallis.ekyc.dto.FnaOthPersPep;
import com.avallis.ekyc.dto.FnaOthPersTpp;
import com.avallis.ekyc.dto.FnaOtherpersonDets;
import com.avallis.ekyc.dto.FnaPendingRequest;
import com.avallis.ekyc.dto.FnaRecomPrdtplanDet;
import com.avallis.ekyc.dto.FnaSelfspouseDets;
import com.avallis.ekyc.dto.FnaSwtchrepPlanDet;
import com.avallis.ekyc.dto.MasterAttachCateg;
import com.avallis.ekyc.dto.MasterProduct;
import com.avallis.ekyc.utils.ApplicationContextUtils;
import com.avallis.ekyc.utils.EKYCQuery;
import com.avallis.ekyc.utils.KycConst;
import com.avallis.ekyc.utils.KycUtils;

public class FNAService {

	public String insertFNADetails(HttpServletRequest request) {

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);
		FNADb ekycdb = new FNADb();

		HttpSession session = request.getSession(false);
		Map<String, String> sessMap = new HashMap<String, String>();

		sessMap = (Map<String, String>) session.getAttribute(KycConst.LOGGED_USER_INFO);
		String strCustId = (String) session.getAttribute(KycConst.CURRENT_CUSTID);

		String strAdvId = sessMap.get(KycConst.LOGGED_USER_ADVSTFID);
		String strUserId = sessMap.get(KycConst.LOGGED_USER_ID);
		String strDistId = sessMap.get(KycConst.LOGGED_DIST_ID);
		String strDistName = sessMap.get(KycConst.LOGGED_DIST_NAME);
		String strMgrId = sessMap.get(KycConst.LOGGED_USER_MGRADVSTFID);

		FnaDetails fnaDetails = new FnaDetails();

		CustomerDetails customerDetails = new CustomerDetails();
		customerDetails.setCustid(strCustId);
		fnaDetails.setCustomerDetails(customerDetails);
		fnaDetails.setCustId(strCustId);

		AdviserStaff adviserStaffByAdvstfId = new AdviserStaff();
		adviserStaffByAdvstfId.setAdvstfId(strAdvId);
		fnaDetails.setAdviserStaffByAdvstfId(adviserStaffByAdvstfId);

		AdviserStaff adviserStaffByMgrId = new AdviserStaff();
		adviserStaffByMgrId.setAdvstfId(strMgrId);
		fnaDetails.setAdviserStaffByMgrId(adviserStaffByMgrId);

		fnaDetails.setFnaCreatedBy(strUserId);
		fnaDetails.setFnaCreatedDate(new Date());
		fnaDetails.setFnaType("SIMPLIFIED");

		JSONObject jsnDetailsDataObj = new JSONObject();
		String strFNAId = KycConst.STR_NULL_STRING;
		List fnaList = new ArrayList();
		fnaList = findFNADetailsByCustId(strCustId);
		int totalFna = fnaList.size();
		if (totalFna > 0) {

			FnaDetails fna = (FnaDetails) fnaList.get(0);
			strFNAId = fna.getFnaId();
			jsnDetailsDataObj.put("advdecOptions", KycUtils.getObjValue(fna.getAdvdecOptions()).equals("\"\"") ? ""
					: KycUtils.getObjValue(fna.getAdvdecOptions()));
			jsnDetailsDataObj.put("apptypesClient", KycUtils.getObjValue(fna.getApptypesClient()).equals("\"\"") ? ""
					: KycUtils.getObjValue(fna.getApptypesClient()));

			request.setAttribute("FNA_DETAILS_DATA", jsnDetailsDataObj.toString());

		} else {
			strFNAId = ekycdb.insertFNADetails(dbDTO, fnaDetails);
		}

		session.setAttribute("CURRENT_FNAID", strFNAId);

		return strFNAId;
	}

	public List findFNADetailsByFnaId(String strFnaId) {
		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		List fnaList = new ArrayList();
		FNADb ekycdb = new FNADb();

		fnaList = ekycdb.findFNADetailsByFnaId(dbDTO, strFnaId);

		return fnaList;
	}// End


	public List findFNADetailsByCustId(String strCustId) {

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		List fnaList = new ArrayList();
		FNADb ekycdb = new FNADb();

		fnaList = ekycdb.findFNADetailsByCustId(dbDTO, strCustId);

		return fnaList;
	}

	public FnaSelfspouseDets findFNASelfSpsDetailsByFnaId(String strFnaId) {

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();

		FnaSelfspouseDets fnaSSObj = ekycdb.findFNASelfSpsDetailsByFnaId(dbDTO, strFnaId);

		return fnaSSObj;
	}

	public List<FnaRecomPrdtplanDet> getFNAProdRecomPlans(HttpServletRequest request, String strFNAId) {

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();

		List<FnaRecomPrdtplanDet> lstPlans = ekycdb.allFNARecommProdPlan(dbDTO, strFNAId);

		return lstPlans;

	}

	public List<MasterProduct> productsByPrinName(String strPrinName) {

		List<MasterProduct> recomList = new ArrayList<MasterProduct>();

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();

		recomList = ekycdb.productsByPrinName(dbDTO, strPrinName);

		return recomList;
	}

	public FnaDetails findFnaLatestDetlsByFnaId(String strFnaId) {

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();

		FnaDetails fnaObj = ekycdb.findFnaLatestDetlsByFnaId(dbDTO, strFnaId);

		return fnaObj;
	}

	public List getFnaDetsByCustId(String strCustId) {
		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb db = new FNADb();
		return db.getFnaDetsByCustId(dbDTO, strCustId, "");
	}

	public FnaSelfspouseDets findFNASelfSpsDetailsByDataFormId(String strDataFormId) {

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();

		FnaSelfspouseDets fnaObj = ekycdb.findFNASelfSpsDetailsByDataFormId(dbDTO, strDataFormId);

		return fnaObj;
	}

	public List<FnaFatcaTaxdet> findTaxDetsByFnaId(String strFnaId) {

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();
		return ekycdb.findFNATaxDetsByFnaId(dbDTO, strFnaId);

	}

	

	public FnaExpenditureDets findExpendByFnaId(String strFnaId) {

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		List fnaList = new ArrayList();
		FNADb ekycdb = new FNADb();

		FnaExpenditureDets expObj = ekycdb.findExpendByFnaId(dbDTO, strFnaId);

		return expObj;
	}

	public List<FnaDependantDets> findFNADependantByFnaId(String strFnaId) {
		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		List<FnaDependantDets> fnaList = new ArrayList<FnaDependantDets>();
		FNADb ekycdb = new FNADb();

		fnaList = ekycdb.findFNADependantByFnaId(dbDTO, strFnaId);
		return fnaList;
	}

	public List<FnaRecomPrdtplanDet> allFNARecommProdPlan(String strFnaId) {

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();
		return ekycdb.allFNARecommProdPlan(dbDTO, strFnaId);

	}

	public String insertPersonalDetails(FnaDetails fnaDetDTO, FnaSelfspouseDets fnaSelfSps, List fnaDet,
			String strFnaId, HttpServletRequest request) throws IllegalArgumentException, IllegalAccessException {

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);
		FNADb ekycdb = new FNADb();

		HttpSession session = request.getSession(false);
		Map<String, String> sessMap = new HashMap<String, String>();
		String strUserId = sessMap.get(KycConst.LOGGED_USER_ID);



		String strSpouseName = KycConst.STR_NULL_STRING;

		FnaDetails fnaDetls = new FnaDetails();
		fnaDetls.setFnaId(strFnaId);

		FnaOtherpersonDets fnaSSDet = new FnaOtherpersonDets();
		FnaOthPersBenfDets a = (FnaOthPersBenfDets) fnaDet.get(0);
		FnaOthPersPep b = (FnaOthPersPep) fnaDet.get(1);
		FnaOthPersTpp c = (FnaOthPersTpp) fnaDet.get(2);
		List allPers = new ArrayList();

		fnaSSDet.setOthperCateg("BENF");
		fnaSSDet.setOthperId(a.getTxtFldCDBenfId());
		fnaSSDet.setOthperName(a.getTxtFldCDBenfName());
		fnaSSDet.setOthperNric(a.getTxtFldCDBenfNric());
		fnaSSDet.setOthperIncorpno(a.getTxtFldCDBenfIncNo());
		fnaSSDet.setOthperRegaddr(a.getTxtFldCDBenfAddr());
		fnaSSDet.setOthperWork(a.getRadFldCDBenfJob());
		fnaSSDet.setOthperWorkCountry(a.getTxtFldCDBenfJobCont());
		fnaSSDet.setOthperRelation(a.getTxtFldCDBenfRel());
		fnaSSDet.setCreatedBy(strUserId);
		fnaSSDet.setCreatedDate(new Date());
		//poovathi add Posheld field in fnaOtherPersonDetls Table on 18-08-21
		fnaSSDet.setOthperPosheld(a.getTxtFldCDBenfPosheld());
		
		allPers.add(fnaSSDet);

		fnaSSDet = new FnaOtherpersonDets();

		fnaSSDet.setOthperCateg("TPP");
		fnaSSDet.setOthperId(c.getTxtFldCDTppId());
		fnaSSDet.setOthperName(c.getTxtFldCDTppName());
		fnaSSDet.setOthperNric(c.getTxtFldCDTppNric());
		fnaSSDet.setOthperIncorpno(c.getTxtFldCDTppIncNo());
		fnaSSDet.setOthperRegaddr(c.getTxtFldCDTppAddr());
		fnaSSDet.setOthperWork(c.getRadFldCDTppJob());
		fnaSSDet.setOthperWorkCountry(c.getTxtFldCDTppJobCont());
		fnaSSDet.setOthperRelation(c.getTxtFldCDTppRel());
		fnaSSDet.setOthperPaymentmode(c.getRadCDTppPayMode());
		fnaSSDet.setOthperBankname(c.getTxtFldCDTppBankName());
		fnaSSDet.setOthperChqOrderno(c.getTxtFldCDTppChqNo());
		fnaSSDet.setOthperContactno(c.getTxtFldCDTppCcontact());
		fnaSSDet.setCreatedBy(strUserId);
		fnaSSDet.setCreatedDate(new Date());
		//poovathi add Posheld field in fnaOtherPersonDetls Table on 18-08-21
		fnaSSDet.setOthperPosheld(c.getTxtFldCDTppPosheld());
		allPers.add(fnaSSDet);

		fnaSSDet = new FnaOtherpersonDets();
		fnaSSDet.setOthperCateg("PEP");
		fnaSSDet.setOthperId(b.getTxtFldCDPepId());
		fnaSSDet.setOthperName(b.getTxtFldCDPepName());
		fnaSSDet.setOthperNric(b.getTxtFldCDPepNric());
		fnaSSDet.setOthperIncorpno(b.getTxtFldCDPepIncNo());
		fnaSSDet.setOthperRegaddr(b.getTxtFldCDPepAddr());
		fnaSSDet.setOthperWork(b.getRadFldCDPepJob());
		fnaSSDet.setOthperWorkCountry(b.getTxtFldCDPepJobCont());
		fnaSSDet.setOthperRelation(b.getTxtFldCDPepRel());
		fnaSSDet.setCreatedBy(strUserId);
		fnaSSDet.setCreatedDate(new Date());
		//poovathi add Posheld field in fnaOtherPersonDetls Table on 18-08-21
		fnaSSDet.setOthperPosheld(b.getTxtFldCDPepPosheld());
		allPers.add(fnaSSDet);

		String strDataFormId = fnaSelfSps.getDataformId();

		String strSessDataFormId = session.getAttribute("CURRENT_DATAFORM_ID") != null ? (String) session.getAttribute("CURRENT_DATAFORM_ID") : "";
		

		String strFNAIdFlg = (String) session.getAttribute("CREATE_NEW_FNA_FLG");
		String strPrevFNAId = session.getAttribute("PREV_FNAID") != null ? (String) session.getAttribute("PREV_FNAID")
				: "";
		if ("true".equals(strFNAIdFlg) && !KycUtils.nullOrBlank(strPrevFNAId)) {
			strDataFormId = null;
		} 
		if (KycUtils.nullOrBlank(strDataFormId)) {
		      
			FnaDetails fnaTemp = updateFNADetails(request, fnaDetDTO);
			fnaSelfSps.setCreatedBy(strUserId);
			fnaSelfSps.setCreatedDate(new Date());
			strSessDataFormId = ekycdb.insertFnaSelfSpsDet(dbDTO, fnaSelfSps, strFnaId);
			
			ekycdb.insertOtherPersDets(allPers, strFnaId, dbDTO);
			
		} else {

			FnaDetails fnaTemp = updateFNADetails(request, fnaDetDTO);
			fnaSelfSps.setFnaDetails(fnaTemp);

			FnaSelfspouseDets fnaSSCurrObj = findFNASelfSpsDetailsByDataFormId(strSessDataFormId);
			BeanUtils.copyProperties(fnaSelfSps, fnaSSCurrObj, getNullPropertyNames(fnaSelfSps));

			ekycdb.updateSelfSpsDetls(dbDTO, fnaSelfSps);

			ekycdb.insertOtherPersDets(allPers, strFnaId, dbDTO);
			strSessDataFormId = fnaSelfSps.getDataformId();
		}

		session.setAttribute("SESS_DF_SELF_NAME", fnaSelfSps.getDfSelfName());
		session.setAttribute("SESS_DF_SPS_NAME",
				KycUtils.nullOrBlank(fnaSelfSps.getDfSpsName()) ? null : fnaSelfSps.getDfSpsName());
		session.setAttribute("CURRENT_DATAFORM_ID", strSessDataFormId);
		session.setAttribute(KycConst.FNA_SELF_NAME, fnaSelfSps.getDfSelfName());
		session.setAttribute(KycConst.FNA_SPOUSE_NAME,
				KycUtils.nullOrBlank(fnaSelfSps.getDfSpsName()) ? null : fnaSelfSps.getDfSpsName());

		if (KycUtils.nullOrBlank(fnaSelfSps.getDfSpsName())) {
			session.removeAttribute("SESS_DF_SPS_NAME");
		}

		return strSessDataFormId;
	}

	public FnaFatcaTaxdet addTaxDetails(FnaFatcaTaxdet fnaSelfSps, String strFnaId, HttpServletRequest request) {

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);
		FNADb ekycdb = new FNADb();

		HttpSession session = request.getSession(false);
		Map<String, String> sessMap = new HashMap<String, String>();
		String strUserId = sessMap.get(KycConst.LOGGED_USER_ID);
//		String strCustId = (String) session.getAttribute(KycConst.CURRENT_CUSTID);

//		List selfSpsList = new ArrayList();
//		selfSpsList = findFNADetailsByCustId(strCustId);
//		int totalselfSpsList = selfSpsList.size();

		String strDataFormId = fnaSelfSps.getTaxId();

		FnaDetails fnaDetls = new FnaDetails();
		fnaDetls.setFnaId(strFnaId);

//		if(totalselfSpsList > 0){
//			fnaSelfSps.setCreatedBy(strUserId);
//			fnaSelfSps.setCreatedDate(new Date());
//			FnaSelfspouseDets fnaSelfSpsDetls = (FnaSelfspouseDets)selfSpsList.get(0);
//			strDataFormId = fnaSelfSpsDetls.getDataformId();
//			ekycdb.updateSelfSpsDetls(dbDTO, fnaSelfSpsDetls);
//			fnaSelfSpsDetls.setFnaDetails(fnaDetls);
//		}else{

		if (KycUtils.nullOrBlank(strDataFormId)) {

			fnaSelfSps.setCreatedBy(strUserId);
			fnaSelfSps.setCreatedDate(new Date());
			fnaSelfSps = ekycdb.insertFnaTaxDets(dbDTO, fnaSelfSps, strFnaId);
		} else {
			fnaSelfSps.setFnaDetails(fnaDetls);
			fnaSelfSps.setTaxModDate(new Date());
			ekycdb.updateTaxDetls(dbDTO, fnaSelfSps);

		}

//		}

//		session.setAttribute("CURRENT_DATAFORMID", strDataFormId);

		return fnaSelfSps;
	}

	public void deleteTaxDetails(FnaFatcaTaxdet fnaSelfSpsDetls) {
		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);
		FNADb ekycdb = new FNADb();
		ekycdb.deleteTaxDetls(dbDTO, fnaSelfSpsDetls);
		;

	}

	public FnaRecomPrdtplanDet addRecomProdPlanDets(FnaRecomPrdtplanDet planDet, String strFnaId) {

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();
		return ekycdb.addRecomProdPlan(dbDTO, planDet, strFnaId);
	}

	public String insertFnaDepntDet(FnaDependantDets depnDet, String strFnaId) {
		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();
		return ekycdb.insertFnaDepntDet(dbDTO, depnDet, strFnaId);
	}

	public String insertDepnCashAssetLiabDetls(FnaExpenditureDets expnDet, String strFnaId) {
		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();
		return ekycdb.insertDepnCashAssetLiabDetls(dbDTO, expnDet, strFnaId);
	}

	public List findFnaDetlsByCustId(String strCustId) {
		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		List fnaList = new ArrayList();
		FNADb ekycdb = new FNADb();

		fnaList = ekycdb.findFnaDetlsByCustId(dbDTO, strCustId);

		return fnaList;

	}

	public FnaFatcaTaxdet findTaxDetsByTaxId(String strTaxId) {
		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb db = new FNADb();
		return db.findTaxDetsByTaxId(dbDTO, strTaxId);
	}

	public FnaDependantDets addDependantDetails(FnaDependantDets fnaDepntDet, String strFnaId,
			HttpServletRequest request) {

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);
		FNADb ekycdb = new FNADb();

		HttpSession session = request.getSession(false);
		Map<String, String> sessMap = new HashMap<String, String>();
		String strUserId = sessMap.get(KycConst.LOGGED_USER_ID);
		String strCustId = (String) session.getAttribute(KycConst.CURRENT_CUSTID);

//		List selfSpsList = new ArrayList();
//		selfSpsList = findFNADetailsByCustId(strCustId);
//		int totalselfSpsList = selfSpsList.size();

		String id = fnaDepntDet.getDepnId();

		FnaDetails fnaDetls = new FnaDetails();
		fnaDetls.setFnaId(strFnaId);

		if (KycUtils.nullOrBlank(id)) {

			fnaDepntDet.setCreatedBy(strUserId);
			fnaDepntDet.setCreatedDate(new Date());
			fnaDepntDet = ekycdb.insertFnaDependatDets(dbDTO, fnaDepntDet, strFnaId);
		} else {
			fnaDepntDet.setFnaDetails(fnaDetls);
//			fnaDepntDet.setmo(new Date());
			ekycdb.updateDepentDetls(dbDTO, fnaDepntDet);

		}

//		}

		return fnaDepntDet;
	}

	public FnaDependantDets updateDepnDetls(FnaDependantDets fnaDepntDet, String strdepnId, String strFNAId,
			HttpServletRequest request) {

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);
		FNADb ekycdb = new FNADb();

		HttpSession session = request.getSession(false);
		Map<String, String> sessMap = new HashMap<String, String>();
		String strUserId = sessMap.get(KycConst.LOGGED_USER_ID);
		String strCustId = (String) session.getAttribute(KycConst.CURRENT_CUSTID);

		fnaDepntDet.setDepnId(strdepnId);

		FnaDetails fnaDetls = new FnaDetails();
		fnaDetls.setFnaId(strFNAId);

		fnaDepntDet.setFnaDetails(fnaDetls);

		ekycdb.updateDepentDetls(dbDTO, fnaDepntDet);

		return fnaDepntDet;
	}

	// poovathi add on 06-05-2021
	public String insertFnaDetails(String fnaIdFlg, FnaDetails fnaDetls, HttpServletRequest request) {

		HttpSession session = request.getSession(false);
		Map<String, String> sessMap = new HashMap<String, String>();

		sessMap = (Map<String, String>) session.getAttribute(KycConst.LOGGED_USER_INFO);

		String strAdvId = "";// sessMap.get(KycConst.LOGGED_USER_ADVSTFID);
		String strUserId = sessMap.get(KycConst.LOGGED_USER_ID);
		String strDistId = sessMap.get(KycConst.LOGGED_DIST_ID);
		String strDistName = sessMap.get(KycConst.LOGGED_DIST_NAME);
		String strMgrId = sessMap.get(KycConst.LOGGED_USER_MGRADVSTFID);
		String strStfType = sessMap.get(KycConst.LOGGED_USER_STFTYPE);

		String strCustId = (String) session.getAttribute(KycConst.CURRENT_CUSTID);
		
//		System.out.println("strCustId"+strCustId);

		CustomerDetails customerDetails = new CustomerDetails();
		customerDetails.setCustid(strCustId);
		fnaDetls.setCustomerDetails(customerDetails);
		fnaDetls.setCustId(strCustId);

//		if(!strStfType.equalsIgnoreCase("ADVISER")) {

		AdviserService as = new AdviserService();
		CustomerDetsService cs = new CustomerDetsService();

		CustomerDetails custTmp = cs.getClientsByCustId(strCustId);
		strAdvId = custTmp.getAdviserStaffByAgentIdCurrent().getAdvstfId();

		AdviserStaff tmpAdvstf = as.getAdviserByAdvId(strAdvId);
		strMgrId = tmpAdvstf.getManagerId();

//		}

		AdviserStaff adviserStaffByAdvstfId = new AdviserStaff();
		adviserStaffByAdvstfId.setAdvstfId(strAdvId);
		fnaDetls.setAdviserStaffByAdvstfId(adviserStaffByAdvstfId);

		AdviserStaff adviserStaffByMgrId = new AdviserStaff();
		adviserStaffByMgrId.setAdvstfId(strMgrId);
		fnaDetls.setAdviserStaffByMgrId(adviserStaffByMgrId);

		fnaDetls.setFnaCreatedBy(strUserId);
		fnaDetls.setFnaCreatedDate(new Date());
		fnaDetls.setFnaType("SIMPLIFIED");

		String strFNAId = fnaDetls.getFnaId();

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);
		FNADb ekycdb = new FNADb();

		fnaDetls.setFnaCreatedDate(new Date());

		if ("true".equals(fnaIdFlg)) {
			strFNAId = ekycdb.insertFNADetails(dbDTO, fnaDetls);
			//poovathi add on 01-07-2021
			//clear latest fna sign related  details when create new fna
			FnaSignatureService signService=new FnaSignatureService();
	                signService.clearSignByFnaId(strFNAId);	
		} else {

			//System.out.println("fnaaId NOT NULL-------->" + fnaIdFlg);
			String strFnaId = (String) session.getAttribute("CURRENT_FNAID");
			//System.out.println("fnaaId NOT NULL session val-------->" + strFnaId);
			List fnaList = new ArrayList();
			fnaList = findFNADetailsByFnaId(strFnaId);
			int totalFna = fnaList.size();

			if (totalFna > 0) {

				FnaDetails fnaDet = (FnaDetails) fnaList.get(0);
				fnaDet.setAdvdecOptions(fnaDetls.getAdvdecOptions());
				fnaDet.setCcRepexistinvestflg(fnaDetls.getCcRepexistinvestflg());
				fnaDet.setApptypesClient(fnaDetls.getApptypesClient());
				ekycdb.updateFNADetails(dbDTO, fnaDet);

				strFNAId = fnaDet.getFnaId();

			} else {
				strFNAId = ekycdb.insertFNADetails(dbDTO, fnaDetls);
			}

		}
		//System.out.println("FNA_ID---------->" + strFNAId);
		session.setAttribute("CURRENT_FNAID", strFNAId);

		return strFNAId;

	}

	public String updateIntrprtDets(FnaDetails fnaDetls, HttpServletRequest request) {

		HttpSession session = request.getSession(false);
		String strFNAId = (String) session.getAttribute("CURRENT_FNAID");

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);
		FNADb ekycdb = new FNADb();

		fnaDetls.setFnaCreatedDate(new Date());

		FnaDetails fnaObj = findFnaLatestDetlsByFnaId(strFNAId);

		if (!KycUtils.nullObj(fnaObj)) {

			fnaObj.setIntprtName(fnaDetls.getIntprtName());
			fnaObj.setIntprtNric(fnaDetls.getIntprtNric());
			fnaObj.setIntprtContact(fnaDetls.getIntprtContact());
			fnaObj.setIntprtRelat(fnaDetls.getIntprtRelat());
			fnaObj.setCdLanguages(fnaDetls.getCdLanguages());
			fnaObj.setCdLanguageOth(fnaDetls.getCdLanguageOth());

			ekycdb.updateFNADetails(dbDTO, fnaObj);

		}

		return strFNAId;

	}

	public FnaDetails updateTaxDets(FnaDetails fnaDetls, FnaSelfspouseDets fnaSSObj, HttpServletRequest request)
			throws IllegalArgumentException, IllegalAccessException {

		HttpSession session = request.getSession(false);

		String strFNAId = (String) session.getAttribute("CURRENT_FNAID");

		String strFNAIdFlg = (String) session.getAttribute("CREATE_NEW_FNA_FLG");

		String strPrevFNAId = session.getAttribute("PREV_FNAID") != null ? (String) session.getAttribute("PREV_FNAID")
				: "";

		if ("true".equals(strFNAIdFlg) && (!KycUtils.nullOrBlank(strPrevFNAId))) {
			    FNAService serv = new FNAService();
                            List<FnaFatcaTaxdet> fnataxDetlsList = serv.findTaxDetsByFnaId(strPrevFNAId);

				int taxlstSize = fnataxDetlsList.size();
				//System.out.println("taxlstSize=------------->" + taxlstSize);
				for (FnaFatcaTaxdet fnaFatcaTaxdet : fnataxDetlsList) {
					fnaFatcaTaxdet.setTaxId(null);
					addTaxDetails(fnaFatcaTaxdet, strFNAId, request);
				}
               }

		//System.out.println("CURRENT_FNAID tax form ---------->" + strFNAId);
		String strDataFormId = (String) session.getAttribute("CURRENT_DATAFORM_ID");

		//System.out.println("CURRENT_DATAFORM_ID tax form strDataFormId ---------->" + strDataFormId);

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);
		FNADb ekycdb = new FNADb();
		
		fnaDetls.setFnaId(strFNAId);
		//System.out.println("fna id tax screen old or new"+fnaDetls.getFnaId());

		FnaDetails fnaTempDets = updateFNADetails(request, fnaDetls);
		
		FnaSelfspouseDets fnaSSCurrObj = findFNASelfSpsDetailsByDataFormId(strDataFormId);

		BeanUtils.copyProperties(fnaSSObj, fnaSSCurrObj, getNullPropertyNames(fnaSSObj));
		
		//FnaDetails fnaDetl = new FnaDetails();
		//fnaDetl.setFnaId(strFNAId);
		//fnaTempDets.setFnaId(strFNAId);
		fnaSSCurrObj.setDataformId(strDataFormId);
		fnaSSCurrObj.setFnaDetails(fnaTempDets);
		ekycdb.updateSelfSpsDetls(dbDTO, fnaSSCurrObj);

		return fnaTempDets;

	}

	public FnaExpenditureDets updateFinanceWellDets(FnaDetails fnaDetls, FnaExpenditureDets fnaExpObj,
			HttpServletRequest request) throws IllegalArgumentException, IllegalAccessException {

		HttpSession session = request.getSession(false);
		String strFNAId = (String) session.getAttribute("CURRENT_FNAID");

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);
		FNADb ekycdb = new FNADb();

		String strFNAIdFlg = (String) session.getAttribute("CREATE_NEW_FNA_FLG");

		String strPrevFNAId = session.getAttribute("PREV_FNAID") != null ? (String) session.getAttribute("PREV_FNAID")
				: "";

		if ("true".equals(strFNAIdFlg) && (!KycUtils.nullOrBlank(strPrevFNAId))) {
			     FNAService serv = new FNAService();

				List<FnaDependantDets> fnaDependatList = serv.findFNADependantByFnaId(strPrevFNAId);

				int fnaDepenListSize = fnaDependatList.size();
				//System.out.println("fnaDepenListSize=------------->" + fnaDepenListSize);
				for (FnaDependantDets fnaDepndet : fnaDependatList) {
					fnaDepndet.setDepnId(null);
					FnaDetails fna = new FnaDetails();
					fna.setFnaId(strFNAId);
					fnaDepndet.setFnaDetails(fna);
					addDependantDetails(fnaDepndet,strFNAId,request);
				}
                
				fnaExpObj.setExpdId(null);
				fnaDetls.setFnaId(strFNAId);
				//ekycdb.updateSFinanceWell(dbDTO, strFNAId, fnaExpObj);
				//FnaDetails fnaTempDets = updateFNADetails(request, fnaDetls);
				
				 session.removeAttribute("CREATE_NEW_FNA_FLG");
			         session.removeAttribute("PREV_FNAID");
			         session.setAttribute("PREV_FNA_REMOV_FLG", "Y");

		}

		FnaDetails fnaTempDets = updateFNADetails(request, fnaDetls);

		FnaExpenditureDets fnaExpCurrObj = findExpendByFnaId(strFNAId);

		if (!KycUtils.checkNullVal(fnaExpCurrObj)) {
			BeanUtils.copyProperties(fnaExpObj, fnaExpCurrObj, getNullPropertyNames(fnaExpObj));
		} else {
			BeanUtils.copyProperties(fnaExpObj, fnaExpCurrObj, getNullPropertyNames(fnaExpObj));
//			fnaExpCurrObj = fnaExpObj;
		}

//		fnaExpObj.setFnaDetails(fnaTempDets);
		FnaExpenditureDets fnaExpObjRet = ekycdb.updateSFinanceWell(dbDTO, strFNAId, fnaExpCurrObj);

		return fnaExpObjRet;

	}

	public FnaDetails updateFNADetails(HttpServletRequest request, FnaDetails sourceFnaDets)
			throws IllegalArgumentException, IllegalAccessException {

		HttpSession session = request.getSession(false);
		String strFNAId = (String) session.getAttribute("CURRENT_FNAID");

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);
		FNADb ekycdb = new FNADb();

		FnaDetails destFnaDets = findFnaLatestDetlsByFnaId(strFNAId);
		BeanUtils.copyProperties(sourceFnaDets, destFnaDets, getNullPropertyNames(sourceFnaDets));

		ekycdb.updateFNADetails(dbDTO, destFnaDets);

		return destFnaDets;

	}

	public void updateRecomProdPlanDets(FnaRecomPrdtplanDet prod, String strRecomPpId, String strFNAId) {
		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();
		ekycdb.updateRecomProdPlanDets(dbDTO, prod, strRecomPpId, strFNAId);
	}

	public void deleteRiderProdPlan(String strRecomPpId) {
		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();
		ekycdb.deleteRiderProdPlan(dbDTO, strRecomPpId);
	}

	public void deleteBasicProdPlan(String strRecomPpProdName, String strFNAId) {

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();
		ekycdb.deleteBasicProdPlan(dbDTO, strRecomPpProdName, strFNAId);
	}

	public void deletePrincipalByClntName(String strRecomPpName, String strRecomPpPrin, String strFNAId) {

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();
		ekycdb.deletePrincipalByClntName(dbDTO, strRecomPpName, strRecomPpPrin, strFNAId);
	}

	public void deleteProdRecomClntProdPlan(String strRecomPpName, String strFNAId) {

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();
		ekycdb.deleteProdRecomClntProdPlan(dbDTO, strRecomPpName, strFNAId);

	}

	public void deleteBasicPlnsByRecommPpId(String strRecomPpId, String strRecomPpProdName, String strFNAId) {
		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();
		ekycdb.deleteBasicPlnsByRecommPpId(dbDTO, strRecomPpId, strRecomPpProdName, strFNAId);

	}

	// File Upload


	public String uploadAdvApprovalDocs(DBIntf dbDTO, HttpServletRequest req, ArrayList<File> DocLst) {
		FNADb ekycdb = new FNADb();
		return ekycdb.uploadAdvApprovalDocs(dbDTO, req, DocLst);
	}


	



	

	private static String[] getNullPropertyNames(Object source) {
		final BeanWrapper src = new BeanWrapperImpl(source);
		java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

		Set<String> emptyNames = new HashSet<String>();
		for (java.beans.PropertyDescriptor pd : pds) {
			Object srcValue = src.getPropertyValue(pd.getName());
			if (srcValue == null)
				emptyNames.add(pd.getName());
		}

		String[] result = new String[emptyNames.size()];
		return emptyNames.toArray(result);
	}

	

	

	public String downloadDoc(HttpServletRequest request, HttpServletResponse response, String strId)
			throws SQLException {

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		String returnString = KycConst.STR_NULL_STRING;
		ResultSet attachmentList = null;

		FNADb db = new FNADb();
		attachmentList = db.searchAttachmentById(dbDTO, strId);

		String attachFileName = null;
		String attachFileSize = null;
		String attachFileType = null;
		// Blob attachDoc = null;
		BFILE attachDoc = null;
		String filename = KycConst.STR_NULL_STRING;

		while (attachmentList.next()) {
			attachDoc = ((OracleResultSet) attachmentList).getBFILE(1);
			attachFileName = attachmentList.getString(2);
			if (attachFileName.contains(",")) {
				attachFileName = attachFileName.replace(",", "");
			}
			attachFileSize = attachmentList.getString(3);
			attachFileType = attachmentList.getString(4);

//			fplog.info("attachDoc-->"+attachDoc + ",attachDoc name-->"+attachDoc.getName() + ",attachDoc name-->"+attachDoc.getDirAlias() +",attachFileSize-->"+attachFileSize+",attachFileName-->"+attachFileName+",attachFileType-->"+attachFileType );
//			System.out.println("attachDoc-->"+attachDoc + ",attachDoc name-->"+attachDoc.getName() + ",attachDoc name-->"+attachDoc.getDirAlias() +",attachFileSize-->"+attachFileSize+",attachFileName-->"+attachFileName+",attachFileType-->"+attachFileType );

		}

		if (attachDoc.fileExists()) {

			filename = KycUtils.getBFILEDirPath(KycConst.GLBL_MODULE_CLNT) + attachDoc.getName();

			InputStream inputStream = null;
			try {
				ServletOutputStream outStream = response.getOutputStream();
				response.setContentLength(Integer.parseInt(attachFileSize));
				response.setContentType(attachFileType);
				response.setHeader("Content-disposition", "attachment;filename=" + attachFileName);
				response.setHeader("Cache-Control", "max-age=600");
				inputStream = new FileInputStream(filename);
				byte[] buffer = new byte[Integer.parseInt(attachFileSize)];
				int n = 0;
				while ((n = inputStream.read(buffer)) != -1) {
					outStream.write(buffer, 0, n);
				}
				inputStream.close();
				outStream.flush();
			} catch (Exception e) {
				returnString = KycConst.FILE_NOT_FOUND;
			}

			returnString = "SUCCESS";
		} else {
			returnString = KycConst.FILE_NOT_FOUND;
		}

//        if(attachmentList.getStatement().getConnection() != null){attachmentList.getStatement().getConnection().close();}

//      if(attachmentList.getStatement()!=null){attachmentList.getStatement().close();}

//      if(attachmentList != null){attachmentList.close();attachmentList=null;}

		return returnString;

	}

	public List findFNAOthPersDetailsByFnaId(String strFnaId) {

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);
		FNADb db = new FNADb();

		List<FnaOtherpersonDets> fnaOthPerList = db.findFNAOthPersDetailsByFnaId(dbDTO, strFnaId);
		List allPers = new ArrayList();

		if (fnaOthPerList.size() > 0) {
			ListIterator<FnaOtherpersonDets> li = fnaOthPerList.listIterator();

			FnaOthPersBenfDets a = new FnaOthPersBenfDets();
			FnaOthPersPep b = new FnaOthPersPep();
			FnaOthPersTpp c = new FnaOthPersTpp();

			while (li.hasNext()) {
				FnaOtherpersonDets fnaSSDet = li.next();
				if (fnaSSDet.getOthperCateg().equalsIgnoreCase("BENF")) {

					fnaSSDet.getOthperCateg();
					a.setTxtFldCDBenfId(fnaSSDet.getOthperId());
					a.setTxtFldCDBenfName(fnaSSDet.getOthperName());
					a.setTxtFldCDBenfNric(fnaSSDet.getOthperNric());
					a.setTxtFldCDBenfIncNo(fnaSSDet.getOthperIncorpno());
					a.setTxtFldCDBenfAddr(fnaSSDet.getOthperRegaddr());
					a.setRadFldCDBenfJob(fnaSSDet.getOthperWork());
					a.setTxtFldCDBenfJobCont(fnaSSDet.getOthperWorkCountry());
					a.setTxtFldCDBenfRel(fnaSSDet.getOthperRelation());
					//poovathi add on 18-08-2021
					a.setTxtFldCDBenfPosheld(fnaSSDet.getOthperPosheld());
//						fnaSSDet.getCreatedBy();
//						fnaSSDet.getCreatedDate(new Date());

				}

				if (fnaSSDet.getOthperCateg().equalsIgnoreCase("TPP")) {

					c.setTxtFldCDTppId(fnaSSDet.getOthperId());
					c.setTxtFldCDTppName(fnaSSDet.getOthperName());
					c.setTxtFldCDTppNric(fnaSSDet.getOthperNric());
					c.setTxtFldCDTppIncNo(fnaSSDet.getOthperIncorpno());
					c.setTxtFldCDTppAddr(fnaSSDet.getOthperRegaddr());
					c.setRadFldCDTppJob(fnaSSDet.getOthperWork());
					c.setTxtFldCDTppJobCont(fnaSSDet.getOthperWorkCountry());
					c.setTxtFldCDTppRel(fnaSSDet.getOthperRelation());
					c.setRadCDTppPayMode(fnaSSDet.getOthperPaymentmode());
					c.setTxtFldCDTppBankName(fnaSSDet.getOthperBankname());
					c.setTxtFldCDTppChqNo(fnaSSDet.getOthperChqOrderno());
					c.setTxtFldCDTppCcontact(fnaSSDet.getOthperContactno());
					//poovathi add on 18-08-2021
					c.setTxtFldCDTppPosheld(fnaSSDet.getOthperPosheld());
//	fnaSSDet.setCreatedBy(strUserId);
//	fnaSSDet.setCreatedDate(new Date());

				}

				if (fnaSSDet.getOthperCateg().equalsIgnoreCase("PEP")) {

					b.setTxtFldCDPepId(fnaSSDet.getOthperId());
					b.setTxtFldCDPepName(fnaSSDet.getOthperName());
					b.setTxtFldCDPepNric(fnaSSDet.getOthperNric());
					b.setTxtFldCDPepIncNo(fnaSSDet.getOthperIncorpno());
					b.setTxtFldCDPepAddr(fnaSSDet.getOthperRegaddr());
					b.setRadFldCDPepJob(fnaSSDet.getOthperWork());
					b.setTxtFldCDPepJobCont(fnaSSDet.getOthperWorkCountry());
					b.setTxtFldCDPepRel(fnaSSDet.getOthperRelation());
					//poovathi add on 18-08-2021
					b.setTxtFldCDPepPosheld(fnaSSDet.getOthperPosheld());
//	fnaSSDet.setCreatedBy(strUserId);
//	fnaSSDet.setCreatedDate(new Date());

				}
			}

			allPers.add(0, a);
			allPers.add(1, c);
			allPers.add(2, b);
		}
//		 System.out.println("allPers---->"+allPers);

		return allPers;
	}

	public JSONObject objectToJSONObject(Object object) {
		Object json = null;
		JSONObject jsonObject = null;
		try {
			json = new JSONTokener(object.toString()).nextValue();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		if (json instanceof JSONObject) {
			jsonObject = (JSONObject) json;
		}
		return jsonObject;
	}

	// delete dependent details

	public void deleteDepnDetls(String strDepnId) {
		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();
		ekycdb.deleteDepnDetls(dbDTO, strDepnId);
	}

	// edit dependent details

	public List<?> edtDepnData(String strdepnId) {
		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb db = new FNADb();
		return db.edtDepnData(dbDTO, strdepnId);
	}

	// delete custAttachment Details


	

	// get Adviser Dashboard Details
	public List<FnaDetails> getAdvtoManagerFnaList(String strLogUserAdvId) {

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();
		List<FnaDetails> fnaObj = new ArrayList<FnaDetails>();

		fnaObj = ekycdb.getAdvtoManagerFnaList(dbDTO, strLogUserAdvId);

		return fnaObj;
	}

//prod switch and replace page related functions start here
	public List<FnaRecomPrdtplanDet> getFNASwtchrepRecomPlans(HttpServletRequest request, String strFNAId) {
		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();
		List<FnaRecomPrdtplanDet> lstPlans = ekycdb.allFNASwtchrepProdPlan(dbDTO, strFNAId);
		return lstPlans;

	}

	public FnaSwtchrepPlanDet addSwtchrepPlanDets(FnaSwtchrepPlanDet swtchrepplanDet, String strFnaId) {

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();
		return ekycdb.addSwtchrepPlanDets(dbDTO, swtchrepplanDet, strFnaId);
	}

	public void updateFnaSwtchrepPlanDet(FnaSwtchrepPlanDet swtchrepplanDet, String strswrepPpId, String strFNAId) {
		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();
		ekycdb.updateFnaSwtchrepPlanDet(dbDTO, swtchrepplanDet, strswrepPpId, strFNAId);
	}

	// delete process

	public void deleteRiderProdPlanofSwitchReplace(String strswrepPpId) {
		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();
		ekycdb.deleteRiderProdPlanofSwitchReplace(dbDTO, strswrepPpId);
	}

	public void deleteBasicProdPlanofSwitchReplace(String strRecomPpProdName, String strFNAId) {

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();
		ekycdb.deleteBasicProdPlanofSwitchReplace(dbDTO, strRecomPpProdName, strFNAId);
	}

	public void deletePrincipalByClntNameofSwitchReplace(String strRecomPpName, String strRecomPpPrin,
			String strFNAId) {

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();
		ekycdb.deletePrincipalByClntNameofSwitchReplace(dbDTO, strRecomPpName, strRecomPpPrin, strFNAId);
	}

	public void deleteProdRecomClntProdPlanofSwitchReplace(String strRecomPpName, String strFNAId) {

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();
		ekycdb.deleteProdRecomClntProdPlanofSwitchReplace(dbDTO, strRecomPpName, strFNAId);

	}

	public void deleteBasicPlnsBySwrepPpId(String strRecomPpId, String strRecomPpProdName, String strFNAId) {
		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();
		ekycdb.deleteBasicPlnsBySwrepPpId(dbDTO, strRecomPpId, strRecomPpProdName, strFNAId);

	}

	public FnaDetails findFnaDetlsJoinByFnaId(String strFNAId) {

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();

		FnaDetails fnaObj = ekycdb.findFnaDetlsJoinByFnaId(dbDTO, strFNAId);

		return fnaObj;
	}
	
	public void kycSentStsUpdate(DBIntf dbDTO, String strFnaId,String strCustId,String strMgrMailSentFlg,String strlogadvid) {

		FNADb ekycdb = new FNADb();
		ekycdb.kycSentStsUpdate(dbDTO, strFnaId,strCustId,strMgrMailSentFlg,strlogadvid);
	}
	
	public void updateManagerApproveStatus(DBIntf dto,String UPDATE_QRY){
		FNADb db = new FNADb();
		db.updateManagerApproveStatus(dto,UPDATE_QRY);
	}
	
	public void updateAdminApproveStatus(DBIntf dto,String UPDATE_QRY){
		FNADb db = new FNADb();
		db.updateAdminApproveStatus(dto,UPDATE_QRY);
	}
	public void updateCompApproveStatus(DBIntf dto,String UPDATE_QRY){
		FNADb db = new FNADb();
		db.updateCompApproveStatus(dto,UPDATE_QRY);
	}
	
	public List getManagerFnaDetails(DBIntf dto,String strLogUserAdvId,String strFnaId){
		FNADb db = new FNADb();
		return db.getManagerFnaDetails(dto,strLogUserAdvId,strFnaId);
	}
	
	public List getAdminFnaDetails(DBIntf dto,String strFnaId){
		FNADb db = new FNADb();
		return db.getAdminFnaDetails(dto,strFnaId);
	}
	
	public List getComplianceFnaDetails(DBIntf dto,String strFnaId){
		FNADb db = new FNADb();
		return db.getCompFnaDetails(dto,strFnaId);
	}
	
	public List getCompAndAdminFnaDets(DBIntf dto,String strFnaId){
		FNADb db = new FNADb();
		return db.getCompAndAdminFnaDets(dto,strFnaId);
	}
	
	public List loadFnaDetails(DBIntf dbDTO,String strLogUserAdvId){
		FNADb db = new FNADb();
		return db.loadFnaDetails(dbDTO,strLogUserAdvId);
	}
	
	public List loadAdminFnaDetails(DBIntf dbDTO){
		FNADb db = new FNADb();
		return db.loadAdminFnaDetails(dbDTO);
	}
	
	public List loadComplianceFnaDetails(DBIntf dbDTO){
		FNADb db = new FNADb();
		return db.loadComplianceFnaDetails(dbDTO);
	}
	
	public List loadCompAndAdminFnaDets(DBIntf dbDTO){
		FNADb db = new FNADb();
		return db.loadCompAndAdminFnaDets(dbDTO);
	}
	
	public List getAdvtoMngrKycSendList(DBIntf dto,String strLogUserAdvId){
		FNADb db = new FNADb();
		return db.getAdvtoMngrKycSendList(dto,strLogUserAdvId);
	}
	
	/*//poovathi add on 05-07-2021
	public List<> getAllPendReqMsgByFnaId(String strFnaId) {

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();
		return ekycdb.getAllPendReqMsgByFnaId(dbDTO, strFnaId);

	}//End
*/
	/*public List<FnaPendingRequest> getAllPendReqMsgByFnaId(String strFnaId) {

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		FNADb ekycdb = new FNADb();
		return ekycdb.getAllPendReqMsgByFnaId(dbDTO, strFnaId);

	}*/
	
	public List<FnaPendingRequest> getAllPendReqMsgByFnaId(String strFnaId) {
		// List CatList;
		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		List<FnaPendingRequest> fnapendReqList = new ArrayList<FnaPendingRequest>();
		FNADb ekycdb = new FNADb();
		fnapendReqList = ekycdb.getAllPendReqMsgByFnaId(dbDTO, strFnaId);
		return fnapendReqList;
	}
}
