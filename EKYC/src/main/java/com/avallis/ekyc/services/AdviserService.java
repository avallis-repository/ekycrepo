package com.avallis.ekyc.services;

import java.util.List;

import com.avallis.ekyc.db.AdviserDB;
import com.avallis.ekyc.dbinterfaces.DBIntf;
import com.avallis.ekyc.dto.AdviserStaff;
import com.avallis.ekyc.dto.AdvstfCodes;

public class AdviserService {

	public List<AdviserStaff> populateParaplannerDets(String strLoggAdvId, String strDistributor) {
		AdviserDB db = new AdviserDB();
		return db.populateParaplannerDets(strLoggAdvId, strDistributor);
	}

	public List<AdviserStaff> getAdviserStaffDetails(DBIntf dbDTO, String strAdviserId) throws Exception {
		AdviserDB db = new AdviserDB();
		return db.getAdviserStaffDetails(dbDTO, strAdviserId);
	}

	public AdviserStaff getAdviserByAdvId(String strAdvId) {
		AdviserDB db = new AdviserDB();

		return db.getAdviserByAdvId(strAdvId);

	}

	public List getFNAAdvMgrDets(DBIntf dbDTO, String strFNAId) {

		AdviserDB db = new AdviserDB();
		return db.getFNAAdvMgrDets(dbDTO, strFNAId);
	}

	public List<AdviserStaff> getActiveAdvisers(String strloggedAdvStaffId, String strloggedUserStaffType,
			String strloggedUsrDistId) {
		AdviserDB db = new AdviserDB();
		return db.getActiveAdvisers(strloggedAdvStaffId, strloggedUserStaffType, strloggedUsrDistId);
	}

	public List getPrxyAdviserStaffDetails(String strAdviserId) throws Exception {
		AdviserDB db = new AdviserDB();
		return db.getPrxyAdviserStaffDetails(strAdviserId);
	}
	
	public List<AdvstfCodes> getAdvStfCodeDetsByAdvId(String strPrinId,String strDistrId,String strAgentId){
		AdviserDB db = new AdviserDB();
		return db.getAdvStfCodeDetsBasedOnAdvId(strPrinId,strDistrId,strAgentId);
	}

}
