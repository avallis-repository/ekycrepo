package com.avallis.ekyc.db;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.context.ApplicationContext;

import com.avallis.ekyc.dbinterfaces.DBImplementation;
import com.avallis.ekyc.dbinterfaces.DBIntf;
import com.avallis.ekyc.dto.CustomerDetails;
import com.avallis.ekyc.dto.FnaDetails;
import com.avallis.ekyc.utils.ApplicationContextUtils;
import com.avallis.ekyc.utils.KycConst;
import com.avallis.ekyc.utils.KycUtils;

public class CustomerDetsDB {

	static Logger kyclog = Logger.getLogger(CustomerDetsDB.class);
	
	@SuppressWarnings("unchecked")
	public List<CustomerDetails> getAllClientsByAdvId(String strAdvId,String strClntName) {

		ApplicationContext appCtx = ApplicationContextUtils
				.getApplicationContext();
		DBImplementation dbDTO = (DBImplementation) appCtx
				.getBean(KycConst.DB_BEAN);

		List<CustomerDetails> clientList = new ArrayList<CustomerDetails>();
		if(KycUtils.nullOrBlank(strClntName)) {
			clientList = dbDTO.searchList("from  com.avallis.ekyc.dto.CustomerDetails cust where "
					+ "cust.adviserStaffByAgentIdCurrent.advstfId=? order by  cust.custName ",strAdvId);
				
		}else {
			clientList = dbDTO.searchList("from  com.avallis.ekyc.dto.CustomerDetails cust where "
					+ "cust.adviserStaffByAgentIdCurrent.advstfId=? and upper(cust.custName) like '%"+strClntName.toUpperCase()+"%' order by  cust.custName ",strAdvId);
			
		}

		

		return clientList;
	}
	
	
	@SuppressWarnings("unchecked")
	public CustomerDetails getClientsByCustId(String strCustId) {

		ApplicationContext appCtx = ApplicationContextUtils
				.getApplicationContext();
		DBImplementation dbDTO = (DBImplementation) appCtx
				.getBean(KycConst.DB_BEAN);

		CustomerDetails clientList = new CustomerDetails();

		clientList = (CustomerDetails) dbDTO
				.searchList(
						"from  com.avallis.ekyc.dto.CustomerDetails cust left join fetch cust.adviserStaffByAgentIdCurrent curradv"
						+ " where cust.custid=? ",
						strCustId).get(0);
		

		return clientList;
	}
	
	
	public String insertCompDBClient(DBIntf dbDTO,CustomerDetails custCompDAO){

		Object cusObj = KycConst.STR_NULL;
		String maxCustId = KycConst.STR_NULL_STRING;

			String cusSeq = " select (max(substr(CUSTID,length(CUSTID)-15,16))+1) from CUSTOMER_DETAILS";
			int maxCusVal =Integer.parseInt(dbDTO.fetchMaxSeqVal(cusSeq));
			String assignId = String.valueOf(maxCusVal);
			String qry="select lpad(" + assignId + ",16,0) from dual";
			maxCustId = "CUST" + dbDTO.fetchMaxSeqVal(qry);
			custCompDAO.setCustid(maxCustId);


			cusObj = custCompDAO;
			dbDTO.insertDB(cusObj);
		return maxCustId;
	}// End Function {insertCompDBClient}
	
	
	public void updateCompDBClient(DBIntf dbDTO,CustomerDetails custCompDAO){

		Object cusObj = KycConst.STR_NULL;
         cusObj = custCompDAO;
			dbDTO.updateDB(cusObj);
	}
	
	@SuppressWarnings("unchecked")
	public List<CustomerDetails> getClientNricLists(String strClntNric,String strCustId,String strAdvId) {

		ApplicationContext appCtx = ApplicationContextUtils
				.getApplicationContext();
		DBImplementation dbDTO = (DBImplementation) appCtx
				.getBean(KycConst.DB_BEAN);
		
		List<CustomerDetails> clientList = new ArrayList<CustomerDetails>();


		
		if(KycUtils.nullOrBlank(strCustId)) {
			clientList = (List<CustomerDetails>) dbDTO.searchListByQuery("from  com.avallis.ekyc.dto.CustomerDetails cust where cust.adviserStaffByAgentIdCurrent='"+strAdvId+"' AND "
					+ "COALESCE((cust.nric),(cust.custFin),(cust.custPassportNum) ) = '"+strClntNric+"'");//.toUpperCase()
				
		}else {
			clientList = (List<CustomerDetails>) dbDTO.searchListByQuery("from  com.avallis.ekyc.dto.CustomerDetails cust where  cust.adviserStaffByAgentIdCurrent='"+strAdvId+"' AND "
					+ "COALESCE((cust.nric),(cust.custFin),(cust.custPassportNum) ) = '"+strClntNric+"' and cust.custid <> '"+strCustId+"'");//.toUpperCase()
			
		}
		


		return clientList;
	}
	
	
	//get all Nric List of clients
	
		@SuppressWarnings("unchecked")
		public List<CustomerDetails> getClientNricLists(String strClntNric,String strCustId) {

			ApplicationContext appCtx = ApplicationContextUtils
					.getApplicationContext();
			DBImplementation dbDTO = (DBImplementation) appCtx
					.getBean(KycConst.DB_BEAN);
			
			List<CustomerDetails> clientList = new ArrayList<CustomerDetails>();


			
			if(KycUtils.nullOrBlank(strCustId)) {
				clientList = (List<CustomerDetails>) dbDTO.searchListByQuery("from  com.avallis.ekyc.dto.CustomerDetails cust where  "
						+ "COALESCE((cust.nric),(cust.custFin),(cust.custPassportNum) )like '%"+strClntNric.toUpperCase()+"%'");
					
			}else {
				clientList = (List<CustomerDetails>) dbDTO.searchListByQuery("from  com.avallis.ekyc.dto.CustomerDetails cust where  "
						+ "COALESCE((cust.nric),(cust.custFin),(cust.custPassportNum) )like '%"+strClntNric.toUpperCase()+"%' and cust.custid <> '"+strCustId+"'");
				
			}
			


			return clientList;
		}
		
		
		 public List getCustDets(String strCustId,String strformType,String strLoggAdvId) throws HibernateException{
				List empStatusList = new ArrayList();
				try{
//					WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(ServletActionContext.getServletContext());
					ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
					DBIntf dbDTO = (DBIntf) ctx.getBean("DBDAO");

					String CUST_EXIST_DETS = "  SELECT 'C' CUST_TYPE,    CUST.CUSTID,    CUST.CUST_CATEG,   "
							+ " CUST.CUST_NAME,    CUST.CUST_INITIALS,    DECODE(CUST.CUST_CATEG,'COMPANY',CUST.ROC_NO,COALESCE(CUST.NRIC,CUST.CUST_FIN,CUST.CUST_PASSPORT_NUM))NRIC,    CUST.CUST_STATUS,    CUST.AGENT_ID_INITIAL,    "
							+ "CUST.AGENT_ID_CURRENT,    ADV.ADVSTF_NAME,      CUST.COMPANY_NAME,    CUST.OCCPN_TITLE,    "
							+ "CUST.OCCPN_DESC,    CUST.ADDRESS_PREF,    CUST.RES_ADDR1,    CUST.RES_ADDR2,    CUST.RES_ADDR3,    "
							+ "CUST.RES_CITY,    CUST.RES_STATE,    CUST.RES_COUNTRY,    CUST.RES_POSTALCODE,    CUST.RES_FAX,    "
							+ "CUST.RES_PH,    CUST.RES_HAND_PHONE,    CUST.OFF_ADDR1,    CUST.OFF_ADDR2,    CUST.OFF_ADDR3,    "
							+ "CUST.OFF_CITY,    CUST.OFF_STATE,    CUST.OFF_COUNTRY,    CUST.OFF_POSTALCODE,    CUST.OFF_PH,   "
							+ "CUST.OFF_HAND_PHONE,    CUST.OFF_FAX,    CUST.COR_ADDR1,    CUST.COR_ADDR2,    CUST.COR_ADDR3,    "
							+ "CUST.COR_CITY,    CUST.COR_STATE,    CUST.COR_COUNTRY,    CUST.COR_POSTALCODE,    CUST.OTH_PH,    "
							+ "TO_DATE(DECODE(CUST.CUST_CATEG,'COMPANY',CUST.ROC_DATE,CUST.DOB))DOB,    CUST.MARITAL_STATUS,    CUST.SEX,    CUST.EMAIL_ID,    CUST.WEBSITE,       CUST.SMOKER_FLG,   "
							+ "CUST.RACE,    CUST.NATIONALITY,    CUST.TITLE,    CUST.WEIGHT,    CUST.HEIGHT,    CUST.ACADEMIC_TITLE,    "
							+ "CUST.INCOME,    CUST.REMARKS,    FP.CUSTID FP_CUSTID,    FP.NRIC SPNRIC,    FP.CUST_NAME SPCUST_NAME,    "
							+ "FP.CUST_INITIALS SPCUST_INITIALS,   NVL(FP.RELATIONSHIP,'NULL') SPREL,    FP.AGE SPAGE,    FP.REMARKS SPREMARKS,    "
							+ "FP.SEX SPSEX,    FP.AGENT_ID_CURRENT SPAGENT_ID_CURRENT,    FP.DOB SPDOB,    "
							+ "FP.MARITAL_STATUS SPMARITAL_STS,    FP.INCOME SPINCOME,    ADV.EMAIL_ID ADV_EMAILID,    "
							+ "ADV.MANAGER_ID MGR_ID,    MGR.ADVSTF_NAME MGR_NAME,    MGR.EMAIL_ID MGR_EMAILID,CUST.RES_ADDR1,CUST.RES_ADDR2,CUST.RES_ADDR3,CUST.RES_CITY,CUST.RES_STATE,CUST.RES_COUNTRY,CUST.RES_POSTALCODE ,CUST.BUSINESS_NATR ,CUST.CITIZENSHIP"
							+ ", LOG_ADV.ADVSTF_NAME LOGGED_ADVSTF_NAME ,"
							+ " LOG_ADV.EMAIL_ID LOGGED_ADVSTF_EMAIL_ID,"
							+ " LOG_ADV.MANAGER_ID LOGGED_ADV_MGR_ID,  LOG_ADV.MGR_NAME LOGGED_ADV_MGR_NAME,LOG_ADV.MGR_EMAIL_ID "
							+ " FROM CUSTOMER_DETAILS CUST ,    ADVISER_STAFF ADV ,    ADVISER_STAFF MGR,    ( SELECT FP.CUSTID,  FP.NRIC ,  FP.CUST_NAME ,"
							+ "  FP.CUST_INITIALS ,  NVL(FP.RELATIONSHIP,'NULL') RELATIONSHIP,  FP.AGE ,  FP.REMARKS ,  FP.SEX ,  FP.AGENT_ID_CURRENT ,  FP.DOB,"
							+ "  FP.MARITAL_STATUS ,  FP.INCOME FROM CUSTOMER_FAMILYPARTICULARS FP WHERE CUSTID =(:paramA) ) FP"
//							+ "  AND UPPER(NVL(FP.RELATIONSHIP,'NULL')) IN ('SPOUSE','REL022','REL22','NULL'))FP "
							+ " , (SELECT ADV.ADVSTF_NAME , ADV.EMAIL_ID, ADV.MANAGER_ID, MGR.ADVSTF_NAME MGR_NAME,  MGR.EMAIL_ID MGR_EMAIL_ID"
							+ " FROM ADVISER_STAFF ADV,ADVISER_STAFF MGR  WHERE ADV.MANAGER_ID = MGR.ADVSTF_ID AND ADV.ADVSTF_ID='"+strLoggAdvId+"')"
							+ " LOG_ADV "
							+ " WHERE CUST.CUSTID         =(:paramA)  AND CUST.AGENT_ID_CURRENT = ADV.ADVSTF_ID  "
							+ " AND ADV.MANAGER_ID        = MGR.ADVSTF_ID  AND CUST.CUSTID           = FP.CUSTID(+)  ";
//							+ " AND UPPER(NVL(FP.RELATIONSHIP,'NULL')) IN ('SPOUSE','REL022','NULL')  ";
//							+ " UNION "
//							+ " select    'S' CUST_TYPE,    CUST.CUSTID,    CUST.CUST_CATEG,    CUST.CUST_NAME,    CUST.CUST_INITIALS,    "
//							+ "CUST.NRIC,    CUST.CUST_STATUS,    CUST.AGENT_ID_INITIAL,    CUST.AGENT_ID_CURRENT,    ADV.ADVSTF_NAME, "
//							+ " CUST.COMPANY_NAME,    CUST.OCCPN_TITLE,    CUST.OCCPN_DESC,    CUST.ADDRESS_PREF,    CUST.RES_ADDR1,   "
//							+ " CUST.RES_ADDR2,    CUST.RES_ADDR3,    CUST.RES_CITY,   CUST.RES_STATE,    CUST.RES_COUNTRY,   "
//							+ " CUST.RES_POSTALCODE,    CUST.RES_FAX,    CUST.RES_PH,    CUST.RES_HAND_PHONE,    CUST.OFF_ADDR1,   "
//							+ " CUST.OFF_ADDR2,    CUST.OFF_ADDR3,    CUST.OFF_CITY,    CUST.OFF_STATE,    CUST.OFF_COUNTRY,   "
//							+ " CUST.OFF_POSTALCODE,    CUST.OFF_PH,    CUST.OFF_HAND_PHONE,    CUST.OFF_FAX,    CUST.COR_ADDR1,   "
//							+ " CUST.COR_ADDR2,    CUST.COR_ADDR3,    CUST.COR_CITY,    CUST.COR_STATE,    CUST.COR_COUNTRY,   "
//							+ " CUST.COR_POSTALCODE,    CUST.OTH_PH,    CUST.DOB,    CUST.MARITAL_STATUS,    CUST.SEX,   "
//							+ " CUST.EMAIL_ID,   CUST.WEBSITE,  CUST.SMOKER_FLG,    CUST.RACE,    CUST.NATIONALITY,    CUST.TITLE,   "
//							+ " CUST.WEIGHT,    CUST.HEIGHT,    CUST.ACADEMIC_TITLE,    CUST.INCOME,    CUST.REMARKS,   "
//							+ " FP.CUSTID FP_CUSTID,    FP.NRIC SPNRIC,    FP.CUST_NAME SPCUST_NAME,    FP.CUST_INITIALS SPCUST_INITIALS, "
//							+ " FP.RELATIONSHIP SPREL,    FP.AGE SPAGE,    FP.REMARKS SPREMARKS,    FP.SEX SPSEX,   "
//							+ " FP.AGENT_ID_CURRENT SPAGENT_ID_CURRENT,    FP.DOB SPDOB,    FP.MARITAL_STATUS SPMARITAL_STS,   "
//							+ " FP.INCOME SPINCOME,    ADV.EMAIL_ID ADV_EMAILID,    ADV.MANAGER_ID MGR_ID,    MGR.ADVSTF_NAME MGR_NAME,  "
//							+ " MGR.EMAIL_ID MGR_EMAILID  "
//							+ " FROM CUSTOMER_DETAILS CUST ,    ADVISER_STAFF ADV ,    ADVISER_STAFF MGR,    CUSTOMER_FAMILYPARTICULARS FP  "
//							+ " WHERE FP.CUSTID         =(:paramA)  AND UPPER(FP.RELATIONSHIP) IN ('SPOUSE','REL022')  "
//							+ " AND CUST.AGENT_ID_CURRENT = ADV.ADVSTF_ID  AND ADV.MANAGER_ID        = MGR.ADVSTF_ID   "
//							+ " AND (FP.NRIC = CUST.NRIC OR FP.FAM_FIN = CUST.CUST_FIN OR FP.FAM_PASSPORT_NUM = CUST.CUST_PASSPORT_NUM OR FP.EXISTING_CUST_ID = CUST.CUSTID)  ";


					String strWhrCls =  " WHERE CUST.CUSTID='"+strCustId+"' AND CUST.AGENT_ID_CURRENT = ADV.ADVSTF_ID   AND ADV.MANAGER_ID = MGR.ADVSTF_ID "
							+ " AND CUST.CUSTID = FP.CUSTID(+) ) LIST_A WHERE (UPPER(LIST_A.SPREL) IN ('SPOUSE','REL022') OR LIST_A.SPREL IS NULL)";

					 Vector vect = new Vector();
					 vect.add(strCustId);

					empStatusList = dbDTO.srchCollNativeSQLQuery(CUST_EXIST_DETS ,vect);

				}catch(HibernateException e){
					kyclog.info("----------> getCustDets " , e);
				}
				return empStatusList;
			}
		 
		 
		 
		 
		 public List getLatestFpmsCustData(DBIntf dbDTO,String strFnaId){
				List latestFpmsCustDets=new ArrayList();

				 String HQL_LATESTFPMSCUST_QRY = " select {fna.*} ,{cust.*} FROM FNA_DETAILS fna, CUSTOMER_DETAILS cust  "
					 		+ " where fna.CUST_ID = cust.CUSTID (+) and ";
				 Map<String,Class> entityObj =  new LinkedHashMap<String, Class>();

				try{

					entityObj.put("fna", FnaDetails.class);
					entityObj.put("cust", CustomerDetails.class);

//					empStatusList = dbDTO.searchListByJoin(HQL_MASTERS_PRINCIPA_ALL,entityObj);

					latestFpmsCustDets = dbDTO.searchListByJoin(HQL_LATESTFPMSCUST_QRY + " fna.FNA_ID='"+strFnaId+"'",entityObj);
					kyclog.info("latestFpmsCustDets.size------------>"+latestFpmsCustDets.size());
				}catch(Exception ex){
//					ex.printStackTrace();
					kyclog.error("------------>getLatestFpmsCustData "+ex);
				}
				return latestFpmsCustDets;
			}
}
