package com.avallis.ekyc.db;

import java.io.File;
import java.io.FileInputStream;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.avallis.ekyc.dbinterfaces.DBIntf;
import com.avallis.ekyc.dto.CustomerAttachments;
import com.avallis.ekyc.dto.FnaDependantDets;
import com.avallis.ekyc.dto.FnaDetails;
import com.avallis.ekyc.dto.FnaExpenditureDets;
import com.avallis.ekyc.dto.FnaFatcaTaxdet;
import com.avallis.ekyc.dto.FnaOtherpersonDets;
import com.avallis.ekyc.dto.FnaPendingRequest;
import com.avallis.ekyc.dto.FnaRecomPrdtplanDet;
import com.avallis.ekyc.dto.FnaSelfspouseDets;
import com.avallis.ekyc.dto.FnaSwtchrepPlanDet;
import com.avallis.ekyc.dto.MasterAttachCateg;
import com.avallis.ekyc.dto.MasterProduct;
import com.avallis.ekyc.utils.ApplicationContextUtils;
import com.avallis.ekyc.utils.EKYCQuery;
import com.avallis.ekyc.utils.KycConst;
import com.avallis.ekyc.utils.KycUtils;



public class FNADb {
	static Logger kyclog = Logger.getLogger(FNADb.class);

	
	public List getFnaDetsByCustId(DBIntf dbDTO,String strCustId,String strformType) throws HibernateException{
		List empStatusList = new ArrayList();
		try{

			if(KycUtils.checkNullVal(dbDTO)){
//				WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(ServletActionContext.getServletContext());
				ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
				dbDTO = (DBIntf) ctx.getBean("DBDAO");
			}

			String	HQL_FNA_COUNT = " from com.avallis.ekyc.dto.FnaDetails fna left join fetch fna.adviserStaffByMgrId mgr"
						+ " left join fetch fna.adviserStaffByAdvstfId adv " ;


			String strWhrCls = " WHERE fna.custId='"+strCustId+"'";
			// AND fna.fnaType='"+strformType+"'";
			String ostrOrderBy = " ORDER BY fna.fnaId desc";

			empStatusList = dbDTO.searchListByQuery(HQL_FNA_COUNT +strWhrCls + ostrOrderBy);

		}catch(HibernateException e){
			kyclog.error("----------> loadFnaDets " , e);
		}
		return empStatusList;
	}
	
	public String insertFNADetails(DBIntf dbDTO, FnaDetails fnaDetls) {

		int maxVal = Integer.parseInt(dbDTO
				.fetchMaxSeqVal(EKYCQuery.STR_SQL_FNAID_MAX));
		String savId = "FNA" + KycUtils.lpadString(17, String.valueOf(maxVal));
		fnaDetls.setFnaId(savId);

		dbDTO.insertDB(fnaDetls);
		return savId;

	}
	
	public void updateIntroDets(DBIntf dbDTO,String ... params){
		
		Session session = dbDTO.getTempSession();
		Transaction trans = session.beginTransaction();
		
		String hqlUpdate = "update FnaDetails c set c.advdecOptions = :advDecOptValue where c.fnaId = :fnaIdValue";
		// or String hqlUpdate = "update Customer set name = :newName where name = :oldName";
		int updatedEntities = session.createQuery( hqlUpdate )
		        .setString( "advDecOptValue", params[0])
		        .setString( "fnaIdValue", params[1] )
		        .executeUpdate();
		trans.commit();
		session.close();
	}
	public void updateFNADetails(DBIntf dbDTO, FnaDetails fnaDetls) {

		dbDTO.updateDB(fnaDetls);
		// return fnaDetails.getFnaId();

	}

	public String insertFnaSelfSpsDet(DBIntf dbDTO, FnaSelfspouseDets fnaDao,
			String strFnaId) {

		Object fnaObj = null;

		String dataFormId = "";

		// try{
		String maxQry = "select (max(substr(DATAFORM_ID,length(DATAFORM_ID)-16,17))+1) from FNA_SELFSPOUSE_DETS";
		int maxAdvStfVal = Integer.parseInt(dbDTO.fetchMaxSeqVal(maxQry));

		dataFormId = "DAT"+ KycUtils.lpadString(17, String.valueOf(maxAdvStfVal));

		fnaDao.setDataformId(dataFormId);

		FnaDetails fnaDetail = new FnaDetails();
		fnaDetail.setFnaId(strFnaId);

		fnaDao.setFnaDetails(fnaDetail);
		fnaObj = fnaDao;

		dbDTO.insertDB(fnaObj);	

		return dataFormId;
	}
	
	public FnaFatcaTaxdet insertFnaTaxDets(DBIntf dbDTO, FnaFatcaTaxdet fnaDao,
			String strFnaId) {

		Object fnaObj = null;

		String dataFormId = "";

		// try{
		String maxQry = "select (max(substr(TAX_ID,length(TAX_ID)-14,15))+1) from FNA_FATCA_TAXDET";
		int maxAdvStfVal = Integer.parseInt(dbDTO.fetchMaxSeqVal(maxQry));

		dataFormId = "TAX"+ KycUtils.lpadString(17, String.valueOf(maxAdvStfVal));

		fnaDao.setTaxId(dataFormId);

		FnaDetails fnaDetail = new FnaDetails();
		fnaDetail.setFnaId(strFnaId);

		fnaDao.setFnaDetails(fnaDetail);
		fnaObj = fnaDao;

		dbDTO.insertDB(fnaObj);

		return fnaDao;
	}

	public void updateTaxDetls(DBIntf dbDTO, FnaFatcaTaxdet fnaSelfSpsDetls) {
		dbDTO.updateDB(fnaSelfSpsDetls);
		
	}
	
	public void deleteTaxDetls(DBIntf dbDTO, FnaFatcaTaxdet fnaSelfSpsDetls) {
		dbDTO.deleteDB(fnaSelfSpsDetls);
		
	}
	
	public List findFNADetailsByCustId(DBIntf dbDTO, String strCustId) {

		List fnaList = new ArrayList();

		fnaList = dbDTO.searchList(EKYCQuery.STR_HQL_FNADETS_BYCUSTID,strCustId);

		return fnaList;

	}
	
	//poovathi addon 06-05-2021
	public List findFNADetailsByFnaId(DBIntf dbDTO, String strFnaId) {
	    List fnaList = new ArrayList();

		fnaList = dbDTO.searchList(EKYCQuery.STR_HQL_FNADETS_BYFNAID,strFnaId);

		return fnaList;

	}

	public List findFnaDetlsByCustId(DBIntf dbDTO, String strCustId) {

		List fnaList = new ArrayList();

		fnaList = dbDTO.searchList(EKYCQuery.STR_HQL_FNADETS_BYCUSTID,strCustId);

		return fnaList;

	}

	 /**
	  * 
	  * @param dbDTO
	  * @param strFnaId
	  * @return
	  */

	public FnaSelfspouseDets findFNASelfSpsDetailsByFnaId(DBIntf dbDTO, String strFnaId) {
		
		List resList = dbDTO.searchList(EKYCQuery.STR_HQL_FNASELFSPSDETS_BYFNAID, strFnaId);
		
		if(resList.size()>0){
			FnaSelfspouseDets fnaSSObj = (FnaSelfspouseDets)resList.get(0);	
			return fnaSSObj;
		}else{
			return null;
		}

		

		
	}
	
public FnaSelfspouseDets findFNASelfSpsDetailsByDataFormId(DBIntf dbDTO, String strDataFormId) {
		
		List resList = dbDTO.searchList(EKYCQuery.STR_HQL_FNASELFSPSDETS_BYDATAFORMID, strDataFormId);
		
		if(resList.size()>0){
			FnaSelfspouseDets fnaSSObj = (FnaSelfspouseDets)resList.get(0);	
			return fnaSSObj;
		}else{
			return null;
		}

		

		
	}
	public List findFNATaxDetsByFnaId(DBIntf dbDTO, String strFnaId) {

		List fnaList = new ArrayList();

		fnaList = dbDTO.searchList(EKYCQuery.STR_HQL_TAXDETS_BYFNAID, strFnaId);

		return fnaList;
	}

	public FnaFatcaTaxdet findTaxDetsByTaxId(DBIntf dbDTO, String strTaxId) {

		FnaFatcaTaxdet fnaList = new FnaFatcaTaxdet();

		fnaList = (FnaFatcaTaxdet)dbDTO.searchList(EKYCQuery.STR_HQL_TAXDETS_BYTAXID, strTaxId).get(0);

		return fnaList;

	}
	public List findFNADependantByFnaId(DBIntf dbDTO, String strFnaId) {

		List fnaList = new ArrayList();

		fnaList = dbDTO.searchList(EKYCQuery.STR_HQL_DEPENDANT_BYFNAID,
				strFnaId);

		return fnaList;
	}
	public FnaDependantDets findDependantByDepId(DBIntf dbDTO, String strTaxId) {

		FnaDependantDets fnaList = new FnaDependantDets();

		fnaList = (FnaDependantDets)dbDTO.searchList(EKYCQuery.STR_HQL_DEPENDANT_BYDEPID, strTaxId).get(0);

		return fnaList;

	}
	
	
	//poovathi add on 20-05-2021
	
	public FnaFatcaTaxdet findFNATaxDetsByFnaIds(DBIntf dbDTO, String strFnaId) {

		FnaFatcaTaxdet expObj = new FnaFatcaTaxdet();

		List resultList= dbDTO.searchList(EKYCQuery.STR_HQL_TAXDETS_BYFNAID,strFnaId);
		
		if(resultList.size()>0){
			ListIterator<FnaFatcaTaxdet> lite = resultList.listIterator();
			while(lite.hasNext()){
				expObj = lite.next();
			}
		}

		return expObj;

	}
	
	
	
	
	public FnaExpenditureDets findExpendByFnaId(DBIntf dbDTO, String strFnaId) {

		FnaExpenditureDets expObj = new FnaExpenditureDets();

		List resultList= dbDTO.searchList(EKYCQuery.STR_HQL_EXPENDITURE_BYFNAID,strFnaId);
		
		if(resultList.size()>0){
			ListIterator<FnaExpenditureDets> lite = resultList.listIterator();
			while(lite.hasNext()){
				expObj = lite.next();
			}
		}

		return expObj;

	}
	
	public List allFNARecommProdPlan(DBIntf dbDTO, String strFnaId) {

		List recomList = new ArrayList();

		recomList = dbDTO.searchList(EKYCQuery.STR_HQL_RECOM_PLANS, strFnaId);

		return recomList;
	}

	public FnaRecomPrdtplanDet addRecomProdPlan(DBIntf dbDTO, FnaRecomPrdtplanDet planDet,
			String strFnaId) {

		int maxVal = Integer.parseInt(dbDTO
				.fetchMaxSeqVal(EKYCQuery.STR_SQL_RECOM_PP_ID_MAX));
		FnaDetails fd = new FnaDetails();
		fd.setFnaId(strFnaId);
		String savId = "ARP" + KycUtils.lpadString(17, String.valueOf(maxVal));
		planDet.setRecomPpId(savId);
		planDet.setFnaDetails(fd);

		dbDTO.insertDB(planDet);

		return planDet;

	}

	public String insertFnaDepntDet(DBIntf dbDTO, FnaDependantDets depnDet,
			String strFnaId) {
		int maxVal = Integer.parseInt(dbDTO
				.fetchMaxSeqVal(EKYCQuery.STR_SQL_DEPN_ID_MAX));
		FnaDetails fd = new FnaDetails();
		fd.setFnaId(strFnaId);
		String depnId = "DEP" + KycUtils.lpadString(17, String.valueOf(maxVal));
		depnDet.setDepnId(depnId);
		depnDet.setFnaDetails(fd);
		dbDTO.insertDB(depnDet);
		return depnId;

	}

	public String insertDepnCashAssetLiabDetls(DBIntf dbDTO,
			FnaExpenditureDets expnDet, String strFnaId) {
		int maxVal = Integer.parseInt(dbDTO
				.fetchMaxSeqVal(EKYCQuery.STR_SQL_EXPD_ID_MAX));
		FnaDetails fd = new FnaDetails();
		fd.setFnaId(strFnaId);
		String assignId = "EXP"
				+ KycUtils.lpadString(17, String.valueOf(maxVal));
		expnDet.setExpdId(assignId);
		expnDet.setFnaDetails(fd);
		dbDTO.insertDB(expnDet);
		return assignId;

	}

	public FnaDetails findFnaLatestDetlsByFnaId(DBIntf dbDTO, String strFNAId) {

		List fnaList = new ArrayList();
		
		fnaList = dbDTO.searchList(EKYCQuery.STR_HQL_FNADETLS_lATEST_BY_FNAID,strFNAId);
		
		if(fnaList.size()>0) {
			FnaDetails fnaObj = (FnaDetails)fnaList.get(0);	
			return fnaObj;
		}else {
			return null;
		}

		

//		return fnaObj;
	}
	public void updateFnaDetls(DBIntf dbDTO, FnaDetails fnaDets) {
		dbDTO.updateDB(fnaDets);
		
	}
	public void updateSelfSpsDetls(DBIntf dbDTO, FnaSelfspouseDets fnaSelfSpsDetls) {
		dbDTO.updateDB(fnaSelfSpsDetls);
		
	}
	public FnaExpenditureDets updateSFinanceWell(DBIntf dbDTO,String strFNAId, FnaExpenditureDets fnaExpObj) {
		
		String strExpId = fnaExpObj.getExpdId();
		
		if(KycUtils.nullOrBlank(strExpId)){
			
			String maxQry = "select (max(substr(EXPD_ID,length(EXPD_ID)-16,17))+1) from FNA_EXPENDITURE_DETS";
			int maxAdvStfVal = Integer.parseInt(dbDTO.fetchMaxSeqVal(maxQry));

			strExpId = "EXP"+ KycUtils.lpadString(17, String.valueOf(maxAdvStfVal));
			
			fnaExpObj.setExpdId(strExpId);
			fnaExpObj.setCreatedDate(new Date());
			
			FnaDetails fnaDetail = new FnaDetails();
			fnaDetail.setFnaId(strFNAId);

			fnaExpObj.setFnaDetails(fnaDetail);
			
			dbDTO.insertDB(fnaExpObj);
		}else{
			dbDTO.updateDB(fnaExpObj);	
		}
		
		return fnaExpObj;
		
		
	}
	
	public List<MasterProduct>  productsByPrinName(DBIntf dbDTO,String strPrinName){
		
		List<MasterProduct> recomList = new ArrayList<MasterProduct>();
		
		recomList = (List<MasterProduct>) dbDTO.searchList(EKYCQuery.STR_HQL_PROD_BY_PRINNAME,strPrinName.toUpperCase());
		
		return  recomList;
	}

	public void updateRecomProdPlanDets(DBIntf dbDTO, FnaRecomPrdtplanDet prod, String strRecomPpId,String strFNAId) {
		prod.setRecomPpId(strRecomPpId);
		prod.setCreatedDate(new Date());
		FnaDetails fnaDet = new FnaDetails();
		fnaDet.setFnaId(strFNAId);
		prod.setFnaDetails(fnaDet);
		dbDTO.updateDB(prod);
	}

	public void deleteRiderProdPlan(DBIntf dbDTO, String strRecomPpId) {
	   try{
		   String STR_SQL_RECOM_DELETE_RDR_PLANS = " delete from FNA_RECOM_PRDTPLAN_DET WHERE RECOM_PP_ID ='"+strRecomPpId+"'";
			 dbDTO.updateDbByNativeSQLQuery(STR_SQL_RECOM_DELETE_RDR_PLANS);

		}catch(HibernateException e){
		}

		
	}

	public void deleteBasicProdPlan(DBIntf dbDTO, String strRecomPpProdName, String strFNAId){
		
		    try{
			   String STR_SQL_RECOM_DELETE_BSC_PLANS = "DELETE FROM FNA_RECOM_PRDTPLAN_DET  WHERE UPPER(RECOM_PP_PRODNAME) = '"+strRecomPpProdName.toUpperCase()+"' AND (RECOM_PP_BASRID = 'BASIC' OR RECOM_PP_BASRID='B') AND  FNA_ID='"+strFNAId+"'";	
			        dbDTO.updateDbByNativeSQLQuery(STR_SQL_RECOM_DELETE_BSC_PLANS);

			   String STR_SQL_RECOM_DELETE_BCS_RDR_PLANS = "DELETE FROM FNA_RECOM_PRDTPLAN_DET  WHERE RECOM_BASIC_REF = '"+strRecomPpProdName+"' AND (RECOM_PP_BASRID = 'RIDER' OR RECOM_PP_BASRID = 'R') AND  FNA_ID='"+strFNAId+"'";
			     dbDTO.updateDbByNativeSQLQuery(STR_SQL_RECOM_DELETE_BCS_RDR_PLANS);	 
				 
			}catch(HibernateException e){

			}

	}

	public void deletePrincipalByClntName(DBIntf dbDTO, String strRecomPpName, String strRecomPpPrin, String strFNAId) {
		
		try{
			String STR_SQL_RECOM_DELETE_PRIN = "DELETE FROM FNA_RECOM_PRDTPLAN_DET  WHERE   UPPER(RECOM_PP_NAME) = '"+strRecomPpName.toUpperCase()+"' AND  UPPER(RECOM_PP_PRIN) = '"+strRecomPpPrin.toUpperCase()+"'  AND  FNA_ID='"+strFNAId+"'";	
				 dbDTO.updateDbByNativeSQLQuery(STR_SQL_RECOM_DELETE_PRIN);
	 
			}catch(HibernateException e){

			}
	}

	public void deleteProdRecomClntProdPlan(DBIntf dbDTO, String strRecomPpName, String strFNAId) {
	
		try{
			String STR_SQL_RECOM_DELETE_PRIN = "DELETE FROM FNA_RECOM_PRDTPLAN_DET  WHERE UPPER(RECOM_PP_NAME) = '"+strRecomPpName.toUpperCase()+"'  AND  FNA_ID='"+strFNAId+"'";	
				 dbDTO.updateDbByNativeSQLQuery(STR_SQL_RECOM_DELETE_PRIN);
	 
			}catch(HibernateException e){

			}
	}

	public void deleteBasicPlnsByRecommPpId(DBIntf dbDTO, String strRecomPpId, String strRecomPpProdName, String strFNAId) {
		 try{
			   String STR_SQL_RECOM_DELETE_BSC_PLANS = "DELETE FROM FNA_RECOM_PRDTPLAN_DET  WHERE UPPER(RECOM_PP_PRODNAME) = '"+strRecomPpProdName.toUpperCase()+"' AND ( RECOM_PP_BASRID = 'BASIC' OR RECOM_PP_BASRID = 'B' ) AND  FNA_ID='"+strFNAId+"' AND RECOM_PP_ID='"+strRecomPpId+"'";	
			    dbDTO.updateDbByNativeSQLQuery(STR_SQL_RECOM_DELETE_BSC_PLANS);

			   String STR_SQL_RECOM_DELETE_BCS_RDR_PLANS = "DELETE FROM FNA_RECOM_PRDTPLAN_DET  WHERE UPPER(RECOM_BASIC_REF) = '"+strRecomPpProdName.toUpperCase()+"' AND (RECOM_PP_BASRID = 'RIDER' OR RECOM_PP_BASRID = 'R')AND  FNA_ID='"+strFNAId+"'";
			     dbDTO.updateDbByNativeSQLQuery(STR_SQL_RECOM_DELETE_BCS_RDR_PLANS);	 
				 
			}catch(HibernateException e){

			}
	}
	
	
	

	
	public String uploadAdvApprovalDocs(DBIntf dbDTO,HttpServletRequest req,ArrayList<File> DocLst){
		HttpSession sess = req.getSession();

		//String regexchars = resource.getString("attach.bfile.SplCharVal");
		String regexchars = "";
		String retMsg="";

		JSONObject jsnObj=new JSONObject();
		JSONArray sessMapArr=(JSONArray) sess.getAttribute("COMMON_SESS_OBJ");
		jsnObj=(JSONObject) sessMapArr.get(0);

		String strCurrAdvName =jsnObj.getString("CURR_ADVSTF_NAME");
		String strCustName=jsnObj.getString("CUST_NAME");
		String strUsrName =jsnObj.getString("STR_LOGGEDUSER");
		 
		String[] strAtmtAttachMode=req.getParameterValues("txtFldKycApprAdvDocmode") != null ? req.getParameterValues("txtFldKycApprAdvDocmode") :null;
		String[] strChkListArr=req.getParameterValues("selKycApprAdvChkList") != null ? req.getParameterValues("selKycApprAdvChkList") :null;
		String[] strDocTitleArrCode=req.getParameterValues("selKycApprAdvDocTitle") != null ? req.getParameterValues("selKycApprAdvDocTitle") :null;
		String[] strDocTitleTxtArr=req.getParameterValues("txtFldPopUpDocText") != null ? req.getParameterValues("txtFldPopUpDocText") :null;
		String[] strAtmtPageNo=req.getParameterValues("txtFldKycApprAdvDocPgNo") != null ? req.getParameterValues("txtFldKycApprAdvDocPgNo") :null;
		String[] strAtmtRemarks=req.getParameterValues("txtFldKycApprAdvDocRem") != null ? req.getParameterValues("txtFldKycApprAdvDocRem") :null;
	
		String strFnaId = req.getParameter("txtFldFNAID") != null ? req.getParameter("txtFldFNAID"):"";

		for(int count=0;count<strAtmtAttachMode.length;count++){
			

		try {
			if(strAtmtAttachMode[count].equals(KycConst.GLBL_INSERT_MODE)){
			byte[] filedata = null;

			filedata = KycUtils.fpmsReadInputStream(new FileInputStream(DocLst.get(count)));
			String strApplcntName = "";
			strApplcntName = KycUtils.checkNullVal(strApplcntName) ? ""  : strApplcntName.replace("\'", "\''");

			String strAtmtFileName = DocLst.get(count).getName();
			String strAtmtFileType = KycUtils.getFileExtension(DocLst.get(count));
			String strAtmtFileSize = Integer.toString(filedata.length);

			strCurrAdvName = KycUtils.replaceSplChars(regexchars, strCurrAdvName);
			strCustName  = KycUtils.replaceSplChars(regexchars, strCustName);
			strAtmtFileName = KycUtils.formatFileName(strAtmtFileName);

//			String strBFILEName = strCurrAdvName+File.separator+strCustName+File.separator+strAtmtFileName;
//			strBFILEName = strBFILEName.replace("\'", "\''");
			
			String strBFILEName = strFnaId + File.separator+File.separator+ strAtmtFileName;

			File srcFile  = KycUtils.createPhysicalDir(KycConst.GLBL_MODULE_NTUC,strFnaId,"",strAtmtFileName);
			retMsg = KycUtils.createPhysicalFile(DocLst.get(count),srcFile);
			
			String strDataKey = "",
					strdesc = "",
					strfilepath = "",
					strkey= "",
					strfiletype = "",
					encryptfilelen="",
					decryptfilename =strAtmtFileName,
					filetype= strAtmtFileType,
					doctitle = strDocTitleArrCode[count], //strDocTitleTxtArr[count],
					decryptfilelen = strAtmtFileSize,
					//ntuc_orcl_dir = resource.getString("attach.bfile.ntuc.dir");
					ntuc_orcl_dir ="";

			if(KycUtils.nullOrBlank(retMsg)){

				if(strAtmtFileType.length()>40){
	    			strAtmtFileType = "document/text";
	    		}
				
				
				 String ATTCH_INSERT_QRY = "INSERT INTO FNA_NTUC_DOCUMENTS(ND_ID,FNA_ID,NTUC_CASE_ID,"
				       		+ "DOC_DATA_KEY,DOC_DESC,DOC_SFTP_FILEPATH,DOC_DECRYPT_KEY,DOC_FILETYPE,DOC_FILESIZE,"
				       		+ "DOC_AVA_DOCTITILE,DOC_AVA_FILENAME,DOC_AVA_FILETYPE,DOC_AVA_FILESIZE,DOC_AVA_CATEGID,"
				       		+ "DOC_CRTD_BY,DOC_CRTD_DATE,"
				       		+ "DOCUMENT)"
				   	    	+" VALUES(KYC_NTUC_DOCID.nextval,'"+strFnaId+"','',"
				   	    	+ "'"+strDataKey+"'," +"'"+strdesc+"','"+strfilepath+"','"+strkey+"','"+strfiletype+"','"+encryptfilelen+"',"
				   	    	+ "'"+doctitle+"','"+decryptfilename+"','"+filetype+"','"+decryptfilelen+"',"
				   	    	+ "null,'"+strUsrName.toUpperCase()+"',SYSDATE,"+"BFILENAME('"+ntuc_orcl_dir+"','"+strBFILEName+"'))";

	    	

	    		dbDTO.updateDbByNativeSQLQuery(ATTCH_INSERT_QRY);

	    		retMsg="Documents are successfully uploaded";
			}
			}
		} catch (Exception e) {
//			e.printStackTrace();
			kyclog.error("------------>getLatestFpmsCustData ",e);
			retMsg="Document upload failed";
		}

		}

		return retMsg;
	}


	
	
	
	
	
	
	
	
	
	
	 
	
	public ResultSet  searchAttachmentById(DBIntf dto,String strAtchId ) throws HibernateException{
		  ResultSet attachDocList = null;
		  try{
			  String strWhrcondQry = " where (attch.CUST_ATTACH_ID)='"+strAtchId+"'";

			  attachDocList = dto.downloadBFILE(EKYCQuery.HQL_FIND_CLTATTACH_BY_ID + strWhrcondQry,"",null);//,strAtchId, strCliId,strDistID);
		  }catch (HibernateException he) {

//			  he.printStackTrace();
			  kyclog.error("------------>searchAttachmentById ",he);
		}
		  return  attachDocList;
	  }
	
	public FnaDependantDets insertFnaDependatDets(DBIntf dbDTO, FnaDependantDets fnaDao,
			String strFnaId) {

		Object fnaObj = null;

		String dataFormId = "";

		// try{
		String maxQry = "select (max(substr(DEPN_ID,length(DEPN_ID)-14,15))+1) from FNA_DEPENDANT_DETS";
		int maxAdvStfVal = Integer.parseInt(dbDTO.fetchMaxSeqVal(maxQry));

		dataFormId = "DEP"+ KycUtils.lpadString(17, String.valueOf(maxAdvStfVal));

		fnaDao.setDepnId(dataFormId);

		FnaDetails fnaDetail = new FnaDetails();
		fnaDetail.setFnaId(strFnaId);

		fnaDao.setFnaDetails(fnaDetail);
		fnaObj = fnaDao;

		dbDTO.insertDB(fnaObj);

		return fnaDao;
	}
	
	
	public void updateDepentDetls(DBIntf dbDTO, FnaDependantDets fnaDepnDetls) {
		dbDTO.updateDB(fnaDepnDetls);
	}
	
	
	public void insertOtherPersDets(List allPerList,String strFnaId,DBIntf dbDTO)
			throws HibernateException {
	

		int insSize = allPerList.size();

		if (insSize > 0) {

			FnaOtherpersonDets othpers = new FnaOtherpersonDets();
			
			//poovathi commented on 12-05-2021 for testing purpose
			 String STR_DEPN_DEL_QRY = " delete from FNA_OTHERPERSON_DETS WHERE FNA_ID ='"+strFnaId+"'";
			 dbDTO.updateDbByNativeSQLQuery(STR_DEPN_DEL_QRY);

			String brkEduMaxQuery = "select (max(substr(OTHPER_ID,length(OTHPER_ID)-14,15))+1) from FNA_OTHERPERSON_DETS";
			int maxVal = Integer.parseInt(dbDTO.fetchMaxSeqVal(brkEduMaxQuery));
			Iterator itIns = allPerList.iterator();
			FnaDetails fd = new FnaDetails();
			fd.setFnaId(strFnaId);

	  while (itIns.hasNext()) {
		othpers = (FnaOtherpersonDets) itIns.next();
//		String qry = "select lpad(" + maxVal + ",17,0) from dual";
//		String grpId = "OTP" + dbDTO.fetchMaxSeqVal(qry);
		othpers.setOthperId(null);
		if(KycUtils.nullOrBlank(othpers.getOthperId())) {
			String grpId = "OTP" + KycUtils.lpadString(17, String.valueOf(maxVal));
			othpers.setOthperId(grpId);
			maxVal++;
		}
		othpers.setFnaDetails(fd);
		
	}
			
			dbDTO.insOrUpdTableValues(allPerList, othpers);

		}



	}
	
	
public List<FnaOtherpersonDets> findFNAOthPersDetailsByFnaId(DBIntf dbDTO, String strFnaId) {
		
		List<FnaOtherpersonDets> resList = (List<FnaOtherpersonDets>) dbDTO.searchList(EKYCQuery.STR_HQL_FNAOTHPERS_BYFNAID, strFnaId);
		
		return resList;		
	}


   // delete dependent details

   public void deleteDepnDetls(DBIntf dbDTO, String strDepnId) {
	   try{
		   String STR_DEPN_DEL_QRY = " delete from FNA_DEPENDANT_DETS WHERE DEPN_ID ='"+strDepnId+"'";
			 dbDTO.updateDbByNativeSQLQuery(STR_DEPN_DEL_QRY);

		}catch(HibernateException e){

		}

		
	}
   
  // edit depn data
	
	public List edtDepnData(DBIntf dbDTO, String strdepnId) {
		
		List depnList = new ArrayList();

	    //depnList = dbDTO.searchList(EKYCQuery.STR_HQL_DEPENDANT_BYDEPID, strdepnId);
		String STR_HQL_DEPENDANT_BY_DEPID = " select * from FNA_DEPENDANT_DETS WHERE DEPN_ID ='"+strdepnId+"'";
	    depnList = dbDTO.searchByNativeSQLQuery(STR_HQL_DEPENDANT_BY_DEPID);
		return depnList;

	}
 
	
	
	 

	 public List<FnaDetails> getAdvtoManagerFnaList(DBIntf dbDTO, String strLogUserAdvId) {
		 
        List<FnaDetails> fnaList = new ArrayList<FnaDetails>();
        
        
        
        //SELECT  customer_details.cust_name ,fna_details.fna_id,fna_details.mgr_approve_status,fna_details.mgr_approve_date,fna_details.mgrapprsts_byadvstf,fna_details.admin_approve_status,fna_details.admin_approve_date,fna_details.adminapprsts_byadvstf,fna_details.comp_approve_status,fna_details.comp_approve_date,fna_details.compapprsts_byadvstf FROM  FNA_DETAILS 
      //FULL OUTER JOIN CUSTOMER_DETAILS ON FNA_DETAILS.CUST_ID=CUSTOMER_DETAILS.CUSTID WHERE  MGR_EMAIL_SENT_FLG = 'Y'  AND  KYC_SENT_STATUS= 'YES'  AND ADVSTF_ID ='ADVFPF00000000000018';
		 
		 String STR_HQL_ADVTOMGR_LIST_BYFNAID = " SELECT  customer_details.cust_name ,fna_details.fna_id,fna_details.mgr_approve_status,"
		 		+ "fna_details.mgr_approve_date,fna_details.mgrapprsts_byadvstf,fna_details.admin_approve_status,fna_details.admin_approve_date,"
		 		+ "fna_details.adminapprsts_byadvstf,fna_details.comp_approve_status,fna_details.comp_approve_date,fna_details.compapprsts_byadvstf FROM  FNA_DETAILS "
		 		+ " FULL OUTER JOIN CUSTOMER_DETAILS ON FNA_DETAILS.CUST_ID=CUSTOMER_DETAILS.CUSTID WHERE  MGR_EMAIL_SENT_FLG = 'Y' "
		 		+ " AND  KYC_SENT_STATUS= 'YES'  AND ADVSTF_ID ='"+strLogUserAdvId+"'";
		 fnaList =  (List<FnaDetails>) dbDTO.searchByNativeSQLQuery(STR_HQL_ADVTOMGR_LIST_BYFNAID);
			return fnaList;

}
	//prod switch and replace page related functions start here
	 public List allFNASwtchrepProdPlan(DBIntf dbDTO, String strFnaId) {

			List recomList = new ArrayList();

			recomList = dbDTO.searchList(EKYCQuery.STR_HQL_SWTCHREP_PLANS, strFnaId);

			return recomList;
		}
 
	 
	 public FnaSwtchrepPlanDet addSwtchrepPlanDets(DBIntf dbDTO, FnaSwtchrepPlanDet swtchrepplanDet,
				String strFnaId) {

			int maxVal = Integer.parseInt(dbDTO
					.fetchMaxSeqVal(EKYCQuery.STR_SQL_SWTCHREP_PP_ID_MAX));
			FnaDetails fd = new FnaDetails();
			fd.setFnaId(strFnaId);
			String savId = "SRP" + KycUtils.lpadString(17, String.valueOf(maxVal));
			swtchrepplanDet.setSwrepPpId(savId);
			swtchrepplanDet.setFnaDetails(fd);

			dbDTO.insertDB(swtchrepplanDet);

			return swtchrepplanDet;

		}
	 
	 
	 public void updateFnaSwtchrepPlanDet(DBIntf dbDTO,  FnaSwtchrepPlanDet swtchrepplanDet, String strswrepPpId,String strFNAId) {
		    swtchrepplanDet.setSwrepPpId(strswrepPpId);
		    swtchrepplanDet.setCreatedDate(new Date());
			FnaDetails fnaDet = new FnaDetails();
			fnaDet.setFnaId(strFNAId);
			swtchrepplanDet.setFnaDetails(fnaDet);
			dbDTO.updateDB(swtchrepplanDet);
		}
	 
	 //delete Process
	 
	 public void deleteRiderProdPlanofSwitchReplace(DBIntf dbDTO, String strswrepPpId) {
		   try{
			   String STR_SQL_SWTCHREP_DELETE_RDR_PLANS = " delete from FNA_SWTCHREP_PLAN_DET WHERE SWREP_PP_ID ='"+strswrepPpId+"'";
				 dbDTO.updateDbByNativeSQLQuery(STR_SQL_SWTCHREP_DELETE_RDR_PLANS);

			}catch(HibernateException e){
			}

			
		}

		public void deleteBasicProdPlanofSwitchReplace(DBIntf dbDTO, String strRecomPpProdName, String strFNAId){
			
			    try{
				   String STR_SQL_SWTCHREP_DELETE_BSC_PLANS = "DELETE FROM FNA_SWTCHREP_PLAN_DET  WHERE UPPER(SWREP_PP_PRODNAME) = '"+strRecomPpProdName.toUpperCase()+"' AND (SWREP_PP_BASRID = 'BASIC' OR SWREP_PP_BASRID='B') AND  FNA_ID='"+strFNAId+"'";	
				        dbDTO.updateDbByNativeSQLQuery(STR_SQL_SWTCHREP_DELETE_BSC_PLANS);

				   String STR_SQL_SWTCHREP_DELETE_BCS_RDR_PLANS = "DELETE FROM FNA_SWTCHREP_PLAN_DET  WHERE RECOM_BASIC_REF = '"+strRecomPpProdName+"' AND (SWREP_PP_BASRID = 'RIDER' OR SWREP_PP_BASRID = 'R') AND  FNA_ID='"+strFNAId+"'";
				     dbDTO.updateDbByNativeSQLQuery(STR_SQL_SWTCHREP_DELETE_BCS_RDR_PLANS);	 
					 
				}catch(HibernateException e){

				}

		}

		public void deletePrincipalByClntNameofSwitchReplace(DBIntf dbDTO, String strSwrepPpName, String SwrepPpPrin, String strFNAId) {
			
			try{
				String STR_SQL_SWTCHREP_DELETE_PRIN = "DELETE FROM FNA_SWTCHREP_PLAN_DET  WHERE   UPPER(SWREP_PP_NAME) = '"+strSwrepPpName.toUpperCase()+"' AND  UPPER(SWREP_PP_PRIN) = '"+SwrepPpPrin.toUpperCase()+"'  AND  FNA_ID='"+strFNAId+"'";	
					 dbDTO.updateDbByNativeSQLQuery(STR_SQL_SWTCHREP_DELETE_PRIN);
		 
				}catch(HibernateException e){
				 
				}
		}

		public void deleteProdRecomClntProdPlanofSwitchReplace(DBIntf dbDTO, String strSwrepPpName, String strFNAId) {
		
			try{
				String STR_SQL_RECOM_DELETE_PRIN = "DELETE FROM FNA_SWTCHREP_PLAN_DET  WHERE UPPER(SWREP_PP_NAME) = '"+strSwrepPpName.toUpperCase()+"'  AND  FNA_ID='"+strFNAId+"'";	
					 dbDTO.updateDbByNativeSQLQuery(STR_SQL_RECOM_DELETE_PRIN);
		 
				}catch(HibernateException e){
					
				}
		}

		public void deleteBasicPlnsBySwrepPpId(DBIntf dbDTO, String strswrepPpId, String strSwrepPpProdName, String strFNAId) {
			
			 try {
				   String STR_SQL_RECOM_DELETE_BSC_PLANS = "DELETE FROM FNA_SWTCHREP_PLAN_DET  WHERE UPPER(SWREP_PP_PRODNAME) = '"+strSwrepPpProdName.toUpperCase()+"' AND ( SWREP_PP_BASRID = 'BASIC' OR SWREP_PP_BASRID = 'B' ) AND  FNA_ID='"+strFNAId+"' AND SWREP_PP_ID='"+strswrepPpId+"'";	
				    dbDTO.updateDbByNativeSQLQuery(STR_SQL_RECOM_DELETE_BSC_PLANS);

				   String STR_SQL_RECOM_DELETE_BCS_RDR_PLANS = "DELETE FROM FNA_SWTCHREP_PLAN_DET  WHERE UPPER(RECOM_BASIC_REF) = '"+strSwrepPpProdName.toUpperCase()+"' AND (SWREP_PP_BASRID = 'RIDER' OR SWREP_PP_BASRID = 'R')AND  FNA_ID='"+strFNAId+"'";
				     dbDTO.updateDbByNativeSQLQuery(STR_SQL_RECOM_DELETE_BCS_RDR_PLANS);	 
					 
				} catch(HibernateException e){

				}
		}
		

		public FnaDetails findFnaDetlsJoinByFnaId(DBIntf dbDTO, String strFNAId) {

			List fnaList = new ArrayList();
			
			fnaList = dbDTO.searchList(EKYCQuery.STR_HQL_FNADETLS_JOIN_BY_FNAID,strFNAId);
			
			if(fnaList.size()>0) {
				FnaDetails fnaObj = (FnaDetails)fnaList.get(0);	
				return fnaObj;
			}else {
				return null;
			}

			

//			return fnaObj;
		}
		
		public void kycSentStsUpdate(DBIntf dbDTO,String strFnaId,String strCustId,String strMgrMailSentFlg,String strlogadvid)throws HibernateException {

			 String qryForm=" update FNA_DETAILS set KYC_SENT_STATUS='YES',MGR_EMAIL_SENT_FLG='"+strMgrMailSentFlg+"', MGR_APPROV_ADVSTF_ID='"+strlogadvid+"' WHERE FNA_ID='"+strFnaId+"' AND CUST_ID='"+strCustId+"'";
			 dbDTO.updateDbByNativeSQLQuery(qryForm);
			 
			 
//				This block is added in new kyc dev to reset the previous status while resend it
			 qryForm = " update FNA_DETAILS set "
						+ "MGR_APPROVE_STATUS=null"
						+ ",MGR_STATUS_REMARKS=null"
						+ ",MGR_APPROVE_DATE = null"					
						+ " WHERE FNA_ID='"+strFnaId+"' AND MGR_APPROVE_STATUS='REJECT'";
				
			 dbDTO.updateDbByNativeSQLQuery( qryForm);
//				This block is added in new kyc dev to reset the previous status while resend it
		 }
		
		 public void updateManagerApproveStatus(DBIntf dto,String UPDATE_QRY) {

				try{
					 //String qryForm=" update FNA_DETAILS set MGR_APPROVE_STATUS='"+status+"' , MGR_STATUS_REMARKS='"+strRemarks+"', KYC_SENT_STATUS='YES' , MGR_APPROVE_DATE = SYSDATE WHERE FNA_ID='"+strFnaId+"'";
//					 System.out.println(UPDATE_QRY);
					 dto.updateDbByNativeSQLQuery(UPDATE_QRY);

				}catch(Exception e){
					//e.printStackTrace();
					kyclog.info("----------> Error in updateManagerApproveStatus QRY --> " +UPDATE_QRY , e);
				}

		}
		 
		 public void updateAdminApproveStatus(DBIntf dto,String UPDATE_QRY) throws HibernateException{

				try{
//					 String qryForm=" update FNA_DETAILS set ADMIN_APPROVE_STATUS='"+status+" , ADMIN_APPROVE_DATE = SYSDATE WHERE FNA_ID='"+strFnaId+"'";
					 dto.updateDbByNativeSQLQuery(UPDATE_QRY);

				}catch(HibernateException e){
					kyclog.info("----------> updateAdminApproveStatus , QRY --->  "+UPDATE_QRY , e);
				}

		}
		 
		 public void updateCompApproveStatus(DBIntf dto,String UPDATE_QRY) throws HibernateException{

				try{
//					 String qryForm=" update FNA_DETAILS set COMP_APPROVE_STATUS='"+status+" , COMP_APPROVE_DATE = SYSDATE WHERE FNA_ID='"+strFnaId+"'";
					 dto.updateDbByNativeSQLQuery(UPDATE_QRY);

				}catch(HibernateException e){
					kyclog.info("----------> updateCompApproveStatus, QRY ------> "+UPDATE_QRY , e);
				}

		}
		 
		 public List getManagerFnaDetails(DBIntf dbDTO,String strLogAdvStfId,String strFnaId) throws HibernateException{
				List fnaDetList = new ArrayList();
				try{
					if(KycUtils.checkNullVal(dbDTO)){
//						WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(ServletActionContext.getServletContext());
						ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
						dbDTO = (DBIntf) ctx.getBean("DBDAO");
					}
//					String strQueryParam = "SELECT FNA.FNA_ID,FNA.CUST_ID,FNA.ADVSTF_ID,ADVSTF.ADVSTF_NAME,CUST.CUST_NAME,FNA.MGR_APPROVE_STATUS, FNA.MGR_APPROVE_DATE,"+
//							"FNA.MGR_STATUS_REMARKS, FNA.ADMIN_APPROVE_STATUS, FNA.ADMIN_APPROVE_DATE, FNA.ADMIN_REMARKS, FNA.COMP_APPROVE_STATUS,"+
//							"FNA.COMP_APPROVE_DATE,FNA.COMP_REMARKS,FNA.NTUC_POLICY_ID,FNA.NTUC_POL_NUM,ADVSTF.EMAIL_ID  FROM FNA_DETAILS FNA,ADVISER_STAFF ADVSTF,CUSTOMER_DETAILS CUST  WHERE (UPPER(FNA.MGR_APPROVE_STATUS) <> UPPER('APPROVE') OR (FNA.MGR_APPROVE_STATUS is null)) AND"+
//							"  ADVSTF.ADVSTF_ID = FNA.ADVSTF_ID  AND CUST.CUSTID = FNA.CUST_ID  AND (FNA.MGR_ID ='"+strLogAdvStfId+"' OR FNA.ADVSTF_ID='"+strLogAdvStfId+"')";



					String strWhrCondition  = "  WHERE "
//							+ "	 FNA.MGR_APPROVE_STATUS IS NULL AND "//Oct_2018
							+ "  ADVSTF.MANAGER_ID = ADVSTFMGR.ADVSTF_ID"
							+ "  AND ADVSTF.ADVSTF_ID = FNA.ADVSTF_ID "
							+ "  AND advstf.advstf_id = advcode.ADVSTF_ID (+)"
							+ "  AND advcode.PRIN_ID='PRIN000065'" 
							+ "  AND CUST.CUSTID = FNA.CUST_ID "
							+ "  AND FNA.FNA_ID = FSSD.FNA_ID "
							+ "  AND FNA.MGR_EMAIL_SENT_FLG ='Y'"
//							+ "  AND FNA.NTUC_POLICY_ID  IS NOT NULL"
							+ "  AND FNA.NTUC_CASE_ID  IS NOT NULL"
							+ "  AND (FNA.MGR_ID='"+strLogAdvStfId+"'"
							+ "	 OR FNA.MGR_ID IN (SELECT ADVSTF_ID FROM ADVSTF_PROXY_MGR_DETS PRXMGR "
												+ "	 WHERE MANAGER_ID ='"+strLogAdvStfId+"' "
												+ " AND TO_DATE(PRXMGR.START_DATE,'DD/MM/YYYY')<= TO_DATE(SYSDATE,'DD/MM/YYYY') AND TO_DATE(PRXMGR.END_DATE,'DD/MM/YYYY') >=TO_DATE(SYSDATE,'DD/MM/YYYY'))"+ ")";

												

				String strWhrConditionNonNtuc  = "  WHERE "
//				+ "	 FNA.MGR_APPROVE_STATUS IS NULL AND "//Oct_2018
				+ "  ADVSTF.MANAGER_ID = ADVSTFMGR.ADVSTF_ID"
				+ "  AND ADVSTF.ADVSTF_ID = FNA.ADVSTF_ID "
				+ "  AND CUST.CUSTID = FNA.CUST_ID "
				+ "  AND FNA.FNA_ID = FSSD.FNA_ID "
				+ "  AND FNA.MGR_EMAIL_SENT_FLG ='Y'"
				+ "  AND (FNA.MGR_ID='"+strLogAdvStfId+"'"
				+ "	 OR FNA.MGR_ID IN (SELECT ADVSTF_ID FROM ADVSTF_PROXY_MGR_DETS PRXMGR "
				+ "	 WHERE MANAGER_ID ='"+strLogAdvStfId+"' "
				+ "  AND TO_DATE(PRXMGR.START_DATE,'DD/MM/YYYY')<= TO_DATE(SYSDATE,'DD/MM/YYYY') AND TO_DATE(PRXMGR.END_DATE,'DD/MM/YYYY') >=TO_DATE(SYSDATE,'DD/MM/YYYY'))"+ ")"
//				+ "  AND FNA.FNA_ID NOT IN (SELECT FNA_ID FROM FNA_RECOM_PRDTPLAN_DET WHERE RECOM_PP_PRIN = 'NTUC Income')"
			//+ "  AND FNA.FNA_ID IN (SELECT FNA_ID FROM FNA_RECOM_PRDTPLAN_DET WHERE RECOM_PP_PRIN != 'NTUC Income')"
				+ "  AND FNA.NTUC_CASE_ID  IS NULL"	
				+ "   AND FNA.NTUC_CASE_STATUS IS NULL "
			;
				//For Manager No;
				
					if(!KycUtils.nullOrBlank(strFnaId)){
						strWhrCondition +=" AND FNA.fna_id='"+strFnaId+"'";
						strWhrConditionNonNtuc +=" AND FNA.fna_id='"+strFnaId+"'";
					}
					
					String strOrderBy = " ORDER BY 5,3,1 DESC ";
					
//					 AND TO_DATE(PRXMGR.START_DATE,'DD/MM/YYYY')<= TO_DATE(SYSDATE,'DD/MM/YYYY') AND TO_DATE(PRXMGR.END_DATE,'DD/MM/YYYY') >=TO_DATE(SYSDATE,'DD/MM/YYYY')

					fnaDetList = dbDTO.searchByNativeSQLQuery(
							
							EKYCQuery.SQL_FNA_APPROVAL_ACSLVL_QRY + strWhrCondition + " UNION "	+ 
									EKYCQuery.SQL_FNA_APPROVAL_ACSLVL_NONNTUC_QRY + strWhrConditionNonNtuc + strOrderBy 
					);



				}catch(HibernateException e){
					kyclog.info("----------> getManagerFnaDetails " , e);
				}
				return fnaDetList;
			}

			public List getAdminFnaDetails(DBIntf dbDTO,String strFnaId) throws HibernateException{
				List fnaDetList = new ArrayList();
				try{
					if(KycUtils.checkNullVal(dbDTO)){
//						WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(ServletActionContext.getServletContext());
						ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
						dbDTO = (DBIntf) ctx.getBean("DBDAO");
					}

//					String strQueryParam = "SELECT FNA.FNA_ID,FNA.CUST_ID,FNA.ADVSTF_ID,ADVSTF.ADVSTF_NAME,CUST.CUST_NAME,FNA.MGR_APPROVE_STATUS, FNA.MGR_APPROVE_DATE,"+
//							"FNA.MGR_STATUS_REMARKS, FNA.ADMIN_APPROVE_STATUS, FNA.ADMIN_APPROVE_DATE, FNA.ADMIN_REMARKS, FNA.COMP_APPROVE_STATUS,"+
//							"FNA.COMP_APPROVE_DATE,FNA.COMP_REMARKS,FNA.NTUC_POLICY_ID,FNA.NTUC_POL_NUM,ADVSTF.EMAIL_ID  FROM FNA_DETAILS FNA,ADVISER_STAFF ADVSTF,CUSTOMER_DETAILS CUST  WHERE  (UPPER(FNA.MGR_APPROVE_STATUS) = UPPER('APPROVE') OR FNA.MGR_APPROVE_STATUS is NOT NULL )  AND "+
//							" FNA.MGR_APPROVE_STATUS <> 'REJECT' AND (UPPER(FNA.ADMIN_APPROVE_STATUS) <> UPPER('APPROVE') OR (FNA.ADMIN_APPROVE_STATUS is null)) AND ADVSTF.ADVSTF_ID = FNA.ADVSTF_ID  AND CUST.CUSTID = FNA.CUST_ID  ";


					String strWhrCondition = " WHERE "
//							+ " FNA.MGR_APPROVE_STATUS IS NOT NULL  AND "//Oct_2018
//							+ " FNA.ADMIN_APPROVE_STATUS  IS NULL AND "//Oct_2018
//							+ " FNA.COMP_APPROVE_STATUS IS NULL AND "//Oct_2018
							+ " (FNA.MGR_APPROVE_STATUS IS NOT NULL AND FNA.MGR_APPROVE_STATUS = 'APPROVE')  AND "//aug_2019
							+ " ADVSTF.MANAGER_ID = ADVSTFMGR.ADVSTF_ID"
							+ " AND ADVSTF.ADVSTF_ID = FNA.ADVSTF_ID "
							+ "  AND advstf.advstf_id = advcode.ADVSTF_ID (+)"
							+ "  AND advcode.PRIN_ID='PRIN000065'"
							+ " AND CUST.CUSTID= FNA.CUST_ID"
							+ " AND FNA.FNA_ID = FSSD.FNA_ID "
							+ " AND FNA.MGR_EMAIL_SENT_FLG ='Y'"//Oct_2018
//							+ " AND FNA.NTUC_POLICY_ID  IS NOT NULL "//Oct_2018
							+ "  AND FNA.NTUC_CASE_ID  IS NOT NULL"
							//This cond is commented on 02/11/2018
//							+ " AND FNA.FNA_ID = (SELECT MAX(FNA_ID) FROM FNA_DETAILS WHERE CUST_ID = FNA.CUST_ID)"
//							+"   AND FNA.FNA_ID NOT IN (SELECT TMP.FNA_ID FROM FNA_DETAILS TMP WHERE TMP.MGR_APPROVE_STATUS IS NOT NULL AND TMP.MGR_APPROVE_STATUS = 'APPROVE' AND   TMP.FNA_ID = '"+strFnaId+"') "
							+" AND UPPER(FNA.NTUC_CASE_STATUS) IN ( 'SUBMITTED' ,'APPROVED') "
							;
					
					
					String strWhrConditionNoNtuc = " WHERE "
//							+ " FNA.MGR_APPROVE_STATUS IS NOT NULL  AND "//Oct_2018
//							+ " FNA.ADMIN_APPROVE_STATUS  IS NULL AND "//Oct_2018
//							+ " FNA.COMP_APPROVE_STATUS IS NOT NULL AND "//Oct_2018//aug_2019
							+ " (FNA.MGR_APPROVE_STATUS IS NOT NULL AND FNA.MGR_APPROVE_STATUS = 'APPROVE')  AND "//aug_2019
//							+ " (FNA.ADMIN_APPROVE_STATUS IS NULL OR FNA.ADMIN_APPROVE_STATUS = 'REJECT') "
//							+ " FNA.COMP_APPROVE_STATUS IS NULL AND "//aug_2019
							+ " ADVSTF.MANAGER_ID = ADVSTFMGR.ADVSTF_ID"
							+ " AND ADVSTF.ADVSTF_ID = FNA.ADVSTF_ID "
							+ " AND CUST.CUSTID= FNA.CUST_ID"
							+ " AND FNA.FNA_ID = FSSD.FNA_ID "
							+ " AND FNA.MGR_EMAIL_SENT_FLG ='Y'"//Oct_2018
//							+ " AND FNA.NTUC_POLICY_ID  IS NOT NULL "//Oct_2018
							//This cond is commented on 02/11/2018
//							+ " AND FNA.FNA_ID = (SELECT MAX(FNA_ID) FROM FNA_DETAILS WHERE CUST_ID = FNA.CUST_ID)"
//							+ "  AND FNA.FNA_ID NOT IN (SELECT FNA_ID FROM FNA_RECOM_PRDTPLAN_DET WHERE RECOM_PP_PRIN ='NTUC Income')"
//							+ "  AND FNA.FNA_ID  IN (SELECT FNA_ID FROM FNA_RECOM_PRDTPLAN_DET WHERE RECOM_PP_PRIN != 'NTUC Income')"
//							+ "  AND FNA.NTUC_CASE_ID  IS NULL"	
//							+ "   AND FNA.NTUC_CASE_STATUS IS NULL "
							+" AND ( UPPER(FNA.NTUC_CASE_STATUS) NOT IN ( 'SUBMITTED' ,'APPROVED') OR FNA.NTUC_CASE_STATUS IS NULL ) "
							;

					if(!KycUtils.nullOrBlank(strFnaId)){
						strWhrCondition+=" AND FNA.fna_id='"+strFnaId+"'";
						strWhrConditionNoNtuc+=" AND FNA.fna_id='"+strFnaId+"'";
					}
					
					String strOrderBy = " ORDER BY 5,3,1 DESC ";
					
					String QRY = EKYCQuery.SQL_FNA_APPROVAL_ACSLVL_QRY + strWhrCondition  +" UNION " + 
							EKYCQuery.SQL_FNA_APPROVAL_ACSLVL_NONNTUC_QRY + strWhrConditionNoNtuc + strOrderBy;

					fnaDetList = dbDTO.searchByNativeSQLQuery(QRY);



				}catch(HibernateException e){
					kyclog.info("----------> getFnaDetails " , e);
				}
				return fnaDetList;
			}

			public List getCompFnaDetails(DBIntf dbDTO,String strFnaId) throws HibernateException{
				List fnaDetList = new ArrayList();
				try{
					if(KycUtils.checkNullVal(dbDTO)){
//						WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(ServletActionContext.getServletContext());
						ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
						dbDTO = (DBIntf) ctx.getBean("DBDAO");
					}



					String strWhrCondition = " WHERE "
//							+ " FNA.MGR_APPROVE_STATUS IS NOT NULL "//Oct_2018
//							+ " AND FNA.ADMIN_APPROVE_STATUS IS NOT NULL"//Oct_2018
//							+ " AND FNA.COMP_APPROVE_STATUS  IS NULL AND " //Oct_2018
//							+ " (upper(FNA.COMP_APPROVE_STATUS) != 'APPROVE'  OR fna.COMP_APPROVE_STATUS is null) AND"
							+ " ADVSTF.MANAGER_ID = ADVSTFMGR.ADVSTF_ID"
							+ " AND ADVSTF.ADVSTF_ID = FNA.ADVSTF_ID"
							+ " AND advstf.advstf_id = advcode.ADVSTF_ID (+)"
							+ " AND advcode.PRIN_ID='PRIN000065'"
							+ " AND CUST.CUSTID = FNA.CUST_ID"
							+ " AND FNA.FNA_ID = FSSD.FNA_ID "
							+ " AND FNA.MGR_EMAIL_SENT_FLG ='Y'"//Oct_2018
//							+ " AND FNA.NTUC_POLICY_ID  IS NOT NULL"//Oct_2018
							+ "  AND FNA.NTUC_CASE_ID  IS NOT NULL"
							+ " AND FNA.NTUC_CASE_STATUS IS NOT NULL  "
//							+ " UPPER(FNA.NTUC_CASE_STATUS) ='SUBMITTED' "
//							+ " AND FNA.FNA_ID = (SELECT MAX(FNA_ID) FROM FNA_DETAILS WHERE CUST_ID = FNA.CUST_ID)"
//							+"   AND FNA.FNA_ID NOT IN (SELECT TMP.FNA_ID FROM FNA_DETAILS TMP WHERE TMP.MGR_APPROVE_STATUS IS NOT NULL AND TMP.MGR_APPROVE_STATUS = 'APPROVE' AND   TMP.FNA_ID = '"+strFnaId+"') "
							+"  AND UPPER(FNA.NTUC_CASE_STATUS) IN ( 'SUBMITTED' , 'APPROVED') "
							;
					
					

					String strWhrConditionNoNtuc = " WHERE "
//							+ " FNA.MGR_APPROVE_STATUS IS NOT NULL "//Oct_2018
//							+ " AND FNA.ADMIN_APPROVE_STATUS IS NOT NULL"//Oct_2018
//							+ " AND FNA.COMP_APPROVE_STATUS  IS NULL AND " //Oct_2018
//							+ " (upper(FNA.COMP_APPROVE_STATUS) != 'APPROVE'  OR fna.COMP_APPROVE_STATUS is null) "//aug_2019
							+ " FNA.MGR_APPROVE_STATUS IS NOT NULL AND "//aug_2019
//							+ " AND FNA.ADMIN_APPROVE_STATUS  = 'APPROVE' "
//							+ " AND FNA.COMP_APPROVE_STATUS  IS NULL "//aug_2019
							+ "  ADVSTF.MANAGER_ID = ADVSTFMGR.ADVSTF_ID"
							+ " AND ADVSTF.ADVSTF_ID = FNA.ADVSTF_ID"
							+ " AND CUST.CUSTID = FNA.CUST_ID"
							+ " AND FNA.FNA_ID = FSSD.FNA_ID "
							+ " AND FNA.MGR_EMAIL_SENT_FLG ='Y'"//Oct_2018
//							+ " AND FNA.FNA_ID = (SELECT MAX(FNA_ID) FROM FNA_DETAILS WHERE CUST_ID = FNA.CUST_ID)"
//							+ "  AND FNA.FNA_ID NOT IN (SELECT FNA_ID FROM FNA_RECOM_PRDTPLAN_DET WHERE RECOM_PP_PRIN ='NTUC Income')"
//							+ "  AND FNA.FNA_ID IN (SELECT FNA_ID FROM FNA_RECOM_PRDTPLAN_DET WHERE RECOM_PP_PRIN != 'NTUC Income')"
//							+ "  AND FNA.NTUC_CASE_ID  IS NULL"	
//							+ "   AND FNA.NTUC_CASE_STATUS IS NULL "
							+"  AND ( UPPER(FNA.NTUC_CASE_STATUS) NOT IN ( 'SUBMITTED' ,'APPROVED') OR FNA.NTUC_CASE_STATUS IS NULL )"
			;
							
					

					if(!KycUtils.nullOrBlank(strFnaId)){
						strWhrCondition+=" AND FNA.fna_id='"+strFnaId+"'";
					}
					
					
					String strOrderBy = " ORDER BY 5,3,1 DESC ";
					
					String QRY = EKYCQuery.SQL_FNA_APPROVAL_ACSLVL_QRY + strWhrCondition  +" UNION " + 
							EKYCQuery.SQL_FNA_APPROVAL_ACSLVL_NONNTUC_QRY + strWhrConditionNoNtuc + strOrderBy;


					fnaDetList = dbDTO.searchByNativeSQLQuery(QRY );



				}catch(HibernateException e){
					kyclog.info("----------> getFnaDetails " , e);
				}
				return fnaDetList;
			}

			public List getCompAndAdminFnaDets(DBIntf dbDTO,String strFnaId) throws HibernateException{
				List fnaDetList = new ArrayList();
				try{
					if(KycUtils.checkNullVal(dbDTO)){
//						WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(ServletActionContext.getServletContext());
						ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
						dbDTO = (DBIntf) ctx.getBean("DBDAO");
					}



					String strWhrCondition = " WHERE "
//							+ " FNA.MGR_APPROVE_STATUS IS NOT NULL "//Oct_2018
//							+ " AND FNA.ADMIN_APPROVE_STATUS IS NOT NULL"//Oct_2018
//							+ " AND FNA.COMP_APPROVE_STATUS  IS NULL AND " //Oct_2018
//							+ " (upper(FNA.COMP_APPROVE_STATUS) != 'APPROVE'  OR fna.COMP_APPROVE_STATUS is null) AND"				
							+ " ADVSTF.MANAGER_ID = ADVSTFMGR.ADVSTF_ID"
							+ " AND ADVSTF.ADVSTF_ID = FNA.ADVSTF_ID"
							+ " AND advstf.advstf_id = advcode.ADVSTF_ID (+)"
							+ " AND advcode.PRIN_ID='PRIN000065'"
							+ " AND CUST.CUSTID = FNA.CUST_ID"
							+ " AND FNA.FNA_ID = FSSD.FNA_ID "
							+ " AND FNA.MGR_EMAIL_SENT_FLG ='Y'"//Oct_2018
//							+ " AND FNA.NTUC_POLICY_ID  IS NOT NULL"//Oct_2018
							+ "  AND FNA.NTUC_CASE_ID  IS NOT NULL"
							+ " AND FNA.NTUC_CASE_STATUS IS NOT NULL " + " AND UPPER(FNA.NTUC_CASE_STATUS) IN ( 'SUBMITTED' ,'APPROVED') "
//							+ " AND FNA.FNA_ID = (SELECT MAX(FNA_ID) FROM FNA_DETAILS WHERE CUST_ID = FNA.CUST_ID)"
			//+"   AND FNA.FNA_ID NOT IN (SELECT TMP.FNA_ID FROM FNA_DETAILS TMP WHERE TMP.MGR_APPROVE_STATUS IS NOT NULL AND TMP.MGR_APPROVE_STATUS = 'APPROVE'  AND   TMP.FNA_ID = '"+strFnaId+"') "

							;
					
					
					String strWhrConditionNoNtuc = " WHERE "
//							+ " FNA.MGR_APPROVE_STATUS IS NOT NULL "//Oct_2018
//							+ " AND FNA.ADMIN_APPROVE_STATUS IS NOT NULL"//Oct_2018
//							+ " AND FNA.COMP_APPROVE_STATUS  IS NULL AND " //Oct_2018
//							+ " (upper(FNA.COMP_APPROVE_STATUS) != 'APPROVE'  OR fna.COMP_APPROVE_STATUS is null) AND "
//							+ " AND FNA.ADMIN_APPROVE_STATUS IS NOT NULL AND FNA.ADMIN_APPROVE_STATUS = 'APPROVE' AND "//aug_2019
							+ "  ADVSTF.MANAGER_ID = ADVSTFMGR.ADVSTF_ID"
							+ " AND ADVSTF.ADVSTF_ID = FNA.ADVSTF_ID"
							+ " AND CUST.CUSTID = FNA.CUST_ID"
							+ " AND FNA.FNA_ID = FSSD.FNA_ID "
							+ " AND FNA.MGR_EMAIL_SENT_FLG ='Y'"//Oct_2018
//							+ " AND FNA.FNA_ID = (SELECT MAX(FNA_ID) FROM FNA_DETAILS WHERE CUST_ID = FNA.CUST_ID)"
//							+ "  AND FNA.FNA_ID NOT IN (SELECT FNA_ID FROM FNA_RECOM_PRDTPLAN_DET WHERE RECOM_PP_PRIN ='NTUC Income')";
//							+ "  AND FNA.FNA_ID IN (SELECT FNA_ID FROM FNA_RECOM_PRDTPLAN_DET WHERE RECOM_PP_PRIN != 'NTUC Income')";
//							+ "  AND FNA.NTUC_CASE_ID  IS NULL"
							+"  AND ( UPPER(FNA.NTUC_CASE_STATUS) NOT IN ( 'SUBMITTED' ,'APPROVED') OR FNA.NTUC_CASE_STATUS IS NULL )" 
							;

					if(!KycUtils.nullOrBlank(strFnaId)){
						strWhrCondition+=" AND FNA.fna_id='"+strFnaId+"'";
						strWhrConditionNoNtuc+=" AND FNA.fna_id='"+strFnaId+"'";
					}


					String strWhrConditionAdmin = " WHERE "
//							+ " FNA.MGR_APPROVE_STATUS IS NOT NULL  AND "//Oct_2018
//							+ " FNA.ADMIN_APPROVE_STATUS  IS NULL AND "//Oct_2018
//							+ " FNA.COMP_APPROVE_STATUS IS NULL AND "//Oct_2018		
							+ " (FNA.MGR_APPROVE_STATUS IS NOT NULL AND FNA.MGR_APPROVE_STATUS = 'APPROVE')  AND "
							+ " ADVSTF.MANAGER_ID = ADVSTFMGR.ADVSTF_ID"
							+ " AND ADVSTF.ADVSTF_ID = FNA.ADVSTF_ID "
							+ "  AND advstf.advstf_id = advcode.ADVSTF_ID (+)"
							+ "  AND advcode.PRIN_ID='PRIN000065'"
							+ " AND CUST.CUSTID= FNA.CUST_ID"
							+ " AND FNA.FNA_ID = FSSD.FNA_ID "
							+ " AND FNA.MGR_EMAIL_SENT_FLG ='Y'"//Oct_2018
//							+ " AND FNA.NTUC_POLICY_ID  IS NOT NULL "//Oct_2018
							+ "  AND FNA.NTUC_CASE_ID  IS NOT NULL"
							//This cond is commented on 02/11/2018
//							+ " AND FNA.FNA_ID = (SELECT MAX(FNA_ID) FROM FNA_DETAILS WHERE CUST_ID = FNA.CUST_ID)"
//							+ "   AND FNA.FNA_ID NOT IN (SELECT TMP.FNA_ID FROM FNA_DETAILS TMP WHERE TMP.MGR_APPROVE_STATUS IS NOT NULL  AND   TMP.FNA_ID = '"+strFnaId+"') "
							+" AND UPPER(FNA.NTUC_CASE_STATUS) IN ( 'SUBMITTED' ,'APPROVED') "
							;
					
					
					String strWhrConditionAdminNonNtuc = " WHERE "
//							+ " FNA.MGR_APPROVE_STATUS IS NOT NULL  AND "//Oct_2018
//							+ " FNA.ADMIN_APPROVE_STATUS  IS NULL AND "//Oct_2018
//							+ " FNA.COMP_APPROVE_STATUS IS NOT NULL AND "//Oct_2018
//							+ " (FNA.MGR_APPROVE_STATUS IS NOT NULL AND FNA.MGR_APPROVE_STATUS = 'APPROVE')  AND "//aug_2019
//							+ " (FNA.ADMIN_APPROVE_STATUS IS NULL OR FNA.ADMIN_APPROVE_STATUS = 'REJECT') AND "
//							+ " FNA.COMP_APPROVE_STATUS IS NULL AND "//aug_2019
							+ " (FNA.MGR_APPROVE_STATUS IS NOT NULL AND FNA.MGR_APPROVE_STATUS = 'APPROVE')  AND "
							+ " ADVSTF.MANAGER_ID = ADVSTFMGR.ADVSTF_ID"
							+ " AND ADVSTF.ADVSTF_ID = FNA.ADVSTF_ID "
							+ " AND CUST.CUSTID= FNA.CUST_ID"
							+ " AND FNA.FNA_ID = FSSD.FNA_ID "
							+ " AND FNA.MGR_EMAIL_SENT_FLG ='Y'"//Oct_2018
//							+ " AND FNA.NTUC_POLICY_ID  IS NOT NULL "//Oct_2018
							//This cond is commented on 02/11/2018
//							+ " AND FNA.FNA_ID = (SELECT MAX(FNA_ID) FROM FNA_DETAILS WHERE CUST_ID = FNA.CUST_ID)"
//							+ "  AND FNA.FNA_ID NOT IN (SELECT FNA_ID FROM FNA_RECOM_PRDTPLAN_DET WHERE RECOM_PP_PRIN ='NTUC Income')"
//							+ "  AND FNA.FNA_ID IN (SELECT FNA_ID FROM FNA_RECOM_PRDTPLAN_DET WHERE RECOM_PP_PRIN != 'NTUC Income')"
//							+ "  AND FNA.NTUC_CASE_ID  IS NULL"	
//							+ "   AND FNA.NTUC_CASE_STATUS IS NULL "
							+" AND ( UPPER(FNA.NTUC_CASE_STATUS) NOT IN ( 'SUBMITTED' ,'APPROVED') OR FNA.NTUC_CASE_STATUS IS NULL )"
							;

					if(!KycUtils.nullOrBlank(strFnaId)){
						
						strWhrConditionAdmin +=" AND FNA.fna_id='"+strFnaId+"'";
						
						strWhrConditionAdminNonNtuc +=" AND FNA.fna_id='"+strFnaId+"'";
						
					}
					
					
					String strOrderBy = " ORDER BY 5,3,1 DESC ";
					
					String QRY = EKYCQuery.SQL_FNA_APPROVAL_ACSLVL_QRY + strWhrConditionAdmin  +" UNION " + EKYCQuery.SQL_FNA_APPROVAL_ACSLVL_NONNTUC_QRY + strWhrConditionAdminNonNtuc + "UNION" + 
							EKYCQuery.SQL_FNA_APPROVAL_ACSLVL_QRY + strWhrCondition  +" UNION " + EKYCQuery.SQL_FNA_APPROVAL_ACSLVL_NONNTUC_QRY + strWhrConditionNoNtuc + strOrderBy;

					fnaDetList = dbDTO.searchByNativeSQLQuery( QRY );



				}catch(HibernateException e){
					kyclog.info("----------> getFnaDetails " , e);
				}
				return fnaDetList;
			}
			
			




public List loadFnaDetails(DBIntf dbDTO,String strLogAdvStfId){
	List fnaDetList = new ArrayList();
	try{


//		String strQueryParam = "SELECT FNA.FNA_ID,FNA.CUST_ID,FNA.ADVSTF_ID,ADVSTF.ADVSTF_NAME,CUST.CUST_NAME,FNA.MGR_APPROVE_STATUS, FNA.MGR_APPROVE_DATE,"+
//				"FNA.MGR_STATUS_REMARKS, FNA.ADMIN_APPROVE_STATUS, FNA.ADMIN_APPROVE_DATE, FNA.ADMIN_REMARKS, FNA.COMP_APPROVE_STATUS,"+
//				"FNA.COMP_APPROVE_DATE,FNA.COMP_REMARKS,FNA.NTUC_POLICY_ID,FNA.NTUC_POL_NUM,ADVSTF.EMAIL_ID  FROM FNA_DETAILS FNA,ADVISER_STAFF ADVSTF,CUSTOMER_DETAILS CUST  WHERE (UPPER(FNA.MGR_APPROVE_STATUS) <> UPPER('APPROVE') OR (FNA.MGR_APPROVE_STATUS is null)) AND"+
//				"  ADVSTF.ADVSTF_ID = FNA.ADVSTF_ID  AND CUST.CUSTID = FNA.CUST_ID  AND (FNA.MGR_ID ='"+strLogUserAdvId+"' OR FNA.ADVSTF_ID='"+strLogUserAdvId+"')";


		/*String strWhrCondition  = "  WHERE "
				+ "	 FNA.MGR_APPROVE_STATUS IS NULL "
				+ "  AND ADVSTF.MANAGER_ID = ADVSTFMGR.ADVSTF_ID"
				+ "  AND ADVSTF.ADVSTF_ID = FNA.ADVSTF_ID "
				+ "  AND CUST.CUSTID = FNA.CUST_ID "
				+ " AND FNA.FNA_ID = FSSD.FNA_ID"
				+ "  AND FNA.NTUC_POLICY_ID  IS NOT NULL"
				+ "  AND (FNA.MGR_ID='"+strLogUserAdvId+"'"
				+ "	 OR FNA.MGR_ID IN (SELECT ADVSTF_ID FROM ADVSTF_PROXY_MGR_DETS PRXMGR "
									+ "	 WHERE MANAGER_ID ='"+strLogUserAdvId+"'"
									+ " AND TO_DATE(PRXMGR.START_DATE,'DD/MM/YYYY')<= TO_DATE(SYSDATE,'DD/MM/YYYY') AND TO_DATE(PRXMGR.END_DATE,'DD/MM/YYYY') >=TO_DATE(SYSDATE,'DD/MM/YYYY'))"
									+ ")";*/

		String strWhrCondition  = "  WHERE "
//				+ "	 FNA.MGR_APPROVE_STATUS IS NULL AND "//Oct_2018
				+ "  ADVSTF.MANAGER_ID = ADVSTFMGR.ADVSTF_ID"
				+ "  AND ADVSTF.ADVSTF_ID = FNA.ADVSTF_ID "
				+ "  AND advstf.advstf_id = advcode.ADVSTF_ID (+)"
				+ "  AND advcode.PRIN_ID='PRIN000065'"
				+ "  AND CUST.CUSTID = FNA.CUST_ID "
				+ "  AND FNA.FNA_ID = FSSD.FNA_ID "
				+ "  AND FNA.MGR_EMAIL_SENT_FLG ='Y'"
//				+ "  AND FNA.NTUC_POLICY_ID  IS NOT NULL"
				+ "  AND FNA.NTUC_CASE_ID  IS NOT NULL"
				+ "  AND (FNA.MGR_ID='"+strLogAdvStfId+"'"
				+ "	 OR FNA.MGR_ID IN (SELECT ADVSTF_ID FROM ADVSTF_PROXY_MGR_DETS PRXMGR "
									+ "	 WHERE MANAGER_ID ='"+strLogAdvStfId+"' "
									+ " AND TO_DATE(PRXMGR.START_DATE,'DD/MM/YYYY')<= TO_DATE(SYSDATE,'DD/MM/YYYY') AND TO_DATE(PRXMGR.END_DATE,'DD/MM/YYYY') >=TO_DATE(SYSDATE,'DD/MM/YYYY'))"+ ")"
//				+ " AND FNA.FNA_ID = (SELECT MAX(FNA_ID) FROM FNA_DETAILS WHERE CUST_ID = FNA.CUST_ID)";
				+"  AND  UPPER(FNA.NTUC_CASE_STATUS) ='APPROVED'"
									
				;
		
		
		String strWhrConditionNonNtuc  = "  WHERE "
//				+ "	 FNA.MGR_APPROVE_STATUS IS NULL AND "//Oct_2018
				+ "  ADVSTF.MANAGER_ID = ADVSTFMGR.ADVSTF_ID"
				+ "  AND ADVSTF.ADVSTF_ID = FNA.ADVSTF_ID "
				+ "  AND CUST.CUSTID = FNA.CUST_ID "
				+ "  AND FNA.FNA_ID = FSSD.FNA_ID "
				+ "  AND FNA.MGR_EMAIL_SENT_FLG ='Y'"
//				+ "  AND FNA.NTUC_POLICY_ID  IS NOT NULL"
				+ "  AND (FNA.MGR_ID='"+strLogAdvStfId+"'"
				+ "	 OR FNA.MGR_ID IN (SELECT ADVSTF_ID FROM ADVSTF_PROXY_MGR_DETS PRXMGR "
									+ "	 WHERE MANAGER_ID ='"+strLogAdvStfId+"' "
									+ " AND TO_DATE(PRXMGR.START_DATE,'DD/MM/YYYY')<= TO_DATE(SYSDATE,'DD/MM/YYYY') AND TO_DATE(PRXMGR.END_DATE,'DD/MM/YYYY') >=TO_DATE(SYSDATE,'DD/MM/YYYY'))"+ ")"
//				+ " AND FNA.FNA_ID = (SELECT MAX(FNA_ID) FROM FNA_DETAILS WHERE CUST_ID = FNA.CUST_ID)";
//				+ "  AND FNA.FNA_ID NOT IN (SELECT FNA_ID FROM FNA_RECOM_PRDTPLAN_DET WHERE RECOM_PP_PRIN ='NTUC Income')"
//				+ "  AND FNA.NTUC_CASE_ID  IS NULL"	
//				+ "   AND FNA.NTUC_CASE_STATUS IS NULL "
//				+ " AND (UPPER(FNA.NTUC_CASE_STATUS) != 'APPROVED' OR FNA.NTUC_CASE_STATUS IS NULL) "
				+" AND ( UPPER(FNA.NTUC_CASE_STATUS) NOT IN ( 'SUBMITTED' ,'APPROVED') OR FNA.NTUC_CASE_STATUS IS NULL )"
				;
		
		
		String strOrderBy = " ORDER BY 5,3,1 DESC ";
		String QRY = EKYCQuery.SQL_FNA_APPROVAL_ACSLVL_QRY + strWhrCondition +" UNION " + EKYCQuery.SQL_FNA_APPROVAL_ACSLVL_NONNTUC_QRY + strWhrConditionNonNtuc + strOrderBy;


		fnaDetList = dbDTO.searchByNativeSQLQuery(QRY);






	}catch(HibernateException e){
		kyclog.info("----------> getFnaDetails " , e);
	}
	return fnaDetList;
}

public List loadAdminFnaDetails(DBIntf dbDTO) throws HibernateException{
	List fnaDetList = new ArrayList();
	try{


//		String strQueryParam = "SELECT FNA.FNA_ID,FNA.CUST_ID,FNA.ADVSTF_ID,ADVSTF.ADVSTF_NAME,CUST.CUST_NAME,FNA.MGR_APPROVE_STATUS, FNA.MGR_APPROVE_DATE,"+
//				"FNA.MGR_STATUS_REMARKS, FNA.ADMIN_APPROVE_STATUS, FNA.ADMIN_APPROVE_DATE, FNA.ADMIN_REMARKS, FNA.COMP_APPROVE_STATUS,"+
//				"FNA.COMP_APPROVE_DATE,FNA.COMP_REMARKS,FNA.NTUC_POLICY_ID,FNA.NTUC_POL_NUM,ADVSTF.EMAIL_ID  FROM FNA_DETAILS FNA,ADVISER_STAFF ADVSTF,CUSTOMER_DETAILS CUST  WHERE  (UPPER(FNA.MGR_APPROVE_STATUS) = UPPER('APPROVE') OR FNA.MGR_APPROVE_STATUS is NOT NULL )  AND "+
//				" FNA.MGR_APPROVE_STATUS <> 'REJECT' AND (UPPER(FNA.ADMIN_APPROVE_STATUS) <> UPPER('APPROVE') OR (FNA.ADMIN_APPROVE_STATUS is null)) AND ADVSTF.ADVSTF_ID = FNA.ADVSTF_ID  AND CUST.CUSTID = FNA.CUST_ID  ";
//
//		fnaDetList = dbDTO.searchByNativeSQLQuery(strQueryParam);


		String strWhrCondition = " WHERE "
//				+ " FNA.MGR_APPROVE_STATUS IS NOT NULL  AND "//Oct_2018
//				+ " FNA.ADMIN_APPROVE_STATUS  IS NULL AND "//Oct_2018
//				+ " FNA.COMP_APPROVE_STATUS IS NULL AND "//Oct_2018
				+ " ADVSTF.MANAGER_ID = ADVSTFMGR.ADVSTF_ID"
				+ " AND ADVSTF.ADVSTF_ID = FNA.ADVSTF_ID "
				+ "  AND advstf.advstf_id = advcode.ADVSTF_ID (+)"
				+ "  AND advcode.PRIN_ID='PRIN000065'"
				+ " AND CUST.CUSTID= FNA.CUST_ID"
				+ " AND FNA.FNA_ID = FSSD.FNA_ID "
				+ " AND FNA.MGR_EMAIL_SENT_FLG ='Y'"//Oct_2018
//				+ " AND FNA.NTUC_POLICY_ID  IS NOT NULL "//Oct_2018
				+ "  AND FNA.NTUC_CASE_ID  IS NOT NULL"
//				+ " AND FNA.FNA_ID = (SELECT MAX(FNA_ID) FROM FNA_DETAILS WHERE CUST_ID = FNA.CUST_ID)"
				+"   AND   UPPER(FNA.NTUC_CASE_STATUS) ='APPROVED' "
				;
		
		
		String strWhrConditionNoNtuc = " WHERE "
//				+ " FNA.MGR_APPROVE_STATUS IS NOT NULL  AND "//Oct_2018
//				+ " FNA.ADMIN_APPROVE_STATUS  IS NULL AND "//Oct_2018
//				+ " FNA.COMP_APPROVE_STATUS IS NULL AND "//Oct_2018
				+ " ADVSTF.MANAGER_ID = ADVSTFMGR.ADVSTF_ID"
				+ " AND ADVSTF.ADVSTF_ID = FNA.ADVSTF_ID "
				+ " AND CUST.CUSTID= FNA.CUST_ID"
				+ " AND FNA.FNA_ID = FSSD.FNA_ID "
				+ " AND FNA.MGR_EMAIL_SENT_FLG ='Y'"//Oct_2018
//				+ " AND FNA.NTUC_POLICY_ID  IS NOT NULL "//Oct_2018
//				+ " AND FNA.FNA_ID = (SELECT MAX(FNA_ID) FROM FNA_DETAILS WHERE CUST_ID = FNA.CUST_ID)"
//				+ "  AND FNA.FNA_ID NOT IN (SELECT FNA_ID FROM FNA_RECOM_PRDTPLAN_DET WHERE RECOM_PP_PRIN ='NTUC Income')"
//				+ "  AND FNA.NTUC_CASE_ID  IS NULL"	
//				+ "   AND FNA.NTUC_CASE_STATUS IS NULL "+
+" AND (UPPER(FNA.NTUC_CASE_STATUS) != 'APPROVED' OR FNA.NTUC_CASE_STATUS IS NULL)"
;
				
		
		
		String strOrderBy = " ORDER BY 5,3,1 DESC ";
		String QRY = EKYCQuery.SQL_FNA_APPROVAL_ACSLVL_QRY + strWhrCondition + " UNION "+EKYCQuery.SQL_FNA_APPROVAL_ACSLVL_NONNTUC_QRY+ strWhrConditionNoNtuc + strOrderBy; 

		fnaDetList = dbDTO.searchByNativeSQLQuery(QRY);




	}catch(HibernateException e){
		kyclog.info("----------> getFnaDetails " , e);
	}
	return fnaDetList;
}

public List loadComplianceFnaDetails(DBIntf dbDTO) throws HibernateException{
	List fnaDetList = new ArrayList();
	try{


//		String strQueryParam = "SELECT FNA.FNA_ID,FNA.CUST_ID,FNA.ADVSTF_ID,ADVSTF.ADVSTF_NAME,CUST.CUST_NAME,FNA.MGR_APPROVE_STATUS, FNA.MGR_APPROVE_DATE,"+
//				"FNA.MGR_STATUS_REMARKS, FNA.ADMIN_APPROVE_STATUS, FNA.ADMIN_APPROVE_DATE, FNA.ADMIN_REMARKS, FNA.COMP_APPROVE_STATUS,"+
//				"FNA.COMP_APPROVE_DATE,FNA.COMP_REMARKS,FNA.NTUC_POLICY_ID,FNA.NTUC_POL_NUM,ADVSTF.EMAIL_ID FROM FNA_DETAILS FNA,ADVISER_STAFF ADVSTF,CUSTOMER_DETAILS CUST  WHERE  (UPPER(FNA.ADMIN_APPROVE_STATUS) = UPPER('APPROVE') OR FNA.ADMIN_APPROVE_STATUS IS NOT NULL)  AND "+
//				" FNA.ADMIN_APPROVE_STATUS <> 'REJECT' AND (FNA.COMP_APPROVE_STATUS <> 'APPROVE' OR (FNA.COMP_APPROVE_STATUS is null)) AND ADVSTF.ADVSTF_ID = FNA.ADVSTF_ID  AND CUST.CUSTID = FNA.CUST_ID  ";
//
//		fnaDetList = dbDTO.searchByNativeSQLQuery(strQueryParam);

		/*String strWhrCondition = " WHERE "
				+ " FNA.MGR_APPROVE_STATUS IS NOT NULL "
				+ " AND FNA.ADMIN_APPROVE_STATUS IS NOT NULL"
				+ " AND FNA.COMP_APPROVE_STATUS  IS NULL"
				+ " AND ADVSTF.MANAGER_ID = ADVSTFMGR.ADVSTF_ID"
				+ " AND ADVSTF.ADVSTF_ID = FNA.ADVSTF_ID"
				+ " AND CUST.CUSTID = FNA.CUST_ID"
				+ " AND FNA.FNA_ID = FSSD.FNA_ID"
				+ " AND FNA.NTUC_POLICY_ID  IS NOT NULL";*/

		String strWhrCondition = " WHERE "
//				+ " FNA.MGR_APPROVE_STATUS IS NOT NULL "//Oct_2018
//				+ " AND FNA.ADMIN_APPROVE_STATUS IS NOT NULL"//Oct_2018
//				+ " AND FNA.COMP_APPROVE_STATUS  IS NULL AND " //Oct_2018
//				+ " (upper(FNA.COMP_APPROVE_STATUS) != 'APPROVE'  OR fna.COMP_APPROVE_STATUS is null) AND"
				+ " ADVSTF.MANAGER_ID = ADVSTFMGR.ADVSTF_ID"
				+ " AND ADVSTF.ADVSTF_ID = FNA.ADVSTF_ID"
				+ " AND advstf.advstf_id = advcode.ADVSTF_ID (+)"
				+ " AND advcode.PRIN_ID='PRIN000065'"
				+ " AND CUST.CUSTID = FNA.CUST_ID"
				+ " AND FNA.FNA_ID = FSSD.FNA_ID "
				+ " AND FNA.MGR_EMAIL_SENT_FLG ='Y'"//Oct_2018
//				+ " AND FNA.NTUC_POLICY_ID  IS NOT NULL"//Oct_2018
				+ "  AND FNA.NTUC_CASE_ID  IS NOT NULL"
				+ " AND FNA.NTUC_CASE_STATUS IS NOT NULL AND UPPER(FNA.NTUC_CASE_STATUS) ='SUBMITTED' "
//				+ " AND FNA.FNA_ID = (SELECT MAX(FNA_ID) FROM FNA_DETAILS WHERE CUST_ID = FNA.CUST_ID)";
				+"   AND   UPPER(FNA.NTUC_CASE_STATUS) ='APPROVED' "
				;
		
		
		String strWhrConditionNonNtuc = " WHERE "
//				+ " FNA.MGR_APPROVE_STATUS IS NOT NULL "//Oct_2018
//				+ " AND FNA.ADMIN_APPROVE_STATUS IS NOT NULL"//Oct_2018
//				+ " AND FNA.COMP_APPROVE_STATUS  IS NULL AND " //Oct_2018
//				+ " (upper(FNA.COMP_APPROVE_STATUS) != 'APPROVE'  OR fna.COMP_APPROVE_STATUS is null) AND"
				+ " ADVSTF.MANAGER_ID = ADVSTFMGR.ADVSTF_ID"
				+ " AND ADVSTF.ADVSTF_ID = FNA.ADVSTF_ID"
				+ " AND CUST.CUSTID = FNA.CUST_ID"
				+ " AND FNA.FNA_ID = FSSD.FNA_ID "
				+ " AND FNA.MGR_EMAIL_SENT_FLG ='Y'"//Oct_2018
//				+ " AND FNA.NTUC_POLICY_ID  IS NOT NULL"//Oct_2018
//				+ " AND FNA.FNA_ID = (SELECT MAX(FNA_ID) FROM FNA_DETAILS WHERE CUST_ID = FNA.CUST_ID)";
//				+ "  AND FNA.FNA_ID NOT IN (SELECT FNA_ID FROM FNA_RECOM_PRDTPLAN_DET WHERE RECOM_PP_PRIN ='NTUC Income')"
//				+ "  AND FNA.NTUC_CASE_ID  IS NULL"	
//				+ "   AND FNA.NTUC_CASE_STATUS IS NULL "
+ " AND (UPPER(FNA.NTUC_CASE_STATUS) != 'APPROVED' OR FNA.NTUC_CASE_STATUS IS NULL) "
;
				


		String strOrderBy = " ORDER BY 5,3,1 DESC ";
		String QRY = EKYCQuery.SQL_FNA_APPROVAL_ACSLVL_QRY + strWhrCondition + " UNION "+EKYCQuery.SQL_FNA_APPROVAL_ACSLVL_NONNTUC_QRY+ strWhrConditionNonNtuc + strOrderBy; 

		
		fnaDetList = dbDTO.searchByNativeSQLQuery(QRY);




	}catch(HibernateException e){
		kyclog.info("----------> getFnaDetails " , e);
	}
	return fnaDetList;
	}

public List loadCompAndAdminFnaDets(DBIntf dbDTO) throws HibernateException{
	List fnaDetList = new ArrayList();
	try{



		String strWhrCondition = " WHERE "
//				+ " (upper(FNA.COMP_APPROVE_STATUS) != 'APPROVE' OR fna.COMP_APPROVE_STATUS is null) AND "
				+ " ADVSTF.MANAGER_ID = ADVSTFMGR.ADVSTF_ID"
				+ " AND ADVSTF.ADVSTF_ID = FNA.ADVSTF_ID"
				+ " AND advstf.advstf_id = advcode.ADVSTF_ID (+)"
				+ " AND advcode.PRIN_ID='PRIN000065'"
				+ " AND CUST.CUSTID = FNA.CUST_ID"
				+ " AND FNA.FNA_ID = FSSD.FNA_ID "
				+ " AND FNA.MGR_EMAIL_SENT_FLG ='Y'"//Oct_2018
//				+ " AND FNA.NTUC_POLICY_ID  IS NOT NULL"//Oct_2018
				+ "  AND FNA.NTUC_CASE_ID  IS NOT NULL"
				+ " AND FNA.NTUC_CASE_STATUS IS NOT NULL AND UPPER(FNA.NTUC_CASE_STATUS) ='APPROVED' "
//				+ " AND FNA.FNA_ID = (SELECT MAX(FNA_ID) FROM FNA_DETAILS WHERE CUST_ID = FNA.CUST_ID)";
				;
		
		String strWhrConditionNonNtuc = " WHERE "
//				+ " (upper(FNA.COMP_APPROVE_STATUS) != 'APPROVE'  OR fna.COMP_APPROVE_STATUS is null) AND"
				+ " ADVSTF.MANAGER_ID = ADVSTFMGR.ADVSTF_ID"
				+ " AND ADVSTF.ADVSTF_ID = FNA.ADVSTF_ID"
				+ " AND CUST.CUSTID = FNA.CUST_ID"
				+ " AND FNA.FNA_ID = FSSD.FNA_ID " 
				+ " AND FNA.MGR_EMAIL_SENT_FLG ='Y'"//Oct_2018
//				+ " AND FNA.NTUC_POLICY_ID  IS NOT NULL"//Oct_2018
//				+ " AND FNA.FNA_ID = (SELECT MAX(FNA_ID) FROM FNA_DETAILS WHERE CUST_ID = FNA.CUST_ID)";
//				+ "  AND FNA.FNA_ID NOT IN (SELECT FNA_ID FROM FNA_RECOM_PRDTPLAN_DET WHERE RECOM_PP_PRIN ='NTUC Income')";
//				+ "  AND FNA.NTUC_CASE_ID  IS NULL"
+ " AND (UPPER(FNA.NTUC_CASE_STATUS) != 'APPROVED' OR FNA.NTUC_CASE_STATUS IS NULL) "
				;


		String strWhrConditionAdmin = " WHERE "
//				+ " FNA.MGR_APPROVE_STATUS IS NOT NULL  AND "//Oct_2018
//				+ " FNA.ADMIN_APPROVE_STATUS  IS NULL AND "//Oct_2018
//				+ " FNA.COMP_APPROVE_STATUS IS NOT NULL AND "//Oct_2018
				+ " ADVSTF.MANAGER_ID = ADVSTFMGR.ADVSTF_ID"
				+ " AND ADVSTF.ADVSTF_ID = FNA.ADVSTF_ID "
				+ "  AND advstf.advstf_id = advcode.ADVSTF_ID (+)"
				+ "  AND advcode.PRIN_ID='PRIN000065'"
				+ " AND CUST.CUSTID= FNA.CUST_ID"
				+ " AND FNA.FNA_ID = FSSD.FNA_ID "
				+ " AND FNA.MGR_EMAIL_SENT_FLG ='Y'"//Oct_2018
//				+ " AND FNA.NTUC_POLICY_ID  IS NOT NULL "//Oct_2018
				+ "  AND FNA.NTUC_CASE_ID  IS NOT NULL"
//				+ " AND FNA.FNA_ID = (SELECT MAX(FNA_ID) FROM FNA_DETAILS WHERE CUST_ID = FNA.CUST_ID)"
				;
		
		String strWhrConditionAdminNonNtuc = " WHERE "
//				+ " FNA.MGR_APPROVE_STATUS IS NOT NULL  AND "//Oct_2018
//				+ " FNA.ADMIN_APPROVE_STATUS  IS NULL AND "//Oct_2018
//				+ " FNA.COMP_APPROVE_STATUS IS NOT NULL AND "//Oct_2018
				+ " ADVSTF.MANAGER_ID = ADVSTFMGR.ADVSTF_ID"
				+ " AND ADVSTF.ADVSTF_ID = FNA.ADVSTF_ID "
				+ " AND CUST.CUSTID= FNA.CUST_ID"
				+ " AND FNA.FNA_ID = FSSD.FNA_ID "
				+ " AND FNA.MGR_EMAIL_SENT_FLG ='Y'"//Oct_2018
//				+ " AND FNA.NTUC_POLICY_ID  IS NOT NULL "//Oct_2018
//				+ "  AND FNA.NTUC_CASE_ID  IS NOT NULL"
//				+ " AND FNA.FNA_ID = (SELECT MAX(FNA_ID) FROM FNA_DETAILS WHERE CUST_ID = FNA.CUST_ID)"
//				+ "  AND FNA.FNA_ID NOT IN (SELECT FNA_ID FROM FNA_RECOM_PRDTPLAN_DET WHERE RECOM_PP_PRIN ='NTUC Income')"
//				+ "  AND FNA.NTUC_CASE_ID  IS NULL"	
//				+ "   AND FNA.NTUC_CASE_STATUS IS NULL "
+ " AND (UPPER(FNA.NTUC_CASE_STATUS) != 'APPROVED' OR FNA.NTUC_CASE_STATUS IS NULL) "
				;
				
		
		
		String strOrderBy = " ORDER BY 5,3,1 DESC ";
		String QRY = EKYCQuery.SQL_FNA_APPROVAL_ACSLVL_QRY + strWhrCondition + " UNION "+EKYCQuery.SQL_FNA_APPROVAL_ACSLVL_NONNTUC_QRY+ strWhrConditionNonNtuc + " UNION " +
				EKYCQuery.SQL_FNA_APPROVAL_ACSLVL_QRY + strWhrConditionAdmin + " UNION "+EKYCQuery.SQL_FNA_APPROVAL_ACSLVL_NONNTUC_QRY+ strWhrConditionAdminNonNtuc + strOrderBy; 

		

		fnaDetList = dbDTO.searchByNativeSQLQuery(QRY);




	}catch(HibernateException e){
		kyclog.info("----------> getFnaDetails " , e);
	}
	return fnaDetList;
	}
	 



public List getAdvtoMngrKycSendList(DBIntf dbDTO,String strLogAdvStfId) throws HibernateException{
	List fnaDetList = new ArrayList();
	try{
		if(KycUtils.checkNullVal(dbDTO)){
            ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
			dbDTO = (DBIntf) ctx.getBean("DBDAO");
		}
          String strWhrConditionAdvtoMngrKycSent  = "WHERE  "
				+ "ADVSTF.MANAGER_ID = ADVSTFMGR.ADVSTF_ID "
				+ "AND ADVSTF.ADVSTF_ID = FNA.ADVSTF_ID "
				+ "AND CUST.CUSTID = FNA.CUST_ID "
				+ "AND FNA.FNA_ID=FSSD.FNA_ID AND FNA.MGR_EMAIL_SENT_FLG ='Y' "
				+ "AND FNA.ADVSTF_ID ='"+strLogAdvStfId+"' AND FNA.KYC_SENT_STATUS= 'YES'  ";
			String qry = 	EKYCQuery.SQL_FNA_APPROVAL_ACSLVL_NONNTUC_QRY + strWhrConditionAdvtoMngrKycSent	;	
			
		
        fnaDetList = dbDTO.searchByNativeSQLQuery(EKYCQuery.SQL_FNA_APPROVAL_ACSLVL_NONNTUC_QRY + strWhrConditionAdvtoMngrKycSent);
         }catch(HibernateException e){
		kyclog.info("----------> getManagerFnaDetails " , e);
	}
	return fnaDetList;
}

 //poovathi add on 05-07-2021

   /* public List getAllPendReqMsgByFnaId(DBIntf dbDTO, String strFnaId) {
    
    	List fnaPendReqList = new ArrayList();
    	
    	String STR_HQL_PEND_REQ_MSG_BYFNAID = " SELECT * FROM  FNA_PENDING_REQUEST WHERE FNA_ID= '"+strFnaId+"'";
    
    	fnaPendReqList = dbDTO.searchByNativeSQLQuery(STR_HQL_PEND_REQ_MSG_BYFNAID);
    	
    	//System.out.println("FnaId"+strFnaId+"\n"+fnaPendReqList);
    
    	return fnaPendReqList;
    }*/
  
/*public List getAllPendReqMsgByFnaId(DBIntf dbDTO, String strFnaId) {

	List fnaList = new ArrayList();

	fnaList = dbDTO.searchList(EKYCQuery.STR_HQL_PEND_REQ_MSG_BYFNAID, strFnaId);
	
	System.out.println(fnaList);

	return fnaList;
}*/

public List<FnaPendingRequest> getAllPendReqMsgByFnaId(DBIntf dbDTO, String strFnaId) {

	List<FnaPendingRequest> fnaPendReqmsgList = new ArrayList<FnaPendingRequest>();
	String STR_HQL_PEND_REQ_MSG_BYFNAID = " SELECT * FROM  FNA_PENDING_REQUEST WHERE FNA_ID= '"+strFnaId+"' ORDER BY REQID DESC";
	fnaPendReqmsgList = (List<FnaPendingRequest>) dbDTO.searchByNativeSQLQuery(STR_HQL_PEND_REQ_MSG_BYFNAID);
	return fnaPendReqmsgList;
}


}