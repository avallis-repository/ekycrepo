package com.avallis.ekyc.db;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;

import com.avallis.ekyc.dbinterfaces.DBIntf;
import com.avallis.ekyc.dto.FnaDetails;
import com.avallis.ekyc.dto.FnaExpenditureDets;
import com.avallis.ekyc.dto.FnaSelfspouseDets;


public class KycDb extends HibernateDaoSupport{


	static Logger kyclog = Logger.getLogger(KycDb.class);

	public List getFnaArchDets(DBIntf dto, String strFnaId)throws HibernateException {

		Map<String,Class> entityObj =  new LinkedHashMap<String, Class>();

		String
		HQL_GET_FNA_DET_ARCH = "select {fna.*},{ssd.*},{expd.*}"

		+ " from FNA_DETAILS fna  "
		+ " , FNA_SELFSPOUSE_DETS ssd "
		+ " , FNA_EXPENDITURE_DETS expd "

		+ "  where fna.FNA_ID = ssd.FNA_ID and "
		+ "	 fna.FNA_ID = expd.FNA_ID(+) and ";
		
		List fnaDetTabElems = new ArrayList();

		entityObj.put("fna", FnaDetails.class);
		entityObj.put("ssd", FnaSelfspouseDets.class);
		entityObj.put("expd", FnaExpenditureDets.class);

		try {

			String strWhrCls = " fna.FNA_ID ='"+strFnaId+"'";

			fnaDetTabElems = dto.searchListByJoin(HQL_GET_FNA_DET_ARCH + strWhrCls,entityObj);

		} catch (HibernateException he) {
//			he.printStackTrace();
			kyclog.error("error in getFnaArchDets ->",he);

		}
		return fnaDetTabElems;
	}

	public List getFnaArchDepntDets(DBIntf dto, String strFnaId)
			throws HibernateException {

		List tabElements = new ArrayList();
		String HQL_GET_FNA_DEPNTDET_ARCH = " from com.avallis.ekyc.dto.FnaDependantDets dep  ";

		try {

			String strWhrCls = "WHERE dep.fnaDetails ='"+strFnaId+"'";
			tabElements = dto.searchListByQuery(HQL_GET_FNA_DEPNTDET_ARCH + strWhrCls);

		} catch (HibernateException he) {
//			he.printStackTrace();
			kyclog.error("------------>getFnaArchDepntDets ",he);
		}
		return tabElements;
	}



	public List getFnaArchAnnlExptDet(DBIntf dto, String strFnaId)
			throws HibernateException {

		List tabElements = new ArrayList();
		String HQL_GET_FNA_ANNLEXPT_ARCH = " from com.avallis.ekyc.dto.FnaExpenditureDets exp  ";

		try {

			String strWhrCls = "WHERE exp.fnaDetails ='"+strFnaId+"'";
			tabElements = dto.searchListByQuery(HQL_GET_FNA_ANNLEXPT_ARCH + strWhrCls);

		} catch (HibernateException he) {
//			he.printStackTrace();
			kyclog.error("------------>getFnaArchAnnlExptDet ",he);
		} finally {  }
		return tabElements;
	}




	public List getFnaArchOthPersDet(DBIntf dto, String strFnaId)
			throws HibernateException {

		List tabElements = new ArrayList();
		String HQL_GET_FNA_OTHPERSDET_ARCH = " from com.avallis.ekyc.dto.FnaOtherpersonDets op  ";

		try {

			String strWhrCls = "WHERE op.fnaDetails ='"+strFnaId+"'";
			tabElements = dto.searchListByQuery(HQL_GET_FNA_OTHPERSDET_ARCH + strWhrCls);

		} catch (HibernateException he) {
//			he.printStackTrace();
			kyclog.error("------------>getFnaArchOthPersDet ",he);
		} finally {  }
		return tabElements;
	}



	public List getFnaArchRecProdPlanDet(DBIntf dto, String strFnaId)
			throws HibernateException {

		List tabElements = new ArrayList();

//		String HQL_GET_FNA_RECPRODDET_ARCH = " from com.avallis.ekyc.dto.FnaRecomPrdtplanDet op  order by upper(op.recomPpName),upper(op.recomPpPrin),upper(op.recomPpPlan),upper(op.recomPpBasrid) ";
		String HQL_GET_FNA_RECPRODDET_ARCH = " from com.avallis.ekyc.dto.FnaRecomPrdtplanDet op  ";
		try {

			String strWhrCls = " WHERE op.fnaDetails ='"+strFnaId+"'";
			tabElements = dto.searchListByQuery(HQL_GET_FNA_RECPRODDET_ARCH + strWhrCls + " order by upper(op.recomPpId)");

		} catch (HibernateException he) {
//			he.printStackTrace();
			kyclog.error("------------>getFnaArchRecProdPlanDet ",he);
		} finally {  }
		return tabElements;
	}

	public List getFnaFatcaTaxDet(DBIntf dto, String strFnaId)
			throws HibernateException {

		List tabElements = new ArrayList();



		String HQL_GET_FNA_FATCATAXDET_ARCH = " from com.avallis.ekyc.dto.FnaFatcaTaxdet op  ";


		try {

			String strWhrCls = "WHERE op.fnaDetails ='"+strFnaId+"'";
			tabElements = dto.searchListByQuery(HQL_GET_FNA_FATCATAXDET_ARCH + strWhrCls);

		} catch (HibernateException he) {
//			he.printStackTrace();
			kyclog.error("------------>getFnaFatcaTaxDet ",he);
		} finally {  }
		return tabElements;
	}

	public List getFnaArchRecFundDet(DBIntf dto, String strFnaId)
			throws HibernateException {

		List tabElements = new ArrayList();
		String HQL_GET_FNA_RECFUNDDET_ARCH = " from com.avallis.ekyc.dto.FnaRecomFundDet op  ";
		try {

			String strWhrCls = " WHERE op.fnaDetails ='"+strFnaId+"'";
			tabElements = dto.searchListByQuery(HQL_GET_FNA_RECFUNDDET_ARCH + strWhrCls + " order by upper(op.recomFfPrin),upper(op.recomFfPlan),upper(op.recomFfBasrid) ");

		} catch (HibernateException he) {
//			he.printStackTrace();
			kyclog.error("------------>getFnaArchRecFundDet ",he);
		} finally {  }
		return tabElements;
	}


	public List getFnaArchSwRepPlanDet(DBIntf dto, String strFnaId)
			throws HibernateException {

		List tabElements = new ArrayList();
//		String HQL_GET_FNA_SWREPPLANDET_ARCH = " from com.avallis.ekyc.dto.FnaSwtchrepPlanDet op order by upper(op.swrepPpName),upper(op.swrepPpBasrid) ";

		String HQL_GET_FNA_SWREPPLANDET_ARCH = " from com.avallis.ekyc.dto.FnaSwtchrepPlanDet op ";

		try {

			String strWhrCls = " WHERE op.fnaDetails ='"+strFnaId+"'";
			tabElements = dto.searchListByQuery(HQL_GET_FNA_SWREPPLANDET_ARCH + strWhrCls + " order by upper(op.swrepPpId)  ");

		} catch (HibernateException he) {
//			he.printStackTrace();
			kyclog.error("------------>getFnaArchSwRepPlanDet ",he);
		} finally {  }
		return tabElements;
	}

	public List getFnaArchSwRepFundDet(DBIntf dto, String strFnaId)
			throws HibernateException {

		List tabElements = new ArrayList();
		String HQL_GET_FNA_SWREPFUNDDET_ARCH = " from com.avallis.ekyc.dto.FnaSwtchrepFundDet op  ";
		try {

			String strWhrCls = "WHERE op.fnaDetails ='"+strFnaId+"'";
			tabElements = dto.searchListByQuery(HQL_GET_FNA_SWREPFUNDDET_ARCH + strWhrCls);

		} catch (HibernateException he) {
//			he.printStackTrace();
			kyclog.error("------------>getFnaArchSwRepFundDet ",he);
		} finally {  }
		return tabElements;
	}
	 
		
}

