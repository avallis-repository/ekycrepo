package com.avallis.ekyc.db;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.context.ApplicationContext;

import com.avallis.ekyc.dbinterfaces.DBImplementation;
import com.avallis.ekyc.dbinterfaces.DBIntf;
import com.avallis.ekyc.dto.AdviserStaff;
import com.avallis.ekyc.dto.Login;
import com.avallis.ekyc.dto.Loginprvlg;
import com.avallis.ekyc.dto.MasterAttachCateg;
import com.avallis.ekyc.dto.MasterCustomerStatus;
import com.avallis.ekyc.dto.MasterDesignation;
import com.avallis.ekyc.dto.MasterPrincipal;
import com.avallis.ekyc.utils.ApplicationContextUtils;
import com.avallis.ekyc.utils.EKYCQuery;
import com.avallis.ekyc.utils.KycConst;

public class FpmsDB {
	
	static Logger kyclog = Logger.getLogger(FpmsDB.class);
	
	public  boolean chkValidDB(){
		
		ApplicationContext appCtx = ApplicationContextUtils
				.getApplicationContext();
		DBImplementation dbDTO = (DBImplementation) appCtx
				.getBean(KycConst.DB_BEAN);
		
		try{
			dbDTO.ChkValidDB("SELECT SYSDATE FROM DUAL");
		}catch(Exception e){
			return false;
		}		
		return true;		
	}
	
//	 * Login and Access Level Related Sections
	public List validateLogin(String ... userparams) {
		List userList = new ArrayList(); 
		String SQL_FPMS_VALIDATE_USER = "SELECT FPU.USER_ID,FPU.USERNAME,FPU.STAFF_ID,"
				+ "ADV.ADVSTF_NAME,ADV.ADVSTF_INITIALS,ADV.STAFF_TYPE,ADV.DESIGNATION,"
				+ "ADV.MANAGER_ID,MGR.ADVSTF_NAME MANAGERNAME,SYSDATE CURRDATE,DESG.MGR_FLG,"
				+ " '1' PASSWORD,FPU.DISTRIBUTOR_ID,FPU.DISTRIBUTOR_NAME,ADV.EMAIL_ID"
				+ " FROM "
				+"FPUSER FPU,"
				+"ADVISER_STAFF ADV,"
				+"ADVISER_STAFF MGR,"
				+"MASTER_DESIGNATION DESG"
				+ " where FPU.STAFF_ID = ADV.ADVSTF_ID "
				+ " AND ADV.MANAGER_ID = MGR.ADVSTF_ID "
				+ " AND ADV.DESIG_ID = DESG.DESIG_ID "
				+ " and FPU.USER_ID = ?"
				+ " and  FPU.PASSWORD =?";
		
		

		
		ApplicationContext appCtx = ApplicationContextUtils.getApplicationContext();
		
		DBImplementation dbDTO = (DBImplementation) appCtx.getBean(KycConst.DB_BEAN);
		
		userList = dbDTO.searchNativeQueryList(SQL_FPMS_VALIDATE_USER,userparams[0],userparams[1]);
		

		return userList;

	}
	
	
	
	
	
	public List<Login> getuserRole(String strUserId) {
		
		
	ApplicationContext appCtx = ApplicationContextUtils.getApplicationContext();
		
		DBImplementation dbDTO = (DBImplementation) appCtx
				.getBean(KycConst.DB_BEAN);
		
		 String HQL_LOGIN_USER_ROLE_PRIVS =
			        " FROM com.avallis.ekyc.dto.Login AS loggin " + "LEFT JOIN FETCH loggin.selrole AS rol " +
			        " LEFT JOIN FETCH loggin.selstaffid as brok LEFT JOIN FETCH brok.masterEmploymentStatus " +
			        " left join fetch brok.masterDesignation desig where loggin.txtFldUserId = ? ";
		
		 List<Login> rolelogn=dbDTO.searchList(HQL_LOGIN_USER_ROLE_PRIVS, strUserId);
		 
		 return rolelogn;
		
	}
	
	@SuppressWarnings("unchecked")
	public List<AdviserStaff> getGloblAcessList(String strloggedAdvStaffId,String strloggedUserStaffType,String strloggedUsrDistId) {

		ApplicationContext appCtx = ApplicationContextUtils
				.getApplicationContext();
		DBImplementation dbDTO = (DBImplementation) appCtx
				.getBean(KycConst.DB_BEAN);
		
		List<AdviserStaff> advstaffList = new ArrayList<AdviserStaff>();
		String STR_GLBL_ACCESS_LVL_QRY = "select advStf.ADVSTF_ID ,advStf.ADVSTF_NAME  from ADVISER_STAFF advStf ,"
				+ " MASTER_EMPLOYMENT_STATUS EMP where "
				+ "advStf.ADVSTF_ID in ( Select ADVISERSTAFFID from ADVSTF_MANAGER"
				+ " CONNECT BY PRIOR ADVISERSTAFFID = MANAGER_ID START WITH ADVISERSTAFFID='"+strloggedAdvStaffId+"' )"
				+ "AND UPPER(ADVSTF.STAFF_TYPE)='"+strloggedUserStaffType+"' AND EMP.EMPSTATUS_ID = ADVSTF.EMPSTATUS_ID  "
				+ "AND EMP.EMPSTATUSNAME = 'ACTIVE' AND INSTR(advStf.ADVSTF_NAME,'(R)')=0 "
				+ "AND (advStf.DISTRIBUTOR_ID) = ('"+strloggedUsrDistId+"') ORDER BY 2 ";
		        advstaffList =   (List<AdviserStaff>) dbDTO.searchByNativeSQLQuery(STR_GLBL_ACCESS_LVL_QRY);
		        


		return advstaffList;
	}
	
	
	 public List srchGlblAcessLvlDB(String strAdvStfId) throws HibernateException{
		  List<MasterDesignation> desigVal = new ArrayList<MasterDesignation>();
				ApplicationContext appCtx = ApplicationContextUtils
						.getApplicationContext();
				DBImplementation dbDTO = (DBImplementation) appCtx
						.getBean(KycConst.DB_BEAN);
//			  desigVal = dbDTO.searchList(EKYCQuery.HQL_GETACESS_LEVEL_FIND_ELEMENT,strAdvStfId);
				
				
				 String  STR_GLBL_ACCESS_LVL__COMP_QRY = "select MGR_FLG,DECODE(MGR_FLG,'Y','ADVISER/STAFF,MANAGER','N','ADVISER/STAFF')"
					 		+ " from MASTER_DESIGNATION where desig_id=(SELECT CC.DESIG_ID FROM ADVISER_STAFF CC WHERE CC.ADVSTF_ID=('"+strAdvStfId+"')) ";
					 
				 desigVal =   (List<MasterDesignation>)dbDTO.searchByNativeSQLQuery(STR_GLBL_ACCESS_LVL__COMP_QRY);
			 
		
		return desigVal;
	 } // End of srchGlblAcessLvlDB 
	 
	 public List<Loginprvlg> getLoginPrvlg(String strUserId){
		 
		 String HQL_LOGIN_USER_PRIVS =
			        " FROM Loginprvlg AS logprvlg " +
			        "LEFT JOIN FETCH logprvlg.selScrfunction AS scrfunc " +
			        //if screen name not needed comment the below line
			        "LEFT JOIN FETCH scrfunc.fpscreen AS scr " +
			        "where logprvlg.seluserId = ";

		 List<Loginprvlg>  usr_prv_logn = null;

			try{
//				WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(ServletActionContext.getServletContext());
				ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
				DBIntf dbDTO = (DBIntf) ctx.getBean("DBDAO");

				usr_prv_logn=(List<Loginprvlg>) dbDTO.searchListByQuery(HQL_LOGIN_USER_PRIVS+"'"+strUserId+"'");

			}catch(HibernateException e){
				kyclog.info("----------> error in getLoginPrvlg " , e);
			}
			return usr_prv_logn;
		}
	 
	 public List<MasterDesignation> getGloblAcessListCombo(String strAdvStfId) throws HibernateException{
		 
			
			ApplicationContext appCtx = ApplicationContextUtils
					.getApplicationContext();
			DBImplementation dbDTO = (DBImplementation) appCtx
					.getBean(KycConst.DB_BEAN);
			
			List<MasterDesignation> AccessList = new ArrayList<MasterDesignation>();
		  //AccessList = dbDTO.searchByNativeSQLQuery(EKYCQuery.HQL_GETACESS_LEVEL_FIND_ELEMENT,strAdvStfId);
		  
		 String  STR_GLBL_ACCESS_LVL__COMP_QRY = "select MGR_FLG,DECODE(MGR_FLG,'Y','ADVISER/STAFF,MANAGER','N','ADVISER/STAFF')"
		 		+ " from MASTER_DESIGNATION where desig_id=(SELECT CC.DESIG_ID FROM ADVISER_STAFF CC WHERE CC.ADVSTF_ID=('"+strAdvStfId+"')) ";
		 
		 AccessList =   (List<MasterDesignation>)dbDTO.searchByNativeSQLQuery(STR_GLBL_ACCESS_LVL__COMP_QRY);

	    return AccessList;
} 
	
//	 * End of Login and Access Level Related Sections
	
	
	
	
//	 * Masters Related Sections
	
	public List<MasterPrincipal> populatePrincipal() {
		List empStatusList = new ArrayList();
		Map<String,Class> entityObj =  new LinkedHashMap<String, Class>();
		try{
//			WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(ServletActionContext.getServletContext());
			ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
			DBIntf dbDTO = (DBIntf) ctx.getBean("DBDAO");

			entityObj.put("PRIN", MasterPrincipal.class);
//			System.out.println("Entity Object"+ entityObj);
			
			String HQL_MASTERS_PRINCIPA_ALL =	"SELECT {PRIN.*} FROM MASTER_PRINCIPAL PRIN,  PROD_LOB_TRANS_DETS a,  "
					+ "MASTER_PRODUCT_LINE b  WHERE prin.PRIN_ID =a.PRIN_ID AND a.PRODUCT_LINE_ID = b.PRODUCT_LINE_ID "
					+ "AND UPPER(b.PRODUCT_LINE_MAIN) = 'LIFE INSURANCE' AND  LIFE_INS_FLAG IS NULL order by upper(prin.PRIN_NAME)";


			empStatusList = dbDTO.searchListByJoin(HQL_MASTERS_PRINCIPA_ALL,entityObj);

		}catch(HibernateException e){
			kyclog.error("----------> populatePrincipal " , e);
		}
		return empStatusList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<MasterCustomerStatus> getMastCustStatus() {

		ApplicationContext appCtx = ApplicationContextUtils
				.getApplicationContext();
		DBImplementation dbDTO = (DBImplementation) appCtx
				.getBean(KycConst.DB_BEAN);

		List<MasterCustomerStatus> clientList = new ArrayList<MasterCustomerStatus>();

		clientList =  (List<MasterCustomerStatus>) dbDTO
				.searchListByQuery(
						"from com.avallis.ekyc.dto.MasterCustomerStatus as cnt order by upper(cnt.customerStatusName) ");
		

		return clientList;
	}
	
public List<MasterAttachCateg> getattachCategList() {
		
		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);
		
        List<MasterAttachCateg> categList = new ArrayList<MasterAttachCateg>();
		categList = (List<MasterAttachCateg>) dbDTO.searchByNativeSQLQuery(EKYCQuery.STR_HQL_MASTER_ATTACH_CATEG);

		return categList;


	}
	
	
	 
	 
	 
		public List populateMasterFM() {
			List fmList = new ArrayList();
			
			String HQL_MASTER_FM_ALL =	" SELECT FM_CODE,NAME FROM SYN_FNA_FPIS_FUNDSMANAGER ORDER BY UPPER(NAME)" ;
			
			try{
//				WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(ServletActionContext.getServletContext());
				ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
				DBIntf dbDTO = (DBIntf) ctx.getBean("DBDAO");

				fmList = dbDTO.searchByNativeSQLQuery(HQL_MASTER_FM_ALL);


			}catch(HibernateException e){
				kyclog.error("----------> populateMasterFM " , e);
			}
			return fmList;
		}
	
//	 * End of Masters Related Sections
	

	
			
			 
			 
			 
			
			 
}
