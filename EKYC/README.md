22.1.1 Release (EKYC Production Release on 14 January 2022)
-------------------------------------------------------------
    1. Principal name amendments<br/>
    2. Approval screen – PDF Expand View feature
    3. Upon verification of FNA, if nothing is entered at New Purchases/Top-up Recommendation, to perform confirmation dialogue.
        (Sathish Comment:  will do in next release, since there is so many impacts by changing the flow.)
        Nor Comment: Noted for next Release
        (Nor Feedback Feature : Document Reference : Report - eKYC PVT1 for Sathish dd 08-11-2021.docx, Sent on 11th Nov By Nor, point#6.E) 
    4.Slow performance issue.
    
Source and WAR file is backup at 97: E:\FROM F DRIVE\FPMS_BCKUP\PRODUCTION DELIVERIES\FOR FPF\22.1.1_EKYCQR_PROD_14012022