<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.net.URL" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<%
URL birtUrl= new URL(request.getScheme(),"internal-ekycqr.avallis.com","/birt-viewer");

com.avallis.ekyc.utils.KycConst.BIRT_PATH = birtUrl.toString();	
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Avallis | eKYC | Approval Section</title>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
 
 <link rel="stylesheet" href="${contextPath}/vendor/bootstrap453/css/bootstrap.css">
 <link rel="stylesheet" href="${contextPath}/views/newapproval/css/normalize.min.css">
  <link rel="stylesheet" href="${contextPath}/views/newapproval/css/select2.min.css">
 <link rel="stylesheet" href="${contextPath}/views/newapproval/css/dataTables.bootstrap4.min.css">
 <link rel="stylesheet" href="${contextPath}/views/newapproval/css/ekyc-layout.css">
 <link rel="stylesheet" href="${contextPath}/views/newapproval/css/style_checkboxs.css">
 <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet" />


<link href="${contextPath}/vendor/SweetAlert2/css/sweetalert2.css">
<link href="${contextPath}/vendor/SweetAlert2/css/sweetalert2.min.css">

 
 <script>
 var seesselfName="",sessspsname ="";
 var BIRT_URL = "<%=com.avallis.ekyc.utils.KycConst.BIRT_PATH%>";
 var approvalscreen="true";
 var baseUrl="",strPageValidMsgFld="";
 var current_fna_id = '${current_fna_id}';
 </script>

<script src="${contextPath}/views/newapproval/js/jquery.min.js"></script>
<script src="${contextPath}/views/newapproval/js/popper.min.js"></script>
<script src="${contextPath}/vendor/bootstrap453/js/bootstrap.js"></script>
<script src="${contextPath}/views/newapproval/js/pdfobject.min.js"></script>
<script src="${contextPath}/views/newapproval/js/prefixfree.min.js"></script>
<script src="${contextPath}/views/newapproval/js/select2.min.js"></script>
<script src="${contextPath}/views/newapproval/js/jquery.dataTables.min.js"></script>
<script src="${contextPath}/views/newapproval/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
<script src="${contextPath}/vendor/avallis/js/Common Script.js"></script>
<script src="${contextPath}/vendor/avallis/js/Constants.js"></script>
<script src="${contextPath}/views/newapproval/js/Constants.js"></script>
<script src="${contextPath}/views/newapproval/js/commonScript.js"></script>
<script src="${contextPath}/views/newapproval/js/lookupJAXreq.js"></script>
<script src="${contextPath}/views/newapproval/js/kycApprovalScript.js"></script>
<script src="${contextPath}/vendor/avallis/js/qresign.js"></script>
 
		  <script src="${contextPath}/vendor/SweetAlert2/js/sweetalert2.min.js"></script>
		  <script src="${contextPath}/vendor/SweetAlert2/js/sweetalert2.all.js"></script>
		  <script src="${contextPath}/vendor/SweetAlert2/js/sweetalert2.all.min.js"></script>
		  <script src="${contextPath}/vendor/SweetAlert2/js/sweetalert2.js"></script>
		  <script src="https://cdn.jsdelivr.net/npm/js-cookie@rc/dist/js.cookie.min.js"></script>

		  
		
</head>	

<style>
body{
/* overflow-y : scroll; */
}

</style>	
<body id="page-top">
<div id="cover-spin"></div>
 <div id="tooltip"></div>
 
 
<form id="frmTempLogin" method="POST" action="${contextPath}/approval/loginValidate" >

	<input type="hidden" value="${KYC_APPROVE_DISTID}" name="hTxtFldDistId"/>
	<input type="hidden" value="<%=session.getAttribute("KYC_APPROVE_ADVID")%>" name="hTxtFldAdvId"/>
	<input type="hidden" value="<%=session.getAttribute("LOGGED_ADVSTFID")%>" name="txtFldLogUserAdvId"/>
	<input type="hidden" value="<%=session.getAttribute("KYC_APPROVE_USERID")%>" name="txtFldCrtdUser" id="txtFldCrtdUser"/>
	<input type="hidden" value="<%=session.getAttribute("KYC_APPROVE_MGRACSFLG")%>" name="hTxtFldMgrAcsFlg"/>
	<input type="hidden" value="<%=session.getAttribute("MANAGER_ACCESSFLG")%>" name="mgrAcsFlg"/>
	<input type="hidden" value="<%=session.getAttribute("KYC_APPROVE_FNAID")%>" name="hTxtFldFnaId" id="hTxtFldFnaId"/>
	<input type="hidden" id="hTxtFldReload" name="hTxtFldReload"/>
	<input type="hidden"  id="txtFldFnaId"/>
	<input type="hidden" id="txtFldCustName"/>
	<input type="hidden" id="txtFldCurFnaId"/>
	<input type="hidden" id="txtFldAdvEmailId"/>
</form>

	<div id="wrapper" style="background-color: #ecf0f1;">

		<div id="content-wrapper" class="d-flex flex-column">
			<div id="content">
				<nav class="navbar navbar-expand navbar-light bg-navbar topbar mb-4 static-top" id="navbar-bg" style="width: 99.8%;">
	       <div>
	        <h5 class="sidebar-brand d-flex align-items-center justify-content-center">
               <span class="sidebar-brand-icon bg-white" style="border-radius:10px">
                  <img src="${contextPath}/vendor/avallis/img/logo/logo.png" style="width: 85px;">
               </span>
               <span class="sidebar-brand-text mx-1 white">eKYC Approval</span>
             </h5>
             </div>
             
	<ul class="navbar-nav ml-auto navbar-fixed-top" >
            <li class="nav-item dropdown no-arrow d-none">
                        <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-search fa-fw"></i>
                        </a>
                       <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown"><!--   -->
                           <form class="navbar-search">
                              <div class="input-group">
                                 <input type="text" class="form-control bg-light border-1 small" placeholder="Search for client details?" aria-label="Search" aria-describedby="basic-addon2" style="border-color: #3f51b5;">
                                 <div class="input-group-append">
                                    <button class="btn btn-sm btn-bs3-prime" type="button">
                                    <i class="fas fa-search fa-sm"></i>
                                    </button>
                                 </div>
                              </div>
                           </form>
                        </div>
                     </li>
                     <div class="topbar-divider d-none d-sm-block"></div>
                     <li class="nav-item dropdown no-arrow">
                     <!-- poovathi commented on 14-06-2021 -->
                        <%-- <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img class="img-profile rounded-circle" src="${contextPath}/vendor/avallis/img/users/Salim.png" style="max-width: 60px" 
                        onerror="this.onerror=null;this.src='vendor/avallis/img/users/client.png';">
                         <span class="ml-2 d-none d-lg-inline text-white small">Logged User : <span id="spanCrtdUser"></span></span>
                        </a> --%>
                        
                        <!-- poovathi Add on 14-06-2021 -->
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img class="img-profile rounded-circle" src="${contextPath}/vendor/avallis/img/users/${LOGGED_USER_INFO.LOGGED_USER_ID}.png" 
                        style="max-width: 60px" onerror="this.onerror=null;this.src='${contextPath}/vendor/avallis/img/users/client.png';">
                         <span class="ml-2 d-none d-lg-inline text-white small">Logged User : <span id="spanCrtdUser">${LOGGED_USER_INFO.LOGGED_USER_ID}</span> </span>
                        </a>
                        
                        
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                       
                         <!-- Poovathi comment Genaerate FNA Link on 14-06-2021 -->
                           <!-- <a class="dropdown-item" href="#" title="Generate FNA Doc." onclick="generateFNA();">
                          <i class="fa fa-external-link  fa-sm fa-fw mr-2" aria-hidden="true"></i>
                           Generate FNA</a>
                           
                     
                           <div class="dropdown-divider"></div> -->
                           <a class="dropdown-item" href="javascript:void(0);" title="Close" data-toggle="modal" data-target=".bs-example-modal-sm" data-backdrop="static"
                           ><i class="fa fa-sign-out fa-sm fa-fw mr-2 "></i>
                           Logout
                           </a>
                        </div>
                     </li>
                  </ul>
</nav>




        <div class="container-fluid" id="container-wrapper">
              <div class="card mb-4 " style="border:1px solid #044CB3;min-height:90vh" id="">
			      <div class="card-body" style="font-size: 13px;">
						 
						 <div class="row">
		
		 <div class="tab-regular" id="clntProdTabs" style="width:100%;">
		 
		  <ul class="nav nav-tabs small" id="landingpage" role="tablist" style="list-style-type:bullet;">
                <li class="nav-item" style="list-style-type:none;">
                	<a class="nav-link active" id="homepage-tab" data-toggle="tab" href="#homepage" role="tab" aria-controls="homepage" aria-selected="true" title="">
                		<img src="${contextPath}/vendor/avallis/img/analyze.png"><span> Dashboard</span>
                	</a>
                </li>
                
                
                <li class="nav-item" style="list-style-type:none;">
                	<a class="nav-link" id="detailspage-tab" data-toggle="tab" href="#detailspage" role="tab" aria-controls="detailspage" aria-selected="true" title="">
                		<img src="${contextPath}/vendor/avallis/img/assessment.png"><span> View FNA</span>
                	</a>
                </li>
							
            </ul>
            
            
           
                              <div class="tab-content" id="myTabContent" style="margin: 15px;">
                                 <div class="tab-pane fade show active" id="homepage" role="tabpanel" aria-labelledby="client1-tab">
									  <!-- HOME PAGE(DASHBOARD CONTENT START HERE -->
									       <div class="row" style="margin-top: 20px;">

    <div class="col-3">
    	<div class="right-tabs clearfix">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs block">
          <li class="nav-item ">
            <a class="nav-link" data-toggle="tab" href="#shome"><span class="txt_colr"><img src="${contextPath}/vendor/avallis/img/survey.png"> Your Approvals</span></a>
          </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content block">

          <div class="tab-pane container active" id="shome">
          
            <div class="row stats cht cursor-pointer mt-2" onclick="openList('PENDING')">

              <div class="col-sm-12">

                <div class="row">

                  <div class="col-sm-12">

                    <div class="row">

                      <div class="col-sm-6">

                        <span style="vertical-align: middle; font-size:130%;" ><i class="fa fa-question-circle"></i>&nbsp;Pending</span>

                      </div>

                      <div class="col-sm-4">

                        <div class="progress" style="height:25px">
                          <div class="progress-bar bg-ekyc-secondary progress-bar-striped progress-bar-animated" style="width:${FNA_MGR_STATUS_LIST.PENDING}%;height:25px;font-size:180%;"></div>
                        </div>

                      </div>

                      <div class="col-sm-2">

                        <span><c:if test="${not empty FNA_MGR_STATUS_LIST}">
		                      	 ${FNA_MGR_STATUS_LIST.PENDING}
		                      	 </c:if>	</span>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>
          
          

            <div class="row stats cht cursor-pointer mt-2" onclick="openList('APPROVED')">

              <div class="col-sm-12">

                <div class="row">

                  <div class="col-sm-12">

                    <div class="row">

                      <div class="col-sm-6">

                        <span style="vertical-align: middle; font-size:130%;"><i class="fa fa-check-circle-o"></i>&nbsp;Approved</span>

                      </div>

                      <div class="col-sm-4">

                        <div class="progress" style="height:25px">
                          <div class="progress-bar bg-ekyc progress-bar-striped progress-bar-animated" style="width:${FNA_MGR_STATUS_LIST.APPROVE}%;height:25px;font-size:180%;"></div>
                        </div>

                      </div>

                      <div class="col-sm-2">

                        <span><c:if test="${not empty FNA_MGR_STATUS_LIST}">
		                    	${FNA_MGR_STATUS_LIST.APPROVE}
		                     </c:if> </span>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>

          

            <div class="row stats cht cursor-pointer mt-2" onclick="openList('REJECTED')">

              <div class="col-sm-12">

                <div class="row">

                  <div class="col-sm-12">

                    <div class="row">

                      <div class="col-sm-6">

                        <span style="vertical-align: middle;font-size:130%;"><i class="fa fa-times-circle"></i>&nbsp;Rejected</span>

                      </div>

                      <div class="col-sm-4">

                        <div class="progress" style="height:25px">
                          <div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" style="width:${FNA_MGR_STATUS_LIST.REJECT}%;height:25px;font-size:180%;"></div>
                        </div>

                      </div>

                      <div class="col-sm-2">

                        <span><c:if test="${not empty FNA_MGR_STATUS_LIST}">
		                      	 ${FNA_MGR_STATUS_LIST.REJECT}
		                      	 </c:if></span>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>
            
            <!--poovathi add on 02-07-2021  -->
             <div class="row stats cht cursor-pointer mt-2" onclick="openList('PENDINGREQ')">

              <div class="col-sm-12">

                <div class="row">

                  <div class="col-sm-12">

                    <div class="row">

                      <div class="col-sm-6">

                        <span style="vertical-align: middle; font-size:130%;"><i class="fa fa-info-circle  text-info"></i>&nbsp;Pending Req.</span>

                      </div>

                      <div class="col-sm-4">

                        <div class="progress" style="height:25px">
                          <div class="progress-bar bg-info progress-bar-striped progress-bar-animated" 
                          style="width:${FNA_MGR_STATUS_LIST.PENDINGREQ}%;height:25px;font-size:180%;"></div>
                        </div>

                      </div>

                      <div class="col-sm-2">

                        <span><c:if test="${not empty FNA_MGR_STATUS_LIST}">
		                    	${FNA_MGR_STATUS_LIST.PENDINGREQ}
		                     </c:if> </span>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>
            <!-- End -->

          </div>
          
          <hr/>
          <div class="clearfix"></div>
          
          <section>
                  <div class="pieID pie"></div>
                    <ul class="pieID legend">
                         <li>
                            <em>Pending</em>
                            <span>${FNA_MGR_STATUS_LIST.PENDING}</span>
                         </li>
                         
                          <li>
                             <em>Approved</em>
                             <span>${FNA_MGR_STATUS_LIST.APPROVE}</span>
                          </li>
                          
                          <li>
                             <em>Rejected</em>
                             <span>${FNA_MGR_STATUS_LIST.REJECT}</span>
                         </li>
                         
                         <!-- poovathi add on 02-07-2021 -->
                           <li>
                             <em>Pending Request</em>
                             <span>${FNA_MGR_STATUS_LIST.PENDINGREQ}</span>
                          </li>
                         <!--  End-->
      
                    </ul>
            </section>

        </div>
        </div>
        
       
     
    </div>
    
    <div class="col-9">
   
    	<div class="right-tabs clearfix">
    	
    	 <ul class="nav nav-tabs block"  id="yourAprovTabs">
          <li class="nav-item">
            <a class="nav-link"  data-toggle="tab" href="#home" onclick="openTable('PENDING');"><i class="fa fa-question-circle"></i><span>&nbsp;Pending</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link"  data-toggle="tab" href="#menu1" onclick="openTable('APPROVE');"><i class="fa fa-check-circle-o"></i><span>&nbsp;Approved</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link"  data-toggle="tab" href="#menu2" onclick="openTable('REJECT');"><i class="fa fa-times-circle-o"></i><span>&nbsp;Rejected</span></a>
          </li>
          
          <!--poovathi add on 02-07-2021  -->
           <li class="nav-item">
            <a class="nav-link"  data-toggle="tab" href="#menu3" onclick="openTable('PENDINGREQ');"><i class="fa fa-info-circle text-info"></i><span>&nbsp;Pending Req.</span></a>
          </li>
          <!--End  -->
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">

          <div class="tab-pane active  mt-2" id="home">
          
          <table id="tblPending" class="table table-striped table-bordered hover"  style="width:100%">
				        <thead>
				            <tr>
				                <th>#</th>
				                <th><div style="width:120px">FNA ID</div></th>
				                <th><div style="width:180px">Client</div></th>				                
				                <th><div style="width:60px">Mgr.</div></th>
				                <th><div style="width:50px">Admin</div></th>
				                <th><div style="width:50px">Comp.</div></th>
				                <th><div style="width:60px">CustId</div></th>
				                <th><div style="width:50px">AdvId</div></th>
				                <th><div style="width:50px">Email</div></th>
				                <th><div style="width:60px">NTUC?</div></th>
				                <th><div style="width:50px">Mgr Appr By</div></th>
				                <th><div style="width:50px">Mgr Appr Date</div></th>
				                <th><div style="width:50px">Mgr Appr Rem</div></th>
				                
				                <th><div style="width:50px">Admin Appr By</div></th>
				                <th><div style="width:50px">Admin Appr Date</div></th>
				                 <th><div style="width:50px">Admin Appr Rem</div></th>
				                 
				                <th><div style="width:50px">Comp Appr By</div></th>
				                <th><div style="width:50px">Comp Appr Date</div></th>
				                 <th><div style="width:50px">Comp Appr Rem</div></th>
				                 
				                 <th><div style="width:50px">s1</div></th>
				                <th><div style="width:50px">s2</div></th>
				                 <th><div style="width:50px">s3</div></th>
				                  <th><div style="width:100px">Adviser</div></th>
				            </tr>
				        </thead>
				        <tbody>
				        </tbody>
			        </table>


          </div>

          <div class="tab-pane fade mt-2" id="menu1">
          
          	<table id="tblApproved" class="table table-striped table-bordered hover" style="width:100%">
				        <thead>
				           <tr>
				                <th>#</th>
				                <th><div style="width:120px">FNA ID</div></th>
				                <th><div style="width:180px">Client</div></th>				                
				                <th><div style="width:60px">Mgr.</div></th>
				                <th><div style="width:50px">Admin</div></th>
				                <th><div style="width:50px">Comp.</div></th>
				                <th><div style="width:50px">CustId</div></th>
				                <th><div style="width:50px">AdvId</div></th>
				                <th><div style="width:50px">Email</div></th>
				                <th><div style="width:50px">NTUC?</div></th>
				                <th><div style="width:50px">Mgr Appr By</div></th>
				                <th><div style="width:50px">Mgr Appr Date</div></th>
				                <th><div style="width:50px">Mgr Appr Rem</div></th>
				                
				                <th><div style="width:50px">Admin Appr By</div></th>
				                <th><div style="width:50px">Admin Appr Date</div></th>
				                 <th><div style="width:50px">Admin Appr Rem</div></th>
				                 
				                <th><div style="width:50px">Comp Appr By</div></th>
				                <th><div style="width:50px">Comp Appr Date</div></th>
				                 <th><div style="width:50px">Comp Appr Rem</div></th>
				                 <th><div style="width:50px">s1</div></th>
				                <th><div style="width:50px">s2</div></th>
				                 <th><div style="width:50px">s3</div></th>
				                  <th><div style="width:100px">Adviser</div></th>
				            </tr>
				        </thead>
				        <tbody>
				        </tbody>
			        </table>
          </div>

          <div class="tab-pane fade mt-2" id="menu2">
         	 <table id="tblRejected" class="table table-striped table-bordered hover" style="width:100%">
				        <thead>
				            <tr>
				                <th>#</th>
				                <th><div style="width:120px">FNA ID</div></th>
				                <th><div style="width:180px">Client</div></th>				                
				                <th><div style="width:60px">Mgr.</div></th>
				                <th><div style="width:50px">Admin</div></th>
				                <th><div style="width:50px">Comp.</div></th>
				                <th><div style="width:50px">CustId</div></th>
				                <th><div style="width:50px">AdvId</div></th>
				                <th><div style="width:50px">Email</div></th>
				                <th><div style="width:60px">NTUC?</div></th>
				                 <th><div style="width:50px">Mgr Appr By</div></th>
				                <th><div style="width:50px">Mgr Appr Date</div></th>
				                <th><div style="width:50px">Mgr Appr Rem</div></th>
				                
				                <th><div style="width:50px">Admin Appr By</div></th>
				                <th><div style="width:50px">Admin Appr Date</div></th>
				                 <th><div style="width:50px">Admin Appr Rem</div></th>
				                 
				                <th><div style="width:50px">Comp Appr By</div></th>
				                <th><div style="width:50px">Comp Appr Date</div></th>
				                 <th><div style="width:50px">Comp Appr Rem</div></th>
				                 <th><div style="width:50px">s1</div></th>
				                <th><div style="width:50px">s2</div></th>
				                 <th><div style="width:50px">s3</div></th>
				                  <th><div style="width:100px">Adviser</div></th>
				            </tr>
				        </thead>
				        <tbody>
				        </tbody>
			        </table>
          </div>
          
          <!-- poovathi add on 02-07-2021 -->
                <div class="tab-pane fade mt-2" id="menu3">
         	         <table id="tblPendingReq" class="table table-striped table-bordered hover" style="width:100%">
				        <thead>
				            <tr>
				                <th>#</th>
				                <th><div style="width:120px">FNA ID</div></th>
				                <th><div style="width:180px">Client</div></th>				                
				                <th><div style="width:60px">Mgr.</div></th>
				                <th><div style="width:50px">Admin</div></th>
				                <th><div style="width:50px">Comp.</div></th>
				                <th><div style="width:50px">CustId</div></th>
				                <th><div style="width:50px">AdvId</div></th>
				                <th><div style="width:50px">Email</div></th>
				                <th><div style="width:60px">NTUC?</div></th>
				                 <th><div style="width:50px">Mgr Appr By</div></th>
				                <th><div style="width:50px">Mgr Appr Date</div></th>
				                <th><div style="width:50px">Mgr Appr Rem</div></th>
				                
				                <th><div style="width:50px">Admin Appr By</div></th>
				                <th><div style="width:50px">Admin Appr Date</div></th>
				                 <th><div style="width:50px">Admin Appr Rem</div></th>
				                 
				                <th><div style="width:50px">Comp Appr By</div></th>
				                <th><div style="width:50px">Comp Appr Date</div></th>
				                 <th><div style="width:50px">Comp Appr Rem</div></th>
				                 <th><div style="width:50px">s1</div></th>
				                <th><div style="width:50px">s2</div></th>
				                 <th><div style="width:50px">s3</div></th>
				                  <th><div style="width:100px">Adviser</div></th>
				            </tr>
				        </thead>
				        <tbody>
				        </tbody>
			        </table>
              </div>
          <!--End  -->

        </div>
    	
       
        </div>

    </div>

  </div> 
									   <!--DASHBOARD TAB SEC CONTENT END HERE  -->     
                                 </div>
                                  
                              <div class="tab-pane fade show" id="detailspage" role="tabpanel" aria-label="client2-tab">
                                           <!--VIEW FNA TAB CONTENT START HERE  -->
                                           <form name="frmKycApprovalNew"  id="frmKycApprovalNew" >
  




<div class="row">
     <div class="col-4">
     <!-- manager,admin,comp and Ntuc status List -->
 <ul class="list-group list-group-horizontal">
  <li class="list-group-item">Manager Status : <i id="iFontMgr" class="fa fa-question-circle"></i> </li>
  <li class="list-group-item">Admin Status : <i id="iFontAdmin" class="fa fa-question-circle" ></i> </li>
  <li class="list-group-item">Compl. Status : <i id="iFontComp" class="fa fa-question-circle"></i> </li>
</ul> 
      
    
</div>

  <div class="col-2 ">
  <ul class="list-group list-group-horizontal">
  <li class="list-group-item d-none">Ntuc Status : <span class="text-center"><i id="ntucblock" class="" aria-hidden="true"></i></span> <span class="text-center"><img src="${contextPath}/views/newapproval/images/policy-pdf.png" style="width: 10px;" class="rounded mx-auto" alt="Get NTUC Policy Details"  title="Get NTUC Policy Details" onclick="" id="imgpolapi" style="display:none" /></span></li>
  
</ul> 
  </div>   <!--end  -->
    
     <div class="col-4">
     <label class="frm-lable-fntsz" >Client Name&nbsp;:&nbsp;</label>
        <select id="dropDownQuickLinkFNAHist" class="form-control" onchange="" style="vertical-align: middle" >
		        		  <option selected value="">--Select FNA List--</option>
		        		  <optgroup label="Pending" id="optgrpPending"></optgroup>
		        		  <optgroup label="Approved" id="optgrpApproved"></optgroup>
		        		  <optgroup label="Rejected" id="optgrpRejected"></optgroup>
		        		  <optgroup label="PendingReq" id="optgrpPendingReq"></optgroup>  
		           </select>
        	
     
     </div>
      <div class="col-2">
      <small>FNA Id : <span id="txtFnaId"></span></small><br>
      <small>Adviser : <span id="txtFldAdvStfName"  title=""><span class="adviserNameTab"></span></span></small>
       </div>
   </div> 

    <div class="row mt-2" >
    
    <!--Approval Sec Start  -->
          <div class="col-4 col-md-4">
		      <div class="card">			
				<div class="card-header txt_colr" id="cardHeaderStyle-22">
				    <div class="row">
				          <div class="col-5"><span id="cardSecTitle">Approval Sections</span></div>
				          <div class="col-5">
				               <!-- <input type="button" class=" btn btn-bs3-warning" id="cardBtnTitle" onclick="toggleApprovPolcySec(this)" value="View Policy Docs."/> -->
				         
				         <span class="badge badge-pill badge-warning amber py-2 btnshake show" title="View Policy Submission Docs."  onclick="toggleApprovPolcySec(this)">
				         <i class="fa fa-folder-open" aria-hidden="true"></i>&nbsp;<span class="font-sz-level9" id="cardBtnTitle">Click here to View Policy Docs.</span></span>
				         
				         
				          </div>
				          <div class="col-2 d-none"><span id="polAllDocDownld" class="hide"><i class="fa fa-file-archive-o" aria-hidden="true"></i><sub><i class="fa fa-cloud-download cursor-pointer" aria-hidden="true"
				            title="Zip all Documents" onclick="downLoadAllFile();"></i></sub></span></div>
				    </div>
				</div>		
				 	
				<div class="card-body" id="ApprovalSec" >			
				<nav class="nav-justified ">
				                  <div class="nav nav-tabs " id="nav-tab" role="tablist">
				                    <a class="nav-item nav-link active" id="appr-mgr-tab" data-toggle="tab" href="#appr-mgr-tab-cont" role="tab" aria-controls="appr-mgr-tab-cont" aria-selected="true"><strong>Manager</strong> </a>
				                    <a class="nav-item nav-link" id="appr-admin-tab" data-toggle="tab" href="#appr-admin-tab-cont" role="tab" aria-controls="appr-admin-tab-cont" aria-selected="false"><strong>Admin</strong> </a>
				                    <a class="nav-item nav-link" id="appr-comp-tab" data-toggle="tab" href="#appr-comp-tab-cont" role="tab" aria-controls="appr-comp-tab-cont" aria-selected="false"><strong>Compliance</strong> </a>
				                    
				                  </div>
				                </nav>
				                <div class="tab-content" id="nav-tabContent" style="padding:9px;">
				                
				                
				                  <div class="tab-pane fade show active" id="appr-mgr-tab-cont" role="tabpanel" aria-labelledby="appr-mgr-tab">
				                  
				                        <div class="pt-3"></div>
				                        
				                        <div class="row">
										   <div class="col-md-12">
										   <label for="" class="frm-lable-fntsz">Status</label>
										    <select class="form-control" 
										    id="txtFldMgrApproveStatus" onchange="setManagerStatus(this)" disabled> 
										        <option value="APPROVE">Approve</option>
										        <option value="REJECT">Reject</option>
										        <option value="">Pending</option>
										        <option value="PENDINGREQ">Pending Request for Further Requirement</option>
										    </select>
										   </div>
										</div>
										
										<div class="row">
											<div class="col-md-12">
											 <label for="" class="frm-lable-fntsz">Remarks:</label>
											  <textarea class="form-control text-wrap txtarea-hrlines" id="txtFldMgrApproveRemarks" 
											  onkeydown="textCounter(this,250);" onkeyup="textCounter(this,250);" maxlength="300" rows="3" readonly ></textarea>
											  <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
											</div>
										</div>
										
										<div class="row">
											 <div class="col-md-6">
										        <label class="frm-lable-fntsz" id="mngrapprLbl" >Approved / Rejected Date</label> 
										             <div class="input-group form">
														<input type="text" class="form-control"  id="txtFldMgrApproveDate" readonly><!-- no-border no-bgcolor bottom-border -->											                 
											         </div>
										    </div>
										    
										    <div class="col-md-6">
										        <label class="frm-lable-fntsz" id="mngrapprByLbl">Approved / Rejected By</label> 
										             <div class="input-group form">
														<input type="text" class="form-control"  id="txtFldMgrStsApprBy" readonly>											                 
											         </div>
										    </div>
										    
										    
										</div>
										
						
				                        
				                  </div>
				                  <div class="tab-pane fade" id="appr-admin-tab-cont" role="tabpanel" aria-labelledby="appr-admin-tab">
				                       <div class="pt-3"></div>
				                       
				                       <div class="row">
										   <div class="col-md-12">
										   <label for="" class="frm-lable-fntsz">Status</label>
										    <select class="form-control" 
										    id="txtFldAdminApproveStatus"  onchange="setAdminStatus(this)" disabled> 
										        <option value="APPROVE">Approve</option>
										        <option value="REJECT">Reject</option>
										        <option value="">Pending</option>
										        <option value="PENDINGREQ">Pending Request for Further Requirement</option>
										    </select>
										   </div>
										</div>
										
										<div class="row">
											<div class="col-md-12"> 
											 <label for="" class="frm-lable-fntsz">Remarks:</label>
											  <textarea class="form-control text-wrap txtarea-hrlines" id="txtFldAdminApproveRemarks" 
											   onkeydown="textCounter(this,250);" onkeyup="textCounter(this,250);" maxlength="300" rows="3" readOnly></textarea>
											  <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
											</div>
										</div>
										
										<div class="row">
											<div class="col-md-6">
										        <label class="frm-lable-fntsz" id="admnapprLbl" ></label> 
										             <div class="input-group form">
											               <input type="text" class="form-control"  id="txtFldAdminApproveDate" readonly>
											                   
											         </div>
										    </div>
										    
										    <div class="col-md-6">
										        <label class="frm-lable-fntsz" id="admnapprByLbl" ></label> 
										             <div class="input-group form">
											               <input type="text" class="form-control"  id="txtFldAdminStsApprBy" readonly>
											                   
											         </div>
										    </div>
										    
										</div>
						
				                  </div>
				                  <div class="tab-pane fade" id="appr-comp-tab-cont" role="tabpanel" aria-labelledby="appr-comp-tab">
				                       <div class="pt-3"></div>
				                        <div class="row">
										   <div class="col-md-12">
										   <label for="" class="frm-lable-fntsz">Status</label>
										    <select class="form-control" 
										    id="txtFldCompApproveStatus" onchange="setComplianceStatus(this)" disabled> 
										        <option value="APPROVE">Approved</option>
										        <option value="REJECT">Rejected</option>
										        <option value="">Pending</option>
										        <option value="PENDINGREQ">Pending Request for Further Requirement</option>
										    </select>
										   </div>
						   
										   
										</div>
						
										<div class="row">
											<div class="col-md-12">
											 <label for="" class="frm-lable-fntsz">Remarks:</label>
											  <textarea class="form-control text-wrap txtarea-hrlines "  id="txtFldCompApproveRemarks" onkeydown="textCounter(this,250);" 
											  onkeyup="textCounter(this,250);" maxlength="300" rows="3"  readonly></textarea>
											  <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
											</div>
										</div>
						
										<div  class="row">
											<div class="col-md-6">
														     <label class="frm-lable-fntsz" id="compapprLbl" ></label> 
														             <div class="input-group form">
															               <input type="text" class="form-control"  id="txtFldCompApproveDate" readonly>
															                  
															         </div>
											</div>
											
											<div class="col-md-6">
														     <label class="frm-lable-fntsz" id="compapprByLbl" ></label> 
														             <div class="input-group form">
															               <input type="text" class="form-control"  id="txtFldCompStsApprBy" readonly>
															                  
															         </div>
														    </div>
										
										</div>
				                  </div>
				                </div>
				
				
				</div>
				
				
				 <div class="card-body hide" id="policySubDocSec" >
				 
				 <div class="card">
				 <div class="card-header" id="cardHeaderStyle"><span class="font-sz-level6"
				 style="border-bottom: 3px solid #178ee5;">Reference Documents.</span>
				  <!--poovathi show file download count on 15-06-2021 -->
					<small class="badge badge-pill badge-info font-sz-level7 right p-2" id="fileCountTxt" ></small>
				</div>
				
				 <div class="card-body">
				  <div class="list-group-item" id="dynaAttachNRICList"  style="max-height: 50vh;overflow-y: scroll; overflow-x: hidden;"></div>
				 </div>
				  </div>
				  
				  <!-- poovathi commented Other Doc sec on 14-06-2021 -->
				<!--   <div class="card mt-2">
				 <div class="card-header" id="cardHeaderStyle"><strong>Other Documents</strong></div>
				 <div class="card-body">
				  <div class="list-group-item" id="dynaAttachNTUCList"  style="max-height: 50vh;overflow-y: scroll; overflow-x: hidden;">-No Refernce Docs-</div>
				 </div>
				  </div> -->
				 
				</div>
				
				
				</div>
				
				
				
			
			 
			 </div>
			
    
    <!-- Approval Sec End -->
    
    
      <div class="col-8 col-md-8">
				<div class="card">
				<div class="card-header" id="cardHeaderStyle-22">
				<div class="row">
				     <div class="col-3">
				        Verify FNA Form :-
				     </div>
				     <div class="col-4">
				         <span class="badge badge-pill badge-success py-2 " title="Click Here to Sign & Approve FNA"   onclick="getQrorSign();" id="btnQRCodesignMgr">
				         <i class="fa fa-check-square-o fa-1x" aria-hidden="true">
				         </i>&nbsp;<span class="font-sz-level9 ls-14">Click here to Sign & Approve / Reject FNA</span></span>
				     </div>
				     <!-- poovathi commented Print Incon on 14-06-2021 -->
				     <!-- <div class="col-3 hide">
				          <span class="badge badge-pill badge-warning py-2 " title="Full View FNA" onclick="fullViewFNA();" id="">
				         <i class="fa fa-print icncolr" aria-hidden="true">
				         </i>&nbsp;<span class="font-sz-level9 ls-14">Print FNA</span></span>
				     </div> -->
				     
				     <!-- poovathi add on 30-06-2021 -->
				     <div class="col-4">
				      <!-- <button type="button" class="btn btn-bs3-prime mt-3" data-toggle="modal" 
                       data-target="#fnaRejectStsMdl">Send Rejection Status</button> -->
                       <span class="badge badge-pill badge-warning amber py-2 hide" title="" data-toggle="modal" 
                             data-target="#fnaRejectStsMdl" data-backdrop="static" id="btnSendReq">
				         <i class="fa fa-share-square-o fa-1x" aria-hidden="true" >
				         </i>&nbsp;<span class="font-sz-level9 ls-14">Send Pending Req. for Further Requirement </span></span>
                     </div>
                     
                     <!-- poovathi add 06-07-2021 -->
                     <div class="col-1">
                     
                          <i class="fa fa-share fa-2x" aria-hidden="true" 
                          onclick="getFnaPendReqHisDetls()" title="Click to view Pending Req. History Details"></i>
                     </div>
				     
				</div>
				</div>
				 
				<div class="card-body" style="overflow-x:hidden">
				
					<iframe class="iframe-placeholder"  style="height:calc(100vh - 40vh);width:100%;border:0px;display:block" id="dynaFrame" >				 
					
					
					</iframe>
					
						
				</div>
				</div>
			</div>
			
			
			
			
			
			
		 
    </div>
    
    
    </form>
                                           <!--VIEW FNS TAB CONTENT SEC END HERE  -->
                              </div>
                              
                              
                              </div>
                           </div>
		    
		</div>     
						
							   
							   
				  </div>
	  
	          </div>
		
        </div>
     
        
        


</div>

</div>

</div>
<!--lOGOUT MODAL   -->
<div class="modal bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header"><h4>Logout <i class="fa fa-lock"></i></h4><button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body"><i class="fa fa-question-circle"></i> Are you sure, do you want to Logout?</div>
      <div class="modal-footer"><a href="javascript:;" class="btn btn-primary " onclick="logoutKyc();">OK</a>
       <a href="javascript:;" class="btn btn-secondary" data-dismiss="modal">Cancel</a> 
      </div>
    </div>
  </div>
</div>
<!--LOGOUT MAODAL END  -->

<div class="modal" tabindex="-1" role="dialog" aria-hidden="true" id="modalNTUCApproval">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header"><h6>KYC Approval Document<i class="fa fa-upload"></i></h6></div>
      <div class="modal-body">
      		<form id='hdnFormKycApprDoc' method='POST'>
      			<div class="form-control">
      				<input type='file' name='fileKycCompApprove' id='fileKycCompApprove' class='fpFileBrowseBtn' style='width:100%;'>
      			</div>
      			
      		</form>
      </div>
      <div class="modal-footer"><a href="javascript:;" class="btn btn-primary " onclick="validateNTUCApprDoc()">Upload</a>
       
      </div>
    </div>
  </div>
</div>


<!--REPORT MODAL START  -->
  <div class="modal fade" id="PDFReportModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg">
       <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">PDF REPORT</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <div class="">
          <iframe  style="height:100vh;width:53vw;border:0px;" id="PDFReportFrame"></iframe>
          </div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
<!--REPORT MODAL END  -->




    <!--mngr sec modal start  -->
      <!-- manager Sec Open Modal Start -->
      
      
      <div class="modal fade" id="openMngesecModal"  aria-modal="true" role="dialog">
    <div class="modal-dialog modal-xl  modal-dialog-scrollable">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header" id="cardHeaderStyle-22">
        <h6 class="modal-title bold"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp;Supervisor's Review</h6>
        <button type="button" class="close" data-dismiss="modal" style="color:#fff;">�</button>
      </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            
          <!-- manager Sec Start  -->
          
                <div class="card card-content-fnsize" style="margin-top:10px;" id="diManagerSection">
                             <div class="card-body">
                                   <div class="row">
										           <div class="col-md-12">
										              <span class="font-sz-level6">I have reviewed the information disclosed in the "Financial Needs Analysis" Form which relates to the
                                                       client's priorities and objectives, investment profile, insurance portfolio, CKA outcome and the client's. 
                                                        declaration &amp; acknowledgement.</span>
										           </div>
								  </div>
								   <div class="row mt-2" id="mngrSecChk">
                                         <div class="col-md-12">
                                          <ul class="list-group mt-2">
		                                          <li class="list-group-item bg-hlght" id="errorFld">
		                                              <span class="italic bold text-primaryy font-sz-level6 "> Select any <span class="txt-urline  bold">ONE</span> Option Below :-</span>
		                                                      <div class="row">
								        <div class="col-md-6 ">
								          <div class="col-md-12">
								              
								              <div id="Sec2ContentFormId">
			                                    <div class="inputGroup">
													         <input id="suprevMgrFlgA" name="suprevMgrFlg" type="radio" value="" onclick="enableMgr();chkAgreeDisagreeFld(this,'suprevFollowReason');">
															<label for="suprevMgrFlgA" id="lblTxtSz-03" style="border: 1px solid grey;"><span><span class="font-sz-level6">  <span class="text-success"><strong>I agree</strong></span> with the Representative's needs analysis and<br> recommendation(s). </span> </span></label>
												 </div>
                                              </div>
										            
										  </div>
								        </div>
								        <div class="col-md-6">
								             <div class="col-md-12">
								             <div id="Sec2ContentFormId">
												<div class="inputGroup">
													         <input id="suprevMgrFlgD" name="suprevMgrFlg" type="radio" value="" onclick="enableMgr();chkAgreeDisagreeFld(this,'suprevFollowReason');">
															<label for="suprevMgrFlgD" id="lblTxtSz-03" style="border: 1px solid grey;"><span><span class="font-sz-level6"> <span class="text-danger"><strong>I disagree</strong></span> with the Representative's needs analysis and<br> recommendation(s).</span></span></label>
												 </div>
                                              </div>
										            
										       </div>
								        </div>
								  </div>
		                                          
		                                          </li>
		                                          
		                                          
		                                          </ul>
                                           <div class="invalid-feedbacks" id="mngrBtnErrorMsg"></div>
                                         
                                             
								          </div>
								        
								    </div>
								 
								 
								  
								  <div class="row mt-2">
								       <div class="col-md-12">
								         <small><i> <strong>Note: </strong> (If <span class=" text-danger bold font-sz-level8">disagree</span> , please state the reasons and/or advise on the follow-up action required, where applicable.)</i></small>
								       </div>
								       
								   </div>
								   
								   
								    <div class="row">
								       <div class="col-md-8">
								             <div class="form-group">
								                  <label class="frm-lable-fntsz "> Reason(s) and Follow-up Action:</label>
									             <textarea class="form-control txtarea-hrlines text-wrap" rows="7" id="suprevFollowReason" name="suprevFollowReason" onblur="hideErrmsg(this)"
									            	 maxlength="300" onkeydown="textCounter(this,300);" onkeyup="textCounter(this,300);"  ></textarea>
								                     <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
							                     <div class="invalid-feedbacks" id="mngrDisagrReason"></div>
							                </div> 
								       </div>
								       
								        <div class="col-md-4">
								             <div class="card" style="margin-top:15px;" id="SignCard">
								             <div class="card-header" id="cardHeaderStyle"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Manager Signature</div>
								                								                  <div class="card-body">
								                     <div class="row mt-2">
								                            <div class="col-md-6">
								                                 <label class="frm-lable-fntsz p-0">Sign. of Supervisor :  </label>
								                            </div>   
								                            
								                            
								                            <!-- signature Click open San to Sign Modal Page start  -->
								                             <div class="col-md-6">
								                                  <div id="mgrQrSecNoData"  class="mgrQrSecNoData show">
								                                   <p class="center noData" id="" style="font-size: 11px;"><img src="${contextPath}/vendor/avallis/img/warning.png" class="">
								                                     &nbsp;Manager Signature Not Found&nbsp;</p>
								                                  </div>
								                                  
								                                  <div id="sign-QR_style" class="mgrQrSec hide">
								                                    <small><img src="${contextPath}/vendor/avallis/img/contract.png"class="mgrQr"></small>
								                                  </div>
								                            </div>  
								                            
								                            <!-- signature Click open San to Sign Modal Page End -->
								                             
								                      </div>
								                      
								                      <div class="row mt-2">
								                            <div class="col-md-6">
								                                <label class="frm-lable-fntsz p-0">Name of Supervisor: </label>
								                            </div>
								                            
								                             <div class="col-md-6">
								                             <input type="text" name="mgrName" id="mgrName"  class="form-control" readonly="readonly" value="${LOGGED_USER_INFO.FNA_ADVSTF_MGRNAME}">
								                            
								                            </div>  
								                                
								                      </div>
								                      
								                       
								                      
								                      <div class="row mt-2">
								                            <div class="col-md-6">
								                                 <label class="frm-lable-fntsz p-0">Date : </label>
								                            </div> 
								                            
								                             <div class="col-md-6">
								                              <span id="mgrSignDate" style="border: none;"></span>
								                              <!--    <input type="text" name="txtFldMgrSignDate" id="txtFldMgrSignDateDeclarepage" readonly="true" style="width:80px;    border: none;"> -->
								                            </div>    
								                      </div>
								                      
								                      <div class="row mt-2 d-none">
								                            <div class="col-md-6">
								                                <label class="frm-lable-fntsz p-0"> Mail Sent Status : </label>
								                            </div> 
								                             <div class="col-md-6">
								                                <span class="badge badge-success " id="mailSentMgr">Email Sent <i class="fa fa-check" aria-hidden="true"></i></span>
								                                <span class="badge badge-warning font-normal d-none" id="mailNotSentMgr">Email Not Sent <i class="fa fa-times" aria-hidden="true"></i></span>
								                            </div>    
								                      </div>
								                  </div>
								             </div> 
								       </div>
								       
								   </div>
								   
								  
								     
                               </div>
                       </div>
          
        
             </div>
      
      <div class="modal-footer">
				<button type="button" class="btn btn-sm btn-bs3-prime" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-sm btn-bs3-prime" id="btnApprvMngr" onclick="mngrSendToApprove()">Update Status</button>
				<button type="button" class="btn btn-sm btn-bs3-prime d-none" id="btnApprMgrFlag" data-dismiss="modal">Close</button>
	   </div>
    </div>
  </div>
  
</div>
   
   
   <!-- mngr sec modal end -->
   
   <!--poovathi add Rejection ststus email to adviser Modal here  -->
    <!-- The Modal -->
  <div class="modal" id="fnaRejectStsMdl">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header" id="cardHeaderStyle-22">
        <h6 class="modal-title"><i class="fa fa-commenting" aria-hidden="true"></i>&nbsp;Request for Further Requirement</h6>
        <button type="button" class="close" data-dismiss="modal" style="color: #fff;">�</button>
      </div>
        
        <!-- Modal body -->
        <div class="modal-body">
             <div class="row">
	              <div class="col-md-12">
			           <div class="form-group">
			                 <label class="frm-lable-fntsz p-0"> Enter your Comments : </label>
							 <textarea class="form-control txtarea-hrlines text-wrap " rows="12" cols="450" 
							 name="txtFldRejStatus" id="txtFldRejStatus" onkeydown="textCounter(this,3950);" maxlength="3950" 
							 onkeyup="textCounter(this,3950);" title="" onchange="validateReqStatus()" autocomplete="off"></textarea>
					         <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
					   </div>
					   
					    <div class="invalid-feedbacks" id="txtFldRejStatusErrorMsg"></div>
			     </div>
           </div>
            <input type="hidden" id="hdntxtFldClntName"><!--Client Name  -->
            <input type="hidden" id="hdntxtFldAdvName"><!--Adviser Name  -->
            <input type="hidden" id="hdntxtFldFnaId"><!-- Fna Id  -->
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer p-1">
           <!--  <button type="button" class="btn btn-bs3-prime" data-dismiss="modal">Close</button> -->
           <button type="button" class="btn btn-bs3-success" 
           onclick="sendMailtoAdviser()"> <i class="fa fa-paper-plane fa-1x" aria-hidden="true"></i>&nbsp;Send Email to adviser</button>
        </div>
        
      </div>
    </div>
  </div>
  <!--modal end  -->
  
  <!--poovathi add on 06-07-2021  -->
    <!-- The Modal -->
  <div class="modal fade" id="fnaPendPeqHistryMdl">
    <div class="modal-dialog modal-dialog-scrollable modal-xl">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header" id="cardHeaderStyle-22">
          <h4 class="modal-title"><i class="fa fa-eye fa-2x text-warning" aria-hidden="true"></i>
          View pending req. Details:-</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body (FnaPendingReqHistory modal) -->
        <div class="modal-body">
             <div class="row">
	              <div class="col-md-12" >
				       <div class="card">
						  <div class="card-body" id="pendReqListSec">
						  </div>
					   </div>
				   </div> 
				  <input type="hidden" id="hdnFldClntName"> 
				  <input type="hidden" id="hdnFldFnaId">
			 </div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-bs3-prime" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
  
  <!-- End -->


 <jsp:include page="/views/pages/scanToSignModal.jsp"></jsp:include>
</body>
<script>

$(document).ready(function(){
	
$('#cover-spin').show(0);
	
     $('#dropDownQuickLinkFNAHist').select2({
	  placeholder: "-- Select FNA List --",
	  width : '100%',
	  //templateResult: loadSelect2PrinCombo,
	  allowClear: true
	  });
	
	baseUrl = "<%=request.getContextPath()%>";

	COMMON_SESS_VALS = <%=session.getAttribute("COMMON_SESS_OBJ")%>;	
	
	isFromMenu=<%=request.getAttribute("FROM_MENU")%>;
	strFnaMgrUnAppDets=<%=request.getAttribute("FNA_MGR_NOTUPDATED_DETS")%>;	
	strAdminFNADets=<%=request.getAttribute("ADMIN_FNA_VERSION_DETS")%>;
	strComplianceFNADets=<%=request.getAttribute("COMPLIANCE_FNA_VERSION_DETS")%>;
	strALLFNAIds=<%=request.getAttribute("ALL_FNA_IDS")%>;
	current_screen_load = '<%=request.getAttribute("current_screen_load")%>';
	
	seesselfName = "${FNA_SELF_NAME}";
	sessspsname = "${FNA_SPOUSE_NAME}";
	acsmode = "${APPROVAL_SECTION_TITLE}";
    jsnDataFnaDetails = ${CURRENT_FNA_DETAILS}
    $("#txtFnaId").text($("#hTxtFldFnaId").val());
	$("#txtFnaId").attr("title",$("#hTxtFldFnaId").val());
	//$("#dropDownQuickLinkFNAHist").val($("#hTxtFldFnaId").val());
	//$('#dropDownQuickLinkFNAHist').trigger('change.select2');
	callBodyInit();

	
	// $("#optgrpPending").append('<option value="'+$("#hTxtFldFnaId").val()+'">'+seesselfName+'</option>');
	// $('#dropDownQuickLinkFNAHist').val(seesselfName);
	// $('#dropDownQuickLinkFNAHist').trigger('change.select2');
 
    // poovathi add on 30-06-2021
	$('#fnaRejectStsMdl').on('shown.bs.modal', function() {
		// clear form fields and Error
		$('#txtFldRejStatus').val("");
		$("#txtFldRejStatusErrorMsg").html(" ");
		
	    $('#txtFldRejStatus').focus();
	    
	    var clnt = $("#dropDownQuickLinkFNAHist").find("option:selected").text().trim();
	    var fnaId = $("#dropDownQuickLinkFNAHist").find("option:selected").val().trim();
	    var advname = $("#txtFldAdvStfName").find("span.adviserNameTab").text().trim();
	    $('#hdntxtFldClntName').val(clnt);
	    $('#hdntxtFldFnaId').val(fnaId);
	    $('#hdntxtFldAdvName').val(advname);
	})//end autofocus function  
	
	
});

function toggleApprovPolcySec(obj){
	var btnLabel = $("#cardBtnTitle").text();
	if(btnLabel=="Click here to View Policy Docs."){
		$("#policySubDocSec").removeClass("hide").addClass("show")
		$("#ApprovalSec").removeClass("show").addClass("hide");
		$("#polAllDocDownld").removeClass("hide").addClass("show");
		$("#cardSecTitle").text("Policy Sub. Docs");
		$("#cardBtnTitle").text("Click here to View Approval Sec.");
		$("#cardBtnTitle").attr("title","View Approval Sec");
	}else if(btnLabel=="Click here to View Approval Sec."){
		$("#policySubDocSec").removeClass("show").addClass("hide")
		$("#ApprovalSec").removeClass("hide").addClass("show")
		$("#cardSecTitle").text("Approval Sections");
		$("#cardBtnTitle").text("Click here to View Policy Docs.");
		$("#cardBtnTitle").attr("title","View Policy Docs.");
		$("#polAllDocDownld").removeClass("show").addClass("hide");
	}
	
}

	</script>
	
	


</html>
