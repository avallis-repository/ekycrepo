var strGlblUsrPrvlg="";

var strFnaMgrUnAppDets="";
var STR_FNA_MGRAPPRSTS_LIST = "APPROVE^REJECT";
var strAdminFNADets = "";
var strComplianceFNADets = "";
var strLoggedUserEmailId = "";
var archiveflg= false;
var baseUrl;
var currDate = "";
var strMngrFlg="",strAdminFlg="",strCompFlg="";
var strALLFNAIds="";


var COMMON_SESS_VALS="";
var LOGGED_DISTID="";//,FNA_RPT_FILELOC="",FNA_PDFGENLOC="",FNA_FORMTYPE="";
var strLoggedAdvStfId = "";
var strLoggedUserId = "";
var ntuc_policy_access_flag=false,ntuc_sftp_acs_flg=false;
var isFromMenu="";

//poovathi add pending req datatable variable 0n 07-02-2021
var pendingdatatable,approveddatatable,rejecteddatatable,pendingreqdatatable;
var acsmode=""
var jsnDataFnaDetails = "";
var mgrmailsentflag = false;


$('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
  /*  $($.fn.dataTable.tables(true)).DataTable()
       .columns.adjust();*/
    
    $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
 }); 


$(document).on('show.bs.modal', function () {
	//$($.fn.dataTable.tables(true)).DataTable() .columns.adjust();
	 $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
});

function callBodyInit(){
	
	
	for ( var sess in COMMON_SESS_VALS) {
		
		var sessDets = COMMON_SESS_VALS[sess];	  
		for (var dets in sessDets) {		  
			if (sessDets.hasOwnProperty(dets)) {
			  
				var sesskey = dets;
				var sessvalue = sessDets[dets];
				
			    if(sesskey == "LOGGED_DISTID") {LOGGED_DISTID = sessvalue;}
			    
			    if(sesskey == "LOGGED_USER_EMAIL_ID"){strLoggedUserEmailId= sessvalue;}
			    
			    if(sesskey == "TODAY_DATE"){currDate = sessvalue;}
			    
			    if(sesskey == "MANAGER_ACCESSFLG"){strMngrFlg = sessvalue;}
			    
			    if(sesskey == "LOGGED_ADVSTFID"){strLoggedAdvStfId = sessvalue;}
			    
			    if(sesskey == "LOGGED_USERID" ){strLoggedUserId = sessvalue;}//|| sessKey == "STR_LOGGEDUSER" || || sessKey == "LOGGED_USER"
			    
			    if(sesskey == "RPT_FILE_LOC"){FNA_RPT_FILELOC = sessvalue;}
			    
			    if(sesskey == "ADMIN_ACCESSFLG"){strAdminFlg = sessvalue;}
			    
			    if(sesskey == "COMPLIANCE_ACCESSFLG"){strCompFlg = sessvalue;}
			    
			    if(sesskey == "NTUC_POLICY_ACCESS"){ntuc_policy_access_flag = sessvalue;}
			    
			    if(sesskey == "NTUC_SFTP_ACCESS"){ntuc_sftp_acs_flg = sessvalue;}
			    
			}
		}
	}
	
	strMngrFlg="Y"
	openList('PENDING');
	
	 pendingdatatable = $('#tblPending').DataTable(
			     {
				    scrollX: true,
					scrollY:"50vh",
					scroller: true,
					scrollCollapse:true,
					autoWidth:false,
					"sDom": '<"top"flp>rt<"bottom"i><"clear">',
				 "columnDefs": [{ "targets": [ 6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21  ],"visible": false,"searchable": false}]}		
			     
			     
	
	 );

	  approveddatatable = $('#tblApproved').DataTable(
			     {
				    scrollX: true,
					scrollY:"50vh",
					scroller: true,
					scrollCollapse:true,
					autoWidth:false,
					"sDom": '<"top"flp>rt<"bottom"i><"clear">',
				  "columnDefs": [{ "targets": [ 6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21 ],"visible": false,"searchable": false}]}
			  );

	  rejecteddatatable = $('#tblRejected').DataTable(
			     { 
				    scrollX: true,
					scrollY:"50vh",
					scroller: true,
					scrollCollapse:true,
					autoWidth:false,
					"sDom": '<"top"flp>rt<"bottom"i><"clear">',
				  
				  "columnDefs": [{ "targets": [ 6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21 ],"visible": false,"searchable": false}]}
			  );
	  
	  //poovathi initialize pendingReq datatable properties here
	  
	  pendingreqdatatable = $('#tblPendingReq').DataTable(
			     { 
				    scrollX: true,
					scrollY:"50vh",
					scroller: true,
					scrollCollapse:true,
					autoWidth:false,
					"sDom": '<"top"flp>rt<"bottom"i><"clear">',
				  
				  "columnDefs": [{ "targets": [ 6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21 ],"visible": false,"searchable": false}]}
			  );
	  //initialization end. 
	  
	  if(isFromMenu == true){
		  $("#landingpage").find("li:eq(0) a").trigger("click");
	  }else{
		 
		  $("#landingpage").find("li:eq(1) a").trigger("click");
		  
		 // $('#cover-spin').hide(0);
	  }
		
		if(strALLFNAIds != null){
			loadALLFNAId();
		}
		
//	}
	
	if(strMngrFlg != "Y"){
		$("#appr-mgr-tab").hide();
		$("#appr-mgr-tab-cont").hide();
	}
	if(strAdminFlg != "Y"){		
		$("#appr-admin-tab").hide();
		$("#appr-admin-tab-cont").hide();		
	}
	if(strCompFlg != "Y"){		
		$("#appr-comp-tab").hide();
		$("#appr-comp-tab-cont").hide();		
	}

	$("#spanCrtdUser").text(strLoggedUserId);
	if(acsmode != "MANAGER"){
		$("#btnQRCodesignMgr").hide()
	}
	
	createPie(".pieID.legend", ".pieID.pie");
	
	
	if(!isEmpty(jsnDataFnaDetails)  && !isJsonObjEmpty(jsnDataFnaDetails)){
		  for(var obj in jsnDataFnaDetails){
			  if (jsnDataFnaDetails.hasOwnProperty(obj)) {
				  var objValue = jsnDataFnaDetails[obj];
//				  console.log(obj,objValue,"--------------")
				  switch(obj){
				  
				  case "adviser_name":
					  $(".adviserNameTab").text(objValue);
					  break;
				  
				  case "mgrEmailSentFlg":
					  if(!isEmpty(objValue) && objValue == "Y" && strMngrFlg == "Y") {
						  mgrmailsentflag = true;
					  }else{
						  mgrmailsentflag = false;
					  }
					  
					  break;
				
				  case "suprevMgrFlg":
   					
    					  if(objValue == "agree" ){ 
    						
                                 $("#suprevMgrFlgA").prop("checked",true);
                                 $("INPUT[name=suprevMgrFlg]").val([objValue]);
                                 //$("#diManagerSection").find(":input").prop("disabled",true);
       						     $("#diManagerSection").addClass("disabledsec");
    						  } 
    					  else if(objValue == "disagree" ){
    						  $("#suprevMgrFlgD").prop("checked",true);
    						  $("INPUT[name=suprevMgrFlg]").val([objValue]);
    						  } 
    					  else {
    						  $("#suprevMgrFlgA").prop("checked",false);
    						  $("#suprevMgrFlgD").prop("checked",false);
    						  $("#diManagerSection").removeClass("disabledsec");
    						  //$("#diManagerSection").find(":input").removeAttr("disabled");
    						  }
    					  break;
					  
				  case "mgrName":
//					  No action
					  break;
					  
				  
				  default:
					  $("#"+obj).val(objValue);
				  }
				  
			  }
			  
		  }

	}
	
	
	
	var fnaId=$("#txtFnaId").text();
	if(isEmpty(fnaId))fnaId  = current_fna_id;
	getAllSign(fnaId);
	
	
	if(!mgrmailsentflag) {
//		alert("")
		
		Swal.fire({
			width:"650px",
	  	  title: "eKYC Notification",
	  	  html: "This FNA is not yet ready for approval. Please contact the adviser for more details",
	  	  		
	  	  icon: "info",
//	  	  showCancelButton: true,
	  	  allowOutsideClick:false,
	  	  allowEscapeKey:false,
	  	  confirmButtonClass: "btn-danger",
	  	  confirmButtonText: "OK",
//	  	  cancelButtonText: "No,Cancel",
//	  	  closeOnConfirm: false,
	  	  //showLoaderOnConfirm: true
//	  	  closeOnCancel: false
	  	}).then((result) => {
			  if (result.isConfirmed) {
				  Swal.close();
				  $("#landingpage").find("li:eq(0) a").trigger("click");
			  } 

	  	});
		
		
	}
	
	
	 //poovathi add on 02-07-2021
	//hide send Req for fur Req btn if loggedUser Status is Approved
	 showhidePendReqBtn();
	//end of func
	
	
}





function loaderBlock(){
	
	/*var loadMsg = document.getElementById("loadingMessage");
	var loadingLayer = document.getElementById("loadingLayer");
	
	loadMsg.style.display="block";
	//alert(loadMsg.style.display)
	loadingLayer.style.display="block"; */
}
 

$("#imgPolicyDocs").on("click",function(){
	downloadAndOpenFile(_selectedFnaid,true);
});

function downLoadAllFile(fnaId,msgflag){
	downloadAndOpenFile(fnaId);
}

function downloadAndOpenFile(hrefobj,msgflag){
	
	var fnaid = hrefobj;

	$.ajax({
		type : "POST",
		url : "KycUtilsServlet",
		data :{"dbcall":"FETCHNTUCDOCUMENT","strFnaId":fnaid},
		dataType : "json",
		async : false,
		beforeSend : function() {
			$("#loadingLayer").show();
		},

		success : function(data) {
			var jsnResponse = data;
			
			 if(jsnResponse[0].SESSION_EXPIRY=='SESSION_EXPIRY'){
				  window.location = baseUrl + SESSION_EXP_JSP;
				  return;
			 }
			 
			 if(!jsnResponse[0].NO_RECORDS){
				// poovathi add on 17-03-2020
				 var strNoRecords ="No Document found.";
				 $("#dynaAttachNTUCList").append(strNoRecords);
				 if(msgflag){
					 alert("No Document found.")	 
				 }
				 
				  return;
			 }
			 
			/*ModalPopups.polAttachmentUWPopup("POL_UW_ATTACH", {width : 900,height : 490,onOk : "closeAttachDialog()"});
			$("#POL_UW_ATTACH_okButton").val("CLOSE");
			 */
			$.each(jsnResponse[0].CLIENT_DOCUMENT,function(i,obj){
				loadDocJsnDataNew(obj,fnaid);
			});
			 
		},
		complete : function() {
			$("#loadingMessage").hide();
		},
		error : function() {
			$("#loadingMessage").hide();
		}
	});

}

//added by kavi 21/09/2018
function ModalPopupsCancelKycDoc(selObj){
	
	var loadMsg = document.getElementById("loadingMessage");
	var loadingLayer = document.getElementById("loadingLayer");
	loadMsg.style.display="none";
	
	selObj.value="";
	ModalPopups.Cancel("KycApprovalDoc");
	 
	 
}

function kycCompApproveDocBinary(){
	
	var parameter = "dbcall=KYCDOCTOBINARYDATA"; 
	
	var form = $('#hdnFormKycApprDoc')[0];
    var data = new FormData(form);
	
    $.ajax({
        url : baseUrl+"/KycUtilsServlet?"+parameter,
        type: "POST",
        data :data,
        cache: false,
        enctype: 'multipart/form-data',
        contentType: false,
        processData: false
    }).done(function(response){ //
    	var saveResp=$.parseJSON(response);
		  
		  if(saveResp[0].SESSION_EXPIRY=='SESSION_EXPIRY'){
			  window.location = baseUrl + SESSION_EXP_JSP;
			  return;
		  }
		  
		  if(saveResp[0].STATUS=='SUCCESS'){
			  //alert(saveResp[0].BINARYDATA);
			  ModalPopupsCancelKycDoc();
		  }
		  
		  if(saveResp[0].STATUS=='FAIL'){
			  alert("Failed to convert to base64.");
		  }
    });
    
    ModalPopupsCancelKycDoc();
}


function closeAttachDialog(){
	document.getElementById("uwattachjsppage").appendChild(document.getElementById("polAttachmentJspUW"));
	$("#KycAttachTbl tbody tr").remove();
	ModalPopups.Cancel("POL_UW_ATTACH");
	return true;
}


function downLoadNtucAttach(ntid){
	
	var url = 'DownloadNtucFile?i='+ntid;
	
	window.open(url,"_blank");

}


function DownLoadAllNtucFile(){
	var url = 'DownloadNtucZipFile.action?txtFldFnaPolId='+$("[name=hdnTxtFldNtucFnaId]").val();
	window.open(url,"_blank");

//	window.location.href =url;
}

function reorderTblById(id){
	var count =1;
	$("#"+id+" tbody tr").each(function(){
		$(this).find("td:first input:first").val(count);
		count++;
	});
}



function ajaxCall(parameter) {

	var jsnResponse = "";

	$.ajax({
		type : "POST",
		url :  baseUrl+ "/KYCServlet",
		data : parameter,
		dataType : "json",

		async : false,

		beforeSend : function() {
		},

		success : function(data) {
			jsnResponse = data;
		},
		complete : function() {
		},
		error : function() {
		}
	});

	return jsnResponse;
}

function openEKYCNew(dataset){
 
	var kycformname = "SIMPLIFIED";
	var lobArrtoKYC = "ALL";
	var strLoggedUserDesig ="";
	var strLoggedDistrbutor="";
	//var emailId = dataset[0]["txtFldAdvStfEmailId"]; 
	var formType = "SIMPLIFIED";
	
	
	var fnaId = dataset[1];
	$("#txtFnaId").text(fnaId)
	$("#txtFldCurFnaId").val(fnaId)
	
    getLatestFnaIdDetls(fnaId)
		
	
	
	
	//var custname = dataset[2].toUpperCase();
	//$("#txtFnaId").text(fnaId)
	
	// $("#optgrpPending").append('<option value="'+fnaId+'">'+custname+'</option>');
//	 $('#dropDownQuickLinkFNAHist').val(custname);
//	 $('#dropDownQuickLinkFNAHist').trigger('change.select2');
	var custname = dataset[2];
	//alert("custname"+custname);
	$('#dropDownQuickLinkFNAHist').val(fnaId);
	$('#dropDownQuickLinkFNAHist').trigger('change.select2');
	//poovathi add readOnly Property to clientName on 14-06-2021
	 $("#dropDownQuickLinkFNAHist").prop('disabled', true);
	 
	var custid = dataset[6]; 
	var advId = dataset[7]; 
	var emailId = dataset[8]; 
	
	var machine = "";
	machine = BIRT_URL + "/frameset?__report=" + FNA_RPT_FILELOC //
			+ "&P_CUSTID=" + custid + "&P_FNAID=" + fnaId+"&__format=pdf"
			+ "&P_PRDTTYPE=" + lobArrtoKYC
			+ "&P_CUSTOMERNAME=&P_CUSTNRIC=&P_FORMTYPE=" + kycformname
			+ "&P_DISTID=" + LOGGED_DISTID;
			setTimeout(function(){ 
				$('#cover-spin').show(0);
				$("#dynaFrame").attr("src",machine);
			 }, 0);
		
		var iframe = document.getElementById('dynaFrame');
		iframe.onload = function() {$('#cover-spin').hide(0);}; 
	
	//setTimeout(function(){$('#cover-spin').hide(0);	}, 1500);
	
}



function formatDataSelect2 (data) {
	  if (!data.id) {
	    return data.text;
	  }
	  var $format = $( '<span>' + data.text + '</span>'	  );
	  return $format;
}


function setManagerStatus(currentObj){
	var thisval = $(currentObj).val();
	
	var agree_m ="",disagree_m="",reason_m="",status_m="";
		agreeordis = thisval;	
		reason_m = $("#txtFldMgrApproveRemarks").val();
		
		status_m = thisval;//agree_m ? "APPROVE" : (disagree_m ? "REJECT" : "");
		
		if(isEmpty(status_m)){
			//alert(FNA_APPR_MGR_AGREE );
			//poovathi add on 2-06-21
			Swal.fire(
					  'Note',
					  ''+FNA_APPR_MGR_AGREE+'',
					  'question'
					)
			 $(currentObj).focus();
			//$("#tr_mgr_agreesec").css({"border":"1px solid red"});
			//$("input[name='suprevMgrFlg']").closest("td").css({"background-color":"yellow"});
			return false;
		}
		
		if(!isEmpty(status_m) && status_m == "REJECT"){
			if(isEmpty(reason_m)){
				//alert(MANAGER_KEYIN_REMARKS);
				//poovathi add on 2-06-21
				Swal.fire(
						  'Note',
						  ''+MANAGER_KEYIN_REMARKS+'',
						  'question'
						)
				$(currobj).val("")
				return false;
			}
		}
		//if(flag){
			
			if(!isEmpty(status_m) && status_m == "APPROVE"){
				//poovathi add on 2-06-21
				//poovathi commented on 14-06-2021
			/*	Swal.fire({
					  title: 'Are you sure?',
					  text: " "+CONFIRM_APPROVE_MANAGER+"",
					  icon: 'info',
					  showCancelButton: true,
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'Yes, Approve it!'
					}).then((result) => {
					  if (result.isConfirmed) {
					    Swal.fire(
					      'Success!',
					      'Your FNA has been Approved.',
					      'success'
					    )
					  }else{
						  $(currentObj).val("");
							
							return false;
					  }
					})*/
					
					
				var conf = window.confirm(CONFIRM_APPROVE_MANAGER);
				if(!conf){
				//	var loadMsg = document.getElementById("loadingMessage");
					//var loadingLayer = document.getElementById("loadingLayer");
					//loadMsg.style.display="none";
//					selObj.value = "";
					
					//agreefld.prop("checked",false);
				//	disagreefld.prop("checked",false);
				 $(currentObj).val("");
					
					return false;
				}else{
					//agreefld.prop("checked",true).prop("disabled",true);
					//disagreefld.prop("checked",false).prop("disabled",true);
					
				}	
			}
			
			if(!isEmpty(status_m) && status_m == "REJECT"){
				
				//poovathi add on 2-06-21
				Swal.fire({
					  title: 'Are you sure?',
					  text: " "+CONFIRM_REJECT_MANAGER+"",
					  icon: 'info',
					  showCancelButton: true,
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'Yes, Reject it!'
					}).then((result) => {
					  if (result.isConfirmed) {
					    Swal.fire(
					      'Success!',
					      'Your FNA has been Rejected.',
					      'success'
					    )
					  }else{
						    selObj.value = "";
		 					$(currentObj).val("");
							return false;
					  }
					})
				/*var conf = window.confirm(CONFIRM_REJECT_MANAGER);
				if(!conf){
					//var loadMsg = document.getElementById("loadingMessage");
					//var loadingLayer = document.getElementById("loadingLayer");
					//loadMsg.style.display="none";
					//agreefld.prop("checked",false);
					//disagreefld.prop("checked",false);
//					selObj.value = "";
 					$(currentObj).val("");
					return false;
				}*/
			}
			
		//}
		
		var mangrEmailId = "";//$("#txtFldAdvMgrEmailId").val()
		var mangrId = "";// $("#txtFldMgrId").val()
		var hNTUCPolicyId = "";//$("#ntucCaseId").val();
		var polNo = "";
		
		var advstfEmailId = $("#txtFldAdvEmailId").val();			            	  					
		var advName ="";// $("#txtFldAdvName").val();
		var custName = "";//$("#dfSelfName").val();
		
		advName = $("#txtFldAdvStfName").find("span.adviserNameTab").text();
		custName =  $("#dropDownQuickLinkFNAHist").find("option:selected").text();
		
		
		var approverAdvStfId = "";
		var approverUserId = "";//$("#hTxtFldFnaLoggedUserId").val();
		var strLoggedUserEmailId = "";
		var fnaPrin = "";
		var fnaId= $("#txtFldFnaId").val();
		
		
		var ajaxParam = "dbcall=MANAGER_APPROVE_STATUS"
		+"&txtFldFnaId="+fnaId
		
		+"&txtFldMgrAppStatus="+status_m
		+"&txtFldManagerEmailId="+mangrEmailId
		+"&txtFldManagerId="+mangrId
		+"&txtFldManagerRemarks="+reason_m
		
		+"&hNTUCPolicyId="+hNTUCPolicyId
		+"&txtFldPolNum="+polNo
		
		+"&txtFldAdvEmailId="+advstfEmailId
		+"&txtFldAdviserName="+advName
		+"&txtFldCustName="+custName		
		 
		+"&txtFldApprAdvId="+approverAdvStfId
		+"&txtFldApprUserId="+approverUserId
		+"&txtFldApprUserEmailId="+strLoggedUserEmailId
		+"&txtFldFnaPrin="+fnaPrin
		;
		
		
		setTimeout(function() {
			window.parent.document.getElementById('cover-spin').style.display="block";
		}, 0);
		setTimeout(function() {
			var ajaxResponse = callAjax(ajaxParam,false);
			window.parent.document.getElementById('cover-spin').style.display="block";
			retval = jsonResponse = eval('(' +ajaxResponse+ ')'); 
			 
			
			for ( var val in retval) {
				
				var tabdets = retval[val];
				
				if (tabdets["SESSION_EXPIRY"]) {
					window.location = baseUrl + SESSION_EXP_JSP;
					return;
				}
				for ( var tab in tabdets) {
					if (tabdets.hasOwnProperty(tab)) {
						var value = tabdets[tab];
						
						
						if (tab == "TAB_MANAGER_APPROVE_DETAILS") {
							
							//alert(value[0].MANAGER_POLICY_STATUS);
							
							
							Swal.fire({
//						  	  title: "Clear Signature",
						  	  html: value[0].MANAGER_POLICY_STATUS,
						  	  		
						  	  icon: "info",
//						  	  showCancelButton: true,
						  	  allowOutsideClick:false,
						  	  allowEscapeKey:false,
						  	  confirmButtonClass: "btn-danger",
						  	  confirmButtonText: "OK",
//						  	  cancelButtonText: "No,Cancel",
//						  	  closeOnConfirm: false,
						  	  //showLoaderOnConfirm: true
//						  	  closeOnCancel: false
						  	}).then((result) => {
								  if (result.isConfirmed) {
									  parent.location.reload(true);
								  } 


						  	});
							
							

//							 location.reload(true);
							 
						}
					}
				}
			}
		}, 1000);
}

function setAdminStatus(currobj){
	
	var thisval = $(currobj).val().toUpperCase();
	
	
	//poovathi add on 06-07-2021 for Admin
	if(!isEmpty(thisval) && (thisval != "PENDINGREQ")){
		
		var fnaId = $("#txtFldFnaId").val();
		var mangrEmailId = "";//strLoggedUserEmailId;
		var remarks = $("#txtFldAdminApproveRemarks").val();
		var hNTUCPolicyId = $("#txtFldNTUCCaseId").val();
		var polNo = $("#txtFldNTUCPolNo").val();
		var advName = $("#txtFldAdvStfName").val();
		var custName = $("#txtFldCustName").val();
		var custId = $("#txtFldCustId").val();
		var advstfEmailId = $("#txtFldAdvEmailId").val();
		
		var mgrApproveSts = $("#txtFldMgrApproveStatus").val();
		
		if(mgrApproveSts != "APPROVE") {
			
			Swal.fire({
					width:"650px",
			  	  title: "eKYC Notification",
			  	  html: "Manager is not yet approved this FNA Details!. Once manager approved then admin can proceed to approval process!",
			  	  		
			  	  icon: "error",
//			  	  showCancelButton: true,
			  	  allowOutsideClick:false,
			  	  allowEscapeKey:false,
			  	  confirmButtonClass: "btn-danger",
			  	  confirmButtonText: "OK",
//			  	  cancelButtonText: "No,Cancel",
//			  	  closeOnConfirm: false,
			  	  //showLoaderOnConfirm: true
//			  	  closeOnCancel: false
			  	}).then((result) => {
					  if (result.isConfirmed) {
						  Swal.close();
					  } 

			  	});
			
			$(currobj).val("");
			return false;
		}
		
		var approverAdvStfId = ""
		var approverUserId = ""
		
		var fnaPrin = !isEmpty(hNTUCPolicyId) ? "NTUC" : "NON-NTUC";


			if(thisval == "REJECT" && isEmpty(remarks)){
				
				//poovathi add on 2-06-21
				Swal.fire(
						  'Note',
						  ''+ADMIN_KEYIN_REMARKS+'',
						  'question'
						)
				
				//swal.fire(ADMIN_KEYIN_REMARKS);
				
		    	$("#txtFldAdminApproveRemarks").prop("readOnly",false);
				$("#txtFldAdminApproveRemarks").focus();
				$(currobj).val("")
				return false;
			}
			
			if(thisval == "REJECT"){
				
				/*//poovathi add on 2-06-21
				Swal.fire({
					  title: 'Are you sure?',
					  text: " "+ADMIN_REJECTSTATUS_CONFIRM+"",
					  icon: 'info',
					  showCancelButton: true,
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'Yes, Reject it!'
					}).then((result) => {
					  if (result.isConfirmed) {
					    Swal.fire(
					      'Success!',
					      'Your FNA has been Rejected.',
					      'success'
					    )
					  }else{
						    $(currobj).val("")
							return false;
					  }
					})*/
				
				
				
				var conf = window.confirm(ADMIN_REJECTSTATUS_CONFIRM);
				if(!conf){
					$(currobj).val("")
					return false;
				}
			}else if(thisval == "APPROVE"){
				
				//poovathi add on 2-06-21
				//poovathi commented on 14-06-2021
				/*Swal.fire({
					  title: 'Are you sure?',
					  text: " "+ADMIN_APPROVESTATUS_CONFIRM+"",
					  icon: 'info',
					  showCancelButton: true,
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'Yes, Approve it!'
					}).then((result) => {
					  if (result.isConfirmed) {
					    Swal.fire(
					      'Success!',
					      'Your FNA has been Approved.',
					      'success'
					    )
					  }else{
						  $(currobj).val("")
						  return false;
					  }
					})*/
			   var conf = window.confirm(ADMIN_APPROVESTATUS_CONFIRM);
				if(!conf){
					$(currobj).val("")
					return false;
				}
			}
			
			
			var ajaxParam = 'dbcall=ADMIN_APPROVE_STATUS&txtFldFnaId=' + fnaId +"&txtFldAdminAppStatus="+thisval+
			"&txtFldManagerEmailId="+mangrEmailId+"&txtFldAdminRemarks="+remarks+"&hNTUCPolicyId="+hNTUCPolicyId+"&txtFldPolNum="+polNo+
			"&txtFldAdviserName="+advName+"&txtFldCustName="+custName+"&txtFldAdvEmailId="+advstfEmailId 
			+"&txtFldApprAdvStfId="+approverAdvStfId + "&txtFldApprUserId="+approverUserId+"&txtFldFnaPrin="+fnaPrin+"&txtFldCustId="+custId;
			
			
			setTimeout(function() {
				document.getElementById('cover-spin').style.display="block";
			}, 0);
			
			setTimeout(function() {
				var ajaxResponse = callAjax(ajaxParam,false);
				retval = jsonResponse = eval('(' +ajaxResponse+ ')'); 
				
				for ( var val in retval) {
					
					var tabdets = retval[val];
					
					if (tabdets["SESSION_EXPIRY"]) {
						window.location = baseUrl + SESSION_EXP_JSP;
						return;
					}
					for ( var tab in tabdets) {
						if (tabdets.hasOwnProperty(tab)) {
							var value = tabdets[tab];						
							 
							if (tab == "TAB_ADMIN_APPROVE_DETAILS") {							 
									
								if(value[0].ADMIN_APPROVE_STATUS == "APPROVE"){
									 
									 if(strAdminFlg == "Y" && strCompFlg == "Y"){
										 
									 }
									
								}
								
								if(value[0].ADMIN_APPROVE_STATUS == "APPROVE"){
//									document.getElementById("txtFldAdminApproveStatus").options[0].innerHTML = "Approved";
								}
								if(value[0].ADMIN_APPROVE_STATUS == "REJECT"){
//									document.getElementById("txtFldAdminApproveStatus").options[1].innerHTML = "Rejected";
								}	
								
//								alert(value[0].ADMIN_POLICY_STATUS);
//								window.location.reload(true);
								
								$('#cover-spin').hide(0);
								Swal.fire({
								  	  title: "eKYC Notification",
								  	  html: value[0].ADMIN_POLICY_STATUS,
								  	  		
								  	  icon: "info",
//								  	  showCancelButton: true,
								  	  allowOutsideClick:false,
								  	  allowEscapeKey:false,
								  	  confirmButtonClass: "btn-danger",
								  	  confirmButtonText: "OK",
//								  	  cancelButtonText: "No,Cancel",
//								  	  closeOnConfirm: false,
								  	  //showLoaderOnConfirm: true
//								  	  closeOnCancel: false
								  	}).then((result) => {
										  if (result.isConfirmed) {
											  parent.location.reload(true);
										  } 

								  	});
								
							}
						}	
					}
				}
			}, 1000);
			
			
		
		

		
	}
	
}

function setComplianceStatus(currobj) {
	
	var thisval = $(currobj).val();
	
	
	//poovathi add on 06-07-2021 for Compliance
	if(!isEmpty(thisval) && (thisval != "PENDINGREQ")){
		
			 
		var fnaId = $("#txtFldFnaId").val() 
		var mangrEmailId = strLoggedUserEmailId;
		var remarks = $("#txtFldCompApproveRemarks").val();
		var hNTUCPolicyId = $("#txtFldNTUCCaseId").val(),
		ntucCaseId= $("#txtFldNTUCCaseId").val();
		var polNo = $("#txtFldNTUCPolNo").val();
		var advName = $("#txtFldAdvStfName").val();
		var advstfId = "";
		var custName = $("#txtFldCustName").val();
		var custId = $("#txtFldCustId").val();
		var custNRIC = "";
		var advstfEmailId = $("#txtFldAdvEmailId").val();
		
		var approverAdvStfId = "";
		var approverUserId = "";
		var advstfcode = "";
		
		var ntucCasests = "";
		var ntucPolCrtdFlg = "";
		
		var mgrApproveSts = $("#txtFldMgrApproveStatus").val();
		var adminApproveSts = $("#txtFldAdminApproveStatus").val();
		
		if(mgrApproveSts != "APPROVE") {
			
			Swal.fire({
					width:"650px",
			  	  title: "eKYC Notification",
			  	  html: "Manager is not yet approved this FNA Details!. Once manager approved then admin can proceed to approval process!",
			  	  		
			  	  icon: "error",
//			  	  showCancelButton: true,
			  	  allowOutsideClick:false,
			  	  allowEscapeKey:false,
			  	  confirmButtonClass: "btn-danger",
			  	  confirmButtonText: "OK",
//			  	  cancelButtonText: "No,Cancel",
//			  	  closeOnConfirm: false,
			  	  //showLoaderOnConfirm: true
//			  	  closeOnCancel: false
			  	}).then((result) => {
					  if (result.isConfirmed) {
						  Swal.close();
					  } 

			  	});
			
			$(currobj).val("");
			return false;
		}
		
		if(adminApproveSts != "APPROVE") {
			
			Swal.fire({
					width:"650px",
			  	  title: "eKYC Notification",
			  	  html: "Admin is not yet approved this FNA Details!. Once Admin approved then Compliance can proceed to approval process!",
			  	  		
			  	  icon: "error",
//			  	  showCancelButton: true,
			  	  allowOutsideClick:false,
			  	  allowEscapeKey:false,
			  	  confirmButtonClass: "btn-danger",
			  	  confirmButtonText: "OK",
//			  	  cancelButtonText: "No,Cancel",
//			  	  closeOnConfirm: false,
			  	  //showLoaderOnConfirm: true
//			  	  closeOnCancel: false
			  	}).then((result) => {
					  if (result.isConfirmed) {
						  Swal.close();
					  } 

			  	});
			
			$(currobj).val("");
			return false;
		}
		
		var fnaPrin = !isEmpty(hNTUCPolicyId) ? "NTUC" : "NON-NTUC";

			if(thisval == "REJECT" && isEmpty(remarks)){
				
				Swal.fire(
						  'Note',
						  ''+ADMIN_KEYIN_REMARKS+'',
						  'question'
						)
				
				//alert(ADMIN_KEYIN_REMARKS);
				
				$("#txtFldCompApproveRemarks").removeAttr("readOnly");
				$("#txtFldCompApproveRemarks").focus();
				$(currobj).val("")
				return false;
			}
			
			var jsontext = ""//formJsonText(fnaId,advstfcode);
			
			if(thisval == "REJECT"){
				
				/*Swal.fire({
					  title: 'Are you sure?',
					  text: " "+ADMIN_REJECTSTATUS_CONFIRM+"",
					  icon: 'info',
					  showCancelButton: true,
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'Yes, Reject it!'
					}).then((result) => {
					  if (result.isConfirmed) {
					    Swal.fire(
					      'Success!',
					      'Your FNA has been Rejected.',
					      'success'
					    )
					  }else{
						  $(currobj).val("")
							return false;
					  }
					})*/
					
				var conf = window.confirm(ADMIN_REJECTSTATUS_CONFIRM);
				if(!conf){
					$(currobj).val("")
					return false;
				}
			}else if(thisval == "APPROVE"){
				
				
				//poovathi add on 2-06-21
				//poovathi commented on 14-06-2021
				/*Swal.fire({
					  title: 'Are you sure?',
					  text: " "+ADMIN_APPROVESTATUS_CONFIRM+"",
					  icon: 'info',
					  showCancelButton: true,
					  confirmButtonColor: '#3085d6',
					  cancelButtonColor: '#d33',
					  confirmButtonText: 'Yes, Approve it!'
					}).then((result) => {
					  if (result.isConfirmed) {
					    Swal.fire(
					      'Success!',
					      'Your FNA has been Approved.',
					      'success'
					    )
					  }else{
						  $(currobj).val("")
							return false;
					  }
					})*/
				
				
				var conf = window.confirm(ADMIN_APPROVESTATUS_CONFIRM);
				if(!conf){
					$(currobj).val("")
					return false;
				}
			}
			
			
			if(thisval == "APPROVE" ){
				
				if(fnaPrin == "NTUC"){
					
					$('#modalNTUCApproval').modal({
						  backdrop: 'static',
						  keyboard: false,
						  show:true,
						}); 				
				}else{
					
					
					
					var ajaxParam = 'dbcall=COMP_APPROVE_STATUS&txtFldFnaId=' + fnaId +"&txtFldCompAppStatus="+
					thisval+"&txtFldManagerEmailId="+mangrEmailId+"&txtFldCompRemarks="+remarks+"&hNTUCPolicyId="+hNTUCPolicyId+"&txtFldPolNum="+polNo+
					"&txtFldAdviserName="+advName+"&txtFldCustName="+custName+"&txtFldAdvEmailId="+advstfEmailId 
					+"&txtFldApprAdvStfId="+approverAdvStfId + "&txtFldApprUserId="+approverUserId+"&txtFldNtucCaseId="+hNTUCPolicyId
					+"&txtFldNtucCasests="+ntucCasests+"&txtFldNtucPolCrtdFlg="+ntucPolCrtdFlg
					+"&CASEID="+ntucCaseId+"&CASESTATUS="+thisval+"&ADVSTFCODE="+advstfcode
					+"&FNAID="+fnaId+"&CUSTID="+custId+"&CUSTNRIC="+custNRIC
					+"&SERVADVID="+advstfId+"&txtFldCustId="+custId+"&txtFldFnaPrin="+fnaPrin+"&KYCJSON="+jsontext
					//console.log(ajaxParam);
					
					setTimeout(function() {
						$('#cover-spin').show(0);
						document.getElementById('cover-spin').style.display="block";
					}, 0);
					
					
					setTimeout(function() {
						 $.ajax({
						        url :  baseUrl+ "/KYCServlet?"+ajaxParam,
						        type: "POST",
//						        data :data,
						        cache: false,
						        async:false,
//						        enctype: 'multipart/form-data',
						        contentType: false,
						        processData: false
						    }).done(function(response){ 
						    	document.getElementById('cover-spin').style.display="none";
						    	
						    	var retval =  $.parseJSON(response);
								
								for ( var val in retval) {
									
									var tabdets = retval[val];
									
									if (tabdets["SESSION_EXPIRY"]) {
										window.location = baseUrl + SESSION_EXP_JSP;
										return;
									}
									for ( var tab in tabdets) {
										if (tabdets.hasOwnProperty(tab)) {
											var value = tabdets[tab];
											
//											alert(tab +","+value[0]);
											 
											if (tab == "TAB_COMPILANCE_REJECT_DETAILS") {
											//	alert(value[0].COMP_POLICY_STATUS);
												
												if("APPROVED" == value[0].COMP_POLICY_STATUS ) {
													Swal.fire({
														  title: "Policy has been Approved by Compliance",
														  html: "Do you want to upload this FNA document to <strong>FPMS</strong> client attachments?",
														  icon: 'question',
														  allowOutsideClick:false,
														  allowEscapeKey:false,
														  showCancelButton: true,
														  confirmButtonColor: '#3085d6',
														  cancelButtonColor: '#d33',
														  confirmButtonText: 'Yes, Proceed'
														}).then((result) => {
														  if (result.isConfirmed) {
														    	
//														    	var cfnaId=$("#hdnSessFnaId").val();
//														    	Cookies.remove(cfnaId, { path: '/' })
													
															$('#cover-spin').show(0);
															setTimeout(function(){insertCustAttach()},1200);
																
																
														  }
														  
														  if(result.isDismissed){
															  
															  window.location.reload(true);
														  }
														})
													
													
												}else {
													
													
													$('#cover-spin').hide(0);
													Swal.fire({
													  	  title: "eKYC Notification",
													  	  html: "Policy has been Rejected by Compliance",
													  	  		
													  	  icon: "info",
//													  	  showCancelButton: true,
													  	  allowOutsideClick:false,
													  	  allowEscapeKey:false,
													  	  confirmButtonClass: "btn-danger",
													  	  confirmButtonText: "OK",
//													  	  cancelButtonText: "No,Cancel",
//													  	  closeOnConfirm: false,
													  	  //showLoaderOnConfirm: true
//													  	  closeOnCancel: false
													  	}).then((result) => {
															  if (result.isConfirmed) {
																  parent.location.reload(true);
															  } 

													  	});
													
												}
													
												
												
												
												
												
											}
											
											
											
											
											
											if (tab == "TAB_COMPILANCE_REJECT_DETAILS_ERROR") {
//												alert(value);
												
												$('#cover-spin').hide(0);
												Swal.fire({
												  	  title: "eKYC Notification",
												  	  html: value,
												  	  		
												  	  icon: "info",
//												  	  showCancelButton: true,
												  	  allowOutsideClick:false,
												  	  allowEscapeKey:false,
												  	  confirmButtonClass: "btn-danger",
												  	  confirmButtonText: "OK",
//												  	  cancelButtonText: "No,Cancel",
//												  	  closeOnConfirm: false,
												  	  //showLoaderOnConfirm: true
//												  	  closeOnCancel: false
												  	}).then((result) => {
														  if (result.isConfirmed) {
															  parent.location.reload(true);
														  } 

												  	});
												
												
											}
										}
									}
								}
						    });
					}, 1000);
				}
				
			}else if(thisval == "REJECT"){
				
				var ajaxParam = 'dbcall=COMP_APPROVE_STATUS&txtFldFnaId=' + fnaId +"&txtFldCompAppStatus="+
				thisval+"&txtFldManagerEmailId="+mangrEmailId+"&txtFldCompRemarks="+remarks+"&hNTUCPolicyId="+hNTUCPolicyId+"&txtFldPolNum="+polNo+
				"&txtFldAdviserName="+advName+"&txtFldCustName="+custName+"&txtFldAdvEmailId="+advstfEmailId 
				+"&txtFldApprAdvStfId="+approverAdvStfId + "&txtFldApprUserId="+approverUserId+"&txtFldNtucCaseId="+ntucCaseId
				+"&txtFldNtucCasests="+ntucCasests+"&txtFldNtucPolCrtdFlg="+ntucPolCrtdFlg
				+"&CASEID="+ntucCaseId+"&CASESTATUS="+thisval+"&ADVSTFCODE="+advstfcode
				+"&FNAID="+fnaId+"&KYCJSON="+jsontext+"&CUSTID="+custId+"&CUSTNRIC="+custNRIC
				+"&SERVADVID="+advstfId+"&txtFldCustId="+custId+"&txtFldFnaPrin="+fnaPrin;
				
				
				//console.log(ajaxParam);
			    
				
				setTimeout(function() {
					document.getElementById('cover-spin').style.display="block";
				}, 0);
				
				
				setTimeout(function() {
					$.ajax({
				        url : baseUrl+ "/KYCServlet?"+ajaxParam,
				        type: "POST",
//				        data :data,
				        cache: false,
//				        enctype: 'multipart/form-data',
				        contentType: false,
				        processData: false,
				        async:false
				    }).done(function(response){ 
				    	
				    	document.getElementById('cover-spin').style.display="none";
				    	
				    	var retval =  $.parseJSON(response);
						
						for ( var val in retval) {
							
							var tabdets = retval[val];
							
							if (tabdets["SESSION_EXPIRY"]) {
								window.location = baseUrl + SESSION_EXP_JSP;
								return;
							}
							for ( var tab in tabdets) {
								if (tabdets.hasOwnProperty(tab)) {
									var value = tabdets[tab];
									
									 
									if (tab == "TAB_COMPILANCE_REJECT_DETAILS") {
										
										
//										alert(value[0].COMP_POLICY_STATUS);
										
										$('#cover-spin').hide(0);
										Swal.fire({
										  	  title: "eKYC Notification",
										  	  html: "Policy has been Rejected by Compliance",
										  	  		
										  	  icon: "info",
//										  	  showCancelButton: true,
										  	  allowOutsideClick:false,
										  	  allowEscapeKey:false,
										  	  confirmButtonClass: "btn-danger",
										  	  confirmButtonText: "OK",
//										  	  cancelButtonText: "No,Cancel",
//										  	  closeOnConfirm: false,
										  	  //showLoaderOnConfirm: true
//										  	  closeOnCancel: false
										  	}).then((result) => {
												  if (result.isConfirmed) {
													  parent.location.reload(true);
												  } 

										  	});
										
//										window.location.reload(true);
										
									}
									
									if(tab == "UPDATEKYC2_STATUS"){
										//alert(value);
										parent.location.reload(true);
									}
									
									if(tab == "UPDATEKYC2_DSF_STATUS" ){
										if(value.toLowerCase() == "success"){
										
										}else if(value.toLowerCase() == "failure"){
											

											
										}
										
									}
									
									if (tab == "TAB_COMPILANCE_REJECT_DETAILS_ERROR") {
									//	alert(value);
										parent.location.reload(true);
										
									}
								}
							}
						}
				    });
				}, 1000);
			    
			    
				
			}
			
			
			
			
	}
	
}

function logoutKyc() {

window.location.href=baseUrl+"/logout";
	 
}


function validateNTUCApprDoc(){

	
	var file = document.getElementById("fileKycCompApprove").value;

	if(isEmpty(file)){
		alert("Select the document");
		return false;
	}else{
		
		
		var fnaId = $("#txtFldFnaId").val() 
		var mangrEmailId = strLoggedUserEmailId;
		var remarks = $("#txtFldAdminApproveRemarks").val();
		var hNTUCPolicyId = $("#txtFldNTUCCaseId").val(),
		ntucCaseId = $("#txtFldNTUCCaseId").val();
		var polNo = $("#txtFldNTUCPolNo").val();
		var advName = $("#txtFldAdvStfName").val();
		var advstfId = "";
		var custName = $("#txtFldCustName").val();
		var custId = $("#txtFldCustId").val();
		var custNRIC = "";
		var advstfEmailId = $("#txtFldAdvEmailId").val();
		
		var approverAdvStfId = "";
		var approverUserId = "";
		var advstfcode = "";
		
		var jsontext = ""//formJsonText(fnaId,advstfcode);
		var thisval = $("#txtFldCompApproveStatus").val();
		
		var ntucCasests = $("#txtFldNTUCCaseStatus").val();
		var ntucPolCrtdFlg = "";
		
		var fnaPrin = !isEmpty(hNTUCPolicyId) ? "NTUC" : "NON-NTUC";

		
		
		var ajaxParam = 'dbcall=COMP_APPROVE_STATUS&txtFldFnaId=' + fnaId +"&txtFldCompAppStatus="+
		thisval+"&txtFldManagerEmailId="+mangrEmailId+"&txtFldCompRemarks="+remarks+"&hNTUCPolicyId="+hNTUCPolicyId+"&txtFldPolNum="+polNo+
		"&txtFldAdviserName="+advName+"&txtFldCustName="+custName+"&txtFldAdvEmailId="+advstfEmailId 
		+"&txtFldApprAdvStfId="+approverAdvStfId + "&txtFldApprUserId="+approverUserId+"&txtFldNtucCaseId="+hNTUCPolicyId
		+"&txtFldNtucCasests="+ntucCasests+"&txtFldNtucPolCrtdFlg="+ntucPolCrtdFlg
		+"&CASEID="+ntucCaseId+"&CASESTATUS="+thisval+"&ADVSTFCODE="+advstfcode
		+"&FNAID="+fnaId+"&CUSTID="+custId+"&CUSTNRIC="+custNRIC
		+"&SERVADVID="+advstfId+"&txtFldCustId="+custId+"&txtFldFnaPrin="+fnaPrin+"&KYCJSON="+jsontext
		//console.log(ajaxParam)
		
		
		var form = $('#hdnFormKycApprDoc')[0];
		var data = new FormData(form);
		
		
		$.ajax({
	        url :  baseUrl+ "/KYCServlet?"+ajaxParam,
	        type: "POST",
	        data :data,
	        cache: false,
	        enctype: 'multipart/form-data',
	        contentType: false,
	        processData: false
	    }).done(function(response){ 
	    	
	    	
//	    	loadMsg.style.display="none";	 
	    	
	    	var retval =  $.parseJSON(response);
			
			for ( var val in retval) {
				
				var tabdets = retval[val];
				
				if (tabdets["SESSION_EXPIRY"]) {
					window.location = baseUrl + SESSION_EXP_JSP;
					return;
				}
				for ( var tab in tabdets) {
					if (tabdets.hasOwnProperty(tab)) {
						var value = tabdets[tab];
						
						 
						if (tab == "TAB_COMPILANCE_REJECT_DETAILS") {
							
							
							alert(value[0].COMP_POLICY_STATUS);
							
						}
						
						if(tab == "UPDATEKYC2_STATUS"){
							alert(value);
						}
						
						
						if(tab == "UPDATEKYC2_DSF_STATUS" ){
//							alert(value.toLowerCase())
							if(value.toLowerCase() == "success"){
							
							}else if(value.toLowerCase() == "failure"){
								

								
							}
							
						}
						
						
						if (tab == "TAB_COMPILANCE_REJECT_DETAILS_ERROR") {
							alert(value);
							
						}
					}
				}
			}
	    });
	}
	return true;
	
}


//poovathi commened on 16-06-2021 (function not used)
/*
function loadDocJsnDataNew(jsnData,fnaid){
	
	
	//check empty field validation for AttachId and InsurarName
	if(isEmpty(jsnData[1])){
		jsnData[1] = "-NIL-";
	}else{
		jsnData[1] = jsnData[1];
	}
	
	if(isEmpty(jsnData[5])){
		jsnData[5] = "-NIL-";
	}else{
		jsnData[5] = jsnData[5];
	}
	
	//poovathi add on 16-06-2021
	//Filetype
	if(isEmpty(jsnData[6])){
		jsnData[6] = "-NIL-";
	}else{
		jsnData[6] = jsnData[6];
	}
	//Filename
	if(isEmpty(jsnData[7])){
		jsnData[7] = "-NIL-";
	}else{
		jsnData[7] = jsnData[7];
	}
	//Filesize
	if(isEmpty(jsnData[8])){
		jsnData[8] = "-NIL-";
	}else{
		jsnData[8] = jsnData[8];
	}
	//Validation End

	var docid = jsnData.txtFldDocNdId;
	
	
	
	var strList ='<a href="#" class="list-group-item list-group-item-action p-2" id="'+docid+'" onmouseover="showFileDetls(this)" onmouseout="hideFileDetls(this)">'+
		    '<div class="row">'+
		    '<div class="col-8"><h6 class="mb-1 font-sz-level5 text-custom-color-gp bold"><span class="text-primary">Category : </span> '+jsnData[1]+'</h6></div>'+
		    '<div class="col-1"><small><i class="fa fa-trash cursor-pointer hide" style="color: #f04040;" onclick=deleteDoc(this) title="Delete Doc."></i></small></div>'+
		    '<div class="col-1"><small><i class="fa fa-download cursor-pointer" style="color: #007bff;" onclick=downloadDoc(\"'+docid+'\") title="Click to Download Doc."></i></small></div>'+
		 '</div>'+
   
     
	     '<div class="row">'+
		     '<div class="col-12"><small class="font-sz-level6"><span class="text-primary bold">Title : </span> '+jsnData[3]+'</small></div>'+
		      '<div class="col-12"><small class="font-sz-level6"><span class="text-primary bold" style="font-weight: 450;">Insurer Name :</span> '+jsnData[5]+'</small> </div>'+
		      remarksLbl+
	    '<hr/></div>'+
     //poovathi add on 16-06-2021
    '<div class="row hide">'+
     '<div class="col-12"><small class="font-sz-level9"><span class="text-primary bold">File Name : </span> '+jsnData[6]+'</small></div>'+
     '<div class="col-12"><small class="font-sz-level9"><span class="text-primary bold">File Size : </span> '+jsnData[7]+'</small></div>'+
      '<div class="col-12"><small class="font-sz-level9"><span class="text-primary bold">File Type : </span> '+jsnData[8]+'</small> </div>'+
    '</div>'+
 '</a>';

$("#dynaAttachNTUCList").append(strList);

var totlDocListLen = $("#dynaAttachNTUCList").children().length;
if(totlDocListLen > 0){
$("#noDocFoundSec").addClass("hide").removeClass("show")
$("#DocListSec").addClass("show").removeClass("hide")
}else{
$("#noDocFoundSec").addClass("show").removeClass("hide")
$("#DocListSec").addClass("hide").removeClass("show")
}

$('#dynaAttachNTUCList a:even').addClass('bsc-pln-bg');
}


//poovathi add on 16-06-2021
function showFileDetls(fldObj){
	$(fldObj).children().eq(2).addClass("show").removeClass("hide");
}
function hideFileDetls(fldObj){
	$(fldObj).children().eq(2).addClass("hide").removeClass("show");
}//end
*/


		
	
	

function populateApprovedRec(dataset,loggedUser){
	
	var fnaId = dataset[1];
	var custname = dataset[2].toUpperCase();
	var mgrstatus = dataset[19]//value[0]["txtFldMgrApproveStatus"];
    var adminstatus = dataset[20]//value[0]["txtFldAdminApproveStatus"];
    var compstatus = dataset[21]//value[0]["txtFldCompApproveStatus"];
	var custid = dataset[6]; 
	var advId = dataset[7]; 
	var emailId = dataset[8]; 
    var ntucCaseId = ""//value[0]["txtFldFnaNtucCaseId"];
var mgrapprby = dataset[10];
var mgrapprdate=dataset[11];
var mgrremarks = dataset[12];    
var adminapprby = dataset[13];
var adminapprdate=dataset[14];
var adminremarks = dataset[15];
var compapprby = dataset[16];
var compapprdate=dataset[17];
var compremarks = dataset[18];    
	
	
	//$("#dropDownQuickLinkFNAHist").val(custname);
	
	
	// $("#optgrpPending").append('<option value="'+fnaId+'">'+custname+'</option>');
	// $('#dropDownQuickLinkFNAHist').val(custname);
	// $('#dropDownQuickLinkFNAHist').trigger('change.select2');
	 
	/*$('#dropDownQuickLinkFNAHist').select2({
			width :'400',				
			 templateResult: formatDataSelect2
		})*/
	 $("#landingpage").find("li:eq(1) a").trigger("click");

	$("#txtFldFnaId").val(fnaId)
	$("#txtFldCustName").val(custname)
//	$("#txtFldAdvStfName").val(advId);
	$("#spanLoggedAdviser").text($("#txtFldAdvStfName").val())	
	$("#txtFldMgrName").val();
	
    //poovathi get hidden text field values below on 16-03-2020
	$("#txtFldNTUCCaseId").val(ntucCaseId);
	$("#txtFldCustId").val(custid);
	//alert(custid)
	//setcustId to hidden field
	$("#hTxtFldCurrCustId").val(custid);
	$("#txtFldAdvEmailId").val(emailId);
	
	if(isEmpty(ntucCaseId)){
		$("#ntucblock").addClass("fa fa-times-circle-o");
	}else{
		$("#ntucblock").addClass("fa fa-check-square-o").removeClass("fa fa-times-circle-o");
	}
	
	
	//MANAGER	    
	if((mgrstatus.toUpperCase()) == "APPROVE"){
		
		$("#mngrapprLbl").text('Approved Date');
		$("#mngrapprByLbl").text('Approved By');
		$("#iFontMgr").removeClass("fa fa-question-circle");
		$("#iFontMgr").removeClass("fa fa-times-circle-o");
		$("#iFontMgr").addClass("fa fa-check-circle-o");
		
		//poovathi add on 02-06-2021
		$("#iFontMgr").parent().attr("title","Manager Status : " + mgrstatus.toUpperCase());
		
		
		
		$("#txtFldMgrApproveStatus").prop("disabled",true);
		$("#txtFldMgrApproveRemarks").prop("readOnly",true);
		
		//poovathi add on 02-07-2021 (hide pending req btn)
		$("#btnSendReq").removeClass("show").addClass("hide");
		
	}else if((mgrstatus.toUpperCase()) == "REJECT") {
		
		$("#mngrapprLbl").text('Rejected Date');
		$("#mngrapprByLbl").text('Rejected By');
		$("#iFontMgr").removeClass("fa fa-check-circle-o");
		$("#iFontMgr").removeClass("fa fa-question-circle");
		$("#iFontMgr").addClass("fa fa-times-circle-o");
		
		//poovathi add on 02-06-2021
		$("#iFontMgr").parent().attr("title","Manager Status : " + mgrstatus.toUpperCase());
		
		//poovathi add on 02-07-2021 (hide pending req btn)
		$("#btnSendReq").removeClass("show").addClass("hide");
		
	}else {
		
		$("#mngrapprLbl").text('');
		$("#mngrapprByLbl").text('');
		//$("#mngrapprByLbl").text('Yet to Approve By');
		$("#iFontMgr").removeClass("fa fa-times-circle-o")
		$("#iFontMgr").removeClass("fa fa-check-circle-o")
		$("#iFontMgr").addClass("fa fa-question-circle");
		
		//poovathi add on 02-06-2021
		$("#iFontMgr").parent().attr("title","Manager Status : PENDING");
		
		//poovathi add on 02-07-2021 (show pending req btn)
		$("#btnSendReq").removeClass("hide").addClass("show");
	}
	
//ADMIN
	
	if(adminstatus.toUpperCase() == "APPROVE"){

		$("#admnapprLbl").text('Approved Date');
		$("#admnapprByLbl").text('Approved By');
		$("#iFontAdmin").removeClass("fa fa-times-circle-o")
		$("#iFontAdmin").removeClass("fa fa-question-circle");
		$("#iFontAdmin").addClass("fa fa-check-circle-o");
		
		//poovathi add on 02-06-2021
		$("#iFontAdmin").parent().attr("title","Admin Status : " + adminstatus.toUpperCase());
		
		//poovathi add on 02-07-2021 (hide pending req btn)
		$("#btnSendReq").removeClass("show").addClass("hide");
		
	}else if((adminstatus.toUpperCase()) == "REJECT") {
		
		$("#admnapprLbl").text('Rejected Date');
		$("#admnapprByLbl").text('Rejected By');
		$("#iFontAdmin").removeClass("fa fa-check-circle-o");
		$("#iFontAdmin").removeClass("fa fa-question-circle");
		$("#iFontAdmin").addClass("fa fa-times-circle-o");
		//poovathi add on 02-06-2021
		$("#iFontAdmin").parent().attr("title","Admin Status : " + adminstatus.toUpperCase());
		
		//poovathi add on 02-07-2021 (hide pending req btn)
		$("#btnSendReq").removeClass("show").addClass("hide");
		
	 }else{
		$("#admnapprLbl").text('');
		$("#admnapprByLbl").text('');
		//$("#admnapprByLbl").text('Yet to Approve By');
		$("#iFontAdmin").removeClass("fa fa-times-circle-o")
		$("#iFontAdmin").removeClass("fa fa-check-circle-o")
		$("#iFontAdmin").addClass("fa fa-question-circle");
		
		//poovathi add on 02-06-2021
		$("#iFontAdmin").parent().attr("title","Admin  Status : PENDING");
		
		//poovathi add on 02-07-2021 (show pending req btn)
		$("#btnSendReq").removeClass("hide").addClass("show");
	}
	
	//COMPLIANCE					
	if(compstatus.toUpperCase() == "APPROVE"){
		
		$("#compapprLbl").text('Approved Date');
		$("#compapprByLbl").text('Approved By');
		$("#iFontComp").removeClass("fa fa-times-circle-o")
		$("#iFontComp").removeClass("fa fa-question-circle");
		$("#iFontComp").addClass("fa fa-check-circle-o")
		
		//poovathi add on 02-06-2021
		$("#iFontComp").parent().attr("title","Compliance Status : " + compstatus.toUpperCase());
		
		//poovathi add on 02-07-2021 (hide pending req btn)
		$("#btnSendReq").removeClass("show").addClass("hide");
		
	}else if(compstatus.toUpperCase() == "REJECT") {
		
		$("#compapprLbl").text('Rejected Date');
		$("#compapprByLbl").text('Rejected By');
		$("#iFontComp").removeClass("fa fa-check-circle-o")
		$("#iFontComp").removeClass("fa fa-question-circle");
		$("#iFontComp").addClass("fa fa-times-circle-o");
		
		//poovathi add on 02-06-2021
		$("#iFontComp").parent().attr("title","Compliance Status : " + compstatus.toUpperCase());
		
		//poovathi add on 02-07-2021 (hide pending req btn)
		$("#btnSendReq").removeClass("show").addClass("hide");
		
	 }else {
		 
		$("#compapprLbl").text('');
		$("#compapprByLbl").text('');
		//$("#compapprByLbl").text('Yet to Approve By');
		$("#iFontComp").removeClass("fa fa-times-circle-o")
		$("#iFontComp").removeClass("fa fa-check-circle-o")
		$("#iFontComp").addClass("fa fa-question-circle");
		
		//poovathi add on 02-06-2021
		$("#iFontComp").parent().attr("title","Compliance  Status : PENDING");
		
		//poovathi add on 02-07-2021 (show pending req btn)
		$("#btnSendReq").removeClass("hide").addClass("show");
	}
	
// FOR MANAGER
	$("#txtFldMgrApproveStatus").val(mgrstatus)
	$("#txtFldMgrApproveDate").val(mgrapprdate);
	$("#txtFldMgrApproveRemarks").val(mgrremarks);	
	$("#txtFldMgrStsApprBy").val(mgrapprby);
	setApprovalStatus($("#txtFldMgrApproveStatus"));
	
// FOR ADMIN				
	$("#txtFldAdminApproveStatus").val(adminstatus);
	$("#txtFldAdminApproveDate").val(adminapprdate);
	$("#txtFldAdminApproveRemarks").val(adminremarks);
	$("#txtFldAdminStsApprBy").val(adminapprby);
	setApprovalStatus($("#txtFldAdminApproveStatus"));
	
// FOR COMPLIANCE	
	$("#txtFldCompApproveStatus").val(compstatus);
	$("#txtFldCompApproveDate").val(compapprdate);
	$("#txtFldCompApproveRemarks").val(compremarks);
	$("#txtFldCompStsApprBy").val(compapprby);
	setApprovalStatus($("#txtFldCompApproveStatus"));
	
	var mgrApprStatus = mgrstatus;//$("#txtFldMgrApproveStatus").val();
	
	var adminApprStatus = adminstatus;//$("#txtFldAdminApproveStatus").val();
	
	var compApprStatus = compstatus;//$("#txtFldCompApproveStatus").val();
	
    openEKYCNew(dataset);

	//downLoadAllFile(fnaId,false);
	//alert(custid+""+fnaId)
	getAllExisitingDocs(custid,fnaId);
//	alert(loggedUser)
	
	if(loggedUser == "ADMIN" ){//|| loggedUser == "COMPANDADMIN"
		
		$("#appr-mgr-tab").addClass("no-drop1").removeClass("active");
		$("#appr-mgr-tab-cont").removeClass("show").removeClass("active");;
		
    	$("#appr-admin-tab").removeClass("no-drop1");
    	$("#appr-admin-tab").addClass("active");
    	$("#appr-admin-tab-cont").addClass("show").addClass("active");
    	$("#txtFldAdminApproveRemarks").prop("readOnly",false);
    	 
    	$("#appr-comp-tab").addClass("no-drop1");	
    	$("#appr-comp-tab").removeClass("show").removeClass("active");
    	$("#appr-comp-tab-cont").removeClass("show").removeClass("active");
    	
    	setApprovalStatus($("#txtFldAdminApproveStatus"));
	}else if(loggedUser == "COMPLIANCE"){//|| loggedUser == "COMPANDADMIN"
		
		var tabfocus = true;
		
		$("#appr-mgr-tab").addClass("no-drop1").removeClass("active");
		$("#appr-mgr-tab-cont").addClass("show").removeClass("active");
		
		$("#appr-admin-tab").addClass("no-drop1").removeClass("active");				    					    	
    	$("#appr-admin-tab-cont").addClass("show").removeClass("active");				    	
    	$("#txtFldAdminApproveRemarks").prop("readOnly",true);
    	 
    	$("#appr-comp-tab").removeClass("no-drop1");
    	$("#appr-comp-tab").removeClass("show");
    	$("#appr-comp-tab").addClass("active");
    	$("#appr-comp-tab-cont").addClass("show").addClass("active");
		
		if(strAdminFlg == "Y" && strCompFlg == "Y"){
			
			if(adminApprStatus == "APPROVE"){}else{}
			
			setApprovalStatus($("#txtFldAdminApproveStatus"));
			
		}else if(strAdminFlg != "Y" && strCompFlg == "Y"){
			
/*			$("#appr-admin-tab").addClass("no-drop1").removeClass("active");				    					    	
	    	$("#appr-admin-tab-cont").removeClass("show").removeClass("active");				    	
	    	$("#txtFldAdminApproveRemarks").prop("readOnly",true);
	    	 
	    	$("#appr-comp-tab").removeClass("show").addClass("active");
	    	$("#appr-comp-tab").addClass("active");
	    	$("#appr-comp-tab-cont").addClass("show").addClass("active");
*/			
		}
		
		
		setApprovalStatus($("#txtFldCompApproveStatus"));
		
	}else if(loggedUser == "COMPANDADMIN"){//|| loggedUser == "COMPANDADMIN"
		
		var tabfocus = true;
		
		$("#appr-mgr-tab").addClass("no-drop1").removeClass("active");
		$("#appr-mgr-tab-cont").addClass("show").removeClass("active");
		
		$("#appr-admin-tab").addClass("no-drop1").removeClass("active");				    					    	
    	$("#appr-admin-tab-cont").addClass("show").removeClass("active");				    	
    	$("#txtFldAdminApproveRemarks").prop("readOnly",true);
    	 
    	$("#appr-comp-tab").removeClass("no-drop1");
    	$("#appr-comp-tab").removeClass("show");
    	$("#appr-comp-tab").addClass("active");
    	$("#appr-comp-tab-cont").addClass("show").addClass("active");
		
		if(strAdminFlg == "Y" && strCompFlg == "Y"){
			
			if(adminApprStatus == "APPROVE"){
				
				$("#appr-comp-tab").removeClass("no-drop1");
		    	$("#appr-comp-tab").removeClass("show");
		    	$("#appr-comp-tab").addClass("active");
		    	$("#appr-comp-tab-cont").addClass("show").addClass("active");
		    	//poovathi add on 06-07-2021 (show pending req btn)
				$("#btnSendReq").removeClass("show").addClass("hide");
			}else{
				
				
		    	$("#appr-admin-tab").removeClass("no-drop1");
		    	$("#appr-admin-tab").addClass("active");
		    	$("#appr-admin-tab-cont").addClass("show").addClass("active");
		    	$("#txtFldAdminApproveRemarks").prop("readOnly",false);
		    	 
		    	$("#appr-comp-tab").addClass("no-drop1");	
		    	$("#appr-comp-tab").removeClass("show").removeClass("active");
		    	$("#appr-comp-tab-cont").removeClass("show").removeClass("active");

		    	
			}
			
			setApprovalStatus($("#txtFldAdminApproveStatus"));
			
		}else if(strAdminFlg != "Y" && strCompFlg == "Y"){
			
/*			$("#appr-admin-tab").addClass("no-drop1").removeClass("active");				    					    	
	    	$("#appr-admin-tab-cont").removeClass("show").removeClass("active");				    	
	    	$("#txtFldAdminApproveRemarks").prop("readOnly",true);
	    	 
	    	$("#appr-comp-tab").removeClass("show").addClass("active");
	    	$("#appr-comp-tab").addClass("active");
	    	$("#appr-comp-tab-cont").addClass("show").addClass("active");
*/			
		}
		
		
		setApprovalStatus($("#txtFldCompApproveStatus"));
		
	}
	
	
	
	
	/*if(mgrApprStatus.toUpperCase() == "APPROVE"){
		$("#appr-mgr-tab-cont").find(":input").prop("disabled",true);
	}else{
		$("#appr-mgr-tab-cont").find(":input").prop("disabled",false);
	}*/
	
	if(adminApprStatus.toUpperCase() == "APPROVE"){
		$("#appr-admin-tab-cont").find(":input").prop("disabled",true);
		//poovathi add on 06-07-2021 (show pending req btn)
		$("#btnSendReq").removeClass("show").addClass("hide");
	}else{
		$("#appr-admin-tab-cont").find(":input").prop("disabled",false);
	}
	
	if(compApprStatus.toUpperCase() == "APPROVE"){
		$("#appr-comp-tab-cont").find(":input").prop("disabled",true);
		//poovathi add on 06-07-2021 (show pending req btn)
		$("#btnSendReq").removeClass("show").addClass("hide");
	}else{
		$("#appr-comp-tab-cont").find(":input").prop("disabled",false);
	}
	
	
	
	if(ntuc_policy_access_flag == "true" && compApprStatus.toUpperCase() == "APPROVE"){			
		 $("#imgpolapi").show()		
	}else{
		 $("#imgpolapi").hide()
	}
	
	//poovathi add on 06-07-2021
	showhidePendReqBtn();
	
}

function generateFNA(){
//	$("#dynaFrame").get(0).contentWindow.print();

	
//	var machine = "";
//	machine = BIRT_URL + "/frameset?__report=" + FNA_RPT_FILELOC //
//			+ "&P_CUSTID=" + custid + "&P_FNAID=" + fnaId+"&__format=pdf"
//			+ "&P_PRDTTYPE=" + lobArrtoKYC
//			+ "&P_CUSTOMERNAME=&P_CUSTNRIC=&P_FORMTYPE=" + kycformname
//			+ "&P_DISTID=" + LOGGED_DISTID;
//	
//	alert(machine)
	
}


function setApprovalStatus(object){
	var thisval = $(object).val();
//	thisval = $(object).find('option[value="' + thisval+ '"]').html();
//	  option = options.find("[value='" + i + "']");
	  
	if(thisval == "APPROVE"){
		$(object).find('option:contains("Approve")').text('Approved');
//		document.getElementById($(object).prop("id")).options[0].innerHTML = "Approved";
		//poovathi add on 02-07-2021
		$("#btnSendReq").removeClass("show").addClass("hide");
	}
	if(thisval == "REJECT"){
		$(object).find('option:contains("Reject")').text('Rejected');
//		document.getElementById($(object).prop("id")).options[1].innerHTML = "Rejected";
		
		//poovathi add on 02-07-2021
		$("#btnSendReq").removeClass("show").addClass("hide");
	}
	
	//poovathi add on 06-07-2021
	if(isEmpty(thisval) || (thisval == "PENDINGREQ")){
		$("#btnSendReq").removeClass("hide").addClass("show");
	}
	
}


function openList(statusMsg){
	 $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
 if(statusMsg == "PENDING"){
	  $("#yourAprovTabs").find("li:eq(0) a").trigger("click");	
	  $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
    }
  
  if(statusMsg == "APPROVED"){
	$("#yourAprovTabs").find("li:eq(1) a").trigger("click");	
	$.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
    }
  
  if(statusMsg == "REJECTED"){
	$("#yourAprovTabs").find("li:eq(2) a").trigger("click");
	$.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
   }
  
  //poovathi add on 02-07-2021
  if(statusMsg == "PENDINGREQ"){
	$("#yourAprovTabs").find("li:eq(3) a").trigger("click");
	$.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
   }
   //End
}




/*PIE CHART START*/
function sliceSize(dataNum, dataTotal) {
	  return (dataNum / dataTotal) * 360;
	}
	function addSlice(sliceSize, pieElement, offset, sliceID, color) {
	  $(pieElement).append("<div class='slice "+sliceID+"'><span></span></div>");
	  var offset = offset - 1;
	  var sizeRotation = -179 + sliceSize;
	  $("."+sliceID).css({
	    "transform": "rotate("+offset+"deg) translate3d(0,0,0)"
	  });
	  $("."+sliceID+" span").css({
	    "transform"       : "rotate("+sizeRotation+"deg) translate3d(0,0,0)",
	    "background-color": color
	  });
	}
	function iterateSlices(sliceSize, pieElement, offset, dataCount, sliceCount, color) {
	  var sliceID = "s"+dataCount+"-"+sliceCount;
	  var maxSize = 179;
	  if(sliceSize<=maxSize) {
	    addSlice(sliceSize, pieElement, offset, sliceID, color);
	  } else {
	    addSlice(maxSize, pieElement, offset, sliceID, color);
	    iterateSlices(sliceSize-maxSize, pieElement, offset+maxSize, dataCount, sliceCount+1, color);
	  }
	}
	function createPie(dataElement, pieElement) {
	  var listData = [];
	  $(dataElement+" span").each(function() {
	    listData.push(Number($(this).html()));
	  });
	  var listTotal = 0;
	  for(var i=0; i<listData.length; i++) {
	    listTotal += listData[i];
	  }
	  var offset = 0;
	  var color = [ "#333E48", 	    "#009A78",	    "#d9534f"   , "#17a2b8" ];
	  for(var i=0; i<listData.length; i++) {
	    var size = sliceSize(listData[i], listTotal);
	    iterateSlices(size, pieElement, offset, i, 0, color[i]);
	    $(dataElement+" li:nth-child("+(i+1)+")").css("border-color", color[i]);
	    offset += size;
	  }
	}
/*PIE CHART END*/

	
	

function getQrorSign(){
	
	//poovathi add on 01-06-2021
	  var selectedFnaId = $("#txtFldCurFnaId").val();
		if(isEmpty(selectedFnaId)){
			curntFnaId = $("#hTxtFldFnaId").val();
		}else{
			curntFnaId = selectedFnaId;
		}
		
		getSignPageData(curntFnaId);
		
		getLatestFnaIdDetls(curntFnaId)
		
		getAllSign(curntFnaId);
			
	   var ff = getCurrFNASigns(curntFnaId);
	
		if(!ff) {
			toastr.clear($('.toast'));
			toastr.options = {  timeOut: "5000", extendedTimeOut: "1000"};
			toastr["error"]("Client(1)/Client(2) and Adviser Signature cannot be empty!");
			$("#openMngesecModal").modal("hide")
			return;	
		}
	
	var mgrSts = $("#txtFldMgrApproveStatus").val();
	
	if(mgrSts == "APPROVE"){
		var mgrName = $("#txtFldMgrStsApprBy").val();
		var clnt = $("#dropDownQuickLinkFNAHist").find("option:selected").text();
		
		//swal.fire("Manager Status","Manager :"+ mgrName + " already Approved the FNA Form for Client : " + clnt)
		toastr.clear($('.toast'));
		toastr.options = {  timeOut: "5000", extendedTimeOut: "1000"};
		toastr["success"](" Manager : "+ mgrName + " has been   approved the FNA Form for Client : " + clnt);
	}
	
	//poovathi add on 02-07-2021
	if((mgrSts == "REJECT")||(isEmpty(mgrSts)) || (mgrSts == "PENDINGREQ")){
		
		$("#openMngesecModal").modal({
		    backdrop: 'static',
		    keyboard: false,
		    show: true
		 });
		        //poovathi add on 01-06-2021
		       if((mgrSts == "REJECT")){
					 $("#suprevMgrFlgD").prop("checked",true);
					 $("#suprevMgrFlgA").prop("checked",false);
				}else{
					 $("#suprevMgrFlgA").prop("checked",false);
					 $("#suprevMgrFlgd").prop("checked",false);
				}
		}
	
	}//func end

   function getAllExisitingDocs(currentCustId,fnaId){
		
		$("#dynaAttachNRICList").empty();
		
	    $.ajax({
	    	     url:baseUrl+ "/CustAttach/getAllNricCustDocs",
	            data:{"custid":currentCustId,"fnaId":fnaId},
	            type: "GET",
	            async:false,
                success: function (response) {
                 for(var d=0;d<response.length;d++){
                     loadDocJsnDataNew(response[d],null)
            	}
            	
            	if(response.length == 0) {
                    $("#dynaAttachNRICList").html("-No Refernce Docs-");
					
					var totlDocListLen = $("#dynaAttachNTUCList").children().length;

					$("#noDocFoundSec").addClass("show").removeClass("hide")
					$("#DocListSec").addClass("hide").removeClass("show")
		            //poovathi add on 17-06-2021
					$("#fileCountTxt").html("");
            	}else{
					$("#noDocFoundSec").addClass("hide").removeClass("show")
					$("#DocListSec").addClass("show").removeClass("hide")
					//poovathi add on 17-06-2021
					$("#fileCountTxt").html("Total Files  : "+'<span><span class="badge badge-pill badge-light font-sz-level7">'+response.length+'</span></span>');
            	}
            	
            },
            error: function(xhr,textStatus, errorThrown){
	 			//$('#cover-spin').hide(0);
	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			}
            });
	}
	
	
	function loadDocJsnDataNew(jsnData,fnaid){

		var docid = jsnData[0];
		

		var remarks = isEmpty(jsnData[2]) ? "" : jsnData[2];
		var remarks = isEmpty(jsnData[2]) ? "" : jsnData[2];
		
		var remarksLbl =isEmpty(remarks)?
						'':'<div class="col-12 font-sz-level8"><span class="text-dark bold">Remarks : </span> '+remarks+'</div>'

		
		if(isEmpty(jsnData[5])){
			jsnData[5] = "-NIL-";
		}else{
			jsnData[5] = jsnData[5];
		}

		

		//poovathi add on 16-06-2021
		//Filetype
		if(isEmpty(jsnData[6])){
			jsnData[6] = "-NIL-";
		}else{
			jsnData[6] = jsnData[6];
		}
		//Filename
		if(isEmpty(jsnData[7])){
			jsnData[7] = "-NIL-";
		}else{
			jsnData[7] = jsnData[7];
		}
		//Filesize
		if(isEmpty(jsnData[8])){
			jsnData[8] = "-NIL-";
		}else{
			jsnData[8] = jsnData[8];
		}
		//Validation End

		var docid = jsnData[0];
		
		
		
		var strList ='<a href="#" class="list-group-item list-group-item-action p-2" id="'+docid+'" onmouseover="showFileDetls(this)" onmouseout="hideFileDetls(this)">'+
			    '<div class="row">'+
				    '<div class="col-8"><h6 class="mb-1 font-sz-level7 text-custom-color-gp bold"><span class="text-dark">Category : </span> '+jsnData[1]+'</h6></div>'+
				    '<div class="col-1"><small><i class="fa fa-trash cursor-pointer hide" style="color: #f04040;" onclick=deleteDoc(this) title="Delete Doc."></i></small></div>'+
				    '<div class="col-1"><small><i class="fa fa-download cursor-pointer" style="color: #007bff;" onclick=downloadDoc(\"'+docid+'\") title="Click to Download Doc."></i></small></div>'+
			 '</div>'+
	   
	     
		      '<div class="row">'+
				     '<div class="col-12"><small class="font-sz-level7"><span class="text-dark bold">Title : </span> '+jsnData[3]+'</small></div>'+
				      '<div class="col-12"><small class="font-sz-level7"><span class="text-dark bold">Insurer Name :</span> '+jsnData[5]+'</small> </div>'+
				      
		     '</div>'+
	     //poovathi add on 16-06-2021
		     '<div class="row hide">'+
			     '<div class="col-12"><small class="font-sz-level8"><span class="text-dark bold">File Name : </span> '+jsnData[6]+'</small></div>'+
			     '<div class="col-12"><small class="font-sz-level8"><span class="text-dark bold">File Size : </span> '+bytesToSize(jsnData[7])+'</small></div>'+
			     '<div class="col-12"><small class="font-sz-level8"><span class="text-dark bold">File Type : </span> '+jsnData[8]+'</small> </div>'+
			     remarksLbl+
		     '</div>'+
	    '</a>';

				
		
		$("#dynaAttachNRICList").append(strList);
		
//		var totlDocListLen = $("#dynaAttachNTUCList").children().length;
//		if(totlDocListLen > 0){
//		$("#noDocFoundSec").addClass("hide").removeClass("show")
//		$("#DocListSec").addClass("show").removeClass("hide")
//		}else{
//		$("#noDocFoundSec").addClass("show").removeClass("hide")
//		$("#DocListSec").addClass("hide").removeClass("show")
//		}

		$('#dynaAttachNTUCList a:even').addClass('bsc-pln-bg');

	}
	
	//poovathi add on 16-06-2021
	function showFileDetls(fldObj){
		$(fldObj).children().eq(2).addClass("show").removeClass("hide");
	}
	function hideFileDetls(fldObj){
		$(fldObj).children().eq(2).addClass("hide").removeClass("show");
	}//end

	function deleteDoc(thisObj){
		var cusAttachId =  $(thisObj).parents(".list-group-item").attr("id");
		//alert(cusAttachId)
				Swal.fire({
				  title: 'Are you sure?',
				  text: "Want to Delete the Document?",
				  icon: 'warning',
		          allowOutsideClick:false,
		          allowEscapeKey:false,
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Yes, Delete it!'
				}).then((result) => {
				  if (result.isConfirmed) {
					  
					  $.ajax({
				   			
				   			 url:"CustAttach/delete/"+cusAttachId,
				   			 type: "POST",async:false,
				            // contentType: "application/json",
				 	            success: function (response) {
				 	            	  var totlDocListLen = $("#dynaAttachNTUCList").children().length;
									  
									  if(totlDocListLen > 1){
										    $(thisObj).parents(".list-group-item").remove();
										    hideNoFileFoundImg();
										 }else{
											$(thisObj).parents(".list-group-item").remove();
											showNoFileFoundImg();
										}
									  
									  swal.fire("Deleted!", "File / Document has been deleted.", "success");
									  
									//poovathi add on 15-06-2021 (after delete change file count)
									  //var listlen = $("#dynaAttachNTUCList").children().length;
									  //$("#fileCountTxt").html("Total Files  : "+'<span><span class="badge badge-pill badge-light font-sz-level7">'+listlen+'</span></span>');
									  },
									  error: function(xhr,textStatus, errorThrown){
								 			//$('#cover-spin').hide(0);
								 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
										}
								  
								});
				 		 			
				 	            }else{
									  swal.fire("Cancelled", "File / Document detail is safe :)", "error");
								}
				 		 		
				 	            });
					        
	}

	function hideNoFileFoundImg(){
	$("#noDocFoundSec").addClass("hide").removeClass("show")
	$("#DocListSec").addClass("show").removeClass("hide")
	}

	function showNoFileFoundImg(){
	$("#noDocFoundSec").addClass("show").removeClass("hide")
	$("#DocListSec").addClass("hide").removeClass("show")
	}
	
	function downloadDoc(id){
		var url = baseUrl+'/downloadDoc/'+id;
		window.open(url,"_blank");
	}

	//manager click  sendTo Approve button
	function mngrSendToApprove(){
		
		if(!validateAgDagFld()){return;}
		var status_m ="",reason_m="";
		
		var disAgreeFlg = $("#suprevMgrFlgD").is(":checked");
		var AgreeFlg = $("#suprevMgrFlgA").is(":checked");
		var mngrVal= $('input[name="suprevMgrFlg"]:checked').val();
		
		 reason_m = $("#suprevFollowReason").val();
	
		if(disAgreeFlg == true){
			 
			 if(!validateDisagreeReasonFld()){return;}
			 
			 //trigger send mail to Manager
			// sendMailtoManger();
			 status_m ="REJECT";
		}
		
		if(AgreeFlg == true){
			
			 status_m ="APPROVE";
			
			//open signature Modal
			/*$("#ScanToSignModal").modal({
			    backdrop: 'static',
			    keyboard: false,
			    show: true
			  })*/
	
		
		}
		
		
		
		
		var mangrEmailId = "";//$("#txtFldAdvMgrEmailId").val()
		var mangrId = "";// $("#txtFldMgrId").val()
		var hNTUCPolicyId = "";//$("#ntucCaseId").val();
		var polNo = "";
		
		var advstfEmailId = $("#txtFldAdvEmailId").val();			            	  					
		var advName ="";// $("#txtFldAdvName").val();
		var custName = "";//$("#dfSelfName").val();
		
		advName = $("#txtFldAdvStfName").find("span.adviserNameTab").text();
		custName =  $("#dropDownQuickLinkFNAHist").find("option:selected").text();
		
		var approverAdvStfId = "";
		var approverUserId = "";//$("#hTxtFldFnaLoggedUserId").val();
		var strLoggedUserEmailId = "";
		var fnaPrin = "";
		var fnaId= $("#txtFldFnaId").val();
		
		
		var ajaxParam = "dbcall=MANAGER_APPROVE_STATUS"
		+"&txtFldFnaId="+fnaId
		
		+"&txtFldMgrAppStatus="+status_m
		+"&txtFldManagerEmailId="+mangrEmailId
		+"&txtFldManagerId="+mangrId
		+"&txtFldManagerRemarks="+reason_m
		
		+"&hNTUCPolicyId="+hNTUCPolicyId
		+"&txtFldPolNum="+polNo
		
		+"&txtFldAdvEmailId="+advstfEmailId
		+"&txtFldAdviserName="+advName
		+"&txtFldCustName="+custName		
		 
		+"&txtFldApprAdvId="+approverAdvStfId
		+"&txtFldApprUserId="+approverUserId
		+"&txtFldApprUserEmailId="+strLoggedUserEmailId
		+"&txtFldFnaPrin="+fnaPrin
		;
		
		
	//	alert(ajaxParam +"<ajaxParam")
		
		setTimeout(function() {
			window.parent.document.getElementById('cover-spin').style.display="block";
		}, 0);
		setTimeout(function() {
			var ajaxResponse = callAjax(ajaxParam,false);
			window.parent.document.getElementById('cover-spin').style.display="block";
			retval = jsonResponse = eval('(' +ajaxResponse+ ')'); 
			 
			
			for ( var val in retval) {
				
				var tabdets = retval[val];
				
				if (tabdets["SESSION_EXPIRY"]) {
					window.location = baseUrl + SESSION_EXP_JSP;
					return;
				}
				for ( var tab in tabdets) {
					if (tabdets.hasOwnProperty(tab)) {
						var value = tabdets[tab];
						
						
						if (tab == "TAB_MANAGER_APPROVE_DETAILS") {
							
							
							
							$('#cover-spin').hide(0);
							
//							alert(value[0].MANAGER_POLICY_STATUS);
//							parent.location.reload(true);
							
							Swal.fire({
							  	  title: "eKYC Notification",
							  	  html: value[0].MANAGER_POLICY_STATUS,
							  	  		
							  	  icon: "info",
//							  	  showCancelButton: true,
							  	  allowOutsideClick:false,
							  	  allowEscapeKey:false,
							  	  confirmButtonClass: "btn-danger",
							  	  confirmButtonText: "OK",
//							  	  cancelButtonText: "No,Cancel",
//							  	  closeOnConfirm: false,
							  	  //showLoaderOnConfirm: true
//							  	  closeOnCancel: false
							  	}).then((result) => {
									  if (result.isConfirmed) {
										  parent.location.reload(true);
									  } 

							  	});


//							 location.reload(true);
							 
						}
					}
				}
			}
		}, 1000);
		
	}

	function validateAgDagFld(){
		
		var disAgreeFlg = $("#suprevMgrFlgD").is(":checked");
		var AgreeFlg = $("#suprevMgrFlgA").is(":checked");
		
		//console.log("disAgreeFlg"+disAgreeFlg+" "+"AgreeFlg"+AgreeFlg)
		
	    if((disAgreeFlg == false)&&(AgreeFlg == false)){
	    	$("#mngrBtnErrorMsg").html("Manager must select any one the options above !")
	        $("#errorFld").css("border","1px solid red")
	    	 return;
	     }else{
	    	 $("#mngrBtnErrorMsg").html("");
	    	 $("#errorFld").css("border",""); 
	     }
	      return true;
	}


	function validateDisagreeReasonFld(){
		
		var mngrDisReason = $("#suprevFollowReason").val();
		if(isEmpty(mngrDisReason)){
			$("#mngrDisagrReason").html("please state the Reasons and Follow-up Action for Disagree the option!");
			$("#suprevFollowReason").focus();
			$("#suprevFollowReason").addClass("err-fld");
			return;
		}else{
			$("#mngrDisagrReason").html("");
			$("#suprevFollowReason").removeClass("err-fld");
		}
		
		return true;
	}

	function hideErrmsg(obj){
		if($(obj).val().length>0){
			$("#mngrDisagrReason").html("");
			$("#suprevFollowReason").removeClass("err-fld");	
		}
	}
		
		
		

	
	function insertCustAttach() {
		
		$('#cover-spin').show(0);
		
		var fnaId = $("#hTxtFldCurrFNAId").val() , 
				custId =$("#hTxtFldCurrCustId").val() , 
				advName= $("#txtFldAdvStfName").val(),
				custName = $("#txtFldCustName").val();
		
		var advName = $("#txtFldAdvStfName").val();
		var custName = $("#txtFldCustName").val();
		
		
		var ajaxParam = 'dbcall=INSERT_CUSTOMER_ATTACHMENTS&paramFnaId='+ fnaId + "&paramcustId=" + custId 				
				+ "&paramAdviserName=" + advName +"&paramCustName="+custName;
		
		var ajaxResponse = callAjax(ajaxParam, false);
		retval = jsonResponse = eval('(' + ajaxResponse + ')');

		for ( var val in retval) {

			var tabdets = retval[val];

			if (tabdets["SESSION_EXPIRY"]) {
				window.location = baseUrl + SESSION_EXP_JSP;
				return;
			}
			for ( var tab in tabdets) {
				if (tabdets.hasOwnProperty(tab)) {
					var value = tabdets[tab];

					if (tab == "CUST_ATTACH_ID") {
//						custattachId = value[0].txtFlsCustAttachId;
						
						if(value == "FAIL") {
//							alert("FNA Document upload failed, try again")
							$('#cover-spin').hide(0);
						}else {
							
							
							$('#cover-spin').hide(0);
							Swal.fire({
//							  	  title: "FNA Form is uploaded successfully in FPMS client attachments!",
							  	  html:"FNA Form is uploaded successfully in FPMS client attachments!",							  	  		
							  	  icon: "info",
//							  	  showCancelButton: true,
							  	  allowOutsideClick:false,
							  	  allowEscapeKey:false,
							  	  confirmButtonClass: "btn-danger",
							  	  confirmButtonText: "OK",
//							  	  cancelButtonText: "No,Cancel",
//							  	  closeOnConfirm: false,
							  	  //showLoaderOnConfirm: true
//							  	  closeOnCancel: false
							  	}).then((result) => {
									  if (result.isConfirmed) {
										  parent.location.reload(true);
									  } 

							  	});
							
//							alert("FNA Document is added to customer!")	
						}
						
						
						
						
//						window.location.reload(true);
					}

					
				}
			}
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	function loadALLFNAId(){
		
		if(strALLFNAIds != null){
		
			var retval = strALLFNAIds;

			for ( var val in retval) {
				var tabdets = retval[val];
				for ( var tab in tabdets) {
					if (tabdets.hasOwnProperty(tab)) {
						
						var key = tab;
						var value = tabdets[tab];
					        if (key == "MANAGER_FNA_DETAILS" || key == "ADMIN_FNA_DETAILS" || key == "COMPLIANCE_FNA_DETAILS") {
							 var ResultLength = value.length;
							 
							  for(var len=0;len<ResultLength;len++){
								  
								  
				                var status = "";
				                
								if(key == "MANAGER_FNA_DETAILS"){
									status = value[len]["txtFldMgrApproveStatus"];
								}else if(key == "ADMIN_FNA_DETAILS"){
									status = value[len]["txtFldAdminApproveStatus"];
								}else if(key == "COMPLIANCE_FNA_DETAILS"){
									status = value[len]["txtFldCompApproveStatus"];
								}
								
								
	//PENDING  Table List
								if(isEmpty(status)){
		                    	   
							        var fnaid = value[len]["txtFldFnaId"],
							        custname = value[len]["txtFldCustName"];
							        pendingdatatable.row.add( [
							                    pendingdatatable.rows().count()+1 ,
							                    value[len]["txtFldFnaId"],
							                    value[len]["txtFldCustName"],
							                    (value[len]["txtFldMgrApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldMgrApproveStatus"] == "REJECT" ? "Rejected" : value[len]["txtFldMgrApproveStatus"] == "PENDINGREQ" ? "Pending Request for Further Requirement" : "Pending" ),
							                    (value[len]["txtFldAdminApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldAdminApproveStatus"] == "REJECT" ? "Rejected" : value[len]["txtFldAdminApproveStatus"] == "PENDINGREQ" ? "Pending Request for Further Requirement" : "Pending" ),
							                    (value[len]["txtFldCompApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldCompApproveStatus"] == "REJECT" ? "Rejected" : value[len]["txtFldCompApproveStatus"] == "PENDINGREQ" ? "Pending Request for Further Requirement" : "Pending" ),
							                    //(value[len]["txtFldMgrApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldMgrApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
							                    //(value[len]["txtFldAdminApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldAdminApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
							                    //(value[len]["txtFldCompApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldCompApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
							                    value[len]["txtFldCustId"],
							                    value[len]["txtFldAdvStfId"],
							                    value[len]["txtFldAdvStfEmailId"],
							                    (value[len]["txtFldFnaPrin"] == "NTUC" ? '<i class="fa fa-check-square"></i>' : '<i class="fa fa-window-close-o"></i>'),
	   											value[len]["txtFldMgrStsApprBy"],value[len]["txtFldMgrApproveDate"],value[len]["txtFldMgrApproveRemarks"],
											    value[len]["txtFldAdminStsApprBy"],value[len]["txtFldAdminApproveDate"],value[len]["txtFldAdminApproveRemarks"],
											    value[len]["txtFldCompStsApprBy"],value[len]["txtFldCompApproveDate"],value[len]["txtFldCompApproveRemarks"],
												value[len]["txtFldMgrApproveStatus"],value[len]["txtFldAdminApproveStatus"],value[len]["txtFldCompApproveStatus"],value[len]["txtFldAdvStfName"]
							                ] ).draw( false );
							       
							        
							        	  $("#optgrpPending").append('<option value="'+fnaid+'">'+custname+'</option>');
							        
								         //console.log("Pending"+ "  "+fnaid+" "+ custname )
								}
								
							
	//APPROVE Table List							
								if(status.toUpperCase() == "APPROVE"){
									
									
									approveddatatable.row.add( [
											                    approveddatatable.rows().count()+1 ,
											                    value[len]["txtFldFnaId"],
											                    value[len]["txtFldCustName"],
											                    (value[len]["txtFldMgrApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldMgrApproveStatus"] == "REJECT" ? "Rejected" : value[len]["txtFldMgrApproveStatus"] == "PENDINGREQ" ? "Pending Request for Further Requirement" : "Pending" ),
											                    (value[len]["txtFldAdminApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldAdminApproveStatus"] == "REJECT" ? "Rejected" : value[len]["txtFldAdminApproveStatus"] == "PENDINGREQ" ? "Pending Request for Further Requirement" : "Pending" ),
											                    (value[len]["txtFldCompApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldCompApproveStatus"] == "REJECT" ? "Rejected" : value[len]["txtFldCompApproveStatus"] == "PENDINGREQ" ? "Pending Request for Further Requirement" : "Pending" ),
											                   // (value[len]["txtFldMgrApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldMgrApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
											                   // (value[len]["txtFldAdminApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldAdminApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
											                   // (value[len]["txtFldCompApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldCompApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
											                    value[len]["txtFldCustId"],
											                    value[len]["txtFldAdvStfId"],
											                    value[len]["txtFldAdvStfEmailId"],
											                    (value[len]["txtFldFnaPrin"] == "NTUC" ? '<i class="fa fa-check-square"></i>' : '<i class="fa fa-window-close-o"></i>'),
											                    value[len]["txtFldMgrStsApprBy"],value[len]["txtFldMgrApproveDate"],value[len]["txtFldMgrApproveRemarks"],
											                    value[len]["txtFldAdminStsApprBy"],value[len]["txtFldAdminApproveDate"],value[len]["txtFldAdminApproveRemarks"],
											                    value[len]["txtFldCompStsApprBy"],value[len]["txtFldCompApproveDate"],value[len]["txtFldCompApproveRemarks"],
																value[len]["txtFldMgrApproveStatus"],value[len]["txtFldAdminApproveStatus"],value[len]["txtFldCompApproveStatus"],value[len]["txtFldAdvStfName"]
											                ] ).draw( false );
//									
									 var fnaid = value[len]["txtFldFnaId"],
								        custname = value[len]["txtFldCustName"] ;
									 
									
							        		  $("#optgrpApproved").append('<option value="'+fnaid+'">'+custname +'</option>');
							        		  
							        		  //console.log("Approve"+ "  "+fnaid+" "+ custname )
									 
									 
								}
								
								
//		REJECTED Table List						
								if(status.toUpperCase() == "REJECT"){
									
									
									 var fnaid = value[len]["txtFldFnaId"],
								        custname = value[len]["txtFldCustName"];
									 
									rejecteddatatable.row.add( [
											                    rejecteddatatable.rows().count()+1 ,
											                    value[len]["txtFldFnaId"],
											                    value[len]["txtFldCustName"],
											                    (value[len]["txtFldMgrApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldMgrApproveStatus"] == "REJECT" ? "Rejected" : value[len]["txtFldMgrApproveStatus"] == "PENDINGREQ" ? "Pending Request for Further Requirement" : "Pending" ),
											                    (value[len]["txtFldAdminApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldAdminApproveStatus"] == "REJECT" ? "Rejected" : value[len]["txtFldAdminApproveStatus"] == "PENDINGREQ" ? "Pending Request for Further Requirement" : "Pending" ),
											                    (value[len]["txtFldCompApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldCompApproveStatus"] == "REJECT" ? "Rejected" : value[len]["txtFldCompApproveStatus"] == "PENDINGREQ" ? "Pending Request for Further Requirement" : "Pending" ),
											                    //(value[len]["txtFldMgrApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldMgrApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
											                    //(value[len]["txtFldAdminApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldAdminApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
											                    //(value[len]["txtFldCompApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldCompApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
											                    value[len]["txtFldCustId"],
											                    value[len]["txtFldAdvStfId"],
											                    value[len]["txtFldAdvStfEmailId"],
											                    (value[len]["txtFldFnaPrin"] == "NTUC" ? '<i class="fa fa-check-square"></i>' : '<i class="fa fa-window-close-o"></i>'),
											                    value[len]["txtFldMgrStsApprBy"],value[len]["txtFldMgrApproveDate"],value[len]["txtFldMgrApproveRemarks"],
											                    value[len]["txtFldAdminStsApprBy"],value[len]["txtFldAdminApproveDate"],value[len]["txtFldAdminApproveRemarks"],
											                    value[len]["txtFldCompStsApprBy"],value[len]["txtFldCompApproveDate"],value[len]["txtFldCompApproveRemarks"],
																value[len]["txtFldMgrApproveStatus"],value[len]["txtFldAdminApproveStatus"],value[len]["txtFldCompApproveStatus"],value[len]["txtFldAdvStfName"]
											                ] ).draw( false );
									
							        		 $("#optgrpRejected").append('<option value="'+fnaid+'">'+custname +'</option>');
							        		 
							        		 //console.log("Reject"+ "  "+fnaid+" "+ custname )
									
									
								}
								
								// poovathi add  PENDINGREQ datas to pendingreqdatatable on 02-07-2021
								if(status.toUpperCase() == "PENDINGREQ"){
									 var fnaid = value[len]["txtFldFnaId"],
								         custname = value[len]["txtFldCustName"];
									     pendingreqdatatable.row.add( [
									    	                    pendingreqdatatable.rows().count()+1 ,
											                    value[len]["txtFldFnaId"],
											                    value[len]["txtFldCustName"],
											                    (value[len]["txtFldMgrApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldMgrApproveStatus"] == "REJECT" ? "Rejected" : value[len]["txtFldMgrApproveStatus"] == "PENDINGREQ" ? "Pending Request for Further Requirement" : "Pending" ),
											                    (value[len]["txtFldAdminApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldAdminApproveStatus"] == "REJECT" ? "Rejected" : value[len]["txtFldAdminApproveStatus"] == "PENDINGREQ" ? "Pending Request for Further Requirement" : "Pending" ),
											                    (value[len]["txtFldCompApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldCompApproveStatus"] == "REJECT" ? "Rejected" : value[len]["txtFldCompApproveStatus"] == "PENDINGREQ" ? "Pending Request for Further Requirement" : "Pending" ),
											                    //(value[len]["txtFldMgrApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldMgrApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
											                    //(value[len]["txtFldAdminApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldAdminApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
											                    //(value[len]["txtFldCompApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldCompApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
											                    value[len]["txtFldCustId"],
											                    value[len]["txtFldAdvStfId"],
											                    value[len]["txtFldAdvStfEmailId"],
											                    (value[len]["txtFldFnaPrin"] == "NTUC" ? '<i class="fa fa-check-square"></i>' : '<i class="fa fa-window-close-o"></i>'),
											                    value[len]["txtFldMgrStsApprBy"],value[len]["txtFldMgrApproveDate"],value[len]["txtFldMgrApproveRemarks"],
											                    value[len]["txtFldAdminStsApprBy"],value[len]["txtFldAdminApproveDate"],value[len]["txtFldAdminApproveRemarks"],
											                    value[len]["txtFldCompStsApprBy"],value[len]["txtFldCompApproveDate"],value[len]["txtFldCompApproveRemarks"],
																value[len]["txtFldMgrApproveStatus"],value[len]["txtFldAdminApproveStatus"],value[len]["txtFldCompApproveStatus"],value[len]["txtFldAdvStfName"]
											                ] ).draw( false );
									 $("#optgrpPendingReq").append('<option value="'+fnaid+'">'+custname +'</option>');
								}//End
								
						}
							  
							  $('#cover-spin').hide(0);
							
							}					
						if (key == "MANAGER_NO_RECORDS_FNA_DETAILS") {
							  $('#cover-spin').hide(0);
							return false;
						}
						
					}
				}
			}
			 
			var currFNAID = $("#hTxtFldFnaId").val();
			
		//	alert(custname)
		//    $("#dropDownQuickLinkFNAHist").val(custname.toUpperCase())
		//	$("#dropDownQuickLinkFNAHist").val(currFNAID);
			
			 $('#tblPending tbody').on('click', 'tr', function () {
				 $(this).toggleClass('selected');
		            var data = pendingdatatable.row( this ).data();
		           // getSelectedFNADetsNew(data);
					openEKYCNew(data);
					
					populateApprovedRec(data,acsmode);
										
		        } );
			 
			 $('#tblApproved tbody').on('click', 'tr', function () {
				 $(this).toggleClass('selected');
		            var data = approveddatatable.row( this ).data();
		           // getSelectedFNADetsNew(data);
					openEKYCNew(data);
					
					populateApprovedRec(data,acsmode);
					
						
		        } );
			 
			 $('#tblRejected tbody').on('click', 'tr', function () {
				 $(this).toggleClass('selected');
		            var data = rejecteddatatable.row( this ).data();
		           // getSelectedFNADetsNew(data);

					openEKYCNew(data);
					
					populateApprovedRec(data,acsmode);
		        } );
			  
			  //poovathi add on 02-07-2021
			  $('#tblPendingReq tbody').on('click', 'tr', function () {
					 $(this).toggleClass('selected');
			            var data = pendingreqdatatable.row( this ).data();
			            openEKYCNew(data);
						populateApprovedRec(data,acsmode);
			       });
			  //End 
			 
		
		pendingdatatable.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
	   		 var data = this.data();
	  		if(data[1] == currFNAID){
	  			
		           	 $(this).addClass('selected');
		           	 
		           	$("#suprevFollowReason").val("")
					$("#diManagerSection").find('input:radio').prop('checked',false);
					
					$("#txtFldMgrSignDateDeclarepage").prop('readOnly',true);
					
					$("#diManagerSection").find(":input").removeAttr("disabled");
					  $("#diManagerSection").removeClass("disabledsec")
					  
					  

						$("#txtFldMgrStsApprBy").val("");
						$("#txtFldAdminStsApprBy").val("");
						$("#txtFldCompStsApprBy").val("");
						
//						$("#hdnSessFnaId").val(data[1] );
//						callBodyInit();
						
						getAllSign(data[1] );
						
						openEKYCNew(data);
						
						populateApprovedRec(data,acsmode);
						
						


												
		           }else{
		        	   $(this).removeClass('highlight');
		           }
		});
		
		
		approveddatatable.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
	   		 var data = this.data();

	  		if(data[1] == currFNAID){
		           	 $(this).addClass('selected');
						
						$("#suprevFollowReason").val("")
						$("#diManagerSection").find('input:radio').prop('checked',false);
						
						$("#txtFldMgrSignDateDeclarepage").prop('readOnly',true);
						
						$("#diManagerSection").find(":input").removeAttr("disabled");
						  $("#diManagerSection").removeClass("disabledsec")
						  
						  

							$("#txtFldMgrStsApprBy").val("");
							$("#txtFldAdminStsApprBy").val("");
							$("#txtFldCompStsApprBy").val("");
							
//							$("#hdnSessFnaId").val(data[1] );
//							callBodyInit();
							
							getAllSign(data[1] );

							openEKYCNew(data);
							
							populateApprovedRec(data,acsmode);
							
												
		           }else{
		        	   $(this).removeClass('highlight');
		           }
		});
		
		rejecteddatatable.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
	   		 var data = this.data();

	  		if(data[1] == currFNAID){
	  			
		           	  $(this).addClass('selected');
						
						$("#suprevFollowReason").val("")
						$("#diManagerSection").find('input:radio').prop('checked',false);
						
						$("#txtFldMgrSignDateDeclarepage").prop('readOnly',true);
						
						$("#diManagerSection").find(":input").removeAttr("disabled");
						  $("#diManagerSection").removeClass("disabledsec")
						  
						  

							$("#txtFldMgrStsApprBy").val("");
							$("#txtFldAdminStsApprBy").val("");
							$("#txtFldCompStsApprBy").val("");
							
//							$("#hdnSessFnaId").val(data[1] );
//							callBodyInit();
							
							getAllSign(data[1] );

							openEKYCNew(data);
							
							populateApprovedRec(data,acsmode);
							
												
		           }else{
		        	   $(this).removeClass('highlight');
		           }
		  });
			
			 
		//poovathi add on 02-07-2021
		  pendingreqdatatable.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
	   		 var data = this.data();
                 if(data[1] == currFNAID){
	  			     $(this).addClass('selected');
						
						$("#suprevFollowReason").val("")
						$("#diManagerSection").find('input:radio').prop('checked',false);
						
						$("#txtFldMgrSignDateDeclarepage").prop('readOnly',true);
						
						$("#diManagerSection").find(":input").removeAttr("disabled");
						$("#diManagerSection").removeClass("disabledsec");
						
						    $("#txtFldMgrStsApprBy").val("");
							$("#txtFldAdminStsApprBy").val("");
							$("#txtFldCompStsApprBy").val("");
							
							getAllSign(data[1] );
                            openEKYCNew(data);
							populateApprovedRec(data,acsmode);
					 }else{
		        	   $(this).removeClass('highlight');
		           }
		});
		//End
		
		
			 
			 $('#tblApproved').on('mousemove', 'tr', function(e) {
				   var rowData = approveddatatable.row(this).data();
				   
				   var info = '<h6>'+rowData[1]+'<br/><small>(Click the row to view the details)</small></h6><small><strong>'+rowData[3]+'</strong> by(Manager) : <strong>'+rowData[10]+'</strong>'+
				   '<hr/>';
				   if(rowData[4] != "Pending"){
					   info += ' <strong>'+rowData[4]+'</strong> by(Admin) : '+rowData[12]+'<hr/>';
				   }
				   if(rowData[5] != "Pending"){
					   info += '<strong>'+rowData[5]+'</strong> by(Compliance) : '+rowData[14]+'<hr/>';
				   }
				   
				   
				   info += '</small>';
				   
				   $("#tooltip").html(info).animate({ left: e.pageX, top: e.pageY }, 1);
				   if (!$("#tooltip").is(':visible')) $("#tooltip").show();
				})
				
				$('#tblApproved').on('mouseleave', function(e) {
				  $("#tooltip").hide();
				}) ;
			 
			 $('#tblRejected').on('mousemove', 'tr', function(e) {
				   var rowData = rejecteddatatable.row(this).data();
				   
				   var info = '<h6>'+rowData[1]+'</h6><small>(Click the row to view the details)</small><br/><small><strong>'+rowData[3]+'</strong> by(Manager) : <strong>'+rowData[10]+'</strong>'+
				   '<hr/>';
				   if(rowData[4] != "Pending"){
					   info += ' <strong>'+rowData[4]+'</strong> by(Admin) : '+rowData[12]+'<hr/>';
				   }
				   if(rowData[5] != "Pending"){
					   info += '<strong>'+rowData[5]+'</strong> by(Compliance) : '+rowData[14]+'<hr/>';
				   }
				   
				   
				   info += '</small>';
				   
				   $("#tooltip").html(info).animate({ left: e.pageX, top: e.pageY }, 1);
				   if (!$("#tooltip").is(':visible')) $("#tooltip").show();
				})
				
				$('#tblRejected').on('mouseleave', function(e) {
				  $("#tooltip").hide();
				}) ;
			
			/*$('#dropDownQuickLinkFNAHist').select2({
				width :'400',				
				 templateResult: formatDataSelect2
			})*/
			

			
		}

	}
	
	function fullViewFNA() {}
	
	function openTable(fldval){
		$.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
		if(fldval == "PENDING"){
			$($.fn.dataTable.tables(true)).DataTable()
			   .columns.adjust();
		}else if(fldval == "APPROVE"){
			$($.fn.dataTable.tables(true)).DataTable()
			   .columns.adjust();
		}else if(fldval == "REJECT"){
			$($.fn.dataTable.tables(true)).DataTable()
			   .columns.adjust();
		}else{
			$($.fn.dataTable.tables(true)).DataTable()
			   .columns.adjust();
		}
		
	}
	
	//poovathi add on 30-06-2021
	
	function sendMailtoAdviser(){
		
	
		if(!validateReqStatus()){return;}
		
		$('#cover-spin').show(0);
		var person = $("#nav-tab").children(".active").text().trim();
		
		var fnaid = $('#hdntxtFldFnaId').val();
		var custName =  $('#hdntxtFldClntName').val();
		var stsMsg = $('#txtFldRejStatus').val();
		var person = $("#nav-tab").children(".active").text().trim();
		var AdvName = $('#hdntxtFldAdvName').val();
		
		var parameter = "dbcall=STATUSMAIL&strFNAId=" + fnaid + 
		"&strCustName="+ custName + "&StrStsMsg="+stsMsg +
		"&StrPerson="+ person + "&StrAdvname=" + AdvName ;
		
		
		var retval = ajaxCall(parameter);
		
		for ( var val in retval) {

			var tabdets = retval[val];

			if (tabdets["SESSION_EXPIRY"]) {
				//window.location = baseUrl + SESSION_EXP_JSP;
				$('#cover-spin').hide(0);
				return;
			}

			for ( var tab in tabdets) {
				if (tabdets.hasOwnProperty(tab)) {
					var key = tab;
					var value = tabdets[tab];
					
					if(key == "SEND_MAIL_FAILURE" || key == "SEND_MAIL_SUCCESS"){
                            if (key == "SEND_MAIL_SUCCESS"){
                            	$('#fnaRejectStsMdl').modal('hide');
                            	$('#cover-spin').hide(0);
                            	 alert(value)
                            	 parent.location.reload(true);
                            	}
	                        if (key == "SEND_MAIL_FAILURE") {
	                        	$('#cover-spin').hide(0);
	                        	$('#fnaRejectStsMdl').modal('hide');
	                        	 alert(value)
	                        	 parent.location.reload(true);
	                        	}
	                 }
					
					
				 }
			}
		}
	}
	
	function validateReqStatus(){
		
		var comment = $("#txtFldRejStatus").val()
		if(isEmptyFld(comment)){
 			 $("#txtFldRejStatusErrorMsg").html("Keyin your Comments !");
 			 $('#txtFldRejStatus').focus();
 			return ;
 		}else{
 			$("#txtFldRejStatusErrorMsg").html(" ");
 		}
 	 return true;
	}
	
	function textCounter( field,maxlimit ) {

		var len=field.value.length;
		  if ( len > maxlimit )
		  {
		    field.value = field.value.substring( 0, maxlimit );
		    donothing();
		    return false;
		  }else if(len <= maxlimit){
			 $(field).next().addClass("show").removeClass("hide")
			 $(field).next().html(maxlimit-len+" characters are left from "+'<span class="text-success">'+maxlimit+'</span>' +" Characters" );
		  }
	 }


	function donothing(){
	       document.returnValue=true;
	}//end donothing
	
	function getFnaPendReqHisDetls(){
		$('#cover-spin').show(0);
		 var clnt = $("#dropDownQuickLinkFNAHist").find("option:selected").text().trim();
		 var fnaId = $("#dropDownQuickLinkFNAHist").find("option:selected").val().trim();
		    
		$.ajax({
	        url:baseUrl+ "/fnadetails/getAllPendReqMsg/"+fnaId,
	        type: "GET",
	        dataType: "json",async:false,
	        contentType: "application/json",
	        success: function (response) {
	           $('#cover-spin').hide(0);
	        if(response.length > 0){
	        		
	        	$('#fnaPendPeqHistryMdl').modal('show');
	        	$("#pendReqListSec").empty();
	        	
	        	var fnaid = '',msg = '',person = '',crtBy = '';crtDate = '';
	        
		        	for(var i = 0; i < response.length; i++){
		        		
		        		    for(var j = 0; j < response[i].length; j++){
		        		    	
		        		    	if(j!=0){
		        		    		if(j == 1){ fnaid = response[i][j];}
		        		    	    if(j == 2){ msg = response[i][j];}
		                            if(j == 3){ person = response[i][j];}
		                            if(j == 4){ crtBy = response[i][j];}
		                     	    if(j == 5){ crtDate = response[i][j];}
		                         }
		        		   }
		        		  
		        		   var strPendPeqList = '<div class="jumbotron p-2 mb-1  font-sz-level5" style="background-color: #f5f5f5; border: 1px solid #f2edf3;">'
		        		       +'<div class="row">'
		        		           +'<div class="col-3"> <small><span class="text-primaryy  bold">Client : </span>'+clnt+'</small></div>'
		        		           +'<div class="col-3"> <small><span class="text-primaryy  bold">FnaId : </span>'+fnaid+'</small></div>'
		        		           +'<div class="col-2"> <small><span class="text-primaryy  bold">Req. Person :</span> '+person+'</small></div>'
		        		           +'<div class="col-2"> <small><span class="text-primaryy  bold">Req.Created By : </span>'+crtBy+'</small></div>'
		        		           +'<div class="col-2"> <small class="hide"><span class="text-primaryy  bold ">Req.Created On : </span>'+crtDate+'</small></div>'
		        		           +'<div class="col-12"> <small style="line-height: 28px;"><span class="text-primaryy  bold">Comments : </span> '+msg+'</small></div>'
		        		       +'</div>'
		        		   +'</div>';
		            		
		        		  $("#pendReqListSec").append(strPendPeqList);
		        		 
		 		     }
	        	
	        }else{
	        		$('#cover-spin').hide(0);
	        		toastr.clear($('.toast'));
	    			toastr.options = {  timeOut: "5000", extendedTimeOut: "1000"};
	    			toastr["error"]("No Pending Request history Details found for Client : " + clnt);
	    		}
	        	
	     },
	   error: function(xhr,textStatus, errorThrown){
				$('#cover-spin').hide(0);
			    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
		}
	});
	}
	
	
	function showhidePendReqBtn(){
		var person = $("#nav-tab").children(".active").text().trim();
	
		var usrStatus = "";
		if(person == "Manager"){
			usrStatus = $("#txtFldMgrApproveStatus").val();
		}else if(person == "Admin"){
			usrStatus = $("#txtFldAdminApproveStatus").val();
		}else{
			usrStatus = $("#txtFldCompApproveStatus").val();
		}
		
		
		if((usrStatus == "APPROVE")||(usrStatus == "REJECT")){
			$("#btnSendReq").removeClass("show").addClass("hide");
		}else{
			$("#btnSendReq").removeClass("hide").addClass("show");
		}
	}
	  