<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.net.URL" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<%
URL birtUrl= new URL(request.getScheme(),"internal-ekycqr.avallis.com","/birt-viewer");

com.avallis.ekyc.utils.KycConst.BIRT_PATH = birtUrl.toString();	
%>

<%
	response.setHeader("Cache-Control", "no-cache"); 
	response.setHeader("Pragma", "no-cache"); 
	response.setDateHeader("Expires", -1);
%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link href="vendor/avallis/img/logo/logo.png" rel="icon">


<link href='https://fonts.googleapis.com/css?family=Roboto:400,700'	rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Poppins:400,600&display=swap"	rel="stylesheet">

<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">

<!-- <link href="vendor/fontawesome-free/css/font-awesome463.min.css"	rel="stylesheet"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<link rel="stylesheet" href="vendor/bootstrap453/css/bootstrap.css">
<link href="vendor/avallis/css/ekyc-layout.css" rel="stylesheet">
<link href="${contextPath}/vendor/avallis/css/ekyc-error.css">


<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet" />


<link href="css/jquery.steps.css" rel="stylesheet">
<link href="css/style_checkboxs.css" rel="stylesheet">
<link href="css/style_accord.css" rel="stylesheet">
<link href="vendor/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css">
<link href="vendor/avallis/css/style_upload.css" rel="stylesheet">
<link href="vendor/avallis/css/style_toggle.css" rel="stylesheet">
<link href="vendor/avallis/css/stepper.css" rel="stylesheet">

<link rel="stylesheet" href="vendor/jquery/jquery-ui.min.css">
<link rel="stylesheet" href="vendor/GitUpToast/css/jquery.toast.css">
<link href="vendor/SweetAlert2/css/sweetalert2.css">
<link href="vendor/SweetAlert2/css/sweetalert2.min.css">
<link href="vendor/avallis/css/sign_Status_Style.css" rel="stylesheet" >

<script src="vendor/jquery/jquery.js"></script>
<script src="vendor/jquery/popper.min.js"></script>
<script src="vendor/bootstrap453/js/bootstrap.js"></script>
<script src="js/modernizr.custom.js"></script>
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="vendor/jquery/jquery.scrollTo.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>

<script src="vendor/datatables/jquery.dataTables.min.js"></script>
<script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

<script src="vendor/avallis/js/ekyc-layout.js"></script>

<script src="vendor/avallis/js/Common Script.js"></script>
<script src="vendor/avallis/js/Constants.js"></script>

<script src="vendor/select2/dist/js/select2.js"></script>

<script src="vendor/jquery/jquery-ui.min.js" ></script>
<script src="vendor/GitUpToast/js/jquery.toast.js"></script>
		  <script src="vendor/SweetAlert2/js/sweetalert2.min.js"></script>
		  <script src="vendor/SweetAlert2/js/sweetalert2.all.js"></script>
		  <script src="vendor/SweetAlert2/js/sweetalert2.all.min.js"></script>
		  <script src="vendor/SweetAlert2/js/sweetalert2.js"></script>

<script src="vendor/avallis/js/appconfig.js"></script>

<script src="vendor/bootstrap453/js/bs.typeahead.js"></script>
<script src="https://cdn.jsdelivr.net/npm/js-cookie@rc/dist/js.cookie.min.js"></script>


<script>
var baseUrl="",strPageValidMsgFld="";
baseUrl="${contextPath}";
LOG_USER_MGRACS='${MANAGER_ACCESSFLG}';
LOG_USER_ADVID='${LOGGED_ADVSTFID}';
var seesselfName = '${SESS_DF_SELF_NAME}';
var sessspsname = '${SESS_DF_SPS_NAME}';
var BIRT_URL = "<%=com.avallis.ekyc.utils.KycConst.BIRT_PATH%>";
strPageValidMsgFld ="${SIGN_VALIDATION_MSG}"
var approvalscreen="false";
	
</script>

<title><tiles:getAsString name="title" /></title>
</head>

<body>
<div id="cover-spin"></div>
<input type="hidden" name="hTxtFldCurrFNAId" id="hTxtFldCurrFNAId" value="${CURRENT_FNAID}"/>
<input type="hidden" name="hTxtFldCurrCustId" id="hTxtFldCurrCustId" value="${CURRENT_CUSTID}"/>
<input type="hidden" name="hTxtFldCurrCustName" id="hTxtFldCurrCustName" value="${CURRENT_CUSTNAME}"/>	
<input type="hidden" name="hTxtFldLoggedAdvId" id="hTxtFldLoggedAdvId" value="${LOGGED_USER_INFO.LOGGED_USER_ADVSTFID}"/>
<input type="hidden" name="hTxtloggedUsrStftype" id="hTxtloggedUsrStftype" value="${LOGGED_USER_INFO.LOGGED_USER_STFTYPE}"/>
<input type="hidden" name="hTxtFldLoggedAdvName" id="hTxtFldLoggedAdvName" value="${LOGGED_USER_INFO.LOGGED_USER_ADVSTFNAME}"/>
<input type="hidden" name="hTxtFldLogDistId" id="hTxtFldLogDistId" value="${LOGGED_USER_INFO.LOGGED_DIST_ID}"/>
 <input type="hidden" name="hTxtFldLogUserId" id="hTxtFldLogUserId" value="${LOGGED_USER_INFO.LOGGED_USER_ID}"/>
<input type="hidden" id="hTxtValidationMsg" name="hTxtValidationMsg">
<input type="hidden" size="2" id="hTxtFldSignedOrNot" name="hTxtFldSignedOrNot"/>
<!--Poovathi add on 11-05-2021  -->
<input type="hidden" name="createNewFnaFlgs" id="createNewFnaFlgs" value="${CREATE_NEW_FNA_FLG}"/> 
<input type="hidden" name="prevFnaId" id="prevFnaId" value="${PREV_FNAID}"/> 
<!--Vignesh add on 07-06-2021  -->
<input type="hidden" value="${LOGGED_USER_INFO.LOGGED_USER_MGRFLAG}" id="hTxtFldMgrAcsFlg" name="hTxtFldMgrAcsFlg"/>
	  
	<div id="wrapper">
		<tiles:insertAttribute name="menu" />
		<div id="content-wrapper" class="d-flex flex-column">
			<div id="content">
				<tiles:insertAttribute name="header" />
				<tiles:insertAttribute name="body" />
			</div>
		</div>
	</div>
	<tiles:insertAttribute name="footer" />
	
	<!-- Poovathi Add on 04-NOV-2020 -->
	 <jsp:include page="/views/pages/logoutModal.jsp"></jsp:include>
     <jsp:include page="/views/pages/profileDetlsModal.jsp"></jsp:include>
     <jsp:include page="/views/pages/scanToSignModal.jsp"></jsp:include>
     <jsp:include page="/views/pages/documentUploadModal.jsp"></jsp:include>
     <jsp:include page="/views/pages/archiveListModal.jsp"></jsp:include>
	<jsp:include page="/views/pages/SignModal.jsp"></jsp:include>
	<%-- <jsp:include page="/views/pages/ekycValidations.jsp"></jsp:include> --%><!--this file not needed this sec will move to footer.jsp file  -->
 
 <!-- Scroll to top -->
  <a class="scroll-to-top" href="#page-top" title="Go to Top" style="display: none;">
   <i class="fa fa-chevron-up" aria-hidden="true"></i>
  </a>
  
 
  
  <div id="ekycinvmodal" class="modal fade" data-backdrop="static" data-keyboard="false" >
	<div class="modal-dialog modal-xl  modal-dialog-scrollable">
	    <div class="modal-content" style="min-height:85vh">
	            <div class="modal-header"  id="cardHeaderStyle-22">
	            	<h6 class="modal-title">Investment eKYC</h6>
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>	                
	            </div>
	            <div class="modal-body">
	            	<!-- <iframe id="ifrInvEkyc" style="width:100%;height:70vh"></iframe>  -->
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	            </div>
	    </div>
	</div>
</div>
	
	 <form id="hiddenForm" method="post" action="" target="TheWindowBIRT" ></form>
	 <form id="validationForm" method="post"></form>
	 
	 
	 <div class="modal fade" id="loadermodal"  role="dialog" data-backdrop="static" data-keyboard="false"  >
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header" id="cardHeaderStyle-22" >
                  <h5 class="modal-title" id="finishedModalLabelClnt"> Loading</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:#fff;">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body text-center">
				
				 
                  <div class="circle-loader">
					  <div class="checkmark draw"></div>
					   
					</div>
					
					 


                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-sm  btn-bs3-prime" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
</body>


<script>




$(function(){
	
	$('.navbar').css('width', '86.5%');
//	$('#wrapper #content-wrapper').css('max-height','100vh');
	
	    //set tooltip for all page  text ,textarea and select combo 
	    $('input[type=text],input[type=email],textarea,select').keyup(function(){
		   $(this).attr('title',$(this).val())
	    })
	    
	    $('input[type=text],input[type=email],textarea,select').mouseover(function(){
		   $(this).attr('title',$(this).val())
	    })
	     $('input[type=text],input[type=email],textarea,select').mouseout(function(){
		   $(this).attr('title',"")
	    })
	     $('input[type=text],input[type=email],textarea,select').keyup(function(){
	       $(this).attr('autocomplete',"nope")
        })
        
        $('input[type=text],input[type=email],textarea,select').keydown(function(){
	    $(this).attr('autocomplete',"nope")
        })

    
        $('input[type=text],input[type=email],textarea,select').keypress(function(){
	    $(this).attr('autocomplete',"nope")
        })
    
        $('input[type=text],input[type=email],textarea,select').mouseover(function(){
    	  $(this).attr('autocomplete',"nope")
        })
        
         $('input[type=text],input[type=email],textarea,select').click(function(){
	     $(this).attr('autocomplete',"nope")
        })
	   //end of tooltip function
	   
});



var elementFloatingChat = $('.floating-chat');
 

setTimeout(function() {
	elementFloatingChat.addClass('enter');
}, 1000);

elementFloatingChat.click(openElement);

function openElement() {
	 var messages = elementFloatingChat.find('.messages');
  //  var textInput = elementFloatingChat.find('.text-box');
    elementFloatingChat.find('>i').hide();
    elementFloatingChat.addClass('expand');
    elementFloatingChat.find('.chat').addClass('enter');
    //var strLength = textInput.val().length * 2;
    //textInput.keydown(onMetaAndEnter).prop("disabled", false).focus();
    elementFloatingChat.off('click', openElement);
    elementFloatingChat.find('.header button').click(closeElement);
    //elementFloatingChat.find('#sendMessage').click(sendNewMessage);
    messages.scrollTop(messages.prop("scrollHeight"));
    
    
	
}

function closeElement() {
	elementFloatingChat.find('.chat').removeClass('enter').hide();
	elementFloatingChat.find('>i').show();
	elementFloatingChat.removeClass('expand');
	elementFloatingChat.find('.header button').off('click', closeElement);
	//elementFloatingChat.find('#sendMessage').off('click', sendNewMessage);
	//elementFloatingChat.find('.text-box').off('keydown', onMetaAndEnter).prop("disabled", true).blur();
    setTimeout(function() {
    	elementFloatingChat.find('.chat').removeClass('enter').show()
        elementFloatingChat.click(openElement);
    }, 500);
}



chkNextChange();

//checkdb onchange function code placed here instead of common Script file

$(".checkdb").bind("change",function(){
	
	var curntObj = $(this)
	enableReSignIfRequired(curntObj)
	});


	
function enableReSignIfRequired(curntObj){
	
	var nodetype = $(curntObj).prop('type').toUpperCase();
	
    if(nodetype == "HIDDEN" || nodetype == "RADIO" || nodetype=="CHECKBOX"){

        }

	var isSigned = $("#hTxtFldSignedOrNot").val();
	
	if(isSigned == "Y") {
		
		var nodetype = $(curntObj).prop('nodeName').toLowerCase();
		
        var chkanychange = $("#chkanychange");
		var jsonvalues = JSON.parse(isEmpty(chkanychange.val()) ? "{}" : chkanychange.val());

		var inputtype = $(curntObj).prop("type").toLowerCase();
		
		if(inputtype ==  "text" || inputtype == "hidden" || inputtype == "select" || inputtype == "textarea" || nodetype == "select"){
            var newobj=jsonvalues;
			newobj[$(curntObj).prop("name")]=$(curntObj).val();
			chkanychange.val(JSON.stringify(newobj));

		}else if(inputtype == "radio" || inputtype == "checkbox"){
			var chkFlg = $(curntObj).is(":checked");
			   if(chkFlg == true || chkFlg == false){
				   var newobj=jsonvalues;
						newobj[$(curntObj).prop("name")]=$(curntObj).val();
						chkanychange.val(JSON.stringify(newobj));
				   }
		}
		
		if(!isEmpty(chkanychange.val())) {
			showConfrmAfterSignChng($("#btnNextFormUniq"),$("#applNextScreen").val(),false,false);

		}
	 }
 }
//checkdb function end	
function enableReSignIfRequiredforEdtDeltAction(){
var isSigned = $("#hTxtFldSignedOrNot").val();
    if(isSigned == "Y") {
		showConfrmAfterSignChng($("#btnNextFormUniq"),$("#applNextScreen").val(),false,false);
	}
}

// poovathi add on 10-05-21
function createNewFna(){
	
	var custId = $("#hTxtFldCurrCustId").val();
	
	 $.ajax({
	        url:baseUrl+"/customerdetails/getAllFnaList/"+custId,
	        type: "GET",
	        //dataType: "json",
	        async:false,
	        contentType: "application/json",
	        success: function (data) {
	        	var fnaList = JSON.parse(data)
	        	var fnaListLen = fnaList.length;
	        	
	        	if(fnaListLen > 0){
	        		Swal.fire({
	        			  //title: 'Are you sure?',
	        			  //poovathi change text on 14-06-2021
	        			  text: " Do you want to create one more FNA detail(s) for client : "+ seesselfName,
	        			  icon: 'info',
	        			  showCancelButton: true,
	        			  confirmButtonColor: '#3085d6',
	        			  cancelButtonColor: '#d33',
	        			  confirmButtonText: 'Yes, Create New FNA !'
	        			}).then((result) => {
	        			  if (result.isConfirmed) {
	        			    window.location.href= "CreateNewFNA"
	        			  }
	        			})
	        		
	        	}else{
	        		toastr.clear($('.toast'));
            		toastr.options = {  timeOut: "5000", extendedTimeOut: "1000"};
            		toastr["error"]("No Previous Archive details found for Client : "+ seesselfName);	
	        	}
	        },
	        error: function(xhr,textStatus, errorThrown){
	 			//$('#cover-spin').hide(0);
	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			}
	        });
	
}

//dashboard detls table column adjust -  poovathi add on 10-05-21
$('#srchClntModalnewtest').on('shown.bs.modal', function (e) {
    $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
});

//poovathi add on 03-06-2021
var loggedUsrStffType = $("#hTxtloggedUsrStftype").val();
if ( loggedUsrStffType == "ADVISER" ) {
	$("#btnDashboardIcon").addClass("show").removeClass("hide")
	$(".topbar-divider").addClass("show").removeClass("hide")
	
} 

</script>


</html>
