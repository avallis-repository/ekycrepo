<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}" />


<%
	response.setHeader("Cache-Control", "no-cache"); 
	response.setHeader("Pragma", "no-cache"); 
	response.setDateHeader("Expires", -1);
%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link href="vendor/avallis/img/logo/logo.png" rel="icon">
  <link href='https://fonts.googleapis.com/css?family=Roboto:400,700'  rel='stylesheet'>
  <link href="https://fonts.googleapis.com/css?family=Poppins:400,600&display=swap" rel="stylesheet">
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <!-- <link href="vendor/fontawesome-free/css/font-awesome463.min.css" rel="stylesheet"> -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
  <link rel="stylesheet" href="vendor/bootstrap453/css/bootstrap.css">
  <link href="vendor/avallis/css/ekyc-layout.css" rel="stylesheet">
  
  
  <link href="vendor/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet" />
  <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  
  <link href="vendor/avallis/css/style_radio_type1.css" rel="stylesheet">
  <link href="vendor/avallis/css/style_upload.css" rel="stylesheet">  
  <link href="vendor/avallis/css/style_toggle.css" rel="stylesheet">
  <!-- <link rel="stylesheet" href="/vendor/avallis/css/ekyc-error.css"> 
  
  <!-- sweet alert 2 Link --css tags  -->
  <link href="vendor/SweetAlert2/css/sweetalert2.css">
   <link href="vendor/SweetAlert2/css/sweetalert2.min.css">
   <!-- end -->

	<script src="vendor/jquery/jquery.js"></script>
	<script src="vendor/jquery/popper.min.js"></script>
	<script src="vendor/bootstrap453/js/bootstrap.js"></script>
	<script src="js/modernizr.custom.js"></script>
	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
	
	<script src="vendor/datatables/jquery.dataTables.min.js"></script>	
  	<script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>
  	
  	
  	
	<script src="vendor/avallis/js/ekyc-layout.js"></script>
	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
	<script src="vendor/select2/dist/js/select2.min.js"></script>
	<script src="vendor/avallis/js/Common Script.js"></script>
	<script src="vendor/avallis/js/Constants.js"></script>
	
	  <script src="vendor/jquery/jquery-ui.min.js" ></script>
	      <script src="vendor/GitUpToast/js/jquery.toast.js"></script>
	      <!-- sweet alert 2 script Tags  -->
		  <script src="vendor/SweetAlert2/js/sweetalert2.min.js"></script>
		  <script src="vendor/SweetAlert2/js/sweetalert2.all.js"></script>
		  <script src="vendor/SweetAlert2/js/sweetalert2.all.min.js"></script>
		  <script src="vendor/SweetAlert2/js/sweetalert2.js"></script>
		  <!--  end-->
<script src="vendor/avallis/js/appconfig.js"></script>
<script src="vendor/bootstrap453/js/bs.typeahead.js"></script>
<script src="https://cdn.jsdelivr.net/npm/js-cookie@rc/dist/js.cookie.min.js"></script>

<script>
var baseUrl="${contextPath}";
var approvalscreen="false";
var paraplanadviser = JSON.parse('${PARA_ADV_EMAILID}')
</script>

<title><tiles:getAsString name="title" /></title>
</head>


<body>
<div id="cover-spin"></div>
	<div id="wrapper" style="background-color: #ecf0f1;">

		<div id="content-wrapper" class="d-flex flex-column">
			<div id="content">
				<tiles:insertAttribute name="header" />
                                       <select class="form-control form-control-sm custom-select mr-sm-2" 
                                       id="selSrchAdvNameGlobal" name="selSrchAdvNameGlobal">
                                          <option  selected="true" value="">--Select--</option>
                                       </select>
				<tiles:insertAttribute name="body" />
			</div>
		</div>
		
		
	</div>
	
	
     
	 <jsp:include page="/views/pages/logoutModal.jsp"></jsp:include>
     <jsp:include page="/views/pages/profileDetlsModal.jsp"></jsp:include>
     
	 <input type="hidden" name="hTxtFldLoggedAdvId" id="hTxtFldLoggedAdvId" value="${LOGGED_USER_INFO.LOGGED_USER_ADVSTFID}"/>
     <input type="hidden" name="hTxtFldCurrFNAId" id="hTxtFldCurrFNAId" value="${CURRENT_FNAID}"/>
     <input type="hidden" name="hTxtFldCurrCustId" id="hTxtFldCurrCustId" value="${CURRENT_CUSTID}"/>
     <input type="hidden" name="hTxtloggedUsrStftype" id="hTxtloggedUsrStftype" value="${LOGGED_USER_INFO.LOGGED_USER_STFTYPE}"/>
     <input type="hidden" name="hTxtFldLoggedAdvName" id="hTxtFldLoggedAdvName" value="${LOGGED_USER_INFO.LOGGED_USER_ADVSTFNAME}"/>
     <input type="hidden" name="hTxtFldLogDistId" id="hTxtFldLogDistId" value="${LOGGED_USER_INFO.LOGGED_DIST_ID}"/>
     <input type="hidden" name="hTxtFldLogUserId" id="hTxtFldLogUserId" value="${LOGGED_USER_INFO.LOGGED_USER_ID}"/>
     <!--Poovathi add on 11-05-2021  -->
     <input type="hidden" name="createNewFnaFlgs" id="createNewFnaFlgs" value="${CREATE_NEW_FNA_FLG}"/> 
     <input type="hidden" name="prevFnaId" id="prevFnaId" value="${PREV_FNAID}"/> 
     <!--Vignesh add on 07-06-2021  -->
     
     <input type="hidden" value="${LOGGED_USER_INFO.LOGGED_USER_MGRFLAG}" id="hTxtFldMgrAcsFlg" name="hTxtFldMgrAcsFlg"/>
     
     <input type="hidden" id="hdnLatestArchFnaId" value="${LAT_ARCHV_FNAID}">

     
</body>
<script>
//poovathi add on 15-11-2020
$(function(){
	$('.navbar').css('width', '99.8%');
	/*
	$('#wrapper #content-wrapper').css('overflow-y','scroll');
	$('#wrapper #content-wrapper').css('overflow-x','hidden');
	$('#wrapper #content-wrapper').css('max-height','100vh'); */
	
	//$('#wrapper #content-wrapper').css('width','100%');
	//$('#wrapper #content-wrapper').css('overflow-y','auto');
	/*  background-color: #e8eced;
	overflow-y: scroll;
    max-height: 100vh;
    width: 100%; */
    
    //$('#wrapper #content-wrapper').css('min-height','100vh'); 
	   //set tooltip for all page  text ,textarea and select combo 
    $('input[type=text],input[type=email],textarea,select').keyup(function(){
	   $(this).attr('title',$(this).val())
    })
    
    $('input[type=text],input[type=email],textarea,select').mouseover(function(){
	   $(this).attr('title',$(this).val())
    })
     $('input[type=text],input[type=email],textarea,select').mouseout(function(){
	   $(this).attr('title',"")
    })
    
    $('input[type=text],input[type=email],textarea,select').keyup(function(){
	   $(this).attr('autocomplete',"nope")
    })
    
        $('input[type=text],input[type=email],textarea,select').keydown(function(){
	   $(this).attr('autocomplete',"nope")
    })

    
        $('input[type=text],input[type=email],textarea,select').keypress(function(){
	   $(this).attr('autocomplete',"nope")
    })

    
    $('input[type=text],input[type=email],textarea,select').mouseover(function(){
    	  $(this).attr('autocomplete',"nope")
    })
    
   //end of tooltip function
   
  //dashboard detls table column adjust -  poovathi add on 10-05-21
    $('#srchClntModalnewtest').on('shown.bs.modal', function (e) {
        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
    });
	   
  //poovathi add on 03-06-2021
   	var loggedUsrStffType = $("#hTxtloggedUsrStftype").val();
    if (loggedUsrStffType == "ADVISER" ) {
    	$("#btnDashboardIcon").addClass("show").removeClass("hide")
    }
    

})
</script>
</html>