<nav
	class="navbar navbar-expand navbar-light bg-navbar topbar mb-4 static-top"
	id="navbar-bg">
	<h1 class="h3 mb-0 text-gray-800" id="goPreviousArrow">
		<a href="index" class="btn btn-sm btn-default" style="color: white"><i class="fa fa-arrow-left"></i>&nbsp;Search/Add Client</a>
	</h1>
	<ul class="navbar-nav ml-auto navbar-fixed-top ">
            <li class="nav-item dropdown no-arrow d-none">
                        <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-search fa-fw"></i>
                        </a>
                       <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown"><!--   -->
                           <form class="navbar-search">
                              <div class="input-group">
                                 <input type="text" class="form-control bg-light border-1 small" placeholder="Search for client details?" aria-label="Search" aria-describedby="basic-addon2" style="border-color: #3f51b5;">
                                 <div class="input-group-append">
                                    <button class="btn btn-sm btn-bs3-prime" type="button">
                                    <i class="fas fa-search fa-sm"></i>
                                    </button>
                                 </div>
                              </div>
                           </form>
                        </div>
                     </li>
                     
                     
                      <li class="nav-item mt-3 d-none">
                           <a href="CreateNewFNA" class="btn btn-bs3-success " role="button">
                           <i class="fa fa-list-alt" aria-hidden="true"></i><sub> <i class="fa fa-plus"
                            aria-hidden="true"></i></sub>&nbsp;Create New FNA</a>

                           
                         </li>
                        <!-- <div class="topbar-divider d-sm-block "></div> -->
                      <li class="nav-item mt-3 ">
                           <!-- vignesh 05-05-2021 -->
                           <!--poovathi change dashboard image to button type on 15-06-2021  -->
                           <button class =" btn btn-bs3-success hide" role="button"  id="btnDashboardIcon"
                           title="Click to View Dashboard Details" onclick="openURLPopup(this)"> 
                           <i class="fa fa-area-chart" aria-hidden="true"></i>&nbsp;View Dashboard Detls. </button> 
                       <!-- End -->     
                       
                        </li>
                        
                        
                     <div class="topbar-divider d-none d-sm-block"></div>
                     <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img class="img-profile rounded-circle" src="vendor/avallis/img/users/${LOGGED_USER_INFO.LOGGED_USER_ID}.png" 
                        style="max-width: 60px" onerror="this.onerror=null;this.src='vendor/avallis/img/users/client.png';">
                        
                        
                        <span class="ml-2 d-none d-lg-inline text-white small">${LOGGED_USER_INFO.LOGGED_USER_ID}</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                           <a class="dropdown-item" href="#" data-toggle="modal" data-target="#profileModal">
                           <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                           Profile
                           </a>
                          <!--  <a class="dropdown-item" href="#">
                           <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                           Settings
                           </a>
                           <a class="dropdown-item" href="#">
                           <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                           Activity Log
                           </a> -->
                           <div class="dropdown-divider"></div>
                           <a class="dropdown-item" href="javascript:void(0);" data-toggle="modal" data-target="#logoutModal">
                           <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                           Logout
                           </a>
                        </div>
                     </li>
                  </ul>
</nav>

 
	
	 <!-- Vignesh SB Add on 05-05-2021 -->
 
 <div class="modal fade" id="srchClntModalnewtest" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"
            aria-labelledby="srchClntModalTitle" aria-hidden="true"    style="display: none;">
            <div class="modal-dialog  modal-xl" role="document" style="min-height: 90%;min-width:95%" >  <!--  modal-dialog-scrollable -->
              <div class="modal-content" style="min-height: 100%;min-width:100%">
                <div class="modal-header" id="cardHeaderStyle-22">
                   <h6 class="modal-title" id="srchClntModalTitle"><img src="vendor/avallis/img/userDashboard.png" style="width: 10%;"
                   class="img-thumbnail">&nbsp;&nbsp; Adviser / Manager Dashboard Details :- </h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:#fff;">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body" id="cloneURLId" style="overflow-y: none;height: 83vh;">
                  
                </div>
                <div class="modal-footer float-left p-1">
				
                  <button type="button" class="btn btn-sm pl-3 pr-3 btn-bs3-prime d-none" data-dismiss="modal">OK</button>
                  
                </div>
              </div>
            </div>
          </div>
 
 <!-- End -->
