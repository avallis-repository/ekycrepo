<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<div id="wrapper">
<ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar" style="background-color:#1d655cf7 !important">
		<li class="nav-item">
			<a class="sidebar-brand d-flex align-items-center justify-content-center">
               <span class="sidebar-brand-icon bg-white" style="border-radius:10px">
                  <img src="vendor/avallis/img/logo/logo.png">
               </span>
               <span class="sidebar-brand-text mx-3">eKYC</span>
             </a>
             
           <div class="sidebar-heading d-flex justify-content-center mt-n2">
               <span class=" font-sz-level6">FNA - V4.2 </span>
            </div>
			
			<hr class="sidebar-divider">
            <div class="sidebar-heading">
               <small class="white" title="${CURRENT_FNAID}">FNA Id : ${CURRENT_FNAID}</small><br>
           </div>
            
			<hr class="sidebar-divider">
            <div class="sidebar-heading">
            	<div class="row">
            		<div class="col-12">
            		    <span><small>Client(1):</small> <br><span class="o_ellipsis" id="MenuselfName" title="${SESS_DF_SELF_NAME}">${SESS_DF_SELF_NAME}</span></span>
            		</div>
            		
            		<div class="col-12">
            		<c:if test = "${SESS_DF_SPS_NAME ne null}">
            		<span><small  id="clnt2Text">Client(2):</small> <br><span class="o_ellipsis" id="MenuSpsName" title="${SESS_DF_SPS_NAME}">${SESS_DF_SPS_NAME}</span></span>
            		</c:if>
            		</div>
            		
            		
            	</div>
              
            </div>
            
            
            
			
            <hr class="sidebar-divider">
            <div class="sidebar-heading">
            </div>
</li>

            
          
             <li class="nav-item">
               <a class="nav-link" onclick="saveCurrentPageData(this,'kycHome',true,true)" >
               <i class="fas fa-fw fa-user"></i>
               <span>Personal Details</span>
               </a>
            </li>
            
            
              <li class="nav-item">
               <a class="nav-link" onclick="saveCurrentPageData(this,'taxResidency',true,true)">
               <i class="fas fa-fw fa-chart-area"></i>
               <span>Tax Residency</span>
               </a>
            </li>
			
			 	 <!--<hr class="sidebar-divider">-->
			 
			 
            <li class="nav-item">
               <a class="nav-link" onclick="saveCurrentPageData(this,'finanWellReview',true,true)">
               <i class="fas fa-fw fa-plus"></i>
               <span>Finan. Wellness Review</span>
               </a>
            </li>
			
			 	 <!--<hr class="sidebar-divider">-->
            <li class="nav-item d-none">
               <a class="nav-link" href="#">
               <i class="fas fa-fw fa-chart-area"></i>
               <span>Investment Objectives</span>
               </a>
            </li>
            
            <hr class="sidebar-divider">
            <div class="sidebar-heading">
               Recommendations
            </div>
            <li class="nav-item">
               <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePageLife" aria-expanded="true" aria-controls="collapsePageLife">
               <i class="fas fa-fw fa-life-ring"></i>
               <span>Life&nbsp;&amp;&nbsp;ILP</span>
               </a>
               <div id="collapsePageLife" class="collapse" aria-labelledby="headingPage" data-parent="#accordionSidebar">
                  <div class="bg-white py-2 collapse-inner rounded">
                     <a class="collapse-item"  onclick="saveCurrentPageData(this,'productRecommend',true,true)">New Purchase</a>
                     <a class="collapse-item" onclick="saveCurrentPageData(this,'productSwitchRecomm',true,true)">Switching</a>
                      <a class="collapse-item" onclick="saveCurrentPageData(this,'productSwitchReplace',true,true)">Switching &amp; Replace.</a>
                  </div>
               </div>
            </li>
            
            
             <li class="nav-item hide d-none">
               <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePageUT" 
               aria-expanded="true" aria-controls="collapsePageUT">
               <i class="fas fa-fw fa-line-chart"></i>
               <span>IAS</span>
               </a>
               <div id="collapsePageUT" class="collapse" aria-labelledby="headingPage" data-parent="#accordionSidebar">
                  <div class="bg-white py-2 collapse-inner rounded">
                     <a class="collapse-item" href="investmentProduct" >Investment Product</a>
                     <a class="collapse-item" href="ProductSummary" >Product Summary</a>     
                  </div>
               </div>
            </li>
            
		    <hr class="sidebar-divider">
			     <div class="sidebar-heading">
	                  Advice
	            </div>
	            <li class="nav-item">
	               <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#AdvBasis"
	                 aria-expanded="true" aria-controls="AdvBasis">
	               <i class="fas fa-fw fa-comments"></i>
	               <span>Adv.&amp;Basis of Recom.</span>
	               </a>
	               
	               <div id="AdvBasis" class="collapse" aria-labelledby="headingPage" data-parent="#accordionSidebar">
	                  <div class="bg-white py-2 collapse-inner rounded">
	                     <a class="collapse-item"  onclick="saveCurrentPageData(this,'adviceBasisRecomm',true,true)" >Life</a> 
	                     <a class="collapse-item hide d-none" href="advBasisUT">UT</a>    
	                  </div>
                  </div>
	            </li>
		 
           
            <hr class="sidebar-divider">
            <div class="sidebar-heading">
                Declaration
            </div>
           
            <li class="nav-item">
               <a class="nav-link"  onclick="saveCurrentPageData(this,'clientSignature',true,true)">
               <i class="fas fa-fw fa-user"></i>
               <span>Client Signature</span>
               </a>
            </li>
			
			<li class="nav-item">
               <a class="nav-link"  onclick="saveCurrentPageData(this,'clientDeclaration',true,true)">
               <i class="fas fa-fw fa-users"></i>
               <span>Client Decl.</span>
               </a>
            </li>
			
			  <!--<hr class="sidebar-divider">-->
            <li class="nav-item">
               <a class="nav-link" onclick="saveCurrentPageData(this,'managerDeclaration',true,true)">
              <i class="fas fa-fw fa-user-secret"></i>
               <span>Adv & Mgr Decl.</span>
               </a>
            </li>
            <hr class="sidebar-divider">
            <div class="version" id="version-div"></div>
         </ul>
         </div>
	