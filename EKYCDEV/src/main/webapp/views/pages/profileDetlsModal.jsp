<!-- Pofile Modal -->
	<div class="modal fade" id="profileModal">
		<div class="modal-dialog ">
			<div class="modal-content">

				<!-- Modal Header -->
				<div class="modal-header" id="cardHeaderStyle-22">
					<h4 class="modal-title">Profile Details</h4>
					<button type="button" class="close white" data-dismiss="modal">&times;</button>
				</div>

				<!-- Modal body -->
				<div class="modal-body">
					<ul class="list-group">
					
					<li class="list-group-item p-2"><span
							class="text-secondary bold italic"><img
								src="vendor/avallis/img/profile.png"> Manager Details</span>

							<div class="row">
								<div class="col-6">
									<small >
											Manager Name :<br><span class="text-primaryy "> ${LOGGED_USER_INFO.LOGGED_USER_MGRADVSTFNAME}</span></small>
								</div>
								<div class="col-6">
									<small >
											Manager Rep RNF :<br><span class="text-primaryy "> -- NIL --</span></small>
								</div>
							</div>
							<div class="row hide">
								<div class="col-6">
									<small><span class="text-primaryy  italic">Manager POS:</span><span> </span></small>
								</div>
								<div class="col-">
									<small><span class="text-primaryy  italic">RNF of rep's Supervisor : </span><span></span></small>
								</div>
							</div></li>
					
						<li class="list-group-item p-2"><span
							class="text-secondary bold italic"><img
								src="vendor/avallis/img/profile.png"> Adviser Details</span>

							<div class="row">
								<div class="col-6">
									<small>Adv.
											Name&nbsp;:&nbsp;<br><span class="text-primaryy ">${LOGGED_USER_INFO.LOGGED_USER_ADVSTFNAME}</span></small>
								</div>
								<div class="col-6">
									<small >Adv.
											RNF No&nbsp;:&nbsp;<br><span class="text-primaryy  ">-- NIL --</span></small>
								</div>
							</div></li>
						

					</ul>
				</div>

				
				

			</div>
		</div>
	</div>

	<!-- Profile Modal End-->