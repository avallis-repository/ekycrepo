<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

<head>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"> 
<style>

.middle {
  width: 100%;
  text-align: center;
}

.text-custom-color{
color:#243665;
}

.card {
    display: block; 
    line-height: 1.42857143;
    background-color: #fff;
    border-radius: 2px;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12); 
    transition: box-shadow 1.25s; 
}

.card:hover {
  box-shadow: 0 12px 17px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
}

.img-card {
  width: 100%;
  height:200px;
  border-top-left-radius:2px;
  border-top-right-radius:2px;
  display:block;
    overflow: hidden;
}

.img-card img{
  width: 100%;
  height: 200px;
  object-fit:cover; 
  transition: all .25s ease;
} 

.card-content {
  padding:15px;
  text-align:left;
}

.card-title {
  margin-top:0px;
  font-weight: 700;
}

.card-title a {
  color: #000;
  text-decoration: none !important;
}

.card-read-more {
  border-top: 1px solid #D4D4D4;
}

.card-read-more input{
  text-decoration: none !important;
  margin:10px;
  font-weight:600; 
}

 #srchClntLoader{
    z-index: 2;
	/* background-color:  transparent; */
	background: rgba(255,255,255,0.8)
	filter:alpha(opacity=70);
	opacity: 0.5;	
	position: absolute;
	top: 48%; 
    left: 0;
    right: 0;
    bottom: 0;
   } 
   
 /* Data Table select Row Styles Applied Here */  
 
	#srchClntTblSec tr{
	      transition: transform .2s;
	}
	
	#srchClntTblSec tr:hover {
	   cursor:pointer; 
	   transform: scale(1.0);
	   color: #fff;
	   background-color: #337ab7;
	   border-color: #2e6da4;
	   overflow-x: hidden;
	}
 /* Data Table select Row Styles Applied End */  
 

   
</style>
</head>
<body id="page-top">
 <!-- <div id="wrapper" >
    
    <div id="content-wrapper" class="d-flex flex-column"> -->
     <!-- Container Fluid-->
        <div class="container" id="container-wrapper" >
         
		<div class="card" style="border:1px solid #044CB3;margin-top:5rem;">
                <div class="card-header  d-flex flex-row align-items-center justify-content-between" id="cardHeaderStyle-22">
                  <h5 class="m-0" style="font-weight:normal;">Financial Need Analysis V4.2</h5>
                </div>
                <div class="card-body" >
                  <div class="row">
				  
				  <div class="col-xs-12 col-sm-2">
					&nbsp;
				  </div>
				  
				  <div class="col-xs-12 col-sm-4">
                        <div class="card ">
						
							<div class="card-content text-center">
                                <img src="vendor/avallis/img/addUser.png" style="width:150px"/>
								<div></div>
								<h6 class="card-title p-3" style="font-size: 13px;">
                                    Add New Client details 
                                  </h6>
							</div>
							
							<div class="card-read-more text-center">
                                 <input type="button" class="btn btn-sm  btn-bs3-prime btnshake" value="  Proceed " id="btnAddClnts"  style="font-size: 75%;" onclick='window.location.href="clientInfo";'/>
                            </div>
                           
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="card ">
						
							<div class="card-content text-center">
                                <img src="vendor/avallis/img/srchUser.png" style="width:150px"/>
								<div></div>
								<h6 class="card-title p-3" style="font-size: 13px;">
                                    Search for existing Client details
                                  </h6>
							</div>
							
							<div class="card-read-more text-center">
                                 <input type="button" class="btn btn-sm  btn-bs3-prime" value="  Proceed " style="font-size: 75%;"  data-toggle="modal"    data-target="#srchClntModal"  />
                            </div>
                           
                        </div>
                    </div>
					<div class="col-xs-12 col-sm-2">
					&nbsp;
				  </div>
					</div>
				   
				 
				 
                </div>
              </div>
          
        </div>
        <!---Container Fluid-->
        
        
        
 		  <!-- Modal Scrollable -->
          <!-- Modal Scrollable -->
           <div class="modal fade" id="srchClntModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"
            aria-labelledby="srchClntModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document" style="min-height: 90%;" >
              <div class="modal-content" style="min-height: 90%;">
                <div class="modal-header" id="cardHeaderStyle-22">
                  <h6 class="modal-title" id="srchClntModalTitle"><img src="vendor/avallis/img/srch.png">&nbsp;&nbsp;Search Client</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:#fff;">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                
                <div class="col-lg-12">
              <div class="card mb-4" >
              
              <div class="card-body" id="secOverlay">
              
             <div class="row">
                   <div class="col-4">
                     <label class="frm-lable-fntsz" for="name">Adviser Staff Level</label>
                                       <select class="form-control form-control-sm custom-select mr-sm-2" 
                                                id="selGlblAcessLvl" name="selGlblAcessLvl" maxlength="20" onchange="getGlblAcessLvlAgnts()">
                                            <c:forEach items="${GLBL_ACCESS_LEVEL}" var="k">
	                                       	<option value="${k.value}">${k.label }</option>
	                                       </c:forEach>
                                       </select>
                      </div>
                      
                     <div class="col-4">
                       <label class="frm-lable-fntsz" for="name">Adviser</label>
                                       <select class="form-control form-control-sm custom-select mr-sm-2" 
                                           id="selSrchAdvName" name="selSrchAdvName" maxlength="20"  >
                                          <option  selected="true" value="">--Select--</option>
                                       </select>
                                       <div class="invalid-feedbacks hide" id="selSrchAdvNameError">Select the  Adviser</div>
                        </div>
                        
                         <div class="col-4">
                   <div class="mt-4 noteInfoMsg" title="Note Information"><small><img src="vendor/avallis/img/infor.png" alt="Note" class="img-rounded mr-2">&nbsp;<span id="noteInfo">Keyin client Name and Click Search Button !</span></small></div>
                   </div>
                        
              </div>
              
              
              
              <div class="row mt-2">
                   <div class="col-8">
                       <label class="frm-lable-fntsz pl-3" for="">Client Name :</label>
									<div class="input-group input-group-sm mb-3 pl-3">
									  <div class="input-group-prepend">
										<span class="input-group-text p-1"><img src="vendor/avallis/img/srchClnt.png" class="pl-1" style="width: 80%;"></span>
									  </div>
									  <input id="txtFldCustName" class="form-control typeahead border-primary" type="text" name="txtFldCustName"  
									  placeholder="Search Client Names here..." autocomplete="nope" onchange="clrTableData()">
                                   </div> 
									
									<div class="invalid-feedbacks pl-3 hide" id="txtFldCustNameError">
                                             Keyin CustName
                                     </div>
									
                   </div>
                   
                   <div class="col-4">
                       <button class="btn  btn-sm btn-bs3-prime mt-4 font-sz-level6" id="btnSrchClint" onclick="getAllFnaSpouseDetails()"><img src="vendor/avallis/img/clntSrch.png" style="width: 26%;">&nbsp; Search</button>
                   </div>
              </div>
              
              <div class="row hide" id="srchClntTblSec">
                   <div class="col-12">
	                   <div id="srchClntLoader"> 
	                       <img src="vendor/avallis/img/srchLoader.gif" style="width:15%;" class="rounded mx-auto d-block">
	                    </div>
	                    
	                     <div class="table-responsive p-2" >
			                  <table class="table align-items-center table-flush table-striped table-hover" id="srchClntTable" >
			                    <thead class="thead-light">
			                      <tr>					    
			                        <th><div style="width:200px">Name</div></th>
			                        <th><div style="width:90px">Initials</div></th>
			                        <th><div style="width:150px">NRIC</div></th>
			                       	<th><div style="width:90px">DOB</div></th>
			                        <th><div style="width:90px">Contact</div></th>
			                         <!-- <th><div style="width:20px">Create FNA </div></th> -->
			                        <th></th>
			                      </tr>
			                    </thead>
			                   
			                    <tbody>
			                      
			                    </tbody>
			                  </table>
                        </div>
                   
                   
                   </div>
              </div>
              
              
                
               
                </div>
              </div>
            </div>
                  
                  
                </div>
                <div class="modal-footer float-left p-1">
				
                  <button type="button" class="btn btn-sm pl-3 pr-3 btn-bs3-prime d-none" data-dismiss="modal">OK</button>
                  
                </div>
              </div>
            </div>
          </div>
    
  <!--   </div>
    
    </div> -->

<!--  poovathi  add on  09-06-2021-->
  <!-- fnaList Modal Page -->
         
           <div class="modal fade" id="fnaListModal" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="fnaListModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header" id="cardHeaderStyle-22">
                  <h6 class="modal-title" id="fnaListModalLabel"><i class="fa  fa-user-plus"></i>&nbsp;Create / Search Archive List(s).</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:#fff;">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="card">
                    
                      <div class="card-body" style="min-height:50vh;">
                      
                      
                      <div class="row">
					    <div class="col-12">
					      <h6 class="text-muted">Client Details :-</h6> 
					      <ul class="list-group" id="fnaListCountInfo"></ul>
					    </div>
					   </div>
                      
                      <div class="row mt-2">
                      
                   
                      
                      <div class="col-12">
                                <div class="custom-control custom-radio">
								    <input type="radio" class="custom-control-input checkdb " id="radBtnCrtFna" name="radfnaList" 
								     value="CRTFNA" onclick="createNewFnaFun()">
								    <label class="custom-control-label" for="radBtnCrtFna"><i class="fa  fa-user-plus" style="color: #0aa00ad4;"></i>&nbsp;Create new Archive </label>
							   </div>
                      </div>
                      
                       <div class="col-12 mt-4">
                                <div class="custom-control custom-radio">
								    <input type="radio" class="custom-control-input checkdb " id="radBtnSrchFna"
								     name="radfnaList" value="SRCHFNA">
								    <label class="custom-control-label" for="radBtnSrchFna"><i class="fa  fa-search" style="color: #1799b7;">
								    </i>&nbsp; Existing Archive  Details</label>
								 </div>
                           
                       </div>
                          
                       <div class="col-12 mt-3">
                               <div class="form-group ml-3">
                                     <!--poovathi commented on 15-06-2021  -->
									 <!--    <label class="frm-lable-fntsz" for="selFldFnaList" style="margin-bottom: 0px;">Search  Archive List(s):</label> -->
									     <select class="form-control  checkdb" id="selFldFnaList" style="width:350px"
										      aria-hidden="true" onchange="getFnaListVal(this)">
										     	  <!--   <option value="" selected>Select Archive List</option> -->
												    <optgroup id="searchExistFnaList" label="Search Archive List">
												     <!--poovathi commented on 15-06-2021  -->
												    <!--  <option value="">--Select--</option>  -->
													</optgroup>
			                             </select>
			                             
			                             <div class="invalid-feedbacks btnshake hide" id="searchExistFnaListError">
			                                    Please select anyone of the Archive  to Proceed!
			                             </div>
			                   </div>
					  </div>
					  
					  
					    <div class="col-12 mt-2">
                          <div class="p-1 noteInfoMsg" title="Note Information"><small><img src="vendor/avallis/img/infor.png" 
                          alt="Note" class="img-rounded mr-2">&nbsp;<span id="">Click <strong>Proceed</strong> button to view FNA details.</span></small></div>
                      </div> 
                           
                         <input type="hidden" id="hdnCustId">
                           
                           
                      </div>
                        
                      </div>
                      
                    
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-sm btn-bs3-prime" id="addSrchFnaListBtn" onclick="validateCrtNewFna()">Proceed</button>
               </div>
              </div>
            </div>
          </div>
     
     
     <!-- fna List Modal Page End  -->
       
<!--End  -->
 
<script src="vendor/avallis/js/index.js"></script>
<script> 
//AccessComboList = ${GLBL_ACCESS_LEVEL}
</script>
</body>
</html>
  
  