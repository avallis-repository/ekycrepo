<!DOCTYPE html>
<html lang="en">

<head>
    
     <body id="page-top">
         <!-- Container Fluid-->
          <div class="container-fluid" id="container-wrapper" style="min-height:75vh">
		
		
		<div class="d-sm-flex align-items-center justify-content-between mb-1">
		 
		 <div class="col-5">
		 <h6>Client Signature Form</h6>
		 </div>
		 
		 <div class="col-md-7">
		           <jsp:include page="/views/pages/policyE-Submission.jsp"></jsp:include>
		  </div>
		 
</div>

          <div class="row">
              <jsp:include page="/views/pages/signApproveStatus.jsp"></jsp:include>
         </div>

<!--Page 14 content area Start  -->
      <div class="card mb-4 " style="border:1px solid #044CB3;min-height:65vh">
			<div class="card-body" style="font-size: 13px;">
                  <div class="row">
                      <div class="col-md-12">
                           <div class="card card-content-fnsize">
                           
                            <div class="card-header" id="cardHeaderStyle-22" style="font-weight: normal;">
										        <div class="row">
										          <div class="col-md-12">
												 <span> By signing this signature form, I / We agree to:-  </span>
											
												  </div>
										          
										        </div>
										        
										       
							</div>
                              <div class="card-body">
                             
                                   <div class="row">
                                       <div class="col-md-12">
                                       
                                      <ul>
										    <li class="mt-1 ln-hei"><small class="font-sz-level6 ">The application type which I / we have selected to conduct fact finding.</small></li>
										    <li class="mt-1 ln-hei"><small class="font-sz-level6 ">Complete the fact finding process in accordance to the chosen application type to understand my /our financial needs and concern.</small></li>
										    <li class="mt-1 ln-hei"><small class="font-sz-level6 ">Disclose and provide truthful information to my / our best knowledge to allow the representative in providing appropriate advise and product recommendation to address my / our financial needs and concerns.</small></li>
										    <li class="mt-1 ln-hei"><small class="font-sz-level6 "> Disclose and supply any information to my / our best knowledge on tax matters required under FATCA and CRS and any applicable intergovernmental agreements.</small></li>
                                            <li class="mt-1 ln-hei"><small class="font-sz-level6 ">Sign the required fields of this document and understand that the provision of such signatures constitute a legally binding transaction and agreement.</small></li>
                                      </ul>  
                                       
                                       
                                       
                                      
                                       </div>
                                     </div>
                             
                                  
                               </div>
                       </div>
                  </div>
              </div>
              
              <div class="row">
                      <div class="col-md-12">
                             <div class="card card-content-fnsize" style="margin-top: 15px;">
                           
                            <div class="card-header" id="cardHeaderStyle-22" style="font-weight: normal;">
										        <div class="row">
										          <div class="col-md-12">
												 <span> I / We further acknowledge and agree that:  </span>
											
												  </div>
										          
										        </div>
										        
										       
							</div>
                              <div class="card-body">
                              <!--  -->
                                   <div class="row">
                                   <div class="col-md-12">
                                   
                                      <ul>
										    <li class="mt-1 ln-hei"><small class="font-sz-level6 ">The representative has fully and completely explained to me / us on the fact finding process and the contents of this documents to my / our satisfaction.
										    </small></li>
										    <li class="mt-1 ln-hei"><small class="font-sz-level6 "> The representative has reviewed my /our concerns and objectives based on my / our disclosure to perform advise and product recommendation.
										    </small></li>
										    <li class="mt-1 ln-hei"><small class="font-sz-level6 ">The advisory and recommendation on my / our financial needs were accessed through reasonable subjective assumptions at this point in time and may not reflect the actual occurrence in the future event.
										    </small></li>
										    <li class="mt-1 ln-hei"><small class="font-sz-level6  text-primaryy italic bold">The representative has presented and explained to me / us to my / our satisfaction on the details of the recommended product(s):</small></li>
										         
										         <ul class="mt-2 lh-20">
													    <li><small class="font-sz-level7 ">Product provider</small></li>
													    <li><small class="font-sz-level7 ">How the recommended product(s) is suitable and be able to address my / our financial concern across the targeted time horizon to meet my / our targeted objective(s).</small></li>
													    <li><small class="font-sz-level7 ">Benefits of the recommended product(s) including guaranteed and non-guaranteed component.</small></li>
													    <li><small class="font-sz-level7 ">Risk Factor of the recommended product(s)</small></li>
													    <li><small class="font-sz-level7 ">Premium / Investment cost, Frequency of payment and the charges / fees incurred for the recommended product(s).</small></li>
													    <li><small class="font-sz-level7 ">Warnings, exclusion and limitation on my /our application which is subjected to my /our disclosure</small></li>
													    <li class="mt-1"><small class="font-sz-level7 ">The representative has highlighted to me / us on the free look period of 14 days for life policies and the cancellation period of 7 calendar days for Unit Trusts from the date that I / we signs the purchase agreement.</small></li>
                                                 </ul>
										    
										    <li class="mt-2 ln-hei"><small class="font-sz-level6">Any product(s) recommended which subsequently not taken up by me/ us for any reason(s) shall not be valid.</small></li>
                                      </ul>
                                   
                                   
                                   
   
</div>
                                   </div>
                              
                                  
                               </div>
                       </div>
                  </div>
              </div>
              
               <div class="row">
                      <div class="col-md-12">
                              <div class="card card-content-fnsize" style="margin-top: 15px;">
                           
                            <div class="card-header" id="cardHeaderStyle-22" style="font-weight: normal;">
										        <div class="row">
										          <div class="col-md-12">
												 <span> I / We confirmed that I /we have read through the details of this document and agrees to the contents
                                                       indicated in this fact finding document:-  </span>
											
												  </div>
										          
										        </div>
										        
										       
							</div>
                              <div class="card-body">
                              <!--  -->
                                   <div class="row">
                                      <div class="col-md-4">
                                           <div class="card h-100" id="SignCard">
                                               <div class="card-header" id="cardHeaderStyle" style="font-weight: normal;">
												        <div class="row">
														          <div class="col-md-12">
																 <span><img src="vendor/avallis/img/client1.png">&nbsp;Client(1)  </span>
															
																  </div>
												          
												        </div>
								               </div>
  <div class="card-body">
                                                    <div class="row">
                                                         <div class="col-md-6">
                                                             <label class="frm-lable-fntsz p-0">Sign. of Client (1):  </label>
                                                            
                                                         </div>
                                                          <div class="col-md-6">
                                                               <div id="client1QrSecNoData" class="client1QrSecNoData show">
								                               <!--   <small><img src="vendor/avallis/img/test.png" class="client1Qr "></small> -->
								                                 <p class="center noData" id="" style="font-size: 11px;"><img src="vendor/avallis/img/warning.png" class="">
								                                 &nbsp;Client (1) Signature Not Found&nbsp;</p>
								                               </div> 
								                               
								                               <div id="sign-QR_style" class=" client1QrSec hide">
								                                <small><img src="vendor/avallis/img/contract.png" class="client1Qr "></small>
								                               </div> 
                                                          </div>
                                                    
                                                    </div>
                                                    
                                                    <div class="row pt-2">
                                                          <div class="col-md-6">
                                                           <label class="frm-lable-fntsz p-0">Name of Client (1):  </label>
                                                         </div>
                                                         
                                                         <div class="col-md-6">
                                                         <p> <span class="frm-lable-fntsz font-normal"> ${SESS_DF_SELF_NAME}</span></p>
                                                         </div> 
                                                
                                                </div>
                                                
                                                <div class="row pt-2">
                                                          <div class="col-md-6">
                                                           <label class="frm-lable-fntsz">Date : </label>
                                                         </div>
                                                         
                                                          <div class="col-md-6">
                                                         <p><span class="frm-lable-fntsz font-normal" id="selfSignDate">  </span></p>
                                                         </div> 
                                                
                                                </div>
                                                
                                            
                                           </div>
                                      </div>
                                 </div>      
                                      <div class="col-md-4">
                                           <div class="card h-100" id="SignCard">
                                               <div class="card-header" id="cardHeaderStyle" style="font-weight: normal;">
												        <div class="row">
														          <div class="col-md-12">
																 <span><img src="vendor/avallis/img/client2.png">&nbsp;Client(2) </span>
															         
																  </div>
												          
												        </div>
								               </div>
                                                  <div class="card-body">
                                                              <div class="row">
                                                         <div class="col-md-6">
                                                             <label class="frm-lable-fntsz p-0">Sign. of Client (2):  </label>
                                                            
                                                         </div>
                                                          <div class="col-md-6">
                                                               <div id="client2QrSecNoData"  class="client2QrSecNoData show">
								                                 <p class="center noData" id="" style="font-size: 11px;"><img src="vendor/avallis/img/warning.png" class="">
								                                 &nbsp;Client (2) Signature Not Found&nbsp;</p>
								                              </div> 
								                              
								                              <div id="sign-QR_style" class=" client2QrSec hide">
								                                 <small><img src="vendor/avallis/img/contract.png" class="client2Qr"></small>
								                               </div>
								                         </div>   
                                                    </div>
                                                    
                                                    <div class="row pt-2">
                                                          <div class="col-md-6">
                                                           <label class="frm-lable-fntsz">Name of Client (2) :  </label>
                                                         </div>
                                                         
                                                         <div class="col-md-6">
                                                         <p> <span class="frm-lable-fntsz font-normal">${SESS_DF_SPS_NAME}   </span></p>
                                                         </div> 
                                                
                                                </div>
                                                
                                                <div class="row pt-2">
                                                          <div class="col-md-6">
                                                           <label class="frm-lable-fntsz">Date : </label>
                                                         </div>
                                                         
                                                          <div class="col-md-6">
                                                         <p><span class="frm-lable-fntsz font-normal" id="spsSignDate">  </span></p>
                                                         </div> 
                                                
                                                </div>
                                                
                                                
                                                </div>
                                            
                                           </div>
                                      
                                      </div>
                                      
                                      
                                      <div class="col-md-4">
                                           <div class="card h-100" id="SignCard">
                                               <div class="card-header" id="cardHeaderStyle" style="font-weight: normal;">
												        <div class="row">
														          <div class="col-md-12">
																 <span> <img src="vendor/avallis/img/agent.png">&nbsp;Representative </span>
															
																  </div>
												          
												        </div>
								               </div>
                                             <div class="card-body">
                                                              <div class="row">
                                                         <div class="col-md-6">
                                                             <label class="frm-lable-fntsz">Sign. of Adviser :  </label>
                                                            
                                                         </div>
                                                          <div class="col-md-6">
                                                                 <div id="advQrSecNoData"  class="advQrSecNoData show">
								                                   <p class="center noData" id="" style="font-size: 11px;"><img src="vendor/avallis/img/warning.png" class="">
								                                     &nbsp;Adviser Signature Not Found&nbsp;</p>
								                                  </div>
								                                  
								                                  <div id="sign-QR_style" class="advQrSec hide">
								                                 <small><img src="vendor/avallis/img/contract.png" class="advQr"></small>
								                               </div>
								                            </div> 
                                                    </div>
                                                    
                                                    <div class="row pt-2">
                                                          <div class="col-md-6">
                                                           <label class="frm-lable-fntsz">Name of Adviser :  </label>
                                                         </div>
                                                         
                                                         <div class="col-md-6">
                                                         <p> <span class="frm-lable-fntsz font-normal"> ${LOGGED_USER_INFO.FNA_ADVSTFNAME} </span></p>
                                                         </div> 
                                                
                                                </div>
                                                
                                                <div class="row pt-2">
                                                          <div class="col-md-6">
                                                           <label class="frm-lable-fntsz">Date : </label>
                                                         </div>
                                                         
                                                          <div class="col-md-6">
                                                         <p><span class="frm-lable-fntsz font-normal" id="advSignDate"> </span></p>
                                                         </div> 
                                                
                                                </div>
                                                
                                                  
                                                
                                                   
                                                </div>
                                            
                                           </div>
                                      
                                      </div>
                                    </div>
                                    
                             </div>
                          </div>
                  </div>
             </div>
             
 
             
             </div>
   </div><!-- page 14 content Area End -->
      
      
  </div>
        
        <!-- container-fluid-end -->
  
  
  </body>
   
   <script type="text/javascript" src="vendor/avallis/js/client_signature.js"></script>
   
     <script>jsnDataFnaDetails = ${CURRENT_FNA_DETAILS}</script>
     
  </html>
