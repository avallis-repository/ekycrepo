<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>

body{
overflow-x: hidden;
overflow-y: auto;
}

.container {
    margin-top: 100px;
    margin-bottom: 100px
}

p {
    font-size: 14px
}



.cursor-pointer {
    cursor: pointer;
    color: #42A5F5
}

.pic {
    margin-top: 45px;
    margin-bottom: 45px;
}

.card-block {
    width: 200px;
    border: 1px solid lightgrey;
    border-radius: 5px !important;
    background-color: #FAFAFA;
    margin-bottom: 30px
}

.card-body.show {
    display: block
}

.card {
    padding-bottom: 20px;
    box-shadow: 2px 2px 6px 0px rgb(200, 167, 216)
}

.radio {
    display: inline-block;
    border-radius: 0;
    box-sizing: border-box;
    cursor: pointer;
    color: #000;
    font-weight: 500;
    -webkit-filter: grayscale(100%);
    -moz-filter: grayscale(100%);
    -o-filter: grayscale(100%);
    -ms-filter: grayscale(100%);
    filter: grayscale(100%)
}

.radio:hover {
    box-shadow: 2px 2px 2px 2px rgba(0, 0, 0, 0.1)
}

.radio.selected {
    box-shadow: 0px 8px 16px 0px #EEEEEE;
    -webkit-filter: grayscale(0%);
    -moz-filter: grayscale(0%);
    -o-filter: grayscale(0%);
    -ms-filter: grayscale(0%);
    filter: grayscale(0%)
}

.selected {
    background-color: #E0F2F1
}

.a {
    justify-content: center !important
}

  .hide{
   display: none;
   }
   
   .show{
   display : block;
   }

   
/* .btn {
    border-radius: 0px
}

.btn,
.btn:focus,
.btn:active {
    outline: none !important;
    box-shadow: none !important
}
 */

</style>
</head>
<body>
<div class="container">
    <div class="row justify-content-center">
     <div class="col-md-12">
            <div class="card text-center justify-content-center shaodw-lg card-1 border-0 bg-white px-sm-2">
                <div class="card-body show ">
                
                           <!--    <div class="col-12">
                                       <div class="col-12 pb-3 ">
												<input class="checkbox-budget" type="radio" name="radBtnCrteNewFna" id="radBtnCrteNewFna" value="LIFE" onclick="">
												<label class="for-checkbox-budget" for="radBtnCrteNewFna">
													<img src="vendor/avallis/img/life.jpg" class="rounded-circle" style="width:150px"><br>Create New FNA Id &amp; Proceed
												</label>
												
												<input class="checkbox-budget" type="radio" name="radBtnSrchExstFna" id="radBtnSrchExstFna" value="INVESTMENT" onclick="">
												<label class="or-checkbox-budget" for="radBtnSrchExstFna">
													<img src="vendor/avallis/img/investment7.jpg" class="rounded-circle" style="width:150px"><br>Search Existing FNA Id &amp; Proceed
												</label>
												
											
									  </div>
                            </div> -->
                
                
                
                
                
                
                
                
                
                </div>
                
                  <div class="row">
                        <div class="col">
                            <h5><b>FNA Listings</b></h5>
                            <p> Select any one of the option below to generate KYC Form </p>
                        </div>
                    </div>
                    <div class="radio-group row justify-content-between px-3 text-center a">
                        <div class="col-5 card-block py-0 text-center radio" id ="radbtnNewFnaId" onclick="createNewFna()">
                            <div class="flex-row">
                                <div class="col">
                                    <div class="pic"> <img class="irc_mut img-fluid" src="https://i.imgur.com/6r0XCez.png" width="100" height="100"> </div>
                                    <p>Create New FNA Id &amp; Proceed</p>
                                </div>
                            </div>
                        </div>
                         <div class="col-1"></div>
                        <div class="col-5 card-block py-0 text-center radio " id="radbtnSrchFnaId" onclick ="SrchExistFnaList()">
                            <div class="flex-row">
                                <div class="col">
                                    <div class="pic"> <img class="irc_mut img-fluid" src="https://i.imgur.com/bGCqROr.png" width="100" height="100"> </div>
                                    <p>Search Existing FNA Id &amp; Proceed</p>
                                </div>
                                
                            </div>
                            
                             <label class="frm-lable-fntsz left selFldFnaList show" for="selFldFnaList">Select FNA Id :</label>  
								   <select class="form-control mb-2 form-control-lg selFldFnaList show" name="selFldFnaList" id="selFldFnaList"  autocomplete="nope">
								        <option value="" selected="">--Select any one of the Option below--</option>
								   </select>
								    <div class="invalid-feedbacks hide" id="selFldFnaListError">Select any on of the FNA Id</div>
                         </div>
                         
                    </div>
                    
                    
               
                    <div class="row justify-content-between">
                        <div class="col-auto"> <button type="button" class="btn btn-bs3-prime font-sz-level3"> <span class="mr-2"><i class="fa fa-angle-left" aria-hidden="true"></i></span> prev</button> </div>
                        <div class="col-auto"> <button type="button" class="btn btn-bs3-success font-sz-level3" id="btnGenerateFna">Generate EKYC <span class="ml-2"><i class="fa fa-angle-right" aria-hidden="true"></i></span> </button> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</body>
<script>var fnaList = ${LATEST_FNA_DETAILS}</script>
<!-- <script>

$(document).ready(function () {
	$('#cover-spin').hide(0);
	/*  $('.radio-group .radio').click(function () {
	$('.selected .fa').removeClass('fa-check');
	$('.radio').removeClass('selected');
	$(this).addClass('selected');
	}); */ 
	   if(!isEmpty(fnaList) && !isJsonObjEmpty(fnaList) ){
     	  
     	  for(var fnaKey in fnaList)
       			  
       			  if (fnaList.hasOwnProperty(fnaKey)) {

       	        		var FnaVal = fnaList[fnaKey];
       	        		
       	        		
       	        		switch(fnaKey){
       	        		
       	        		case "fnaId":
       	        			 var newOpt = '<option value="'+FnaVal+'">'+FnaVal+'</option>';
       	        			 $("#selFldFnaList").append(newOpt);
       	        			break;
       		      		default:
  		      				   $("#"+fnaKey).val(FnaVal);
       		      		}
       		}
       	  }
	
	});

function createNewFna(){
	$("#radbtnNewFnaId").addClass("selected")
	$("#radbtnSrchFnaId").removeClass("selected");
	$(".selFldFnaList").addClass("hide").removeClass("show");
	$("#selFldFnaList").val('')
	//window.location.href="kycIntro"
}

function SrchExistFnaList(){
	$("#radbtnSrchFnaId").addClass("selected")
	$("#radbtnNewFnaId").removeClass("selected")
	$(".selFldFnaList").removeClass("hide").addClass("show");
	$("#selFldFnaList").val('')
}

$("#btnGenerateFna").bind("click",function(){
var createFnaSec  = $("#radbtnNewFnaId").hasClass("selected");
var SrchFnaSec = $("#radbtnSrchFnaId").hasClass("selected");
 if( createFnaSec == false && SrchFnaSec == false ){
		alert("Please select any one  option to generate KYC")
	}else if(createFnaSec == true){
		
		// call ajax function for fna insert action for everytime
		var curntFnaId = $("#selFldFnaList > option:selected").val();
		    alert("curntFnaId"+curntFnaId)
		    window.location.href="kycIntro";
		clrKycIntroFrmDetls();
		$("#fnaId").val("")
		alert("Fna id value"+$("#fnaId").val(""))
		var kycInroFrmDetls = $('#KycIntroFrm :input').serializeObject();
		console.log(JSON.stringify(kycInroFrmDetls))
	    $.ajax({
	            url:baseUrl+"/fnadetails/register/FnaDetls/",
	            type: "POST",
	            dataType: "json",
	            async:false,
	            contentType: "application/json",
	            data: JSON.stringify(kycInroFrmDetls),
	            success: function (response) {
	            //	alert(response)
	            	// $('#cover-spin').hide(0);
	            	//$("#fnaId").val(response.FNA_ID);
	            	//window.location.href="kycHome";
	            },
		 		error: function(xhr,textStatus, errorThrown){
	            	// $('#cover-spin').hide(0);
		 			//alert("xhr,textStatus, errorThrown>",xhr,textStatus, errorThrown);
		 			}
	            });
		
		//window.location.href="kycIntro";
		
	} else if(SrchFnaSec == true){
	var curntFnaId = $("#selFldFnaList > option:selected").val();
	if(isEmpty(curntFnaId)){
		$("#selFldFnaList").addClass("err-fld");
		$("#selFldFnaListError").addClass("show").removeClass("hide");
		return false;
	}else{
		$("#selFldFnaList").removeClass("err-fld");
		$("#selFldFnaListError").removeClass("show").addClass("hide");
		//return true;
		window.location.href="kycIntro";
		clrKycIntroFrmDetls();
		
	}
	}
})

$("#selFldFnaList").bind("change",function(){
	var curntFnaId = $(this).val()
	if(isEmpty(curntFnaId)){
		$("#selFldFnaList").addClass("err-fld");
		$("#selFldFnaListError").addClass("show").removeClass("hide");
		return false;
	}else{
		$("#selFldFnaList").removeClass("err-fld");
		$("#selFldFnaListError").removeClass("show").addClass("hide");
		return true;
		
	}
});

function clrKycIntroFrmDetls(){
	//KycIntroFrm
	alert("clear forms................")
	$("#KycIntroFrm").find("input,textarea,select").val("");
	$("#KycIntroFrm").find(':input').removeAttr("readOnly");
	$("#KycIntroFrm").find(':input').removeAttr("disabled");
	$("#KycIntroFrm").find('input:checkbox').prop('checked',false);
	$("#KycIntroFrm").find('input:radio').prop('checked',false);
}

</script> -->
</html>
