<%@ page language="java" import="java.util.*" %>
<%ResourceBundle Ekycprop = ResourceBundle.getBundle("AppResource");%>
<html lang="en">
<head>
<link href="vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" >
</head>
<body id="page-top">
        
<div class="container-fluid" id="container-wrapper" style="min-height:75vh">
		 <div class="d-sm-flex align-items-center justify-content-between mb-1">
		        <div class="col-md-5">
		            <h6>Personal Details</h6>
		        </div>
		        
		        <div class="col-md-7">
		            <jsp:include page="/views/pages/policyE-Submission.jsp"></jsp:include>
				</div>
          </div>
          
          <div class="row">
             <jsp:include page="/views/pages/signApproveStatus.jsp"></jsp:include>
         </div>
          
		<div class="card mb-1 maincontentdiv " style="border:1px solid #044CB3;min-height:65vh" id="fnaPersonalForm">
			
		 	<input type="hidden" name="fnaId" id="fnaId" value="${CURRENT_FNAID}" size="20">
		 	<input type="hidden" name="dataformId" id="dataformId" value="${CURRENT_DATAFORM_ID}"/>
		    <input type="hidden" name="resAddr1" id="resAddr1"/>
			<input type="hidden" name="resAddr2" id="resAddr2"/>
			<input type="hidden" name="resAddr3" id="resAddr3"/>
			<input type="hidden" name="resCity" id="resCity"/>
			<input type="hidden" name="resState" id="resState"/>
			<input type="hidden" name="resCountry" id="resCountry"/>
			<input type="hidden" name="resPostalcode" id="resPostalcode"/>
			<input type="hidden" name="dfSelfHeight" id="dfSelfHeight"/>
			<input type="hidden" name="dfSelfWeight" id="dfSelfWeight"/>
			<input type="hidden" name="dfSpsHeight" id="dfSpsHeight"/>
			<input type="hidden" name="dfSpsWeight" id="dfSpsWeight"/>
			<input type="hidden" name="fnaCreatedBy" id="fnaCreatedBy"/>
			<input type="hidden" name="fnaCreatedDate" id="fnaCreatedDate"/>
			
			<input type="hidden" name="dfSelfTaxres" id="dfSelfTaxres"/>
			<input type="hidden" name="dfSelfTaxresOth" id="dfSelfTaxresOth"/>
			<input type="hidden" name="dfSpsTaxres" id="dfSpsTaxres"/>
			<input type="hidden" name="dfSpsfTaxresOth" id="dfSpsfTaxresOth"/>
			<input type="hidden" name="dfSelfUsnotifyflg" id="dfSelfUsnotifyflg"/>
			<input type="hidden" name="dfSpsUsnotifyflg" id="dfSpsUsnotifyflg"/>
			<input type="hidden" name="dfSelfUspersonflg" id="dfSelfUspersonflg"/>
			<input type="hidden" name="dfSpsUspersonflg" id="dfSpsUspersonflg"/>
			<input type="hidden" name="dfSelfUstaxno" id="dfSelfUstaxno"/>
			<input type="hidden" name="dfSpsUstaxno" id="dfSpsUstaxno"/>
			<input type="hidden" name="currAdviserId" id="currAdviserId" value="${FNA_SERVADV_ID}"/>
		      <div class="card-body">
		 
		
		<!--Tabs  -->
		<div class="row">
		
		 <div class="tab-regular" id="clntProdTabs" style="width:100%;">
		 
		  <ul class="nav nav-tabs small" id="myTab" role="tablist" style="list-style-type:none;">
                <li class="nav-item" style="list-style-type:none;">
                	<a class="nav-link active dfself" id="client1-tab" data-toggle="tab" href="#client1" role="tab" aria-controls="client1" aria-selected="true" title="${CURRENT_CUSTNAME}">
                		${CURRENT_CUSTNAME}
                	</a>
                </li>
				<li class="nav-item ml-auto">
					<a class="btn btn-link" href="javascript:void(0)" onclick="javascript:expandAll($(this))" id="expandcollape"><i class="fa fa-expand" id="iconStyle-02" aria-hidden="true"></i>&nbsp;Expand All</a>
				</li>				 
                <li class="nav-item" id="btnAddNewInsur">
                	<a class="btn btn-link" href="javascript:void(0)" id="listStyle-02" onclick="addNewInsured()"><i class="fa fa-user-plus" id="iconStyle-01" aria-hidden="true"></i>&nbsp;Add New Client</a>
                </li>				
            </ul>
                              <div class="tab-content" id="myTabContent" style="margin: 15px;">
                                 <div class="tab-pane fade show active" id="client1" role="tabpanel" aria-labelledby="client1-tab">
									         <div class="row" id="accordian-row">
										        <div class="col-6">
										            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
										                <div class="panel panel-default">
										                    <div class="panel-heading" role="tab" id="headingOne">
										                        <h4 class="panel-title">
										                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
										                                <i class="fa fa-user" style="color:#4caf50;"></i>&nbsp;Personal Details
										                            </a>
										                        </h4>
										                    </div>
										                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
										                        <div class="panel-body" style="min-height: 61vh;">
																<div class="card">
																						
																	<div class="card-body">
																			<div class="form-group">
										                                                   <div class="row">
										                                                 <div class="col-md-12">
										                                                     <label class="frm-lable-fntsz" for="dfSelfName">Name(as per NRIC)</label>
										                                                     <input id="dfSelfName" name="dfSelfName" type="text"  class="form-control input-md checkdb" maxlength="75" >
										                                                 </div>
										                                            </div>
										                                              </div>
																	   
																	   
																	   <div class="form-group">
										                                                   <div class="row">
										                                                 <div class="col-md-6">
										                                                     <label class="frm-lable-fntsz" for="dfSelfNric">NRIC/Passport No.</label>
										                                                     <input type="text" id="dfSelfNric" name="dfSelfNric" class="form-control input-md checkdb" maxlength="20" >
										                                                </div>
										                                                
																				    <div class="col-md-6">
																				       <!--  <div class="form-group">
																						       <button type="button" class="btn btn-toggle mt-4" id="btndfSelfGender" data-toggle="button" aria-pressed="false" >
																									<div class="handle"></div>
																								</button>
																								<input type = "hidden" name="dfSelfGender" id="dfSelfGender">
																						</div> -->
																						
																						<div class="row">
																						    <div class="col-6">
																							      <div id="custTypeRadioBtn" class="mt-3">
																									  <input type="radio" name="dfSelfGender" id="radBtnSelfmale" value="M" maxlength="10" class="checkdb dfSelfGendergrp">
																									  <label for="radBtnSelfmale" class="pt-1" style="height:5vh;">
																									  <img src="vendor/avallis/img/man.png">&nbsp;Male
																									  </label>
																				                 </div>
																							</div>
																							
																							<div class=" col-6">
																							      <div id="custTypeRadioBtn" class="mt-3">
																									  <input type="radio"  name="dfSelfGender" id="radBtnSelffemale" value="F" maxlength="10" class="checkdb dfSelfGendergrp">
																									  <label for="radBtnSelffemale" class="pt-1" style="height:5vh;">
																									  <img src="vendor/avallis/img/woman.png">&nbsp;Female
																									  </label>
																							     </div>
																							</div>
																						
																						<input type="hidden" id="hTxtSelfGender"/>
																						</div>
												   
																						 </div>
																				
										                                            </div>
										                                              </div>
																	   
																	   <div class="form-group">
										                                                    <div class="row">
										                                                 
										                                                 
										                                                 <div class="col-md-6">
										                                                      <div class="form-group" id="simple-date1">
										                                                              <label class="frm-lable-fntsz" for="dfSelfDob">Date of Birth</label>
																                      <div class="input-group input-group date">
																                        <div class="input-group-prepend">
																                          <span class="input-group-text"><i class="fas fa-calendar"></i></span>
																                        </div>
																                        <input type="text"  name="dfSelfDob" id="dfSelfDob" class="form-control checkdb" maxlength="10" >
																                        <div class="invalid-feedbacks hide" id="dobError"></div>
																                      </div>
																                      
																                      <small id="selfdobHelp" class="form-text text-muted font-sz-level8 mt-1">date Format : DD/MM/YYYY</small>
																                      
																                      
										           										    </div>
										           										     <input type="hidden" name="dfSelfAge" id="dfSelfAge"/>
										                                                   
										                                                 </div>
																				
																				<div class="col-md-6">
										                                                     <label class="frm-lable-fntsz" for="name">Marital Status</label>
										                                                       <select class="form-control checkdb" name="dfSelfMartsts" id="dfSelfMartsts" maxlength="20">
										                                                    <option selected value="">--Select--</option>
																						    <option value="Single">Single</option>
																						    <option value="Married">Married</option>
																						    <option value="Separated">Separated</option>
																						    <option value="Divorced">Divorced</option>
																						    <option value="Attached">Attached</option>
																						    <option value="Widow">Widow</option>
																						    <option value="Widower">Widower</option>
										                                                        </select>
										                                                 </div>
																				
										                                             </div>    
										                                             </div>
																	  
																	  
																	  <div class="form-group">
										                                       <div class="row">
																			 			<div class="col-md-6">
										                                                     <label class="frm-lable-fntsz" for="name">Country of Birth</label>
										                                                          <select class="form-control checkdb" name="dfSelfBirthcntry" id="dfSelfBirthcntry" >
										                                                          <option value="" selected>--Select--</option>
																								    <% for(String country:Ekycprop.getString("app.country").split("\\^")){ %>
																								    	<option value="<%=country%>"><%=country%></option>
																								     <%}%>
										                                                        </select>
										                                              </div>
																				
																				
																				<div class="col-6">
																				<span id="spanSelfNatlyDets"></span>
																				 <label class="frm-lable-fntsz" for="name">Nationality</label>
																				           <div class="custom-control custom-radio">
																									    <input type="radio" class="custom-control-input checkdb dfSelfNationalitygrp" type="radio" name="dfSelfNationality" id="dfSelfNationalitySign"
																									     value="SG"  onclick="fnaNaltyFlg(this,'dfSelfNatyDets');removeErrFld(this)" maxlength="30">
																									    <label class="custom-control-label  frm-lable-fntsz font-normal" for="dfSelfNationalitySign">Singaporean</label>
																					        </div>
																					
																					        <div class="custom-control custom-radio">
																									    <input type="radio" class="custom-control-input checkdb dfSelfNationalitygrp" type="radio" name="dfSelfNationality"
																									     id="dfSelfNationalitySign-PR" value="SGPR"  onclick="fnaNaltyFlg(this,'dfSelfNatyDets')" maxlength="30">
																									    <label class="custom-control-label  frm-lable-fntsz font-normal" for="dfSelfNationalitySign-PR"> Singapore - PR</label>
																					         </div>
																					
																					
																					          <div class="custom-control custom-radio">
																									    <input type="radio" class="custom-control-input checkdb dfSelfNationalitygrp" type="radio" name="dfSelfNationality" id="dfSelfNationalityOth" value="OTH"
																									     onclick="fnaNaltyFlg(this,'dfSelfNatyDets')" maxlength="30">
																									    <label class="custom-control-label  frm-lable-fntsz font-normal" for="dfSelfNationalityOth">Others</label>
																							  </div>
																																														
																					
																					<div class="form-check ">
																					  <select class="form-control invisible checkdb" id="dfSelfNatyDets"
																					   name="dfSelfNatyDets" maxlength="60">
																								<option value="" selected>--Select--</option>
																								<%for(String data:Ekycprop.getString("app.citizenship").split("\\^")){ %>
																									<option value="<%=data%>"><%=data%></option>
																								<%} %>
																														  
																					</select>		
																					</div>
																					
																				</div>
																				
																			 </div>
																	  </div>
																	</div>
																						
																</div>
																
																</div>
										                    </div>
										                </div>
										                <div class="panel panel-default">
										                    <div class="panel-heading" role="tab" id="headingTwo">
										                        <h4 class="panel-title">
										                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
										                               <i class="fa fa-map-marker" style="color: #f33;"></i>&nbsp;Address Details
										                            </a>
										                        </h4>
										                   </div>
										                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
															
															<div class="panel-body" style="min-height: 36vh;">
															
															 <div class="card">
																						
																						<div class="card-body">
																						
																						
																						<div class="form-group">
										                                                        <div class="row">
												                                                    <div class="col-md-12">
												                                                        <label class="frm-lable-fntsz" for="dfSelfHomeaddr">Registered Residential Address</label>
												                                                        <input id="dfSelfHomeaddr" name="dfSelfHomeaddr" type="text"  class="form-control input-md checkdb" 
												                                                        onblur = "enableSelfSpsSameAddrFlg('dfSelfHomeaddr','self')" maxlength="450">
												                                                    </div>
												                                              </div>
										                                                 </div>
										                                                 
																						   <div class="form-group">
										                                                        <div class="row">
												                                                    <div class="col-md-12">
												                                                     
												                                                         <div class="custom-control custom-checkbox">
																										    <input type="checkbox" class="custom-control-input checkdb" name="dfSelfSameasregadd" id="chkSelfSameAddrFlg" onclick="sameAsResiAddr(this,'chkSelfNoSameAddrFlg')" value="N" maxlength="20">
																										    <label class="custom-control-label frm-lable-fntsz" for="chkSelfSameAddrFlg">Mailing Address is same as Residential Address</label>
																										  </div>
																																						                                                    
												                                                    
												                                                        <!-- <div class="checkbox">
													                                                        <label for="time-01">
													                                                        <input type="checkbox" name="dfSelfSameasregadd" id="chkSelfSameAddrFlg" onclick="sameAsResiAddr(this,'chkSelfNoSameAddrFlg')" value="">&nbsp;<span class="frm-lable-fntsz">Mailing Address is same as Residential Address</span></label>
													                                                    </div> -->
												                                                    </div>
												                                                </div>
												                                          </div>
																						  
																						  
																						  <div class="form-group">
										                                                        <div class="row">
												                                                    <div class="col-md-12">
												                                                    
												                                                    <div class="custom-control custom-checkbox">
																										    <input type="checkbox" class="custom-control-input checkdb" name="dfSelfMailaddrflg" id="chkSelfNoSameAddrFlg" onclick="NotsameAsResiAddr(this,'chkSelfSameAddrFlg')" value="N" maxlength="20">
																										    <label class="custom-control-label frm-lable-fntsz" for="chkSelfNoSameAddrFlg">If different from Registered Residential Address.
										                                                                   </label>
																									 </div>
												                                                     <!--  <div class="checkbox">
													                                                        <label for="time-02">
													                                                        <input type="checkbox" name="dfSelfMailaddrflg" id="chkSelfNoSameAddrFlg" onclick="NotsameAsResiAddr(this,'chkSelfSameAddrFlg')" >&nbsp;<span class="frm-lable-fntsz text-wrap txtarea-hrlines">If different from Registered Residential Address.
										                                                                   </span></label>
													                                                    </div> -->
												                                                    </div>
												                                                </div>
												                                                <div class="row pl-3 ">
												                                                      <div class="col- "><img src="vendor/avallis/img/infor.png" class="mt-2"></div>
												                                                    <div class="col-11">
													                                                     <div class="col-">
													                                                       <small class="font-italic bold text-primaryy font-sz-level8">Please submit a proof of address issued within the last 6 months(e.g.) Bank/Utility/Phone bill statement.</small>
													                                                     </div>
												                                                     </div>
												                                                </div>
												                                                 
												                                          </div>
												                                          
																						     <div class="form-group">
										                                                        <div class="row">
												                                                    <div class="col-md-12">
												                                                        <input  name="dfSelfMailaddr" id="dfSelfMailaddr" type="text"   class="form-control input-md checkdb" maxlength="450">
												                                                    </div>
												                                                </div>
												                                           </div>
																						   
																						   
																						</div>
																						
																						</div>
															</div>
										                        
										                    </div>
										                </div>
										                <div class="panel panel-default">
										                    <div class="panel-heading" role="tab" id="headingThree">
										                        <h4 class="panel-title">
										                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
										                               <i class="fa fa-phone-square" style="color: #028686;"></i>&nbsp;Contact Details
										                            </a>
										                        </h4>
										                    </div>
										                    
										                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
										                        <div class="panel-body">
										                             <div class="card">
																						
																						<div class="card-body">
																						
																						<div class="form-group">
										                                                        <div class="row">
												                                                    <div class="col-md-6">
												                                                        <label class="frm-lable-fntsz" for="dfSelfMobile">Mobile</label>
												                                                        <input id="dfSelfMobile" name="dfSelfMobile" type="text"  class="form-control input-md checkdb dfSelfMobilegrp" maxlength="60" >
												                                                 </div>
												                                                    
												                                                    <div class="col-md-6">
												                                                        <label class="frm-lable-fntsz" for="dfSelfHome">Home</label>
												                                                        <input id="dfSelfHome" name="dfSelfHome" type="text"  class="form-control input-md checkdb dfSelfMobilegrp"  maxlength="60">
												                                                    </div>
												                                               </div>
										                                                   </div>
																						   
																						       <div class="form-group">
										                                                        <div class="row">
												                                                    <div class="col-md-6">
												                                                        <label class="frm-lable-fntsz" for="dfSelfOffice">Office</label>
												                                                        <input id="dfSelfOffice" name="dfSelfOffice" type="text"  class="form-control input-md checkdb dfSelfMobilegrp" maxlength="60" >
												                                                    </div>
												                                                    
												                                                    <div class="col-md-6">
												                                                        <label class="frm-lable-fntsz" for="dfSelfPersemail">Email Address</label>
												                                                        <input id="dfSelfPersemail" name="dfSelfPersemail" type="email"  class="form-control input-md checkdb" 
												                                                        onblur="validateEmail(this,'dfSelfPersemail')"  maxlength="60">
												                                                        <div class="invalid-feedbacks hide" id="dfSelfPersemailError"></div>
												                                                    </div>
												                                               </div>
										                                                   </div>
																						</div>
																						</div>
										                                 </div>
										                    </div>
										                </div>
										            </div>
										        </div>
												<div class="col-6">
										            <div class="panel-group" id="accordion_right" role="tablist" aria-multiselectable="true">
										                <div class="panel panel-default">
										                    <div class="panel-heading" role="tab" id="headingOneR">
										                        <h4 class="panel-title">
										                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_right" href="#collapseOneR" aria-expanded="true" aria-controls="collapseOneR">
										                                <i class="fa fa-money" style="color:#ff9800 ;"></i>&nbsp;Employment &amp; Financial Details
										                            </a>
										                        </h4>
										                    </div>
										                    
										                    <div id="collapseOneR" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOneR" style="margin-bottom:15px;">
										                        <div class="panel-body" >
										                           <div class="row">
																			  <div class="col-6">
																			  <div class="card ">
																						<div class="card-header" id="cardHeaderStyle"><span>Employment Info</span></div>
																						<div class="card-body">
																						
																						 
										  												<div class="form-group">
										                                                        <div class="row">
												                                                    <div class="col-md-12">
												                                                        <label class="frm-lable-fntsz" for="dfSelfCompname">Name of Employer</label>
												                                                        <input id="dfSelfCompname" name="dfSelfCompname" type="text" 
												                                                         class="form-control input-md checkdb" maxlength="150" >
												                                                    </div>
												                                               </div>
										                                                   </div>
										  
										  												<div class="form-group">
										                                                        <div class="row">
												                                                    <div class="col-md-12">
												                                                        <label class="frm-lable-fntsz" for="dfSelfOccpn">Occupation</label>
												                                                        <input id="dfSelfOccpn" name="dfSelfOccpn" type="text"  
												                                                        class="form-control input-md checkdb" maxlength="60" >
												                                                    </div>
												                                                     
												                                              </div>
										                                                   </div>
																						   
																						  <div class="form-group">
										                                                        <div class="row">
												                                                    
												                                                     <div class="col-md-12">
												                                                        <label class="frm-lable-fntsz" for="name">Estd.Annual Income</label>
												                                                          <div class="input-group ">
																											  <div class="input-group-prepend">
																											    <span class="input-group-text">$</span>
																											  </div>
																											  <input type="text"  name="dfSelfAnnlincome" id="dfSelfAnnlincome"
																											   class="form-control checkdb" aria-label="Amount (to the nearest dollar)" maxlength="30">
																											  <div class="input-group-prepend">
																											    <span class="input-group-text">.00</span>
																											  </div>
																											</div>
												                                                   
												                                                    </div>
												                                              </div>
										                                                   </div>
																						   
																						   <div class="form-group">
										                                                        <div class="row">
												                                                    <div class="col-md-12">
												                                                          <label class="frm-lable-fntsz" for="dfSelfBusinatr">Nature of Business</label>  
												                                                           <select class="form-control checkdb" name="dfSelfBusinatr" id="dfSelfBusinatr"
												                                                           onchange="valiBusNatOthers(this)" maxlength="150">
												                                                             <option value="" selected>--Select--</option>
																											    <%for (String value:Ekycprop.getString("fna.business.list").split("\\^")){%>
																											      <option value="<%=value%>"><%=value%></option>
																											   <%}%>
												                                                           </select>
												                                                    </div>
												                                                     
												                                              </div>
										                                                   </div>
										                                                   
										                                                   
										                                                   <div class="form-group invisible" id="dfSelfBusNatOthSec">
										                                                        <div class="row">
												                                                    <div class="col-md-12">
												                                                        <label class="frm-lable-fntsz" for="dfSelfBusinatrDets">Other Detls.</label>
												                                                        <input id="dfSelfBusinatrDets" name="dfSelfBusinatrDets" type="text"  
												                                                        class="form-control checkMand input-md checkdb" maxlength="150" onchange="validateFld(this)" >
												                                                        <div class="invalid-feedbacks invisible" id="dfSelfBusinatrDetsError">keyin Other Details </div>
												                                                    </div>
												                                                     
												                                              </div>
										                                                   </div>
										                                                   
										                                                 
										                                                   
																						
																						</div>
																						</div>
																				</div>
																				
																				<div class="col-6">
																				<div class="card">
																						<div class="card-header" id="cardHeaderStyle"><span>Source of Fund</span></div>
																						<div class="card-body">
																						
																						<ul class="list-group" id="ulLiStyle-01">
																							  <li class="list-group-item">
																							   <div class="custom-control custom-checkbox">
																								   <input type="checkbox"  class="custom-control-input checkdb chkCDClntFundSrcgrp" name="chkCDClntFundSrc" id="chkCDClntErndIncm"  data="CERN" value="" onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'dfSelfFundsrc');removeErrFld(this)">
																							  <label class="custom-control-label frm-lable-fntsz" for="chkCDClntErndIncm">&nbsp;&nbsp;Earned Income</label>
																							  <input type="hidden" name="htfcderndicn"/> 
																							 </div>
																							  </li>
																							  
																							  <li class="list-group-item">
																							  <div class="custom-control custom-checkbox">
																								    <input type="checkbox"  class="custom-control-input checkdb chkCDClntFundSrcgrp" name="chkCDClntFundSrc" id="chkCDClntInvIncm"  data="CINV"  value="" onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'dfSelfFundsrc');;removeErrFld(this)">
																							  <label class="custom-control-label frm-lable-fntsz" for="chkCDClntInvIncm">&nbsp;&nbsp;Investment</label>
																							   <input type="hidden" name="htfcdinvsticn"/>
																							  </div>
																							 </li>
																							  
																							  <li class="list-group-item">
																							  <div class="custom-control custom-checkbox">
																								   <input type="checkbox" class="custom-control-input checkdb chkCDClntFundSrcgrp" name="chkCDClntFundSrc" id="chkCDClntPrsnlIncm"   data="CPRS"  value=""  onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'dfSelfFundsrc');removeErrFld(this)">
																							  <label class="custom-control-label frm-lable-fntsz" for="chkCDClntPrsnlIncm">&nbsp;&nbsp;Savings</label>
																							  <input type="hidden" name="htfcdpersicn"/>
																							  </div>
																							 </li>
																							  
																							  <li class="list-group-item">
																							  <div class="custom-control custom-checkbox">
																									 <input type="checkbox" class="custom-control-input checkdb chkCDClntFundSrcgrp" name="chkCDClntFundSrc" id="chkCDClntCPFSvngs"  data="CCPF"  value="" onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'dfSelfFundsrc');;removeErrFld(this)">
																								  <label class="custom-control-label frm-lable-fntsz" for="chkCDClntCPFSvngs">&nbsp;&nbsp;CPF Savings</label>
																								  <input type="hidden" name="htfcdcpficn"/>
																							  </div>
																							  </li>
																							  
																							  <li class="list-group-item">
																							  
																							  <div class="custom-control custom-checkbox">
																								     <input type="checkbox" class="custom-control-input checkdb chkCDClntFundSrcgrp"  name="chkCDClntFundSrc" id="chkCDClntSrcIncmOthrs" data="COTH"  value="" onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'dfSelfFundsrc');chkSelfSpsOthrValid(this,'dfSelfFundsrcDets');;removeErrFld(this)">
																							       <label class="custom-control-label frm-lable-fntsz" for="chkCDClntSrcIncmOthrs">&nbsp;&nbsp;Others</label>
													                                              <input type="hidden" name="htfcothicn"/>
													                                              <textarea name="dfSelfFundsrcDets" id="dfSelfFundsrcDets" class="form-control text-wrap txtarea-hrlines checkdb" rows="2" maxlength="50" onkeydown="textCounter(this,50);" onkeyup="textCounter(this,50);"></textarea>
													                                              <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
													                                              <input type="hidden" name="txtFldCDClntSrcIncmOthDet" id="txtFldCDClntSrcIncmOthDet"/>
										  													      <input type="hidden" name="dfSelfFundsrc" id="dfSelfFundsrc" maxlength="150">
																							  </div>
																							  
																							
										  													 </li>
										  												</ul>
																						
																						</div>
																					</div>
																				
																				</div>
										                        </div>
										                    </div>
										                </div>
										                
										                <div class="panel panel-default">
										                    <div class="panel-heading" role="tab" id="headingTwoR">
										                        <h4 class="panel-title">
										                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_right" href="#collapseTwoR" aria-expanded="false" aria-controls="collapseTwoR">
										                                <i class="fa fa-graduation-cap" style="color:#01579b;"></i>&nbsp;Education Details
										                            </a>
										                        </h4>
										                    </div>
										                    <div id="collapseTwoR" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwoR">
										                        <div class="panel-body">
										                           
										                                      <div class="row">
																			  
																			  <div class="col-6">
																			  	<div class="card h-100">
																						<div class="card-header" id="cardHeaderStyle"><span>Highest Education Qualification</span></div>
																						<div class="card-body">
																						<ul class="list-group" id="ulLiStyle-01">
																						
																							  <li class="list-group-item">  
																								  <div class="custom-control custom-radio">
																									    <input type="radio" class="custom-control-input checkdb"  name="dfSelfEdulevelRad" id="chkClntHEQPri" onclick="setEduLevel(this,'dfSelfEdulevel')" value="PRIMARY" >
																									    <label class="custom-control-label frm-lable-fntsz" for="chkClntHEQPri"> Primary</label>
																								  </div> 
																								
																							</li>
																							  
																							  <li class="list-group-item">
																							   <div class="custom-control custom-radio">
																								    <input type="radio" class="custom-control-input checkdb"  name="dfSelfEdulevelRad" id="chkClntHEQSec" onclick="setEduLevel(this,'dfSelfEdulevel')" value="SECONDARY">
																								    <label class="custom-control-label frm-lable-fntsz" for="chkClntHEQSec"> Secondary</label>
																							  </div> 
																							  
																							  </li>
																							  
																							  <li class="list-group-item"> 
																							  
																							   <div class="custom-control custom-radio">
																								    <input type="radio" class="custom-control-input checkdb" name="dfSelfEdulevelRad" id="chkClntHEQPreTer" onclick="setEduLevel(this,'dfSelfEdulevel')" value="PRETER">
																								    <label class="custom-control-label frm-lable-fntsz" for="chkClntHEQPreTer"> Pre-Tertiary</label>
																							  </div> 
																							  
																								
																							  </li>
																							  
																							  <li class="list-group-item"> 
																							  
																							  <div class="custom-control custom-radio">
																								    <input type="radio" class="custom-control-input checkdb" name="dfSelfEdulevelRad" id="chkClntHEQGce" onclick="setEduLevel(this,'dfSelfEdulevel')" value="EQGCE">
																								    <label class="custom-control-label frm-lable-fntsz" for="chkClntHEQGce">  GCE 'N' or 'O Level or Equivalent Academic Qualification</label>
																							  </div> 
																							  <input type="hidden" name="dfSelfEdulevel" id="dfSelfEdulevel" maxlength="60">
																							</li>
																							
																							<li class="list-group-item"> 
																							  
																							    
																							   <div class="custom-control custom-radio">
																								    <input type="radio" class="custom-control-input checkdb" name="dfSelfEdulevelRad" id="chkClntHEQTerAbv" onclick="setEduLevel(this,'dfSelfEdulevel')" value="EQTERABV">
																								    <label class="custom-control-label frm-lable-fntsz" for="chkClntHEQTerAbv"> Tertiary and above</label>
																							  </div> 
																							  
																								
																							  </li>
																							
																							
																						</ul>	
																					</div>
																				
																				</div>
																			  </div>
																			  <div class="col-6">
																			  <div class="card">
																						<div class="card-header" id="cardHeaderStyle"><span>Language Proficiency</span></div>
																						<div class="card-body">
																						<span id="span_dfSelfEngSpoken"></span>
																						
																						
																						<ul class="list-group" id="ulLiStyle-01">
																							  <li class="list-group-item"> <div class="frm-lable-fntsz bold" style="font-weight: 700;">Spoken English</div>
																								  
																								  <div class="custom-control custom-radio">
																									    <input type="radio" class="custom-control-input checkdb" id="dfSelfEngSpokenY" name="dfSelfEngSpoken" value="Y" maxlength="1">
																									    <label class="custom-control-label frm-lable-fntsz" for="dfSelfEngSpokenY" style="text-align:left;"> Proficient </label>
																								  </div> 
																								  
																								   <div class="custom-control custom-radio">
																									    <input type="radio" class="custom-control-input checkdb" id="dfSelfEngSpokenN" name="dfSelfEngSpoken" value="N" maxlength="1">
																									    <label class="custom-control-label frm-lable-fntsz" for="dfSelfEngSpokenN" style="text-align:left;"> <span class="txt-urline" style="font-weight: 500;"> Not</span> Proficient </label>
																								  </div>
																							
																							</li>
																							
																							
																							  
																							  																							
																							
																						</ul>
																					 
																						
																						</div>
																						
																						<div class="card-body">
																						
																						<span id="span_dfSelfEngWritten"></span>
																						<ul class="list-group" id="ulLiStyle-01">
																						<li class="list-group-item">
																							  <div class="frm-lable-fntsz bold" style="font-weight: 700;">Written English</div>
																								  <div class="custom-control custom-radio">
																									    <input type="radio" class="custom-control-input checkdb" id="dfSelfEngWrittenY" name="dfSelfEngWritten" value="Y" maxlength="1">
																									    <label class="custom-control-label frm-lable-fntsz" for="dfSelfEngWrittenY" style="text-align:left;"> Proficient  </label>
																								  </div> 
																								  
																								 <div class="custom-control custom-radio">
																									    <input type="radio" class="custom-control-input checkdb" id="dfSelfEngWrittenN" name="dfSelfEngWritten" value="N" maxlength="1">
																									    <label class="custom-control-label frm-lable-fntsz" for="dfSelfEngWrittenN" style="text-align:left;"><span class="txt-urline" style="font-weight: 500;"> Not</span> Proficient </label>
																								  </div> 
																									
																									
																							</li>
																						
																						</ul>
																						</div>
																					
																					</div>
																					
																					
										                                      </div>
																			  </div>
																			  
																			  
															 </div>
										                    </div>
										                </div>
										                
										               
										                   <div class="panel panel-default">
										                    <div class="panel-heading" role="tab" id="headingIntrPreTrust">
										                        <h4 class="panel-title">
										                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_right" href="#headingInterpreterDetls" aria-expanded="false" aria-controls="headingInterpreterDetls">
										                                <i class="fa fa-user" style="color:#388e3c;"></i>&nbsp;Interpreter / Trusted Individual Details
										                            </a>
										                        </h4>
										                    </div>
										                    <div id="headingInterpreterDetls" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingIntrPreTrust">
										                        <div class="panel-body">
										                           
										                                      <div class="row">
																					
																				  <div class="col-6">
																			   
																				
																				<div class="card ">
																				     <div class="card-header" id="cardHeaderStyle">
																				        <div class="row">
																				          <div class="col-md-9">
																					          <!-- <div class="custom-control custom-switch ">
																								  <input type="checkbox" class="custom-control-input checkdb " id="btnInptrSwtch" onclick="enableIntrprt(this)">
																								  <label class="custom-control-label pt-1" for="btnInptrSwtch">&nbsp;Any Interpreter</label>
																								  <input type="checkbox" class="form-controls" onclick="fnaAssgnFlg(this,'chk');enableIntrprt(this)" id="cdIntrprtflg" name="cdIntrprtflg">&nbsp;Any Interpreter
																								  
																								</div>  -->
																								
																								<div class="custom-control custom-checkbox">
																								    <input type="checkbox" class="custom-control-input checkdb " onclick="fnaAssgnFlg(this,'chk');enableIntrprt(this)" id="cdIntrprtflg" name="cdIntrprtflg" value="N">
																								    <label class="custom-control-label pt-1" for="cdIntrprtflg">Any Interpreter</label>
																								</div>
																								
																								
																							</div>
																				          <div class="col-md-3"><span class="badge badge-pill badge-warning mt-1" style="margin: 0px 0px 0px -3em;">Note :&nbsp;<i class="fa fa-commenting" aria-hidden="true" data-toggle="popoverPersDetls" data-trigger="hover" data-original-title="" title="" id="inptrNote"></i></span></div>
																				        </div>
																				     </div>
																				     
																				     <div class="card-body" id="tempIntprtDiv">
																				    
																				     
																				     <div class="row pl-1 ">
												                                                      <div class="col-1"><img src="vendor/avallis/img/infor.png" class="mt-2"></div>
												                                                    <div class="col-10">
													                                                       <small class="font-italic bold text-primaryy font-sz-55prcnt"> (If Any Interpreter / Trusted Individual presented then key-in the following details)</small>
												                                                     </div>
												                                                </div>
																					    
																						<div class="">
																						    <form action="">
																						  <div class="form-group">
																						    <label class="frm-lable-fntsz" for="intprtName" style="margin-bottom: 0px;">Name</label>
																						    <input type="text" class="form-control checkdb checkMand" name="intprtName" id="intprtName"
																						     disabled="true" maxlength="60" onchange="validateFld(this)">
																						     <div class="invalid-feedbacks hide" id="intprtNameError">Keyin Name</div>
																						  </div>
																						  <div class="form-group">
																						    <label class="frm-lable-fntsz" for="intprtContact" style="margin-bottom: 0px;">Contact Number</label>
																						    <input type="text" class="form-control checkdb checkMand" name="intprtContact" id="intprtContact" 
																						    disabled="true" maxlength="20" onchange="validateFld(this)">
																						    <div class="invalid-feedbacks hide" id="intprtContactError">Keyin Contact Number</div>
																						  </div>
																						  
																						  <div class="form-group">
																						    <label class="frm-lable-fntsz" for="" style="margin-bottom: 0px;">Relationship</label>
																						    <select class="form-control checkdb checkMand" id="intprtRelat" name="intprtRelat" 
																						    disabled="true" maxlength="60" onchange="validateFld(this)">
																						    <option value="" selected="selected">--Select--</option>
																						    <% for(String relationship:Ekycprop.getString("app.relationship").split("\\^")){ %>
																								 <option value="<%=relationship%>"><%=relationship%></option>
																							<%}%>
																						    </select>
																						    <div class="invalid-feedbacks hide" id="intprtRelatError">Select the Relationship</div>
																						  </div>
																						  </form>
																						</div>				
																										
																										
																				     </div>
																				     
																				
																				</div>
																			</div> 	
																					   
																				<div class="col-6">
																			  <div class="card ">
																						<div class="card-header" id="cardHeaderStyle"><span>Language Use</span></div>
																						<div class="card-body">
																							<ul class="list-group selfInptrLangSec" id="ulLiStyle-01" >
																							  <li class="list-group-item">  
																									  <div class="custom-control custom-checkbox">
																									  <input class="custom-control-input kycLanguage checkdb" type="checkbox" name="htflaneng" id="htflaneng" value="Y" data="ENG" onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'cdLanguages');">
																									  <label class="custom-control-label frm-lable-fntsz" for="htflaneng"> English </label>
																									</div>
																							  </li>
																							  
																							  <li class="list-group-item">
																									    <div class="custom-control custom-checkbox">
																									  <input class="custom-control-input kycLanguage checkdb" type="checkbox" name="htflanman" id="htflanman" value="Y" data="MAN" onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'cdLanguages');">
																									  <label class="custom-control-label frm-lable-fntsz" for="htflanman">Mandarin </label>
																									</div>
																							  </li>
																							  
																							  <li class="list-group-item">
																									  <div class="custom-control custom-checkbox">
																									  <input class="custom-control-input kycLanguage checkdb" type="checkbox" name="htflanmalay" id="htflanmalay" value="Y" data="MAL" onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'cdLanguages');">
																									  <label class="custom-control-label frm-lable-fntsz" for="htflanmalay">Malay </label>
																									</div>
																							  </li>
																							  
																							  <li class="list-group-item">
																									    <div class="custom-control custom-checkbox">
																									  <input class="custom-control-input kycLanguage checkdb" type="checkbox" name="htflantamil" id="htflantamil" value="Y" data="TAM" onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'cdLanguages');">
																									  <label class="custom-control-label frm-lable-fntsz" for="htflantamil">Tamil </label>
																									</div>
																							  </li>
																							  
																							  <li class="list-group-item">
																									    <div class="custom-control custom-checkbox">
																									  <input class="custom-control-input kycLanguage checkdb" type="checkbox" name="htflanoth" id="htflanoth"  value="Y" data="OTH" onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'cdLanguages');chkSelfSpsOthrValid(this,'cdLanguageOth');" >
																									  <label class="custom-control-label frm-lable-fntsz" for="htflanoth">Others </label>
																									  <textarea id="cdLanguageOth"  name="cdLanguageOth" disabled class="form-control checkdb text-wrap txtarea-hrlines" rows="2" maxlength="150" 
																									  onkeydown="textCounter(this,150);" onkeyup="textCounter(this,150);" ></textarea>
																									  <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
																									   <input type="hidden" name="cdLanguages" id="cdLanguages" maxlength="60" class="checkdb"/>
																									</div>
																							  </li>
																							  
																							  </ul>
																							  <div class="invalid-feedbacks hide" id="selfInptrLangSecError">Atleast Select any one of the Language in Interpreter Language Section</div>
																						</div>
																					
																					</div>
																					
																					
										                                      </div>	
										                                      
										                                         
										                
										                
													
										        </div>
												  <br>
												<div class="row">
													<div class="col-12">
													
													<div class="card">
																			 <div class="card-body">
																					 <div class="list-group" id="listgroupbg-01">
																						  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
																						    <div class="d-flex w-100 ">
																						     
																						     
																						     <div class="custom-control custom-checkbox">
																							    <input type="checkbox" class="custom-control-input checkdb " name="cdBenfownflg" id="cdBenfownflg"
																							      onclick="openOthInptrSec(this,'BENOWNER')" maxlength="1" value="N">
																							    <label class="custom-control-label bold font-sz-level6 pt-1" for="cdBenfownflg">Benefical Owner</label>
																							    <small class="pl-4"><button type="button" class="btn btn-link p-0 font-sz-level6" data-toggle="modal" data-target="#txtAreaModal1">
																						         <i class="fa fa-external-link" aria-hidden="true"></i>&nbsp;View</button></small>
																							  </div>
																						     
																						     
																						     
																						      
																						    
																						    </div>
																						    <p class="mb-1 italic font-sz-level7">Is someone else expected to participate in, make decisions about, or benefit from this policy in any way?
																						(This does not include Account holder/Policyholder, Proposed Life Insured, Payer, Beneficiary or signing officer.)</p>
																						    
																						  </a>
																						  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
																						    <div class="d-flex w-100 ">
																						       <div class="custom-control custom-checkbox">
																							    <input type="checkbox" class="custom-control-input checkdb " name="cdTppflg" id="cdTppflg" 
																							     onclick="openOthInptrSec(this,'TPP')" maxlength="1" value="N">
																							    <label class="custom-control-label bold font-sz-level6 pt-1" for="cdTppflg">Third Party Payer</label>
																							    <small class="pl-3"><button type="button" class="btn btn-link p-0 font-sz-level6" data-toggle="modal" data-target="#txtAreaModal2">
																						         <i class="fa fa-external-link" aria-hidden="true"></i>&nbsp;View</button></small>
																							  </div> 
																						         
																						   </div>
																						    <p class="mb-1 italic font-sz-level7">Is anyone other than the Account holder/Policyholder be paying for this policy / Investment account?</p>
																						  </a>
																						  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
																						    <div class="d-flex w-100">
																						     
																						      <div class="custom-control custom-checkbox">
																							    <input type="checkbox" class="custom-control-input checkdb " name="cdPepflg" id="cdPepflg" 
																							     onclick="openOthInptrSec(this,'PEP')" maxlength="1" value="N">
																							    <label class="custom-control-label bold font-sz-level6 pt-1" for="cdPepflg">PEP and RCA</label>
																							    <small class="pl-5"><button type="button" class="btn btn-link p-0 font-sz-level6" data-toggle="modal" data-target="#txtAreaModal3">
																						         <i class="fa fa-external-link" aria-hidden="true"></i>&nbsp;View</button></small>
																							  </div>
																						     </div>
																						    <p class="mb-1 italic font-sz-level7">Are you or any close relative(s) currently or previously held a senior position in a government, political party, military, tribunal or government-owned corporation?.</p>
																						  </a>
																						</div>
																					 </div>
																			</div>
																		</div>
																	
																	</div>
																	
																	 
															    </div>
																
																
															</div>
										                   
										                </div>
										            </div>
										        </div>
										    </div>
										</div>
                                 </div>
                                  
                              </div>
                           </div>
		    
		</div>
	    </div>
        
		  
           <div class="modal fade" id="new2ModalScrollable" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="exampleModalLabelClient2" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header" id="cardHeaderStyle-22">
                  <h6 class="modal-title" id="exampleModalLabelLogout"><i class="fa  fa-user-plus"></i>&nbsp;Add New Client Details</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:#fff;">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="card">
                    
                      <div class="card-body" style="height:350px;">
                      <div class="row">
                      
                           <div class="col-12">
                                <div class="custom-control custom-radio">
								    <input type="radio" class="custom-control-input checkdb " id="SrchInsured" name="AddSrchInsured" value="SrchInsured" checked="checked">
								    <label class="custom-control-label" for="SrchInsured"><i class="fa  fa-search" style="color: #1799b7;"></i>&nbsp;Search Existing Client Details</label>
								 </div>
                           
                           </div>
                          
                           <div class="col-12 mt-3">
                                 <form action="">
									  <div class="form-group ml-5">
									    <label class="frm-lable-fntsz" for="" style="margin-bottom: 0px;">Search  Client Name:</label>
									     <select class="form-control checkdb" id="selFldNRICName" style="width:350px" aria-hidden="true">
									     	<option value="" selected>Select Client</option>
											<!-- <optgroup id="addMewGrp" label="Add New Insured Details">
												<option>Add New</option>
											  </optgroup> -->
											  
											  <optgroup id="searchExistGrp" label="Search Client Details">
												</optgroup>
			                             </select>
			                             
			                             <div class="invalid-feedbacks hide" id="selFldNRICNameError">
			                                  Select Client Name / Client Name cannot be empty 
			                             </div>
			                             
			                            
									  </div>
						        </form>
                           </div>
                           
                           <div class="col-12 mt-4">
                                <div class="custom-control custom-radio">
								    <input type="radio" class="custom-control-input checkdb " id="AddInsured" name="AddSrchInsured" value="AddInsured" onclick="addNewInsuredClnt(this)">
								    <label class="custom-control-label" for="AddInsured"><i class="fa  fa-user-plus" style="color: #0aa00ad4;"></i>&nbsp;Add New Client Details</label>
								 </div>
                           
                           </div>
                           
                           
                      </div>
                        
                      </div>
                      
                    
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-sm btn-bs3-prime" data-dismiss="modal">Cancel</button>
                  <button type="button" class="btn btn-sm btn-bs3-prime" id="addNewClient2Btn">OK</button>
                </div>
              </div>
            </div>
          </div>

          <div class="modal fade" id="genKYCModalScrollable" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="genKYCModalScrollableTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header" id="cardHeaderStyle-22">
                  <h6 class="modal-title" id="genKYCModalScrollableTitle">Loading...</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:#fff;">
                    <span aria-hidden="true">�</span>
                  </button>
                </div>
                <div class="modal-body">
                  
                  <div class="col-lg-12">
              
               <div class="loader">
					  <p></p>
					</div>
              
            </div>
                  
                  
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-sm btn-bs3-prime" data-dismiss="modal">OK</button>
                  
                </div>
              </div>
            </div>
          </div>
		  
		  <div class="modal fade" id="txtAreaModal1" data-backdrop="static" data-keyboard="false" >
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header" id="cardHeaderStyle-22">
        <h6 class="modal-title"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;View Beneficiary.Details</h6>
        <button type="button" class="close inptrModalClass" data-dismiss="modal" style="color:#fff;">�</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
         <div class="row">
             <div class="col-md-12">
                  <div class="form-group">
					  <label class="frm-lable-fntsz">Name<sup style="color: red;"><strong>*</strong></sup></label>
					     <input type="text" class="form-control checkdb mandatory checkMand" name="txtFldCDBenfName" 
					     id="txtFldCDBenfName" maxlength="60" onchange="validateFld(this)">
					     <input type="hidden" name="txtFldCDBenfId" id="txtFldCDBenfId">
					     <div class="invalid-feedbacks hide" id="txtFldCDBenfNameError">Keyin Name</div>
					  </div>
             
                  </div>
           </div>
        
        <div class="row">
             <div class="col-md-6">
                 <div class="form-group">
					  <label class="frm-lable-fntsz">NRIC / Passport No.<sup style="color: red;"><strong>*</strong></sup></label>
					    <input type="text" class="form-control checkdb mandatory checkMand"  name="txtFldCDBenfNric"
					     id="txtFldCDBenfNric" maxlength="20">
					     <div class="invalid-feedbacks hide" id="txtFldCDBenfNricError">Keyin NRIC / Passport No. </div>
                  </div>
             </div>
             
              <div class="col-md-6">
                    <div class="form-group">
					  <label class="frm-lable-fntsz">Entity Incorporation No.<sup style="color: red;"><strong>*</strong></sup></label>
					   <input type="text" class="form-control checkdb mandatory checkMand"  name="txtFldCDBenfIncNo"
					    id="txtFldCDBenfIncNo" maxlength="20">
					    <div class="invalid-feedbacks hide" id="txtFldCDBenfIncNoError">Keyin Entity Incorporation No.</div>
                  </div>
             </div>
        
        </div>
        
        <div class="row">
             <div class="col-md-12">
                 <div class="form-group">
                     <label class="frm-lable-fntsz">Registered Residential / Cooperation Address<sup style="color: red;"><strong>*</strong></sup></label>
                     <textarea class="form-control checkdb txtarea-hrlines text-wrap checkMand mandatory" rows="4" name="txtFldCDBenfAddr" id="txtFldCDBenfAddr" 
                     maxlength="450" onkeydown="textCounter(this,450);" onkeyup="textCounter(this,450);"></textarea>
                     <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
                     <div class="invalid-feedbacks hide" id="txtFldCDBenfAddrError">Keyin Registered Residential / Cooperation Address</div>
                 </div>
             </div>
        
        </div>
        
        <div class="row">
            <div class="col-md-12">
             
             <div class="form-group">
                  <label class="frm-lable-fntsz">Job / Political / Military Title of Position Held<sup style="color: red;"><strong>*</strong></sup></label><br>
                <div class="row">
                     <div class="col-md-4">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input  name="radFldCDBenfJob"  id="IntrBenJob" type="radio" value="JOB" maxlength="30"  class="checkdb mandatory">
													    <label for="IntrBenJob" id="lblTxtSz-01"><span><img src="vendor/avallis/img/job.png">&nbsp;Job</span></label>
							                         </div> 	
							 </div>
						</div>	
						
						 <div class="col-md-4">
						 
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input  name="radFldCDBenfJob"  id="IntrBenPol" type="radio" value="POLITICAL" maxlength="30"  class="checkdb mandatory">
													    <label for="IntrBenPol" id="lblTxtSz-01"><span><img src="vendor/avallis/img/political.png">&nbsp;Political</span></label>
							                         </div> 	
					      </div>
					      
						</div>	
						
						 <div class="col-md-4">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input name="radFldCDBenfJob"  id="IntrBenMil"  type="radio" value="MILITARY" maxlength="30"  class="checkdb mandatory">
													    <label for="IntrBenMil" id="lblTxtSz-01"><span><img src="vendor/avallis/img/soldier.png">&nbsp;Military</span></label>
							                         </div> 	
							 </div>
						</div>	
						
						<div class="invalid-feedbacks hide ml-3" id="radFldCDBenfJobError">Select the anyone option in  Job / Political / Military Title of Position Held</div>						  
			 </div>
        </div>
             
             </div>
        </div>
        
        <div class="row">
             <div class="col-md-12">
             <div class="form-group">
   <label class="frm-lable-fntsz">In what country is / was the position held<sup style="color: red;"><strong>*</strong></sup></label>
     <select class="custom-select checkdb checkMand mandatory" id="txtFldCDBenfJobCont" name="txtFldCDBenfJobCont" maxlength="30">
    <option selected value="">--Select--</option>
       <% for(String country:Ekycprop.getString("app.country").split("\\^")){ %>
	<option value="<%=country%>"><%=country%></option>
	 <%}%>
  </select>
  <div class="invalid-feedbacks hide" id="txtFldCDBenfJobContError">Select the Country</div>
  </div>
             </div>
        
        </div>
        
        
   <div class="row">
       <div class="col-md-12">
             <div class="form-group">
           <label class="frm-lable-fntsz">Relationship to Client (1)/(2)<sup style="color: red;"><strong>*</strong></sup>
            </label>
      <select class="custom-select checkdb checkMand mandatory" id="txtFldCDBenfRel" name="txtFldCDBenfRel" maxlength="60" onchange="validateFld(this)">
    <option selected value="">--Select--</option>
    <% for(String relationship:Ekycprop.getString("app.relationship").split("\\^")){ %>
		<option value="<%=relationship%>"><%=relationship%></option>
		<%}%>
  </select>
  <div class="invalid-feedbacks hide" id="txtFldCDBenfRelError">Select the Relationship to Client (1)/(2)</div>
  </div>
       </div>
  </div>
  
  
  <!--input hdnFld Configuration  -->
  
  <input type="hidden" name="othperId" id="othperId">
  
  <!--input hdnFld Configuration  -->
  
  
  
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
	  
	  <button type="button" class="btn btn-sm btn-bs3-prime mr-auto inptrModalClass" id="btnBenTpp" data-dismiss="modal" data-toggle="modal" data-target="#txtAreaModal2">TPP Dets</button>
	  
	  <button type="button" class="btn btn-sm btn-bs3-prime mr-auto inptrModalClass" id="btnBenPep" data-dismiss="modal" data-toggle="modal" data-target="#txtAreaModal3">PEP Dets </button>
	   <button type="button" class="btn btn-sm btn-bs3-prime inptrModalClass" data-dismiss="modal">Ok</button>
        
       
      </div>

    </div>
  </div>
</div>
          
		  
		  <div class="modal fade" id="txtAreaModal2" data-backdrop="static" data-keyboard="false" >
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content w-550">

      <!-- Modal Header -->
      <div class="modal-header" id="cardHeaderStyle-22">
        <h6 class="modal-title"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;View TPP.Details</h6>
        <button type="button" class="close inptrModalClass" data-dismiss="modal" style="color:#fff;">�</button>
      </div>

      <!-- Modal body -->
       <div class="modal-body">
        
        
        <div class="row">
             <div class="col-md-12">
	              <div class="form-group">
	             <label class="frm-lable-fntsz">Name<sup style="color: red;"><strong>*</strong></sup></label>
	             <input type="text" class="form-control checkdb mandatory checkMand" name="txtFldCDTppName"
	              id="txtFldCDTppName" maxlength="60" onchange="validateFld(this)">
                  <input type="hidden" name="txtFldCDTppId" id="txtFldCDTppId">
                  <div class="invalid-feedbacks hide" id="txtFldCDTppNameError">Keyin Name</div>
              </div>
             
             </div>
          
        </div>
        
        <div class="row">
             <div class="col-md-6">
                 <div class="form-group">
					  <label class="frm-lable-fntsz">NRIC / Passport No.<sup style="color: red;"><strong>*</strong></sup></label>
					     <input type="text" class="form-control checkdb mandatory checkMand" name="txtFldCDTppNric" 
					     id="txtFldCDTppNric" maxlength="20"  onchange="validateFld(this)">
					     <div class="invalid-feedbacks hide" id="txtFldCDTppNricError">Keyin NRIC / Passport No.</div>
                  </div>
             </div>
             
              <div class="col-md-6">
                    <div class="form-group">
					  <label class="frm-lable-fntsz">Entity Incorporation No.<sup style="color: red;"><strong>*</strong></sup></label>
					     <input type="text" class="form-control checkdb checkMand mandatory" name="txtFldCDTppIncNo"
					      id="txtFldCDTppIncNo" maxlength="20" onchange="validateFld(this)">
					      <div class="invalid-feedbacks hide" id="txtFldCDTppIncNoError">Keyin Entity Incorporation No.</div>
                  </div>
             </div>
        
        </div>
        
        <div class="row">
             <div class="col-md-12">
                 <div class="form-group">
                     <label class="frm-lable-fntsz">Registered Residential / Cooperation Address<sup style="color: red;"><strong>*</strong></sup></label>
                     <textarea class="form-control checkdb txtarea-hrlines text-wrap checkMand mandatory" rows="4" name="txtFldCDTppAddr" id="txtFldCDTppAddr"
                     maxlength="450" onkeydown="textCounter(this,450);" onkeyup="textCounter(this,450);" onchange="validateFld(this)"></textarea>
                     <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
                     <div class="invalid-feedbacks hide" id="txtFldCDTppAddrError">Keyin  Registered Residential / Cooperation Address</div>
                 </div>
             </div>
        
        </div>
        
        <div class="row">
            <div class="col-md-12">
             
             <div class="form-group">
                  <label class="frm-lable-fntsz">Job / Political / Military Title of Position Held<sup style="color: red;"><strong>*</strong></sup></label><br>
                <div class="row">
                     <div class="col-md-4">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input name="radFldCDTppJob"  id="IntrTppJob" type="radio" value="JOB" maxlength="30"  class="checkdb mandatory"  onchange="radioAndChkboxFld(this)">
													    <label for="IntrTppJob" id="lblTxtSz-01"><span><img src="vendor/avallis/img/job.png">&nbsp;Job</span></label>
							                         </div> 	
							 </div>
						</div>	
						
						 <div class="col-md-4">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input name="radFldCDTppJob"  id="IntrTppPol" type="radio"  value="POLITICAL"  maxlength="30"  class="checkdb mandatory" onchange="radioAndChkboxFld(this)" > 
													    <label for="IntrTppPol" id="lblTxtSz-01"><span><img src="vendor/avallis/img/political.png">&nbsp;Political</span></label>
							                         </div> 	
							 </div>
						</div>	
						
						 <div class="col-md-4">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input name="radFldCDTppJob"  id="IntrTppMil" type="radio"  value="MILITARY"  maxlength="30"  class="checkdb mandatory" onchange="radioAndChkboxFld(this)">
													    <label for="IntrTppMil" id="lblTxtSz-01"><span><img src="vendor/avallis/img/soldier.png">&nbsp;Military</span></label>
							                         </div> 	
							 </div>
						</div>	
						
						<div class="invalid-feedbacks hide ml-3" id="radFldCDTppJobError">Select the anyone of the option in Job / Political / Military Title of Position Held </div>						  
				 </div>
                
              </div>
             
             </div>
             
            
        
        </div>
        
        <div class="row">
             <div class="col-md-7">
             <div class="form-group">
   <label class="frm-lable-fntsz text-wrap  checkMand mandatory" style="text-align:left;">In what country is / was the position held<sup style="color: red;"><strong>*</strong></sup></label>
     <select class="custom-select checkdb" id="txtFldCDTppJobCont" name="txtFldCDTppJobCont"  maxlength="30"  onchange="validateFld(this)">
    <option selected value="">--Select--</option>
   <% for(String country:Ekycprop.getString("app.country").split("\\^")){ %>
	<option value="<%=country%>"><%=country%></option>
	 <%}%>
  </select>
  <div class="invalid-feedbacks hide" id="txtFldCDTppJobContError">Select the Country</div>
  </div>
             </div>
             
             <div class="col-md-5">
             <div class="form-group">
  <label class="frm-lable-fntsz text-wrap" style="text-align:left;">Relationship to Client (1)/(2)<sup style="color: red;"><strong>*</strong></sup>
</label>
      <select class="custom-select checkdb checkMand mandatory" id="txtFldCDTppRel" name="txtFldCDTppRel"  maxlength="60" onchange="validateFld(this)">
    <option selected value="">--Select--</option>
    <% for(String relationship:Ekycprop.getString("app.relationship").split("\\^")){ %>
		<option value="<%=relationship%>"><%=relationship%></option>
		<%}%>
  </select>
  <div class="invalid-feedbacks hide" id="txtFldCDTppRelError">Select the Relationship to Client (1)/(2)</div>
  </div>
       </div>
        
        </div>
   <div class="row">
        <div class="col-md-12">
             <div class="form-group">
  <label class="frm-lable-fntsz mandLblTxtBenTPP">Payment Mode<sup style="color: red;"><strong>*</strong></sup></label>
      <div class="row">
                     <div class="col-md-7">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input id="radPayChq" name="radCDTppPayMode" type="radio" value="CHQ" maxlength="20"  class="checkdb mandatory" onchange="radioAndChkboxFld(this)" >
													    <label for="radPayChq" id="lblTxtSz-01"><span><img src="vendor/avallis/img/cheque.png">&nbsp;Cheque/Cashier Order</span></label>
							                         </div> 	
							 </div>
						</div>	
						
						 <div class="col-md-5">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input id="radPayCrdCard" name="radCDTppPayMode" type="radio" value="CRDTCARD" maxlength="20"  class="checkdb mandatory" onchange="radioAndChkboxFld(this)" >
													    <label for="radPayCrdCard" id="lblTxtSz-01"><span><img src="vendor/avallis/img/card.png">&nbsp;Credit Card</span></label>
							                         </div> 	
							 </div>
													</div>
													
							 <div class="invalid-feedbacks hide ml-3" id="radCDTppPayModeError">Select anyone of the Payment Mode</div>
													
									</div>	
							  </div>
							       </div>
							       </div>
       
                                                  <div class="row">
                                                  
                                                           <div class="col-7">
			                                                     <div class="col-">
			                                                       <img src="vendor/avallis/img/infor.png" class="mt-2">&nbsp;<small class="font-italic bold text-primaryy font-sz-level8">Please attach a copy of the Third Party Payer's NRIC/Passport/ACRA (For Company).</small>
			                                                     </div>
			                                                     <div class="col-">
                                                                 <div class="custom-file">
																	    <input type="file" class="custom-file-input checkdb mandatory" id="filecdTppNricCopy" name="filecdTppNricCopy" disabled>
																	    <label class="custom-file-label frm-lable-fntsz pt-1"  style="font-weight: normal;" for="filecdTppNricCopy">Choose file</label>
  																</div>
                                                                 </div>
		                                                     </div>
		                                                     
		                                                      <div class="col-5">
				                                                   <div class="form-group">
																		<label class="frm-lable-fntsz mandLblTxtBenTPP mt-3">Contact<sup style="color: red;"><strong>*</strong></sup></label>
																		<input type="text" class="form-control checkdb checkMand mandatory" name="txtFldCDTppCcontact" id="txtFldCDTppCcontact" maxlength="20" onchange="validateFld(this)">
																		<input type="text" class="form-control checkdb hide" name="txtFldCDTppBankName" id="txtFldCDTppBankName" maxlength="60" >
																		 <input type="text" class="form-control checkdb hide" name="txtFldCDTppChqNo" id="txtFldCDTppChqNo" maxlength="20">
																		  <div class="invalid-feedbacks hide" id="txtFldCDTppCcontactError">Keyin Contact</div>
																	</div>
		                                                      </div>
		                                                   
		                                                </div>
			                                                
      </div>
	  
	  

      <!-- Modal footer -->
        <div class="modal-footer">
	  
	  <button type="button" class="btn btn-sm btn-bs3-prime mr-auto inptrModalClass" id="btnTppBen" data-dismiss="modal" data-toggle="modal" data-target="#txtAreaModal1">BENF Dets</button>
	  
	  <button type="button" class="btn btn-sm btn-bs3-prime mr-auto inptrModalClass" id="btnTppPep" data-dismiss="modal" data-toggle="modal" data-target="#txtAreaModal3">PEP Dets </button>
	   
	  
         <button type="button" class="btn btn-sm btn-bs3-prime inptrModalClass" data-dismiss="modal">Ok</button>
        
       
      </div>

    </div>
  </div>
</div>



<div class="modal fade" id="txtAreaModal3" data-backdrop="static" data-keyboard="false" >
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content w-550">

      <!-- Modal Header -->
      <div class="modal-header" id="cardHeaderStyle-22">
        <h6 class="modal-title"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;View PEP/RCA.Details</h6>
        <button type="button" class="close" data-dismiss="modal" style="color:#fff;">�</button>
      </div>
      
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
             <div class="col-md-12">
                  <div class="form-group">
				  <label class="badge">Name<sup style="color: red;"><strong>*</strong></sup></label>
				     <input type="text" class="form-control checkdb mandatory checkMand" name="txtFldCDPepName"
				      id="txtFldCDPepName" maxlength="60" onchange="validateFld(this)">
				     <input type="hidden" name="txtFldCDPepId" id="txtFldCDPepId">
				      <div class="invalid-feedbacks hide" id="txtFldCDPepNameError">Keyin Name</div>
				  </div>
				             
             </div>
          
        </div>
        
        <div class="row">
             <div class="col-md-6">
                 <div class="form-group">
					  <label class="frm-lable-fntsz">NRIC / Passport No.<sup style="color: red;"><strong>*</strong></sup></label>
					  <input type="text" class="form-control checkdb mandatory" name="txtFldCDPepNric"
					   id="txtFldCDPepNric" maxlength="20" onchange="validateFld(this)">
					    <div class="invalid-feedbacks hide" id="txtFldCDPepNricError">Keyin NRIC / Passport No.</div>
                  </div>
             </div>
             
              <div class="col-md-6">
                    <div class="form-group">
					  <label class="frm-lable-fntsz">Entity Incorporation No.<sup style="color: red;"><strong>*</strong></sup></label>
					    <input type="text" class="form-control checkdb checkMand mandatory" name="txtFldCDPepIncNo"
					     id="txtFldCDPepIncNo" maxlength="20"onchange="validateFld(this)">
					      <div class="invalid-feedbacks hide" id="txtFldCDPepIncNoError">Keyin Entity Incorporation No.</div>
                  </div>
             </div>
        
        </div>
        
        <div class="row">
             <div class="col-md-12">
                 <div class="form-group">
                     <label class="frm-lable-fntsz">Registered Residential / Cooperation Address<sup style="color: red;"><strong>*</strong></sup></label>
                     <textarea class="form-control checkdb txtarea-hrlines text-wrap checkMand mandatory" rows="4" name="txtFldCDPepAddr" id="txtFldCDPepAddr"
                     maxlength="450" onkeydown="textCounter(this,450);" onkeyup="textCounter(this,450);"  onchange="validateFld(this)"></textarea>
                     <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
                      <div class="invalid-feedbacks hide" id="txtFldCDPepAddrError">Select the Registered Residential / Cooperation Address</div>
                                    </div>
             </div>
        
        </div>
        
        <div class="row">
            <div class="col-md-12">
             
             <div class="form-group">
                  <label class="frm-lable-fntsz">Job / Political / Military Title of Position Held<sup style="color: red;"><strong>*</strong></sup></label><br>
                <div class="row">
                     <div class="col-md-4">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input id="IntrPepJob" name="radFldCDPepJob" type="radio" value="JOB" maxlength="30"  class="checkdb mandatory" onchange="radioAndChkboxFld(this)">
													    <label for="IntrPepJob" id="lblTxtSz-01"><span><img src="vendor/avallis/img/job.png">&nbsp;Job</span></label>
							                         </div> 	
							 </div>
						</div>	
						
						 <div class="col-md-4">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input id="IntrPepPol" name="radFldCDPepJob" type="radio" value="POLITICAL" maxlength="30"  class="checkdb mandatory " onchange="radioAndChkboxFld(this)">
													    <label for="IntrPepPol" id="lblTxtSz-01"><span><img src="vendor/avallis/img/political.png">&nbsp;Political</span></label>
							                         </div> 	
							 </div>
						</div>	
						
						 <div class="col-md-4">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input id="IntrPepMil" name="radFldCDPepJob" type="radio" value="MILITARY" maxlength="30" class="checkdb mandatory" onchange="radioAndChkboxFld(this)">
													    <label for="IntrPepMil" id="lblTxtSz-01"><span><img src="vendor/avallis/img/soldier.png">&nbsp;Military</span></label>
							                         </div> 	
							 </div>
						</div>
						 <div class="invalid-feedbacks hide ml-3" id="radFldCDPepJobError">Select anyone of the option in  Job / Political / Military Title of Position Held</div>							  
				   </div>
            </div>
             
             </div>
        
        </div>
        
        <div class="row">
             <div class="col-md-12">
             <div class="form-group">
   <label class="frm-lable-fntsz">In what country is / was the position held<sup style="color: red;"><strong>*</strong></sup></label>
     <select class="custom-select checkdb checkMand mandatory" id="txtFldCDPepJobCont" 
     name="txtFldCDPepJobCont" maxlength="30" onchange="validateFld(this)">
    <option selected value="">--Select--</option>
   <% for(String country:Ekycprop.getString("app.country").split("\\^")){ %>
	   <option value="<%=country%>"><%=country%></option>
    <%}%>
  </select>
   <div class="invalid-feedbacks hide" id="txtFldCDPepJobContError">Select the Country</div>
  </div>
             </div>
        
        </div>
   <div class="row">
       <div class="col-md-12">
             <div class="form-group">
  <label class="frm-lable-fntsz">Relationship to Client (1)/(2)<sup style="color: red;"><strong>*</strong></sup>
</label>
      <select class="custom-select checkdb checkMand mandatory" id="txtFldCDPepRel"
       name="txtFldCDPepRel" maxlength="60" onchange="validateFld(this)">
    <option selected value="">--Select--</option>
     <% for(String relationship:Ekycprop.getString("app.relationship").split("\\^")){ %>
		<option value="<%=relationship%>"><%=relationship%></option>
		<%}%>
  </select>
   <div class="invalid-feedbacks hide" id="txtFldCDPepRelError">Select the Relatinship</div>
  </div>
       </div>
  </div>
  
  <input type="hidden" id="hTxtDataFrmId" name="hTxtDataFrmId" value="${SELF_SPS_ID}">
  
      </div>

      <!-- Modal footer -->
        <div class="modal-footer">
	  
	  <button type="button" class="btn btn-sm btn-bs3-prime mr-auto inptrModalClass" id="btnPepBen" data-dismiss="modal" data-toggle="modal" data-target="#txtAreaModal1">BENF Dets</button>
	  
	  <button type="button" class="btn btn-sm btn-bs3-prime mr-auto inptrModalClass" id="btnPepTpp" data-dismiss="modal" data-toggle="modal" data-target="#txtAreaModal2">TPP Dets </button>
	   
	  
         <button type="button" class="btn btn-sm btn-bs3-prime inptrModalClass" data-dismiss="modal">Ok</button>
        
       
      </div>

    </div>
  </div>
</div>


  
</div>
  
   </div>
   
   <div id="divSpouseDetsElemsParent">
   
   <div class="d-none" id="divSpouseDetsElems">
   
   <div class="row" id="accordian-row">
										        <div class="col-6">
										            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
										                <div class="panel panel-default">
										                    <div class="panel-heading" role="tab" id="headingOne">
										                        <h4 class="panel-title">
										                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOneSps" aria-expanded="true" aria-controls="collapseOneSps">
										                                <i class="fa fa-user" style="color:#4caf50;"></i>&nbsp;Personal Details
										                            </a>
										                        </h4>
										                    </div>
										                    <div id="collapseOneSps" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
										                        <div class="panel-body" style="min-height: 61vh;">
																<div class="card">
																						
																	<div class="card-body">
																			<div class="form-group">
										                                                   <div class="row">
											                                                 <div class="col-12">
											                                                     <label class="frm-lable-fntsz" for="dfSelfName">Name(as per NRIC)</label>
											                                                       <div class="input-group">
																									   <input id="dfSpsName" name="dfSpsName" type="text"  class="form-control checkdb input-md spsTabChange" 
																									    maxlength="75" onchange="changeSpsTabName(this)">
																									     <div class="input-group-prepend">
																											<span class="input-group-text bg-white" style="border-top-right-radius: 6px;border-bottom-right-radius: 6px;"><i class="fa fa-pencil fa-1x text-primary px-1" title="Edit Client(2) Name" aria-hidden="true" onclick="enableEditSpsNameFld()"></i></span>
																										</div>
																								 </div>
											                                                     
											                                                    <div class="invalid-feedbacks hide" id="dfSpsNameError">Keyin Spouse / Client(2) Name</div>
											                                                 
											                                                 </div>
										                                            </div>
										                                              </div>
																	   
																	   
																	   <div class="form-group">
										                                                   <div class="row">
										                                                 <div class="col-md-6">
										                                                     <label class="frm-lable-fntsz" for="dfSpsNric">NRIC/Passport No.</label>
										                                                     <input type="text" id="dfSpsNric" name="dfSpsNric" 
										                                                     class="form-control checkdb input-md" maxlength="20" >
										                                                </div>
										                                                
																				    <div class="col-md-6">
																				      <!--   <div class="form-group">
																						       <button type="button" class="btn btn-toggle mt-4" id="btndfSpsGender" data-toggle="button" aria-pressed="false" >
																									<div class="handle"></div>
																								</button>
																								<input type = "hidden" name="dfSpsGender" id="dfSpsGender">
																						</div> -->
																						
																						<div class="row">
																						    <div class="col-6">
																							      <div id="custTypeRadioBtn" class="mt-3">
																									  <input type="radio" name="dfSpsGender" id="radBtnSpsmale" value="M" maxlength="10" class="checkdb dfSpsGendergrp" >
																									  <label for="radBtnSpsmale" class="pt-1" style="height:5vh;">
																									  <img src="vendor/avallis/img/man.png">&nbsp;Male
																									  </label>
																				                 </div>
																							</div>
																							
																							<div class=" col-6">
																							      <div id="custTypeRadioBtn" class="mt-3">
																									  <input type="radio"  name="dfSpsGender" id="radBtnSpsfemale" value="F" maxlength="10" class="checkdb dfSpsGendergrp">
																									  <label for="radBtnSpsfemale" class="pt-1" style="height:5vh;">
																									  <img src="vendor/avallis/img/woman.png">&nbsp;Female
																									  </label>
																							     </div>
																							</div>
																						
																						
																						</div>
												   
																						 </div>
																				
										                                            </div>
										                                              </div>
																	   
																	   <div class="form-group">
										                                                    <div class="row">
										                                                 
										                                                 
										                                                 <div class="col-md-6">
										                                                      <div class="form-group" id="simple-date2">
										                                                              <label class="frm-lable-fntsz" for="dfSpsDob">Date of Birth</label>
																                      <div class="input-group date">
																                        <div class="input-group-prepend">
																                          <span class="input-group-text"><i class="fas fa-calendar"></i></span>
																                        </div>
																                        <input type="text"  name="dfSpsDob" id="dfSpsDob" class="form-control checkdb" 
																                        maxlength="10">
																                        
																                      </div>
																                      <small id="spsdobHelp" class="form-text text-muted font-sz-level8 mt-1">date Format : DD/MM/YYYY</small>
										           										    </div>
										           										    <input type="hidden" name="dfSpsAge" id="dfSpsAge"/>
										                                                   
										                                                 </div>
																				
																				<div class="col-md-6">
										                                                     <label class="frm-lable-fntsz" for="name">Marital Status</label>
										                                                       <select class="form-control checkdb" name="dfSpsMartsts"
										                                                        id="dfSpsMartsts" maxlength="20">
										                                                     <option selected value="">--Select--</option>
																						    <option value="Single">Single</option>
																						    <option value="Married">Married</option>
																						    <option value="Separated">Separated</option>
																						    <option value="Divorced">Divorced</option>
																						    <option value="Attached">Attached</option>
																						    <option value="Widow">Widow</option>
																						    <option value="Widower">Widower</option>
										                                                        </select>
										                                                 </div>
																				
										                                             </div>    
										                                             </div>
																	  
																	  
																	  <div class="form-group">
										                                       <div class="row">
																			 			<div class="col-md-6">
										                                                     <label class="frm-lable-fntsz" for="name">Country of Birth</label>
										                                                          <select class="form-control checkdb" name="dfSpsBirthcntry" 
										                                                          id="dfSpsBirthcntry" maxlength="20">
										                                                          <option value="" selected>--Select--</option>
																								    <% for(String country:Ekycprop.getString("app.country").split("\\^")){ %>
																								    	<option value="<%=country%>"><%=country%></option>
																								     <%}%>
										                                                        </select>
										                                              </div>
																				
																				
																				<div class="col-6">
																				<span id="spanSpsNatlyDets"></span>
																				 <label class="frm-lable-fntsz" for="name">Nationality</label>
																				           <div class="custom-control custom-radio">
																									    <input type="radio" class="custom-control-input checkdb dfSpsNationalitygrp" type="radio" name="dfSpsNationality" id="dfSpsNationalitySign" value="SG" 
																									    onclick="fnaNaltyFlg(this,'dfSpsNatyDets')" maxlength="30">
																									    <label class="custom-control-label  frm-lable-fntsz font-normal" for="dfSpsNationalitySign">Singaporean</label>
																					        </div>
																					
																					        <div class="custom-control custom-radio">
																									    <input type="radio" class="custom-control-input checkdb dfSpsNationalitygrp" type="radio"
																									     name="dfSpsNationality" id="dfSpsNationalitySign-PR" value="SGPR" 
																									     onclick="fnaNaltyFlg(this,'dfSpsNatyDets')" maxlength="30">
																									    <label class="custom-control-label  frm-lable-fntsz font-normal" for="dfSpsNationalitySign-PR"> Singapore - PR</label>
																					         </div>
																					
																					
																					          <div class="custom-control custom-radio">
																									    <input type="radio" class="custom-control-input checkdb dfSpsNationalitygrp" type="radio" name="dfSpsNationality" id="dfSpsNationalityOth" value="OTH"
																									     onclick="fnaNaltyFlg(this,'dfSpsNatyDets')" maxlength="30">
																									    <label class="custom-control-label  frm-lable-fntsz font-normal" for="dfSpsNationalityOth">Others</label>
																							  </div>
																																														
																					
																					<div class="form-check ">
																					  <select class="form-control checkdb invisible" id="dfSpsNatyDets" name="dfSpsNatyDets" maxlength="60">
																								<option value="" selected>--Select--</option>
																								<%for(String data:Ekycprop.getString("app.citizenship").split("\\^")){ %>
																									<option value="<%=data%>"><%=data%></option>
																								<%} %>
																														  
																					</select>		
																					</div>
																					
																					
																				</div>
																				
																			 </div>
																	  </div>
																	</div>
																						
																</div>
																
																</div>
										                    </div>
										                </div>
										                <div class="panel panel-default">
										                    <div class="panel-heading" role="tab" id="headingTwo">
										                        <h4 class="panel-title">
										                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwoSps" aria-expanded="false" aria-controls="collapseTwoSps">
										                               <i class="fa fa-map-marker" style="color: #f33;"></i>&nbsp;Address Details
										                            </a>
										                        </h4>
										                   </div>
										                    <div id="collapseTwoSps" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
															
															<div class="panel-body" style="min-height: 36vh;">
															
															 <div class="card">
																						
																						<div class="card-body">
																						
																						
																						<div class="form-group">
										                                                        <div class="row">
												                                                    <div class="col-md-12">
												                                                        <label class="frm-lable-fntsz" for="dfSpsHomeaddr">Registered Residential Address</label>
												                                                        <input id="dfSpsHomeaddr" name="dfSpsHomeaddr" type="text"  class="form-control checkdb input-md" 
												                                                         onblur = "enableSelfSpsSameAddrFlg('dfSpsHomeaddr','spouse')" maxlength="450">
												                                                    </div>
												                                              </div>
										                                                 </div>
										                                                 
																						   <div class="form-group">
										                                                        <div class="row">
												                                                    <div class="col-md-12">
												                                                        <!-- <div class="checkbox">
													                                                        <label for="time-01">
													                                                        <input type="checkbox" name="chkSpsSameAddrFlg" id="chkSpsSameAddrFlg" onclick="sameAsResiAddr(this,'chkSpsNoSameAddrFlg')" value="">&nbsp;<span class="frm-lable-fntsz">Mailing Address is same as Residential Address</span></label>
													                                                    </div> -->
													                                                    
													                                                    <div class="custom-control custom-checkbox">
																										    <input type="checkbox" class="custom-control-input checkdb " name="chkSpsSameAddrFlg" 
																										    id="chkSpsSameAddrFlg" onclick="sameAsResiAddr(this,'chkSpsNoSameAddrFlg')" value="" maxlength="20">
																										    <label class="custom-control-label frm-lable-fntsz" for="chkSpsSameAddrFlg">Mailing Address is same as Residential Address</label>
																										  </div>
												                                                    </div>
												                                                </div>
												                                          </div>
																						  
																						  <div class="form-group">
										                                                        <div class="row">
												                                                    <div class="col-md-12">
												                                                       <!--  <div class="checkbox">
													                                                        <label for="time-02">
													                                                        <input type="checkbox" name="dfSpsMailaddrflg" id="chkSpsNoSameAddrFlg" onclick="NotsameAsResiAddr(this,'chkSpsSameAddrFlg')" >&nbsp;<span class="frm-lable-fntsz text-wrap txtarea-hrlines">If different from Registered Residential Address.
										                                                                   </span></label>
													                                                    </div> -->
													                                                    
													                                                     <div class="custom-control custom-checkbox">
																										    <input type="checkbox" class="custom-control-input checkdb " name="dfSpsMailaddrflg" id="chkSpsNoSameAddrFlg" 
																										    onclick="NotsameAsResiAddr(this,'chkSpsSameAddrFlg')" value="" maxlength="20">
																										    <label class="custom-control-label frm-lable-fntsz" for="chkSpsNoSameAddrFlg">If different from Registered Residential Address.</label>
																										  </div>
												                                                    </div>
												                                                </div>
												                                                <div class="row pl-3 ">
												                                                      <div class="col- "><img src="vendor/avallis/img/infor.png" class="mt-2"></div>
												                                                    <div class="col-11">
													                                                     <div class="col-">
													                                                       <small class="font-italic bold text-primaryy font-sz-level8">Please submit a proof of address issued within the last 6 months(e.g.) Bank/Utility/Phone bill statement.</small>
													                                                     </div>
												                                                     </div>
												                                                </div>
												                                                 
												                                          </div>
												                                          
																						     <div class="form-group">
										                                                        <div class="row">
												                                                    <div class="col-md-12">
												                                                        <input  name="dfSpsMailaddr" id="dfSpsMailaddr" type="text"  
												                                                         class="form-control checkdb input-md" maxlength="450">
												                                                    </div>
												                                                </div>
												                                           </div>
																						   
																						   
																						</div>
																						
																						</div>
															</div>
										                        
										                    </div>
										                </div>
										                <div class="panel panel-default">
										                    <div class="panel-heading" role="tab" id="headingThree">
										                        <h4 class="panel-title">
										                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
										                               <i class="fa fa-phone-square" style="color: #028686;"></i>&nbsp;Contact Details
										                            </a>
										                        </h4>
										                    </div>
										                    
										                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
										                        <div class="panel-body">
										                             <div class="card">
																						
																						<div class="card-body">
																						
																						<div class="form-group">
										                                                        <div class="row">
												                                                    <div class="col-md-6">
												                                                        <label class="frm-lable-fntsz" for="dfSelfMobile">Mobile</label>
												                                                        <input id="dfSpsHp" name="dfSpsHp" type="text"
												                                                          class="form-control checkdb input-md dfSpsHpgrp" maxlength="150">
												                                                 </div>
												                                                    
												                                                    <div class="col-md-6">
												                                                        <label class="frm-lable-fntsz" for="dfSelfHome">Home</label>
												                                                        <input id="dfSpsHome" name="dfSpsHome" type="text"
												                                                          class="form-control checkdb input-md dfSpsHpgrp" maxlength="60">
												                                                    </div>
												                                               </div>
										                                                   </div>
																						   
																						       <div class="form-group">
										                                                        <div class="row">
												                                                    <div class="col-md-6">
												                                                        <label class="frm-lable-fntsz" for="dfSelfOffice">Office</label>
												                                                        <input id="dfSpsOffice" name="dfSpsOffice" type="text"
												                                                          class="form-control checkdb input-md dfSpsHpgrp" maxlength="60">
												                                                    </div>
												                                                    
												                                                    <div class="col-md-6">
												                                                        <label class="frm-lable-fntsz" for="dfSelfPersemail">Email Address</label>
												                                                        <input id="dfSpsPersemail" name="dfSpsPersemail" type="email"  class="form-control checkdb input-md dfSpsHpgrp"
												                                                         onblur="validateEmail(this,'dfSpsPersemail')" maxlength="150">
												                                                        <div class="invalid-feedbacks hide" id="dfSpsPersemailError"></div>
												                                                    </div>
												                                               </div>
										                                                   </div>
																						</div>
																						</div>
										                                 </div>
										                    </div>
										                </div>
										            </div>
										        </div>
												<div class="col-6">
										            <div class="panel-group" id="accordion_right" role="tablist" aria-multiselectable="true">
										                <div class="panel panel-default">
										                    <div class="panel-heading" role="tab" id="headingOneR">
										                        <h4 class="panel-title">
										                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_right" href="#collapseOneSpsR" aria-expanded="true" aria-controls="collapseOneSpsR">
										                                <i class="fa fa-money" style="color:#ff9800 ;"></i>&nbsp;Employment &amp; Financial Details
										                            </a>
										                        </h4>
										                    </div>
										                    
										                    <div id="collapseOneSpsR" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOneR" style="margin-bottom:15px;">
										                        <div class="panel-body">
										                           <div class="row">
																			  <div class="col-6">
																			  <div class="card ">
																						<div class="card-header" id="cardHeaderStyle"><span>Employment Info</span></div>
																						<div class="card-body">
																						
																						 
										  												<div class="form-group">
										                                                        <div class="row">
												                                                    <div class="col-md-12">
												                                                        <label class="frm-lable-fntsz" for="dfSelfCompname">Name of Employer</label>
												                                                        <input id="dfSpsCompname" name="dfSpsCompname" type="text"  
												                                                        class="form-control checkdb input-md" maxlength="150" >
												                                                    </div>
												                                               </div>
										                                                   </div>
										  
										  												<div class="form-group">
										                                                        <div class="row">
												                                                    <div class="col-md-12">
												                                                        <label class="frm-lable-fntsz" for="dfSpsOccpn">Occupation</label>
												                                                        <input id="dfSpsOccpn" name="dfSpsOccpn" type="text"
												                                                          class="form-control checkdb input-md" maxlength="60">
												                                                    </div>
												                                                     
												                                              </div>
										                                                   </div>
																						   
																						  <div class="form-group">
										                                                        <div class="row">
												                                                    
												                                                     <div class="col-md-12">
												                                                        <label class="frm-lable-fntsz" for="name">Estd.Annual Income</label>
												                                                          <div class="input-group ">
																											  <div class="input-group-prepend">
																											    <span class="input-group-text">$</span>
																											  </div>
																											  <input type="text"  name="dfSpsAnnlincome" id="dfSpsAnnlincome" 
																											  class="form-control checkdb" aria-label="Amount (to the nearest dollar)" maxlength="30">
																											  <div class="input-group-prepend">
																											    <span class="input-group-text">.00</span>
																											  </div>
																											</div>
												                                                   
												                                                    </div>
												                                              </div>
										                                                   </div>
																						   
																						   <div class="form-group">
										                                                        <div class="row">
												                                                    <div class="col-md-12">
												                                                          <label class="frm-lable-fntsz" for="dfSelfBusinatr">Nature of Business</label>  
												                                                           <select class="form-control checkdb" name="dfSpsBusinatr" id="dfSpsBusinatr"
												                                                             onchange="valiBusNatOthers(this)" maxlength="150">
												                                                             <option value="" selected>--Select--</option>
																											    <%for (String value:Ekycprop.getString("fna.business.list").split("\\^")){%>
																											      <option value="<%=value%>"><%=value%></option>
																											   <%}%>
												                                                           </select>
												                                                    </div>
												                                                     
												                                              </div>
										                                                   </div>
										                                                   
										                                                
										                                                     <div class="form-group invisible" id="dfSpsBusNatOthSec">
										                                                        <div class="row">
												                                                    <div class="col-md-12">
												                                                        <label class="frm-lable-fntsz" for="">Other Detls.</label>
												                                                        <input id="dfSpsBusinatrDets" name="dfSpsBusinatrDets" type="text"  
												                                                        class="form-control checkMand input-md checkdb" maxlength="150" onchange="validateFld(this)">
												                                                        <div class="invalid-feedbacks invisible" id="dfSpsBusinatrDetsError">keyin Other Details </div>
												                                                    </div>
												                                                     
												                                              </div>
										                                                   </div>
										                                                    
																						
																						</div>
																						</div>
																				</div>
																				
																				<div class="col-6">
																				<div class="card">
																						<div class="card-header" id="cardHeaderStyle"><span>Source of Fund</span></div>
																						<div class="card-body">
																						
																						<ul class="list-group" id="ulLiStyle-01">
																							  <li class="list-group-item">
																							   <div class="custom-control custom-checkbox">
																								   <input type="checkbox"  class="custom-control-input checkdb spsSrcFundgrp " name="chkCDSpsFundSrc" id="chkCDSpsErndIncm"  data="SCERN" value="" onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'dfSpsFundsrc')">
																							  <label class="custom-control-label frm-lable-fntsz" for="chkCDSpsErndIncm">&nbsp;&nbsp;Earned Income</label>
																							  <input type="hidden" name="htfcderndicn"/> 
																							 </div>
																							  </li>
																							  
																							  <li class="list-group-item">
																							  <div class="custom-control custom-checkbox">
																								    <input type="checkbox"  class="custom-control-input checkdb spsSrcFundgrp" name="chkCDSpsFundSrc" id="chkCDSpsInvIncm"  data="SCINV"  value="" onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'dfSpsFundsrc');">
																							  <label class="custom-control-label frm-lable-fntsz" for="chkCDSpsInvIncm">&nbsp;&nbsp;Investment</label>
																							   <input type="hidden" name="htfcdinvsticn"/>
																							  </div>
																							 </li>
																							  
																							  <li class="list-group-item">
																							  <div class="custom-control custom-checkbox">
																								   <input type="checkbox" class="custom-control-input checkdb spsSrcFundgrp" name="chkCDSpsFundSrc" id="chkCDSpsPrsnlIncm"   data="SCPRS"  value=""  onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'dfSpsFundsrc');">
																							  <label class="custom-control-label frm-lable-fntsz" for="chkCDSpsPrsnlIncm">&nbsp;&nbsp;Savings</label>
																							  <input type="hidden" name="htfcdpersicn"/>
																							  </div>
																							 </li>
																							  
																							  <li class="list-group-item">
																							  <div class="custom-control custom-checkbox">
																									 <input type="checkbox" class="custom-control-input checkdb spsSrcFundgrp " name="chkCDSpsFundSrc" id="chkCDSpsCPFSvngs"  data="SCCPF"  value="" onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'dfSpsFundsrc');">
																								  <label class="custom-control-label frm-lable-fntsz" for="chkCDSpsCPFSvngs">&nbsp;&nbsp;CPF Savings</label>
																								  <input type="hidden" name="htfcdcpficn"/>
																							  </div>
																							  </li>
																							  
																							  <li class="list-group-item">
																							  
																							  <div class="custom-control custom-checkbox">
																								     <input type="checkbox" class="custom-control-input checkdb spsSrcFundgrp"  name="chkCDSpsFundSrc" id="chkCDSpsSrcIncmOthrs" data="SCOTH"  value="" onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'dfSpsFundsrc');chkSelfSpsOthrValid(this,'dfSpsFundsrc');">
																							       <label class="custom-control-label frm-lable-fntsz" for="chkCDSpsSrcIncmOthrs">&nbsp;&nbsp;Others</label>
													                                              <input type="hidden" name="htfcothicn"/>
													                                              <textarea name="dfSpsFundsrcDets" id="dfSpsFundsrcDets" class="form-control checkdb text-wrap txtarea-hrlines" rows="2" maxlength="50" onkeydown="textCounter(this,50);" onkeyup="textCounter(this,50);"></textarea>
													                                              <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
													                                              <input type="hidden" name="txtFldCDSpsSrcIncmOthDet" id="txtFldCDSpsSrcIncmOthDet"/>
										  													      <input type="hidden" name="dfSpsFundsrc" id="dfSpsFundsrc" maxlength="150"/>
																							 </div>
																							  
																							
										  													 </li>
										  												</ul>
																						
																						</div>
																					</div>
																				
																				</div>
										                        </div>
										                    </div>
										                </div>
										                
										                <div class="panel panel-default">
										                    <div class="panel-heading" role="tab" id="headingTwoR">
										                        <h4 class="panel-title">
										                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_right" href="#collapseTwoSpsR" aria-expanded="false" aria-controls="collapseTwoSpsR">
										                                <i class="fa fa-graduation-cap" style="color:#01579b;"></i>&nbsp;Education Details
										                            </a>
										                        </h4>
										                    </div>
										                    <div id="collapseTwoSpsR" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwoR">
										                        <div class="panel-body">
										                           
										                                      <div class="row">
																			  
																			  <div class="col-6">
																			  <div class="card h-100" id="divSelfEduLevel">
																						<div class="card-header" id="cardHeaderStyle"><span>Highest Education Qualification</span></div>
																						<div class="card-body">
																						<ul class="list-group" id="ulLiStyle-01">
																						
																							  <li class="list-group-item">  
																								  <div class="custom-control custom-radio">
																									    <input type="radio" class="custom-control-input checkdb "  name="dfSpsEdulevelRad" id="chkSpsHEQPri" onclick="setEduLevel(this,'dfSpsEdulevel')" value="PRIMARY" >
																									    <label class="custom-control-label frm-lable-fntsz" for="chkSpsHEQPri"> Primary</label>
																								  </div> 
																								
																							</li>
																							  
																							  <li class="list-group-item">
																							   <div class="custom-control custom-radio">
																								    <input type="radio" class="custom-control-input checkdb "  name="dfSpsEdulevelRad" id="chkSpsHEQSec" onclick="setEduLevel(this,'dfSpsEdulevel')" value="SECONDARY">
																								    <label class="custom-control-label frm-lable-fntsz" for="chkSpsHEQSec"> Secondary</label>
																							  </div> 
																							  
																							  </li>
																							  
																							  <li class="list-group-item"> 
																							  
																							   <div class="custom-control custom-radio">
																								    <input type="radio" class="custom-control-input checkdb " name="dfSpsEdulevelRad" id="chkSpsHEQPreTer" onclick="setEduLevel(this,'dfSpsEdulevel')" value="PRETER">
																								    <label class="custom-control-label frm-lable-fntsz" for="chkSpsHEQPreTer"> Pre-Tertiary</label>
																							  </div> 
																							  
																								
																							  </li>
																							  
																							  
																							  
																							  <li class="list-group-item"> 
																							  
																							  <div class="custom-control custom-radio">
																								    <input type="radio" class="custom-control-input checkdb " name="dfSpsEdulevelRad" id="chkSpsHEQGce" onclick="setEduLevel(this,'dfSpsEdulevel')" value="EQGCE">
																								    <label class="custom-control-label frm-lable-fntsz" for="chkSpsHEQGce">  GCE 'N' or 'O Level or Equivalent Academic Qualification</label>
																							  </div> 
																							  <input type="hidden" name="dfSpsEdulevel" id="dfSpsEdulevel" maxlength="60">
																							</li>
																							
																							<li class="list-group-item"> 
																							  
																							    
																							   <div class="custom-control custom-radio">
																								    <input type="radio" class="custom-control-input checkdb " name="dfSpsEdulevelRad" id="chkSpsHEQTerAbv" onclick="setEduLevel(this,'dfSpsEdulevel')" value="EQTERABV">
																								    <label class="custom-control-label frm-lable-fntsz" for="chkSpsHEQTerAbv"> Tertiary and above</label>
																							  </div> 
																							  
																								
																							  </li>
																							
																							
																						</ul>	
																					</div>
																				
																				</div>
																			  </div>
																			  <div class="col-6">
																			  <div class="card">
																						<div class="card-header" id="cardHeaderStyle"><span>Language Proficiency</span></div>
																						<div class="card-body">
																						
																						<span id="span_dfSpsEngSpoken"></span>
																						
																						<ul class="list-group" id="ulLiStyle-01">
																							  <li class="list-group-item"> <div class="frm-lable-fntsz bold" style="font-weight: 700;">Spoken English</div>
																								  
																								  <div class="custom-control custom-radio">
																									    <input type="radio" class="custom-control-input checkdb " id="dfSpsEngSpokenY" name="dfSpsEngSpoken" value="Y" maxlength="1">
																									    <label class="custom-control-label frm-lable-fntsz" for="dfSpsEngSpokenY" style="text-align:left;"> Proficient </label>
																								  </div> 
																								  
																								   <div class="custom-control custom-radio">
																									    <input type="radio" class="custom-control-input checkdb " id="dfSpsEngSpokenN" name="dfSpsEngSpoken" value="N" maxlength="1">
																									    <label class="custom-control-label frm-lable-fntsz" for="dfSpsEngSpokenN" style="text-align:left;"> <span class="txt-urline" style="font-weight: 500;"> Not</span> Proficient </label>
																								  </div>
																							
																							</li>
																							</ul>
																							</div>
																							
																							<div class="card-body">
																							<span id="span_dfSpsEngWritten"></span>
																							  <ul class="list-group" id="ulLiStyle-01">
																							  <li class="list-group-item">
																							  <div class="frm-lable-fntsz bold" style="font-weight: 700;">Written English</div>
																								  <div class="custom-control custom-radio">
																									    <input type="radio" class="custom-control-input checkdb " id="dfSpsEngWrittenY" name="dfSpsEngWritten" value="Y" maxlength="1">
																									    <label class="custom-control-label frm-lable-fntsz" for="dfSpsEngWrittenY" style="text-align:left;"> Proficient  </label>
																								  </div> 
																								  
																								 <div class="custom-control custom-radio">
																									    <input type="radio" class="custom-control-input checkdb " id="dfSpsEngWrittenN" name="dfSpsEngWritten" value="N" maxlength="1">
																									    <label class="custom-control-label frm-lable-fntsz" for="dfSpsEngWrittenN" style="text-align:left;"><span class="txt-urline" style="font-weight: 500;"> Not</span> Proficient </label>
																								  </div> 
																									
																									
																							</li>
																							
																							
																						</ul>
																					 
																						
																						</div>
																					
																					</div>
																					
																					
										                                      </div>
																			  </div>
															 </div>
										                    </div>
										                </div>
										                
										               
										                   
										            </div>
										        </div>
										    </div>
										</div>
   </div>
   
   </div>
   
   
   
    <!-- Popover contents kycpage2.html  personal details card notes content here -->
		 <div id="popover-contentPg2PersonalDetlsCardNotes" style="display: none">
		  <ul class="list-group custom-popover">
		    <li class="list-group-item"><i class="fa fa-sticky-note-o" aria-hidden="true" id="i-icon-note-style"></i>&nbsp;Our company may contact you to confirm your understanding of the product(s) recommended by the Representative.</li>
		  </ul>
		 </div>
		<!--End--> 

         <script src="vendor/jquery-easing/jquery.easing.min.js"></script> 
          <script src="vendor/avallis/js/ekyc-layout.js"></script> 
          <script src="js/jquery.steps.js"></script>
          <script src="js/select2.js"></script>
		  <script src="vendor/gojs/go.js"></script>  
		  <script src="vendor/gojs/FreehandDrawingTool.js"></script>
		  <script src="vendor/gojs/GeometryReshapingTool.js"></script>
		  <script src="vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
		  <script src="vendor/avallis/js/kyc_home.js"></script>
		  <script src="vendor/avallis/js/Common Script.js"></script>
		  
		  
		  <script>jsnDataCustomerDetails =${CUSTOMER_DETAILS}</script>
		  
		  <script>jsnDataFnaDetails = ${CURRENT_FNA_DETAILS}</script>
	  
		  <script>jsnDataSelfSpsDetails = ${SELFSPOUSE_DETAILS}</script>
		  
		  <script>jsnFnaOtherPersDets = ${OTHPERS_DETAILS}</script>
		  
		  
		 
		  <script>
		//Bootstrap Date Picker
		  $('#simple-date1 .input-group.date').datepicker({
			//dateFormat: 'dd/mm/yyyy',
		  	 format: 'dd/mm/yyyy',
		  	todayBtn: 'linked',
		  	language: "it",
		  	todayHighlight: true,
		  	autoclose: true,         
		  });

		  $('#simple-date2 .input-group.date').datepicker({
		  	//dateFormat: 'dd/mm/yyyy',
		  	format: 'dd/mm/yyyy',
		  	todayBtn: 'linked',
		  	language: "it",
		  	todayHighlight: true,
		  	autoclose: true,         
		  });
		  
		  </script>
		  
</body>
</html>
