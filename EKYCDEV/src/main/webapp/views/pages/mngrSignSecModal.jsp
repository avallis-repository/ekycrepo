 <!-- manager Sec Open Modal Start -->
      
      
      <div class="modal fade" id="openMngesecModal"  aria-modal="true" role="dialog">
    <div class="modal-dialog modal-xl  modal-dialog-scrollable">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header" id="cardHeaderStyle-22">
        <h6 class="modal-title bold"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp;Supervisor's Review</h6>
        <button type="button" class="close" data-dismiss="modal" style="color:#fff;">�</button>
      </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            
          <!-- manager Sec Start  -->
          
                <div class="card card-content-fnsize" style="margin-top:10px;" id="diManagerSection">
                          <div class="card-body">
                                   <div class="row">
										           <div class="col-md-12">
										              <span class="font-sz-level6">I have reviewed the information disclosed in the "Financial Needs Analysis" Form which relates to the
                                                       client's priorities and objectives, investment profile, insurance portfolio, CKA outcome and the client's. 
                                                        declaration &amp; acknowledgement.</span>
										           </div>
								  </div>
								   <div class="row mt-2" id="mngrSecChk">
                                         <div class="col-md-12">
                                          <ul class="list-group mt-2">
		                                          <li class="list-group-item bg-hlght" id="errorFld">
		                                              <span class="italic bold text-primaryy font-sz-level6 "> Select any <span class="txt-urline  bold">ONE</span> Option Below :-</span>
		                                                      <div class="row">
								        <div class="col-md-6 ">
								          <div class="col-md-12">
								              
								              <div id="Sec2ContentFormId">
			                                    <div class="inputGroup">
													         <input id="suprevMgrFlgA" name="suprevMgrFlg" type="radio" value="" onclick="enableMgr();chkAgreeDisagreeFld(this,'suprevFollowReason');">
															<label for="suprevMgrFlgA" id="lblTxtSz-03" style="border: 1px solid grey;"><span><span class="font-sz-level6">  <span class="text-success"><strong>I agree</strong></span> with the Representative's needs analysis and<br> recommendation(s). </span> </span></label>
												 </div>
                                              </div>
										            
										  </div>
								        </div>
								        <div class="col-md-6">
								             <div class="col-md-12">
								             <div id="Sec2ContentFormId">
												<div class="inputGroup">
													         <input id="suprevMgrFlgD" name="suprevMgrFlg" type="radio" value="" onclick="enableMgr();chkAgreeDisagreeFld(this,'suprevFollowReason');">
															 <label for="suprevMgrFlgD" id="lblTxtSz-03" style="border: 1px solid grey;"><span><span class="font-sz-level6"> <span class="text-danger"><strong>I disagree</strong></span> with the Representative's needs analysis and<br> recommendation(s).</span></span></label>
												 </div>
                                              </div>
										            
										       </div>
								        </div>
								  </div>
								  
		                                          
		                                          </li>
		                                          
		                                          
		                                          </ul>
		                                         
		                                          
                                           <div class="invalid-feedbacks" id="mngrBtnErrorMsg"></div>
                                         
                                            
								          </div>
								        
								    </div>
								 
								 
								  
								  <div class="row mt-2">
								       <div class="col-md-12">
								         <small><i> <strong>Note: </strong> (If <span class=" text-danger bold font-sz-level8">disagree</span> , please state the reasons and/or advise on the follow-up action required, where applicable.)</i></small>
								       </div>
								       
								   </div>
								   
								   
								    <div class="row">
								       <div class="col-md-8">
								             <div class="form-group">
								                  <label class="frm-lable-fntsz "> Reason(s) and Follow-up Action:</label>
									             <textarea class="form-control txtarea-hrlines text-wrap" rows="7" id="suprevFollowReason" name="suprevFollowReason" onblur="hideErrmsg(this)"
									            	 maxlength="300" onkeydown="textCounter(this,300);" onkeyup="textCounter(this,300);"></textarea>
								                     <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
							                     <div class="invalid-feedbacks" id="mngrDisagrReason"></div>
							                </div> 
								       </div>
								       
								        <div class="col-md-4">
								             <div class="card" style="margin-top:15px;" id="SignCard">
								             <div class="card-header" id="cardHeaderStyle"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Manager Signature</div>
								                								                  <div class="card-body">
								                     <div class="row mt-2">
								                            <div class="col-md-6">
								                                 <label class="frm-lable-fntsz p-0">Sign. of Supervisor :  </label>
								                            </div>   
								                            
								                            
								                            <!-- signature Click open San to Sign Modal Page start  -->
								                             <div class="col-md-6">
								                                 <div id="mgrQrSecNoData"  class="mgrQrSecNoData show">
								                                   <p class="center noData" id="" style="font-size: 11px;"><img src="${contextPath}/vendor/avallis/img/warning.png" class="">
								                                     &nbsp;Manager Signature Not Found&nbsp;</p>
								                                  </div>
								                                  
								                                  <div id="sign-QR_style" class="mgrQrSec hide">
								                                    <span><img src="${contextPath}/views/newapproval/images/contract.png" class="mgrQr" data-toggle="modal" data-target="#ScanToSignModal"></span>
								                                  </div>
								                            </div>  
								                            
								                            <!-- signature Click open San to Sign Modal Page End -->
								                             
								                      </div>
								                      
								                      <div class="row mt-2">
								                            <div class="col-md-6">
								                                <label class="frm-lable-fntsz p-0">Name of Supervisor: </label>
								                            </div>
								                            
								                             <div class="col-md-6">
								                             <input type="text" name="mgrName" class="form-control" value="">
								                            
								                            </div>  
								                                
								                      </div>
								                      
								                       <div class="row mt-2 hide">
								                            <div class="col-md-6">
								                                <label class="frm-lable-fntsz p-0"> Mail Sent Status : </label>
								                            </div> 
								                             <div class="col-md-6">
								                                <span class="badge badge-success " id="mailSentMgr" style="display:inline;">Email Sent <i class="fa fa-check" aria-hidden="true"></i></span>
								                                <span class="badge badge-warning font-normal d-none" id="mailNotSentMgr">Email Not Sent <i class="fa fa-times" aria-hidden="true"></i></span>
								                            </div>    
								                      </div>
								                      
								                      <div class="row mt-2">
								                            <div class="col-md-6">
								                                 <label class="frm-lable-fntsz p-0">Date: </label>
								                            </div> 
								                            
								                             <div class="col-md-6">
								                                 <span id="mgrSignDate" ></span>
								                            </div>    
								                      </div>
								                  </div>
								             </div> 
								       </div>
								       
								   </div>
								   
								     
                               </div>
                       </div>
          
          <!--manager Sec  End -->
             </div>
      
      <div class="modal-footer">
				<button type="button" class="btn btn-sm btn-bs3-prime" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-sm btn-bs3-prime" id="btnApprvMngr111" onclick="mngrSendToApprove()">Update Status</button>
	   </div>
    </div>
  </div>
  
</div>
   
   
   <!-- mngr sec modal end -->