<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

  
   <script src="${contextPath}/vendor/gojs/go.js"></script>


<script>
//go.licenseKey = "73f04ee4b41c28c702d90776423d6bf919a52e609a8418a35a0442f6ba096146729ce97154d788d2c2ff46ac127cc38a8e91392a934c076be235d5d513e7d4feb03674b5130a42def003219499f928a6ab6a61f497e571a288288de0fbabc29c54f7fbcb48c2"
go.licenseKey = "";
</script>
   


  <script src="${contextPath}/vendor/gojs/FreehandDrawingTool.js"></script>
  <script src="${contextPath}/vendor/gojs/GeometryReshapingTool.js"></script>
  
 <link rel="stylesheet" href="${contextPath}/vendor/avallis/css/stylesheet.css"/>
 
 <style>
 
 
 .disabledsec{
    pointer-events: none;
    opacity: 0.4;
}

 </style>
 
 <script src="${contextPath}/vendor/avallis/js/qresign.js"></script>
 
<div class="modal right fade" id="ScanToSignModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
      <!-- Modal body -->
        <div class="modal-body">
        <small class="noteInfoMsg" title="Note Information"><img src="${contextPath}/vendor/avallis/img/infor.png" alt="Note" class="img-rounded mr-2">&nbsp;<span id="noteInfo">Select the name for e-Signature</span></small>
       
        
     <input type="hidden" id="hdnSessFnaId" value=""/>
     <input type="hidden" name="dataformId" id="dataformId" value=""/>
     <input type="hidden" id="curPage" value="<%= session.getAttribute("CURRENT_SCREEN") %>"/>
      <input type="hidden" id="perType" name="perType">
<div class="tab-regular mt-3" id="clntProdTabs" style="width:100%;">
                              <ul class="nav nav-tabs " id="myTabSign" role="tablist">
                                 <li class="nav-item"> <a class="nav-link active" id="test1-tab" data-toggle="tab" href="#test1" style="padding: 8px;" role="tab" aria-controls="test1" aria-selected="true"><i class="fa fa-qrcode" aria-hidden="true"></i>&nbsp;Scan to Sign</a> </li>
                                <!--  <li class="nav-item"> <a class="nav-link" id="test2-tab" data-toggle="tab" href="#test2" style="padding: 8px;" role="tab" aria-controls="test2" aria-selected="false"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Signpad</a> </li> -->
                                
                              </ul>
                              <!--Client 1 Table Tab Content Start  -->
                              <div class="tab-content" id="myTabContent" style="/*margin: 15px;*/">
                                 <div class="tab-pane fade show active" id="test1" role="tabpanel" aria-labelledby="test1-tab">
                                   
									<!-- Signature Card Start  -->
									<div class="row">
									    <div class="col-12">
									         <div class="card p-3">
								                    <div class="row">
								                        <div class="col-5 border-right">
								                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
										                            <a id="scanToSignClient1Btn" class="nav-link font-sz-level5 text-primaryy bold mt-4 active" data-toggle="pill" href="#v-pills-Client1" role="tab" 
										                            onclick="genQrOrSign('CLIENT','Client1');"><span class="selfNameTab"></span>
										                            <br/><small>Client(1)</small><small class="float-right"><i id="signmodalclient1qrsign" class="fa fa-exclamation-triangle" aria-hidden="true"></i></small>
										                            </a> 
										                           <%--  <% if (session.getAttribute("FNA_SPOUSE_NAME") != null) { %> --%>
																	    <a  id="scanToSignClient2Btn" class="nav-link font-sz-level5 text-primaryy bold mt-4 hide" data-toggle="pill" href="#v-pills-Client2" role="tab" 
																	    onclick="genQrOrSign('SPOUSE','Client2');"><span class="spsNameTab"></span>
																	    <br/><small>Client(2)</small><small class="float-right"><i id="signmodalclient2qrsign" class="fa fa-exclamation-triangle" aria-hidden="true"></i></small>
																	    </a>
																	<%-- <%}%> --%>
																	   
										                             
										                            <a id="signAdvisor" class="nav-link font-sz-level5 text-primaryy bold mt-4" data-toggle="pill" href="#v-pills-Advisor" role="tab" 
										                            onclick="genQrOrSign('ADVISER','Adviser');"><span class="adviserNameTab">${LOGGED_USER_INFO.FNA_ADVSTFNAME}</span><%-- ${LOGGED_USER_INFO.FNA_ADVSTFNAME} --%>
										                            <br/><small>Adviser</small><small class="float-right"><i id="signmodaladviserqrsign" class="fa fa-exclamation-triangle" aria-hidden="true"></i></small>
										                            
										                            </a>
										                            
										                            <a id="signManagerQr" class="nav-link font-sz-level5 text-primaryy bold mt-4" data-toggle="pill" href="#v-pills-Manager" role="tab"
										                             onclick="genQrOrSign('MANAGER','Manager');" style="display:none;"><span class="managerNameTab">${LOGGED_USER_INFO.FNA_ADVSTF_MGRNAME}</span><%-- ${LOGGED_USER_INFO.FNA_ADVSTF_MGRNAME} --%>
										                             <br/><small>Manager</small><small class="float-right"><i id="signmodalmanagerqrsign" class="fa fa-exclamation-triangle" aria-hidden="true"></i></small>
										                             </a>
										                            
								                             </div>
								                        </div>
								                        <div class="col-7"><!-- style="width:100%;height:22vh;"  -->
								                            <div class="tab-content" id="v-pills-tabContent">
								                                <div class="tab-pane fade show active" id="v-pills-Client1" role="tabpanel" aria-labelledby="v-pills-Client1-tab">
								                                      <div class="card" style="min-height:33vh;">
								                                           <div class="card-header" id="cardHeaderStyle">
								                                           <span class="font-sz-level7">Client(1) : </span><span class="selfNameTab"></span><br/>
								                                            
								                                           </div>
								                                           <div class="card-body text-center" id="qrCodeSecClient1">
								                                                 <!-- Signature Loader -->
								                                                      <img src="${contextPath}/vendor/avallis/img/signload.gif" id="signLoaderClient1" style="margin: 10% 32%; width: 40%; display: none;">
								                                                  <!-- end -->
								                                               	 <img src="" id="qrCodeClient1">
								                                           </div>
								                                           
								                                           <div class="card-body text-center" id="signSecClient1">
									                                           <div id="self">
	  																			 <div id="selfDiagram" style="border: solid 1px black; width: 100%; height: 250px"></div>
	  																		   	<div id="buttons">
																				    <!-- <button onclick="selfSave();">Save</button> -->
																				    <!-- <button onclick="selfClearDiagram('selfSavedDiagram');">Clear</button> -->
																				     <button class="genqrbtn" onclick="generateQrCode('CLIENT','Client1');">Clear Signature</button>
																			  </div>
																			  <form name="selfform">
																			  <input type="hidden" id="selfSignId" name="selfSignId">
																			  <input type="hidden" id="selfLoad" name="selfLoad" value="Y">
																			  <textarea id="selfSavedDiagram" name="selfSavedDiagram" style="width:100%;height:300px;display:none">
																			{ "position": "0 0",
																			  "model": { "class": "GraphLinksModel",
																			  "nodeDataArray": [],
																			  "linkDataArray": []} }
																			  </textarea>
																			  </form>
									                                      </div>  
									                                    </div>
									                                    <div class="card-footer p-1 bg-info"><div class="font-sz-level7 white" id="spanClientSignWaitMsg"></div></div> 
								                                      </div>  
								                                </div>
								                                <div class="tab-pane fade" id="v-pills-Client2" role="tabpanel" aria-labelledby="v-pills-Client2-tab">
								                                        <div class="card" style="min-height:33vh;">
								                                           <div class="card-header" id="cardHeaderStyle">
								                                           <span class="font-sz-level7">Client(2) :</span><span class="spsNameTab"></span><br/>
								                                           
								                                           </div>
								                                           <div class="card-body text-center " id="qrCodeSecClient2">
								                                                <!-- Signature Loader -->
								                                                      <img src="${contextPath}/vendor/avallis/img/signload.gif" id="signLoaderClient2" style="margin: 10% 32%; width: 40%; display: none;">
								                                                  <!-- end -->
								                                               	 <img src="" id="qrCodeClient2">
								                                           </div>
								                                           
								                                                <div class="card-body text-center" id="signSecClient2">
								                                           <div id="sps">
  																			 <div id="spsDiagram" style="border: solid 1px black; width: 100%; height: 250px"></div>
  																		   	<div id="buttons">
  																		   	   <!-- <button onclick="spsClearDiagram('spsSavedDiagram');">Clear</button>
																			    <button onclick="spsSave();">Save</button> -->
																			    <button class="genqrbtn" onclick="generateQrCode('SPOUSE','Client2');">Clear Signature</button>
																			    
																			    
																		  
																		  </div>
																		  <form name="spsform">
																		 <input type="hidden" id="spsSignId" name="spsSignId">
																		 <input type="hidden" id="spsLoad" name="spsLoad" value="Y">
																		  <textarea id="spsSavedDiagram" name="spsSavedDiagram" style="width:100%;height:300px;display:none">
																		{ "position": "0 0",
																		  "model": { "class": "GraphLinksModel",
																		  "nodeDataArray": [],
																		  "linkDataArray": []} }
																		  </textarea>
																		  </form>
								                                      </div>  
								                                      </div>
								                                           
								                                            <div class="card-footer p-1 bg-info"><div class="font-sz-level7 white" id="spanSpsSignWaitMsg"></div></div> 
								                                      </div> 
								                                </div>
								                                
								                                <div class="tab-pane fade" id="v-pills-Advisor" role="tabpanel" aria-labelledby="v-pills-Advisor-tab">
								                                          <div class="card" style="min-height:33vh;">
								                                           <div class="card-header" id="cardHeaderStyle">
								                                           
								                                           <div class="row">
								                                           		<div class="col-6">
								                                           			<span class="font-sz-level7">Adviser : </span><span class="adviserNameTab"></span><br/><%-- ${LOGGED_USER_INFO.FNA_ADVSTFNAME} --%>
								                                           		</div>
								                                           		<div class="col-6">
								                                           			<span class="float-right">
											                                            <a class="btn btn-bs3-prime btn-sm esignpad d-none" onclick="toggleSignQrCodeAdv(this,'Adviser')" id="btnAdvSignToggle">e-Sign Pad</a>
											                                            <input type="hidden" id="hTxtFldCurrAdvSignMode" value="qr"/>
											                                         </span>
								                                           		</div>
								                                           		
								                                           	</div>
								                                           
								                                          
								                                           </div>
								                                           <div class="card-body text-center" id="qrCodeSecAdviser">
								                                                  <!-- Signature Loader -->
								                                                      <img src="${contextPath}/vendor/avallis/img/signload.gif" id="signLoaderAdviser" style="margin: 10% 32%; width: 40%; display: none;">
								                                                  <!-- end -->
								                                               	 <img src="" id="qrCodeAdviser">
								                                           </div>
								                                           
								                                         <div class="card-body text-center" id="signSecAdviser">
								                                           <div id="adv">
  																			 <div id="advDiagram" style="border: solid 1px black; width: 100%; height: 250px"></div>
  																		   	<div id="buttons">
  																		   	    <!-- <button onclick="advClearDiagram('advSavedDiagram');">Clear</button>
																			    <button onclick="advSave();">Save</button>
																			     -->
																		       <button class="genqrbtn" onclick="generateQrCode('ADVISER','Adviser');">Clear Signature</button>
																		  </div>
																		  <form name="advform">
																		  <input type="hidden" id="advSignId" name="advSignId">
																		  <input type="hidden" id="advLoad" name="advLoad" value="Y">
																		  <textarea id="advSavedDiagram" name="advSavedDiagram" style="width:100%;height:300px;display:none">
																		{ "position": "0 0",
																		  "model": { "class": "GraphLinksModel",
																		  "nodeDataArray": [],
																		  "linkDataArray": []} }
																		  </textarea>
																		  </form>
								                                      </div> 
								                                      </div> 
								                                      <div class="card-footer p-1 bg-info"><div class="font-sz-level7 white" id="spanAdvSignWaitMsg"></div></div>
								                                      </div> 
								                                </div>
								                              
								                                <div class="tab-pane fade" id="v-pills-Manager" role="tabpanel" aria-labelledby="v-pills-Manager-tab">
								                                          <div class="card" style="min-height:33vh;">
								                                           <div class="card-header" id="cardHeaderStyle">
								                                           
								                                           	<div class="row">
								                                           		<div class="col-6">
								                                           			<span class="font-sz-level7">Manager : ${LOGGED_USER_INFO.FNA_ADVSTF_MGRNAME}</span>
								                                           		</div>
								                                           		<div class="col-6">
								                                           			<span class="float-right">
											                                            <a class="btn btn-bs3-prime btn-sm esignpad" onclick="toggleSignQrCode(this,'Manager')" id="btnMgrSignToggle">e-Sign Pad</a>
											                                         </span>
								                                           		</div>
								                                           		
								                                           	</div>
								                                           		
								                                            	<div class="row">
								                                            	   <div class="col-12">
								                                            	        <small class="p-1 ml-3 m-1" title="Note Information">
                               		                                                     <img src="${contextPath}/vendor/avallis/img/infor.png" alt="Note" class="img-rounded mr-2">&nbsp;<span id="spanSignHelpText">Draw your Sign and Click Ok Button</span>
                                                                                	</small>
								                                            	   </div>
								                                            	</div>
								                                           </div>
								                                           
								                                            <div class="card-body text-center d-none" id="qrCodeSecManager">
								                                                  <!-- Signature Loader -->
								                                                      <img src="${contextPath}/vendor/avallis/img/signload.gif" id="signLoaderManager" style="margin: 10% 32%; width: 40%; display: none;">
								                                                  <!-- end -->
								                                               	 <img src="" id="qrCodeManager"> <br/>
								                                               	 
								                                               	 
								                                               	 
								                                           </div>
								                                           
								                                           <div class="card-body text-center" id="signSecManager">
								                                                 <div id="mgr">
  																			 <div id="mgrDiagram" style="border: solid 1px black; width: 100%; height: 250px"></div>
  																			 <div id="buttons">
  																		   	    <!-- <button onclick="advClearDiagram('advSavedDiagram');">Clear</button>
																			    <button onclick="advSave();">Save</button>
																			     -->
																		       <button class="genqrbtnmgr" onclick="generateQrCode('MANAGER','Manager');">Clear Signature</button>
																		  </div>
  																		   	<!-- <div id="buttons"><button onclick="mgrClearDiagram('mgrSavedDiagram');">Clear</button>
																			    <button onclick="mgrSave();">Save</button>
																			    
																		  
																		  </div> -->
																		  <form name="mgrform">
																		   <input type="hidden" id="mgrSignId" name="mgrSignId">
																		   <input type="hidden" id="mgrLoad" name="mgrLoad" value="Y">
																		  <textarea id="mgrSavedDiagram" name="mgrSavedDiagram" style="width:100%;height:300px;display:none">{"position": "0 0","model":{"class":"GraphLinksModel","nodeDataArray":[],"linkDataArray": []}}</textarea>
																		  <!-- Dont change this textarea content -->
																		  </form>
								                                           </div>
								                                          
								                                           
								                                      </div> 
								                                        
								                                       <div class="card-footer">   <span class="font-sz-level7" id="spanMgrSignWaitMsg"></span> </div>
								                                </div>
								                            </div>
								                        </div>
								                    </div>
                                             </div>
									    </div>
									</div>
									<!-- Signature Card End -->
									
									
                                   </div>
                                   </div>
                                   
                                   <%--                                  <div class="tab-pane fade" id="test2" role="tabpanel" aria-labelledby="test2-tab">
                                   <!-- Start signpad -->
                                    <div class="row">
									    <div class="col-12">
									         <div class="card p-3">
								                    <div class="row">
								                        <div class="col-5 border-right">
								                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
										                             <a  class="nav-link font-sz-level5 text-primaryy bold mt-4" data-toggle="pill" href="#v-pills-spClient1" role="tab" onclick="selfInit('selfDiagram','selfSavedDiagram');setClientType('CLIENT');selfSignData();">${FNA_SELF_NAME}<br/><small>Client(1)</small></a>
										                            <% if (session.getAttribute("FNA_SPOUSE_NAME") != null) { %>
																	    <a  class="nav-link font-sz-level5 text-primaryy bold mt-4" data-toggle="pill" href="#v-pills-spClient2" role="tab" onclick="spsInit('spsDiagram','spsSavedDiagram');setClientType('SPOUSE');spsSignData();"><%= session.getAttribute("FNA_SPOUSE_NAME") %><br/><small>Client(2)</small></a>
																	<%}%>
																	   
										                             
										                            <a id="signAdvisor" class="nav-link font-sz-level5 text-primaryy bold mt-4" data-toggle="pill"
										                             href="#v-pills-spAdvisor" role="tab" 
										                             onclick="advInit('advDiagram','advSavedDiagram');setClientType('ADVISER');advSignData();">${LOGGED_USER_INFO.FNA_ADVSTFNAME}<br/><small>Adviser</small></a>
										                            <a id="signManager" class="nav-link font-sz-level5 text-primaryy bold mt-4" data-toggle="pill" 
										                            href="#v-pills-spManager" role="tab" 
										                            onclick="mgrInit('mgrDiagram','mgrSavedDiagram');setClientType('MANAGER');mgrSignData();" style="display:none;">${LOGGED_USER_INFO.FNA_ADVSTF_MGRNAME}</a>
								                             </div>
								                        </div>
								                        <div class="col-7" >
								                            <div class="tab-content" id="v-pills-tabContent" >
								                           
								                                <div class="tab-pane fade" id="v-pills-spClient1" role="tabpanel" aria-labelledby="v-pills-spClient1-tab">
								                                      <div class="card" style="min-height:33vh;" >
								                                           <div class="card-header" id="cardHeaderStyle">
								                                           <span class="font-sz-level7">Client(1) : ${FNA_SELF_NAME} </span>
								                                           
								                                           
								                                           </div>
								                                           <div class="card-body text-center" >
									                                           <div id="self">
	  																			 <div id="selfDiagram" style="border: solid 1px black; width: 100%; height: 250px"></div>
	  																		   	<div id="buttons"><button onclick="selfClearDiagram('selfSavedDiagram');">Clear</button>
																				    <button onclick="selfSave();">Save</button>
																				   
																			  </div>
																			  <form name="selfform">
																			  <input type="hidden" id="selfSignId" name="selfSignId">
																			  <input type="hidden" id="selfLoad" name="selfLoad" value="Y">
																			  <textarea id="selfSavedDiagram" name="selfSavedDiagram" style="width:100%;height:300px;display:none">
																			{ "position": "0 0",
																			  "model": { "class": "GraphLinksModel",
																			  "nodeDataArray": [],
																			  "linkDataArray": []} }
																			  </textarea>
																			  </form>
									                                      </div>  
									                                    </div>
								                                </div>
								                                </div>
								                                <div class="tab-pane fade" id="v-pills-spClient2" role="tabpanel" aria-labelledby="v-pills-spClient2-tab">
								                                        <div class="card" style="min-height:33vh;">
								                                           <div class="card-header" id="cardHeaderStyle">
								                                           <span class="font-sz-level7">Client(2) : ${FNA_SPOUSE_NAME}</span>
								                                           
								                                           </div>
								                                           <div class="card-body text-center" >
								                                           <div id="sps">
  																			 <div id="spsDiagram" style="border: solid 1px black; width: 100%; height: 250px"></div>
  																		   	<div id="buttons"><button onclick="spsClearDiagram('spsSavedDiagram');">Clear</button>
																			    <button onclick="spsSave();">Save</button>
																			    
																		  
																		  </div>
																		  <form name="spsform">
																		 <input type="hidden" id="spsSignId" name="spsSignId">
																		 <input type="hidden" id="spsLoad" name="spsLoad" value="Y">
																		  <textarea id="spsSavedDiagram" name="spsSavedDiagram" style="width:100%;height:300px;display:none">
																		{ "position": "0 0",
																		  "model": { "class": "GraphLinksModel",
																		  "nodeDataArray": [],
																		  "linkDataArray": []} }
																		  </textarea>
																		  </form>
								                                      </div>  
								                                      </div>
								                                      </div> 
								                                </div>
								                                
								                                <div class="tab-pane fade" id="v-pills-spAdvisor" role="tabpanel" aria-labelledby="v-pills-spAdvisor-tab">
								                                          <div class="card" style="min-height:33vh;">
								                                           <div class="card-header" id="cardHeaderStyle">
								                                           <span class="font-sz-level7">Adviser : ${LOGGED_USER_INFO.FNA_ADVSTFNAME}</span>
								                                           
								                                           </div>
								                                           <div class="card-body text-center" >
								                                           <div id="adv">
  																			 <div id="advDiagram" style="border: solid 1px black; width: 100%; height: 250px"></div>
  																		   	<div id="buttons"><button onclick="advClearDiagram('advSavedDiagram');">Clear</button>
																			    <button onclick="advSave();">Save</button>
																			    
																		  
																		  </div>
																		  <form name="advform">
																		  <input type="hidden" id="advSignId" name="advSignId">
																		  <input type="hidden" id="advLoad" name="advLoad" value="Y">
																		  <textarea id="advSavedDiagram" name="advSavedDiagram" style="width:100%;height:300px;display:none">
																		{ "position": "0 0",
																		  "model": { "class": "GraphLinksModel",
																		  "nodeDataArray": [],
																		  "linkDataArray": []} }
																		  </textarea>
																		  </form>
								                                      </div> 
								                                      </div> 
								                                      </div> 
								                                </div>
								                              
								                                <div class="tab-pane fade" id="v-pills-spManager" role="tabpanel" aria-labelledby="v-pills-spManager-tab">
								                                          <div class="card" style="min-height:33vh;">
								                                           <div class="card-header" id="cardHeaderStyle">
								                                           <span class="font-sz-level7">Manager : ${LOGGED_USER_INFO.FNA_ADVSTF_MGRNAME}</span>
								                                           
								                                           </div>
								                                           <div class="card-body text-center">
								                                          <div id="mgr">
  																			 <div id="mgrDiagram" style="border: solid 1px black; width: 100%; height: 250px"></div>
  																		   	<div id="buttons"><button onclick="mgrClearDiagram('mgrSavedDiagram');">Clear</button>
																			    <button onclick="mgrSave();">Save</button>
																			    
																		  
																		  </div>
																		  <form name="mgrform">
																		   <input type="hidden" id="mgrSignId" name="mgrSignId">
																		   <input type="hidden" id="mgrLoad" name="mgrLoad" value="Y">
																		  <textarea id="mgrSavedDiagram" name="mgrSavedDiagram" style="width:100%;height:300px;display:none">
																		{ "position": "0 0",
																		  "model": { "class": "GraphLinksModel",
																		  "nodeDataArray": [],
																		  "linkDataArray": []} }
																		  </textarea>
																		  </form>
								                                      </div>  
								                                      </div> 
								                                      </div>
								                                </div>
								                            </div>
								                        </div>
								                    </div>
                                             </div>
									    </div>
									</div>
									<!-- End Signpad -->  
   

</div> --%>
                                   
                                   
                                   
                               </div>

          
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
         <button type="button" class="btn btn-bs3-success font-sz-level5" onclick="closeScanToSign()">OK</button>
         <button type="button" class="btn btn-bs3-warning font-sz-level5" onclick="cancelScanToSign()">Cancel</button>
        </div>
        
       </div>
    </div>
</div>
</div>
 <!--Mail popup  -->
  
  
   
