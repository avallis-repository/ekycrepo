<%@ page language="java" import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%ResourceBundle Ekycprop =ResourceBundle.getBundle("AppResource");%>
<!DOCTYPE html>
<html lang="en">

<head>
  
   <link rel="stylesheet" href="vendor/avallis/css/tax-residency.css">
 

<body id="page-top">
  	 <!-- Container Fluid-->
       
	     <div class="container-fluid" id="container-wrapper" style="min-height:75vh">
		
		
		<div class="d-sm-flex align-items-center justify-content-between mb-1">
		    <div class="col-5">
			 <h6>Tax Residency Declaration</h6>
			</div>
			
	        <div class="col-md-7">
	           <jsp:include page="/views/pages/policyE-Submission.jsp"></jsp:include>
			 </div>
       </div>
       
        <div class="row">
               <jsp:include page="/views/pages/signApproveStatus.jsp"></jsp:include>
              </div>

<!--Page 11 content area Start  -->
			      <div class="card mb-4 " style="border:1px solid #044CB3;min-height:65vh" id="fnaTaxForm">
			      <input type="hidden" name="fnaId" id="fnaId" value="${CURRENT_FNAID}" size="20">
			      <input type="hidden" name="dataformId" id="dataformId" value="${CURRENT_DATAFORM_ID}"/>
						<div class="card-body" style="font-size: 13px;">
						      <div class="row">
                                   <div class="col-md-12">
					                           <div class="card">
					                           
					                            <div class="card-header" id="cardHeaderStyle-22" style="font-weight: normal;">
															        <div class="row">
															          <div class="col-md-12">
																	    <span> U.S Tax Declaration Under Foreign Account Tax Compliance Act (FATCA)</span>
																
																	  </div>
															          
															        </div>
												</div>
					                              <div class="card-body">
					                                  <div class="row">
					                                  
					                                   <div class="col-md-6">
						          <div class="card ">
						                <div class="card-header" id="cardHeaderStyle"><img src="vendor/avallis/img/client1.png">&nbsp;Client(1)</div>
						                 <div class="card-body">
						                 
						                      <div class="row">
						                              <div class="col-md-12">
						                                  <!--  -->
						                                     <form class="bs-example" action="">
						                                     <input type="hidden" id="hdnSessDataFormId" value="${DATA_FORM_ID}"/>
						                                    
  <div class="panel-group" id="accordionTax">
    <div class="panel panel-default">
      <div class="panel-heading">
      
       <h6 class="panel-title">
           <span class="custom-control custom-checkbox">
			  <input type="checkbox" onclick="chngChktoRadFATCA(this,'client');"  id="dfSelfUsnotifyflg1" name="dfSelfUsnotifyflg" 
			  class="custom-control-input checkdb dfSelfUsnotifyflggrp"  maxlength="1">
			  <label class="custom-control-label font-sz-level6 pt-1  text-custom-color-gp" for="dfSelfUsnotifyflg1">&nbsp;&nbsp;I am  <span class="bold text-primaryy txt-urline">Not</span> a U.S Person
			  <a data-toggle="collapse" data-parent="#accordionTax" href="#collapseSec1"></a>
			  </label>
          </span>
       </h6>
       
      </div>
      <div id="collapseSec1" class="panel-collapse collapse in hide">
        <div class="panel-body">
             <div class="row">
				<div class="col-md-12">
				  <div class="pl-4 ml-4 divStyle-01">
					<span class="font-sz-level7"> <strong class="text-primaryy font-sz-level7  italic">I hereby confirm that:</strong><br>
	                       I am not a U.S. Person and I am not acting for / on behalf of a U.S. Person / U.S. Indicia .
	                       If my tax status change and i become a U.S. Person, I shall notify Avallis Financial within 30 days from the date of change and provide the necessary documents? 
	                </span>
	              </div>
				</div>
			</div>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
          <h6 class="panel-title">
           <span class="custom-control custom-checkbox">
			  <input type="checkbox" onclick="chngChktoRadFATCA(this,'client')"
				   id="dfSelfUspersonflg1" name="dfSelfUspersonflg"   class="custom-control-input checkdb dfSelfUsnotifyflggrp" maxlength="1">
			  <label class="custom-control-label font-sz-level6 pt-1  text-custom-color-gp" for="dfSelfUspersonflg1">&nbsp;&nbsp;I am a <span class="bold text-primaryy txt-urline">U.S</span>&nbsp;(&nbsp;<img src="vendor/avallis/img/americaflag.png">&nbsp;)&nbsp;Person
			  <a data-toggle="collapse" data-parent="#accordionTax" href="#collapseSec2"></a>
			  </label>
          </span>
       </h6>
      
       
      </div>
      <div id="collapseSec2" class="panel-collapse collapse hide">
        <div class="panel-body">
           <div class="row">
				 <div class="col-md-12">
				     <div class="pl-4 ml-4 divStyle-01">
					 <span class="font-sz-level7"><strong class="text-primaryy font-sz-level7 italic">I hereby confirm that:</strong><br> I am a U.S Person and i have Submitted the Completed Form <span class="text-info">W -9<sup>1</sup></span> </span>
							<div class="row">
								<div class="col-md-12">
									
										 <div class="form-group">
											 <label for="tdUSClntLblTxt" class="frm-lable-fntsz" id="tdUSClntLblTxt">TIN of Client(1)</label>
											 <input type="text" class="form-control checkdb" name="dfSelfUstaxno" id="dfSelfUstaxno" maxlength="60">
										</div>
									
								</div>
							</div>
					  </div>
				 </div>
			</div>  
        </div>
      </div>
    </div>
  </div></form>
                  </div>
						                         
						                         </div>
						                      
						                 </div>  
						           </div>
						     </div>
						     
						      <div class="col-md-6">
						          <div class="card ">
						                <div class="card-header" id="cardHeaderStyle"><img src="vendor/avallis/img/client2.png">&nbsp;Client(2)</div>
						                 <div class="card-body">
						                 
						                 <div class="row">
						                              <div class="col-md-12">
						                                  <!--  -->
						                                     <form class="bs-example" action="">
  <div class="panel-group" id="accordionTaxi">
    <div class="panel panel-default">
      <div class="panel-heading">
      
       <h6 class="panel-title">
           <div class="custom-control custom-checkbox">
			  <input type="checkbox" onclick="chngChktoRadFATCA(this,'spouse');"  id="dfSpsUsnotifyflg1" name="dfSpsUsnotifyflg"
			   class="custom-control-input checkdb dfSpsUsnotifyflggrp"  maxlength="1">
			  <label class="custom-control-label font-sz-level6 pt-1  text-custom-color-gp" for="dfSpsUsnotifyflg1">&nbsp;&nbsp;I am  <span class="bold text-primaryy txt-urline">Not</span>  a U.S Person
			         <a data-toggle="collapse" data-parent="#accordionTaxi" href="#collapseSec3"></a>
			  </label>
          </div>
       </h6>
        
      </div>
      <div id="collapseSec3" class="panel-collapse collapse in hide">
        <div class="panel-body">
             <div class="row">
				<div class="col-md-12">
				     <div class="pl-4 ml-4 divStyle-01">
					<span class="font-sz-level7"> <strong class="text-primaryy font-sz-level7 italic">I hereby confirm that:</strong><br>
	                       I am not a U.S. Person and I am not acting for / on behalf of a U.S. Person / U.S. Indicia .
	                       If my tax status change and i become a U.S. Person, I shall notify Avallis Financial within 30 days from the date of change and provide the necessary documents? 
	                </span>
	                </div>
				</div>
			</div>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h6 class="panel-title">
           <div class="custom-control custom-checkbox">
			  <input type="checkbox" onclick="chngChktoRadFATCA(this,'spouse')"
			  id="dfSpsUspersonflg1" name="dfSpsUspersonflg" class="custom-control-input checkdb dfSpsUsnotifyflggrp" maxlength="1">
			  <label class="custom-control-label font-sz-level6 pt-1  text-custom-color-gp" for="dfSpsUspersonflg1">&nbsp;&nbsp;I am a <span class="bold text-primaryy txt-urline">U.S</span>&nbsp;(&nbsp;<img src="vendor/avallis/img/americaflag.png">&nbsp;)&nbsp;Person
			  <a data-toggle="collapse" data-parent="#accordionTaxi" href="#collapseSec4"></a>
			  </label>
          </div>
       </h6>
       
      </div>
      <div id="collapseSec4" class="panel-collapse collapse hide">
        <div class="panel-body">
             <div class="row">
								<div class="col-md-12">
								    <div class="pl-4 ml-4 divStyle-01">
									 <span class="font-sz-level7"><strong class="text-primaryy font-sz-level7 italic">I hereby confirm that:</strong><br> I am a U.S Person and i have Submitted the Completed Form <span class="text-info">W -9<sup>1</sup></span> </span>
											<div class="row">
												  <div class="col-md-12">
														<div class="form-group">
																<label for="tdUSSpsLblTxt" class="frm-lable-fntsz" id="tdUSSpsLblTxt">TIN of Client(2)</label>
																<input type="text" class="form-control checkdb"  name="dfSpsUstaxno" id="dfSpsUstaxno" maxlength="60">
														</div>
														
												  </div>
										
											</div>
									</div>
							 </div>
			</div>
        </div>
      </div>
    </div>
  </div></form>

						                                  
						                                  
						                              </div>
						                         
						                         </div>
						         
						                 
						                 </div>  
						           </div>
						     </div>
												       
												  </div>
												  
												  <div class="row mt-2">
																	            <div class="col-md-12">
																	             <span class="text-primaryy bold font-sz-level8"><span><i>1 Form W-9 / Form W-8BEN / Form W-8BENE can be obtained from http://www.irs.gov</i></span></span>
																	            </div>
															              </div>
												</div>
										</div>
								</div>
								
							</div><!--Sec 1 End  -->
							
							<!--Sec 2 start  -->
							      <div class="row">
                                   <div class="col-md-12">
                                   	           <div class="card" style="margin-top: 15px;">
					                           
					                            <div class="card-header" id="cardHeaderStyle-22" style="font-weight: normal;">
															        <div class="row">
															          <div class="col-md-12">
																	    <span class="">Declaration for Common Reporting Standard (CRS)</span><span class="font-sz-level6"> [Please provide information on your Tax Residency.]</span>
																
																	  </div>
															          
															        </div>
															        
															       
												</div>
					                              <div class="card-body">
					                                  <div class="row">
					                                      <div class="col-md-12">
					                                      <span id="span_dfSelfUstaxno"></span>	
                                   	<span id="span_dfSpsUstaxno"></span>
					                
					                                           <span class="font-sz-level6">For more information on Tax Residency &amp; Tax Identification Number, please refer to <span class="text-info bold">OECD</span> website:&nbsp;</span>
					                                            
					                                            <span><i class="fa fa-external-link" aria-hidden="true"></i> <a class="bold btn-link italic font-sz-level7" target="_blank" href="http://www.oecd.org/tax/automatic-exchange/crs-implementation-and-assistance/tax-residency/">Tax Residency</a>&nbsp;&nbsp;|&nbsp;&nbsp;</span>
					                                            <span><i class="fa fa-external-link" aria-hidden="true"></i> <a class="bold btn-link italic font-sz-level7" target="_blank" href="https://www.oecd.org/tax/automatic-exchange/crs-implementation-and-assistance/tax-identification-numbers/">Tax Identification Number</a></span>
					                                     
					                                      </div>
					                                  </div>
					                                  
					                                 
														<div class="tab-regular mt-2" >
															 <ul class="nav nav-tabs" id="selfSpouseTabs" role="tablist"> </ul>
															 	<div class="tab-content"  id="Taxcontent">
																</div>
														</div>
                                          			 </div><!--card-body -end  -->
							
							
							                 </div>
							       </div>
							       
							    </div><!-- Sec 2 end -->
							    
							    <!-- Sec 3 Start -->
							    
							     <div class="row">
                                   <div class="col-md-12">
					                           <div class="card card-content-fnsize" style="margin-top: 15px;">
					                           
					                            <div class="card-header" id="cardHeaderStyle-22" style="font-weight: normal;">
															        <div class="row">
															          <div class="col-md-12">
																	    <span>Declaration</span>
																
																	  </div>
															        
															        </div>
															        
															       
												</div>
					                              <div class="card-body">
					                                 <div class="row">
					                                      <div class="col-md-12">
					                                      
					                                      <ul>
															  <li class="mt-1"><span class="font-sz-level6 ">I confirm that I am not a tax resident of any country(ies) other than the one(s) that I have declared above.</span></li>
															   <li class="mt-1"><span class="font-sz-level6 ">I declare that the information provided on this form is to the best of my knowledge and belief, accurate and complete.</span></li>
															   <li class="mt-1"><span class="font-sz-level6 ">I agree to notify and provide assistance Avallis Financial Pte Ltd immediately in the event of changes to the information I have declared for it to comply with the relevant tax regulations.</span></li>
															   <li class="mt-1"><span class="font-sz-level6 ">I agree that Avallis Financial Pte Ltd can supply any of such information or documentation to any relevant tax authority to the extent required under FATCA and CRS and its implementing regulations and applicable intergovernmental agreements.</span></li>
                                                          </ul>
					                                        
					                                       
					                                      
					                                      </div>
					                                 
					                                 </div>
					                              
					                              
					                              </div>
							
							
							                 </div>
							       </div>
							       
							    </div><!-- Sec 3 end -->
				  </div>
	  
	          </div><!--  Container fluid end-->
	          
	  
      
      
  
   </div>
	  
	   
      <!--  End Container Fluid-->    
          
          
        <!--Add  Form Details Popup Page    -->   
          
<div class="modal fade" id="addModalFormDetls" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" 
data-backdrop="static" data-keyboard="false" >
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header" id="cardHeaderStyle-22">
        <h5 class="modal-title" id="exampleModalLabel" >Tax Residency Form Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <input type="hidden" name="taxId" id="sessTaxId">
      </div>
      <div class="modal-body">
        <div class="card">
             <div class="card-body">
                  <div class="row">
                       <div class="col-md-12">
                          <div class="form-group">
                              <label for="exampleInputPassword1" class="badge">Country of Tax Residence</label>
						               <select id="country" name="taxCountry" 
						               class="form-control checkdb" maxlength="60">
                                      <option selected="true" value="">--Select--</option>
                                       <% for(String country:Ekycprop.getString("app.country").split("\\^")){ %>
									    	<option value="<%=country.toUpperCase()%>"><%=country%></option>
									     <%}%>
               
                                    </select>
						  </div>
                       </div>
                       <div class="col-md-12">
                             <div class="form-group">
						    <label for="exampleInputPassword1" class="badge">Tax Identification Number (TIN)</label>
						    <input type="text" class="form-control checkdb" id="taxRefno"
						     name="taxRefno" maxlength="20">
						  </div>
                       </div>
                  </div>
                  
                  <div class="row">
                       <div class="col-md-12">
                              <div class="form-group">
						    <label for="reasonToNotax" class="badge">If no TIN available,enter <span class="text-secondary" >Reason A <i class="fa fa-info-circle" id="noteReasonA" aria-hidden="true" data-toggle="ReasonAPopup" data-trigger="hover"></i></span> 
						    
						    |<span  class="text-primaryy"> Reason B <i class="fa fa-info-circle" id="noteReasonB" aria-hidden="true" data-toggle="ReasonBPopup" data-trigger="hover"></i></span>
						     |<span class="text-info"> Reason C <i class="fa fa-info-circle" id="noteReasonC" aria-hidden="true" data-toggle="ReasonCPopup" data-trigger="hover"></i></span></label>
						    <select class="form-control checkdb"  id="reasonToNotax" name="reasonToNotax"
						     onchange="enableReasonDets(this);" maxlength="20" disabled>
						        <option selected="true" value="">--Select--</option>
						        <option value="Reason A">Reason A</option>
						        <option value="Reason B">Reason B</option>
						        <option value="Reason C">Reason C</option>
						    </select>
						  </div>
                       </div>
                       <div class="col-md-12">
                             <div class="form-group">
						    <label for="exampleInputPassword1" class="badge">Please state reason(s) 
                             if Reason B is selected</label>
						    <textarea class="form-control txtarea-hrlines text-wrap checkdb" rows="5" id="reasonbDetails" name="reasonbDetails"
						    maxlength="800" onkeydown="textCounter(this,800);" onkeyup="textCounter(this,800);" disabled></textarea>
						    <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
						  </div>
                       </div>
                  </div>
             </div>
        
        </div>
        <!--set self and spouse hidden field values  -->
        <input type="hidden" id="hdnSelfNric">
        <input type="hidden" id="hdnSpsNric">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-bs3-prime " style="background: green;border:1px solid green;"  data-dismiss="modal" onclick="saveTaxDets();">Add detls.</button>
        <button type="button" class="btn btn-sm btn-bs3-prime " data-dismiss="modal" style="background: grey;border:1px solid grey;">Close</button>
        
      </div>
    </div>
  </div>
</div>
        <!-- End Tax Resi.Form Details Page --> 
        
        <!-- Popover contents Tax Residency  each card notes content here -->


<div id="popover-ReasonAPopup" style="display: none">
  <ul class="list-group custom-popover">
    <li class="list-group-item"><i class="fa fa-sticky-note-o" aria-hidden="true" id="i-icon-note-style"></i>&nbsp;<span class="text-secondary"><strong>Reason A</strong></span> - The country where you are liable to pay tax does not issue TINs to its residents.</li>
    
  </ul>
</div>

<div id="popover-ReasonBPopup" style="display: none">
  <ul class="list-group custom-popover">
    <li class="list-group-item"><i class="fa fa-sticky-note-o" aria-hidden="true" id="i-icon-note-style"></i>&nbsp;<span class="text-primaryy"><strong>Reason B</strong></span> -  You are unable to obtain a TIN or equivalent number (Please explain in the above table if you have selected this reason).</li>
    
    
  </ul>
</div>

<div id="popover-ReasonCPopup" style="display: none">
  <ul class="list-group custom-popover">
    <li class="list-group-item"><i class="fa fa-sticky-note-o" aria-hidden="true" id="i-icon-note-style"></i>&nbsp;<span class="text-info"><strong>Reason C</strong></span> - No TIN is required. (Note: Only select this reason if the authorities of the country of tax residence entered above do not require a TIN to be disclosed). </li>
    
  </ul>
</div><!-- page 11 tax Resi Reason Popup Notes End -->
	  
   
 </body>
  
          <script type="text/javascript" src="vendor/avallis/js/tax_residency.js"></script>
          
            <script>jsnDataFnaDetails = ${CURRENT_FNA_DETAILS}</script>
            
          <script>jsnDataSelfSpsDetails = ${SELFSPOUSE_DETAILS}</script>
          
 </html>
                
