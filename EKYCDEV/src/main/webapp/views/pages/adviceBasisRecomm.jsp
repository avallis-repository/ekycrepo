<!DOCTYPE html>
<html lang="en">



<body id="page-top">
  		<!-- Container Fluid-->
       <div class="container-fluid" id="container-wrapper" style="min-height:75vh">
		<div id="frmBasisOfRecomForm">
		
		<input type="hidden" name="fnaId" id="fnaId" value="${CURRENT_FNAID}" >
		<input type="hidden" name="dataformId" id="dataformId" value="${CURRENT_DATAFORM_ID}"/>
		
		<div class="d-sm-flex align-items-center justify-content-between mb-1">
		 
	       <div class="col-5">
		       <h6>Advice &amp; Basis of Recommendations.</h6>
		  </div>
		  <div class="col-md-7">
		           <jsp:include page="/views/pages/policyE-Submission.jsp"></jsp:include>
		  </div>
       
         </div>
		  
		  <div class="row">
              <jsp:include page="/views/pages/signApproveStatus.jsp"></jsp:include>
         </div>
		 
		<div class="card mb-4 " style="border:1px solid #044CB3;min-height:65vh">
		
		    <div class="card-body">
			   
			    <div class="row">
			          
			        <div class="col-12">
			           <!--  -->
			               <div class="tab-regular mt-2" id="">
							      <ul class="nav nav-tabs" id="" role="tablist">
							            <li class="nav-item"> <a class="nav-link active" id="Life-tab" data-toggle="tab" href="#Life" role="tab" aria-controls="Life" aria-selected="true">Life</a> </li>
							             <li class="nav-item d-none"> <a class="nav-link" id="UT-tab" data-toggle="tab" href="#UT" role="tab" aria-controls="UT" aria-selected="false">UT</a> </li>
                                  </ul>
                                  <div class="tab-content" id="">
								        <div class="tab-pane fade show active" id="Life" role="tabpanel" aria-labelledby="Life-tab">
								              <div class="card mb-1  p-1 m-1 " id="">
													<div class="card-body" style="border:1px solid #ddd;" id="">
														 <div class="row">
													          <div class="col-md-6">
													            <div class="card" id="card-btomspceStyle">
															            <div class="card-header" id="cardHeaderStyle-22" style="font-weight:normal;">
															               <div class="row">
															                  <div class="col-md-9"><i class="fa fa-user" aria-hidden="true" style="color:;"></i>&nbsp;<span class="font-sz-level5">Client's Objective(s)</span></div>
															                  <div class="col-md-2">
																			      <span class="badge badge-pill badge-warning">Note:&nbsp;<i class="fa fa-commenting Note-iconStyle" id="Note" aria-hidden="true" data-toggle="popover1" data-trigger="hover" data-original-title="" title=""></i></span>
																			      </div>
															                  <div class="col-md-1"><span><i class="fa fa-eye viewRemarkContLIFE ml-n2" id="iconViewStyle" aria-hidden="true" title="click to view Client's Objective(s)" data-toggle="modal" data-target="#txtAreaModal1LIFE"></i></span></div>
															                </div>
															            </div>
															            <div class="card-body">
															                  <div class="row">
															                   <div class="col-md-12">
															                         <div class="form-group">
																						   <textarea class="form-control txtarea-hrlines text-wrap checkdb" name="advrecReason" maxlength="3950"
																						    id="advrecReason" onkeydown="textCounter(this,3950);" onkeyup="textCounter(this,3950);" rows="5"></textarea>
																				           <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
																				     </div>
															                      </div>
															              </div>
															            </div>
													            </div>

												             <div class="card">
												                  <div class="card-header" id="cardHeaderStyle-22" style="font-weight:normal;">
										                             <div class="row">
												                        <div class="col-md-9"><i class="fa fa-thumb-tack" aria-hidden="true" style="color: ;"></i>&nbsp;<span class="font-sz-level5">Risk / Limitation(s) of Product(s)</span></div>
												                        <div class="col-md-2">
																            <span class="badge badge-pill badge-warning">Note:&nbsp;<i class="fa fa-commenting Note-iconStyle" id="Note1" aria-hidden="true" data-toggle="popover2" data-trigger="hover" data-original-title="" title=""></i></span>
																        </div>
												                        <div class="col-md-1"><span><i class="fa fa-eye viewRemarkContLIFE ml-n2" id="iconViewStyle" aria-hidden="true" title="click to view Risk / Limitation(s) of Product(s)" data-toggle="modal" data-target="#txtAreaModal2LIFE"></i></span></div>
												                    </div>
												            </div>
										                    <div class="card-body">
												                 <div class="row">
												                   <div class="col-md-12">
												                         <div class="form-group">
																			  <textarea class="form-control txtarea-hrlines text-wrap checkdb" name="advrecReason2" id="advrecReason2" maxlength="3950" onkeydown="textCounter(this,3950);" onkeyup="textCounter(this,3950);" rows="5"></textarea>
																	          <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
																	     </div>
												                      </div>
												                 </div>
												            </div>
												            
												            </div>
												           </div>
		          
		          
		          
		                                   <div class="col-md-6">
										        <div class="card" id="card-btomspceStyle">
										              <div class="card-header" id="cardHeaderStyle-22" style="font-weight:normal;">
								
														<div class="row">
														    <div class="col-md-9"><i class="fa fa-tags" aria-hidden="true" style="color:;"></i>&nbsp;<span class="font-sz-level5">Reason(s) for Recommendations</span> </div>
														      <div class="col-md-2">
														      <span class="badge badge-pill badge-warning">Note:&nbsp;<i class="fa fa-commenting Note-iconStyle" id="Note2" aria-hidden="true" data-toggle="popover3" data-trigger="hover" data-original-title="" title=""></i></span>
														      </div>
														    <div class="col-md-1"><span><i class="fa fa-eye viewRemarkContLIFE ml-n2" id="iconViewStyle" aria-hidden="true" title="click to view Reason(s) for Recommendations" data-toggle="modal" data-target="#txtAreaModal3LIFE"></i></span></div>
														  </div>
														
								                       </div>
										                <div class="card-body">
										                    <div class="row">
										                   <div class="col-md-12">
										                         <div class="form-group">
																	  <textarea class="form-control txtarea-hrlines text-wrap checkdb" name="advrecReason1" maxlength="3950" id="advrecReason1" onkeydown="textCounter(this,3950);" onkeyup="textCounter(this,3950);" rows="5"></textarea>
															          <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
															     </div>
										                      </div>
										              </div>
										            </div>
										       </div>
		            
		                                      <div class="card">
									              <div class="card-header" id="cardHeaderStyle-22" style="font-weight:normal;">
							
													<div class="row">
													     <div class="col-md-9">
													      <i class="fa fa-question-circle" aria-hidden="true" style="color:;"></i>&nbsp;<span class="font-sz-level5">Reason(s) for Deviation(s)</span>
													     </div>
													       <div class="col-md-2">
													      <span class="badge badge-pill badge-warning">Note:&nbsp;<i class="fa fa-commenting Note-iconStyle" id="Note3" aria-hidden="true" data-toggle="popover4" data-trigger="hover" data-original-title="" title=""></i></span>
													      </div>
													      <div class="col-md-1"><span><i class="fa fa-eye viewRemarkContLIFE ml-n2" aria-hidden="true" id="iconViewStyle" title="click to view Reason(s) for Deviation(s)" data-toggle="modal" data-target="#txtAreaModal4LIFE"></i></span></div>
													</div>
													   </div>
                                                   <div class="card-body">
									                <div class="row">
									                   <div class="col-md-12">
									                         <div class="form-group">
																  <textarea class="form-control txtarea-hrlines text-wrap checkdb" name="advrecReason3" id="advrecReason3" maxlength="3950" onkeydown="textCounter(this,3950);" onkeyup="textCounter(this,3950);" rows="5"></textarea>
														          <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
														     </div>
									                      </div>
									              </div>
									            </div>
		                                   </div>
		                              </div>
		                          </div>
													</div>
											  </div>
                                          </div><!-- Life Tab Content End -->
                                                                 
                                           <div class="tab-pane fade d-none" id="UT" role="tabpanel" aria-labelledby="UT-tab">
								                 <div class="card mb-1  p-1 m-1 " id="">
													  <div class="card-body" style="border:1px solid #ddd;" id="">
															 <div class="row">
													          <div class="col-md-6">
													            <div class="card" id="card-btomspceStyle">
															            <div class="card-header" id="cardHeaderStyle-22" style="font-weight:normal;">
															               <div class="row">
															                  <div class="col-md-9"><i class="fa fa-user" aria-hidden="true" style="color:;"></i>&nbsp;<span class="font-sz-level5">Client's Objective(s)</span></div>
															                  <div class="col-md-2">
																			      <span class="badge badge-pill badge-warning">Note:&nbsp;<i class="fa fa-commenting Note-iconStyle" id="NoteDummy" aria-hidden="true" data-toggle="popover1" data-trigger="hover" data-original-title="" title=""></i></span>
																			      </div>
															                  <div class="col-md-1"><span><i class="fa fa-eye viewRemarkContUT" id="iconViewStyle" aria-hidden="true" title="click to view Client's Objective(s)" data-toggle="modal" data-target="#txtAreaModal1UT"></i></span></div>
															                </div>
															            </div>
															            <div class="card-body">
															                  <div class="row">
															                   <div class="col-md-12">
															                         <div class="form-group">
																						  <textarea class="form-control txtarea-hrlines text-wrap checkdb" name="advrecReasonDUMMY" id="advrecReasonDUMMY" maxlength="3950" onkeydown="textCounter(this,3950);" onkeyup="textCounter(this,3950);" rows="5"></textarea>
																				           <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
																				     </div>
															                      </div>
															              </div>
															            </div>
													            </div>

												             <div class="card">
												                  <div class="card-header" id="cardHeaderStyle-22" style="font-weight:normal;">
										                             <div class="row">
												                        <div class="col-md-9"><i class="fa fa-thumb-tack" aria-hidden="true" style="color: ;"></i>&nbsp;<span class="font-sz-level5">Risk / Limitation(s) of Product(s).</span></div>
												                        <div class="col-md-2">
																            <span class="badge badge-pill badge-warning">Note:&nbsp;<i class="fa fa-commenting Note-iconStyle" id="Note1Dummy" aria-hidden="true" data-toggle="popover2" data-trigger="hover" data-original-title="" title=""></i></span>
																        </div>
												                        <div class="col-md-1"><span><i class="fa fa-eye viewRemarkContUT" id="iconViewStyle" aria-hidden="true" title="click to view Risk / Limitation(s) of Product(s)" data-toggle="modal" data-target="#txtAreaModal2UT"></i></span></div>
												                    </div>
												            </div>
										                    <div class="card-body">
												                 <div class="row">
												                   <div class="col-md-12">
												                         <div class="form-group">
																			  <textarea class="form-control txtarea-hrlines text-wrap checkdb" name="advrecReason2DUMMY" id="advrecReason2DUMMY" maxlength="3950" onkeydown="textCounter(this,3950);" onkeyup="textCounter(this,3950);" rows="5"></textarea>
																	          <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
																	     </div>
												                      </div>
												                 </div>
												            </div>
												            
												            </div>
												           </div>
		          
		          
		          
		                                   <div class="col-md-6">
										        <div class="card" id="card-btomspceStyle">
										              <div class="card-header" id="cardHeaderStyle-22" style="font-weight:normal;">
								
														<div class="row">
														    <div class="col-md-9"><i class="fa fa-tags" aria-hidden="true" style="color:;"></i>&nbsp;<span class="font-sz-level5">Reason(s) for Recommendations.</span> </div>
														      <div class="col-md-2">
														      <span class="badge badge-pill badge-warning">Note:&nbsp;<i class="fa fa-commenting Note-iconStyle" id="Note2Dummy" aria-hidden="true" data-toggle="popover3" data-trigger="hover" data-original-title="" title=""></i></span>
														      </div>
														    <div class="col-md-1"><span><i class="fa fa-eye viewRemarkContUT" id="iconViewStyle" aria-hidden="true" title="click to view Reason(s) for Recommendations" data-toggle="modal" data-target="#txtAreaModal3UT"></i></span></div>
														  </div>
														
								                       </div>
										                <div class="card-body">
										                    <div class="row">
										                   <div class="col-md-12">
										                         <div class="form-group">
																	  <textarea class="form-control txtarea-hrlines text-wrap checkdb" name="advrecReason1DUMMY" id="advrecReason1DUMMY"  maxlength="3950" onkeydown="textCounter(this,3950);" onkeyup="textCounter(this,3950);" rows="5"></textarea>
															          <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
															     </div>
										                      </div>
										              </div>
										            </div>
										       </div>
		            
		                                      <div class="card">
									              <div class="card-header" id="cardHeaderStyle-22" style="font-weight:normal;">
							
													<div class="row">
													     <div class="col-md-9">
													      <i class="fa fa-question-circle" aria-hidden="true" style="color:;"></i>&nbsp;<span class="font-sz-level5">Reason(s) for Deviation(s)</span>
													     </div>
													       <div class="col-md-2">
													      <span class="badge badge-pill badge-warning">Note:&nbsp;<i class="fa fa-commenting Note-iconStyle" id="Note3Dummy" aria-hidden="true" data-toggle="popover4" data-trigger="hover" data-original-title="" title=""></i></span>
													      </div>
													      <div class="col-md-1"><span><i class="fa fa-eye viewRemarkContUT" aria-hidden="true" id="iconViewStyle" title="click to view Reason(s) for Deviation(s)" data-toggle="modal" data-target="#txtAreaModal4UT"></i></span></div>
													</div>
													   </div>
                                                   <div class="card-body">
									                <div class="row">
									                   <div class="col-md-12">
									                         <div class="form-group">
																  <textarea class="form-control txtarea-hrlines text-wrap checkdb" name="advrecReason3DUMMY" id="advrecReason3DUMMY" maxlength="3950" onkeydown="textCounter(this,3950);" onkeyup="textCounter(this,3950);" rows="5"></textarea>
														          <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
														     </div>
									                      </div>
									              </div>
									            </div>
		                                   </div>
		                              </div>
		                          </div>
														</div>
												</div>
                                            </div><!--  UT Tab Content End -->
                                            
                                   </div><!-- Tab Content Area End -->
                                                         
                            </div><!-- tab Regular end -->
			           
			           <!--  -->
			        </div>
			        
			    </div>
		    
		    
		    
		    
		    
		    
		    
		    
		    </div>
		    <div class="card-footer"></div>
		
       
      </div>
      
      </div>
   </div>

<!-- Popover contents Advice Basis Recomm.  each card notes content here -->

<div id="popover-contentPg10CardNotes1" style="display: none">
  <ul class="list-group custom-popover">
    <li class="list-group-item"><i class="fa fa-sticky-note-o" aria-hidden="true" id="i-icon-note-style"></i>&nbsp;Client's concern and objectives, time horizon, where applicable.</li>
    
  </ul>
</div>

<div id="popover-contentPg10CardNotes2" style="display: none">
  <ul class="list-group custom-popover">
    <li class="list-group-item"><i class="fa fa-sticky-note-o" aria-hidden="true" id="i-icon-note-style"></i>&nbsp;How the plan meets client's need(s).</li>
    <li class="list-group-item"><i class="fa fa-sticky-note-o" aria-hidden="true" id="i-icon-note-style"></i>&nbsp;Features and benefits of selected product.</li>
    
  </ul>
</div>

<div id="popover-contentPg10CardNotes3" style="display: none">
  <ul class="list-group custom-popover">
    <li class="list-group-item"><i class="fa fa-sticky-note-o" aria-hidden="true" id="i-icon-note-style"></i>&nbsp;Possible risk(s) relating to
product(s) sold.</li>
    <li class="list-group-item"><i class="fa fa-sticky-note-o" aria-hidden="true" id="i-icon-note-style"></i>&nbsp;Possible disadvantage(s)/
limitation / Exclusion /</li>
    <li class="list-group-item"><i class="fa fa-sticky-note-o" aria-hidden="true" id="i-icon-note-style"></i>&nbsp;Warning based on
circumstances disclosed by client.</li>
  </ul>
</div>

<div id="popover-contentPg10CardNotes4" style="display: none">
  <ul class="list-group custom-popover">
    <li class="list-group-item"><i class="fa fa-sticky-note-o" aria-hidden="true" id="i-icon-note-style" ></i>&nbsp;Premiums / Investment
amount are more than client's budget.</li>
    <li class="list-group-item"><i class="fa fa-sticky-note-o" aria-hidden="true" id="i-icon-note-style"></i>&nbsp;Product(s) recommended
are of higher risk than client's risk preference.</li>
    <li class="list-group-item"><i class="fa fa-sticky-note-o" aria-hidden="true" id="i-icon-note-style"></i>&nbsp;Client choice of product(s)
differs from product(s) recommended by Representative.</li>
    
  </ul>
</div>


<!--page 10 each card sec text area model start  -->

<!-- The Modal -->
<div class="modal" id="txtAreaModal1LIFE" >
  <div class="modal-dialog">
    <div class="modal-content w-600">

      <!-- Modal Header -->
      <div class="modal-header" id="cardHeaderStyle-22">
        <h6 class="modal-title"><i class="fa fa-user" aria-hidden="true" style="color:;"></i>&nbsp;Client's Objective(s)</h6>
        <button type="button" class="close setValuetoTxtAreaLIFE" data-dismiss="modal" style="color: #fff;">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
              <div class="col-md-12">
		           <div class="form-group">
						 <textarea class="form-control txtarea-hrlines text-wrap checkdb" rows="15" cols="450" name="advrecReason_pop" id="advrecReason_pop" onkeydown="textCounter(this,3950);" maxlength="3950" onkeyup="textCounter(this,3950);"></textarea>
				         <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
				   </div>
		     </div>
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
          
        
       <div class="row">
              <div class="col-md-4" style="border-right: 1px solid grey;">
              
              <button type="button" class="btn btn-link btn-sm text-wrap checkdb setValuetoTxtAreaLIFE" data-toggle="modal" data-target="#txtAreaModal3LIFE" data-dismiss="modal"  style="font-size: 11px;"  >Reason for recomm.</button>
              </div>
              
              <div class="col-md-3" style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm setValuetoTxtAreaLIFE" data-toggle="modal" data-target="#txtAreaModal2LIFE" data-dismiss="modal"  style="font-size: 11px;"  >Risk/Limit</button>
              </div>
              
              <div class="col-md-4 " style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm setValuetoTxtAreaLIFE" data-toggle="modal" data-target="#txtAreaModal4LIFE" data-dismiss="modal"  style="font-size: 11px;"  >Reason for deviate.</button>
              </div>
              
              <div class="col-md-1">
                <button type="button" class="btn btn-sm btn-bs3-prime setValuetoTxtAreaLIFE" data-dismiss="modal">OK</button>
              </div>
          </div>
      </div>

    </div>
  </div>
</div>


<div class="modal" id="txtAreaModal2LIFE">
  <div class="modal-dialog">
    <div class="modal-content w-600">

      <!-- Modal Header -->
      <div class="modal-header" id="cardHeaderStyle-22">
        <h6 class="modal-title"><i class="fa fa-thumb-tack" aria-hidden="true" style="color: ;"></i>&nbsp;Risk / Limitation(s) of Products.</h6>
        <button type="button" class="close setValuetoTxtAreaLIFE" data-dismiss="modal" style="color: #fff;">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
              <div class="col-md-12">
		           <div class="form-group">
						 <textarea class="form-control txtarea-hrlines text-wrap checkdb" rows="15" cols="450" maxlength="3950" name="advrecReason2_pop" id="advrecReason2_pop" onkeydown="textCounter(this,3950);" onkeyup="textCounter(this,3950);"></textarea>
				         <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
				   </div>
		     </div>
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
          
           <div class="row">
              
              
              <div class="col-md-3 " style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm setValuetoTxtAreaLIFE" data-toggle="modal" data-target="#txtAreaModal1LIFE" data-dismiss="modal"  style="font-size: 11px;"  >Client obj.</button>
              </div>
              
              <div class="col-md-4" style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm setValuetoTxtAreaLIFE" data-toggle="modal" data-target="#txtAreaModal3LIFE" data-dismiss="modal"  style="font-size: 11px;" >Reason for recomm.</button>
              </div>
              
              
              <div class="col-md-4" style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm setValuetoTxtAreaLIFE" data-toggle="modal" data-target="#txtAreaModal4LIFE" data-dismiss="modal"  style="font-size: 11px;"  >Reason for deviate</button>
              </div>
              
              
              <div class="col-md-1">
                <button type="button" class="btn btn-sm btn-bs3-prime setValuetoTxtAreaLIFE" data-dismiss="modal">OK</button>
              </div>
          </div>
        
       
      </div>

    </div>
  </div>
</div>

<div class="modal" id="txtAreaModal3LIFE">
  <div class="modal-dialog">
    <div class="modal-content w-600">

      <!-- Modal Header -->
      <div class="modal-header" id="cardHeaderStyle-22">
        <h6 class="modal-title"><i class="fa fa-tags" aria-hidden="true" style="color:;"></i>&nbsp;Reason(s)for Recommendations.</h6>
        <button type="button" class="close setValuetoTxtAreaLIFE" data-dismiss="modal" style="color: #fff;">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
              <div class="col-md-12">
		           <div class="form-group">
						 <textarea class="form-control txtarea-hrlines text-wrap checkdb" rows="15" cols="450" maxlength="3950" name="advrecReason1_pop" id="advrecReason1_pop" onkeydown="textCounter(this,3950);" onkeyup="textCounter(this,3950);"></textarea>
				         <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
				   </div>
		     </div>
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
      
            <div class="row">
              <div class="col-md-3" style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm setValuetoTxtAreaLIFE" data-toggle="modal" data-target="#txtAreaModal1LIFE" data-dismiss="modal"  style="font-size: 11px;"  >Client obj.</button>
              </div>
              
               <div class="col-md-4" style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm setValuetoTxtAreaLIFE" data-toggle="modal" data-target="#txtAreaModal2LIFE" data-dismiss="modal"  style="font-size: 11px;"  >Risk/Limit</button>
              </div>
              
              <div class="col-md-4 " style="border-right: 1px solid grey;">
               <button type="button" class="btn btn-link btn-sm setValuetoTxtAreaLIFE text-wrap checkdb" data-toggle="modal" data-target="#txtAreaModal4LIFE" data-dismiss="modal"  style="font-size: 11px;" >Reason for deviate.</button>
              </div>
              
             
              
              <div class="col-md-1">
                <button type="button" class="btn btn-sm btn-bs3-prime setValuetoTxtAreaLIFE" data-dismiss="modal">OK</button>
              </div>
          </div>
         
        
       
      </div>

    </div>
  </div>
</div>

<div class="modal" id="txtAreaModal4LIFE">
  <div class="modal-dialog">
    <div class="modal-content w-600">

      <!-- Modal Header -->
      <div class="modal-header" id="cardHeaderStyle-22">
        <h6 class="modal-title"> <i class="fa fa-question-circle" aria-hidden="true" style="color:;" ></i>&nbsp;Reason(s)for Deviation(s)</h6>
        <button type="button" class="close setValuetoTxtAreaLIFE" data-dismiss="modal" style="color: #fff;">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
              <div class="col-md-12">
		           <div class="form-group">
						 <textarea class="form-control txtarea-hrlines text-wrap checkdb" rows="15" cols="450" maxlength="3950" name="advrecReason3_pop" id="advrecReason3_pop" onkeydown="textCounter(this,3950);" onkeyup="textCounter(this,3950);"></textarea>
				         <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
				   </div>
		     </div>
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
          <div class="row">
              <div class="col-md-3" style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm setValuetoTxtAreaLIFE" data-toggle="modal" data-target="#txtAreaModal1LIFE" data-dismiss="modal"  style="font-size: 11px;" >Client obj.</button>
              </div>
              
              <div class="col-md-4 " style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm text-wrap checkdb setValuetoTxtAreaLIFE" data-toggle="modal" data-target="#txtAreaModal3LIFE" data-dismiss="modal"  style="font-size: 11px;"  >Reason for recomm.</button>
              </div>
              
              <div class="col-md-4" style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm setValuetoTxtAreaLIFE" data-toggle="modal" data-target="#txtAreaModal3LIFE" data-dismiss="modal" style="font-size: 11px;" >Risk/Limit</button>
              </div>
              
              <div class="col-md-1">
                <button type="button" class="btn btn-sm btn-bs3-prime setValuetoTxtAreaLIFE" data-dismiss="modal">OK</button>
              </div>
          </div>
       
      </div>

    </div>
  </div>
</div>


<div class="modal" id="txtAreaModal1UT" >
  <div class="modal-dialog">
    <div class="modal-content w-600">

      <!-- Modal Header -->
      <div class="modal-header" id="cardHeaderStyle-22">
        <h6 class="modal-title"><i class="fa fa-user" aria-hidden="true" style="color:;"></i>&nbsp;Client's Objective(s)</h6>
        <button type="button" class="close setValuetoTxtAreaUT" data-dismiss="modal" style="color: #fff;">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
              <div class="col-md-12">
		           <div class="form-group">
						 <textarea class="form-control txtarea-hrlines text-wrap checkdb" rows="15" cols="450" name="advrecReasonDUMMY_pop" id="advrecReasonDUMMY_pop" onkeydown="textCounter(this,3950);" maxlength="3950" onkeyup="textCounter(this,3950);"></textarea>
				         <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
				   </div>
		     </div>
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
          
        
       <div class="row">
              <div class="col-md-4" style="border-right: 1px solid grey;">
              
              <button type="button" class="btn btn-link btn-sm text-wrap checkdb setValuetoTxtAreaUT" data-toggle="modal" data-target="#txtAreaModal3UT" data-dismiss="modal"  style="font-size: 11px;"  >Reason for recomm.</button>
              </div>
              
              <div class="col-md-3" style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm setValuetoTxtAreaUT" data-toggle="modal" data-target="#txtAreaModal2UT" data-dismiss="modal"  style="font-size: 11px;"  >Risk/Limit</button>
              </div>
              
              <div class="col-md-4 " style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm setValuetoTxtAreaUT" data-toggle="modal" data-target="#txtAreaModal4UT" data-dismiss="modal"  style="font-size: 11px;"  >Reason for deviate.</button>
              </div>
              
              <div class="col-md-1">
                <button type="button" class="btn btn-sm btn-bs3-prime setValuetoTxtAreaUT" data-dismiss="modal">OK</button>
              </div>
          </div>
      </div>

    </div>
  </div>
</div>


<div class="modal" id="txtAreaModal2UT">
  <div class="modal-dialog">
    <div class="modal-content w-500">

      <!-- Modal Header -->
      <div class="modal-header" id="cardHeaderStyle-22">
        <h6 class="modal-title"><i class="fa fa-thumb-tack" aria-hidden="true" style="color: ;"></i>&nbsp;Risk / Limitation(s) of Products.</h6>
        <button type="button" class="close setValuetoTxtAreaUT" data-dismiss="modal" style="color: #fff;">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
              <div class="col-md-12">
		           <div class="form-group">
						 <textarea class="form-control txtarea-hrlines text-wrap checkdb" rows="15" cols="450" maxlength="3950" name="advrecReasonDUMMY2_pop" id="advrecReasonDUMMY2_pop" onkeydown="textCounter(this,3950);" onkeyup="textCounter(this,3950);"></textarea>
				         <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
				   </div>
		     </div>
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
          
           <div class="row">
              
              
              <div class="col-md-3 " style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm setValuetoTxtAreaUT" data-toggle="modal" data-target="#txtAreaModal1UT" data-dismiss="modal"  style="font-size: 11px;"  >Client Obj.</button>
              </div>
              
              <div class="col-md-4" style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm setValuetoTxtAreaUT" data-toggle="modal" data-target="#txtAreaModal4UT" data-dismiss="modal"  style="font-size: 11px;"  >Reason for deviate</button>
              </div>
              
              <div class="col-md-4" style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm setValuetoTxtAreaUT" data-toggle="modal" data-target="#txtAreaModal3UT" data-dismiss="modal"  style="font-size: 11px;" >Reason for recomm.</button>
              </div>
              
              <div class="col-md-1">
                <button type="button" class="btn btn-sm btn-bs3-prime setValuetoTxtAreaUT" data-dismiss="modal">OK</button>
              </div>
          </div>
        
       
      </div>

    </div>
  </div>
</div>

<div class="modal" id="txtAreaModal3UT">
  <div class="modal-dialog">
    <div class="modal-content w-500">

      <!-- Modal Header -->
      <div class="modal-header" id="cardHeaderStyle-22">
        <h6 class="modal-title"><i class="fa fa-tags" aria-hidden="true" style="color:;"></i>&nbsp;Reason(s)for Recommendations.</h6>
        <button type="button" class="close setValuetoTxtAreaUT" data-dismiss="modal" style="color: #fff;">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
              <div class="col-md-12">
		           <div class="form-group">
						 <textarea class="form-control txtarea-hrlines text-wrap checkdb" rows="15" cols="450" maxlength="3950" name="advrecReasonDUMMY1_pop" id="advrecReasonDUMMY1_pop" onkeydown="textCounter(this,3950);" onkeyup="textCounter(this,3950);"></textarea>
				         <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
				   </div>
		     </div>
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
      
            <div class="row">
              <div class="col-md-3" style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm setValuetoTxtAreaUT" data-toggle="modal" data-target="#txtAreaModal1UT" data-dismiss="modal"  style="font-size: 11px;"  >ClientObj</button>
              </div>
              
               <div class="col-md-4" style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm setValuetoTxtAreaUT" data-toggle="modal" data-target="#txtAreaModal2UT" data-dismiss="modal"  style="font-size: 11px;"  >Risk/Limit</button>
              </div>
              
              <div class="col-md-4 " style="border-right: 1px solid grey;">
               <button type="button" class="btn btn-link btn-sm setValuetoTxtAreaUT text-wrap checkdb" data-toggle="modal" data-target="#txtAreaModal4UT" data-dismiss="modal"  style="font-size: 11px;" >ReasonForDeviate.</button>
              </div>
              
             
              
              <div class="col-md-1">
                <button type="button" class="btn btn-sm btn-bs3-prime setValuetoTxtAreaUT" data-dismiss="modal">OK</button>
              </div>
          </div>
         
        
       
      </div>

    </div>
  </div>
</div>

<div class="modal" id="txtAreaModal4UT">
  <div class="modal-dialog">
    <div class="modal-content w-500">

      <!-- Modal Header -->
      <div class="modal-header" id="cardHeaderStyle-22">
        <h6 class="modal-title"> <i class="fa fa-question-circle" aria-hidden="true" style="color:;" ></i>&nbsp;Reason(s)for Deviation(s)</h6>
        <button type="button" class="close setValuetoTxtAreaUT" data-dismiss="modal" style="color: #fff;">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
              <div class="col-md-12">
		           <div class="form-group">
						 <textarea class="form-control txtarea-hrlines text-wrap checkdb" rows="15" cols="450" maxlength="3950" name="advrecReasonDUMMY3_pop" id="advrecReasonDUMMY3_pop" onkeydown="textCounter(this,3950);" onkeyup="textCounter(this,3950);"></textarea>
				         <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
				   </div>
		     </div>
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
          <div class="row">
              <div class="col-md-3" style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm setValuetoTxtAreaUT" data-toggle="modal" data-target="#txtAreaModal1UT" data-dismiss="modal"  style="font-size: 11px;" >Client obj.</button>
              </div>
              
              <div class="col-md-4 " style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm text-wrap checkdb setValuetoTxtAreaUT" data-toggle="modal" data-target="#txtAreaModal3UT" data-dismiss="modal"  style="font-size: 11px;"  >Reason for recomm.</button>
              </div>
              
              <div class="col-md-4" style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm setValuetoTxtAreaUT" data-toggle="modal" data-target="#txtAreaModal3UT" data-dismiss="modal" style="font-size: 11px;" >Risk/Limit</button>
              </div>
              
              <div class="col-md-1">
                <button type="button" class="btn btn-sm btn-bs3-prime setValuetoTxtAreaUT" data-dismiss="modal">OK</button>
              </div>
          </div>
       
      </div>

    </div>
  </div>
</div>



  <script type="text/javascript" src="vendor/avallis/js/advice_basis_recomm.js"></script>
    
<script>

          jsnDataFnaDetails = ${CURRENT_FNA_DETAILS};

          </script>  
 
</body>

</html>