

var jsnDataFnaDetails = "";

$(document).ready(function() {
	
	if(!isEmpty(jsnDataFnaDetails)  && !isJsonObjEmpty(jsnDataFnaDetails)){
		  for(var obj in jsnDataFnaDetails){
			  if (jsnDataFnaDetails.hasOwnProperty(obj)) {
				  var objValue = jsnDataFnaDetails[obj];
				  switch(obj){
				  
				  case "suprevMgrFlg":
					  if(objValue == "agree" )
					  { 
						  $("#suprevMgrFlgA").prop("checked",true);
						  $("INPUT[name=suprevMgrFlg]").val([objValue]);
					  } 
					  else if(objValue == "disagree" ){
						  $("#suprevMgrFlgD").prop("checked",true);
						  $("INPUT[name=suprevMgrFlg]").val([objValue]);
						  } 
					  else {
						  $("#suprevMgrFlgA").prop("checked",false);
						  $("#suprevMgrFlgD").prop("checked",false);
						 }
					  break;
				  
				  default:
					  $("#"+obj).val(objValue);
				  }
				  
			  }
			  
		  }

	}
	
			// Enable and Disable manager / Superviser sec below
			var strAdvMgrId ="";
			if (LOG_USER_MGRACS == "Y" && LOG_USER_ADVID == servAdvMgrId){
				$("#diManagerSection").removeClass('disabledMngrSec');
			}else{
				$("#diManagerSection").addClass('disabledMngrSec');
			}
			
			var fnaId=$("#hTxtFldCurrFNAId").val();
			
	getAllSign(fnaId);
    setPageManagerSts();
	setPageAdminSts();
	setPageComplSts();
	
});




function saveCurrentPageData(obj,nextscreen,navigateflg,infoflag){
	
	var applnextscreen = $("#applNextScreen").val();
	
	var frmElements = $('#frmManagerForm :input').serializeObject();
	
	var agreeFlag = document.getElementById("suprevMgrFlgA");
	var disagreeFlag = document.getElementById("suprevMgrFlgD");
    if (agreeFlag.checked == true){
		frmElements["suprevMgrFlg"] = "agree";
    }
    if (disagreeFlag.checked == true) {
   	 frmElements["suprevMgrFlg"] = "disagree";
    }

		$.ajax({
	            url:"fnadetails/register/managerdecl",
	            type: "POST",
	            async:false,
	            dataType: "json",
	            contentType: "application/json",
	            data: JSON.stringify(frmElements),
	            success: function (response) {
					if(navigateflg && nextscreen != "FINISHED"){
						  window.location.href=nextscreen;
					}else{
						if(applnextscreen == "FINISHED"){

							if(infoflag){
								
								var signornot = $("#hTxtFldSignedOrNot").val();
								var mgrkycSentStatus = $("#kycSentStatus").val();
								var mgrapproveStatus = $("#mgrApproveStatus").val();
								
								if(signornot == "Y" ) {
									if(!isEmpty(mgrkycSentStatus)) {
										Swal.fire({
										  text: isEmpty(mgrapproveStatus)?
												  "This FNA Form is already signed and sent for approval process"
												  :"This FNA Form is already signed and updated by manager",
										  icon: 'info',
										  showCancelButton: false,
										  allowOutsideClick:false,
										  allowEscapeKey:false,
										}).then((result) => {
										  if (result.isConfirmed) {
											  
//											  if(mgrapproveStatus == "APPROVE") {
													openSrchScreen()
//												}
											  
											  Swal.close() 
										  }
										})
									}else{
										showManagerMailConfirm();
									}
									
									
								}else {
								

									swal.fire({
									  text: "Do you want to finish and proceed to key information verification?",
									  icon: 'question',
									  showCancelButton: true,
									//  confirmButtonColor: '#3085d6',
									 // cancelButtonColor: '#d33',
									  confirmButtonText: 'Yes, Verify!',
										showLoaderOnConfirm: true,
										 allowOutsideClick:false,
										  allowEscapeKey:false,
									}).then((result) => {
									  if (result.isConfirmed) {
									    $("#btntoolbarsign").trigger("click");
									  }
									})
								}
								
								

							}
	            		}
					}
	            	
	            	
	            },
	            error: function(xhr,textStatus, errorThrown){
		 			//$('#cover-spin').hide(0);
		 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
				}
	            });
	  }

