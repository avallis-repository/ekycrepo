var strMastAttachCateg = "";
var mastAttachCategArr = new Array();
var attachmentArray = new Array();
var currentCustId="";
 var titMap = new Map();
    var categMap = new Map();
    var insMap = new Map();
    var filesArr=[];
    
    
$( document ).ready(function() {
        //$("#file-0").fileinput( ); 
	
	//$("#file-0").fileinput();//poovathi commented on 23-06-2021
	
	//poovathi add on 24-06-2021
	 $("#file-0").fileinput({
		   maxFilePreviewSize: 200000 // maxinum file size preview 200MB
      });
	
	/*$("#file-0").fileinput({
		 allowedFileExtensions: ["jpg", "jpeg", "gif", "png"],
        });*/
});
    
    // Add the following code if you want the name of the file appear on select box
    $(".custom-file-input").on("change", function() {
      var fileName = $(this).val().split("\\").pop();
      $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
    
    $("#btnUplodCustAttchFile").click(function(){
    	saveDocuments(true)
     });
    
$("#btnWithoutUplodAndEmail").click(function(){
	sendMailtoManger();
	
})

$("#btnUplodAndEmail").click(function(){
	if(!saveDocuments(false))
		return;
	
})


function saveDocuments(flag){
		
//    	validation required - all the document title and category shold be selected
    	
    	var data = new FormData();
    	var customerAttachments={};
    	var mastCategArr = {};
        var filesLength=document.getElementById('file-0').files.length;
        

       var tmpfilesLength = $("div.file-drop-zone").find("div.file-preview-frame").length;
       tmpfilesLength = tmpfilesLength/2;
        //alert(tmpfilesLength)

			if(tmpfilesLength == 0){
					toastr.clear($('.toast'));
					toastr.options = {  timeOut: "5000", extendedTimeOut: "1000"};
					toastr["error"]("Upload any documents!");	
					return false;
			}
        
           /* for(var i=0;i<filesLength;i++){
			    var fileInfo=$('#file-0')[0].files[i];*/
               
            /*for(var i=0;i<tmpfilesLength;i++){
		         var fileInfo=document.getElementById('file-0').files[i];*/
                 //  var fileInfo=filesArr[i];
        
           var filesLength=filesArr.length;
             for(var i=0;i<filesLength;i++){
	        // var fileInfo=document.getElementById('file-0').files[i];
               var fileInfo=filesArr[i];
	
//			    console.log("--------fileId"+fileInfo.name);
	            var filePrefix=fileInfo.name.replace("(","_");
	            var fileId=filePrefix.replace(")","_");
	            var categ = document.getElementById("categ"+fileId);
	            //alert(fileId)
	            if(fileId != null && categ!=null){
	          
	
	           // var categ = document.getElementById("categ"+fileInfo.name);
	  			var selCateg = categ.value;
	  			var selcategText= isEmpty(selCateg) ? "-NIL-": categ.options[categ.selectedIndex].text;
		    	data.append('custAttachFileList',fileInfo);
	
	  			customerAttachments.selCustAttachTitle=document.getElementsByName("tit"+fileId)[0].value;
	            customerAttachments.attachFor=document.getElementsByName("attach"+fileId)[0].value;
		        //customerAttachments.selCustAttachTitle = document.getElementsByName("tit"+fileInfo.name)[0].value;
		        customerAttachments.txtFldCustAttachAttachCategName = selcategText;//selCateg;
		        customerAttachments.txtFldCustAttachFilename = fileInfo.name;
		      //  customerAttachments.txtFldCustAttachRemarks = document.getElementsByName("rem"+fileInfo.name)[0].value;
				customerAttachments.txtFldCustAttachRemarks=document.getElementsByName("rem"+fileId)[0].value;	        
		        customerAttachments.selCustAttachMasterAttachCateg = selCateg;//categMap.get(selCateg);
		        customerAttachments.screenFrom=String(flag);
	            //Insurar Field Value
		        customerAttachments.insurarName = document.getElementsByName("insur"+fileId)[0].value;
		        /*customerAttachments.fnaId = $("#fnaId").val();*/
		       
//		        console.log("customerAttachments.insurarName--------------------->>>>>>>"+customerAttachments.insurarName)
	       		data.append("custAttachments",JSON.stringify(customerAttachments));
	       		
	            }
	        
	       } 
        
            $.ajax({
    	    url: "CustAttach/uploaddata",
            type: "POST",
            data: data,
            processData: false,
            contentType: false,
            cache : false,
            async:false,   
            success: function (data, textStatus, jqXHR) {
                    //alert("Upload Success");
					toastr.clear($('.toast'));
					toastr.options = {  timeOut: "5000", extendedTimeOut: "1000"};
					toastr["success"]("Document(s) are uploaded successfully!");
					if(flag){
						$("#uplodModal").modal('hide');	
					}else{
						setTimeout(function() { loaderBlock() ;}, 0);	
						setTimeout(function() { sendMailtoManger() ;}, 500);	
					}
					
					// reset file upload properties 
					$('#file-0').fileinput('reset');
					$('#file-0').fileinput('clear'); 
					filesArr=[];
					
					//clear browser cache using jquery
					/*$.ajax({
				        url: "",
				        context: document.body,
				        success: function(s,x){
                             $('html[manifest=saveappoffline.appcache]').attr('content', '');
				                $(this).html(s);
				        }
				    }); */
					
                    return true;
                
            },
            error: function(xhr,textStatus, errorThrown){
	 			ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			    return false;

            }
        });

return true;


}
    
    var categList={};
    function getAllCustAttchCategList(){
    	$.ajax({
            url:"CustAttach/getAllCustCategList",
            type: "GET",
			async:false,
            dataType: "json",
            contentType: "application/json",
            
            success: function (response) {
            	categList = response;
//            	alert("categList"+ categList);
            	
            },
            error: function(xhr,textStatus, errorThrown){
	 			//$('#cover-spin').hide(0);
	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			}
            });
    	
    	
    	
    	
    	
    	
    }
    
    //get all Principal Names for Insurar combo field
    var prinList = {};
    function getAllInsurPrinList(){
    	$.ajax({
    	            url:"productsrecommend/prodRecomSumDetls/",
    	            type: "GET",async:false,
    	            dataType: "json",
    	            contentType: "application/json",
    	            success: function (response) {
    	            	prinList = response;
    	            	
    	             },
    	             error: function(xhr,textStatus, errorThrown){
    	 	 			//$('#cover-spin').hide(0);
    	 	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
    	 			}
    	            });
    	 
    }//End
    
    function handleFiles(files){
        for(var i=0;i<files.length;i++){
  	    filesArr.push(files[i]);
       }
      }
   
     function getCategData(id){
    	
    	 var slctId="categ"+id;
    	 
    	 var insurId = "insur"+id;

         //Attachment for
         var attachForId="attach"+id;
         var arrAttach=["SELF","SPOUSE"];
         autocomplete(document.getElementById(attachForId), arrAttach);
    		//End
    		
    	    for(var i=0;i<categList.length;i++){
    		    var categData=categList[i];
    		    var attchName=categData[1];
    	        var titName=categData[2];
    	        
    	        setMapValue(titMap,attchName,titName);
    			setCategMap(categMap,attchName,categData[0]);

    	    }
    	    
    	    // poovathi change variable names on 14-06-2021 
    	    var prodArr = [];var x = [];
    	        for(var j=0;j<prinList.length;j++){
    		    var prinData=prinList[j];
    		    var prinId = prinData.recomPpPrin;
    		    var prinName=prinData.recomPpPrin;
    		   
    		    //poovathi add on 07-06-2021 
    		   if(!prodArr.includes(prinName)){
    			   prodArr.push(prinName)
    		   }
       	     }
    	        
       	      //set PrincipalNames in Insurar Combo filed
    		   for(var k = 0; k < prodArr.length; k++) {
		    	   curntVal = prodArr[k];
		    	   var prinOpts = document.createElement('option');
       		       prinOpts.value = curntVal;
       		       prinOpts.text = curntVal;
       		       document.getElementById(insurId).appendChild(prinOpts);
		           
		        }//end
    		   
    		   //poovathi commented on 07-06-2021
    		   /* var selFldInsurId = document.getElementById(insurId), curntVal, i;
                    for(i = 0; i < selFldInsurId.length; i++) {
    		    	   curntVal = selFldInsurId[i];
    		           prodArr.push(curntVal.value.trim());
    		        }
    		    if(!prodArr.includes(prinName.trim())){
    			   var prinOpts = document.createElement('option');
       		       prinOpts.value = prinName;
       		       prinOpts.text = prinName;
       		       document.getElementById(insurId).appendChild(prinOpts);
       		       
    		       }*/
    		     
         var arrCateg=[];
		for(let key of categMap.keys()){
//    				console.log(slctId,"slctIdslctId")
    				var option = new Option(key,categMap.get(key));
    				$(option).html(key);
    				document.getElementById(slctId).append(option);
                    arrCateg.push(key);
    				cateName=attchName;
    			}
    }
     
     
    function setCategMap(map,key,value){
    	if (!map.has(key)) {
    	   map.set(key,value);
            return;
        }
       
    }
     function setMapValue(map, key, value) {
        if (!map.has(key)) {
    	   var setVal=new Set();
            map.set(key,setVal.add(value));
            return;
        }
        map.get(key).add(value);
    }

    function setDocTitle(slctBoxid){
    	var id=$(slctBoxid).attr("id");
    	var titId="tit"+id.substring(5,id.length);
    	//clear all selectbox value except first option
    	var select = document.getElementById(titId);
			var length = select.options.length;
			if(length > 1){
				for (i = length-1; i >= 0; i--) {
	  			  select.options[i] = null;
	  			}
				

				var option = new Option("--SELECT--","");
	  			$(option).html(val);
	  			document.getElementById(titId).append(option);
			}
			
    
       document.getElementById(titId).value="";
    	
       var text = slctBoxid.options[slctBoxid.selectedIndex].text
//       console.log("text-->",text,"titMap",titMap)
    	//Set doctitle 
        var arrTit=[];
  		var titKeys = titMap.get(text);
  		if(titKeys!=undefined){
  		for(var val of titKeys) {
  			var option = new Option(val,val);
  			$(option).html(val);
  			document.getElementById(titId).append(option);
              arrTit.push(val);
  			
  		};
           //autocomplete(document.getElementById(titId), arrTit);
          
  		}
    }
       
        	
         
 /*    function setDocTitle(slctBoxid,id){
    	 alert()
     //	var id=$(slctBoxid).attr("id");
     	var titId="tit"+id;//.substring(5,id.length);
     	alert(titId)
         //Set hidden field category id
         //var categId="categId"+id.substring(5,id.length);
         
         //var titId=$(slctBoxid).attr("id");
         //var categId="categ"+titId.substring(3,titId.length);
     	
     	document.getElementById(titId).value="";
     	//Category select box text and value
     	//var text=$(categId).val();
         var text=document.getElementById("categ"+id).value;
         alert(text)
         //document.getElementById(categId).value=categMap.get(text);
        
        //var text=document.getElementById(titId).value;
     	//var value=$(slctBoxid).val();
//     	console.log("text-->",text,"titMap",titMap)
     	//Set doctitle 
           var arrTit=[];
     		var titKeys=titMap.get(text);
     		if(titKeys!=undefined){
     		for(var val of titKeys) {
     			var option = new Option(val,val);
     			$(option).html(val);
     			document.getElementById(titId).append(option);
                 arrTit.push(val);
     			
     		};
              //autocomplete(document.getElementById(titId), arrTit);
             
     		}
     		
     }*/
    
    
    
    
    

    function checkForEmptyCombo(fldObj,idMsg){
    	if(fldObj.length<=1){
    	  document.getElementById("selCustAttachMastAttachCateg").classList.remove("hide");
      	  document.getElementById("selCustAttachMastAttachCateg").classList.add("show");
      	  document.getElementById("selCustAttachMastAttachCategError").classList.add("err-fld");
    		return false;
    	}
    }	
   
   /* function test(){
    	var x = $("file-preview-thumbnails clearfix").children().length;
    	alert(x)
    	for (i = 0; i < strMastAttachCateg.length; i++) {
    	var strNewOpt = new Option("option text", "value");
        $(strNewOpt).html('<option value="'+strMastAttachCateg[i]+'">'+strMastAttachCateg[i]+'</option>');
        document.getElementById("selCustAttachMastAttachCateg").append(strNewOpt);
    	}
 }   */
   
    	
   
    
    
    
    
	$('#uplodModal').on('shown.bs.modal', function () {
		//alert(currentCustId)
		$('#file-0').fileinput('reset');
		$('#file-0').fileinput('clear'); 
		  filesArr=[];
		  getAllExisitingDocs();
		 //show Uploads Button 
		   $("#btnUplodCustAttchFile").removeClass("d-none")
		
	});
	
	
	function getAllExisitingDocs(){
		
		var fnaId=$("#hTxtFldCurrFNAId").val();
		
		$("#dynaAttachNTUCList").empty()
		
		 $.ajax({
	    	 url:"CustAttach/getAllNricCustDocs",
	            data:{"custid":currentCustId,"fnaId":fnaId},
	            type: "GET",
	            async:false,
                success: function (response) {
                 for(var d=0;d<response.length;d++){
                     loadDocJsnDataNew(response[d],null)
            	}
            	
            	if(response.length == 0) {
                    $("#dynaAttachNRICList").html("-No Refernce Docs-");
					
					var totlDocListLen = $("#dynaAttachNTUCList").children().length;
                    $("#noDocFoundSec").addClass("show").removeClass("hide");
					$("#DocListSec").addClass("hide").removeClass("show");
					//poovathi add on 15-06-2021
					$("#fileCountTxt").html("");
		
            	}else{
					
					$("#noDocFoundSec").addClass("hide").removeClass("show");
					$("#DocListSec").addClass("show").removeClass("hide");
					//poovathi add on 15-06-2021
					$("#fileCountTxt").html("Total Files : "+'<span><span class="badge badge-pill badge-light font-sz-level7">'+response.length+'</span></span>');
					
					
            	}
            	
            },
            error: function(xhr,textStatus, errorThrown){
	 			//$('#cover-spin').hide(0);
	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			}
            });
	}		
	
	function loadDocJsnDataNew(jsnData,fnaid){
        
		
		var remarks = isEmpty(jsnData[2]) ? "" : jsnData[2];
		var remarks = isEmpty(jsnData[2]) ? "" : jsnData[2];
		
		var remarksLbl =isEmpty(remarks)?
						'':'<div class="col-12 font-sz-level8"><span class="text-dark bold">Remarks : </span> '+remarks+'</div>'

		
		if(isEmpty(jsnData[5])){
			jsnData[5] = "-NIL-";
		}else{
			jsnData[5] = jsnData[5];
		}

		

		//poovathi add on 16-06-2021
		//Filetype
		if(isEmpty(jsnData[6])){
			jsnData[6] = "-NIL-";
		}else{
			jsnData[6] = jsnData[6];
		}
		//Filename
		if(isEmpty(jsnData[7])){
			jsnData[7] = "-NIL-";
		}else{
			jsnData[7] = jsnData[7];
		}
		//Filesize
		if(isEmpty(jsnData[8])){
			jsnData[8] = "-NIL-";
		}else{
			jsnData[8] = jsnData[8];
		}
		//Validation End

		var docid = jsnData[0];
		
		
		
		var strList ='<a href="#" class="list-group-item list-group-item-action p-2" id="'+docid+'" onmouseover="showFileDetls(this)" onmouseout="hideFileDetls(this)">'+
			    '<div class="row">'+
				   '<div class="col-12"><small><i class="fa fa-trash cursor-pointer right ml-3" style="color: #f04040;" onclick=deleteDoc(this) title="Delete Doc."></i></small><small><i class="fa fa-download cursor-pointer right" style="color: #007bff;" onclick=downloadDoc(\"'+docid+'\") title="Click to Download Doc."></i></small></div>'+
				   '<div class="col-12 mt-2"><h6 class="mb-1 font-sz-level7 text-custom-color-gp bold"><span class="text-dark">Category : </span> '+jsnData[1]+'</h6></div>'+
				   //'<div class="col-1"></div>'+
			 '</div>'+
	   
	     
		      '<div class="row">'+
				     '<div class="col-12"><small class="font-sz-level7"><span class="text-dark bold">Title : </span> '+jsnData[3]+'</small></div>'+
				      '<div class="col-12"><small class="font-sz-level7"><span class="text-dark bold">Insurer Name :</span> '+jsnData[5]+'</small> </div>'+
				      
		     '</div>'+
	     //poovathi add on 16-06-2021
		     '<div class="row hide">'+
			     '<div class="col-12"><small class="font-sz-level8"><span class="text-dark bold">File Name : </span> '+jsnData[6]+'</small></div>'+
			     '<div class="col-12"><small class="font-sz-level8"><span class="text-dark bold">File Size : </span> '+bytesToSize(jsnData[7])+'</small></div>'+
			     '<div class="col-12"><small class="font-sz-level8"><span class="text-dark bold">File Type : </span> '+jsnData[8]+'</small> </div>'+
			     remarksLbl+
		     '</div>'+
	    '</a>';
		
		$("#dynaAttachNTUCList").append(strList);
		
		var totlDocListLen = $("#dynaAttachNTUCList").children().length;
		if(totlDocListLen > 0){
			$("#noDocFoundSec").addClass("hide").removeClass("show")
			$("#DocListSec").addClass("show").removeClass("hide")
		}else{
			$("#noDocFoundSec").addClass("show").removeClass("hide")
			$("#DocListSec").addClass("hide").removeClass("show")
		}
		
		$('#dynaAttachNTUCList a:even').addClass('bsc-pln-bg');
	  
	  
	}
	
	//poovathi add on 16-06-2021
	function showFileDetls(fldObj){
		$(fldObj).children().eq(2).addClass("show").removeClass("hide");
	}
	function hideFileDetls(fldObj){
		$(fldObj).children().eq(2).addClass("hide").removeClass("show");
	}//end
	
	function deleteDoc(thisObj){
		var cusAttachId =  $(thisObj).parents(".list-group-item").attr("id");
		//alert(cusAttachId)
				Swal.fire({
				  title: 'Are you sure?',
				  text: "Want to Delete Document?",
				  icon: 'warning',
		          allowOutsideClick:false,
		          allowEscapeKey:false,
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Yes, Delete it!'
				}).then((result) => {
				  if (result.isConfirmed) {
					  
					  $.ajax({
				   			
				   			 url:"CustAttach/delete/"+cusAttachId,
				   			 type: "POST",async:false,
				            // contentType: "application/json",
				 	            success: function (response) {
				 	            	  var totlDocListLen = $("#dynaAttachNTUCList").children().length;
									  
									  if(totlDocListLen > 1){
										    $(thisObj).parents(".list-group-item").remove();
										    hideNoFileFoundImg();
										 }else{
											$(thisObj).parents(".list-group-item").remove();
											showNoFileFoundImg();
										}
									  
									  swal.fire("Deleted!", "File / Document has been deleted.", "success");
									//poovathi add on 15-06-2021
									  var listlen = $("#dynaAttachNTUCList").children().length;
										$("#fileCountTxt").html("Total Files : "+'<span><span class="badge badge-pill badge-light font-sz-level7">'+listlen+'</span></span>');
								  },
								  error: function(xhr,textStatus, errorThrown){
							 			//$('#cover-spin').hide(0);
							 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
									}
								  
								});
				 		 			
				 	            }else{
									  swal.fire("Cancelled", "File / Document detail is safe :)", "error");
								}
				 		 		
				 	            });
					        
					  
					  
					  
					  
					  
					  
					  
					
	}
	
	
	
 function hideNoFileFoundImg(){
$("#noDocFoundSec").addClass("hide").removeClass("show")
$("#DocListSec").addClass("show").removeClass("hide")
}
	
function showNoFileFoundImg(){
$("#noDocFoundSec").addClass("show").removeClass("hide")
$("#DocListSec").addClass("hide").removeClass("show")
}
	//var count = 0;
	function downloadDoc(id){
		var url = 'downloadDoc/'+id;
		//count = count+1;
		window.open(url,"_blank");
		
		
	}
	
	
/*Auto complete text box*/	
	function autocomplete(inp, arr) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      /*append the DIV element as a child of the autocomplete container:*/
      this.parentNode.appendChild(a);
      /*for each item in the array...*/
      for (i = 0; i < arr.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
        //if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
	    if (arr[i].indexOf(val.toUpperCase()) != -1) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          /*make the matching letters bold:*/
          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
          b.innerHTML += arr[i].substr(val.length);
          /*insert a input field that will hold the current array item's value:*/
          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/
          b.addEventListener("click", function(e) {
              /*insert the value for the autocomplete text field:*/
              inp.value = this.getElementsByTagName("input")[0].value;
              /*close the list of autocompleted values,
              (or any other open lists of autocompleted values:*/
              closeAllLists();
          });
          a.appendChild(b);
        }
      }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
 
  
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
		 var titId=e.target.id;
		  if(titId.startsWith("tit")){
			var categId="categ"+titId.substring(3,titId.length);
			var text=document.getElementById(categId).value;
			 var arrTit=[];
	    		var titKeys=titMap.get(text);
	            if(titKeys!=undefined){
		    		for(let val of titKeys) {
		    			
		                arrTit.push(val);
		    			
		    		};
		             document.getElementById(titId).value=arrTit[0];
		             autocomplete(document.getElementById(titId), arrTit);
	           }
		}
	      closeAllLists(e.target);
	  });
  
}
	
	function dltFileName(id){
		var fileId=id.substring(id.indexOf("_")+1,id.length);
		const regex = /_20/ig;
		var fName=fileId.replaceAll(regex,' ');
		for(var i=0;i<filesArr.length;i++){
			var fileName=filesArr[i].name;
			if(fileName == fName){
				filesArr.splice(i,1);
			}
		}
	}
	
	
	
