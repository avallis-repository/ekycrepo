var  strDataFrmId = "",jsnDataCustomerDetails ="",jsnDataSelfSpsDetails = "",jsnDataFnaDetails ="";
var selfSpouseDets={};

$( document ).ready(function() {

	$('[data-toggle="ReasonAPopup"]').popover({
 	   
        placement :'top',   
        html: true,
        content: function() {
         return $('#popover-ReasonAPopup').html();
       }
     });
   
   $('[data-toggle="ReasonBPopup"]').popover({
	   
	   placement :'top', 
       html: true,
       content: function() {
         return $('#popover-ReasonBPopup').html();
       }
     });
   
   $('[data-toggle="ReasonCPopup"]').popover({
	   
	   placement :'top',
       html: true,
       content: function() {
         return $('#popover-ReasonCPopup').html();
       }
     });
  
   
//     var dataFormId=$("#hdnSessDataFormId").val();

if(!isEmpty(jsnDataFnaDetails)  && !isJsonObjEmpty(jsnDataFnaDetails)){
		  for(var obj in jsnDataFnaDetails){
			  if (jsnDataFnaDetails.hasOwnProperty(obj)) {
				  var objValue = jsnDataFnaDetails[obj];
				  
				  var nodetype = $("#"+obj).prop('nodeName');
				  var inputtype = $("#"+obj).prop("type");
				  
				  switch(obj){
				  default:		
					  if(inputtype == "radio" || inputtype == "checkbox"){
						  if(objValue == "Y"){
							  $("#"+obj).prop("checked",true);
						  }
					  }
					  
					  $("#"+obj).val(objValue);
				  }
			  }
		  }
	  }
 
	 if(!isEmpty(jsnDataSelfSpsDetails) && !isJsonObjEmpty(jsnDataSelfSpsDetails)){
		
		 var spsTab = '';
		 for(var obj in jsnDataSelfSpsDetails){
				  if (jsnDataSelfSpsDetails.hasOwnProperty(obj)) {
				  
					  var objValue = jsnDataSelfSpsDetails[obj];
					  var nodetype = $("#"+obj).prop('nodeName');
					  var inputtype = $("#"+obj).prop("type");
					  
//					  console.log(obj,",",objValue);
					  switch(obj){
					  
					  case "dfSelfNric":
						    if(!isEmpty(objValue)){
	        					$("#hdnSelfNric").val(objValue)
	        				}else{
	        					$("#hdnSelfNric").val('')
			      			}
	        				break;
	        				
					  case "dfSpsNric":
						     
	        				if(!isEmpty(objValue)){
	        					$("#hdnSpsNric").val(objValue)
	        				}else{
	        					$("#hdnSpsNric").val('')
			      			}
	        				break;	
					  
					  case "dfSelfName":
					  		addTab("CLIENT",objValue,"selfList","dfSelfTaxres","dfSelfTaxresOth","C");
					  break;
					  
					  case "dfSpsName":
					  if(!isEmpty(objValue)){
						  spsTab = objValue;
						addTab("SPOUSE",objValue,"spsList","dfSpsTaxres","dfSpsfTaxresOth","S");
						}
					break;
					  
					  case "dfSelfUsnotifyflg":
						  
						 
						  if(objValue == "Y"){
						  	$("#dfSelfUsnotifyflg1").prop("checked",true).val("Y");
						  	$("#collapseSec1").removeClass("hide").addClass("show")
						  	 $("#dfSelfUspersonflg1").prop("checked",false).val("N");
							  $("#collapseSec2").removeClass("show").addClass("hide")
							  $("#dfSelfUstaxno").val("");
							  
					 	
						  }else if(objValue == "N") {
 							  $("#dfSelfUsnotifyflg1").prop("checked",false).val("N");
							  $("#collapseSec1").removeClass("show").addClass("hide")
						 }
						  
						  
					  break;
					  
			          case "dfSelfUspersonflg":
			        	   if(objValue == "Y"){
                        	 $("#dfSelfUspersonflg1").prop("checked",true).val("Y");
					         $("#collapseSec2").removeClass("hide").addClass("show")
					          $("#dfSelfUsnotifyflg1").prop("checked",false).val("N");
						  $("#collapseSec1").removeClass("show").addClass("hide")
						  }else if(objValue == "N") {
 							  $("#dfSelfUspersonflg1").prop("checked",false).val("N");
							  $("#collapseSec2").removeClass("show").addClass("hide")
						 }
                           
					  break;
					  
					  
					  case "dfSpsUsnotifyflg":
						 
						  if(objValue == "Y"){
							  $("#dfSpsUsnotifyflg1").prop("checked",true).val("Y");
							  $("#collapseSec3").removeClass("hide").addClass("show")
							  $("#dfSpsUspersonflg1").prop("checked",false).val("N");
							  $("#collapseSec4").removeClass("show").addClass("hide")
							  $("#dfSpsUstaxno").val("");
						 }else if(objValue == "N") {
 							  $("#dfSpsUsnotifyflg1").prop("checked",false).val("N");
							  $("#collapseSec3").removeClass("show").addClass("hide")
						 }
						  
						  
					  break;
					  
			          case "dfSpsUspersonflg":
			        	  
                          if(objValue == "Y"){
                        	  $("#dfSpsUspersonflg1").prop("checked",true).val("Y");
                        	  $("#collapseSec4").removeClass("hide").addClass("show")
                        	  $("#dfSpsUsnotifyflg1").prop("checked",false).val("N");
						      $("#collapseSec3").removeClass("show").addClass("hide")
              		 	  }	else if(objValue == "N") {
 							  $("#dfSpsUspersonflg1").prop("checked",false).val("N");
							  $("#collapseSec4").removeClass("show").addClass("hide")
						 }
                         
				      break;
					  
					   case "dfSelfTaxres":
					   case  "dfSpsTaxres":
					   if(!isEmpty(objValue)){
					   	 var ListId = "",radbtnName = "";
					   	         //set values for Self CRI Section
							   	 if(objValue.charAt(0) =='C'){
							   		  ListId = "ulLiStyle-01selfList";
							   		  radbtnName = "dfSelfTaxres";
							   		}	
								
							     //set values for Sps CRI Section
							   	  if(objValue.charAt(0)=='S'){
							   		 ListId = "ulLiStyle-01spsList";
							   		 radbtnName = "dfSpsTaxres";
							   		}	
						
					   	$("#"+ListId).children().find(".form-check-input").each(function(){
							 var thisFldRadBtnVal = $(this).val();
							 if ((objValue == thisFldRadBtnVal)){
							     $(this).trigger("click");
							 }
						 })
						 
							    
						}
					   
					   break;
					   
					  
					  					  
					  default:	
						  
						  if(inputtype == "radio" || inputtype == "checkbox"){
							  if(objValue == "Y"){
								  $("#"+obj).prop("checked",true);
								  $("#"+obj).val(objValue);
							  }
						  }
						  
						  						  
						  $("#"+obj).val(objValue);
					  }	
					  
					  if(isEmpty(spsTab)){
						  $("#dfSpsUspersonflg1").prop("checked",false).val("N");//Us Person flag spouse
						  $("#dfSpsUsnotifyflg1").prop("checked",false).val("N");//not US Person flag spouse
						  $("#dfSpsUspersonflg1").prop("disabled",true)//disable self US flag
						  $("#dfSpsUsnotifyflg1").prop("disabled",true)//disable sos not US flag
						  $("#collapseSec3").removeClass("show").addClass("hide")
						  $("#collapseSec4").removeClass("show").addClass("hide")
						  $("#dfSpsUstaxno").val('')
					  }else if(!isEmpty(spsTab)){
						  $("#dfSpsUspersonflg1").removeAttr("disabled")//Enable self US flag
						  $("#dfSpsUsnotifyflg1").removeAttr("disabled")//Enable Sps Not US Flag
						  
					  }
					  
				  }				  
			  }
		 
		 
			selfSpouseDets=JSON.stringify(jsnDataSelfSpsDetails);
			//setUsTaxDets(selfSpouseDets);
			//setCRSData(selfSpouseDets);
			
			//poovathi add on 19-05-2021
			  var fnaIdFlg = $("#createNewFnaFlgs").val();
              var prevFnaId = $("#prevFnaId").val();
              var fnaId = "";
          	if((fnaIdFlg == "true")&&(!isEmpty(prevFnaId))){
          		fnaId = prevFnaId;
			}else{
				fnaId=$("#hTxtFldCurrFNAId").val();
			}
		 
			getAllDataByFnaId(fnaId);
			
		  }
	 
	    //always set self Client (1) Tab Active in Tax Residency Screen
	    
		$('#selfSpouseTabs').children().eq(0).find("a.nav-link").trigger("click");
		//$('#Taxcontent').children().eq(1).addClass("hide").removeClass("active show");
		 

	var fnaId=$("#hTxtFldCurrFNAId").val();
	getAllSign(fnaId);

	setPageManagerSts();
	setPageAdminSts();
	setPageComplSts();
	
	
	//call self checkdb onchange function
  	$("#ulLiStyle-01selfList").children().find(".form-check-input,.form-control").bind("change",function(){
  		var curntObj = $(this)
  		enableReSignIfRequired(curntObj);
	 });
	 //call sps checkdb onchange function
	 $("#ulLiStyle-01spsList").children().find(".form-check-input,.form-control").bind("change",function(){
  		var curntObj = $(this)
  		enableReSignIfRequired(curntObj);
	 });
	 
	
     
 });   



function chngChktoRadFATCA(chkbox,clnttype){
if(clnttype == "client"){

	var chkboxname = $(chkbox).prop("name");
	//console.log(chkboxname,"chkboxname")
	
	if(chkbox.checked == true){
		//$("input[name='dfSelfUsnotifyflg'], input[name='dfSelfUspersonflg']").attr("checked",false);
		
		chkbox.checked=true;
		switch(chkboxname) {
		 	case "dfSelfUsnotifyflg":
		 		$("#dfSelfUsnotifyflg1").prop("checked",true).val("Y");
			  	$("#collapseSec1").removeClass("hide").addClass("show")
			  	
			  	$("#dfSelfUspersonflg1").prop("checked",false).val("N");
		 		$("#collapseSec2").removeClass("show").addClass("hide")
			  	
		 		break;
		 	case "dfSelfUspersonflg":
		 		$("#dfSelfUspersonflg1").prop("checked",true).val("Y");
		 		$("#collapseSec2").removeClass("hide").addClass("show")
		 		$("#dfSelfUstaxno").val('')
		 		$("#dfSelfUstaxno").focus()
		 		
		 		$("#dfSelfUsnotifyflg1").prop("checked",false).val("N");
			  	$("#collapseSec1").removeClass("show").addClass("hide")
		 		break;
		}
	
	}else if(chkbox.checked == false){
		
		 var chkboxname = $(chkbox).prop("name");
		 if(chkboxname  == 'dfSelfUsnotifyflg'){
			 $(chkbox).val("N");
			 $("#collapseSec1").removeClass("show").addClass("hide")
		 }
		 if(chkboxname == 'dfSelfUspersonflg'){
			 $(chkbox).val("N");
			 $("#collapseSec2").removeClass("show").addClass("hide")
	       }
	}
}

if(clnttype == "spouse"){
	if(chkbox.checked == true){
		//$("input[name='dfSpsUsnotifyflg'], input[name='dfSpsUspersonflg']").attr("checked",false);
		chkbox.checked=true;
		
		var chkboxname = $(chkbox).prop("name");
		
		switch(chkboxname) {
		 	case "dfSpsUsnotifyflg":
		 		$("#dfSpsUsnotifyflg1").prop("checked",true).val("Y");
		 		$("#collapseSec3").removeClass("hide").addClass("show")
		 		
		 		$("#dfSpsUspersonflg1").prop("checked",false).val("N");
		 		$("#collapseSec4").removeClass("show").addClass("hide")	
		 		break;
		 		
		 	case "dfSpsUspersonflg":
		 		$("#dfSpsUspersonflg1").prop("checked",true).val("Y");
		 		$("#collapseSec4").removeClass("hide").addClass("show")	 
		 		
		 		$("#dfSpsUstaxno").val('')
		 		$("#dfSpsUstaxno").focus()
		 		
		 		$("#dfSpsUsnotifyflg1").prop("checked",false).val("N");
		 		$("#collapseSec3").removeClass("show").addClass("hide")
		 		break;
		}
		
	}else if(chkbox.checked == false){
		 
		
		 var chkboxname = $(chkbox).prop("name");
		 if(chkboxname  == 'dfSpsUsnotifyflg'){
			// alert(chkboxname)
			 $(chkbox).val("N");
			 $("#collapseSec3").removeClass("show").addClass("hide")
		 }
		 if(chkboxname == 'dfSpsUspersonflg'){
			// alert(chkboxname)
			 $(chkbox).val("N");
			 $("#collapseSec4").removeClass("show").addClass("hide")
	       }
	}
   }
chkNextChange();
}

function enabOrDisbTxtFld(chkObj, txtFldID, lblFldID) {

	var txtFldObj = document.getElementById(txtFldID);
	var lblFldObj = document.getElementById(lblFldID);
   if(chkObj!=null && lblFldObj!=null && txtFldObj != null){
	if (chkObj.checked ) {

		lblFldObj.style.color = '#243665';
		txtFldObj.className = 'form-control checkdb';
		txtFldObj.readOnly = false;
		txtFldObj.focus();

	} else if (!(chkObj.checked)) {

		lblFldObj.style.color = 'black';

		txtFldObj.value = '';
		txtFldObj.className = 'form-control';
		txtFldObj.readOnly = true;
	}// end of if else
}
}// end of enabOrDisbTxtFld



function chkJsnOptionsSingle(chkbox,jsnFld){

	$(chkbox).closest("tr").find("input:checkbox").each(function(){
		var chkName=$(this).attr("name");

		if(!$(this).is($(chkbox))){
			this.checked=false
		}

		if(chkName=="chkCDClntOth" &&  this.checked==false){
			$("#dfSelfTaxresOth").removeClass("writeModeTextbox").addClass("form-control")
			.attr('readOnly',true).val("");
		}

		if(chkName=="chkCDSpsOth" &&  this.checked==false){
			$("#dfSpsfTaxresOth").removeClass("writeModeTextbox").addClass("form-control")
			.attr('readOnly',true).val("");
		}
	});

	/*if(chkName!="chkCDClntOth" || chkName!="chkCDSpsOth"){
		if(chkName)
		$("#"+Ele+"").removeClass("writeModeTextbox").addClass("form-control")
		.attr('readOnly',true).val("");
	}*/

//	if(!chkbox.checked)chkbox.checked=true;

	var advAppTypeOpt = $("#"+jsnFld+"");
	advAppTypeOpt.val("");
	var jsonvalues = JSON.parse(isEmpty(advAppTypeOpt.val()) ? "{}" : advAppTypeOpt.val());
	var chkd = chkbox.checked;
	var val = $(chkbox).attr("data");

	var newobj=jsonvalues;
	newobj[val]=(chkd == true ? "Y" :"N");
	advAppTypeOpt.val(JSON.stringify(newobj));
}



//Set and get US tax declaration
//Radio button 
var arrUsTaxRadioData=["dfSelfUsnotifyflg","dfSelfUspersonflg",
						"dfSpsUsnotifyflg","dfSpsUspersonflg",
						"dfSelfTaxres","dfSpsTaxres"];
						
//Textbox 
var arrUsTaxTextData=["dfSelfUstaxno","dfSpsUstaxno","dfSelfTaxresOth"];
function getUsTaxDets(usTaxDets){
	for(var i=0;i<arrUsTaxRadioData.length;i++){
		var yOrn=$('input[name='+arrUsTaxRadioData[i]+']:checked').val();
		if(i<=3){
			if(yOrn == "Y"){
			 	usTaxDets[arrUsTaxRadioData[i]]=yOrn;
			}else{
				usTaxDets[arrUsTaxRadioData[i]]="N";
			}
		}else{
			usTaxDets[arrUsTaxRadioData[i]]=yOrn;
		}
	}
	for(var i=0;i<arrUsTaxTextData.length;i++){
		usTaxDets[arrUsTaxTextData[i]]=document.getElementsByName(arrUsTaxTextData[i])[0].value;
	}
}

function setUsTaxDets(selfSpouseDets){
	for(var i=0;i<arrUsTaxRadioData.length-2;i++){
		var collId=i+1;
		if(selfSpouseDets[arrUsTaxRadioData[i]] == "Y"){
			$("#"+arrUsTaxRadioData[i]+"1").prop('checked',true);
			$("#collapseSec"+collId).removeClass("hide").addClass("show");
    	
		}
		
	}
	for(var i=0;i<arrUsTaxTextData.length;i++){
		if(selfSpouseDets[arrUsTaxTextData[i]] != undefined){
			document.getElementsByName(arrUsTaxTextData[i])[0].value=selfSpouseDets[arrUsTaxTextData[i]];
		}
	}
	
}

function setClrTaxData(tdata){
	for(var i=0;i<arrUsTaxRadioData.length;i++){
		var value=tdata[arrUsTaxRadioData[i]];
		
		if(value == "STU"){
			$("#clrStu"+arrUsTaxRadioData[i]).prop('checked',true);
		}
		else if(value == "CUL"){
			$("#clrCul"+arrUsTaxRadioData[i]).prop('checked',true);
		}
		else if(value == "HWD"){
			$("#clrHwd"+arrUsTaxRadioData[i]).prop('checked',true);
		}
		else if(value == "OTH"){
			$("#clrOth"+arrUsTaxRadioData[i]).prop('checked',true);
		}
	}
	//oth text area
	for(var i=2;i<arrUsTaxTextData.length;i++){
		$("#"+arrUsTaxTextData[i]).val(tdata[arrUsTaxTextData[i]]);
	}
}

function setCRSData(data){

	
//	if(!isEmpty(data.dfSelfName)){
		addTab("CLIENT",$("#hTxtFldCurrCustName").val(),"selfList","dfSelfTaxres","dfSelfTaxresOth","C");
//	}else{
		
		
//	}
	
	
	if(data.dfSpsName!=null){
		addTab("SPOUSE",data.dfSpsName,"spsList","dfSpsTaxres","dfSpsfTaxresOth","S");
		arrUsTaxTextData.push("dfSpsfTaxresOth");
	}
	setClrTaxData(data);	
	
}
//Get all data by Fna Id
function getAllDataByFnaId(fnaId){
	
	$.ajax({
            url:"fnadetails/taxdetails/"+fnaId,
            type: "GET",
            dataType: "json",async:false,
            contentType: "application/json",
            
			success: function (data) {
				if(data.length == 0){
					
				}
				for(var i=0;i<data.length;i++){
					setTaxSelfOrSpouse(data[i]);
				}
				
            },
            error: function(xhr,textStatus, errorThrown){
	 			//$('#cover-spin').hide(0);
	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			}
            });
}
var s=1,ss=1;
function setTaxSelfOrSpouse(data){
	if(data.taxFor == "CLIENT"){
			setTaxResiDets(data,s,"selfList");
			s++
	}
	if(data.taxFor == "SPOUSE"){
			setTaxResiDets(data,ss,"spsList");
			ss++;
	}
	
}
var arrTaxData=["taxId","taxCountry","taxRefno","reasonToNotax","reasonbDetails","taxFor"];
var taxDets={};
function saveTaxDets(){
	for(var i=0;i<arrTaxData.length;i++){
		taxDets[arrTaxData[i]]=document.getElementsByName(arrTaxData[i])[0].value;
	}
	taxDets.fnaDetails=selfSpouseDets.fnaDetails;
	
//	alert(JSON.stringify(taxDets));
	closeElement();
	var fnaId=$("#hTxtFldCurrFNAId").val();
	$.ajax({
            url:"fnadetails/taxdetails/register/"+fnaId,
            type: "POST",async:false,
             data: JSON.stringify(taxDets),
             dataType: "json",
            contentType: "application/json",
           
            success: function (data) {
	         var sessTaxId=$("#sessTaxId").val();
	         if(sessTaxId != ""){
		        $("#"+data.taxId).remove();
			 }
	         
	         
	        
	        
//	          console.log(data,"tax details");
               setTaxSelfOrSpouse(data);
               clrTaxResData(data.taxFor);
               
               removeAllErrorMsgInChat();
               
				chkNextChange();

            },
            error: function(xhr,textStatus, errorThrown){
	 			//$('#cover-spin').hide(0);
	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			}
            });
}
//Clear Tax residency modal 
function clrTaxResData(){
	for(var i=0;i<arrTaxData.length-1;i++){
		document.getElementsByName(arrTaxData[i])[0].value="";
	}
}


//Delete tax residency details
function dltTaxData(obj){
	
	
	var taxId = $(obj).attr("id");
	
	var curntList = $(obj).parents(".list-group").attr("id");
	
	closeElement()
	
	const swalWithBootstrapButtons = Swal.mixin({
		  customClass: {
		    confirmButton: 'btn btn-success',
		    cancelButton: 'btn btn-danger mr-3'
		  },
		  buttonsStyling: false
		})

		swalWithBootstrapButtons.fire({
		  title: 'Are you sure?',
		  text: "You will not be able to recover this TaxTesidency Details ?",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonText: 'Yes, delete it!',
		  cancelButtonText: 'No, cancel!',
		  reverseButtons: true
		}).then((result) => {
		  if (result.isConfirmed) {
		        $.ajax({
	   			
	   			 url:"fnadetails/taxdetails/delete/"+taxId,
	             type: "DELETE",
	             dataType: "json",
	             async:false,
	             contentType: "application/json",
	 	            success: function (response) {
	 	            	
	 	            	if(curntList == "selfList"){
	 	            		
	 	            		var selfTaxList = $("#"+curntList).children().length;
	 	            		
	 	            		
	 	            	
	 	            		if(selfTaxList > 1){
	 	            			$("#"+taxId).remove();	
	 	            			hideTaxImgSelf(curntList);
	 	            			
	 	            		}
	 	            		
	 	            		if(selfTaxList == 1){
	 	            			$("#"+taxId).remove();
	 	            			showTaxImgSelf(curntList);
	 	            			
	 	            		}
	 	            		
	 	            	}
	 	            	
	 	            	if(curntList == "spsList"){
	 	            		
                            var spsTaxList = $("#"+curntList).children().length;
	 	            		
	 	            		if(spsTaxList > 1){
	 	            			$("#"+taxId).remove();	
	 	            			hideTaxImgSps(curntList);
	 	            		}
	 	            		
	 	            		if(spsTaxList == 1){
	 	            			$("#"+taxId).remove();
	 	            			showTaxImgSps(curntList);
	 	            		}
	 	            	}
	 	            	
	 	            	
	 		 		
	 		 			swalWithBootstrapButtons.fire("Deleted!", "Your Tax Residential Details has been deleted.", "success");
				        chkNextChange();
				        //checkdb onchange function for delete operation
				        enableReSignIfRequiredforEdtDeltAction();
	 		 			
	 	            },
	 	           error: function(xhr,textStatus, errorThrown){
	 		 			//$('#cover-spin').hide(0);
	 		 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
	 				}	 	            });
		        
		       
		    
		    
		    
		  } else if (
		    /* Read more about handling dismissals below */
		    result.dismiss === Swal.DismissReason.cancel
		  ) {
		    swalWithBootstrapButtons.fire(
		    		"Cancelled", "Your Tax Residential Details is safe :)", "error"
		    )
		  }
		})
}

function setTaxResiDets(data,taxNo,listId){
	
	//country field empty validation 
	if(isEmpty(data.taxCountry)){
		data.taxCountry = "NIL";
	}else{
		data.taxCountry = data.taxCountry;
	}
	
	//TIN Number empty validation 
	if(isEmpty(data.taxRefno)){
		data.taxRefno = "NIL";
	}else{
		data.taxRefno = data.taxRefno;
       }
	
	
	//Reason field empty validation 
	if(isEmpty(data.reasonToNotax)){
		data.reasonToNotax = "NIL";
	}else{
		data.reasonToNotax = data.reasonToNotax;	
	}
	
	//Remarks field empty validation 
	if(isEmpty(data.reasonbDetails)){
		data.reasonbDetails = "NIL";
	}else{
		data.reasonbDetails = data.reasonbDetails;
	}
	
	var listGroup='<a id='+data.taxId+' href="#" class="list-group-item list-group-item-action flex-column align-items-start ">'
    +'<div class="d-flex w-100 justify-content-between">'
      +'<h6 class="mb-1 text-primaryy font-sz-level6 bold">Tax#'+taxNo+'</h6>'
     +'<span class="font-sz-level7 bold text-custom-color-gp"><i class="fa fa-pencil-square-o mr-2"" aria-hidden="true" style="color:blue;" title="Edit Tax Details" onclick="editTaxDets(/'+data.taxId+'/)"></i>&nbsp;&nbsp;<i class="fa fa-trash checkdb" aria-hidden="true" style="color: red;" title="Delete Tax Details"  id="'+data.taxId+'" onclick="dltTaxData(this)"></i></span>'
     +'</div>'
     +'<div class="row">'
         +'<div class="col-md-6">'
         +'<span class="font-sz-level7 bold text-custom-color-gp"><strong>Country&nbsp; :&nbsp;</strong></span>&nbsp;<span class="font-sz-level7 font-normal">'+data.taxCountry+'</span>'
         +'</div>'
         +'<div class="col-md-6">'
     +'<span class="font-sz-level7 bold text-primary">TIN Number&nbsp;:&nbsp;'+data.taxRefno+'</span>'
         +'</div>'
       +'</div>'
         
           +'<div class="row">'
         +'<div class="col-md-4">'
        +'<span class="font-sz-level7 bold text-custom-color-gp"><strong>Reason&nbsp;:&nbsp;</strong></span><span class="font-sz-level7 font-normal">'+data.reasonToNotax+'</span>'
         +'</div>'
          +'<div class="col-md-8">'
        +'<span class="font-sz-level7 bold text-custom-color-gp"><strong>Remarks&nbsp;:&nbsp;</strong></span><span class="font-sz-level7 font-normal">'+data.reasonbDetails+'</span>'
         +'</div>'
      +'</div>'
    
  +'</a>';
	
	//hide self sps tax image while datalist > 0
	    if(listId == "selfList"){
	    	hideTaxImgSelf(listId);
	    }
	    if(listId == "spsList"){
	    	hideTaxImgSps(listId);
	    }
	    //end
	    
     $("#"+listId).append(listGroup);

}

function hideTaxImgSps(listId){
	$("#Taxcontent").find("#tab2").find("#NoTaxDetlsRow"+listId).removeClass("show").addClass("hide");
}

function hideTaxImgSelf(listId){
	$("#Taxcontent").find("#tab1").find("#NoTaxDetlsRow"+listId).removeClass("show").addClass("hide");
}


function showTaxImgSps(listId){
	$("#Taxcontent").find("#tab2").find("#NoTaxDetlsRow"+listId).removeClass("hide").addClass("show");
}

function showTaxImgSelf(listId){
	$("#Taxcontent").find("#tab1").find("#NoTaxDetlsRow"+listId).removeClass("hide").addClass("show");
}




function editTaxDets(taxId){
	//checkdb onchange function for edit operation
	enableReSignIfRequiredforEdtDeltAction();
	$.ajax({
            url:"fnadetails/taxdetails/taxid/"+taxId,
            type: "GET",
            dataType: "json",async:false,
            contentType: "application/json",
            
			success: function (data) {
				setTaxResModalData(data);
				
            },
            error: function(xhr,textStatus, errorThrown){
	 			//$('#cover-spin').hide(0);
	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			}
            });
	
	
}
function setTaxResModalData(data){
	$('#addModalFormDetls').modal('show');
	for(var i=0;i<arrTaxData.length;i++){
			
			if(!isEmpty(data[arrTaxData[i]])){
				document.getElementsByName(arrTaxData[i])[0].value=data[arrTaxData[i]];
			}else{
				document.getElementsByName(arrTaxData[i])[0].value = ""
			}
			
			
			
			
	}
}

function addModal(clientType){
	
	//check checkdb onchange function for AddingTax Details
	 enableReSignIfRequiredforEdtDeltAction();
	
	// set default country type singapore to self and spouse
	   $("#country").val("SINGAPORE");
	
	//set Slef and Nric here
    if(clientType == '/CLIENT/'){
    	$('#taxRefno').val($("#hdnSelfNric").val())
    	if(isEmpty($("#taxRefno").val())){
    		$("#reasonToNotax").removeAttr("disabled");
    	}else{
    		$("#reasonToNotax").prop("disabled",true);
    	}
		
	}
    //set Spouse Nric here 
    else if(clientType == '/SPOUSE/'){
    	$('#taxRefno').val($("#hdnSpsNric").val())
         if(isEmpty($("#taxRefno").val())){
        	 $("#reasonToNotax").removeAttr("disabled");
    	}else{
    		$("#reasonToNotax").prop("disabled",true);
    	}
    }
	
	var taxForStr=String(clientType)
	document.getElementsByName("taxFor")[0].value=taxForStr.substring(1,taxForStr.length-1);
	
}

$("#taxRefno").bind("change",function(){
	var taxVal = $(this).val()
	 if(isEmpty(taxVal)){
    	 $("#reasonToNotax").removeAttr("disabled");
    	 $("#reasonbDetails").prop("readOnly",false);
	}else{
		$("#reasonToNotax").prop("disabled",true);
		$("#reasonToNotax").val("");
		
		$("#reasonbDetails").prop("readOnly",true);
		$("#reasonbDetails").val("");
		
	}
})


function addTab(clientType,tabName,listId,optName,optOth,clntPrefix){
	
	var nextTab = $('#selfSpouseTabs li').length+1;
	
  	// create the tab
    $('<li class="nav-item"> <a class="nav-link" id="tab'+nextTab+'" data-toggle="tab" href="#tab'+nextTab+'"  onclick="openThisTabContArea(this)" role="tab" aria-controls="client2" aria-selected="false">'+tabName+'</a> </li>').appendTo('#selfSpouseTabs');
   
  	
  	
  	// create the tab content
  	//$('<div class="tab-pane" id="tab'+nextTab+'">tab' +nextTab+' content</div>').appendTo('.tab-content');
    var taxDetls = '<div class="tab-pane fade" id="tab'+nextTab+'" role="tabpanel" aria-labelledby="client2-tab">'
        +'<input type="hidden" id="hdnClientType" name="taxFor" value='+clientType+'>'
        +'<input type="hidden" id="hdnClientType" name="tabName" value='+tabName+'>'
		+'<div class="card mb-1  p-1 m-1 " id="">'
		+'<div class="card-body" style="border:1px solid #ddd;" id="">'
		+'<div class="row">'
		+'<div class="col-md-7">'	
			+'<div class="card">'																          
				+'<div class="card-header" id="cardHeaderStyle">'
					 +'<div class="row">'																                       
						+'<div class="col-md-8">'
						+'<div class="pt-1">Tax Residency Details</div>'
					  +'</div>'
			          +'<div class="col-md-4">'
						+'<button class="btn btn-sm btn-bs3-prime addtaxdetsbtn" style="background: green; border: green; float:right;" data-toggle="modal" data-target="#addModalFormDetls" onclick="addModal(/'+clientType+'/);"><i class="fa fa-plus" aria-hidden="true"></i> Add Tax Resi. Detls</button>'
					   +'</div>'
		         +'</div>' 
		+'</div>'
		
		//+'<div class="card-body" style="min-height: 70vh;overflow-y: scroll;overflow-x: hidden;">'
		
		+'<div class="card-body" style="height: 80vh;overflow-y: scroll;overflow-x: hidden;">'
		

		   +'<div class="row NoTaxDetlsRow'+listId+'" id="NoTaxDetlsRow'+listId+'">'
	                +'<div class="col-2"></div>'
	                +'<div class="col-8">'
	                 +'<img src="vendor/avallis/img/tax.gif" class="rounded mx-auto d-block img-fluid  prod-img" id="TaxImg" style="width: 100%;height: 45vh;border:1px solid grey;" >'
	                 +'<p class="center noData mt-2" id="NoTaxInfo"><img src="vendor/avallis/img/warning.png" class="mr-3">&nbsp;No Tax Details Available&nbsp;</p>'
	                +'</div>'
	                
	               +'<div class="col-2"></div>'
	        +'</div>'
		
+'<div class="list-group" id='+listId+'>'
   
 
 +'</div>'
		 +'</div>'
			+'</div>'
			+'</div>'
				+'<div class="col-md-5">'
					+'<div class="card h-100">'
					  +'<div class="card-header" id="cardHeaderStyle">Classification of Tax Residence Information</div>'
							+'<div class="card-body">'
								+'<div class="row pl-1  ">'
		                             +'<div class="col- "><img src="vendor/avallis/img/infor.png" class="mt-2"></div>'
		                                    +'<div class="col-11">'
			                                   +'<div class="col-">'
			                                       +'<span class="font-sz-level6 lh-20">If the country indicated in your mailing address/contact number is different from the country(ies) which you have disclosed as tax residence(s), please provide your explanation below. Otherwise, please disregard this section and proceed to Declaration. Please select <span class="text-primaryy bold txt-urline ">ONE</span> option only:</span>'
			                                   +'</div>'
		                                     +'</div>'
		                              +'</div>'
																					                           
								+'<ul class="list-group mt-2" id="ulLiStyle-01'+listId+'" class="selfSpsCRISec">'
									+'<li class="list-group-item">' 
									   +'<div class="form-check checkdb">'	
  											+'<label class="form-check-label checkdb">'
    											+'<input type="radio" class="form-check-input checkdb" id="clrSTU'+optName+'" name='+optName+' onclick="disableTextArea(/'+optOth+'/)"  value="'+clntPrefix+'STU" maxlength="60">I am a student'
  											 +'</label>'
										+'</div>'
										
									+'</li>'
									+'<li class="list-group-item">' 
									   +'<div class="form-check checkdb">'	
  											+'<label class="form-check-label checkdb">'
    											+'<input type="radio" class="form-check-input checkdb" id="clrCUL'+optName+'" name='+optName+' value="'+clntPrefix+'CUL" onclick="disableTextArea(/'+optOth+'/)" maxlength="60">I am on cultural/diplomatic purpose'
  											 +'</label>'
										+'</div>'
										
									+'</li>'
									+'<li class="list-group-item">' 
									   +'<div class="form-check checkdb">'	
  											+'<label class="form-check-label checkdb">'
    											+'<input type="radio" class="form-check-input checkdb" id="clrHWD'+optName+'" name='+optName+' value="'+clntPrefix+'HWD" onclick="disableTextArea(/'+optOth+'/)" maxlength="60">I am a housewife/dependent'
  											 +'</label>'
										+'</div>'
										
									+'</li>'
									+'<li class="list-group-item">' 
									   +'<div class="form-check checkdb">'	
  											+'<label class="form-check-label checkdb">'
    											+'<input type="radio" class="form-check-input checkdb" id="clrOTH'+optName+'" name='+optName+' value="'+clntPrefix+'OTH" onclick="enableTextArea(/'+optOth+'/);" maxlength="60">Others'
  											 +'</label>'
										+'</div>'
										+'<textarea class="form-control txtarea-hrlines text-wrap checkdb mt-1 " rows="3" id='+optOth+' name='+optOth+' maxlength="300" onkeydown="textCounter(this,300);" onkeyup="textCounter(this,300);"disabled></textarea><div class="font-sz-level7 mt-2 text-primaryy char-count"></div>'
									+'</li>'
																													  
									
								+'</ul>'
				+'</div>'
				+'</div>'
				+'</div>'
				+'</div>'
				+'</div>'
+'</div>'
+'</div>';
    
   
    
    $(taxDetls).appendTo('.tab-content');
  	
  	// make the new tab active
  	$('#selfSpouseTabs a:last').tab('show');
}

//click to show current tabContent only
function openThisTabContArea(obj){
    var tabid = $(obj).attr("id");
    
	var tabcontAreaId=[];
	$('#Taxcontent div').each(function(){
		var tabContId = $(this).attr("id");
		if(tabid == tabContId) {
			$(this).addClass("active show");
			$('#Taxcontent div').not(this).removeClass("active show");
		 }
	});
} 

function enableTextArea(optOth){
	
	var optStr=String(optOth);
	var opt=optStr.substring(1,optStr.length-1);
    $("#"+opt).prop("disabled", false);
}
function disableTextArea(optOth){
	var optStr=String(optOth);
	var opt=optStr.substring(1,optStr.length-1);
    $("#"+opt).prop("disabled", true);
     $("#"+opt).val("");
}
//Set and get tax residency

function updateSelfSpouse(selfSpouse){
        getUsTaxDets(selfSpouse);
        
		$.ajax({
            url:"FnaSelfSpouse/updateData",
            type: "PUT",
            dataType: "json",async:false,
            contentType: "application/json",
            data: JSON.stringify(selfSpouse),
            success: function (data) {
            	//alert(data);
            },
            error: function(xhr,textStatus, errorThrown){
	 			//$('#cover-spin').hide(0);
	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			}
            });

}
function saveData(){
	
	//alert(JSON.stringify(selfSpouseDets));
	updateSelfSpouse(selfSpouseDets);
	
}
  


function saveCurrentPageData(obj,nextscreen,navigateflag,infoflag){
//	var nextPge  = $(obj).attr("href");
	var frmElements = $('#fnaTaxForm :input').serializeObject();
	$('#fnaTaxForm input[type=checkbox]').each(function() {
		
		var chekboxvalue = "N"
		
		if(this.checked)chekboxvalue  = "Y";
		
		frmElements[this.name] = chekboxvalue; 
		
	});

//	    alert(JSON.stringify(frmElements))
 //    var frmElements={};
	//  getUsTaxDets(frmElements);
	    
		$.ajax({
	            url:"fnadetails/register/taxform",
	            type: "POST",
	            async:false,
	            dataType: "json",
	            contentType: "application/json",
	            data: JSON.stringify(frmElements),
	            success: function (response) {
	            //	alert(response);
//	            	$("#dataFormId").val(response.DATA_FORM_ID);
	            	if(navigateflag){
	            		window.location.href=nextscreen;	
	            	}
	            	
	            	
	            },
	            error: function(xhr,textStatus, errorThrown){
		 			//$('#cover-spin').hide(0);
		 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
				}
	            });
	  }


function enableReasonDets(obj) {
   
	if(isEmpty(obj.value)){
		$("#noteReasonA,#noteReasonB,#noteReasonC").trigger("mouseout");
		$("#reasonbDetails").prop("disabled",true);
		$("#reasonbDetails").val("");
		$(".char-count").addClass("hide").removeClass("show")
	}
	
	if (obj.value == "Reason B") {
		$("#noteReasonB").trigger("mouseover");
		$("#noteReasonA,#noteReasonC").trigger("mouseout");
		$("#reasonbDetails").prop("disabled",false);
		$(".char-count").addClass("hide").removeClass("show")
	}
	if(obj.value == "Reason A") {
		$("#noteReasonA").trigger("mouseover");
		$("#noteReasonB,#noteReasonC").trigger("mouseout");
		$("#reasonbDetails").prop("disabled",true);
		$("#reasonbDetails").val("");
		$(".char-count").addClass("hide").removeClass("show")
	}
	
	if(obj.value == "Reason C") {
		$("#noteReasonC").trigger("mouseover");
		$("#noteReasonA,#noteReasonB").trigger("mouseout");
		$("#reasonbDetails").prop("disabled",true);
		$("#reasonbDetails").val("");
		$(".char-count").addClass("hide").removeClass("show")
		
	}
}