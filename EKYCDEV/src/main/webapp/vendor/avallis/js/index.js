//var strGLBL_ACCESS_LEVEL = "";
//var AccessComboList = "";

var clientAutoCompList = [];

$(document).ready(function () {

	
	$('#cover-spin').hide(0);
	
	$('#searchIcon').hide();
    	$('#goPreviousArrow').hide();
    
    getGlblAcessLvlAgnts();
    		
	$('#srchClntModal').on('shown.bs.modal', function () {
		//GetAllData
		triggerClientDets();
	});

	$("#radNewClient").on("click",function(){
		 window.location.href="clientInfo";
	})
	
	//poovathi add on 08-06-2021
	var $input = $("#txtFldCustName");
	 $input.typeahead({
	  source:clientAutoCompList,
	  autoSelect: false
	});
	// End
	 
	 
	//Poovathi add fnaList select combo Select 2 Initialization on 05-05-2021
	 $('#selFldFnaList').select2({
	 	   templateResult: formatFnaListOpt,
	 	   placeholder: '--Select Archive below--'
	 });
	
});



	

$("#selSrchAdvName").on("change",function(){
	validatecurntAdvsr();
	clientAutoCompList.length=0;
	triggerClientDets();
	//clear preview recordes in Table
	searchtable.clear().draw();
	
})
function triggerClientDets() {
	
	setTimeout(function() { $('#cover-spin').show(0);}, 0);	
	setTimeout(function() {  
		getAllClntNames();
		$("#txtFldCustName").val("");
		$("#txtFldCustName").focus();
		
	}, 1000);
	
}


function getAllClntNames(){
	var custName = ''; 
	  var loggedAdvId = $("#selSrchAdvName").val() ;//$("#hTxtFldLoggedAdvId").val();
	  var globalAcsLvelAdvId = $("#selSrchAdvName option:selected").val();
	  if(!isEmpty(globalAcsLvelAdvId)) {
		

		   $.ajax({
	        url:baseUrl+"/customerdetails/getAllClients/"+globalAcsLvelAdvId,
	        type: "GET",
	        dataType: "json",async:false,
	        contentType: "application/json",
	        data : {"clientname":custName},
	        success: function (data) {
	        	for (var i=0; i<data.length; i++) {
	                 clientAutoCompList.push(data[i].custName);//Add Client Names in Autocomplete List Array
	            }
	        	
	        	
	        },
		        error: function(xhr,textStatus, errorThrown){
		 			$('#cover-spin').hide(0);
		 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
				}
	        });
		   
	  }else {
			$('#cover-spin').hide(0);
				//toastr.clear($('.toast'));
				//toastr.options = {  timeOut: "5000", extendedTimeOut: "1000"};
				//toastr["error"]("Select the Adviser");

	  }
	  
}


	   

var searchtable = $('#srchClntTable').DataTable({
//	 processing: true,
//     language: {
//         'loadingRecords': '&nbsp;',
//         'processing': '<div class="spinner"></div>'
//     } ,
    
	scrollX: true,
	scrollY:"300px",
	scroller: true,
	scrollCollapse:true,
	autoWidth:false,
//	paging:true,
//	pagingType: "simple",
	columnDefs: [
	                {
	                    "targets": [ 5 ],
	                    "visible": false,
	                    "searchable": false
	                }
	            ]
	
});

//commented by poovathi on 09-06-2021
/*$('#srchClntTable tbody').on('click', 'tr', function () {
	//$(this).addClass("bg-success");
	//$(this).css('cursor', 'pointer');
    var data = searchtable.row(this).data();
    
    var custId = data[5];
    
    window.location.href="clientInfo?c="+custId;
} );*/


//poovathi add on 03-02-2021

$('#srchClntTable tbody').on('click', 'tr', function () {
	//$(this).addClass("bg-success");
	//$(this).css('cursor', 'pointer');
    var data = searchtable.row(this).data();
    
    var custName = data[0]
    var custId = data[5];
    $.ajax({
        url:baseUrl+"/customerdetails/getAllFnaList/"+custId,
        type: "GET",
        //dataType: "json",
        async:false,
        contentType: "application/json",
        success: function (data) {
        	
        	$('#cover-spin').hide(0);
        	var fnaList = JSON.parse(data)
        	
        	var fnaListLen = fnaList.length;
        	
        	
        	
        	//var custName = $("#custName").val();
        	/*var fnaListInfo= '<span class="font-sz-level6  text-custom-color-gp" title="Total Archive List : '+fnaListLen+'">Client : <strong>'+custName+'</strong> totally have '
        	+'<span class="badge badge-pill badge-primary" title="'+fnaListLen+'">'+ fnaListLen +'</span>&nbsp;Archive List(s). Select Latest Archive and Click <strong>Proceed</strong> Button.</span>';
        	 $("#fnaListCountInfo").prop("title","Total Archive List : "+ fnaListLen );
        	 */
        	 
        	 var fnaListInfo ='<li class="list-group-item p-1 font-sz-level6 "><i class="fas fa-user text-secondary mx-2"></i><span>Client Name : </span><span><strong>'+custName+'</strong></span></li>'  
		            +'<li class="list-group-item p-1 font-sz-level6 "><i class="fas fa-list text-secondary mx-2" title="'+fnaListLen+'"></i>Total Archive List : '+
		            '<span class="badge badge-primary badge-pill">'+fnaListLen+'</span></li>';
		         
        	if(fnaListLen > 0){
        		$('#srchClntModal').modal('hide');
        		$('#fnaListModal').modal('show');
        		 //if fnaList exists ,have to enable search fnadetls radio button 
        		 $("input:radio[name=radfnaList]:last").prop('checked', true);
        		//show notification msg
        		 $("#fnaListCountInfo").html(fnaListInfo).addClass("show").removeClass("hide");
        		 /*$.each(fnaList,function(key,val){
	        		$.each(val,function(key,val){
	        			if((key == "fnaId")&&(val != undefined)){
	        				$("#selFldFnaList").find('optgroup#searchExistFnaList').append('<option value="'+val+'" >'+val+'</option>');
	        			}
		        	});
	        	});*/
        		 for (var i=0; i<fnaListLen; i++) {
        			 if( i ==0){
        				 $("#selFldFnaList").find('optgroup#searchExistFnaList').append('<option value="'+fnaList[i].fnaId+'" >Archive - '+(i+1)+' (Last Saved EKYC) </option>');
        			 }else{
        				 $("#selFldFnaList").find('optgroup#searchExistFnaList').append('<option value="'+fnaList[i].fnaId+'" disabled >Archive - '+(i+1)+' </option>');
        			 }
             		
             	 }
        		 
        		 $("#selFldFnaList").val($('#searchExistFnaList > option').eq(0).val());
   			     $('#selFldFnaList').trigger('change.select2');
   			     $("#hdnCustId").val(custId)
	   			     
   			    // $('#searchExistFnaList option:contains("'+selfName+'")').prop("disabled","disabled");
        		/*if(fnaListLen == 1){
        		    	
        		    	var fnaId = $('#searchExistFnaList > option').eq(1).val();
        		    	$('#fnaListModal').modal('hide');
        		    	//console.log("only one fna id"+ fnaId)
        		    	window.location.href="kycIntro?id="+fnaId;
        		    	
        			  //if only one fnalist present,have to set that fnaid in combo
        			  $("#selFldFnaList").val($('#searchExistFnaList > option').eq(1).val());
        			  //$('#selFldFnaList').trigger('change.select2');
	        	}*/
        		
        		/*if(fnaListLen > 1 ){
        			 //if only one fnalist present,have to set that fnaid in combo
        			  $('#finishedModal').modal('show');
        			  $("#selFldFnaList").val($('#searchExistFnaList > option').eq(0).val());
        			  $('#selFldFnaList').trigger('change.select2');
        			  
        		}*/
        		
        	}
        	
        	//if no fnaList exist navigate to create new fna Section
        	if(fnaListLen == 0){
        		$('#fnaListModal').modal('hide');
        		$("#fnaListCountInfo").html("").addClass("hide").removeClass("show");
        		//window.location.href="kycIntro"
        		
        		//poovathi add on 29-06-2021 to encrypt custId
        		custId =  window.btoa(custId);
        		window.location.href="clientInfo?c="+custId;
        	}
        	
        	
        	
        	
         },
         error: function(xhr,textStatus, errorThrown){
	 			$('#cover-spin').hide(0);
	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			}
        });
   
} );

//show Drag and drop Text Content...in File Upload Modal
$('#fnaListModal').on('shown.bs.modal', function () {
	$('#cover-spin').hide(0);
})
//End


function validateCrtNewFna(){
	 
	 var curFnaId = "";
	 var custId = $("#hdnCustId").val();
  	   var curOpt =  $('input[name="radfnaList"]:checked').val();
  	   if(curOpt == "SRCHFNA"){
  		   
  		   curFnaId = $("#selFldFnaList option:selected").val();
  		   
  		   if(isEmpty(curFnaId)){
  			    $("#searchExistFnaListError").addClass("show").removeClass("hide")
  			    $("#selFldFnaList").focus();
  			    return false;
  		   }else{
  			    $("#searchExistFnaListError").addClass("hide").removeClass("show") 
  			 }
  		 
		  	  //poovathi add on 29-06-2021 to encrypt custId and FnaID
		   	  custId =  window.btoa(custId);
		   	  curFnaId =  window.btoa(curFnaId);
  		      window.location.href="clientInfo?c="+custId+"&fnaId="+curFnaId;
  		   
  		 }else if(curOpt == "CRTFNA"){
  			 //window.location.href="CreateNewFNA"
  			 //poovathi add on 29-06-2021 to encrypt custId and FnaID
		   	  custId =  window.btoa(custId);
		   	  curOpt =  window.btoa(curOpt);
  			  window.location.href="clientInfo?c="+custId+"&newFna="+curOpt;
  		 }
  	   
  	   
}


// hide error message of fnaListCombo
function getFnaListVal(selObj){
	var  curFnaId = $("#selFldFnaList option:selected").val();
	  if(isEmpty(curFnaId)){
			    $("#searchExistFnaListError").addClass("show").removeClass("hide")
			    $("#selFldFnaList").focus();
			    return false;
		   }else{
			    $("#searchExistFnaListError").addClass("hide").removeClass("show") 
	  }
}


function createNewFnaFun(){
	 $("#searchExistFnaListError").addClass("hide").removeClass("show");
	 $('#selFldFnaList').val('');
	 $('#selFldFnaList').select2({
		   templateResult: formatFnaListOpt,
		   placeholder: '--Select FNA Id below--'
	});

}
 

function callAjaxParam(curFnaId){
	 
	//alert(curFnaId)
	 
	 $.ajax({
	        url:baseUrl+"/fnadetails/register/Fnadetails/"+curFnaId, 
	        type: "GET",
           dataType: "json",
           async:false,
	        contentType: "application/json",
	        success: function (data) {
	        	 //alert(data.FNA_ID)
	        	  var fnaId = window.btoa(data.FNA_ID);
	        	 window.location.href="kycIntro?id="+fnaId;
	       },
	       error: function(xhr,textStatus, errorThrown){
	 			$('#cover-spin').hide(0);
	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			}
   });
	 
	 }


//Poovathi add fnaList select combo Select 2 Initialization on 05-05-2021
    function formatFnaListOpt (fnaList) {
		
		  if (!fnaList.id ) {
			 return fnaList.text;
		  }
		  
		  if( fnaList.id == "Select FNA Id/Add New FNA Id"){
			  return fnaList.text;
		  }
		 
		  var image = "vendor/avallis/img/idCard.png";
		  var $fnaList = $(
		         '<span><img style="width:25px" src="' + image +'" class="img-flag" /> ' + fnaList.text + '</span>'
		  );
		  return $fnaList;
		} 
//end of code 09-06-2021

function getAllFnaSpouseDetails(){
	
	//validate Adviser field while click search Button
	if(!validatecurntAdvsr()){return;}
	
	setTimeout(function() { loaderBlock() ;}, 0);	
	setTimeout(function() {  
		
		// show search results table
		$("#srchClntModal").find(".modal-body").find("#srchClntTblSec").removeClass('hide').addClass('show');
		
		$("#srchClntModal").find(".modal-body").find("#noteInfo").text("Dont click anywhere in the window,Until it Searching Your Results !");
		
		//set overlay class for loader
//		$("#srchClntModal").find(".modal-body").find("#secOverlay").addClass("disabledMngrSec");
		
		// get client Name 
		var custName = ''; 
			
		var client = $("#txtFldCustName").val();
		
		if(client.length == 0){
			custName  = client;
		}else{
			custName  = client;
		}
		
	    var loggedAdvId = $("#hTxtFldLoggedAdvId").val();
	    var globalAcsLvelAdvId = $("#selSrchAdvName option:selected").val();
	    
//		var tblId = document.getElementById("srchClntTable");
//		var tbody = tblId.tBodies[0]; 
		
		searchtable.clear().draw();
		var col0="",col1="",col2="",col3="",col4="",col5="",col6="";var col7="";
		
		if(!isEmpty(globalAcsLvelAdvId)) {
			$.ajax({
			    url:baseUrl+"/customerdetails/getAllClients/"+globalAcsLvelAdvId,
	            type: "GET",
	            dataType: "json",async:false,
	            contentType: "application/json",
	            data : {"clientname":custName},
	            success: function (data) {
	            	
	            	
//	            	console.log("dada--------------------->"+data)
		            for (var i=0; i<data.length; i++) {
		            	
		            	col0=data[i].custName;
		            	
		            	//poovathi comment on 18-06-2021(to avoid duplicate client name in anutocomplete list)
		            	//clientAutoCompList.push(col0);
		            	
//		            	console.log("autocomplete List--------------------->"+clientAutoCompList)
		            	
		            	col1=data[i].custInitials;
		            	col2=data[i].nric;
		            	col3=data[i].dob;
		            	col4=data[i].resHandPhone;
		            	col5=data[i].custid;
		            	col6=data[i].custCateg;
		            	//col7 = '<a href="#" class="btn btn-link" role="">New FNA</a>';
		            	if(col6 != "COMPANY") {
		            		searchtable.row.add( [col0,col1,col2,col3,col4,col5] ).draw( false );
		            	}
		            	
		            	
	              }
	            	
	            	$("#srchClntModal").find(".modal-body").find('#srchClntLoader').removeClass("d-block").addClass("hide");
	            	//remove overlay class for loader
	            	$("#srchClntModal").find(".modal-body").find("#secOverlay").removeClass("disabledMngrSec");
	            	$("#srchClntModal").find(".modal-body").find(".noteInfoMsg").removeClass('hide').addClass('show');
	            	$("#srchClntModal").find(".modal-body").find("#noteInfo").text("Click a row to view the details");
	            },
	            error: function(xhr,textStatus, errorThrown){
		 			$('#cover-spin').hide(0);
		 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
				}
	            });
		}else {
			$('#cover-spin').hide(0);
			
			//toastr.clear($('.toast'));
			//toastr.options = {  timeOut: "5000", extendedTimeOut: "1000"};
			//toastr["error"]("Select the Adviser");
		}
		
		
	}, 1000);
	
	
}



function validatecurntAdvsr(){
	 var advId = $("#selSrchAdvName").val();
	 var Fld = "Error";
	 if(isEmptyFld(advId)){
		  $("#selSrchAdvName"+Fld).removeClass("hide").addClass("show");
		  $("#selSrchAdvName").addClass('err-fld');
		  $("#selSrchAdvName").focus();
	      return;
		}else{
		  $("#selSrchAdvName"+Fld).removeClass("show").addClass("hide");
		  $("#selSrchAdvName").removeClass('err-fld');
		  
	}
	
	 return true;
}


// get Adviser List Cobo based on Access Level
function getGlblAcessLvlAgnts(){
	
	//clear preview recordes in Table
	searchtable.clear().draw();
	//clr cutname field
	$("#txtFldCustName").val("");
	
	clearAdvCombo();
	var globalAcsLvel = $("#selGlblAcessLvl option:selected").val();
	var strloggedAdvStaffId = $("#hTxtFldLoggedAdvId").val();
	var strloggedUserStaffType = $("#hTxtloggedUsrStftype").val();
	var strloggedUsrMgrFlg = $("#hTxtloggedUsrMgrFlg").val();
	var strloggedUsrDistId ='DIS000000001';//$("#hTxtloggedUsrDistId").val();
	
	var advName = $("#hTxtFldLoggedAdvName").val();
	
	
	var param =  {"loggedAdvStaffId":strloggedAdvStaffId,"loggedUserStaffType":strloggedUserStaffType,"loggedUsrDistId":strloggedUsrDistId}
	if( globalAcsLvel == "MANAGER"){
		
		$.ajax({
			
			    url:"customerdetails/getGloblAcessList/",
	            type: "GET",
	            async:false,
	            dataType: "json",
	            contentType: "application/json",
	            data :param,
	            success: function (data) {
          
	           
                for (var i=0; i<data.length; i++) {
	        	    var newOpt = '<option value="'+data[i].advstfId+'">'+data[i].advstfName+'</option>'
	            	$("#selSrchAdvName").append(newOpt);
	        	    $("#selSrchAdvNameGlobal").append(newOpt);
                 } 	
               
               validatecurntAdvsr();
	            	
	         },
	         error: function(xhr,textStatus, errorThrown){
		 			//$('#cover-spin').hide(0);
		 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
				}
            });
	}
	
   if( globalAcsLvel == "ADVISER/STAFF"){
	   
	   if(strloggedUserStaffType == "PARAPLANNER") {
		   
		   var totalpara = paraplanadviser.length;
		   
			for(var para=0;para<totalpara;para++) {
				var jsonObj = paraplanadviser[para];

				var newOpt = '<option value="'+jsonObj.paraAdvStfId+'">'+jsonObj.paraAdvStfName+'</option>';
						$("#selSrchAdvName").append(newOpt);
						$("#selSrchAdvNameGlobal").append(newOpt);
			}
	   }else {
		
		   var newOpt = '<option value="'+strloggedAdvStaffId+'">'+advName+'</option>'
			   	   $("#selSrchAdvName").append(newOpt);
				   $("#selSrchAdvName").val(strloggedAdvStaffId);
				   
				   $("#selSrchAdvNameGlobal").append(newOpt);
				   $("#selSrchAdvNameGlobal").val(strloggedAdvStaffId);
				   
				   //hide Adviser field Error (if exists)
				   validatecurntAdvsr();
				   
				   // focus custname text Field
				   $("#txtFldCustName").val("");
				   $("#txtFldCustName").focus();
		 }
	   validatecurntAdvsr();
	}
   
   if( globalAcsLvel == "COMPANY"){
	   $.ajax({
			
		   url:"customerdetails/getGloblAcessListCompany/",
           type: "GET",
           async:false,
           dataType: "json",
           contentType: "application/json",
           data :param,
           success: function (data) {
        	   
        	
         for (var i=0; i<data.length; i++) {
       	    var newOpt = '<option value="'+data[i].advstfId+'">'+data[i].advstfName+'</option>'
           	$("#selSrchAdvName").append(newOpt);
       	     $("#selSrchAdvNameGlobal").append(newOpt);
            } 	
         
         validatecurntAdvsr();
           	
        },
        error: function(xhr,textStatus, errorThrown){
 			//$('#cover-spin').hide(0);
 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
		}
       });
	}
   
  
}//fun end

//clear combo values
function clearAdvCombo(){
	$("#selSrchAdvName").find('option').not(':first').remove();
	 $("#selSrchAdvNameGlobal").find('option').not(':first').remove();
}

//poovathi add on 15-06-2021
function clrTableData(){
	//clear preview recordes in Table
	searchtable.clear().draw();
}





