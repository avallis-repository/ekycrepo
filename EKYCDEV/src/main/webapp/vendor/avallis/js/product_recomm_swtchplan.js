var jsnDataFnaDetails = "";


$( document ).ready(function() {
	var fnaId=$("#hdnSessFnaId").val();
	// getFnaDets(fnaId);
	
	
	if(!isEmpty(jsnDataFnaDetails)  && !isJsonObjEmpty(jsnDataFnaDetails)){
		  for(var obj in jsnDataFnaDetails){
			  if (jsnDataFnaDetails.hasOwnProperty(obj)) {
				  var objValue = jsnDataFnaDetails[obj];
				  switch(obj){
//				  var arrProdRecomSwtchData=["swrepConflg","swrepAdvbyflg","swrepDisadvgflg","swrepProceedflg"];
				  case "swrepConflg":
				  case "swrepAdvbyflg":
				  case"swrepDisadvgflg":
				  case"swrepProceedflg":			  
					  $("#"+obj).val(objValue);
					  if(objValue == "Y" ){
						  $("#"+obj).val(objValue);
						 $("#"+obj+"Y").prop("checked",true);
						 $("#swrepConfDets").removeAttr("disabled");
					  }  else  if(objValue == "N" ) { 
						  $("#"+obj).val(objValue);
//						  $("#"+obj).prop("checked",false);
						  $("#"+obj+"N").prop("checked",true);
						  $("#swrepConfDets").prop("disabled",true);
						}else {
							 $("#"+obj).val("");
							 $("#"+obj+"Y").prop("checked",false);
							  $("#"+obj+"N").prop("checked",false);
							  
						}
					  break;
				  
				  default:
					  $("#"+obj).val(objValue);
				  }
				  
			  }
			  
		  }
	}
	
	var fnaId=$("#hTxtFldCurrFNAId").val();
	getAllSign(fnaId);

	setPageManagerSts();
	setPageAdminSts();
	setPageComplSts();
	
});


//Set and Get Product Switch Plan Recommendation
var arrProdRecomSwtchData=["swrepConflg","swrepAdvbyflg","swrepDisadvgflg","swrepProceedflg"];
function getFnaData(fnaDets){
	for(var i=0;i<arrProdRecomSwtchData.length;i++){
		fnaDets[arrProdRecomSwtchData[i]]=$('input[name='+arrProdRecomSwtchData[i]+']:checked').val();
	}
	fnaDets.swrepConfDets=$("#swrepConfDets").val();
}
function setFnaData(fnaDets){
	for(var i=0;i<arrProdRecomSwtchData.length;i++){
		if(fnaDets[arrProdRecomSwtchData[i]] == "Y"){
			$("#"+arrProdRecomSwtchData[i]+"Y").prop('checked',true);
		}
		$("#swrepConfDets").val(fnaDets.swrepConfDets);
	}
}

//SaveData
function saveData(){
	updateFnaDets();
}

function saveCurrentPageData(obj,nextscreen,navigateflg,infoflag){
	
	
//	$("#frmSwitchProdForm input[type='checkbox']").each(function(){
//		if(!this.checked){
//			$('#frmSwitchProdForm').append('<input type="hidden" name="'+this.name+'" value="N"/>');
//		}
		
//	});
	
	var frmElements = $('#frmSwitchProdForm :input').serializeObject();
	
/*$('#frmSwitchProdForm input[type=checkbox]').each(function() {
		
		var chekboxvalue = ""
		
		if(this.checked)chekboxvalue  = "Y";
		
		frmElements[this.name] = chekboxvalue; 
		
	});*/

	$.ajax({
	            url:"fnadetails/register/switchprod/",
	            type: "POST",
	            async:false,
	            dataType: "json",
	            contentType: "application/json",
	            data: JSON.stringify(frmElements),
	            success: function (response) {
					if(navigateflg){
	            		window.location.href=nextscreen;
					}
	            	
	            },
	            error: function(xhr,textStatus, errorThrown){
		 			//$('#cover-spin').hide(0);
		 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
				}
	            });
	  }
	


