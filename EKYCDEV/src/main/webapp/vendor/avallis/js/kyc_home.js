
var  strDataFrmId = "",jsnDataCustomerDetails ="",jsnDataSelfSpsDetails = "",jsnDataFnaDetails ="",
jsnFnaOtherPersDets="";




$(document).ready(function(){
	
	//defalut hide client(2) label when Spouse is not present
	if($("#MenuSpsName").text().length > 0 ) {
		$("#clnt2Text").addClass("show").removeClass("hide")
	}else{
		$("#clnt2Text").addClass("hide").removeClass("show")
	}
	//End
	
	 $('[data-toggle="popoverPersDetls"]').popover({
		  placement:'top',
	      html: true,
	      content: function() {
	        return $('#popover-contentPg2PersonalDetlsCardNotes').html();
	      }
	   
	  });
	  
	  
	  if(!isEmpty(jsnDataFnaDetails)  && !isJsonObjEmpty(jsnDataFnaDetails)){
		  for(var obj in jsnDataFnaDetails){
			  if (jsnDataFnaDetails.hasOwnProperty(obj)) {
				  var objValue = jsnDataFnaDetails[obj];
				  
				  var nodetype = $("#"+obj).prop('nodeName');
				  var inputtype = $("#"+obj).prop("type");
				  
				  switch(obj){
				  
				  
				  case"cdLanguages":
					  
					  var data=JSON.parse(objValue);
                      setJsonObjToElm(obj,data);

						if (!isEmpty(data) && !jQuery.isEmptyObject(objValue)) {
	 						if(data.OTH == "Y"){
                      
                       			$("input[name='cdLanguageOth']").removeClass('readOnlyText')
									.addClass('writeModeTextbox').prop('readonly',false).prop('disabled',false)
                      			$("#cdLanguageOth").removeAttr("disabled");
                       		}
						}
					  break;
					  
					  case"cdIntrprtflg":
//						  alert(objValue)
			      		if(objValue == "Y"){
			      			$("#cdIntrprtflg").prop("checked",true);
			      			$("#cdIntrprtflg").val("Y");
			      			 $("#tempIntprtDiv").find(":input").prop("disabled",false);
			      		//$("#cdIntrprtflg").trigger("click");
			      		}else {
			      			$("#cdIntrprtflg").prop("checked",false)
			      			$("#cdIntrprtflg").val("N")
			      			
			      			 $("#htflaneng").prop("checked",false);
			      	    	 $("#htflaneng").val('');
			      	    	 $("#cdLanguages").val('');
			      	    	 $(".kycLanguage").prop("checked",false);
			      	    	 $("#cdLanguageOth").val("")
			      	    	 
			      	    	$("#tempIntprtDiv").find(":input").prop("disabled",true);
			      		}
			      			//fnaAssgnFlg($("#"+key),'chk');
			      			//enableIntrprt($("#"+key));
			      		
//			      		fnaAssgnFlg($("#cdIntrprtflg"),'chk');
//			      		enableIntrprt($("#cdIntrprtflg"))
			      			break;
			      			
		       	 case "cdBenfownflg":
						  if(objValue == "Y"){
						 $("#cdBenfownflg").prop("checked",true);
						  $("#cdBenfownflg").val(objValue);
						  }else{
							  $("#cdBenfownflg").prop("checked",false);
							  $("#cdBenfownflg").val(objValue);  
						  }
						  
						  break;
						  
						  
						
							  
			  case "cdTppflg":
				  if(objValue == "Y"){
					  $("#cdTppflg").prop("checked",true);
					  $("#cdTppflg").val(objValue);
					  }else{
						  $("#cdTppflg").prop("checked",false);
						  $("#cdTppflg").val(objValue);  
					  }
						  
						  break;
						  
						 
							  
		     case "cdPepflg":
		    	 if(objValue == "Y"){
		    		
					  $("#cdPepflg").prop("checked",true);
					  $("#cdPepflg").val(objValue);
					  }else{
						  $("#cdPepflg").prop("checked",false);
						  $("#cdPepflg").val(objValue);  
					  }
						  
						  break;
						  
			  
				  default:		
					      if(inputtype == "radio" || inputtype == "checkbox"){
						  if(objValue == "Y"){
							  $("#"+obj).prop("checked",true);
							  $("#"+obj).val(objValue);
						  }
					  }
					  
					  $("#"+obj).val(objValue);
				  }
				  
			  }
			  
		  }

		
	  }
	  
	
	  if(!isEmpty(jsnDataSelfSpsDetails)  && !isJsonObjEmpty(jsnDataCustomerDetails)){
		
		  
//		  for (var i=0; i<jsnDataSelfSpsDetails.length; i++) {
		  for(var objects in jsnDataSelfSpsDetails){
			  
			  
			  if (jsnDataSelfSpsDetails.hasOwnProperty(objects)) {

	        		var objValue = jsnDataSelfSpsDetails[objects];
	        		
	        		var key = objects,value=objValue;
	        		//console.log(key , value)
	        			
	        			switch(key){
	        			
	        			case "dfSelfName":
	        				if(!isEmpty(value)){
	        					$("#"+key).val(value);
			      				$("#"+key).prop("readonly",true);
	        				}
	        				
	        				break;
	        				
	        				
	        			case "dfSelfNric":
	        				if(!isEmpty(value)){
	        					$("#"+key).val(value);
			      				$("#"+key).prop("readonly",true);
	        				}else{
	        					$("#"+key).val(value);
			      				$("#"+key).prop("readonly",false);
		  		              
	        				}
	        				break;
	        			
	        			  case "dfSelfDob":
	        				 if(!isEmpty(value)){
	        					 $("#"+key).val(value);
	        					 // set updated date in datepicker calendar
     		                     $('#simple-date1 .input-group.date').datepicker("update", value);	
     		                     $('#simple-date1 .input-group.date').datepicker("destroy") 
     		                   	 $("#"+key).prop("readonly",true);
	        				  }else {
	        					  $('#simple-date1 .input-group.date').datepicker("update", value);
	        					  $("#"+key).prop("readonly",false);
	        				  }
	        				break;
	        				
	        			case"dfSpsName":
			      			if(!isEmpty(value)){
//			      				add dynamic tab for spouse
			      				
			      				
//			      				$("#addNewClient2Btn").trigger("click");
			      				
			      				
			      				var nextTab = $('#myTab li').length+1;

//			      				$('#myTab').find("a.nav-link").removeClass("active");
			      			
			      				$('#myTab').find('li:nth-last-child(2)').before('<li class="nav-item" style="list-style-type:none;"><a class="nav-link" id="client'+nextTab+'-tab" data-toggle="tab" href= "#tab'+nextTab+'" role="tab" aria-controls="client'+nextTab+'" aria-selected="false" title="'+value+'"> '+value+'&nbsp;<i class="fa fa-trash-o checkdb delBtnclr pl-3" aria-hidden="true" style="font-size: larger;" id="'+value+'" title="Delete : '+value+'" onclick="deletInsured(this)" ></i></a> </li>');

//			      				$('#myTabContent').find("div.tab-pane").removeClass("active");
			      		   
			      			
			      				var dynamictab = $("#divSpouseDetsElems");
			      				$("#divSpouseDetsElems").removeClass("d-none");
	
			      		  	 
			      				$('<div class="tab-pane fade show" id="tab'+nextTab+'" role="tabpanel" aria-label="client'+nextTab+'-tab" ></div>').prepend(dynamictab).appendTo('#myTabContent');
			      				
			      				$("#"+key).val(value);
			      				$("#"+key).prop("readonly",true);   
			      				
			      				
			      				
			      			}
			      			break;
			      			
	        			 case "dfSpsNric":
	                        	
				      			if(!isEmpty(value)){
				      				$("#dfSpsNric").val(value);
					      			$("#dfSpsNric").prop("readonly",true);    
		  		                }else{
		  		                	$("#dfSpsNric").val(value);
		  		                	$("#dfSpsNric").prop("readonly",false); 
		  		                }
				      			
				      			break;	
				      			
	        			 case "dfSpsDob":
	        				 if(!isEmpty(value)){
	        					 $("#"+key).val(value);
     			      		     // set updated date in datepicker calendar
//     		                     $('#simple-date2 .input-group.date').datepicker("update", value);
	        					 $('#simple-date2 .input-group.date').datepicker("destroy");
	        					 $("#"+key).prop("readonly",true);
	        				  }else {
	        					  $('#simple-date2 .input-group.date').datepicker("update", value);
	        						 $("#"+key).prop("readonly",false);
	        				  }
	        				break;		
		        		
	        			case "dfSelfGender":
	        				
	        				var clntGndr = value;//isEmpty(value)?"M":value;
	        				if(clntGndr == "F"){
	        					$("INPUT[name=dfSelfGender]").val([value]);
	        					$("#radBtnSelffemale").prop("checked",true);
	        					$("#radBtnSelfmale").prop("checked",false);
	        				}else if(clntGndr == "M"){
	        					$("INPUT[name=dfSelfGender]").val([value]);
	        					$("#radBtnSelfmale").prop("checked",true);
	        					$("#radBtnSelffemale").prop("checked",false);
	        				}else {
	        					$("INPUT[name=dfSelfGender]").val("");
	        					$("#radBtnSelfmale").prop("checked",false);
	        					$("#radBtnSelffemale").prop("checked",false);
	        				}
	        				break;
	        				
	        			case "dfSpsGender":
	        				
	        				var spsGender = value;//isEmpty(value)?"M":value;
	        				if(spsGender == "F"){
	        					$("INPUT[name=dfSpsGender]").val([value]);
	        					$("#radBtnSpsmale").prop("checked",false);
	        					$("#radBtnSpsfemale").prop("checked",true);
	        				}else if(spsGender == "M"){
	        					$("INPUT[name=dfSpsGender]").val([value]);
	        					$("#radBtnSpsfemale").prop("checked",false);
	        					$("#radBtnSpsmale").prop("checked",true);
	        				}else {
	        					$("INPUT[name=dfSpsGender]").val("");
	        					$("#radBtnSpsfemale").prop("checked",false);
	        					$("#radBtnSpsmale").prop("checked",false);
	        				}
	        				
	        				break;
			      			
//			      		
	        				
	        			 case "dfSelfHomeaddr":
	        				    $("#dfSelfHomeaddr").val(value);
	        				    if(!isEmpty(value))$("#dfSelfHomeaddr").prop("readOnly",true);
	        				break;
	        				
	        			 case "dfSelfMailaddr":
	        				    $("#dfSelfMailaddr").val(value);
		        				break;
	        				
	        			case "dfSelfSameasregadd":
	        				if(value =='Y' || value == 'on'){
			      				   $("#chkSelfSameAddrFlg").prop("checked",true);
			      				   $("#chkSelfSameAddrFlg").val(value);
			      				   $("#dfSelfMailaddr").prop("readOnly",true);
			      			}
	        				break;
	        				
	        			case "dfSelfMailaddrflg":
	        				if(value =='Y'  || value == 'on'){
			      				   $("#chkSelfNoSameAddrFlg").prop("checked",true);
			      				   $("#chkSelfNoSameAddrFlg").val(value);
			      				   $("#dfSelfMailaddr").prop("readOnly",false)
			      			}
	        				break;	
	        				
	        			 case "dfSpsHomeaddr":
	        				    $("#dfSpsHomeaddr").val(value);
	        				    if(!isEmpty(value))$("#dfSpsHomeaddr").prop("readOnly",true);
	        				break;
	        				
        			     case "dfSpsMailaddr":
	        				  $("#dfSpsMailaddr").val(value);
	        				break;
	        				
	        			 case "dfSpsMailaddrflg":
	        				if(!isEmpty(value)){
			      				   $("#chkSpsNoSameAddrFlg").prop("checked",true);
			      				   $("#chkSpsNoSameAddrFlg").val(value)
			      				   $("#dfSpsMailaddr").prop("readOnly",false)
			      			}else{
			      				 $("#chkSpsSameAddrFlg").prop("checked",true);
			      				 $("#chkSpsSameAddrFlg").val(value)
			      				 $("#dfSpsMailaddr").prop("readOnly",true)
			      			}
	        				break;		
	        			
			      			
			      		case "dfSelfNationality":
			      		case "dfSpsNationality":
			      			if(!isEmpty(value)){
				      			if(value.toUpperCase() == "SG" || value == "Singaporean"){
				      				$("#"+key+"Sign").prop("checked",true);
				      				if(key == "dfSelfNationality"){
				      					$("#dfSelfNatyDetsOth").removeClass('show').addClass('invisible');
				      				}else if(key == "dfSpsNationality"){
				      					$("#dfSpsNatyDetsOth").removeClass('show').addClass('invisible');
				      				}
				      				
				      		    }else if(value.toUpperCase() == "SGPR" || value == "Singapore - PR"){
				      				$("#"+key+"Sign-PR").prop("checked",true);
				      				if(key == "dfSelfNationality"){
				      					$("#dfSelfNatyDets").removeClass('invisible').addClass('show');
				      				}else if(key == "dfSpsNationality"){
				      					$("#dfSpsNatyDets").removeClass('invisible').addClass('show');
				      				}
				      				
				      			}else{
				      				$("#"+key+"Oth").prop("checked",true).trigger("click");
				      				if(key == "dfSelfNationality"){
				      					$("#dfSelfNatyDets").removeClass('invisible').addClass('show');
				      				}else if(key == "dfSpsNationality"){
				      					$("#dfSpsNatyDets").removeClass('invisible').addClass('show');
				      				}
				      			}
			      			}else{
			      				$("#"+key+"Sign").prop("checked",false);
			      				$("#"+key+"Sign-PR").prop("checked",false);
		      					$("#"+key+"Oth").prop("checked",false);
		      					if(key == "dfSelfNationality"){
			      					$("#dfSelfNatyDetsOth").removeClass('show').addClass('invisible');
			      				}else if(key == "dfSpsNationality"){
			      					$("#dfSpsNatyDetsOth").removeClass('show').addClass('invisible');
			      				}
			      			}
			      			break;
			      			
//			      		case"dfSelfMartsts":
//			      			$("#dfSelfMartsts option").filter(function() {
//			      			    return $(this).text() ==value;
//			      			}).prop("selected", true);
//			      			break;
			      			
			      			
			      		
			      			
			      		case "dfSelfEngSpoken":
			      		case "dfSpsEngSpoken":
			      			if(value == "Y"){
			      				$("#"+key+"Y").prop("checked",true);
			      				$("#"+key+"N").prop("checked",false);
			      			}else if(value == "N"){
			      				$("#"+key+"Y").prop("checked",false);
			      				$("#"+key+"N").prop("checked",true);
			      			}else{
			      				$("#"+key+"Y").prop("checked",false);
			      				$("#"+key+"N").prop("checked",false);
			      			}
			      			break;
			      			
			      			
			      		case "dfSelfEngWritten":
			      		case "dfSpsEngWritten":
			      			if(value == "Y"){
			      				$("#"+key+"Y").prop("checked",true);
			      				$("#"+key+"N").prop("checked",false);
			      			}else if(value == "N"){
			      				$("#"+key+"Y").prop("checked",false);
			      				$("#"+key+"N").prop("checked",true);
			      			}else{
			      				$("#"+key+"Y").prop("checked",false);
			      				$("#"+key+"N").prop("checked",false);
			      			}
							
			      			break;
			      			
			      		case "dfSelfEdulevel":
			      		case "dfSpsEdulevel":
							if(document.getElementsByName(key+"Rad")){
								for(var cnt1=0;cnt1<document.getElementsByName(key+"Rad").length;cnt1++){
									if(document.getElementsByName(key+"Rad")[cnt1].value ==value){
										document.getElementsByName(key+"Rad")[cnt1].checked=true;
									}
								}
							}
//							document.getElementsByName(key)[0].value==value;
							if(key == "dfSelfEdulevel"){
								$("input[name='dfSelfEdulevel']").val(value);
							}
							if(key == "dfSpsEdulevel"){
								$("input[name='dfSpsEdulevel']").val(value);
							}
							break;
			      			
			      		case'dfSelfFundsrc':
			                       var data=JSON.parse( value );
			                       setJsonObjToElm(key,data);
			                      if( $("#chkCDClntSrcIncmOthrs").is(":checked")){
			                    	  $("input[name='dfSelfFundsrcDets']").removeClass('readOnlyText').addClass('writeModeTextbox').prop('readonly',false)
			                    	  .prop('disabled',false).removeAttr("disabled");
			                      }
								$("#dfSelfFundsrc").val(value);
							break;

			      		case'dfSpsFundsrc':
			                       var data=JSON.parse( value );
			                       setJsonObjToElm(key,data);
			                       if( $("#chkCDSpsSrcIncmOthrs").is(":checked")){
				                    	  $("input[name='dfSpsFundsrcDets']").removeClass('readOnlyText').addClass('writeModeTextbox').prop('readonly',false)
				                    	  .prop('disabled',false);
				                   }
			                   $("#dfSpsFundsrc").val(value);    
						break;
						
//			      		case"cdIntrprtflg":
//			      			fnaAssgnFlg($("#"+key),'chk');
//			      			enableIntrprt($("#"+key));
//			      			break;
			      			
			      		case "dfSelfBusinatr":
			      		case "dfSpsBusinatr":
			      			 if(key == "dfSelfBusinatr"){
			      				if (value == 'Others'){
			      					 $("#"+key).val(value);
			      			  		 $("#dfSelfBusNatOthSec").removeClass("invisible").addClass("show");
			      			  	}else{
			      			  		$("#dfSelfBusNatOthSec").removeClass("show").addClass("invisible");
			      			  	    $("#"+key).val(value);
			      			  	}  
			      		  }
				      		  if(key == "dfSpsBusinatr"){
				      			    if (value == 'Others'){
				      			    	 $("#dfSpsBusNatOthSec").removeClass("invisible").addClass("show");
				      			  		 $("#"+key).val(value);
				      			  	}else{
				      			  		$("#dfSpsBusNatOthSec").removeClass("show").addClass("invisible");
				      			    	$("#"+key).val(value);
				      			  	} 
				      		  }
			      			break;
			      			
			      			//vignesh add on 13-05-2021		
			      		   case "dfSelfBirthcntry":
			      			     $("#dfSelfBirthcntry").val(value); 
			      			break;
			      			
			      			//poovathi add on 03-06-2021
			      		    case "dfSpsBirthcntry":
			      			   $("#dfSpsBirthcntry").val(value); 
			      			break;
			      			
			      			default:
			      				$("#"+key).val(value);
		        		
		        		}
	        			
//	        		});
	        		
			  
		      }
			  
		  }
		  
		  
		  
	  }
		  //No Self Spouse but only customer details
		 if(!isEmpty(jsnDataCustomerDetails)  && isJsonObjEmpty(jsnDataSelfSpsDetails)   ){
		  var resAddr1="",resAddr2="",resAddr3="",city="",state="",country="",pstlCode="";
//		  console.log("inside log");
		  for ( var cont in jsnDataCustomerDetails) {
	    	  

	      	if (jsnDataCustomerDetails.hasOwnProperty(cont)) {

	      		var contvalue = jsnDataCustomerDetails[cont];
	      		
//	      		console.log(cont,contvalue);
	      		//Address Details
		      		if(!isJsonObjEmpty(jsnDataCustomerDetails)){
		      			
		      			switch(cont){
			      		
			      		
			      		case "custName":
			      			$("#dfSelfName").val(contvalue);
			      			$("#dfSelfName").prop("readonly",true);
			      			break;
			      			
			      		case "dob":
                             if(!isEmpty(contvalue)){
                            	 $("#dfSelfDob").val(contvalue);
                            	 $("#dfSelfDob").prop("readonly",true);
     			      		    // set updated date in datepicker calendar
     		                    $('#simple-date1 .input-group.date').datepicker("update", contvalue);	
//     		                   $('#simple-date1 .input-group.date').datepicker("update", value);	
     		                   $('#simple-date1 .input-group.date').datepicker("destroy") 
	        				  }
			      			
			      			break;
			      			
			      		case "nric":
			      			
			      			if(!isEmpty(contvalue)){
			      				$("#dfSelfNric").val(contvalue);
				      			$("#dfSelfNric").prop("readonly",true);    
	  		                }else{
	  		                	$("#dfSelfNric").val(contvalue);
	  		                	$("#dfSelfNric").prop("readonly",false); 
	  		                }
			      			
			      			break;
			      			
			      		case "maritalStatus":
			      			$("#dfSelfMartsts").val(contvalue);
			      			break;
			      			
			      		case "sex":
							var clntGndr = contvalue;
							
							
							if(clntGndr == "F"){
	        					$("INPUT[name=dfSelfGender]").val([value]);
	        					$("#radBtnSelffemale").prop("checked",true);
	        					$("#radBtnSelfmale").prop("checked",false);
	        				}else if(clntGndr == "M"){ 
	        					$("INPUT[name=dfSelfGender]").val([value]);
	        					$("#radBtnSelfmale").prop("checked",true);
	        					$("#radBtnSelffemale").prop("checked",false);
	        				}else {
	        					$("INPUT[name=dfSelfGender]").val("")
	        					$("#radBtnSelffemale").prop("checked",false);
	        					$("#radBtnSelfmale").prop("checked",false);
	        					
	        				}

							/*if(clntGndr == "F"){
								$("#btndfSelfGender").trigger("click");
								$("#btndfSelfGender").addClass("active");
								$("#dfSelfGender").val("F");
							}else{
								$("#btndfSelfGender").removeClass("active");
								$("#dfSelfGender").val("M");
							}*/
							break;
			      			
			      		case "resCountry":
			      			$("#dfSelfRescountry").val(contvalue);
			      			country = contvalue;
			      			break;
			      			
			      		case "nationality":
			      			var nationty = contvalue;
			      			if(nationty == "Singaporean" || nationty == "SG"){
			      				$('#dfSelfNationalitySign').prop('checked',true);
			      				$("#dfSelfNatyDets").removeClass('show').addClass('invisible');
			      				//poovathi fix 0n 17-06-2021
			      			}else if(nationty == "Singaporean-PR" || nationty == "SGPR"  || nationty == "Singapore-PR"){
			      				$('#dfSelfNationalitySign-PR').prop('checked',true);
                    	//vignesh add on 24-05-2021
			      				$("#dfSelfNatyDets").removeClass('invisible').addClass('show');
			      				$("#dfSelfNatyDets").val("");
			      			}else{
			      				if(isEmpty(nationty)) {
			      					$('#dfSelfNationalityOth').prop('checked',false);
//				                	$("#dfSelfNatyDets").removeClass('invisible').addClass('show');
//				                	$("#dfSelfNatyDets").val(nationty);
			      				}else {
			      					$('#dfSelfNationalityOth').prop('checked',true);
				                	$("#dfSelfNatyDets").removeClass('invisible').addClass('show');
				                	$("#dfSelfNatyDets").val(nationty);	
			      				}
			                	
			      			}
			      			break;
			      			
			      		//Address Details
			      		case "resAddr1":
			      			$("#"+cont).val(contvalue);
			      			
			      			if(!isEmpty(contvalue)){
	  		               
	  		                	resAddr1 = contvalue;
	                        }
			      			break;
			      			
			      		case "resAddr2":
			      			$("#"+cont).val(contvalue);
			      			if(!isEmpty(contvalue)){
	  		                	resAddr2 = contvalue;
	                        }
			      			
			      			break;
			      			
			      		case "resAddr3":
			      			$("#"+cont).val(contvalue);
			      			if(!isEmpty(contvalue)){
	  		                	resAddr3 = contvalue;
	                        }
			      			
			      			break;
			      			
			      		case "resCity":
			      			$("#"+cont).val(contvalue);
			      			if(!isEmpty(contvalue)){
	  		                	city = contvalue;
	                        }
			      			
			      			break;
			      			
			      		case "resState":
			      			$("#"+cont).val(contvalue);
			      			if(!isEmpty(contvalue)){
	  		                	state = contvalue;
	                        }
			      			
			      			break;
			      			
			      			
			      		case "resPostalcode":
			      			$("#"+cont).val(contvalue);
			      			if(!isEmptyFld(contvalue)){
	  		                	pstlCode = contvalue;
	                        }
			      			 
			      			break;
			      			
			      		//Contact Details Fields
			      		case "resHandPhone":
			      			$("#dfSelfMobile").val(contvalue);
			      			break;
			      			
			      		case "emailId":
			      			$("#dfSelfPersemail").val(contvalue);
			      			break;
			      	    
			      		case "resPh":
			      			$("#dfSelfHome").val(contvalue);
			      			break;
			      			
			      		case "offPh":
			      			$("#dfSelfOffice").val(contvalue);
			      			break;
			      			
			      		
			      			
			      		// Emp &Finance Dtls	
			      		case "companyName":
			      			$("#dfSelfCompname").val(contvalue);
			      			break;
			      			
			      		case "occpnDesc":
			      			$("#dfSelfOccpn").val(contvalue);
			      			break;
			      			
			      		case "income":
			      			$("#dfSelfAnnlincome").val(contvalue);
			      			break;
			      			
			      		case "businessNatr":
			      			if(contvalue == 'Others'){
			      				$("#dfSelfBusinatr").val(contvalue);
			   		    		$("#dfSelfBusNatOthSec").removeClass("invisible").addClass("show")
			   		    	}else{
			   		    		$("#dfSelfBusinatr").val(contvalue);
			   		    		$("#dfSelfBusNatOthSec").removeClass("show").addClass("invisible");
			   		    		$("#dfSelfBusinatrDets").val("");
			   				}
			      			
			      		    break;
			      		case "businessNatrDets":
			      			$("#dfSelfBusinatrDets").val(contvalue);
			      			break;
			      			
			      			//vignesh add on 13-05-2021		
			      		case "countryofBirth":
			      			 $("#dfSelfBirthcntry").val(contvalue); 
			      			break;
			      		 
			      			
	//		      			height missing
	//		      			weight missing
			      			
			      		default:
			      			$("#"+cont).val(contvalue);
			      			
			      		}
		      			
		      		
		      			
 		            	
		      			 
		      			
		      		}
	      		
	      		
	      		
	      		
	            }

	          }

	var addr_f="";
	if(!isEmpty(resAddr1)){
		addr_f+=resAddr1
	}
	if(!isEmpty(resAddr2)){
		addr_f+=","+resAddr2
	}
	
	if(!isEmpty(resAddr3)){
		addr_f+=","+resAddr3
	}
	if(!isEmpty(city)){
		addr_f+=","+city
	}
	if(!isEmpty(state)){
		addr_f+=","+state
	}
	if(!isEmpty(country)){
		addr_f+=","+country
	}
	if(!isEmpty(pstlCode)){
		addr_f+=","+pstlCode
	}
	
 					    $('#dfSelfHomeaddr').val(addr_f).prop("readOnly",true)
 					  
 					   //Default check self Mail Address Checkbox
		      		 	$('#chkSelfSameAddrFlg').prop("checked",true);
		      		 	$('#chkSelfSameAddrFlg').val("Y");
		      		if($("#chkSelfSameAddrFlg").is(":checked")) {
		      			$('#dfSelfMailaddr').val($('#dfSelfHomeaddr').val());
		      			$('#dfSelfMailaddr').prop("readOnly",true)
		      		}
		      		
		      		if($("#chkSpsSameAddrFlg").is(":checked")) {
		      			$('#dfSpsMailaddr').val($('#dfSpsHomeaddr').val());
		      			$('#dfSpsMailaddr').prop("readOnly",true)
		      		}
		      		 	
		      			
		      		   
		      			
		  
	  }
		 
		 /*var newinsureddata = getAllClients();
		 
		 $('#selFldNRICName').select2({
			   templateResult: formatClientOpt,
			   placeholder: 'Select a Client Name'
         });*/
		 
		 

for ( var cont in jsnFnaOtherPersDets) {
var jsonite = jsnFnaOtherPersDets[cont]
for(it in jsonite)
	if (jsonite.hasOwnProperty(it)) {
		if(it == "radFldCDBenfJob" || it == "radFldCDTppJob" || it == "radFldCDPepJob" || it == "radCDTppPayMode"){
			$('input[name="'+it+'"][value="'+jsonite[it]+'"]').prop("checked",true);
		}
		$("#"+it).val(jsonite[it]);
	}

}
		 
		 if($('#myTab li').length == 4){
			$('#myTab').find("#btnAddNewInsur").children(".btn").css("cursor","not-allowed");
			$('#myTab').find("#btnAddNewInsur").children(".btn").attr("data-toggle","");
			$('#myTab').find("#btnAddNewInsur").children(".btn").attr("data-target","");
		 }
		 
//		 else if($('#myTab li').length < 4){
//			 $('#myTab').find("#btnAddNewInsur").children(".btn").css("cursor","pointer");
//			 $('#myTab').find("#btnAddNewInsur").children(".btn").attr("data-toggle","modal");
//			 $('#myTab').find("#btnAddNewInsur").children(".btn").attr("data-target","#new2ModalScrollable");
//		 }
		 
		 
		 if($("#chkSelfSameAddrFlg").is(":checked")){
			 sameAsResiAddr($("#chkSelfSameAddrFlg"),'chkSelfNoSameAddrFlg')
		  }
		 
		 
		 setCurrentPageData(jsnDataFnaDetails);
		 setCurrentPageData(jsnDataSelfSpsDetails);
		 $("#expandcollape").trigger("click");
	var fnaId=$("#hTxtFldCurrFNAId").val();
	getAllSign(fnaId);

	setPageManagerSts();
	setPageAdminSts();
	setPageComplSts();
      
}); 


function formatClientOpt (client) {
	
	  if (!client.id ) {
		  
	    return client.text;
	  }
	  
	  if( client.id == "Select Client/Add New Client"){
		  return client.text;
	  }
	 
	  
	  var image = "vendor/avallis/img/client.png";
	  var $clients = $(
	    '<span><img style="width:25px" src="' + image +'" class="img-flag" /> ' + client.text + '</span>'
	  );
	  return $clients;
	};

function getAllClients(){
	
	var loggedAdvId = $("#hTxtFldLoggedAdvId").val();
	var currAdvStfId = $("#currAdviserId").val();	
	var loggedUserStfLvl = $("#hTxtloggedUsrStftype").val();
	
	if (loggedUserStfLvl == 'PARAPLANNER'){
		loggedAdvId = 		currAdvStfId;
	}
	
	var col0="",col1="",col2="",col3="";
	
	var selfName = $("#dfSelfName").val();
	$.ajax({
            url:baseUrl+ "/customerdetails/getAllClients/"+loggedAdvId,
            type: "GET",
            async:false,
 			data : {"clientname":""},
            dataType: "json",
            contentType: "application/json",
            
            success: function (data) {
	
	//$("#loadermodal").modal("hide")
	            for (var i=0; i<data.length; i++) {
	            	
	            	col0=data[i].custName;
	            	col1=data[i].dob;
	            	col2=data[i].resHandPhone;
	            	col3=data[i].custid;
	            	
	            	$("#selFldNRICName").find('optgroup#searchExistGrp').append('<option value="'+col3+'" >'+col0+'</option>');
	            	
              }
	            
	            $('#selFldNRICName option:contains("'+selfName+'")').prop("disabled","disabled");

            },
            error: function(xhr,textStatus, errorThrown){
	 			//$('#cover-spin').hide(0);
	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			}
            });
	
	
	
	
//	return (allClientData);
}


function getClientById(custId){
	
	var resAddr1="",resAddr2="",resAddr3="",city="",state="",country="",pstlCode="";
	var addr_f="";
	$.ajax({
            url:"customerdetails/getClientsById/"+custId,
            type: "GET",
            async:false,
            dataType: "json",
            contentType: "application/json",
            
            success: function (data) {
            	
            	
          		  
          		  for ( var cont in data) {
          	    	  

          	      	if (data.hasOwnProperty(cont)) {

          	      		var contvalue = data[cont];
          	      		
          	      		
          	      		//Address Details
          	      	
          	      		switch(cont){
          	      		
          	      		
          	      		case "custName":
          	      			$("#dfSpsName").val(contvalue);
          	      			$("#dfSpsName").prop("readonly",true);
          	      			break;
          	      			
          	      		case "dob":
          	      		    if(!isEmpty(contvalue)){
	          	      			$("#dfSpsDob").val(contvalue);
				      		    // set updated date in datepicker calendar
			                    $('#simple-date2 .input-group.date').datepicker("update", contvalue);	
       				       }
          	      			
          	      			break;
          	      			
          	      		case "nric":
          	      			
          	      		  if(!isEmpty(contvalue)){
		      				$("#dfSpsNric").val(contvalue);
			      			$("#dfSpsNric").prop("readonly",true);    
  		                   }else{
  		                	$("#dfSpsNric").val(contvalue);
  		                 	$("#dfSpsNric").prop("readonly",false); 
  		                    }
          	      			
          	      			break;
          	      			
          	      		case "maritalStatus":
          	      			$("#dfSpsMartsts").val(contvalue);
          	      			break;
          	      			
          	      		case "sex":
          					var clntGndr = contvalue;
          					if(clntGndr == "F"){
          						$("#radBtnSpsfemale").trigger("click");
          						$("#btndfSpsGender").addClass("active");
          						$("#dfSpsGender").val("F");
          					}else{
          						$("#radBtnSpsmale").removeClass("active");
          						$("#dfSelfGender").val("M");
          					}
          					break;
          	      			
          	      		
          	      		case "nationality":
          	      			var nationty = contvalue;
          	      			if(nationty == "Singaporean" || nationty == "SG"){
          	      				$('#dfSpsNationalitySign').prop('checked',true);
          	      				$("#dfSpsNatyDets").removeClass('show').addClass('invisible');
          	      				//poovathi fix on 17-06-2021
          	      		    }else if(nationty == "Singaporean-PR" || nationty == "SGPR"  || nationty == "Singapore-PR"){
			      				$('#dfSpsNationalitySign-PR').prop('checked',true);
                    	//vignesh add on 24-05-2021
			      				$("#dfSpsNatyDets").removeClass('invisible').addClass('show');
			      				$("#dfSpsNatyDets").val("");
			      			
          	      			}else{
          	      				if(isEmpty(nationty)) {
	          	      				$('#dfSpsNationalityOth').prop('checked',false);
//	          	                	$("#dfSpsNatyDets").removeClass('invisible').addClass('show');
//	          	                	$("#dfSpsNatyDets").val(nationty);
          	      				}else {
	          	      				$('#dfSpsNationalityOth').prop('checked',true);
	          	                	$("#dfSpsNatyDets").removeClass('invisible').addClass('show');
	          	                	$("#dfSpsNatyDets").val(nationty);	
          	      				}
          	                	
          	      			}
          	      			break;
          	      			
          	      		//Address Details
          	      		case "resAddr1":
          	      		    if(isEmptyFld(contvalue)){
          	      		     }else{
          	      			   resAddr1 = contvalue;
          	         	      }
//          	      			$("#"+cont).val(contvalue);
          	      			
          	      			break;
          	      			
          	      		case "resAddr2":
//          	      			$("#"+cont).val(contvalue);
          	      		       if(isEmptyFld(contvalue)){
     	      		           }else{
     	      		    	      resAddr2 = contvalue;
     	         	            }
          	      			
          	      			break;
          	      			
          	      		case "resAddr3":
//          	      			$("#"+cont).val(contvalue);
          	      	        	if(isEmptyFld(contvalue)){
	      		                }else{
	          	      			  resAddr3 = contvalue;
                                }
          	      			break;
          	      			
          	      		case "resCity":
//          	      			$("#"+cont).val(contvalue);
		          	      		if(isEmptyFld(contvalue)){
		  		                }else{
		  		                	city = contvalue;
		                        }
          	      			
          	      			break;
          	      			
          	      		case "resState":
//          	      			$("#"+cont).val(contvalue);
		          	      		if(isEmptyFld(contvalue)){
		  		                }else{
		  		                	state = contvalue;
		                        }
          	      			
          	      			break;
          	      			
          	      			
          	      		case "resPostalcode":
//          	      			$("#"+cont).val(contvalue);
		          	      		if(isEmptyFld(contvalue)){
		  		                }else{
		  		                	pstlCode = contvalue;
		                        }
          	      			
          	      			break;
          	      			
          	      	   case "resCountry":
//  	      			     $("#dfSelfRescountry").val(contvalue);
		          	      		if(isEmptyFld(contvalue)){
		  		                }else{
		  		                	country = contvalue;
		  		                	 
		                        }
  	      			         
  	      			         break;
  	      				
          	      			
          	      		//Contact Details Fields
          	      		case "resHandPhone":
          	      			$("#dfSpsHp").val(contvalue);
          	      			break;
          	      			
          	      		case "emailId":
          	      			$("#dfSpsPersemail").val(contvalue);
          	      			break;
          	      	    
          	      		case "resPh":
          	      			$("#dfSpsHome").val(contvalue);
          	      			break;
          	      			
          	      		case "offPh":
          	      			$("#dfSpsOffice").val(contvalue);
          	      			break;
          	      			
          	      		
          	      			
          	      		// Emp &Finance Dtls	
          	      		case "companyName":
          	      			$("#dfSpsCompname").val(contvalue);
          	      			break;
          	      			
          	      		case "occpnDesc":
          	      			$("#dfSpsOccpn").val(contvalue);
          	      			break;
          	      			
          	      		case "income":
          	      			$("#dfSpsAnnlincome").val(contvalue);
          	      			break;
          	      			
          	      		
          	      	case "businessNatr":
		      				if(contvalue == 'Others'){
		      				//	alert(contvalue)
		      				$("#dfSpsBusinatr").val(contvalue);
		   		    		$("#dfSpsBusNatOthSec").removeClass("hide").addClass("show")
		   		    	}else{
		   		    		$("#dfSpsBusinatr").val(contvalue);
		   		    		$("#dfSpsBusNatOthSec").removeClass("show").addClass("hide");
		   		    		$("#dfSpsBusinatrDets").val("");
		   				    }
		      	     break;
		      	     
          	      case "businessNatrDets":
          	    	  $("#dfSpsBusinatrDets").val(contvalue);
		      			break;			
          	      			
                //vignesh add on 13-05-2021		      			
          	    case "countryofBirth":
	      		    $("#dfSpsBirthcntry").val(contvalue); 
	      			break;
	      			
          	      			
          	      			
//          	      			height missing
//          	      			weight missing
          	      			
          	      		default:
          	      			$("#"+cont).val(contvalue);
          	      			
          	      		}
          	      		
	              
            	      		
            	  	
          	      	}
          	      	
          	      

          	          }
          		  
          		
          	      		 
            	
	            
            },
            error: function(xhr,textStatus, errorThrown){
	 			//$('#cover-spin').hide(0);
	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			}
            });
	
	
	
	    if(!isEmpty(resAddr1)){
	  		
	  		addr_f+=resAddr1
	  	}
	  	if(!isEmpty(resAddr2)){
	  		addr_f+=","+resAddr2
	  	}
	  	
	  	if(!isEmpty(resAddr3)){
	  		addr_f+=","+resAddr3
	  	}
	  	if(!isEmpty(city)){
	  		addr_f+=","+city
	  	}
	  	if(!isEmpty(state)){
	  		addr_f+=","+state
	  	}
	  	if(!isEmpty(country)){
	  		addr_f+=","+country
	  	}
	  	if(!isEmpty(pstlCode)){
	  		addr_f+=","+pstlCode
	  	}
	  
	      	$('#dfSpsHomeaddr').val(addr_f);
	        $('#dfSpsMailaddr').val( $('#dfSpsHomeaddr').val());
     		    //Default check sps Mail Address Checkbox
     		 	$('#chkSpsSameAddrFlg').prop("checked",true);
     		    $('#chkSpsSameAddrFlg').val("Y");
	
	
	
	
//	return (allClientData);
}
      
    	 

      

//KYC OLD page 14 function reuse here

function fnaAssgnFlg(obj, id) {
	if (id == "chk") {
		if (obj.checked) {
			hideInptrLanSecError();
			obj.value = "Y";
		} else if (!obj.checked) {
		    obj.value = "";
		}
		
	} else {
		if (obj.checked) {
			document.getElementById(id).value = obj.value;
		}
	}
}
	






//Education Level Fun

function setEduLevel(rad,txtName){
	if(rad.checked){
		document.getElementsByName(txtName)[0].value=rad.value;
	}
	checkChanges(document.getElementsByName(txtName)[0])
}

function checkChanges(tblcellelem){
	var chkanychange = $("#chkanychange");
	var jsonvalues = JSON.parse(isEmpty(chkanychange.val()) ? "{}" : chkanychange.val());
	var newobj=jsonvalues;
	newobj[tblcellelem.name]=tblcellelem.value;
	chkanychange.val(JSON.stringify(newobj));
    

}

function fnaNaltyFlg(chkbox,Ele){
	var chkd = chkbox.checked;
	var val = $(chkbox).val();
	var fldId = $(chkbox).attr("id");
		
	if((chkd && val == "SG")){ //||
		$("#"+Ele).removeClass("show").addClass("invisible");
		removErrMsgInChat('dfSelfNationalitySign');
		removErrMsgInChat('dfSelfNatyDets');
		//sps
		removErrMsgInChat('dfSpsNationalitySign');
		removErrMsgInChat('dfSpsNatyDets');
	}
	else if((chkd && val == "SGPR")){
		//$("#"+Ele).removeClass("show").addClass("invisible");
		//vignesh add on 24-05-2021
		$("#"+Ele).removeClass("invisible").addClass("show");
		removErrMsgInChat('dfSelfNationalitySign');
		//sps
		removErrMsgInChat('dfSpsNationalitySign');
	}else{
		$("#"+Ele).removeClass("invisible").addClass("show");
		removErrMsgInChat('dfSelfNationalitySign');
		//sps
		removErrMsgInChat('dfSpsNationalitySign');
//		$("#"+Ele+"").val('');
	}
}
//KYC OLD page 14 function reuse function end


function sameAsResiAddr(obj, Elem) {
	var crntClnt = "";
	if(Elem == "chkSelfNoSameAddrFlg"){
		crntClnt = "Self" 
	}
    if(Elem == "chkSpsNoSameAddrFlg"){
    	crntClnt = "Sps" 
	}
	
	if ($(obj).is(':checked')) {
      $("#df"+crntClnt+"Mailaddr").val($("#df"+crntClnt+"Homeaddr").val());
      $("#df"+crntClnt+"Mailaddr").prop("readOnly",true);
      $("#"+Elem).prop("checked",false);
      $(obj).val("Y");
    }else{
    	$(obj).val("N");
    }
}

function NotsameAsResiAddr(obj, Elem) {
	var crntClnt = "";
	if(Elem == "chkSelfSameAddrFlg"){
		crntClnt = "Self" 
	}
    if(Elem == "chkSpsSameAddrFlg"){
    	crntClnt = "Sps" 
	}
    
	if ($(obj).is(':checked')) {
		$("#df"+crntClnt+"Mailaddr").prop("readOnly",false);
		$("#df"+crntClnt+"Mailaddr").removeAttr("readOnly");
		$("#df"+crntClnt+"Mailaddr").val("");
		$("#df"+crntClnt+"Mailaddr").focus();
		$("#"+Elem).prop("checked",false);
		$(obj).val("Y");
	}    
	       
}
	  
function addNewInsurdFieldCombo (state) {
	if (!state.id) {
	return state.text;
	}


	var prin = state.element.value.toLowerCase() ;
	var icon = "";
	switch(prin){

	case "Salim M Amin":
	icon = "search1img/.png";
	break;

	case "Danielle Salim":
	icon = "search1img/.png";
	break;

	
	default:
    icon="img/follow.png";
}
	var $state = $('<span><img src="' + icon +'" class="img-flag" style="width:35px;height:20px;" /> &nbsp; | &nbsp; <strong>' + state.text + '</strong></span>'
	);
	return $state;
	};
	
	

 function expandAllPage2Accordian(){
	  
	 /*  $('#collexpans').on('click', function () { */
		    $('#accordion .panel-collapse').collapse('toggle');
		/* }); */ 
  }
  
  
  

 
 // Toggle New/Existing Customer
  var custType = $('#customer-type'),
      newCust = $('.new-customer'),
      existCust = $('.existing-customer'),
      createAccBtn = $('.create-account'),
      verifyAccBtn = $('.verify-account');
 
  custType.val($(this).is(':checked')).change(function() {
  if ($(this).is(':checked')) {
        newCust.fadeToggle(400, function() { // Hide Full form when checked
          existCust.fadeToggle(500); //Display Small form when checked
          createAccBtn.toggleClass('hide');
          verifyAccBtn.toggleClass('hide');
        });
        
      } else {
        existCust.fadeToggle(400, function() { //Hide Small form when unchecked
          newCust.fadeToggle(500); //Display Full form when unchecked
          createAccBtn.toggleClass('hide');
          verifyAccBtn.toggleClass('hide');
        });
        
      }
 });

$("#opt_client2_add").bind("click",function(){
	
});


$("#addNewClient2Btn").bind("change",function(){
	var thisval = $("#addNewClient2Btn").val();
	if(isEmpty(thisval) || thisval == "_addnew_"){
			
		$("#selFldNRICName").closest("form-group").addClass("err-fld");	
	}else{
		$("#selFldNRICName").closest("form-group").removeClass("err-fld");
	}
	
})


//selfNRICName onchnge hide Error Mesage

$("#selFldNRICName").bind("change",function(){
	var newinusredcustid = $('#selFldNRICName option:selected').val(); 
	if(isEmptyFld(newinusredcustid)){
         $("#selFldNRICNameError").removeClass("hide").addClass("show");
		return;
	  }else{
		$("#selFldNRICNameError").removeClass("show").addClass("hide");
	 }
	
})
  

$("#addNewClient2Btn").on("click",function(){
	var srchFlag = $("#SrchInsured").is(":checked")
	var addFlag = $("#AddInsured").is(":checked")
		if(srchFlag == true){
		   if(!validateInsSrchDels()){return;}
		   createInsuredTab();
		}

		if(addFlag == true){
			$("#selFldNRICNameError").removeClass("show").addClass("hide");
			addNewInsuredClnt($("#AddInsured"));
			
		}
 });
  
  

function createInsuredTab(){
	
	var newinsured =  $('#selFldNRICName option:selected').text();
	var newinusredcustid = $('#selFldNRICName option:selected').val(); 
	
	var nextTab = $('#myTab li').length+1;

		$('#myTab').find("a.nav-link").removeClass("active");
	
		//$('#myTab').find('li:nth-last-child(2)').before('<li class="nav-item" style="list-style-type:none;"><a class="nav-link active" id="client'+nextTab+'-tab" data-toggle="tab" href= "#tab'+nextTab+'" role="tab" aria-controls="client'+nextTab+'" aria-selected="false"> '+newinsured+'</a> </li>');
		$('#myTab').find('li:nth-last-child(2)').before('<li class="nav-item" style="list-style-type:none;"><a class="nav-link dfsps active" id="client'+nextTab+'-tab" data-toggle="tab" href= "#tab'+nextTab+'" role="tab" aria-controls="client'+nextTab+'" aria-selected="false" title="'+newinsured+'"> '+newinsured+'&nbsp;<i class="fa fa-trash-o checkdb delBtnclr pl-3" aria-hidden="true" style="font-size: larger;" id="'+newinsured+'" title="Delete : '+newinsured+'" onclick="deletInsured(this)" ></i></a> </li>');


		$('#myTabContent').find("div.tab-pane").removeClass("active");
   
	
//		var dynamictab = $("#divSpouseDetsElems").html();
		var dynamictab = $("#divSpouseDetsElems");
		$("#divSpouseDetsElems").removeClass("d-none");
  	 
//		$('<div class="tab-pane fade show active" id="tab'+nextTab+'" role="tabpanel" aria-labelledby="client'+nextTab+'-tab" > '+dynamictab+'</div>').appendTo('#myTabContent');
		$('<div class="tab-pane fade show active" id="tab'+nextTab+'" role="tabpanel" aria-label="client'+nextTab+'-tab" ></div>').prepend(dynamictab).appendTo('#myTabContent');
	 
		if(newinusredcustid != "_addnew_" && !isEmpty(newinusredcustid)){
			getClientById(newinusredcustid);
		}
  	
		
	 	$('.modal').modal('hide');
	 	saveCurrentPageData($(this),'kycHome',true,true);
	 	
	 	
		$('#myTab a:eq(client'+nextTab+'-tab)').tab('show');
		
		if($('#myTab li').length == 4){
			$('#myTab').find("#btnAddNewInsur").children(".btn").css("cursor","not-allowed");
			$('#myTab').find("#btnAddNewInsur").children(".btn").attr("data-toggle","");
			$('#myTab').find("#btnAddNewInsur").children(".btn").attr("data-target","");
		 }
		
	  }

//);
  
     function validateInsSrchDels(){
	
	var newinusredcustid = $('#selFldNRICName option:selected').val();
	    if(isEmptyFld(newinusredcustid)){
 	         $("#selFldNRICNameError").removeClass("hide").addClass("show");
			return;
		  }else{
			$("#selFldNRICNameError").removeClass("show").addClass("hide");
		 }
	return true;
}

function enableIntrprt(obj){
	hideInptrFldErrors();
	
  var flag = $(obj).is(":checked");
  
     //enable lanuguage use Option (English) if interpretor flsg is selected
     if(flag == true){
    	 var flg = $("#htflaneng").is(":checked");
    	 if(flg != true){
    		 $("#htflaneng").trigger("click");
    	 }
    	 $("#tempIntprtDiv").find(":input").prop("disabled",false);
    	// $("#tempIntprtDiv").find("input:text,select").val("");
     }else{
    	 $("#htflaneng").prop("checked",false);
    	 $("#htflaneng").val('');
    	 $("#cdLanguages").val('');
    	 $(".kycLanguage").prop("checked",false);
    	 $("#cdLanguageOth").val("")
    	 $("#tempIntprtDiv").find(":input").prop("disabled",true);
    	 $("#tempIntprtDiv").find("input:text,select").val("");
     }
  
	 /*$("#tempIntprtDiv").find(":input").prop("disabled",function(i, v) { return !v; });
	 $("#tempIntprtDiv").find("input:text,select").val("");*/
   }
   
// Add the following code if you want the name of the file appear on select
   $("#filecdTppNricCopy").on("change", function() {
     var fileName = $(this).val().split("\\").pop();
     $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
   });  

function expandAll(thisobj){
	
	var txt = $(thisobj).text();
	 
	 $(thisobj).html(function(i, text){
		 
		 if(text.indexOf("Expand All") > 0)
			 return '<i class="fa fa-compress" aria-hidden="true" style="color:#ff9800;"></i>&nbsp;Collapse All';
		 
		 if(text.indexOf("Collapse All")  >0)
			 return '<i class="fa fa-expand" aria-hidden="true" style="color:#656767;"></i>&nbsp;Expand All';
		 
        		  		  
      });
	 
	 
	  //$('div.panel-collapse').toggleClass('show');
	 // $('div.panel-heading').find('.panel-title').find('a').toggleClass('collapsed');
	 
	  if(txt.indexOf("Expand All") > 0){
		  $('div.panel-collapse').addClass('show');
		  $('div.panel-heading').find('.panel-title').find('a').removeClass('collapsed');
		  
	  }	  
	  if(txt.indexOf("Collapse All")  >0){
		  $('div.panel-collapse').removeClass('show');
		  $('div.panel-heading').find('.panel-title').find('a').addClass('collapsed');
	  }
	  
	}
  
//CRUD operation
 

function openOthInptrSec(obj,val){
	if(val=="BENOWNER"){
		if($(obj).is(":checked")){
			$(obj).val("Y");
			$(obj).attr("data-toggle","modal");
			$(obj).attr("data-target","#txtAreaModal1");
			$(obj).attr("data-backdrop","static");
			$(obj).attr("data-keyboard","false");
		}else{
			$(obj).val("N");
			$(obj).removeAttr("data-toggle","modal");
			$(obj).removeAttr("data-target","#txtAreaModal1");
			$(obj).removeAttr("data-backdrop","static");
			$(obj).removeAttr("data-keyboard","false");
			Swal.fire({
				  title: 'Are you sure?',
				  text: "Want to Clear Benefical Owner Details?",
				  icon: 'warning',
		 allowOutsideClick:false,
		 allowEscapeKey:false,
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Yes, Clear it!'
				}).then((result) => {
				  if (result.isConfirmed) {
					  clearBenOwnerDetls();
					  saveCurrentPageData($(this),'kycHome',false,true);
				      swal.fire("Cleared!", "Your Benefical Owner Details.", "success");
				      $(obj).val("N");
				  }else{
						$(obj).prop("checked",true);
						$(obj).val("Y");
					}
				  
				})
		}	
	}
	
	if(val=="TPP"){
		if($(obj).is(":checked")){
			$(obj).val("Y");
			$(obj).attr("data-toggle","modal");
			$(obj).attr("data-target","#txtAreaModal2");
			$(obj).attr("data-backdrop","static");
			$(obj).attr("data-keyboard","false");
			
		}else{
			$(obj).val("N");
			$(obj).removeAttr("data-toggle","modal");
			$(obj).removeAttr("data-target","#txtAreaModal2");
			$(obj).removeAttr("data-backdrop","static");
			$(obj).removeAttr("data-keyboard","false");
			Swal.fire({
				  title: 'Are you sure?',
				  text: "Want to Clear Third Party Payer Details?",
				  icon: 'warning',
		 allowOutsideClick:false,
		allowEscapeKey:false,
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Yes, Clear it!'
				}).then((result) => {
					if (result.isConfirmed) {
						 clearThridPartyDetls();
						 saveCurrentPageData($(this),'kycHome',false,true);
						 swal.fire("Cleared!", "Your Third Party Payer Details.", "success");
						 $(obj).val("N");
					}else{
						$(obj).prop("checked",true);
						$(obj).val("Y");
					}
				})
				
			
		}	    
	}
	
	if(val=="PEP"){
		if($(obj).is(":checked")){
			$(obj).val("Y");
			$(obj).attr("data-toggle","modal");
			$(obj).attr("data-target","#txtAreaModal3");
			$(obj).attr("data-backdrop","static");
			$(obj).attr("data-keyboard","false");
			
		}else{
			$(obj).val("N");
			$(obj).removeAttr("data-toggle","modal");
			$(obj).removeAttr("data-target","#txtAreaModal3");
			$(obj).removeAttr("data-backdrop","static");
			$(obj).removeAttr("data-keyboard","false");
			Swal.fire({
				  title: 'Are you sure?',
				  text: "want to clear PEP and RCA Details",
				  icon: 'warning',
				  allowOutsideClick:false,
				  allowEscapeKey:false,
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Yes, Clear it!'
				}).then((result) => {
					if (result.isConfirmed) {
						   clearPepRcaDetls();
						   saveCurrentPageData($(this),'kycHome',false,true);
						  swal.fire("Cleared!", "Your PEP and RCA Details.", "success");
						  $(obj).val("N");
					}else{
						$(obj).prop("checked",true);
						$(obj).val("Y");
					}
				})
			
			
			
		}	
	}
}




$('#txtAreaModal1').on('hidden.bs.modal', function () {
	var allempty = true;
	$("#txtAreaModal1").find(".modal-body").find("input.mandatory, textarea.mandatory,select.mandatory").each(function(){
		var thisval = $(this).val();
		//console.log($(this).prop("name") +","+thisval)
		if(!isEmpty(thisval)){
			allempty=false;
			$("#cdBenfownflg").prop("checked",true);
			$("#cdBenfownflg").val("Y")
		}
	})
	
	if(allempty){
		$("#cdBenfownflg").prop("checked",false);
		$("#cdBenfownflg").val("N")
	}
	
})

$('#txtAreaModal2').on('hidden.bs.modal', function () {
	var allempty = true;
	$("#txtAreaModal2").find(".modal-body").find("input.mandatory, textarea.mandatory,select.mandatory").each(function(){
		var thisval = $(this).val();
		if(!isEmpty(thisval)){
			allempty=false;
			$("#cdTppflg").prop("checked",true);
			$("#cdTppflg").val("Y")
		}
	})
	
	if(allempty){
		$("#cdTppflg").prop("checked",false);
		$("#cdTppflg").val("N")
	}
	
})

$('#txtAreaModal3').on('hidden.bs.modal', function () {
	var allempty = true;
	$("#txtAreaModal3").find(".modal-body").find("input.mandatory, textarea.mandatory,select.mandatory").each(function(){
		var thisval = $(this).val();
		if(!isEmpty(thisval)){
			allempty=false;
			$("#cdPepflg").prop("checked",true);
			$("#cdPepflg").val("Y")
		}
	})
	
	if(allempty){
		$("#cdPepflg").prop("checked",false);
		$("#cdPepflg").val("N")
	}
	
})

// Clear all Benefial Owner Details
function clearBenOwnerDetls(){
	$("#txtAreaModal1").find(".modal-body").find("input[type=text],textarea,select").val("");
	$('input[name="radFldCDBenfJob"]').prop('checked', false);
}

// Clear all Thrid Party Details
function clearThridPartyDetls(){
	$("#txtAreaModal2").find(".modal-body").find("input[type=text], textarea,select").val("");
	$('input[name="radFldCDTppJob"]').prop('checked', false);
	$('input[name="radCDTppPayMode"]').prop('checked', false);
	
}

// Clear all PEP / RCA Details
function clearPepRcaDetls(){
	$("#txtAreaModal3").find(".modal-body").find("input[type=text], textarea,select").val("");
	$('input[name="radFldCDPepJob"]').prop('checked', false);
}

function saveCurrentPageData(obj,nextscreen,navigateflg,infoflag){
	//interpreter sec Validation
	var inptrFlag = $("#cdIntrprtflg").is(":checked");
	if(inptrFlag == true){
		if(!validateInptrValidation()){return;}
	 }
	
	//interpreter popup sec val;idation
	var cdBenfownflg = $("#cdBenfownflg").is(":checked");
	if(cdBenfownflg == true){
		
		
		//validateBenOwnerDetls
		  if(!validateBenOwnerDetls()){
			  $('#txtAreaModal1').modal('show');  
			  return;
			}
		  $('#txtAreaModal1').modal('hide');
	 }
	
	var cdTppflg = $("#cdTppflg").is(":checked");
	if(cdTppflg == true){
		
		 //validateTppDetls
		  if(!validateTppDetls()){
			  $('#txtAreaModal2').modal('show');
			  return;
			  }
		  $('#txtAreaModal2').modal('hide');
	 }
	
	var cdPepflg = $("#cdPepflg").is(":checked");
	if(cdPepflg == true){
		
		 //validatePepRcaDetls
		  if(!validatePepRcaDetls()){
			  $('#txtAreaModal3').modal('show');
			  return;
			 }
		  $('#txtAreaModal3').modal('hide');
	 }
	
	
	
//	var isSignedOrNot = $("#hTxtFldSignedOrNot").val();
//	var chkanychange = $("#chkanychange").val();
//	
//	if(isSignedOrNot == "Y" && !isEmpty(chkanychange)) {
//		showConfrmAfterSignChng(obj,nextscreen,navigateflg,infoflag);		
//	}
//	else {
//		doCurrentPageData(obj,nextscreen,navigateflg,infoflag);
//	}
		
	 $("#tempIntprtDiv").find(":input").removeAttr("disabled");
		var frmElements = $('#fnaPersonalForm :input').serializeObject();
		
//		 var fnaDetails=getInterprtData();
//		 frmElements.fnaDetails=fnaDetails;
		
		frmElements["dfSelfGender"] = $("#radBtnSelfmale").is(":checked") ? "M" :
			$("#radBtnSelffemale").is(":checked") ? "F" : "" ;
		

		frmElements["dfSpsGender"] = $("#radBtnSpsmale").is(":checked") ? "M" :
			$("#radBtnSpsfemale").is(":checked") ? "F" : "" ;
		
		var intrprtFlg = document.getElementById("cdIntrprtflg");
	     if (intrprtFlg.checked == true){
			frmElements["cdIntrprtflg"] = "Y";
	     }else {
	    	 frmElements["cdIntrprtflg"] = "N";
	     }
	     
	     var intrprtFlg = document.getElementById("cdBenfownflg");
	     if (intrprtFlg.checked == true){
			frmElements["cdBenfownflg"] = "Y";
	     }else {
	    	 frmElements["cdBenfownflg"] = "N";
	     }
	     
	     var intrprtFlg = document.getElementById("cdTppflg");
	     if (intrprtFlg.checked == true){
			frmElements["cdTppflg"] = "Y";
	     }else {
	    	 frmElements["cdTppflg"] = "N";
	     }
	     
	     var intrprtFlg = document.getElementById("cdPepflg");
	     if (intrprtFlg.checked == true){
			frmElements["cdPepflg"] = "Y";
	     }else {
	    	 frmElements["cdPepflg"] = "N";
	     }
	     
	     
		    
			$.ajax({
		            url:"fnadetails/register/personaldetails/",
		            type: "POST",
		            async:false,
		            dataType: "json",
		            contentType: "application/json",
		            data: JSON.stringify(frmElements),
		            success: function (response) {
//		            	alert(response.DATA_FORM_ID);
		            	$("#dataformId").val(response.DATA_FORM_ID);
//		            	 $("#tempIntprtDiv").find(":input").prop("disabled",true);
		            	if(navigateflg){
		            		window.location.href=nextscreen;	
		            	}
		            	
		            	
		            },
		            error: function(xhr,textStatus, errorThrown){
			 			//$('#cover-spin').hide(0);
			 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
					}
		            });





}

function doCurrentPageData(obj,nextscreen,navigateflg,infoflag) {}



//Set and get Interpreter details
var arrIntPrt=["intprtName","intprtContact","intprtRelat","cdLanguages"];
function getInterprtData(){
	
	var interPrt ={};
	
     var intrprtFlg = document.getElementById("cdIntrprtflg");
       
     if (intrprtFlg.checked == true){
			interPrt.cdIntrprtFlg="Y";
			} 
	 else {
		interPrt.cdIntrprtFlg="N";
		
		}
      for(var i=0;i<arrIntPrt.length;i++){
	     
          interPrt[arrIntPrt[i]]=document.getElementsByName(arrIntPrt[i])[0].value;
      }
      interPrt.cdLanguageOth=$("#cdLanguageOth").val();
     return interPrt;
}

function setInterprtData(interPrt){
	
	for(var i=0;i<arrIntPrt.length;i++){
		document.getElementsByName(arrIntPrt[i])[0].value=interPrt[arrIntPrt[i]];
		 if(arrIntPrt[i]=="cdLanguages" && arrIntPrt[i]!=null){
			  var cdLanguage=interPrt[arrIntPrt[i]];
	         if(cdLanguage!="" && cdLanguage!=null && cdLanguage!= "undefined"){
				  var cdLangData=JSON.parse(cdLanguage);
		          setLanguageData(cdLangData,interPrt);
             }
		  }
          
      }
    //Set interpreter flag
    if(interPrt.cdIntrprtFlg=="Y"){
    	$("#btnInptrSwtch").prop('checked',true);enableIntrprt();}
  
}

 
function setLanguageData(cdLangData,interPrt){
	if(cdLangData.ENG=="Y"){
		 $("#htflaneng").prop('checked',true);
	}
	if(cdLangData.MAN=="Y"){
		 $("#htflanman").prop('checked',true);
	}
	if(cdLangData.MAL=="Y"){
		 $("#htflanmalay").prop('checked',true);
	}
	if(cdLangData.TAM=="Y"){
		 $("#htflantamil").prop('checked',true);
	}
	if(cdLangData.OTH=="Y"){
		 $("#htflanoth").prop('checked',true);
	     $("#cdLanguageOth").prop('disabled',false).val(interPrt.cdLanguageOth);
		  
	}
}






function deletInsured(obj){
	
	var parentTabli = $(obj).closest("li")
	var parentTab = $(obj).closest("li").find("a.nav-link");
	var tabId = parentTab.prop("id");
	
	
	var signornot = $("#hTxtFldSignedOrNot").val();
	if(signornot != "Y" ) {
		
		var strClient = $(obj).attr("id");
		//alert($(obj).closest(".ProdDetlsSec").children().eq(1).html())
		swal.fire({
			  title: "Are you sure?",
			  text: "Do you want to delete this Client : (" +strClient+ ") details!",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonClass: "btn-danger",
			  confirmButtonText: "Yes, delete it!",
			  cancelButtonText: "No,Cancel",
			  closeOnConfirm: false,
			  closeOnCancel: false
			}).then((result) => {
		  if (result.isConfirmed) {
		     //clear all Spouse Details
				    // date Picker Values Clear of Spouse
				    $("#divSpouseDetsElems").find('#simple-date2 .input-group.date').datepicker("update", new Date()); 
					$("#divSpouseDetsElems").find("input,textarea,select").val("");
					$("#divSpouseDetsElems").find(':input').removeAttr("readOnly");
					$("#divSpouseDetsElems").find(':input').removeAttr("disabled");
					$("#divSpouseDetsElems").find('input:checkbox').prop('checked',false);
					$("#divSpouseDetsElems").find('input:radio').prop('checked',false);
					
					//Clear spouseName in sidemenu.jsp File
					$("#MenuSpsName").text("");
					sessspsname = "";
					
					//End
					var spouseElemsDiv = $("#divSpouseDetsElems");
					$(spouseElemsDiv).addClass("d-none");
					$(spouseElemsDiv).appendTo("#divSpouseDetsElemsParent")
					
					
					$("#myTabContent").find('div[aria-label='+tabId+']').remove();	
					$(parentTabli).remove();
					
					$("#myTab").find("li:eq(0)>a").addClass("active");
					$("#myTabContent").find("div.tab-pane").addClass("active").addClass("show");
					
					$('#myTab').find("#btnAddNewInsur").children(".btn").css("cursor","pointer");
					$('#myTab').find("#btnAddNewInsur").children(".btn").attr("data-toggle","modal");
					$('#myTab').find("#btnAddNewInsur").children(".btn").attr("data-target","#new2ModalScrollable");
					
					saveCurrentPageData($(this),'kycHome',true,true);
					
					swal.fire("Deleted!", "Client Name : (" +strClient+ ") has been deleted.", "success");
		  }else {
			    swal.fire("Cancelled", "Client Name : (" +strClient+ ") detail is safe :)", "error");
			  }
		});

	}else {
		
		showConfrmAfterSignChng($("#btnNextFormUniq"),$("#applNextScreen").val(),false,false);
		
	}
	

	
}

$("#htfrepdecno1all").on("click",function(){
	if(this.checked){
		$("#repDecTbl input[type=checkbox]").each(function(){
			var chkid = $(this).prop("id");
			if(chkid != "htfrepdecno1all"){
				this.checked=true;
    			$(this).trigger("change");	
			}
			
		});
	}else{
		$("#repDecTbl input[type=checkbox]").each(function(){
			var chkid = $(this).prop("id");
			if(chkid != "htfrepdecno1all"){
    			this.checked=false;
    			$(this).trigger("change");
			}
		});
	}
	
});


function addNewInsured(){
	
	if($('#myTab li').length == 4){
		$('#myTab').find("#btnAddNewInsur").children(".btn").css("cursor","not-allowed");
		$('#myTab').find("#btnAddNewInsur").children(".btn").attr("data-toggle","");
		$('#myTab').find("#btnAddNewInsur").children(".btn").attr("data-target","");
	 }else {
		 
		 
			var signornot = $("#hTxtFldSignedOrNot").val();
			if(signornot != "Y" ) {
				
				setAddSrchRadioBtnFields();
				
				var newinsureddata = getAllClients();
				 
				 $('#selFldNRICName').select2({
					   templateResult: formatClientOpt,
					   placeholder: 'Select a Client Name'
			    });
				
				if($('#myTab li').length == 4){
					$('#myTab').find("#btnAddNewInsur").children(".btn").css("cursor","not-allowed");
					$('#myTab').find("#btnAddNewInsur").children(".btn").attr("data-toggle","");
					$('#myTab').find("#btnAddNewInsur").children(".btn").attr("data-target","");
				 }else if($('#myTab li').length < 4){
					 $('#myTab').find("#btnAddNewInsur").children(".btn").css("cursor","pointer");
					 $('#myTab').find("#btnAddNewInsur").children(".btn").attr("data-toggle","modal");
					 $('#myTab').find("#btnAddNewInsur").children(".btn").attr("data-target","#new2ModalScrollable");
				 }

				
			}else {
				showConfrmAfterSignChng($("#btnNextFormUniq"),$("#applNextScreen").val(),false,false);
			}
		 
		 
	 }
	

	
	
	
}

function enableSelfSpsSameAddrFlg(obj,client){
	
	if(client == 'self'){
		var selfHomeaddr = $("#"+obj).val();
		var flg = $("#chkSelfSameAddrFlg").is(':checked');
		if((selfHomeaddr.length>=0 )&&(flg == true)){
			$("#dfSelfMailaddr").val(selfHomeaddr);
			$("#chkSelfSameAddrFlg").val("Y");
		}
		
	}
		if(client == 'spouse'){
			var spsHomeaddr = $("#"+obj).val();
			var flg = $("#chkSpsSameAddrFlg").is(':checked');
			if((spsHomeaddr.length>=0 )&&(flg == true)){
				$("#dfSpsMailaddr").val(spsHomeaddr);
				$("#chkSpsSameAddrFlg").val("Y");
			}
	}
	
}


// add new insured details

function addNewInsuredClnt(obj){
	
	var insurAddFlg = $("#AddInsured").is(':checked');
	
	if( insurAddFlg== true){
		$("#selFldNRICNameError").removeClass("show").addClass("hide");
		//clear Search Insured Combo Box
		 $('#selFldNRICName').val('');
		 $('#selFldNRICName').select2({
			   templateResult: formatClientOpt,
			   placeholder: 'Select a Client Name'
	     });
		
		
		  

	 swal.fire({
		  	title: "Add  New Client!",
			input:"text",
		  	inputLabel: "Client Name :",
			//html:'Insured Name: <input id="swal-input1" class="swal2-input">' ,
		  	showCancelButton: true,
		  	closeOnConfirm: false,
			inputValidator: function(value)  {
	    		if (!value) {
	      			return 'Client Name Cannot be empty!'
	    		}
		 	},
		 	//poovathi add on 25-06-2021 for sps max length set
		 	inputAttributes: {
		 	    'maxlength': 75,
		 	    'autocapitalize': 'off',
		 	    'autocorrect': 'off'
		 	  }//end
		}).then((result) => {
			 if (result.value) {
				var inputValue = result.value
				
		  var newinsured = inputValue.trim();
			
			// check if already tab existed or not
			var tabArr =[],strTab = "";
		      $('#myTab li a').each(function(){
		    	  strTab = $(this).text();
				   tabArr.push(strTab.trim().toLowerCase());
			     }); 
		      
		    var clntTabNameExts = tabArr.includes(newinsured.trim().toLowerCase());
		    
		        if(!clntTabNameExts){
		        var nextTab = $('#myTab li').length+1;

				$('#myTab').find("a.nav-link").removeClass("active");
	
				$('#myTab').find('li:nth-last-child(2)').before('<li class="nav-item" style="list-style-type:none;"><a class="nav-link dfsps active" id="client'+nextTab+'-tab" data-toggle="tab" href= "#tab'+nextTab+'" role="tab" aria-controls="client'+nextTab+'" aria-selected="false"> '+newinsured+'&nbsp;<i class="fa fa-trash-o checkdb delBtnclr pl-3" aria-hidden="true" style="font-size: larger;" id="'+newinsured+'" title="Delete : '+newinsured+'" onclick="deletInsured(this)" ></i></a> </li>');


					$('#myTabContent').find("div.tab-pane").removeClass("active");
	   
		
					var dynamictab = $("#divSpouseDetsElems");
					$("#divSpouseDetsElems").removeClass("d-none");
	  	 
					$('<div class="tab-pane fade show active" id="tab'+nextTab+'" role="tabpanel" aria-label="client'+nextTab+'-tab" ></div>').prepend(dynamictab).appendTo('#myTabContent');
		 
					$('#myTab a:eq(client'+nextTab+'-tab)').tab('show');
					
					//set Spouse Name in dfSpsName Field
					$("#dfSpsName").val(newinsured);
					
					//set spousename Fld ReadOnly Property
					$("#dfSpsName").prop("readOnly",true);
					
					if($('#myTab li').length == 4){
						$('#myTab').find("#btnAddNewInsur").children(".btn").css("cursor","not-allowed");
						$('#myTab').find("#btnAddNewInsur").children(".btn").attr("data-toggle","");
						$('#myTab').find("#btnAddNewInsur").children(".btn").attr("data-target","");
					 }
//					alert("")		
					saveCurrentPageData($(this),'kycHome',false,true);
					
					$('#new2ModalScrollable').modal('hide');
					swal.close();
		          }else{
		        	  swal.fire("New Client Name already Exists ,Add different Client Name!","","error");
		          }
				
				
    		 }
			
			
		});
		
	
	}
}

//set Search Insured radio btn values while click add Insured Button 
function setAddSrchRadioBtnFields(){
	 $("#selFldNRICNameError").removeClass("show").addClass("hide");
	 $("#SrchInsured").prop("checked",true);
	 $('input[name="AddSrchInsured"]:checked').val("SrchInsured");
	 $('#selFldNRICName').val('');
	 $('#selFldNRICName').select2({
		   templateResult: formatClientOpt,
		   placeholder: 'Select a Client Name'
     });
	$("#AddInsured").prop("checked",false);
 	
}

 //enable client(2)/ spouse name to edit
  function enableEditSpsNameFld(){
	  $("#dfSpsName").prop("readOnly",false); 
	  $("#dfSpsName").focus();
	 
  }
  
 //dfSpsName onchange --> set SpouseName to Insured Tab
 // $('input[name=dfSpsName]').bind("change",function(){
 function changeSpsTabName(obj){
	 var SpsName = $(obj).val();
	   if(isEmptyFld(SpsName)){
	         $("#dfSpsNameError").removeClass("hide").addClass("show");
			return;
		  }else{
			$("#dfSpsNameError").removeClass("show").addClass("hide");
			$('#myTab li a.active').text(SpsName.trim());
			
			//create Insured Delete Icon and Append to Spouse Tab
			var strInsDelIcon = '<i class="fa fa-trash-o checkdb delBtnclr pl-3"'+
				'aria-hidden="true" style="font-size: larger;" '+
				'id="'+SpsName.trim()+'" title="Delete :'+SpsName.trim()+'" onclick="deletInsured(this)"></i>';
		        $('#myTab li a.active').eq(0).append(strInsDelIcon);
		        $("#dfSpsName").prop("readOnly",true); 
		  }
  }

  //interpreter Sec validation start here
  
  $(".inptrModalClass").bind("click",function(){
	  
	  //validateBenOwnerDetls
	  if(!validateBenOwnerDetls()){return;}
	  //validateTppDetls
	  if(!validateTppDetls()){return;}
	  //validatePepRcaDetls
	  if(!validatePepRcaDetls()){return;}
	  
		 enableTppPopUp();
		 enableBenOwnerPopUp();
		 enablePepRcaPopUp();
	  })
  

	 function validateBenOwnerDetls(){
		var name,nric,incorNum,resiAddr,job,country,relationship,Fld="Error";
		name = $('#txtFldCDBenfName').val();
		nric = $('#txtFldCDBenfNric').val();
		incorNum = $('#txtFldCDBenfIncNo').val();
		resiAddr = $('#txtFldCDBenfAddr').val();
		job = $('input[name="radFldCDBenfJob"]:checked').val();
		country = $('#txtFldCDBenfJobCont').val();
		relationship = $('#txtFldCDBenfRel').val();
		
		 //numberOfChecked = $(".selfInptrLangSec").find('input:checkbox:checked').length;
		
		//Client Name
		  if(isEmptyFld(name)){
				$("#txtFldCDBenfName"+Fld).removeClass("hide").addClass("show");
				$("#txtFldCDBenfName").addClass('err-fld');
				$("#txtFldCDBenfName").focus();
				return;
			}else{
				$("#txtFldCDBenfName"+Fld).removeClass("show").addClass("hide");
				$("#txtFldCDBenfName").removeClass('err-fld');
			}
		  
		  
		  if(isEmpty(nric)&&(isEmpty(incorNum))){
			  	        //nric
						$("#txtFldCDBenfNric"+Fld).removeClass("hide").addClass("show");
						$("#txtFldCDBenfNric").addClass('err-fld');
						$("#txtFldCDBenfNric").focus();
					     
						//incno
				     	$("#txtFldCDBenfIncNo"+Fld).removeClass("hide").addClass("show");
						$("#txtFldCDBenfIncNo").addClass('err-fld');
						$("#txtFldCDBenfIncNo").focus();
						
						return;
		 }else{
			 //nric
			  $("#txtFldCDBenfNric"+Fld).removeClass("show").addClass("hide");
			  $("#txtFldCDBenfNric").removeClass('err-fld');
			  
			  //inc no
			  $("#txtFldCDBenfIncNo"+Fld).removeClass("show").addClass("hide");
			  $("#txtFldCDBenfIncNo").removeClass('err-fld');
		  }	  
		  
		  //Resi Addr
		  if(isEmptyFld(resiAddr)){
				$("#txtFldCDBenfAddr"+Fld).removeClass("hide").addClass("show");
				$("#txtFldCDBenfAddr").addClass('err-fld');
				$("#txtFldCDBenfAddr").focus();
				return;
			}else{
				$("#txtFldCDBenfAddr"+Fld).removeClass("show").addClass("hide");
				$("#txtFldCDBenfAddr").removeClass('err-fld');
			}
		  
		  
		  //job
		  if(isEmptyFld(job)){
				$("#radFldCDBenfJob"+Fld).removeClass("hide").addClass("show");
				//$("#txtFldCDBenfAddr").addClass('err-fld');
				//$("#txtFldCDBenfAddr").focus();
				return;
			}else{
				$("#radFldCDBenfJob"+Fld).removeClass("show").addClass("hide");
				//$("#txtFldCDBenfAddr").removeClass('err-fld');
			}
		  
		  
		  
		  //Country
		  if(isEmptyFld(country)){
				$("#txtFldCDBenfJobCont"+Fld).removeClass("hide").addClass("show");
				$("#txtFldCDBenfJobCont").addClass('err-fld');
				$("#txtFldCDBenfJobCont").focus();
				return;
			}else{
				$("#txtFldCDBenfJobCont"+Fld).removeClass("show").addClass("hide");
				$("#txtFldCDBenfJobCont").removeClass('err-fld');
			}
		  
		  //Relationship
		  if(isEmptyFld(relationship)){
				$("#txtFldCDBenfRel"+Fld).removeClass("hide").addClass("show");
				$("#txtFldCDBenfRel").addClass('err-fld');
				$("#txtFldCDBenfRel").focus();
				return;
			}else{
				$("#txtFldCDBenfRel"+Fld).removeClass("show").addClass("hide");
				$("#txtFldCDBenfRel").removeClass('err-fld');
			}
		
		 return true;
	}  
  
  
  //Tpp sec mandotary fld  Validation 
  
	 function validateTppDetls(){
			var name,nric,incorNum,resiAddr,job,country,relationship,payMode,contact,Fld="Error";
			name = $('#txtFldCDTppName').val();
			nric = $('#txtFldCDTppNric').val();
			incorNum = $('#txtFldCDTppIncNo').val();
			resiAddr = $('#txtFldCDTppAddr').val();
			job = $('input[name="radFldCDTppJob"]:checked').val();
			country = $('#txtFldCDTppJobCont').val();
			relationship = $('#txtFldCDTppRel').val();
			payMode = $('input[name="radCDTppPayMode"]:checked').val();
			contact = $('#txtFldCDTppCcontact').val();
			 //numberOfChecked = $(".selfInptrLangSec").find('input:checkbox:checked').length;
			
			
			//Client Name
			  if(isEmptyFld(name)){
					$("#txtFldCDTppName"+Fld).removeClass("hide").addClass("show");
					$("#txtFldCDTppName").addClass('err-fld');
					$("#txtFldCDTppName").focus();
					return;
				}else{
					$("#txtFldCDTppName"+Fld).removeClass("show").addClass("hide");
					$("#txtFldCDTppName").removeClass('err-fld');
				}
			  
			  
			  
			  if(isEmpty(nric)&&(isEmpty(incorNum))){
		  	        //nric
				    $("#txtFldCDTppNric"+Fld).removeClass("hide").addClass("show");
					$("#txtFldCDTppNric").addClass('err-fld');
					$("#txtFldCDTppNric").focus();
				     
					//incno
					$("#txtFldCDTppIncNo"+Fld).removeClass("hide").addClass("show");
					$("#txtFldCDTppIncNo").addClass('err-fld');
					$("#txtFldCDTppIncNo").focus();
					
					return;
			 }else{
				 //nric
				    $("#txtFldCDTppNric"+Fld).removeClass("show").addClass("hide");
					$("#txtFldCDTppNric").removeClass('err-fld');
				  
				  //inc no
					$("#txtFldCDTppIncNo"+Fld).removeClass("show").addClass("hide");
					$("#txtFldCDTppIncNo").removeClass('err-fld');
				}
			  
			  //Resi Addr
			  if(isEmptyFld(resiAddr)){
					$("#txtFldCDTppAddr"+Fld).removeClass("hide").addClass("show");
					$("#txtFldCDTppAddr").addClass('err-fld');
					$("#txtFldCDTppAddr").focus();
					return;
				}else{
					$("#txtFldCDTppAddr"+Fld).removeClass("show").addClass("hide");
					$("#txtFldCDTppAddr").removeClass('err-fld');
				}
			  
			  
			  //job
			  if(isEmptyFld(job)){
					$("#radFldCDTppJob"+Fld).removeClass("hide").addClass("show");
					//$("#txtFldCDBenfAddr").addClass('err-fld');
					//$("#txtFldCDBenfAddr").focus();
					return;
				}else{
					$("#radFldCDTppJob"+Fld).removeClass("show").addClass("hide");
					//$("#txtFldCDBenfAddr").removeClass('err-fld');
				}
			  
			  
			  
			  //Country
			  if(isEmptyFld(country)){
					$("#txtFldCDTppJobCont"+Fld).removeClass("hide").addClass("show");
					$("#txtFldCDTppJobCont").addClass('err-fld');
					$("#txtFldCDTppJobCont").focus();
					return;
				}else{
					$("#txtFldCDTppJobCont"+Fld).removeClass("show").addClass("hide");
					$("#txtFldCDTppJobCont").removeClass('err-fld');
				}
			  
			  //Relationship
			  if(isEmptyFld(relationship)){
					$("#txtFldCDTppRel"+Fld).removeClass("hide").addClass("show");
					$("#txtFldCDTppRel").addClass('err-fld');
					$("#txtFldCDTppRel").focus();
					return;
				}else{
					$("#txtFldCDTppRel"+Fld).removeClass("show").addClass("hide");
					$("#txtFldCDTppRel").removeClass('err-fld');
				}
			  
			//payMode
				
				 if(isEmptyFld(payMode)){
						$("#radCDTppPayMode"+Fld).removeClass("hide").addClass("show");
						//$("#txtFldCDBenfAddr").addClass('err-fld');
						//$("#txtFldCDBenfAddr").focus();
						return;
					}else{
						$("#radCDTppPayMode"+Fld).removeClass("show").addClass("hide");
						//$("#txtFldCDBenfAddr").removeClass('err-fld');
					}
				  
				
				//Contact
				  if(isEmptyFld(contact)){
						$("#txtFldCDTppCcontact"+Fld).removeClass("hide").addClass("show");
						$("#txtFldCDTppCcontact").addClass('err-fld');
						$("#txtFldCDTppCcontact").focus();
						return;
					}else{
						$("#txtFldCDTppCcontact"+Fld).removeClass("show").addClass("hide");
						$("#txtFldCDTppCcontact").removeClass('err-fld');
					}
			
			 return true;
		} 
	 
	 
	 //Pep/Rca Mand Field Validation 
	 function validatePepRcaDetls(){
			var name,nric,incorNum,resiAddr,job,country,relationship,Fld="Error";
			name = $('#txtFldCDPepName').val();
			nric = $('#txtFldCDPepNric').val();
			incorNum = $('#txtFldCDPepIncNo').val();
			resiAddr = $('#txtFldCDPepAddr').val();
			job = $('input[name="radFldCDPepJob"]:checked').val();
			country = $('#txtFldCDPepJobCont').val();
			relationship = $('#txtFldCDPepRel').val();
			
			 //numberOfChecked = $(".selfInptrLangSec").find('input:checkbox:checked').length;
			
			//Client Name
			  if(isEmptyFld(name)){
					$("#txtFldCDPepName"+Fld).removeClass("hide").addClass("show");
					$("#txtFldCDPepName").addClass('err-fld');
					$("#txtFldCDPepName").focus();
					return;
				}else{
					$("#txtFldCDPepName"+Fld).removeClass("show").addClass("hide");
					$("#txtFldCDPepName").removeClass('err-fld');
				}
			  
			  
			  if(isEmpty(nric)&&(isEmpty(incorNum))){
		  	        //nric
				  $("#txtFldCDPepNric"+Fld).removeClass("hide").addClass("show");
					$("#txtFldCDPepNric").addClass('err-fld');
					$("#txtFldCDPepNric").focus();
				     
					//incno
					$("#txtFldCDPepIncNo"+Fld).removeClass("hide").addClass("show");
					$("#txtFldCDPepIncNo").addClass('err-fld');
					$("#txtFldCDPepIncNo").focus();
					
					return;
			 }else{
				 //nric
				    $("#txtFldCDPepNric"+Fld).removeClass("show").addClass("hide");
					$("#txtFldCDPepNric").removeClass('err-fld');
				  
				  //inc no
					$("#txtFldCDPepIncNo"+Fld).removeClass("show").addClass("hide");
					$("#txtFldCDPepIncNo").removeClass('err-fld');
				}
			  
			  //Resi Addr
			  if(isEmptyFld(resiAddr)){
					$("#txtFldCDPepAddr"+Fld).removeClass("hide").addClass("show");
					$("#txtFldCDPepAddr").addClass('err-fld');
					$("#txtFldCDPepAddr").focus();
					return;
				}else{
					$("#txtFldCDPepAddr"+Fld).removeClass("show").addClass("hide");
					$("#txtFldCDPepAddr").removeClass('err-fld');
				}
			  
			  
			  //job
			  if(isEmptyFld(job)){
					$("#radFldCDPepJob"+Fld).removeClass("hide").addClass("show");
					//$("#txtFldCDBenfAddr").addClass('err-fld');
					//$("#txtFldCDBenfAddr").focus();
					return;
				}else{
					$("#radFldCDPepJob"+Fld).removeClass("show").addClass("hide");
					//$("#txtFldCDBenfAddr").removeClass('err-fld');
				}
			  
			  
			  
			  //Country
			  if(isEmptyFld(country)){
					$("#txtFldCDPepJobCont"+Fld).removeClass("hide").addClass("show");
					$("#txtFldCDPepJobCont").addClass('err-fld');
					$("#txtFldCDPepJobCont").focus();
					return;
				}else{
					$("#txtFldCDPepJobCont"+Fld).removeClass("show").addClass("hide");
					$("#txtFldCDPepJobCont").removeClass('err-fld');
				}
			  
			  //Relationship
			  if(isEmptyFld(relationship)){
					$("#txtFldCDPepRel"+Fld).removeClass("hide").addClass("show");
					$("#txtFldCDPepRel").addClass('err-fld');
					$("#txtFldCDPepRel").focus();
					return;
				}else{
					$("#txtFldCDPepRel"+Fld).removeClass("show").addClass("hide");
					$("#txtFldCDPepRel").removeClass('err-fld');
				}
			
			 return true;
		} 
	  
	  
	  
	  
	  
  
  //enable Tpp checkbox
  function enableTppPopUp(){
	 var strTppName = $("#txtFldCDTppName").val();
	 if(strTppName.length > 0){
		 $("#cdTppflg").prop("checked",true);
		 $("#cdTppflg").val("Y");
	 }else if(strTppName.length == 0){
		// $("#cdTppflg").prop("checked",false);
		// $("#cdTppflg").val("");
	 }
     
  }
  
  //enable benOwner checkbox
  function enableBenOwnerPopUp(){
	 var strBenName = $("#txtFldCDBenfName").val();
	 if(strBenName.length > 0){
		 $("#cdBenfownflg").prop("checked",true);
		 $("#cdBenfownflg").val("Y");
	 }else if(strBenName.length == 0){
		 //$("#cdBenfownflg").prop("checked",false);
		 //$("#cdBenfownflg").val("");
	 }
     
  }
  
  //enable PepRca checkbox
  function enablePepRcaPopUp(){
	 var strPepName = $("#txtFldCDPepName").val();
	 if(strPepName.length > 0){
		 $("#cdPepflg").prop("checked",true);
		 $("#cdPepflg").val("Y");
	 }else if(strPepName.length == 0){
		 //$("#cdPepflg").prop("checked",false);
		// $("#cdPepflg").val("");
	 }
     
  }
  
  function hideInptrFldErrors(){
		var Fld= "Error"
		$("#intprtName"+Fld).removeClass("show").addClass("hide");
		$("#intprtName").removeClass('err-fld');
		$("#intprtContact"+Fld).removeClass("show").addClass("hide");
		$("#intprtContact").removeClass('err-fld');
		$("#intprtRelat"+Fld).removeClass("show").addClass("hide");
		$("#intprtRelat").removeClass('err-fld');
		$("#selfInptrLangSec"+Fld).removeClass("show").addClass("hide");
		$(".selfInptrLangSec").removeClass('err-fld');
	}
  
  function validateInptrValidation(){
		var name,Contact,relation,numberOfChecked,Fld="Error";
		name = $('#intprtName').val();
		Contact = $('#intprtContact').val();
		relation = $('#intprtRelat').val();
		 numberOfChecked = $(".selfInptrLangSec").find('input:checkbox:checked').length;
		
		//Client Name
		  if(isEmptyFld(name)){
				$("#intprtName"+Fld).removeClass("hide").addClass("show");
				$("#intprtName").addClass('err-fld');
				$("#intprtName").focus();
				return;
			}else{
				$("#intprtName"+Fld).removeClass("show").addClass("hide");
				$("#intprtName").removeClass('err-fld');
			}
		 //contact
		 if(isEmptyFld(Contact)){
				$("#intprtContact"+Fld).removeClass("hide").addClass("show");
				$("#intprtContact").addClass('err-fld');
				$("#intprtContact").focus();
				return;
			}else{
				$("#intprtContact"+Fld).removeClass("show").addClass("hide");
				$("#intprtContact").removeClass('err-fld');
			}
		 //Relationship
		  if(isEmptyFld(relation)){
				$("#intprtRelat"+Fld).removeClass("hide").addClass("show");
				$("#intprtRelat").addClass('err-fld');
				$("#intprtRelat").focus();
				return;
			}else{
				$("#intprtRelat"+Fld).removeClass("show").addClass("hide");
				$("#intprtRelat").removeClass('err-fld');
			}
		  
		  if(isEmptyFld(numberOfChecked)){
				$("#selfInptrLangSec"+Fld).removeClass("hide").addClass("show");
				$(".selfInptrLangSec").addClass('err-fld');
				//$("#intprtRelat").focus();
				return;
			}else{
				hideInptrLanSecError();
			}
		 return true;
	}
  
  
  //hide errormsg while onchange field values
  
 // $(".modal-body").find(".checkMand").bind("change",function(){
  function validateFld(obj){
	 // alert("hhuh")
		var thisFldId = $(obj).attr("id");
		var thisFldVal = $(obj).val();
		
		if(isEmptyFld(thisFldVal)){
	        $("#"+thisFldId+"Error").removeClass("hide").addClass("show");
	        $("#"+thisFldId).addClass('err-fld');
			return;
		  }else{
			  $("#"+thisFldId+"Error").removeClass("show").addClass("hide");
			  $("#"+thisFldId).removeClass('err-fld');
			 }
	//})
		}
	function radioAndChkboxFld(obj){
		var thisFldName = $(obj).attr("name");
		var thisFldVal = $("input[name="+thisFldName+"]:checked").val();
		
		if(isEmptyFld(thisFldVal)){
	        $("#"+thisFldName+"Error").removeClass("hide").addClass("show");
	        //$("#"+thisFldId).addClass('err-fld');
			return;
		  }else{
			  $("#"+thisFldName+"Error").removeClass("show").addClass("hide");
			 // $("#"+thisFldId).removeClass('err-fld');
			 }
	}
	
	
  function hideInptrLanSecError(){
	    $("#selfInptrLangSecError").removeClass("show").addClass("hide");
		$(".selfInptrLangSec").removeClass('err-fld');
  }
  
  
  //validate self and sps business natue details
  
//validate BusinessNature fld validation

  function valiBusNatOthers(selObj){
	  var selFldId = $(selObj).attr("id");
	  
	  if(selFldId == "dfSelfBusinatr"){
			$("#dfSelfBusinatrDets").val("");
			var selFldVal= $(selObj).val();
		  	if (selFldVal == 'Others'){
		  		 $("#dfSelfBusNatOthSec").removeClass("invisible").addClass("show");
		  		 if(isEmpty($("#businessNatrDets").val())){
		  			 $("#dfSelfBusinatrDets").addClass("err-fld");
		  			 $("#dfSelfBusinatrDets").focus();
		  			 $("#dfSelfBusinatrDetsError").addClass("show").removeClass("invisible");
		  		 }else{
		  			 $("#dfSelfBusinatrDets").removeClass("err-fld");
		  			 $("#dfSelfBusinatrDetsError").removeClass("show").addClass("invisible");
		  		 }
		  	}else{
		  		
		  		$("#dfSelfBusNatOthSec").removeClass("show").addClass("invisible");
		  		$("#dfSelfBusinatrDetsError").removeClass("show").addClass("invisible");
		  	}  
	  }
	  if(selFldId == "dfSpsBusinatr"){
			$("#dfSpsBusinatrDets").val("");
		  	var selFldVal= $(selObj).val();
		  	if (selFldVal == 'Others'){
		  		 $("#dfSpsBusNatOthSec").removeClass("invisible").addClass("show");
		  		 if(isEmpty($("#businessNatrDets").val())){
		  			 $("#dfSpsBusinatrDets").addClass("err-fld");
		  			 $("#dfSpsBusinatrDets").focus();
		  			 $("#dfSpsBusinatrDetsError").addClass("show").removeClass("invisible");
		  		 }else{
		  			 $("#dfSpsBusinatrDets").removeClass("err-fld");
		  			 $("#dfSpsBusinatrDetsError").removeClass("show").addClass("invisible");
		  		 }
		  	}else{
		  		$("#dfSpsBusNatOthSec").removeClass("show").addClass("invisible");
		  		$("#dfSpsBusinatrDetsError").removeClass("show").addClass("invisible");
		  	} 
	  }
	  
  
  }