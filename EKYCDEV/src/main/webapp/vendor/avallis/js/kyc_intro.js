var latestFNADetails = "";

                $(function ()
                		
                {
                	var prevFnaId = $("#prevFnaId").val();
                	
					$("#wizard").steps({
                        headerTag: "h2",
                        bodyTag: "section",
                        transitionEffect: "slideLeft",
                        stepsOrientation: "vertical",
						titleTemplate: '<span class="badge badge--info"></span> #title#',
						onFinishing: function (event, currentIndex) {
							
							var kycInroFrmDetls = $('#KycIntroFrm :input').serializeObject();
							//console.log(JSON.stringify(kycInroFrmDetls))
							
							var fnaIdFlg = $("#createNewFnaFlgs").val();
							
							//var prevFnaId = $("#prevFnaId").val();
							
							//alert("prevFnaId ------> " + prevFnaId);
							
							if(fnaIdFlg == "true"){
								fnaIdFlg = fnaIdFlg
							}else{
								fnaIdFlg = "false";
							}
							
						    $.ajax({
						    	    url:baseUrl+"/fnadetails/registerNew/"+fnaIdFlg,
						            type: "POST",
						            dataType: "json",
						            async:false,
						            contentType: "application/json",
						            data: JSON.stringify(kycInroFrmDetls),
						            success: function (response) {
						            //	alert(response)
						            	 $('#cover-spin').hide(0);
						            	 $("#fnaId").val(response.FNA_ID);//window.location.href="kycHome?FNA Id="+response.FNA_ID;
						            	 window.location.href="kycHome";
						            },
						            error: function(xhr,textStatus, errorThrown){
							 			$('#cover-spin').hide(0);
							 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
									}
						            });
							return true; 
						
						}
						
						
                    });
                    
                  
                   
                   
                    var emptObjFlg = isEmptyObj(latestFNADetails);
                	
                	if(emptObjFlg == true){
                		$("#noteInfoFnaId").removeClass("show").addClass("hide");
                	}else if(emptObjFlg == false){
                		$("#noteInfoFnaId").removeClass("hide").addClass("show");
                	}    
                    
                	
                	//poovathi add on 11-05-2021
                	var fnaIdFlg = $("#createNewFnaFlgs").val();
                	if((fnaIdFlg == "true")&&(!isEmpty(prevFnaId))){
                	    $("#noteInfoFnaId").removeClass("show").addClass("hide");
                     }
                    
              if(!isEmpty(latestFNADetails) && !isJsonObjEmpty(latestFNADetails) ){
            	  
            	  for(var fnaKey in latestFNADetails)
              			  
              			  if (latestFNADetails.hasOwnProperty(fnaKey)) {

              	        		var FnaVal = latestFNADetails[fnaKey];
              	        		
              	        		
              	        		switch(fnaKey){
              	        		
              	        		case "fnaId":
              	        			 $("#noteInfoFnaId").text("FNA Id : "+ FnaVal);
              	        			 $("#noteInfoFnaId").attr("title",FnaVal);
              	        			break;
              		      		
              		      		
              		      	  case "ccRepexistinvestflg":
              		      		 if((FnaVal == "Y")&&(FnaVal.length == 1)){
              		      			$("#ccRepexistinvestflgY").prop("checked",true);
              		      		    $("#ccRepexistinvestflgY").val(FnaVal);
              		      		    $("#ccRepexistinvestflg").val(FnaVal);
              		      		 }else if((FnaVal == "N")&&(FnaVal.length == 1)){
              		      			$("#ccRepexistinvestflgN").prop("checked",true);
              		      		    $("#ccRepexistinvestflgN").val(FnaVal);
              		      		    $("#ccRepexistinvestflg").val(FnaVal);
              		      		 }else{
              		      		   $("#ccRepexistinvestflgN").val("");
           		      		       $("#ccRepexistinvestflg").val("");
           		      		       $("#ccRepexistinvestflgY").val("");
        		      		       $("#ccRepexistinvestflg").val("");
        		      		       $("#ccRepexistinvestflgY").prop("checked",false);
        		      		       $("#ccRepexistinvestflgN").prop("checked",false);
              		      		 }
              		      		 break;
              		      			
              		           case "advdecOptions":
              		        	 var obj = JSON.parse(FnaVal);
              		        	  $("#advdecOptions").val(FnaVal);
              		        	 for (var key in obj) {
									    if (obj.hasOwnProperty(key)) {
									      
										           if((key == "NO1ALL")&&(obj[key] =="Y")){
											        	$("#htfrepdecno1all").prop("checked",true);
											       }
									        	   if((key == "NO1A")&&(obj[key] =="Y")){
									        		   $("#htfrepdecno1a").prop("checked",true);
											       }
											        if((key == "NO1B")&&(obj[key] =="Y")){
											        	$("#htfrepdecno1b").prop("checked",true);
											        }
											        if((key == "NO2")&&(obj[key] =="Y")){
											        	$("#htfrepdecno2").prop("checked",true);
											        }
											        if((key == "NO3")&&(obj[key] =="Y")){
											        	$("#htfrepdecno3").prop("checked",true);
											        }
											        if((key == "NO4")&&(obj[key] =="Y")){
											        	$("#htfrepdecno4").prop("checked",true);
											        }
											        if((key == "NO5")&&(obj[key] =="Y")){
											        	$("#htfrepdecno5").prop("checked",true);
											        }
									         
									    }
                                      }
              		        	   break;	
          		      			
              		           case "apptypesClient":
              		        	 var appObj = JSON.parse(FnaVal);
              		        	    $("#apptypesClient").val(FnaVal)  ;
              		        	 for (var key in appObj) {
									    if (appObj.hasOwnProperty(key)) {
									       // console.log(key + "" + appObj[key]);
									        if((key == "LIF1")&&(appObj[key]=="Y")){
									        	$("#htfcclife1").prop("checked",true);
									        	$("#htfcclife1").val(appObj[key]);
									         }
									        
									        if((key == "LIF2")&&(appObj[key]=="Y")){
									        	$("#htfcclife2").prop("checked",true);
									        	$("#htfcclife2").val(appObj[key]);
									         }
									     
									    }
                                   }
              		        	
         		      			   break;
         		      			   
         		      			   default:
         		      				   $("#"+fnaKey).val(FnaVal);
              		      			
              		      			
              	        		}
              		}
              	  }
              
           //poovathi add on 11-05-2021
          	var fnaIdFlg = $("#createNewFnaFlgs").val();
          	
          	if((fnaIdFlg == "true")&&(!isEmpty(prevFnaId))){
          	  
          	   $("#ccRepexistinvestflg").val("");
      		   $("#ccRepexistinvestflgY").val("");
   		       $("#ccRepexistinvestflg").val("");
   		       $("#ccRepexistinvestflgY").prop("checked",false);
   		       $("#ccRepexistinvestflgN").prop("checked",false);
   		       
   		       $("#htfcclife1").val("");
		       $("#htfcclife2").val("");
		       $("#htfcclife1").prop("checked",false);
		       $("#htfcclife2").prop("checked",false);
		       $("#apptypesClient").val("");
          		
               }
              
              $('#cover-spin').hide(0);
              
                });
            
function chkAdvDecOptions(chkbox){

		var advDecOpt = $("#advdecOptions");
		var jsonvalues = JSON.parse(isEmpty(advDecOpt.val()) ? "{}" : advDecOpt.val());
		var chkd = chkbox.checked;
		if(chkd == true){
			hideclntDeclError();
		}
		var val = chkbox.value;
	    var newobj=jsonvalues;
	    newobj[val]=(chkd == true ? "Y" :"N");
		advDecOpt.val(JSON.stringify(newobj));
}


//Declaration sec Validation 
function slctAllDeclTypes(obj){
	
	if($(obj).is(":checked")){
		
		$("#htfrepdecno1a").prop("checked",true);
		$("#htfrepdecno1b").prop("checked",true);
		$("#htfrepdecno2").prop("checked",true);
		$("#htfrepdecno3").prop("checked",true);
		$("#htfrepdecno4").prop("checked",true);
		$("#htfrepdecno5").prop("checked",true);
		var clntDelcObj = {NO1A:"Y",NO1B:"Y",NO1ALL:"Y",NO2:"Y",NO3:"Y",NO4:"Y",NO5:"Y"}
		$("#advdecOptions").val(JSON.stringify(clntDelcObj));
		hideclntDeclError();
		}
    if(!$(obj).is(":checked")){
    	
    	$("#htfrepdecno1a").prop("checked",false);
		$("#htfrepdecno1b").prop("checked",false);
		$("#htfrepdecno2").prop("checked",false);
		$("#htfrepdecno3").prop("checked",false);
		$("#htfrepdecno4").prop("checked",false);
		$("#htfrepdecno5").prop("checked",false);
		$("#advdecOptions").val("");
		showclntDeclError();
		
	}
	
}




//old kyc Reuse functions
function fnaAssgnFlg(obj, id) {
	var numberOfChecked = $("#advDclSec").find('input:checkbox:checked').length;
		if (id == "chk") {
			if (obj.checked) {
				obj.value = "Y";
				hideclntDeclError();
				if(numberOfChecked == 6){
					$("#htfrepdecno1all").prop("checked",true);
					$("#htfrepdecno1all").val("Y");
					var clntDelcObj = {NO1A:"Y",NO1B:"Y",NO1ALL:"Y",NO2:"Y",NO3:"Y",NO4:"Y",NO5:"Y"}
					$("#advdecOptions").val(JSON.stringify(clntDelcObj));
				}
			
			} else if (!obj.checked) {
				obj.value = "";
				
				if(numberOfChecked == 0){
					showclntDeclError();
			    }
				
				var otherval  = $("#htfrepdecno1all").val();
				if(otherval == "Y"){
					$("#htfrepdecno1all").prop("checked",false);
					$("#htfrepdecno1all").val("");
					
					var clntDelcObj = {NO1A:"Y",NO1B:"Y",NO2:"Y",NO3:"Y",NO4:"Y",NO5:"Y"}
					$("#advdecOptions").val(JSON.stringify(clntDelcObj));
					
				}
					
			}
			
		} else {
			if (obj.checked) {
				document.getElementById(id).value = obj.value;
			}
		}
	}


function chkJsnOptions(chkbox,jsnFld){
	    var advAppTypeOpt = $("#"+jsnFld+"");
		var jsonvalues = JSON.parse(isEmpty(advAppTypeOpt.val()) ? "{}" : advAppTypeOpt.val());
		var chkd = chkbox.checked;
		var val = $(chkbox).attr("data");

		var newobj=jsonvalues;
		newobj[val]=(chkd == true ? "Y" :"N");
		advAppTypeOpt.val(JSON.stringify(newobj));
	}



function validateApplcntType() {

	var chkOptOneLandN = document.getElementById('htfcclife1');
	var chkOptOneAandH = document.getElementById('htfccah1');
	var chkOptTwoLandN = document.getElementById('htfcclife2');
	var chkOptTwoAandH = document.getElementById('htfccah2');
	
	if ((chkOptOneLandN.checked) || (chkOptOneAandH.checked)
			|| (chkOptTwoLandN.checked) || (chkOptTwoAandH.checked)) {
		   showLifeError();
		  // hideSelcetAllLblProp();
		   
		} else if (!(chkOptOneLandN.checked) && !(chkOptOneAandH.checked)
			&& !(chkOptTwoLandN.checked) && !(chkOptTwoAandH.checked)) {
    	    $("#life12Error").removeClass("hide").addClass("show");

    	   hideLifeError();
         chkOptOneLandN.focus();
      }
}// end of validateApplcntType


function toggleExistInvest(obj){
var id = $(obj).attr("id");
if(id == 'ccRepexistinvestflgY'){
	if(obj.checked == true){
		$("#ccRepexistinvestflg").val("");
		$("#ccRepexistinvestflgN").val("");
		$("#ccRepexistinvestflgN").prop("checked",false)
		$("#ccRepexistinvestflg").val("Y");
		$("#ccRepexistinvestflgY").val("Y");
		hideRepexistinvestflgError();
	}
	else if(obj.checked == false){
		$("#ccRepexistinvestflgY").prop("checked",false)
        $("#ccRepexistinvestflg").val("");
		$("#ccRepexistinvestflgY").val("");
		showRepexistinvestflgError();
	}
}

if(id == 'ccRepexistinvestflgN'){
	if(obj.checked == true){
		$("#ccRepexistinvestflg").val("");
		$("#ccRepexistinvestflgY").val("");
		$("#ccRepexistinvestflgY").prop("checked",false)
		$("#ccRepexistinvestflg").val("N");
		$("#ccRepexistinvestflgN").val("N");
		hideRepexistinvestflgError();
	}
	else if(obj.checked == false){
		$("#ccRepexistinvestflgN").prop("checked",false)
        $("#ccRepexistinvestflg").val("");
		$("#ccRepexistinvestflgN").val("");
		showRepexistinvestflgError();
	}
}
	
}

function chkAdvAppTypeOptions(chkbox){

	var advAppTypeOpt = $("#apptypesClient");
	var jsonvalues = JSON.parse(isEmpty(advAppTypeOpt.val()) ? "{}" : advAppTypeOpt.val());
	var chkd = chkbox.checked;
	var val = $(chkbox).attr("data");

	var newobj=jsonvalues;
	newobj[val]=(chkd == true ? "Y" :"N");
	advAppTypeOpt.val(JSON.stringify(newobj));
}

function slectAllAppType(obj){
	
	//hide if any error message shows
	showLifeError();
	//hideRepexistinvestflgError();
	hideclntDeclError();
	
	if($(obj).is(":checked")){
		$("#htfcclife1").prop("checked",true);
		$("#ccRepexistinvestflgYN").prop("checked",true);
		$("#htfcclife2").prop("checked",true);
		$("#htfcclife1").val("Y");
		$("#ccRepexistinvestflg").val("Y");
		var advProcedObj = {LIF1: "Y",LIF2: "Y"};
		$("#apptypesClient").val(JSON.stringify(advProcedObj));
		}
    if(!$(obj).is(":checked")){
    	$("#htfcclife1").prop("checked",false);
		$("#ccRepexistinvestflgYN").prop("checked",false);
		$("#htfcclife2").prop("checked",false);
		$("#htfcclife1").val("");
		$("#ccRepexistinvestflg").val("");
		$("#apptypesClient").val("");
	}
}
function setJsonObjToElm(cont, data) {
	var val = JSON.stringify(data);
	
	$('input[name="' + cont + '"]').val("");
	if (!isEmpty(data) && !jQuery.isEmptyObject(val)) {
		$('input[name="' + cont + '"]').val(val);
		$.each(data, function(obj, val) {
			if (val == 'Y')
				$('input[data="' + obj + '"]').prop("checked", true);
			else
				$('input[data="' + obj + '"]').prop("checked", false);
		});
	}
}


function validateDeclSec(crntIndex){
	

	if( crntIndex == 2){
	   var numberOfChecked = $("#advDclSec").find('input:checkbox:checked').length;
	
		if(isEmptyFld(numberOfChecked)){
			showclntDeclError();
		   return ;
		}else{
			hideclntDeclError();
		}
}
	 return true;
}

//kyc Intro sec 3 validation part while click finish button 

function valiTypeOfAdvSec(crntIndex){
	
	if(crntIndex == 2){
		var life1  = $('input[name="htfcclife1"]:checked').val();
		var life2  = $('input[name="htfcclife2"]:checked').val();
		if((isEmptyFld(life1))&&(isEmptyFld(life2))){
			  hideLifeError();
			   return ;
			}else{
			  showLifeError();
			}
		
		var life3Flg  = $("#ccRepexistinvestflg").val();
			if(isEmptyFld(life3Flg)){
				showRepexistinvestflgError();
				   return ;
				}else{
				hideRepexistinvestflgError();
				}
	}
	  
 return true;
}

function hideLifeError(){
	$("#life1Error").addClass("err-fld");
	$("#life2Error").addClass("err-fld");
	$("#life12Error").removeClass("hide").addClass("show");
}

function showLifeError(){
	$("#life1Error").removeClass("err-fld");
	$("#life2Error").removeClass("err-fld");
	$("#life12Error").removeClass("show").addClass("hide");
}
function showRepexistinvestflgError(){
	 $("#repInvExistFlgError").addClass("err-fld");
	 $("#repInvExtFlg").removeClass("hide").addClass("show"); 
}

function hideRepexistinvestflgError(){
	$("#repInvExistFlgError").removeClass("err-fld");
	$("#repInvExtFlg").removeClass("show").addClass("hide");
 }

function hideclntDeclError(){
	$("#advDclSec").css("border", "");
	$("#advDclSec").find("#appClntChioceError").addClass("hide").removeClass("show");
}
 
function showclntDeclError(){
	$("#advDclSec").css("border", "1px solid #f75c02");
	$("#advDclSec").find("#appClntChioceError").addClass("show").removeClass("hide");
}

//uncheck select all label if life and inv flag is unchecked

/*function hideSelcetAllLblProp(){
	var selAllFld = $("#chkSelectAll").is(":checked");
	if(selAllFld == true){
		$("#chkSelectAll").prop("checked",false);
	}else{
		
	}
}*/

function fnaAssgnFlgs(obj, id) {
	if (id == "chk") {
		if (obj.checked) {
			obj.value = "Y";
			//enableselectAllOpt();
			
		} else if (!obj.checked) {
		    obj.value = "";
		}
		
	} else {
		if (obj.checked) {
			document.getElementById(id).value = obj.value;
		}
	}
}

/*function enableselectAllOpt(){
	var numberOfChecked = $("#AppTypeSec").find('input:checkbox:checked').length;
	  if(numberOfChecked == 3){
        	document.getElementById("chkSelectAll").checked = true;
			//$("#chkSelectAll").trigger("click");
			//$("#chkSelectAll").prop("cheked",true);
		}
}*/


function showClientInfo(custId) {
	//poovathi add on 07-07-2021
    var custId = window.btoa(custId);
    //end
	window.location.href="clientInfo?c="+custId;
}