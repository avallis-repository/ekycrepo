var jsnDataFnaExpDets="",jsnDataFnaDepentDets="",jsnDataFnaDetails="",jsnDataSelfSpsDetails="";


$(document).ready(function(){
	
	
	 //hide and show dependent , cash Flow / asset sec start
	 $('#fwrDepntflg').on('click', function(){
		 if($(this).is(":checked") == true){
			// $("#depnNote").trigger("mouseover")
			 $("#collapseDepend").removeClass("hide").addClass("show"); 
			 $(this).val("Y");
			 
		 }else{
			 $("#collapseDepend").addClass("hide").removeClass("show");
			 $(this).val("N");
			 //$("#depnNote").trigger("mouseout")
		 }
	    });
	 
      $('#fwrFinassetflg').on('click', function(){
			 if($(this).is(":checked") == true){
				 //$("#cashAssetNote").trigger("mouseover")
				 $(this).val("Y");
				 $("#collapsecashFlow").removeClass("hide").addClass("show");; 
			 }else{
				 $("#collapsecashFlow").addClass("hide").removeClass("show");
				 $(this).val("N");
				 //$("#cashAssetNote").trigger("mouseout")
			 }
	   });
      
      //hide and show dependent , cash Flow / asset sec End
		 $('[data-toggle="pg4CashFlwNote"]').popover({
		   
		   placement :'bottom', 
	       html: true,
	       content: function() {
	         return $('#popover-pg4CashFlwNote').html();
	       }
	     });
	   
	   $('[data-toggle="pg4AssetNote"]').popover({
		   
		   placement :'top',
	       html: true,
	       content: function() {
	         return $('#popover-pg4AssetNote').html();
	       }
	     });
	   
	    $('[data-toggle="Pg4assetETANote"]').popover({
	    	placement :'left',
	    	html: true,
	       content: function() {
	         return $('#popover-Pg4assetETANote').html();
	       }
	     }); 
	    
	    $('[data-toggle="Pg4assetETLNote"]').popover({
	    	placement :'left',
	    	html: true,
	        content: function() {
	          return $('#popover-Pg4assetETLNote').html();
	        }
	      }); 

	    
	    $('[data-toggle="pg4DependNote"]').popover({
		    placement :'left',   
	        html: true,
	        content: function() {
	         return $('#popover-pg4DependNote').html();
	       }
	     });

	    
	    
	    
	    if(!isEmpty(jsnDataFnaDetails)  && !isJsonObjEmpty(jsnDataFnaDetails)){
			  for(var obj in jsnDataFnaDetails){
				  if (jsnDataFnaDetails.hasOwnProperty(obj)) {
					  var objValue = jsnDataFnaDetails[obj];
					  
					  var nodetype = $("#"+obj).prop('nodeName');
					  var inputtype = $("#"+obj).prop("type");
					  
					  switch(obj){
					  
					  case "fwrDepntflg":
					  if(objValue == 'Y'){
						  
						  $("#"+obj).prop("checked",true);
						  $("#"+obj).val(objValue);
						  $("#collapseDepend").removeClass("hide").addClass("show"); 
					   }else{
						   
						$("#"+obj).prop("checked",false);
						$("#"+obj).val("N");
						$("#collapseDepend").removeClass("show").addClass("hide"); 
					  }
					  break;
					  
					  case "fwrFinassetflg":	  
						  if(objValue == 'Y'){
							 
							  $("#"+obj).prop("checked",true);
							  $("#"+obj).val(objValue);
							  $("#collapsecashFlow").removeClass("hide").addClass("show"); 
						   }else{
							  
							$("#"+obj).prop("checked",false);
							$("#"+obj).val("N");
							$("#collapsecashFlow").removeClass("show").addClass("hide"); 
						  }
						  break;
					  default:		
						$("#"+obj).val(objValue);
					  }
					  
				  }
				  
			  }
	
	
		  }
		  
		  
		  
	    if(!isEmpty(jsnDataFnaExpDets)  && !isJsonObjEmpty(jsnDataFnaExpDets)){
			  for(var obj in jsnDataFnaExpDets){
				  if (jsnDataFnaExpDets.hasOwnProperty(obj)) {
					  var objValue = jsnDataFnaExpDets[obj];
					  
					  var nodetype = $("#"+obj).prop('nodeName');
					  var inputtype = $("#"+obj).prop("type");
					  
					  switch(obj){
					  
					  default:		
						  
						  if(inputtype == "radio" || inputtype == "checkbox"){
							  if(objValue == "Y"){
								  $("#"+obj).prop("checked",true);
							  }
						  }
						  
						  $("#"+obj).val(objValue);
					  }
					  
				  }
				  
			  }
		  }
	    
	   
	    
	    
	    //show and  hide dependent image based on dependent List length
       var emptObjFlg = jsnDataFnaDepentDets.length;
		
		if(emptObjFlg == 0){
			$("#NoDepnDetlsRow").removeClass("hide").addClass("show");
		}else if(emptObjFlg > 0){
			$("#NoDepnDetlsRow").removeClass("show").addClass("hide");
		}
		
		 //Poovathi add on 25-05-2021 for create new fnaList
		var expnObjLen = !isEmptyObj(jsnDataFnaExpDets);
	    var prevFnaId = $("#prevFnaId").val();
	    var fnaIdFlg =  $("#createNewFnaFlgs").val();
	      if((fnaIdFlg == "true") && (!isEmpty(prevFnaId))){
	    		if(emptObjFlg > 0 ){
	    			 $("#fwrDepntflg").prop("checked",true);
	    			 $("#fwrDepntflg").val("Y");
					 $("#collapseDepend").removeClass("hide").addClass("show"); 
	    		}
	    		var selfInc = $("#cfSelfAnnlinc").val();
	    		if(!isEmpty(selfInc)){
	    			$("#fwrFinassetflg").prop("checked",true);
	    			$("#fwrFinassetflg").val("Y");
	    			$("#collapsecashFlow").removeClass("hide").addClass("show"); 
	    		}
	    		
	    		
    	}
		 
	      
	    for(var i=0;i<jsnDataFnaDepentDets.length;i++){
	           
            addListItem(jsnDataFnaDepentDets[i]);
        }
	    
	 

	var fnaId=$("#hTxtFldCurrFNAId").val();
		getAllSign(fnaId);
	    setPageManagerSts();
		setPageAdminSts();
		setPageComplSts();
	})



	
function getAllDepnData(){
  //document.getElementById('loader').style.display = 'block';
  pageLoaderShow();
   $.ajax({
            url:"FnaDependantDets/getAllData",
            type: "GET",
            dataType: "json",
			async:false,
            contentType: "application/json",
           
            success: function (data) {
                 if(data.length>0){
                    $('#fwrDepntflg').click();
                 }
	            for(var i=0;i<data.length;i++){
		           
		            addListItem(data[i]);
		        }
            	
            	pageLoaderHide();

            },
            error: function(xhr,textStatus, errorThrown){
	 			//$('#cover-spin').hide(0);
	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			}
            });

}
function pageLoaderShow(){
    $(".page-loader").show();
	$(".overlay").show();
}
function pageLoaderHide(){
   $(".page-loader").hide();
   $(".overlay").hide();
}


function addListItem(dependent){

    // gender validation
	var genIcon = "";
	var genColor = "";
	if(isEmpty(dependent.depnGender)){
		genIcon = "";
		dependent.depnGender = "NIL"  
    }else{
    	if(dependent.depnGender == "Male"){
    		genIcon="fa fa-male imale";
    	}
    	else if(dependent.depnGender == "Female"){
    		genIcon="fa fa-female iFemale";
    	}
    }
	
	//age validation
	if(isEmpty(dependent.depnAge)){
		dependent.depnAge = "NIL"  
    }else{
    	dependent.depnAge = dependent.depnAge + " Yrs."
    }
	
	//yrs to sup validation
	if(isEmpty(dependent.depnYrssupport)){
		dependent.depnYrssupport = "NIL"  
    }else{
    	dependent.depnYrssupport = dependent.depnYrssupport + " Yrs."
    }
	
	//Relationship validation
	if(isEmpty(dependent.depnRelationship)){
		dependent.depnRelationship = "NIL"  
    }
	
	//occupation validation 
	
	if(isEmpty(dependent.depnOccp)){
		dependent.depnOccp = "NIL"  
    }
	
var listGroup='<a id='+dependent.depnId+' href="#" class="list-group-item list-group-item-action flex-column align-items-start p-1">'
    +'<div class="d-flex w-100 justify-content-between">'
      +'<h6 class="mb-1 text-primaryy font-sz-level6 bold">'+dependent.depnName+'</h6><input type="hidden" id="htxtdepnName" name="htxtdepnName" value="'+dependent.depnName+'">'
      +'<small id="'+dependent.depnId+'"><i class="fa fa-pencil-square-o mr-2" aria-hidden="true" title="Edit Dependent data" style="color:blue;" onclick="getEditData(this);"></i>&nbsp;&nbsp;<i class="fa fa-trash checkdb mr-1" aria-hidden="true" title="Delete Dependent data" style="color: red;" onclick="dltData(this);"></i></small>'
    +'</div>'
   +' <div class="row">'
         
        +' <div class="col-md-12" id="FirstRow">'
             +'<div class="row">'
               +'  <div class="col-md-4" id="gender">'
                 +'<span class="font-sz-level7  text-custom-color-gp"><strong>Gender&nbsp;:&nbsp;</strong></span>&nbsp;<span class="font-sz-level7 font-normal"><i class="'+genIcon+'" aria-hidden="true"></i> <span id="gndrtxt">&nbsp;'+dependent.depnGender+'</span></span>'
                 +'</div>'
                 +'<div class="col-md-3" id="age">'
                 +' <span class="font-sz-level7  text-custom-color-gp"><strong>Age&nbsp;:&nbsp;</strong></span>&nbsp;<span class="font-sz-level7 font-normal">'+dependent.depnAge+'</span>'
                 +'</div>'
                +'<div class="col-md-5" id="yrsSup">'
                  +'<span class="font-sz-level7  text-custom-color-gp"><strong >No of Yrs to Support&nbsp;:&nbsp;</strong></span>&nbsp;<span class="font-sz-level7 font-normal">'+dependent.depnYrssupport+' </span>'
                 +'</div>'
                 
            +'</div>'
         +'</div>'
        
         +'<div class="col-md-12" id="secondRow">'
           +' <div class="row">'
                 +'<div class="col-md-4" id="relation">'
                 +'<span class="font-sz-level7  text-custom-color-gp"><strong>RelationShip&nbsp;:&nbsp;</strong></span>&nbsp;<span class="font-sz-level7 font-normal">'+dependent.depnRelationship+'</span>'
                 +'</div>'
                 +'<div class="col-md-8" id="occupation">'
                  +'<span class="font-sz-level7  text-custom-color-gp"><strong>Occupation&nbsp;:&nbsp;</strong></span>&nbsp;<span class="font-sz-level7 font-normal">'+dependent.depnOccp+'.</span>'
                 +'</div>'
            +'</div>'
       +'</div>'
    +'</div>';
  
   var ListLen = $("#addlist").find("#"+dependent.depnId).length;
    if(ListLen == 0){
    	$("#NoDepnDetlsRow").removeClass("show").addClass("hide");
	    $("#addlist").append(listGroup);
    }

 

}

//Delete Data
function dltData(thisobj){
	
	  var depnId = $(thisobj).parent().attr("id");
	  var depnName = $("#"+depnId).find('input:hidden[name=htxtdepnName]').val();
	  
	   const swalWithBootstrapButtons = Swal.mixin({
			  customClass: {
			    confirmButton: 'btn btn-success',
			    cancelButton: 'btn btn-danger mr-3'
			  },
			  buttonsStyling: false
			})

		swalWithBootstrapButtons.fire({
		  title: 'Are you sure?',
		  text: "You will not be able to recover this Dependent Details : (" +depnName+ ")",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonText: 'Yes, delete it!',
		  cancelButtonText: 'No, cancel!',
		  reverseButtons: true
		}).then((result) => {
		  if (result.isConfirmed) {
		        $.ajax({
				
		          url:"fnaDependent/delDepn/"+depnId,
		          type: "POST",async:false,
		          success: function (response) {
		          	$(thisobj).parents("#"+depnId).remove();
		          	var totlLst = $("#addlist").children().length;
		          	
		          	if(totlLst == 0){
		          		$("#NoDepnDetlsRow").removeClass("hide").addClass("show");
		          	}
		          	 swalWithBootstrapButtons.fire("Deleted!", "Your Dependent : (" +depnName+ ")  Details has been deleted.", "success");
		          	//checkdb onchange function for delete operation
		          	enableReSignIfRequiredforEdtDeltAction();
		          },
		          error: function(xhr,textStatus, errorThrown){
			 			//$('#cover-spin').hide(0);
			 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
					}
		          });
		    
		    
		  } else if (
		    /* Read more about handling dismissals below */
		    result.dismiss === Swal.DismissReason.cancel
		  ) {
			  swalWithBootstrapButtons.fire(
		    		  "Cancelled", "Your Dependent  (" +depnName+ ") Details is safe :)", "error")
		  }
		})	
			
}//End of Delete Each Rider Plan Details...
	  
	  
	


//Edit dependent data

function getEditData(thisobj){
	
	//checkdb onchange function for edit operation
 	enableReSignIfRequiredforEdtDeltAction();
 	
	clearData();
	setEdtFun();
	
	
	var depnId = $(thisobj).parent().attr("id");
	
	$.ajax({
            url:"fnaDependent/getDataById/"+depnId,
            type: "GET",
            dataType: "json",async:false,
            contentType: "application/json",
           
            success: function (dependent) {
            	 setDepndata(dependent);
              },
              error: function(xhr,textStatus, errorThrown){
  	 			//$('#cover-spin').hide(0);
  	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
  			}
            });

}

function setEdtFun(){
	$("#hdnMode").val("E");
	$("#btnSaveDepnData").attr("value","Edit Details");
	$("#btnSaveDepnData").removeClass("btn-success").addClass("btn-primary");
}

function setInsFun(){
	$("#hdnMode").val("I");
	$("#btnSaveDepnData").attr("value","Add Details");
	$("#btnSaveDepnData").removeClass("btn-primary").addClass("btn-success");
}

function setDepndata(dependent){
	
	if(!isEmpty(dependent[0][0])){
		$("#depnId").val(dependent[0][0]);
	}
	
	if(!isEmpty(dependent[0][2])){
		$("#depName").val(dependent[0][2]);
	}
	
	if(!isEmpty(dependent[0][3])){
		$("#depnRelationship").val(dependent[0][3])
	}else{
		$("#depnRelationship").val("");
	}
	
	
	if(!isEmpty(dependent[0][4])){
		$("#depnAge").val(dependent[0][4]);
	}else{
		$("#depnAge").val("");
	}
	
	if(!isEmpty(dependent[0][6])){
		$("#depnYrssupport").val(dependent[0][6]);
	}else{
		$("#depnYrssupport").val("");
	}
	
	if(!isEmpty(dependent[0][12])){
		$("#depnOccp").val(dependent[0][12]);
	}else{
		$("#depnOccp").val("");
	}
	
	// set male and female value here
	
	if(dependent[0][5] == "Male"){
		$("#radBtnDepnmale").prop("checked",true)
		$('input[name="depnGender"]:checked').val(dependent[0][5]);
	}
	else if((dependent[0][5] == "Female")){
		$("#radBtnDepnfemale").prop("checked",true);
		$('input[name="depnGender"]:checked').val(dependent[0][5]);
	}else{
		$('input[name="depnGender"]:checked').val("");
	}
	
}



var arrDependentDet=["depnName","depnAge","depnYrssupport","depnRelationship","depnOccp","depnId"];
function setDependentData(dependent){
	
	if(dependent.depnGender != "Male"){
		$('#depnGender').addClass('active');
	}else{
		$('#depnGender').removeClass('active');
	}
		
	for(var i=0;i<arrDependentDet.length;i++){
		document.getElementsByName(arrDependentDet[i])[0].value=dependent[0][arrDependentDet[i]];
	}
}
function getDependantData(dependent){
    for(var i=0; i<arrDependentDet.length; i++){
		dependent[arrDependentDet[i]]=document.getElementsByName(arrDependentDet[i])[0].value;
	}
    
   var depnGndr  =   $('input[name="depnGender"]:checked').val();
          dependent.depnGender = depnGndr;
         
	return dependent;

}
function saveDependantData(){
	
	var mode = $("#hdnMode").val();
	
	if(mode == 'I'){
		
		$("#depnId").val("");
		
		//Name validation
	    var depnName=$("#depName").val();
	    if(isEmpty(depnName)){
	          $("#depName").addClass("err-fld");
	          $("#depnNameError").addClass("show").removeClass("hide");
	          return false;
	    }else{
	    	$("#depName").removeClass("err-fld");
			$("#depnNameError").addClass("hide").removeClass("show");
	    }
		 
	         
	       
		var dependent={};
		dependent=getDependantData(dependent);
		var strFnaId = $("#hTxtFldCurrFNAId").val();
		$.ajax({
	            url:"fnadetails/dependant/register/"+strFnaId,
	            type: "POST",
	             data: JSON.stringify(dependent),
	             dataType: "json",async:false,
	            contentType: "application/json",
	           
	            success: function (data) {
//	               $("#"+data.depnId).remove();
	            	addListItem(data);
	            	clearData();
	            	
	            	
	            },
	            error: function(xhr,textStatus, errorThrown){
		 			//$('#cover-spin').hide(0);
		 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
				}
	            });
		
		chkNextChange();
	}
	
	if(mode == 'E'){
		
		var depnId = $("#depnId").val();
		
		//Name validation
	    var depnName=$("#depName").val();
	    if(isEmpty(depnName)){
	          $("#depName").addClass("err-fld");
	          $("#depnNameError").addClass("show").removeClass("hide");
	          return false;
	    }else{
	    	$("#depName").removeClass("err-fld");
			$("#depnNameError").addClass("hide").removeClass("show");
	    }
	
		var dependent={};
		dependent=getDependantData(dependent);
		var strFnaId = $("#hTxtFldCurrFNAId").val();
		
		$.ajax({
            url:"fnadetails/dependant/update/"+depnId,
            type: "POST",
             data: JSON.stringify(dependent),
             dataType: "json",async:false,
            contentType: "application/json",
           
            success: function (data) {
                
            	var curDepnId = $("#depnId").val();
                 
            	var depnGndr  =   $('input[name="depnGender"]:checked').val();
            	
            	
            	if(depnGndr == 'Male'){
            		var gndrClass = "fa fa-male imale";
            		$("#addlist").find("#"+curDepnId).find("#gender") .children().eq(1).children().eq(0).removeClass("fa fa-female iFemale").addClass(gndrClass);
            	}
            	else if(depnGndr == 'Female'){
            		var gndrClass = "fa fa-female iFemale";
            		$("#addlist").find("#"+curDepnId).find("#gender") .children().eq(1).children().eq(0).removeClass("fa fa-male imale").addClass(gndrClass);
            	}else{
            		var gndrClass = "";
            		$("#addlist").find("#"+curDepnId).find("#gender") .children().eq(1).children().eq(0).removeClass("fa fa-male imale").addClass(gndrClass);
            		depnGndr = "NIL";
            	}
            	
            	
            	var depnAge = $("#depnAge").val();
            	var depnYrssupport = $("#depnYrssupport").val();
            	var depnRelationship = $("#depnRelationship option:selected").val();
            	var depnOccp = $("#depnOccp").val()
            	
            	
            	//age validation
            	if(isEmpty(depnAge)){
            		depnAge = "NIL"  
                }else{
                	depnAge = depnAge +" Yrs."
                }
            	
            	//yrs to sup validation
            	if(isEmpty(depnYrssupport)){
            		depnYrssupport = "NIL"  
                }else{
                	depnYrssupport = depnYrssupport +" Yrs."
                }
            	
            	//Relationship validation
            	if(isEmpty(depnRelationship)){
            		depnRelationship = "NIL"  
                }
            	
            	//occupation validation 
            	if(isEmpty(depnOccp)){
            		depnOccp = "NIL"  
                }
            	
            	// set edited values to List Items
            	$("#addlist").find("#"+curDepnId).find("#gender") .children().eq(1).find("#gndrTxt").text(depnGndr);
            	$("#addlist").find("#"+curDepnId).children().eq(0).find(".bold").text($("#depName").val());
            	$("#addlist").find("#"+curDepnId).children().eq(0).find('input:hidden[name=htxtdepnName]').val($("#depName").val());
            	$("#addlist").find("#"+curDepnId).find("#age") .children().eq(1).text(depnAge);
            	$("#addlist").find("#"+curDepnId).find("#yrsSup") .children().eq(1).text(depnYrssupport);
            	$("#addlist").find("#"+curDepnId).find("#relation") .children().eq(1).text(depnRelationship);
            	$("#addlist").find("#"+curDepnId).find("#occupation") .children().eq(1).text(depnOccp);
                
            	clearData();
            },
	            error: function(xhr,textStatus, errorThrown){
		 			//$('#cover-spin').hide(0);
		 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
				}
            });
		
		chkNextChange();
		
	}
   
}


function clearData(){
     for(var i=0; i<arrDependentDet.length; i++){
		document.getElementsByName(arrDependentDet[i])[0].value="";
	}
	
	$("#radBtnDepnmale").prop("checked",false)
 	$("#radBtnDepnfemale").prop("checked",false);
 	$('input[name="depnGender"]:checked').val("");
 	
 	// set Insert mode after clear form Details
 	   setInsFun();
 	 
 	 // hide depn error msg --> when it shows in screen
 	    $("#depName").removeClass("err-fld");
		$("#depnNameError").addClass("hide").removeClass("show");
 	 
}
//Validation
function isValidName(){
	var depnName=document.getElementsByName("depnName")[0].value;
	if(isEmpty(depnName)){
        $("#depName").addClass("err-fld");
        $("#depnNameError").addClass("show").removeClass("hide");
        return false;
  }else{
  	    $("#depName").removeClass("err-fld");
		$("#depnNameError").addClass("hide").removeClass("show");
  }

}
function isNumber(event,errMsgId){
    
    event = (event) ? event : window.event;
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    	//poovathi add 0n 03-06-2021
    	$("#"+errMsgId).removeClass("hide").addClass("show");
	        return false;
	    }
    $("#"+errMsgId).removeClass("show").addClass("hide");
    return true;
	
}

//CashFlow

function calcFinCommit(txtobj,op){
    var txtval = Number(txtobj.value);
    //Self
	var sa  = $("#cfSelfAnnlinc").val()
	var sb = $("#cfSelfCpfcontib").val();
	var sc = $("#cfSelfEstexpense").val();

    var stot = Number(sa)-Number(sb)-Number(sc);

	
	$("#cfSelfSurpdef").val(stot);
	//Spouse
	var spa  = $("#cfSpsAnnlinc").val()
	var spb = $("#cfSpsCpfcontib").val();
	var spc = $("#cfSpsEstexpense").val();
    var sptot = Number(spa)-Number(spb)-Number(spc);

	$("#cfSpsSurpdef").val(sptot);
	//Combined
	var sfa  = $("#cfFamilyAnnlinc").val()
	var sfb = $("#cfFamilyCpfcontib").val();
	var sfc = $("#cfFamilyEstexpense").val();
	var sftot = Number(sfa)-Number(sfb)-Number(sfc);
	$("#cfFamilySurpdef").val(sftot);
	
	//poovathi add on 03-06-2021
	// $("#"+txtobj+"ErrMsg").removeClass("show").addClass("hide");
}
//asset & Liablities
function calcCombined(txtobj){
    var parrow= txtobj.parentNode.parentNode;
   /* var client = parrow.cells[1].childNodes[0];//alSelfTotasset
    var spouse = parrow.cells[2].childNodes[0]//alSpsTotasset
	var combined = parrow.cells[3].childNodes[0]//alFamilyTotasset
   */	
    //poovathi change field Type (Table to TextField) on 12-11-2020
	var client = $("#alSelfTotasset");//alSelfTotasset
    var spouse = $("#alSpsTotasset");//alSpsTotasset
	var combined = $("#alFamilyTotasset");//alFamilyTotasset
    
	combined.value = Number(client.value)+Number(spouse.value)
	
    var sfa  = $("#cfFamilyAnnlinc").val()
	var sfb = $("#cfFamilyCpfcontib").val();
	var sfc = $("#cfFamilyEstexpense").val();
	var sftot = Number(sfa)-Number(sfb)-Number(sfc);

	$("#cfFamilySurpdef").val(sftot);


	var sa2  = $("#alFamilyTotasset").val()
	var sb2 = $("#alFamilyTotliab").val()
	$("#alFamilyNetasset").val(Number(sa2)-Number(sb2))
	
	//poovathi add on 03-06-2021
	 //$("#"+txtobj+"ErrMsg").removeClass("show").addClass("hide");
}


function calcAsset(txtobj){
	var txtval = Number(txtobj.value)

	var sa  = $("#alSelfTotasset").val()
	var sa1  = $("#alSpsTotasset").val()
	var sa2  = $("#alFamilyTotasset").val()

	var sb  = $("#alSelfTotliab").val()
	var sb1  = $("#alSpsTotliab").val();
	var sb2 = $("#alFamilyTotliab").val()

	$("#alSelfNetasset").val(Number(sa)-Number(sb))
	$("#alSpsNetasset").val(Number(sa1)-Number(sb1))
	$("#alFamilyNetasset").val(Number(sa2)-Number(sb2))
	
	//poovathi add on 03-06-2021
	 //$("#"+txtobj+"ErrMsg").removeClass("show").addClass("hide");
}

//Save cashflow and Assert details

var arrExpendData=["expdId","cfSelfAnnlinc","cfSelfCpfcontib","cfSelfEstexpense","cfSelfSurpdef",
					"cfSpsAnnlinc","cfSpsCpfcontib","cfSpsEstexpense","cfSpsSurpdef",
					"cfFamilyAnnlinc","cfFamilyCpfcontib","cfFamilyEstexpense","cfFamilySurpdef",
					"alSelfTotasset","alSelfTotliab","alSelfNetasset",
					"alSpsTotasset","alSpsTotliab","alSpsNetasset",
					"alFamilyTotasset","alFamilyTotliab","alFamilyNetasset"];
					
function saveData(){
 
  var expendDets={};
  for(var i=0;i<arrExpendData.length;i++){
  		expendDets[arrExpendData[i]]=document.getElementsByName(arrExpendData[i])[0].value;
  }
  
  $.ajax({
            url:"ExpendDets/saveData",
            type: "POST",
             data: JSON.stringify(expendDets),
             dataType: "json",async:false,
            contentType: "application/json",
           
            success: function (data) {
               //alert("success");
            },
            error: function(xhr,textStatus, errorThrown){
	 			//$('#cover-spin').hide(0);
	 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
			}
            });
  
  
}

//Get Cashflow and Asset data
function getCashAssetData(fnaId){
   
	 $.ajax({
            url:"ExpendDets/getDataById/"+fnaId,
            type: "GET",
            dataType: "json",async:false,
            contentType: "application/json",
           
            success: function (data) {
                 
	            setCashFlowAndAssetData(data);
            	
            	
            },
	            error: function(xhr,textStatus, errorThrown){
		 			//$('#cover-spin').hide(0);
		 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
				}
            });
	

}

function setCashFlowAndAssetData(expendData){
	 for(var i=0;i<arrExpendData.length;i++){
  		document.getElementsByName(arrExpendData[i])[0].value=expendData[arrExpendData[i]];
  }
}


 

function saveCurrentPageData(obj,nextscreen,navigateflg,infoflag){
	
	var frmElements = $('#frmDepntFinWell :input').serializeObject();
	
	$('#frmDepntFinWell input[type=checkbox]').each(function() {
		
		var chekboxvalue = "N"
		
		if(this.value == "on" || this.value == "Y")chekboxvalue  = "Y";
		
		frmElements[this.name] = chekboxvalue; 
		
	});
		$.ajax({
	            url:"fnadetails/register/financewell",
	            type: "POST",
	            async:false,
	            dataType: "json",
	            contentType: "application/json",
	            data: JSON.stringify(frmElements),
	            success: function (response) {
	            	$("#expdId").val(response.expdId);
					if(navigateflg){
		            	window.location.href=nextscreen;
					}
	            },
	            error: function(xhr,textStatus, errorThrown){
		 			//$('#cover-spin').hide(0);
		 		    ajaxCommonError(textStatus,xhr.status,xhr.statusText);
				}
	        });
	  }

//depnName onchange hide error msg

$(".depnchkMand").bind("change",function(){
	var thisfld = $(this).val();
	if(thisfld.length>0){
		  $("#depName").removeClass("err-fld");
		  $("#depnNameError").addClass("hide").removeClass("show");
	  
	}
});
