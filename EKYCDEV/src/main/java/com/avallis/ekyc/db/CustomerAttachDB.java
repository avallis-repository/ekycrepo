package com.avallis.ekyc.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.web.context.WebApplicationContext;

import com.avallis.ekyc.dbinterfaces.DBIntf;
import com.avallis.ekyc.dto.CustomerAttachments;
import com.avallis.ekyc.utils.EKYCQuery;

public class CustomerAttachDB {

	static Logger kyclog = Logger.getLogger(CustomerAttachDB.class.getName());

	public List<CustomerAttachments> getCustAttachByCustId(DBIntf dbDTO, String strCustId) {

		List<CustomerAttachments> custDocs = new ArrayList<CustomerAttachments>();
		custDocs = (List<CustomerAttachments>) dbDTO
				.searchByNativeSQLQuery(EKYCQuery.STR_HQL_CUSTATTACH_BY_CUSTID + "'" + strCustId + "'");
		return custDocs;
	}

	public List<CustomerAttachments> getCustAttachByCustIdFnaId(DBIntf dbDTO, String strCustId, String strFNAId) {

		List<CustomerAttachments> custDocs = new ArrayList<CustomerAttachments>();
		// poovathi add on 11-05-2021
		String whereClass = " where CUSTID= '" + strCustId + "' AND FNA_ID = '" + strFNAId + "'";
		custDocs = (List<CustomerAttachments>) dbDTO
				.searchByNativeSQLQuery(EKYCQuery.STR_HQL_CUSTATTACH_BY_CUSTID + whereClass);
		return custDocs;
	}

	public void insertCustAttach(DBIntf dbDTO, List<String> list) {
		for (String query : list) {
			
			

			dbDTO.updateDbByNativeSQLQuery(query);
		}
	}

	public String insertCustAttForFnaAdmin(DBIntf dbDTO, String strCustId, String strGenFileName,
			String strAtmtFileSize, String strAtmtCreatedBy, String custdir, String strBFILEName, String strDistId,
			String strFnaId) throws HibernateException {

		String custAttachId = dbDTO.createSeqIdVal(EKYCQuery.STR_SQL_CUSTATTACH_NEXTID);
		
		
		String insQry = "INSERT INTO CUSTOMER_ATTACHMENTS "
				+ "(CUST_ATTACH_ID,CUSTID,TITLE,PAGE_NUM,FILENAME,FILESIZE,FILETYPE,ATTACH_CATEG_NAME,CREATED_BY,CREATED_DATE,DISTRIBUTOR_ID,DOCUMENT,FNA_ID,ATTACH_FOR)VALUES "
				+ "('" + custAttachId + "','" + strCustId + "','FNA','-NA-'," + "'" + strGenFileName + "','"
				+ strAtmtFileSize + "','pdf'," + "'KYC','" + strAtmtCreatedBy + "',SYSDATE," + "'" + strDistId
				+ "',BFILENAME('" + custdir + "','" + strBFILEName + "'),'" + strFnaId + "','FNA_ADMIN_AUTO')";

		dbDTO.updateDbByNativeSQLQuery(insQry);

		return custAttachId;
	}

	public ResultSet downloadNRICAttachments(DBIntf dto, String strClntId, String strDistId,
			WebApplicationContext context, String strFNAID) throws HibernateException {
		ResultSet attachDocList = null;

		
		try {
			String strWhrcondQry = 
					 " where (attch.CUSTID)=(SELECT CUST_ID FROM FNA_DETAILS WHERE FNA_ID='" + strFNAID + "') "
					+ " and attch.TITLE='NRIC/PASSPORT' " + " and  (attch.DISTRIBUTOR_ID) = '" + strDistId + "'";

			attachDocList = dto.downloadBFILE(EKYCQuery.HQL_FIND_CLTATTACH_BY_ID + strWhrcondQry, "LIFE", context);
			
		} catch (HibernateException he) {
			kyclog.error(" -- > downloadNRICAttachments error " + he);
		}
		return attachDocList;
	}

	public void deleteCustAttachemtDetls(DBIntf dbDTO, String strCustAttachId) {
		String STR_DEPN_DEL_QRY = EKYCQuery.STR_SQL_CUSTATTACH_DEL +"  WHERE CUST_ATTACH_ID ='" + strCustAttachId + "'";
		dbDTO.updateDbByNativeSQLQuery(STR_DEPN_DEL_QRY);

	}

}
