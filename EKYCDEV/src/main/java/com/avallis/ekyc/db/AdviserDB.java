package com.avallis.ekyc.db;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.context.ApplicationContext;

import com.avallis.ekyc.dbinterfaces.DBImplementation;
import com.avallis.ekyc.dbinterfaces.DBIntf;
import com.avallis.ekyc.dto.AdviserStaff;
import com.avallis.ekyc.dto.AdvstfCodes;
import com.avallis.ekyc.utils.ApplicationContextUtils;
import com.avallis.ekyc.utils.KycConst;
import com.avallis.ekyc.utils.KycUtils;

public class AdviserDB {
	
	static Logger kyclog = Logger.getLogger(AdviserDB.class);
	
	/**
	 * 
	 * @param dbDTO
	 * @param strAdviserId
	 * @return
	 * @throws Exception
	 */

	public List<AdviserStaff> getAdviserStaffDetails(DBIntf dbDTO,String strAdviserId)throws Exception{

		
		List<AdviserStaff> advStfList = new ArrayList<AdviserStaff>();


		if(KycUtils.checkNullVal(dbDTO)){
			ApplicationContext appCtx = ApplicationContextUtils.getApplicationContext();
			dbDTO = (DBImplementation) appCtx.getBean("DBBean");
		}

		advStfList =  (List<AdviserStaff>) dbDTO.searchListByQuery("from com.kyc.Dao.AdviserStaff advstf where advstf.advstfId='"+strAdviserId+"'");

		return advStfList;

		
	}
	
	@SuppressWarnings("unchecked")
	public AdviserStaff getAdviserByAdvId(String strAdvId) {

		ApplicationContext appCtx = ApplicationContextUtils
				.getApplicationContext();
		DBImplementation dbDTO = (DBImplementation) appCtx
				.getBean(KycConst.DB_BEAN);

		AdviserStaff advStfDets = new AdviserStaff();

		advStfDets = (AdviserStaff) dbDTO.searchList( "from  com.avallis.ekyc.dto.AdviserStaff cust   where cust.advstfId=? ", strAdvId).get(0);
		

		return advStfDets;
	}
	
	public List<AdviserStaff> populateParaplannerDets(String strLoggAdvId,String strDistributor) throws HibernateException{
		List empStatusList = new ArrayList();
		try{
//			WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(ServletActionContext.getServletContext());
			ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
			DBIntf dbDTO = (DBIntf) ctx.getBean("DBDAO");
			
			String
			HQL_PARAPLANNER_ADV = " SELECT  ADVSTF_ID,  ADVSTF_NAME,  EMAIL_ID"	+ " FROM ADVISER_STAFF ADVSTF,  MASTER_EMPLOYMENT_STATUS EMP "
			+ " WHERE EMP.EMPSTATUS_ID        = ADVSTF.EMPSTATUS_ID(+)"	+ " AND Upper(EMP.EMPSTATUSNAME)='ACTIVE' "
			+ " AND ADVSTF_ID IN ("	+ " SELECT WRKWITH_ADVISER_ID  FROM ADVSTF_INTRO_WRKWITH "+ " WHERE ";

			String strWhrCond =  "ADVSTF_ID  ='"+strLoggAdvId+"'AND DISTRIBUTOR_ID='"+strDistributor+"' ) ORDER BY UPPER(ADVSTF_NAME)";

			empStatusList = dbDTO.searchByNativeSQLQuery(HQL_PARAPLANNER_ADV + strWhrCond);

		}catch(HibernateException e){
			kyclog.error("----------> populateParaplannerDets " , e);
		}
		return empStatusList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AdviserStaff> getActiveAdvisers(String strloggedAdvStaffId,String strloggedUserStaffType,String strloggedUsrDistId) {

		ApplicationContext appCtx = ApplicationContextUtils
				.getApplicationContext();
		DBImplementation dbDTO = (DBImplementation) appCtx
				.getBean(KycConst.DB_BEAN);
		
		List<AdviserStaff> advstaffList = new ArrayList<AdviserStaff>();
		String STR_GLBL_ACCESS_LVL__COMP_QRY = "select advStf.ADVSTF_ID,advStf.ADVSTF_NAME from ADVISER_STAFF advStf ,MASTER_EMPLOYMENT_STATUS EMP "
				+ " where upper(advStf.STAFF_TYPE)='ADVISER'"
				+ " AND (advStf.DISTRIBUTOR_ID) ='"+strloggedUsrDistId+"' AND EMP.EMPSTATUS_ID = ADVSTF.EMPSTATUS_ID AND EMP.EMPSTATUSNAME = 'ACTIVE'"
						+ " AND INSTR(advStf.ADVSTF_NAME,'(R)')=0 "
						+ " UNION  SELECT advStf.ADVSTF_ID,advStf.ADVSTF_NAME FROM ADVSTF_DIST_WRKWITH workwith,"
						+ " ADVISER_STAFF advStf ,MASTER_EMPLOYMENT_STATUS EMP"
						+ " WHERE upper(advStf.STAFF_TYPE)='ADVISER' and workwith.advstf_id=advstf.advstf_id  AND workwith.WRKWITH_DIST_ID='"+strloggedUsrDistId+"'"
						+ " AND EMP.EMPSTATUS_ID = ADVSTF.EMPSTATUS_ID AND EMP.EMPSTATUSNAME = 'ACTIVE'  "
						+ " AND INSTR(advStf.ADVSTF_NAME,'(R)')=0  order by 2 ";
		
		        advstaffList =   (List<AdviserStaff>) dbDTO.searchByNativeSQLQuery(STR_GLBL_ACCESS_LVL__COMP_QRY);


		return advstaffList;
	}
	
	 public List getFNAAdvMgrDets(DBIntf dbDTO,String strFNAId) throws HibernateException{
			List empStatusList = new ArrayList();
			try{

				if(KycUtils.checkNullVal(dbDTO)){
//					WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(ServletActionContext.getServletContext());
					ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
					dbDTO = (DBIntf) ctx.getBean("DBDAO");
				}

				String
				HQL_FNA_COUNT = " from com.avallis.ekyc.dto.FnaDetails fna "
						+ " left join fetch fna.adviserStaffByMgrId mgr "
						+ " left join fetch fna.adviserStaffByAdvstfId adv "
						
						;
				
				if(!KycUtils.nullOrBlank(strFNAId)){
					String strWhrCls = " WHERE fna.fnaId='"+strFNAId+"'";
					String ostrOrderBy = " ORDER BY fna.fnaId";
					String Qry = strWhrCls + ostrOrderBy;
					 empStatusList = dbDTO.searchListByQuery(HQL_FNA_COUNT+Qry);
				}else {
					 empStatusList = dbDTO.searchListByQuery(HQL_FNA_COUNT);
				}
				

				
             

			}catch(HibernateException e){
				kyclog.error("----------> getFNAAdvMgrDets " , e);
			}
			return empStatusList;
		}
	 
	 
	 /**
		 * 
		 * @param strAdviserId
		 * @return
		 */
		public List getPrxyAdviserStaffDetails(String strAdviserId){
			List prxyAdvStfList = new ArrayList();
			try{

			 ApplicationContext appCtx = ApplicationContextUtils.getApplicationContext();
			 DBImplementation dbDTO = (DBImplementation) appCtx.getBean("DBBean");

			
			 prxyAdvStfList = dbDTO.searchByNativeSQLQuery("SELECT ADVSTF_ID,ADVSTF_NAME,EMAIL_ID from ADVISER_STAFF advstf where advstf.ADVSTF_ID IN "
			 		+ "( SELECT MANAGER_ID FROM ADVSTF_PROXY_MGR_DETS PRXMGR WHERE  ADVSTF_ID ='"+strAdviserId+"'"
			 				+ " AND TO_DATE(PRXMGR.START_DATE,'DD/MM/YYYY')<= TO_DATE(SYSDATE,'DD/MM/YYYY')"
			 				+ " AND TO_DATE(PRXMGR.END_DATE,'DD/MM/YYYY')  >=TO_DATE(SYSDATE,'DD/MM/YYYY')"
			 				+ ")");
			 
			 

			 

			}catch(Exception e){
//				e.printStackTrace();
				//throw new Exception(KycConst.WS_SUCCESSMESSAGE);
				kyclog.error("----------> getPrxyAdviserStaffDetails " , e);
			}
			return prxyAdvStfList;
		}
		
		
		 /**
		  * 
		  * @param strPrinId
		  * @param strDistrId
		  * @param strAgentId
		  * @return
		  */
		 
		 public List<AdvstfCodes> getAdvStfCodeDetsBasedOnAdvId(String strPrinId,String strDistrId,String strAgentId) {
				List<AdvstfCodes> advstfList = new ArrayList<AdvstfCodes>();
				try{

					String HQL_GET_ADVSTFDETAILS = " from com.kyc.Dao.AdvstfCodes advstfDet ";
					
					 ApplicationContext appCtx = ApplicationContextUtils.getApplicationContext();
					 DBImplementation dbDTO = (DBImplementation) appCtx.getBean("DBBean");

					String strWhrCls = "where advstfDet.adviserStaff='"+strAgentId+"' and advstfDet.masterPrincipal ='"+strPrinId+"' and"
							+ " advstfDet.fpmsDistributor='"+strDistrId+"'";
					advstfList = (List<AdvstfCodes>) dbDTO.searchListByQuery(HQL_GET_ADVSTFDETAILS +strWhrCls);

				}catch(HibernateException e){
					kyclog.info("----------> getAdvStfCodeDetsBasedOnAdvId " , e);
				}
				return advstfList;
		}

}
