package com.avallis.ekyc.db;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.avallis.ekyc.dbinterfaces.DBIntf;
import com.avallis.ekyc.dto.FnaDetails;
import com.avallis.ekyc.dto.FnaSignature;
import com.avallis.ekyc.dto.MasterAttachCateg;
import com.avallis.ekyc.utils.EKYCQuery;
import com.avallis.ekyc.utils.KycUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class SignatureDb {

	
	public void insertData(DBIntf dbDTO, FnaSignature sign) {

		Random ran=new Random();
		int no=ran.nextInt();
		String savId=String.valueOf(no);
		sign.setEsignId(savId.getBytes());
		
		String strFnaId  = sign.getFnaDetails().getFnaId();
		String strSignPerson = sign.getSignPerson();
		 dbDTO.updateDbByNativeSQLQuery("DELETE FROM FNA_ESIGNATURE WHERE FNA_ID = '"+strFnaId+"'  and SIGN_PERSON='"+strSignPerson+"'");

	    dbDTO.insertDB(sign);
		
	}
	
	
	public void clearSignature(DBIntf dbDTO, FnaSignature sign) {

		String strFnaId  = sign.getFnaDetails().getFnaId();
		String strSignPerson = sign.getSignPerson();
		 dbDTO.updateDbByNativeSQLQuery("DELETE FROM FNA_ESIGNATURE WHERE FNA_ID = '"+strFnaId+"'  and SIGN_PERSON='"+strSignPerson+"'");

		
	}
	
	public List<?> getAllDataById(DBIntf dbDTO,String fnaId) throws IOException {

	   String ekycQuery="SELECT * from FNA_ESIGNATURE WHERE FNA_ID='"+fnaId+"'";
	   List<?> signList= dbDTO.searchByNativeSQLQuery(ekycQuery);
	   for(Object fnaSign:signList) {
		   ObjectMapper mapper = new ObjectMapper();
		   FnaSignature custDets=mapper.readValue(fnaSign.toString(), FnaSignature.class);
//			System.out.println(custDets.getSignPerson());
	   }
	   return signList;
	}
	
	
	
	public void clearSignByFnaId(DBIntf dbDTO, String strFnaId) {

		
		 dbDTO.updateDbByNativeSQLQuery("UPDATE FNA_DETAILS SET "
		 			+ " SUPREV_MGR_FLG =null, SUPREV_FOLLOW_REASON = null,"
			 		+ " KYC_SENT_STATUS = null,MGR_APPROVE_STATUS = null, MGR_APPROVE_DATE = null,MGR_EMAIL_SENT_FLG=null,"
			 		+ " ADMIN_KYC_SENT_STATUS =  null, ADMIN_APPROVE_STATUS = null, ADMIN_APPROVE_DATE = null, "
			 		+ " COMP_KYC_SENT_STATUS = null, COMP_APPROVE_STATUS = null, COMP_APPROVE_DATE = null,  "
			 		+ " MGRAPPRSTS_BYADVSTF=null,ADMINAPPRSTS_BYADVSTF=null,COMPAPPRSTS_BYADVSTF=null"
			 		+ " WHERE FNA_ID = '"+strFnaId+"'");

		 
		 dbDTO.updateDbByNativeSQLQuery("DELETE FROM FNA_ESIGNATURE WHERE FNA_ID = '"+strFnaId+"'");
		 

		
	}
	
	
	public List getClientSignAllCol(DBIntf dbDTO,String strFNAId,String strSignFor,String strPageRef){
		 List spsList = new ArrayList();
		 
		 String qry="";
		 
		  
		 qry="SELECT ESIGN_ID,FNA_ID,SIGN_PERSON,ESIGN,SIGN_DOC_BLOB,TO_CHAR(SIGN_DATE,'dd/MM/yyyy')SIGN_DATE,PAGE_SEC_REF FROM FNA_ESIGNATURE WHERE FNA_ID='"+strFNAId+"'";
//		 qry="SELECT * FROM FNA_ESIGNATURE WHERE FNA_ID='"+strFNAId+"'";
		 
		 if(!KycUtils.nullOrBlank(strSignFor)){
			 if(!strSignFor.equalsIgnoreCase("ALL")){
				 qry += " and UPPER(SIGN_PERSON) = '"+strSignFor.toUpperCase()+"'";	 
			 }
		 }
		 
		 if(!KycUtils.nullOrBlank(strPageRef)){
			 if(!strPageRef.equalsIgnoreCase("ALL")){
				 qry += " and UPPER(PAGE_SEC_REF) = '"+strPageRef.toUpperCase()+"'";	 
			 }
		 }
		 
		 spsList=dbDTO.searchByNativeSQLQuery(qry);
		 
		 return spsList;
	}
	
	public List getClientSign(DBIntf dbDTO,String strFNAId,String strSignFor,String strPageRef){
		 List spsList = new ArrayList();
		 
		 String qry="";
		 
		 
		 
			
		  
		 qry="SELECT ESIGN_ID,SIGN_PERSON,ESIGN,SIGN_DATE,PAGE_SEC_REF,SIGN_DOC_BLOB FROM FNA_ESIGNATURE WHERE FNA_ID='"+strFNAId+"'";
		 //qry="SELECT * FROM FNA_ESIGNATURE WHERE FNA_ID='"+strFNAId+"'";
		 if(!KycUtils.nullOrBlank(strSignFor)){
			 if(!strSignFor.equalsIgnoreCase("ALL")){
				 qry += " and UPPER(SIGN_PERSON) = '"+strSignFor.toUpperCase()+"'";	 
			 }
		 }
		 
		 if(!KycUtils.nullOrBlank(strPageRef)){
			 if(!strPageRef.equalsIgnoreCase("ALL")){
				 qry += " and UPPER(PAGE_SEC_REF) = '"+strPageRef.toUpperCase()+"'";	 
			 }
		 }
		 
		 spsList=dbDTO.searchByNativeSQLQuery(qry);
		 
		 return spsList;
	 }
}
