package com.avallis.ekyc.utils;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.EnumMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.avallis.ekyc.controller.LoginController;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

public class QrCode {
	
	static Logger kyclog = Logger.getLogger(LoginController.class.getName());
	ResourceBundle appresource =ResourceBundle.getBundle("AppResource"); 
	
	public  byte[] getQRCodeImage(String text,String dataFormId, int width, int height) {
		Map<EncodeHintType, Object> hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
		hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
		hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
		hints.put(EncodeHintType.MARGIN,4); /* default = 4 */
		QRCodeWriter writer = new QRCodeWriter();
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] imgByte = null;
		try {
			
			BitMatrix bitMatrix = writer.encode(text, BarcodeFormat.QR_CODE,width,height, hints);
		    MatrixToImageConfig config = new MatrixToImageConfig(0xFF00B999,0xFFFFFFFF);
			
		    BufferedImage qrImage = MatrixToImageWriter.toBufferedImage(bitMatrix, config);
		    
		    Resource resource=new ClassPathResource(appresource.getString("logo.filepath"));
		    BufferedImage logoImage = ImageIO.read(resource.getFile());
		    int deltaHeight = qrImage.getHeight() - logoImage.getHeight();
		    int deltaWidth = qrImage.getWidth() - logoImage.getWidth();
		    BufferedImage combined = new BufferedImage(qrImage.getHeight(), qrImage.getWidth(), BufferedImage.TYPE_INT_ARGB);
		    
		    Graphics2D graphics = (Graphics2D) combined.getGraphics();
		    graphics.fillRect(20,20,combined.getWidth(),combined.getWidth());
		    graphics.drawImage(qrImage, 0, 0, null);
		    graphics.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));
		    
		    Font font = new Font("Times New Roman", Font.PLAIN, 12);
		    graphics.setFont(font);
		    graphics.setColor(Color.BLACK);
		    
		    graphics.drawString(dataFormId, 45, 200);
		    graphics.drawImage(logoImage, (int) Math.round(deltaWidth / 2), (int) Math.round(deltaHeight / 2), null);
		   
		    ImageIO.write(combined, "png",baos);
//		    imgByte = Base64.getEncoder().encode(baos.toByteArray());
		    imgByte = Base64.encodeBase64(baos.toByteArray());
		}
		catch(Exception e) {
//			e.printStackTrace();
			kyclog.error("error in getQRCodeImage-->",e);
			
		}
		return imgByte;
		

	}
	
}
