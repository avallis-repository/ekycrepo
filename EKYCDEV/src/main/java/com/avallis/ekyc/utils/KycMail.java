package com.avallis.ekyc.utils;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger; 

public class KycMail {
	static Logger kyclog = Logger.getLogger(KycMail.class.getName());
	static ResourceBundle bundle = ResourceBundle.getBundle("LabelsBundle", Locale.ENGLISH);

	public KycMail() {
	}
	
	public static String sendMail(String frmReceipent, String[] toReceipent, String[] toReceipentCC,
            String msgContent,String Subj) throws UnsupportedEncodingException {
				String ackMsg = "";
				kyclog.info("Message in " + Locale.ENGLISH + ": " + 
				bundle.getString("hostId"));
				
				String hostId = bundle.getString("hostId");
				String port = bundle.getString("port");
				final String userId = bundle.getString("smtpUser");
				final String pwd = bundle.getString("smtpPwd");
				String frmAddr = bundle.getString("fromAddr");
				String frmAddrDisplay=bundle.getString("fromAddrDisplayName");
				 
				Properties props = new Properties();
				props.put("mail.smtp.auth", true);
				props.put("mail.smtp.starttls.enable", "true");
				props.put("mail.smtp.host",hostId);
				props.put("mail.smtp.port", port);
				Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
				        return new PasswordAuthentication(userId,pwd);
				}
				});
				
				try {
				
				session.setDebug(false);
				Message msg = new MimeMessage(session);
				InternetAddress addressFrom = new InternetAddress(frmAddr,frmAddrDisplay);
				msg.setFrom(addressFrom);
				StringBuffer strBuffEmail = new StringBuffer();
				for (int i = 0; i < toReceipent.length; i++) {
				kyclog.info(" Mail To Addr "+toReceipent[i]);
				strBuffEmail.append(toReceipent[i]);
				}
				String strEEmail = strBuffEmail.toString();
				StringTokenizer stTok = new StringTokenizer(strEEmail, ";");
				
				int iTok = stTok.countTokens();
				
				InternetAddress[] toAddr = new InternetAddress[iTok];
				for (int i = 0; i < iTok; i++) {
				String strElem = (String)stTok.nextToken();
				toAddr[i] = new InternetAddress(strElem);
				}
				StringBuffer strBuffEmailCC = new StringBuffer();
				for (int i = 0; i < toReceipentCC.length; i++) {
				kyclog.info(" MailCC To Addr "+toReceipentCC[i]);
				strBuffEmailCC.append(toReceipentCC[i]);
				}
				String strEEmailCC = strBuffEmailCC.toString();
				StringTokenizer stTokCC = new StringTokenizer(strEEmailCC, ";");
				
				int iTokCC = stTokCC.countTokens();
				//
				InternetAddress[] toAddrCC = new InternetAddress[iTokCC];
				for (int i = 0; i < iTokCC; i++) {
				String strElemCC = (String)stTokCC.nextToken();
				toAddrCC[i] = new InternetAddress(strElemCC);
				}
				kyclog.info(" to fpms addr "+toAddr);
				msg.setRecipients(Message.RecipientType.TO, toAddr);
				msg.setRecipients(Message.RecipientType.CC, toAddrCC);
				kyclog.info(" to fpms addr cc "+toAddrCC);
				msg.setSubject(Subj);
				msg.setSentDate(new Date());
				msg.setContent(msgContent, "text/html");
				kyclog.info("Sending Mail");
				Transport.send(msg);
				kyclog.info("Sent Mail");
				ackMsg = "SUCCESS";
				} catch (MessagingException mex) {
				mex.printStackTrace();
				kyclog.debug(mex, mex.getCause());
				ackMsg = "FAILURE";
				}
				return ackMsg;
		}

public static String sendMailWithAttach(  String[] toReceipent,String[] toReceipentCC, String msgContent, String Subj,String strAttachFileName,String strDispAttachName) {
		
		String ackMsg = "";
		
		//System.out.println("mailwithattachment file i==============>"+strAttachFileName);
		
		
		

		String hostId = bundle.getString("hostId");
		String port = bundle.getString("port");
		final String userId = bundle.getString("smtpUser");
		final String pwd = bundle.getString("smtpPwd");
		String frmAddr = bundle.getString("fromAddr");
		String frmAddrDisplay=bundle.getString("fromAddrDisplayName");
		Properties props = new Properties();
		props.put("mail.smtp.auth", true);
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", hostId);
		props.put("mail.smtp.port", port);
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(userId, pwd);
					}
				});
		
		try {

			session.setDebug(false);
			Message msg = new MimeMessage(session);
			
			
			msg.setFrom(new InternetAddress(frmAddr,frmAddrDisplay));
			
			StringBuffer strBuffEmail = new StringBuffer();
			
			
			for (int i = 0; i < toReceipent.length; i++){
				
				strBuffEmail.append(toReceipent[i]).append(";");
			}
			
			
			
			StringTokenizer stTok = new StringTokenizer(strBuffEmail.toString(), ";");
			
			
			int iTok = stTok.countTokens();
			InternetAddress[] toAddr = new InternetAddress[iTok];
			
			for (int i = 0; i < iTok; i++) toAddr[i] = new InternetAddress((String) stTok.nextToken());
			
			StringBuffer strBuffEmailCC = new StringBuffer();
			for (int i = 0; i < toReceipentCC.length; i++)strBuffEmailCC.append(toReceipentCC[i]).append(";");
			
			
			StringTokenizer stTokCC = new StringTokenizer(strBuffEmailCC.toString(), ";");
			int iTokCC = stTokCC.countTokens();
			InternetAddress[] toAddrCC = new InternetAddress[iTokCC];
			
			for (int i = 0; i < iTokCC; i++)toAddrCC[i] = new InternetAddress((String) stTokCC.nextToken());
			 
			//String fileName = strAttachFileName.substring(strAttachFileName.lastIndexOf("/")+1);
			 
			
			msg.setRecipients(Message.RecipientType.TO, toAddr);
			msg.setRecipients(Message.RecipientType.CC, toAddrCC);  
			msg.setSubject(Subj);
			msg.setSentDate(new Date());	
			
			
			
			MimeBodyPart bodypart = new MimeBodyPart();
			bodypart.setContent(msgContent,"text/html");
			
			
			Multipart multipart = new MimeMultipart(); 
			multipart.addBodyPart(bodypart);
			
			bodypart=new MimeBodyPart();
			DataSource ds = new FileDataSource(strAttachFileName);
			bodypart.setDataHandler(new DataHandler(ds));			
			bodypart.setFileName(strDispAttachName);
			multipart.addBodyPart(bodypart);
			
			msg.setContent(multipart);
			
			//msg.setContent(msgContent, "text/plain");
			
			kyclog.info("KYC Mail Sending....");
			
			Transport.send(msg);
			
			//System.out.println("KYC Mail Sent Successfully!!!!");
			kyclog.info("KYC Mail Sent Successfully!!!!");
			
			ackMsg = "SUCCESS";
			
		} catch (Exception mex) {
			
			mex.printStackTrace();
			kyclog.error(mex);
			kyclog.debug(mex, mex.getCause());
			ackMsg = "FAILURE";
		}
		return ackMsg;
	}

public static String sendMailWithAttachAsFile(  String[] toReceipent,String[] toReceipentCC,String[] toReceipentBCC, String msgContent, String Subj,ArrayList<File> Files) {
	
	String ackMsg = "";
	
	//System.out.println("mailwithattachment file i==============>"+strAttachFileName);
	
	
	

	String hostId = bundle.getString("hostId");
	String port = bundle.getString("port");
	final String userId = bundle.getString("smtpUser");
	final String pwd = bundle.getString("smtpPwd");
	String frmAddr = bundle.getString("fromAddr"); 
	String frmAddrDisplay=bundle.getString("fromAddrDisplayName");
	Properties props = new Properties();
	props.put("mail.smtp.auth", true);
	props.put("mail.smtp.starttls.enable", "true");
	props.put("mail.smtp.host",hostId);
	props.put("mail.smtp.port", port);
	Session session = Session.getInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(userId, pwd);
				}
			});
	
	try {

		session.setDebug(false);
		Message msg = new MimeMessage(session);
		
		
		msg.setFrom(new InternetAddress(frmAddr,frmAddrDisplay));
		
		StringBuffer strBuffEmail = new StringBuffer();
		
		
		for (int i = 0; i < toReceipent.length; i++){
			
			strBuffEmail.append(toReceipent[i]).append(";");
		}
		
		
		
		StringTokenizer stTok = new StringTokenizer(strBuffEmail.toString(), ";");
		
		
		int iTok = stTok.countTokens();
		InternetAddress[] toAddr = new InternetAddress[iTok];
		
		for (int i = 0; i < iTok; i++) toAddr[i] = new InternetAddress((String) stTok.nextToken());
		
		StringBuffer strBuffEmailCC = new StringBuffer();
		for (int i = 0; i < toReceipentCC.length; i++)strBuffEmailCC.append(toReceipentCC[i]).append(";");
		
		
		StringTokenizer stTokCC = new StringTokenizer(strBuffEmailCC.toString(), ";");
		int iTokCC = stTokCC.countTokens();
		InternetAddress[] toAddrCC = new InternetAddress[iTokCC];
		
		for (int i = 0; i < iTokCC; i++)toAddrCC[i] = new InternetAddress((String) stTokCC.nextToken());
		 
		//String fileName = strAttachFileName.substring(strAttachFileName.lastIndexOf("/")+1);
		
		StringBuffer strBuffEmailBCC = new StringBuffer();
		for (int i = 0; i < toReceipentBCC.length; i++)strBuffEmailBCC.append(toReceipentBCC[i]).append(";");
		StringTokenizer stTokBCC = new StringTokenizer(strBuffEmailBCC.toString(), ";");
		int iTokBCC = stTokBCC.countTokens();
		InternetAddress[] toAddrBCC = new InternetAddress[iTokBCC];		
		for (int i = 0; i < iTokBCC; i++)toAddrBCC[i] = new InternetAddress((String) stTokBCC.nextToken());
		 
		
		msg.setRecipients(Message.RecipientType.TO, toAddr);
		msg.setRecipients(Message.RecipientType.CC, toAddrCC);
		msg.setRecipients(Message.RecipientType.BCC, toAddrBCC);
		msg.setSubject(Subj);
		msg.setSentDate(new Date());	
		
		
		
		MimeBodyPart bodypart = new MimeBodyPart();
		bodypart.setContent(msgContent,"text/html");
		
		
		Multipart multipart = new MimeMultipart(); 

		multipart.addBodyPart(bodypart);
		
		if(!KycUtils.nullObj(Files) && Files.size()>0){
			for(File file:Files){
				bodypart=new MimeBodyPart();
				attachFile(file,multipart,bodypart);
				}
		}
		/*DataSource ds = new FileDataSource(strAttachFileName);
		bodypart.setDataHandler(new DataHandler(ds));			
		bodypart.setFileName(strDispAttachName);
		multipart.addBodyPart(bodypart);*/
		
		msg.setContent(multipart);
		
		//msg.setContent(msgContent, "text/plain");
		
		kyclog.info("KYC Mail Sending....");
		
		Transport.send(msg);
		
		//System.out.println("KYC Mail Sent Successfully!!!!");
		kyclog.info("KYC Mail Sent Successfully!!!!");
		
		ackMsg = "SUCCESS";
		
	} catch (Exception mex) {
		
		mex.printStackTrace();
		kyclog.error(mex);
		kyclog.debug(mex, mex.getCause());
		ackMsg = "FAILURE";
	}
	return ackMsg;
}

public static void attachFile(File file, Multipart multipart, MimeBodyPart messageBodyPart) throws MessagingException {
    DataSource source = new FileDataSource(file);
    messageBodyPart.setDataHandler(new DataHandler(source));
    messageBodyPart.setFileName(file.getName());
    multipart.addBodyPart(messageBodyPart);    
}

}
