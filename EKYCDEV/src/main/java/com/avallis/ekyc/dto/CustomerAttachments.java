package com.avallis.ekyc.dto;
// Generated Dec 23, 2014 3:18:02 AM by Hibernate Tools 3.2.0.CR1


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnoreType;

/**
 * CustomerAttachments generated by hbm2java
 */
@JsonIgnoreType
public class CustomerAttachments  implements java.io.Serializable {


     private String txtFldCustAttachId;
     private FpmsDistributor selCustAttachFpmsDistributor;
     private CustomerDetails selCustAttachCustomerDetails;
     private MasterAttachCateg selCustAttachMasterAttachCateg;
     private String selCustAttachTitle;
     private String txtFldCustAttachPageNum;
     private String txtFldCustAttachRemarks;
     private String txtFldCustAttachFilename;
     private String txtFldCustAttachFilesize;
     private String txtFldCustAttachFiletype;
     private String txtFldCustAttachAttachCategName;
     private String txtFldCustAttachCreatedBy;
     private Date   txtFldCustAttachCreatedDate;
     private String txtFldCustAttachModifiedBy;
     private Date   txtFldCustAttachModifiedDate;
     private String txtFldCustAttachDistributorName;
     private String txtFldCustAttachNtucPolId;
     //private Set fnaPolicyEmailHistDetses_set = new HashSet(0);
     private String insurarName;

    public CustomerAttachments() {
    }

	
    public CustomerAttachments(String txtFldCustAttachId, FpmsDistributor selCustAttachFpmsDistributor, CustomerDetails selCustAttachCustomerDetails, String selCustAttachTitle, String txtFldCustAttachCreatedBy, Date txtFldCustAttachCreatedDate,String insurarName) {
        this.txtFldCustAttachId = txtFldCustAttachId;
        this.selCustAttachFpmsDistributor = selCustAttachFpmsDistributor;
        this.selCustAttachCustomerDetails = selCustAttachCustomerDetails;
        this.selCustAttachTitle = selCustAttachTitle;
        this.txtFldCustAttachCreatedBy = txtFldCustAttachCreatedBy;
        this.txtFldCustAttachCreatedDate = txtFldCustAttachCreatedDate;
        this.insurarName = insurarName;
    }
    public CustomerAttachments(String txtFldCustAttachId, FpmsDistributor selCustAttachFpmsDistributor, CustomerDetails selCustAttachCustomerDetails, MasterAttachCateg selCustAttachMasterAttachCateg, String selCustAttachTitle, String txtFldCustAttachPageNum, String txtFldCustAttachRemarks, String txtFldCustAttachFilename, String txtFldCustAttachFilesize, String txtFldCustAttachFiletype, String txtFldCustAttachAttachCategName, String txtFldCustAttachCreatedBy, Date txtFldCustAttachCreatedDate, String txtFldCustAttachModifiedBy, Date txtFldCustAttachModifiedDate,
    		String txtFldCustAttachDistributorName, String txtFldCustAttachNtucPolId, Set fnaPolicyEmailHistDetses_set, String insurarName) {
       this.txtFldCustAttachId = txtFldCustAttachId;
       this.selCustAttachFpmsDistributor = selCustAttachFpmsDistributor;
       this.selCustAttachCustomerDetails = selCustAttachCustomerDetails;
       this.selCustAttachMasterAttachCateg = selCustAttachMasterAttachCateg;
       this.selCustAttachTitle = selCustAttachTitle;
       this.txtFldCustAttachPageNum = txtFldCustAttachPageNum;
       this.txtFldCustAttachRemarks = txtFldCustAttachRemarks;
       this.txtFldCustAttachFilename = txtFldCustAttachFilename;
       this.txtFldCustAttachFilesize = txtFldCustAttachFilesize;
       this.txtFldCustAttachFiletype = txtFldCustAttachFiletype;
       this.txtFldCustAttachAttachCategName = txtFldCustAttachAttachCategName;
       this.txtFldCustAttachCreatedBy = txtFldCustAttachCreatedBy;
       this.txtFldCustAttachCreatedDate = txtFldCustAttachCreatedDate;
       this.txtFldCustAttachModifiedBy = txtFldCustAttachModifiedBy;
       this.txtFldCustAttachModifiedDate = txtFldCustAttachModifiedDate;
       this.txtFldCustAttachDistributorName = txtFldCustAttachDistributorName;
       this.txtFldCustAttachNtucPolId = txtFldCustAttachNtucPolId;
       //this.fnaPolicyEmailHistDetses_set = fnaPolicyEmailHistDetses_set;
       this.insurarName = insurarName;
    }
   
    public String getTxtFldCustAttachId() {
        return this.txtFldCustAttachId;
    }
    
    public void setTxtFldCustAttachId(String txtFldCustAttachId) {
        this.txtFldCustAttachId = txtFldCustAttachId;
    }
    public FpmsDistributor getSelCustAttachFpmsDistributor() {
        return this.selCustAttachFpmsDistributor;
    }
    
    public void setSelCustAttachFpmsDistributor(FpmsDistributor selCustAttachFpmsDistributor) {
        this.selCustAttachFpmsDistributor = selCustAttachFpmsDistributor;
    }
    public CustomerDetails getSelCustAttachCustomerDetails() {
        return this.selCustAttachCustomerDetails;
    }
    
    public void setSelCustAttachCustomerDetails(CustomerDetails selCustAttachCustomerDetails) {
        this.selCustAttachCustomerDetails = selCustAttachCustomerDetails;
    }
    public MasterAttachCateg getSelCustAttachMasterAttachCateg() {
        return this.selCustAttachMasterAttachCateg;
    }
    
    public void setSelCustAttachMasterAttachCateg(MasterAttachCateg selCustAttachMasterAttachCateg) {
        this.selCustAttachMasterAttachCateg = selCustAttachMasterAttachCateg;
    }
    public String getSelCustAttachTitle() {
        return this.selCustAttachTitle;
    }
    
    public void setSelCustAttachTitle(String selCustAttachTitle) {
        this.selCustAttachTitle = selCustAttachTitle;
    }
    public String getTxtFldCustAttachPageNum() {
        return this.txtFldCustAttachPageNum;
    }
    
    public void setTxtFldCustAttachPageNum(String txtFldCustAttachPageNum) {
        this.txtFldCustAttachPageNum = txtFldCustAttachPageNum;
    }
    public String getTxtFldCustAttachRemarks() {
        return this.txtFldCustAttachRemarks;
    }
    
    public void setTxtFldCustAttachRemarks(String txtFldCustAttachRemarks) {
        this.txtFldCustAttachRemarks = txtFldCustAttachRemarks;
    }
    public String getTxtFldCustAttachFilename() {
        return this.txtFldCustAttachFilename;
    }
    
    public void setTxtFldCustAttachFilename(String txtFldCustAttachFilename) {
        this.txtFldCustAttachFilename = txtFldCustAttachFilename;
    }
    public String getTxtFldCustAttachFilesize() {
        return this.txtFldCustAttachFilesize;
    }
    
    public void setTxtFldCustAttachFilesize(String txtFldCustAttachFilesize) {
        this.txtFldCustAttachFilesize = txtFldCustAttachFilesize;
    }
    public String getTxtFldCustAttachFiletype() {
        return this.txtFldCustAttachFiletype;
    }
    
    public void setTxtFldCustAttachFiletype(String txtFldCustAttachFiletype) {
        this.txtFldCustAttachFiletype = txtFldCustAttachFiletype;
    }
    public String getTxtFldCustAttachAttachCategName() {
        return this.txtFldCustAttachAttachCategName;
    }
    
    public void setTxtFldCustAttachAttachCategName(String txtFldCustAttachAttachCategName) {
        this.txtFldCustAttachAttachCategName = txtFldCustAttachAttachCategName;
    }
    public String getTxtFldCustAttachCreatedBy() {
        return this.txtFldCustAttachCreatedBy;
    }
    
    public void setTxtFldCustAttachCreatedBy(String txtFldCustAttachCreatedBy) {
        this.txtFldCustAttachCreatedBy = txtFldCustAttachCreatedBy;
    }
    public Date getTxtFldCustAttachCreatedDate() {
        return this.txtFldCustAttachCreatedDate;
    }
    
    public void setTxtFldCustAttachCreatedDate(Date txtFldCustAttachCreatedDate) {
        this.txtFldCustAttachCreatedDate = txtFldCustAttachCreatedDate;
    }
    public String getTxtFldCustAttachModifiedBy() {
        return this.txtFldCustAttachModifiedBy;
    }
    
    public void setTxtFldCustAttachModifiedBy(String txtFldCustAttachModifiedBy) {
        this.txtFldCustAttachModifiedBy = txtFldCustAttachModifiedBy;
    }
    public Date getTxtFldCustAttachModifiedDate() {
        return this.txtFldCustAttachModifiedDate;
    }
    
    public void setTxtFldCustAttachModifiedDate(Date txtFldCustAttachModifiedDate) {
        this.txtFldCustAttachModifiedDate = txtFldCustAttachModifiedDate;
    }
    public String getTxtFldCustAttachDistributorName() {
        return this.txtFldCustAttachDistributorName;
    }
    
    public void setTxtFldCustAttachDistributorName(String txtFldCustAttachDistributorName) {
        this.txtFldCustAttachDistributorName = txtFldCustAttachDistributorName;
    }
    public String getTxtFldCustAttachNtucPolId() {
        return this.txtFldCustAttachNtucPolId;
    }
    
    public void setTxtFldCustAttachNtucPolId(String txtFldCustAttachNtucPolId) {
        this.txtFldCustAttachNtucPolId = txtFldCustAttachNtucPolId;
    }
    
  //getter and setter method for insurar field
    public String getinsurarName() {
  		return this.insurarName;
  	}

   public void setinsurarName(String insurarName) {
  		this.insurarName = insurarName;
  	}



}


