package com.avallis.ekyc.dto;

// Generated Mar 13, 2018 9:17:21 PM by Hibernate Tools 4.0.0

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreType;

/**
 * FnaContinDepnDets generated by hbm2java
 */
@JsonIgnoreType
public class FnaContinDepnDets implements java.io.Serializable {

	private String cdepId;
	private FnaDetails fnaDetails;
	private String cdepName;
	private String cdepAge;
	private Short cdepYrs;
	private String cdepLivingneeds;
	private String cdepRelationship;
	private Short cdepYrsteritary;
	private Short cdepEdnyrs;
	private Double cdepEdnyrsannlcost;
	private Double cdepEdnfund;
	private String cdepRb;
	private String cdepWard;
	private String cdepHosptype;
	private String createdBy;
	private Date createdDate;

	public FnaContinDepnDets() {
	}

	public FnaContinDepnDets(String cdepId) {
		this.cdepId = cdepId;
	}

	public FnaContinDepnDets(String cdepId, FnaDetails fnaDetails,
			String cdepName, String cdepAge, Short cdepYrs,
			String cdepLivingneeds, String cdepRelationship,
			Short cdepYrsteritary, Short cdepEdnyrs,
			Double cdepEdnyrsannlcost, Double cdepEdnfund,
			String cdepRb, String cdepWard, String cdepHosptype,
			String createdBy, Date createdDate) {
		this.cdepId = cdepId;
		this.fnaDetails = fnaDetails;
		this.cdepName = cdepName;
		this.cdepAge = cdepAge;
		this.cdepYrs = cdepYrs;
		this.cdepLivingneeds = cdepLivingneeds;
		this.cdepRelationship = cdepRelationship;
		this.cdepYrsteritary = cdepYrsteritary;
		this.cdepEdnyrs = cdepEdnyrs;
		this.cdepEdnyrsannlcost = cdepEdnyrsannlcost;
		this.cdepEdnfund = cdepEdnfund;
		this.cdepRb = cdepRb;
		this.cdepWard = cdepWard;
		this.cdepHosptype = cdepHosptype;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
	}

	public String getCdepId() {
		return this.cdepId;
	}

	public void setCdepId(String cdepId) {
		this.cdepId = cdepId;
	}

	public FnaDetails getFnaDetails() {
		return this.fnaDetails;
	}

	public void setFnaDetails(FnaDetails fnaDetails) {
		this.fnaDetails = fnaDetails;
	}

	public String getCdepName() {
		return this.cdepName;
	}

	public void setCdepName(String cdepName) {
		this.cdepName = cdepName;
	}

	public String getCdepAge() {
		return this.cdepAge;
	}

	public void setCdepAge(String cdepAge) {
		this.cdepAge = cdepAge;
	}

	public Short getCdepYrs() {
		return this.cdepYrs;
	}

	public void setCdepYrs(Short cdepYrs) {
		this.cdepYrs = cdepYrs;
	}

	public String getCdepLivingneeds() {
		return this.cdepLivingneeds;
	}

	public void setCdepLivingneeds(String cdepLivingneeds) {
		this.cdepLivingneeds = cdepLivingneeds;
	}

	public String getCdepRelationship() {
		return this.cdepRelationship;
	}

	public void setCdepRelationship(String cdepRelationship) {
		this.cdepRelationship = cdepRelationship;
	}

	public Short getCdepYrsteritary() {
		return this.cdepYrsteritary;
	}

	public void setCdepYrsteritary(Short cdepYrsteritary) {
		this.cdepYrsteritary = cdepYrsteritary;
	}

	public Short getCdepEdnyrs() {
		return this.cdepEdnyrs;
	}

	public void setCdepEdnyrs(Short cdepEdnyrs) {
		this.cdepEdnyrs = cdepEdnyrs;
	}

	public Double getCdepEdnyrsannlcost() {
		return this.cdepEdnyrsannlcost;
	}

	public void setCdepEdnyrsannlcost(Double cdepEdnyrsannlcost) {
		this.cdepEdnyrsannlcost = cdepEdnyrsannlcost;
	}

	public Double getCdepEdnfund() {
		return this.cdepEdnfund;
	}

	public void setCdepEdnfund(Double cdepEdnfund) {
		this.cdepEdnfund = cdepEdnfund;
	}

	public String getCdepRb() {
		return this.cdepRb;
	}

	public void setCdepRb(String cdepRb) {
		this.cdepRb = cdepRb;
	}

	public String getCdepWard() {
		return this.cdepWard;
	}

	public void setCdepWard(String cdepWard) {
		this.cdepWard = cdepWard;
	}

	public String getCdepHosptype() {
		return this.cdepHosptype;
	}

	public void setCdepHosptype(String cdepHosptype) {
		this.cdepHosptype = cdepHosptype;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

}
