package com.avallis.ekyc.dto;

import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/** @author Hibernate CodeGenerator */
public class FpisFundCategoryPK implements Serializable {

    /** identifier field */
    private String categoryCode;

    /** identifier field */
    private String status;

    /** full constructor */
    public FpisFundCategoryPK(String categoryCode, String status) {
        this.categoryCode = categoryCode;
        this.status = status;
    }

    /** default constructor */
    public FpisFundCategoryPK() {
    }

    public String getCategoryCode() {
        return this.categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("categoryCode", getCategoryCode())
            .append("status", getStatus())
            .toString();
    }

    public boolean equals(Object other) {
        if ( (this == other ) ) return true;
        if ( !(other instanceof FpisFundCategoryPK) ) return false;
        FpisFundCategoryPK castOther = (FpisFundCategoryPK) other;
        return new EqualsBuilder()
            .append(this.getCategoryCode(), castOther.getCategoryCode())
            .append(this.getStatus(), castOther.getStatus())
            .isEquals();
    }

    public int hashCode() {
        return new HashCodeBuilder()
            .append(getCategoryCode())
            .append(getStatus())
            .toHashCode();
    }

}
