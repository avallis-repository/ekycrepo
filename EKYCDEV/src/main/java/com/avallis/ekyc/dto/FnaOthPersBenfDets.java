package com.avallis.ekyc.dto;

public class FnaOthPersBenfDets {
	
	private String txtFldCateg="BENF";	
	private String txtFldCDBenfId;
	private String txtFldCDBenfName;	
	private String txtFldCDBenfNric;
	private String txtFldCDBenfIncNo;
	private String txtFldCDBenfAddr;
	private String radFldCDBenfJob;
	private String txtFldCDBenfJobCont;
	private String txtFldCDBenfRel;
	
	
	public String getTxtFldCateg() {
		return txtFldCateg;
	}
	public void setTxtFldCateg(String txtFldCateg) {
		this.txtFldCateg = txtFldCateg;
	}
	public String getTxtFldCDBenfId() {
		return txtFldCDBenfId;
	}
	public void setTxtFldCDBenfId(String txtFldCDBenfId) {
		this.txtFldCDBenfId = txtFldCDBenfId;
	}
	public String getTxtFldCDBenfName() {
		return txtFldCDBenfName;
	}
	public void setTxtFldCDBenfName(String txtFldCDBenfName) {
		this.txtFldCDBenfName = txtFldCDBenfName;
	}
	public String getTxtFldCDBenfNric() {
		return txtFldCDBenfNric;
	}
	public void setTxtFldCDBenfNric(String txtFldCDBenfNric) {
		this.txtFldCDBenfNric = txtFldCDBenfNric;
	}
	public String getTxtFldCDBenfIncNo() {
		return txtFldCDBenfIncNo;
	}
	public void setTxtFldCDBenfIncNo(String txtFldCDBenfIncNo) {
		this.txtFldCDBenfIncNo = txtFldCDBenfIncNo;
	}
	public String getTxtFldCDBenfAddr() {
		return txtFldCDBenfAddr;
	}
	public void setTxtFldCDBenfAddr(String txtFldCDBenfAddr) {
		this.txtFldCDBenfAddr = txtFldCDBenfAddr;
	}
	public String getRadFldCDBenfJob() {
		return radFldCDBenfJob;
	}
	public void setRadFldCDBenfJob(String radFldCDBenfJob) {
		this.radFldCDBenfJob = radFldCDBenfJob;
	}
	public String getTxtFldCDBenfJobCont() {
		return txtFldCDBenfJobCont;
	}
	public void setTxtFldCDBenfJobCont(String txtFldCDBenfJobCont) {
		this.txtFldCDBenfJobCont = txtFldCDBenfJobCont;
	}
	public String getTxtFldCDBenfRel() {
		return txtFldCDBenfRel;
	}
	public void setTxtFldCDBenfRel(String txtFldCDBenfRel) {
		this.txtFldCDBenfRel = txtFldCDBenfRel;
	}
	
	
	
	

}
