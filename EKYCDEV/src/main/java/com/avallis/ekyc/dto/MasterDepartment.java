package com.avallis.ekyc.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonIgnoreType;


/** @author Hibernate CodeGenerator */
@JsonIgnoreType
public class MasterDepartment implements Serializable {

    /** identifier field */
    private String departmentId;

    /** nullable persistent field */
    private String departmentName;

    /** persistent field */
    private Date createdDate;

    /** nullable persistent field */
    private Date modifiedDate;

    /** nullable persistent field */
    private String modifiedBy;

    /** persistent field */
    private String createdBy;

    /** nullable persistent field */
    private String departmentDesc;

    /** persistent field */
    private Set customerKeypersons;

    /** full constructor */
    public MasterDepartment(String departmentId, String departmentName, Date createdDate, Date modifiedDate, String modifiedBy, String createdBy, String departmentDesc, Set customerKeypersons) {
        this.departmentId = departmentId;
        this.departmentName = departmentName;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
        this.modifiedBy = modifiedBy;
        this.createdBy = createdBy;
        this.departmentDesc = departmentDesc;
        this.customerKeypersons = customerKeypersons;
    }

    /** default constructor */
    public MasterDepartment() {
    }

    /** minimal constructor */
    public MasterDepartment(String departmentId, Date createdDate, String createdBy, Set customerKeypersons) {
        this.departmentId = departmentId;
        this.createdDate = createdDate;
        this.createdBy = createdBy;
        this.customerKeypersons = customerKeypersons;
    }

    public String getDepartmentId() {
        return this.departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartmentName() {
        return this.departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return this.modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return this.modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getDepartmentDesc() {
        return this.departmentDesc;
    }

    public void setDepartmentDesc(String departmentDesc) {
        this.departmentDesc = departmentDesc;
    }

    public Set getCustomerKeypersons() {
        return this.customerKeypersons;
    }

    public void setCustomerKeypersons(Set customerKeypersons) {
        this.customerKeypersons = customerKeypersons;
    }

    public String toString() {
        return new ToStringBuilder(this)
            .append("departmentId", getDepartmentId())
            .toString();
    }

}
