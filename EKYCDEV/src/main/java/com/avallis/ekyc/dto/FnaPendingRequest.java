package com.avallis.ekyc.dto;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

public class FnaPendingRequest implements java.io.Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private String txtFldPendReqId;
   
    private FnaDetails fnaDetails;
    private String txtFldPendMsg;
    private String txtFldreqBy;
    private String txtFldCrtdBy;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy",timezone = "Asia/Singapore")
    private Date   txtFldCrtdDate;
    
   /* public FnaPendingRequest() {
    }
    
    public FnaPendingRequest(String txtFldPendReqId, FnaDetails fnaDetails, String txtFldPendMsg,String txtFldreqBy, String txtFldCrtdBy, Date txtFldCrtdDate) {
        this.txtFldPendReqId = txtFldPendReqId;
        this.fnaDetails = fnaDetails;
        this.txtFldPendMsg = txtFldPendMsg;
        this.txtFldreqBy = txtFldreqBy;
        this.txtFldCrtdBy = txtFldCrtdBy;
        this.txtFldCrtdDate = txtFldCrtdDate;
      
    }
*/
    public String getTxtFldPendReqId() {
        return txtFldPendReqId;
    }

    public void setTxtFldPendReqId(String txtFldPendReqId) {
        this.txtFldPendReqId = txtFldPendReqId;
    }

    public FnaDetails getFnaDetails() {
        return fnaDetails;
    }

    public void setFnaDetails(FnaDetails fnaDetails) {
        this.fnaDetails = fnaDetails;
    }

    public String getTxtFldPendMsg() {
        return txtFldPendMsg;
    }

    public void setTxtFldPendMsg(String txtFldPendMsg) {
        this.txtFldPendMsg = txtFldPendMsg;
    }

    public String getTxtFldreqBy() {
        return txtFldreqBy;
    }

    public void setTxtFldreqBy(String txtFldreqBy) {
        this.txtFldreqBy = txtFldreqBy;
    }

    public String getTxtFldCrtdBy() {
        return txtFldCrtdBy;
    }

    public void setTxtFldCrtdBy(String txtFldCrtdBy) {
        this.txtFldCrtdBy = txtFldCrtdBy;
    }

    public Date getTxtFldCrtdDate() {
        return txtFldCrtdDate;
    }

    public void setTxtFldCrtdDate(Date txtFldCrtdDate) {
        this.txtFldCrtdDate = txtFldCrtdDate;
    }
}
