package com.avallis.ekyc.dto;

public class FnaOthPersPep {

	private String txtFldCateg="PEP";
	private String txtFldCDPepId;
	private String txtFldCDPepName;
	private String txtFldCDPepNric;
	private String txtFldCDPepIncNo;
	private String txtFldCDPepAddr;
	private String radFldCDPepJob;
	private String txtFldCDPepJobCont;
	private String txtFldCDPepRel;
	
	
	public String getTxtFldCateg() {
		return txtFldCateg;
	}
	public void setTxtFldCateg(String txtFldCateg) {
		this.txtFldCateg = txtFldCateg;
	}
	public String getTxtFldCDPepId() {
		return txtFldCDPepId;
	}
	public void setTxtFldCDPepId(String txtFldCDPepId) {
		this.txtFldCDPepId = txtFldCDPepId;
	}
	public String getTxtFldCDPepName() {
		return txtFldCDPepName;
	}
	public void setTxtFldCDPepName(String txtFldCDPepName) {
		this.txtFldCDPepName = txtFldCDPepName;
	}
	public String getTxtFldCDPepNric() {
		return txtFldCDPepNric;
	}
	public void setTxtFldCDPepNric(String txtFldCDPepNric) {
		this.txtFldCDPepNric = txtFldCDPepNric;
	}
	public String getTxtFldCDPepIncNo() {
		return txtFldCDPepIncNo;
	}
	public void setTxtFldCDPepIncNo(String txtFldCDPepIncNo) {
		this.txtFldCDPepIncNo = txtFldCDPepIncNo;
	}
	public String getTxtFldCDPepAddr() {
		return txtFldCDPepAddr;
	}
	public void setTxtFldCDPepAddr(String txtFldCDPepAddr) {
		this.txtFldCDPepAddr = txtFldCDPepAddr;
	}
	public String getRadFldCDPepJob() {
		return radFldCDPepJob;
	}
	public void setRadFldCDPepJob(String radFldCDPepJob) {
		this.radFldCDPepJob = radFldCDPepJob;
	}
	public String getTxtFldCDPepJobCont() {
		return txtFldCDPepJobCont;
	}
	public void setTxtFldCDPepJobCont(String txtFldCDPepJobCont) {
		this.txtFldCDPepJobCont = txtFldCDPepJobCont;
	}
	public String getTxtFldCDPepRel() {
		return txtFldCDPepRel;
	}
	public void setTxtFldCDPepRel(String txtFldCDPepRel) {
		this.txtFldCDPepRel = txtFldCDPepRel;
	}
}
