package com.avallis.ekyc.dto;

// Generated Mar 13, 2018 9:17:21 PM by Hibernate Tools 4.0.0

import java.util.Date;

import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
@JsonIgnoreType
public class FnaDetails implements java.io.Serializable {

	private String fnaId;
	private AdviserStaff adviserStaffByAdvstfId;
	private AdviserStaff adviserStaffByMgrId;
	private CustomerDetails customerDetails;
	private String custId;
	private String fnaType;
	private String ccRepexistinvestflg;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy",timezone = "Asia/Singapore")
	private Date custSignDate;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy",timezone = "Asia/Singapore")
	private Date spsSignDate;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy",timezone = "Asia/Singapore")
	private Date advSignDate;
	private String comments;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy",timezone = "Asia/Singapore")
	private Date mgrSign;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy",timezone = "Asia/Singapore")
	private Date mgrSginDate;
	private String crInvstobj;
	private Double crInvstamt;
	private String crInvsttimehorizon;
	private String crRiskpref;
	private String crRiskclass;
	private Double crRoi;
	private String crAdeqfund;
	private String crOthconcern;
	private String cdMailaddrflg;
	private String cdCustMailaddr;
	private String cdSpsMailaddr;
	private String cdMailaddrresflg;
	private String cdMailaddrOthdet;
	private String cdIntrprtflg;
	private String cdBenfownflg;
	private String cdTppflg;
	private String cdPepflg;
	private String advrecRemarks;
	private String swrepConflg;
	private String swrepConfDets;
	private String swrepAdvbyflg;
	private String swrepDisadvgflg;
	private String swrepProceedflg;
	private String swrepRemarks;
	private String swrepPpFfRemarks;
	private String advrecReason;
	private String cdackAgree;
	private String cdackAdvrecomm;
	private String advdecRemarks;
	private String advrecFnanumber;
	private String ackmgrComments;
	private String ackmgrFnanumber;
	private String fnaCreatedBy;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy",timezone = "Asia/Singapore")
	private Date fnaCreatedDate;
	private String archStatus;
	private String kycSentStatus;
	private String mgrApproveStatus;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy",timezone = "Asia/Singapore")
	private Date mgrApproveDate;
	private String cdCustEmplyr;
	private String cdSpsEmplyr;
	private String cdCustAnnlincome;
	private String cdSpsAnnlincome;
	private String cdRemarks;
	private String advrecRemarks1;
	private String advrecFnanumber1;
	private String advrecRemarks2;
	private String advrecFnanumber2;
	private String ackmgrComments1;
	private String ackmgrFnanumber1;
	private String fatcaBirthplace;
	private String mgrStatusRemarks;
	private String clientConsent;
	private String fatcaNocontryFlg;
	private String custAttachFlg;
	private String suprevMgrFlg;
	private String suprevFollowReason;
	private String cdMrktmatPostalflg;
	private String cdMrktmatEmailflg;
	private String ntucPolicyId;
	private String adminKycSentStatus;
	private String adminApproveStatus;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy",timezone = "Asia/Singapore")
	private Date adminApproveDate;
	private String compKycSentStatus;
	private String compApproveStatus;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	private Date compApproveDate;
	private String adminRemarks;
	private String compRemarks;
	private String ntucPolNum;
	private String mgrapprstsByadvstf;
	private String adminapprstsByadvstf;
	private String compapprstsByadvstf;
	private String mgrEmailSentFlg;
	private String clientFeedbackFlag;
	private String disclosureFollowups;
	private String callbackClient;
	private String callbackAdviser;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy",timezone = "Asia/Singapore")
	private Date callbackApptdate;
	private String callbackCustph;
	private String callbackBy;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy",timezone = "Asia/Singapore")
	private Date callbakOnDate;
	private String callbackFeedbacks;
	private String intprtContact;
	private String intprtName;
	private String intprtNric;
	private String intprtRelat;
	private String cdLanguages;
	private String fwrDepntflg;
	private String fwrFinassetflg;
	private String prodrecObjectives;
	private String advdecOptions;
	private String apptypesClient;
	private String cdLanguageOth;
	private String advrecReason1;
	private String advrecReason2;
	private String advrecReason3;
	private String mgrName;
	private String ntucCaseLock;
	private String ntucLockedCase;
	private String ntucCaseId;
	private String ntucCaseStatus;
	private String policyCreateFlg;
	private String crRiskclassILP;
	private String cdConsentApprch;

	
	public FnaDetails() {
	}

	public FnaDetails(String fnaId, AdviserStaff adviserStaffByAdvstfId,
			AdviserStaff adviserStaffByMgrId, String custId, String fnaType,
			String createdBy, Date createdDate) {
		this.fnaId = fnaId;
		this.adviserStaffByAdvstfId = adviserStaffByAdvstfId;
		this.adviserStaffByMgrId = adviserStaffByMgrId;
		this.custId = custId;
		this.fnaType = fnaType;
		this.fnaCreatedBy = createdBy;
		this.fnaCreatedDate = createdDate;
	}

	public FnaDetails(String fnaId, AdviserStaff adviserStaffByAdvstfId,
			AdviserStaff adviserStaffByMgrId, String custId, String fnaType,
			String ccRepexistinvestflg, Date custSignDate, Date spsSignDate,
			Date advSignDate, String comments, Date mgrSign, Date mgrSginDate,
			String crInvstobj, Double crInvstamt, String crInvsttimehorizon,
			String crRiskpref, String crRiskclass, Double crRoi,
			String crAdeqfund, String crOthconcern, String cdMailaddrflg,
			String cdCustMailaddr, String cdSpsMailaddr,
			String cdMailaddrresflg, String cdMailaddrOthdet,
			String cdIntrprtflg, String cdBenfownflg, String cdTppflg,
			String cdPepflg, String advrecRemarks, String swrepConflg,
			String swrepConfDets, String swrepAdvbyflg,
			String swrepDisadvgflg, String swrepProceedflg,
			String swrepRemarks, String swrepPpFfRemarks, String advrecReason,
			String cdackAgree, String cdackAdvrecomm, String advdecRemarks,
			String advrecFnanumber, String ackmgrComments,
			String ackmgrFnanumber, String createdBy, Date createdDate,
			String archStatus, String kycSentStatus, String mgrApproveStatus,
			Date mgrApproveDate, String cdCustEmplyr, String cdSpsEmplyr,
			String cdCustAnnlincome, String cdSpsAnnlincome, String cdRemarks,
			String advrecRemarks1, String advrecFnanumber1,
			String advrecRemarks2, String advrecFnanumber2,
			String ackmgrComments1, String ackmgrFnanumber1,
			String fatcaBirthplace, String mgrStatusRemarks,
			String clientConsent, String fatcaNocontryFlg,
			String custAttachFlg, String suprevMgrFlg,
			String suprevFollowReason, String cdMrktmatPostalflg,
			String cdMrktmatEmailflg, String ntucPolicyId,
			String adminKycSentStatus, String adminApproveStatus,
			Date adminApproveDate, String compKycSentStatus,
			String compApproveStatus, Date compApproveDate,
			String adminRemarks, String compRemarks, String ntucPolNum,
			String mgrapprstsByadvstf, String adminapprstsByadvstf,
			String compapprstsByadvstf, String mgrEmailSentFlg,
			String clientFeedbackFlag, String disclosureFollowups,
			String callbackClient, String callbackAdviser,
			Date callbackApptdate, String callbackCustph, String callbackBy,
			Date callbakOnDate, String callbackFeedbacks, String intprtContact,
			String intprtName, String intprtNric, String intprtRelat,
			String cdLanguages, String fwrDepntflg, String fwrFinassetflg,
			String prodrecObjectives, String advdecOptions,
			String apptypesClient, String cdLanguageOth, String advrecReason1,
			String advrecReason2, String advrecReason3, String mgrName,
			String ntucCaseLock, String ntucLockedCase, String ntucCaseId,
			String ntucCaseStatus, String policyCreateFlg,
			String crRiskclassILP,
			/*
			 * Set<FnaDependantDets> fnaDependantDetses, Set<FnaExpenditureDets>
			 * fnaExpenditureDetses, Set<FnaFinLiability> fnaFinLiabilities,
			 * Set<FnaCurassDets> fnaCurassDetses, Set<FnaOtherpersonDets>
			 * fnaOtherpersonDetses, Set<FnaRetireplanDets> fnaRetireplanDetses,
			 * Set<FnaContinDepnDets> fnaContinDepnDetses,
			 * Set<FnaSwtchrepPlanDet> fnaSwtchrepPlanDets, Set<FnaGroupValues>
			 * fnaGroupValueses, Set<FnaHistoryDetails> fnaHistoryDetailses,
			 * Set<FnaInvsetmentDets> fnaInvsetmentDetses,
			 * Set<FnaOwnerinvsetDets> fnaOwnerinvsetDetses, Set<FnaFatcaTaxdet>
			 * fnaFatcaTaxdets, Set<FnaCashassetDets> fnaCashassetDetses,
			 * Set<FnaClientFeedback> fnaClientFeedbacks, Set<FnaPersprioIncsrc>
			 * fnaPersprioIncsrcs, Set<FnaContingencyDets> fnaContingencyDetses,
			 * Set<FnaSwtchrepFundDet> fnaSwtchrepFundDets, Set<FnaFlowDets>
			 * fnaFlowDetses, Set<FnaPropownDets> fnaPropownDetses,
			 */
			/*
			 * Set<FnaOtherassetDets> fnaOtherassetDetses,
			 * Set<FnaVehicleownDets> fnaVehicleownDetses,
			 * Set<FnaDisclosureChklist> fnaDisclosureChklists,
			 * Set<FnaSavingGoal> fnaSavingGoals, Set<FnaHealthinsInfo>
			 * fnaHealthinsInfos, Set<FnaSelfspouseDets> fnaSelfspouseDetses,
			 * Set<FnaClientFeedbackReferal> fnaClientFeedbackReferals,
			 * Set<FnaSummaryAnalysis> fnaSummaryAnalysises, Set<FnaCpfDets>
			 * fnaCpfDetses, Set<FnaCkaDets> fnaCkaDetses,
			 * Set<FnaRecomPrdtplanDet> fnaRecomPrdtplanDets,
			 * Set<FnaPolreplaceDets> fnaPolreplaceDetses, Set<FnaRecomFundDet>
			 * fnaRecomFundDets, Set<FnaLifeinsplansDets> fnaLifeinsplansDetses,
			 */

			String cdConsentApprch) {
		this.fnaId = fnaId;
		this.adviserStaffByAdvstfId = adviserStaffByAdvstfId;
		this.adviserStaffByMgrId = adviserStaffByMgrId;
		this.custId = custId;
		this.fnaType = fnaType;
		this.ccRepexistinvestflg = ccRepexistinvestflg;
		this.custSignDate = custSignDate;
		this.spsSignDate = spsSignDate;
		this.advSignDate = advSignDate;
		this.comments = comments;
		this.mgrSign = mgrSign;
		this.mgrSginDate = mgrSginDate;
		this.crInvstobj = crInvstobj;
		this.crInvstamt = crInvstamt;
		this.crInvsttimehorizon = crInvsttimehorizon;
		this.crRiskpref = crRiskpref;
		this.crRiskclass = crRiskclass;
		this.crRoi = crRoi;
		this.crAdeqfund = crAdeqfund;
		this.crOthconcern = crOthconcern;
		this.cdMailaddrflg = cdMailaddrflg;
		this.cdCustMailaddr = cdCustMailaddr;
		this.cdSpsMailaddr = cdSpsMailaddr;
		this.cdMailaddrresflg = cdMailaddrresflg;
		this.cdMailaddrOthdet = cdMailaddrOthdet;
		this.cdIntrprtflg = cdIntrprtflg;
		this.cdBenfownflg = cdBenfownflg;
		this.cdTppflg = cdTppflg;
		this.cdPepflg = cdPepflg;
		this.advrecRemarks = advrecRemarks;
		this.swrepConflg = swrepConflg;
		this.swrepConfDets = swrepConfDets;
		this.swrepAdvbyflg = swrepAdvbyflg;
		this.swrepDisadvgflg = swrepDisadvgflg;
		this.swrepProceedflg = swrepProceedflg;
		this.swrepRemarks = swrepRemarks;
		this.swrepPpFfRemarks = swrepPpFfRemarks;
		this.advrecReason = advrecReason;
		this.cdackAgree = cdackAgree;
		this.cdackAdvrecomm = cdackAdvrecomm;
		this.advdecRemarks = advdecRemarks;
		this.advrecFnanumber = advrecFnanumber;
		this.ackmgrComments = ackmgrComments;
		this.ackmgrFnanumber = ackmgrFnanumber;
		this.fnaCreatedBy = createdBy;
		this.fnaCreatedDate = createdDate;
		this.archStatus = archStatus;
		this.kycSentStatus = kycSentStatus;
		this.mgrApproveStatus = mgrApproveStatus;
		this.mgrApproveDate = mgrApproveDate;
		this.cdCustEmplyr = cdCustEmplyr;
		this.cdSpsEmplyr = cdSpsEmplyr;
		this.cdCustAnnlincome = cdCustAnnlincome;
		this.cdSpsAnnlincome = cdSpsAnnlincome;
		this.cdRemarks = cdRemarks;
		this.advrecRemarks1 = advrecRemarks1;
		this.advrecFnanumber1 = advrecFnanumber1;
		this.advrecRemarks2 = advrecRemarks2;
		this.advrecFnanumber2 = advrecFnanumber2;
		this.ackmgrComments1 = ackmgrComments1;
		this.ackmgrFnanumber1 = ackmgrFnanumber1;
		this.fatcaBirthplace = fatcaBirthplace;
		this.mgrStatusRemarks = mgrStatusRemarks;
		this.clientConsent = clientConsent;
		this.fatcaNocontryFlg = fatcaNocontryFlg;
		this.custAttachFlg = custAttachFlg;
		this.suprevMgrFlg = suprevMgrFlg;
		this.suprevFollowReason = suprevFollowReason;
		this.cdMrktmatPostalflg = cdMrktmatPostalflg;
		this.cdMrktmatEmailflg = cdMrktmatEmailflg;
		this.ntucPolicyId = ntucPolicyId;
		this.adminKycSentStatus = adminKycSentStatus;
		this.adminApproveStatus = adminApproveStatus;
		this.adminApproveDate = adminApproveDate;
		this.compKycSentStatus = compKycSentStatus;
		this.compApproveStatus = compApproveStatus;
		this.compApproveDate = compApproveDate;
		this.adminRemarks = adminRemarks;
		this.compRemarks = compRemarks;
		this.ntucPolNum = ntucPolNum;
		this.mgrapprstsByadvstf = mgrapprstsByadvstf;
		this.adminapprstsByadvstf = adminapprstsByadvstf;
		this.compapprstsByadvstf = compapprstsByadvstf;
		this.mgrEmailSentFlg = mgrEmailSentFlg;
		this.clientFeedbackFlag = clientFeedbackFlag;
		this.disclosureFollowups = disclosureFollowups;
		this.callbackClient = callbackClient;
		this.callbackAdviser = callbackAdviser;
		this.callbackApptdate = callbackApptdate;
		this.callbackCustph = callbackCustph;
		this.callbackBy = callbackBy;
		this.callbakOnDate = callbakOnDate;
		this.callbackFeedbacks = callbackFeedbacks;
		this.intprtContact = intprtContact;
		this.intprtName = intprtName;
		this.intprtNric = intprtNric;
		this.intprtRelat = intprtRelat;
		this.cdLanguages = cdLanguages;
		this.fwrDepntflg = fwrDepntflg;
		this.fwrFinassetflg = fwrFinassetflg;
		this.prodrecObjectives = prodrecObjectives;
		this.advdecOptions = advdecOptions;
		this.apptypesClient = apptypesClient;
		this.cdLanguageOth = cdLanguageOth;
		this.ntucCaseLock = ntucCaseLock;
		this.ntucLockedCase = ntucLockedCase;
		this.ntucCaseId = ntucCaseId;
		this.ntucCaseStatus = ntucCaseStatus;
		this.policyCreateFlg = policyCreateFlg;
		/*
		 * this.fnaDependantDetses = fnaDependantDetses;
		 * this.fnaExpenditureDetses = fnaExpenditureDetses;
		 * this.fnaFinLiabilities = fnaFinLiabilities; this.fnaCurassDetses =
		 * fnaCurassDetses; this.fnaOtherpersonDetses = fnaOtherpersonDetses;
		 * this.fnaRetireplanDetses = fnaRetireplanDetses;
		 * this.fnaContinDepnDetses = fnaContinDepnDetses;
		 * this.fnaSwtchrepPlanDets = fnaSwtchrepPlanDets; this.fnaGroupValueses
		 * = fnaGroupValueses; this.fnaHistoryDetailses = fnaHistoryDetailses;
		 * this.fnaInvsetmentDetses = fnaInvsetmentDetses;
		 * this.fnaOwnerinvsetDetses = fnaOwnerinvsetDetses;
		 * this.fnaFatcaTaxdets = fnaFatcaTaxdets; this.fnaCashassetDetses =
		 * fnaCashassetDetses; this.fnaClientFeedbacks = fnaClientFeedbacks;
		 * this.fnaPersprioIncsrcs = fnaPersprioIncsrcs;
		 * this.fnaContingencyDetses = fnaContingencyDetses;
		 * this.fnaSwtchrepFundDets = fnaSwtchrepFundDets; this.fnaFlowDetses =
		 * fnaFlowDetses; this.fnaPropownDetses = fnaPropownDetses;
		 */
		this.advrecReason1 = advrecReason1;
		this.advrecReason2 = advrecReason2;
		this.advrecReason3 = advrecReason3;
		this.mgrName = mgrName;
		this.crRiskclassILP = crRiskclassILP;

		/*
		 * this.fnaOtherassetDetses = fnaOtherassetDetses;
		 * this.fnaVehicleownDetses = fnaVehicleownDetses;
		 * this.fnaDisclosureChklists = fnaDisclosureChklists;
		 * this.fnaSavingGoals = fnaSavingGoals; this.fnaHealthinsInfos =
		 * fnaHealthinsInfos; this.fnaSelfspouseDetses = fnaSelfspouseDetses;
		 * this.fnaClientFeedbackReferals = fnaClientFeedbackReferals;
		 * this.fnaSummaryAnalysises = fnaSummaryAnalysises; this.fnaCpfDetses =
		 * fnaCpfDetses; this.fnaCkaDetses = fnaCkaDetses;
		 * this.fnaRecomPrdtplanDets = fnaRecomPrdtplanDets;
		 * this.fnaPolreplaceDetses = fnaPolreplaceDetses; this.fnaRecomFundDets
		 * = fnaRecomFundDets; this.fnaLifeinsplansDetses =
		 * fnaLifeinsplansDetses;
		 */
		this.cdConsentApprch = cdConsentApprch;
	}

	public String getFnaId() {
		return this.fnaId;
	}

	public void setFnaId(String fnaId) {
		this.fnaId = fnaId;
	}

	public AdviserStaff getAdviserStaffByAdvstfId() {
		return this.adviserStaffByAdvstfId;
	}

	public void setAdviserStaffByAdvstfId(AdviserStaff adviserStaffByAdvstfId) {
		this.adviserStaffByAdvstfId = adviserStaffByAdvstfId;
	}

	public AdviserStaff getAdviserStaffByMgrId() {
		return this.adviserStaffByMgrId;
	}

	public void setAdviserStaffByMgrId(AdviserStaff adviserStaffByMgrId) {
		this.adviserStaffByMgrId = adviserStaffByMgrId;
	}

	public String getCustId() {
		return this.custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getFnaType() {
		return this.fnaType;
	}

	public void setFnaType(String fnaType) {
		this.fnaType = fnaType;
	}

	public String getCcRepexistinvestflg() {
		return this.ccRepexistinvestflg;
	}

	public void setCcRepexistinvestflg(String ccRepexistinvestflg) {
		this.ccRepexistinvestflg = ccRepexistinvestflg;
	}

	public Date getCustSignDate() {
		return this.custSignDate;
	}

	public void setCustSignDate(Date custSignDate) {
		this.custSignDate = custSignDate;
	}

	public Date getSpsSignDate() {
		return this.spsSignDate;
	}

	public void setSpsSignDate(Date spsSignDate) {
		this.spsSignDate = spsSignDate;
	}

	public Date getAdvSignDate() {
		return this.advSignDate;
	}

	public void setAdvSignDate(Date advSignDate) {
		this.advSignDate = advSignDate;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getMgrSign() {
		return this.mgrSign;
	}

	public void setMgrSign(Date mgrSign) {
		this.mgrSign = mgrSign;
	}

	public Date getMgrSginDate() {
		return this.mgrSginDate;
	}

	public void setMgrSginDate(Date mgrSginDate) {
		this.mgrSginDate = mgrSginDate;
	}

	public String getCrInvstobj() {
		return this.crInvstobj;
	}

	public void setCrInvstobj(String crInvstobj) {
		this.crInvstobj = crInvstobj;
	}

	public Double getCrInvstamt() {
		return this.crInvstamt;
	}

	public void setCrInvstamt(Double crInvstamt) {
		this.crInvstamt = crInvstamt;
	}

	public String getCrInvsttimehorizon() {
		return this.crInvsttimehorizon;
	}

	public void setCrInvsttimehorizon(String crInvsttimehorizon) {
		this.crInvsttimehorizon = crInvsttimehorizon;
	}

	public String getCrRiskpref() {
		return this.crRiskpref;
	}

	public void setCrRiskpref(String crRiskpref) {
		this.crRiskpref = crRiskpref;
	}

	public String getCrRiskclass() {
		return this.crRiskclass;
	}

	public void setCrRiskclass(String crRiskclass) {
		this.crRiskclass = crRiskclass;
	}

	public Double getCrRoi() {
		return this.crRoi;
	}

	public void setCrRoi(Double crRoi) {
		this.crRoi = crRoi;
	}

	public String getCrAdeqfund() {
		return this.crAdeqfund;
	}

	public void setCrAdeqfund(String crAdeqfund) {
		this.crAdeqfund = crAdeqfund;
	}

	public String getCrOthconcern() {
		return this.crOthconcern;
	}

	public void setCrOthconcern(String crOthconcern) {
		this.crOthconcern = crOthconcern;
	}

	public String getCdMailaddrflg() {
		return this.cdMailaddrflg;
	}

	public void setCdMailaddrflg(String cdMailaddrflg) {
		this.cdMailaddrflg = cdMailaddrflg;
	}

	public String getCdCustMailaddr() {
		return this.cdCustMailaddr;
	}

	public void setCdCustMailaddr(String cdCustMailaddr) {
		this.cdCustMailaddr = cdCustMailaddr;
	}

	public String getCdSpsMailaddr() {
		return this.cdSpsMailaddr;
	}

	public void setCdSpsMailaddr(String cdSpsMailaddr) {
		this.cdSpsMailaddr = cdSpsMailaddr;
	}

	public String getCdMailaddrresflg() {
		return this.cdMailaddrresflg;
	}

	public void setCdMailaddrresflg(String cdMailaddrresflg) {
		this.cdMailaddrresflg = cdMailaddrresflg;
	}

	public String getCdMailaddrOthdet() {
		return this.cdMailaddrOthdet;
	}

	public void setCdMailaddrOthdet(String cdMailaddrOthdet) {
		this.cdMailaddrOthdet = cdMailaddrOthdet;
	}

	public String getCdIntrprtflg() {
		return this.cdIntrprtflg;
	}

	public void setCdIntrprtflg(String cdIntrprtflg) {
		this.cdIntrprtflg = cdIntrprtflg;
	}

	public String getCdBenfownflg() {
		return this.cdBenfownflg;
	}

	public void setCdBenfownflg(String cdBenfownflg) {
		this.cdBenfownflg = cdBenfownflg;
	}

	public String getCdTppflg() {
		return this.cdTppflg;
	}

	public void setCdTppflg(String cdTppflg) {
		this.cdTppflg = cdTppflg;
	}

	public String getCdPepflg() {
		return this.cdPepflg;
	}

	public void setCdPepflg(String cdPepflg) {
		this.cdPepflg = cdPepflg;
	}

	public String getAdvrecRemarks() {
		return this.advrecRemarks;
	}

	public void setAdvrecRemarks(String advrecRemarks) {
		this.advrecRemarks = advrecRemarks;
	}

	public String getSwrepConflg() {
		return this.swrepConflg;
	}

	public void setSwrepConflg(String swrepConflg) {
		this.swrepConflg = swrepConflg;
	}

	public String getSwrepConfDets() {
		return this.swrepConfDets;
	}

	public void setSwrepConfDets(String swrepConfDets) {
		this.swrepConfDets = swrepConfDets;
	}

	public String getSwrepAdvbyflg() {
		return this.swrepAdvbyflg;
	}

	public void setSwrepAdvbyflg(String swrepAdvbyflg) {
		this.swrepAdvbyflg = swrepAdvbyflg;
	}

	public String getSwrepDisadvgflg() {
		return this.swrepDisadvgflg;
	}

	public void setSwrepDisadvgflg(String swrepDisadvgflg) {
		this.swrepDisadvgflg = swrepDisadvgflg;
	}

	public String getSwrepProceedflg() {
		return this.swrepProceedflg;
	}

	public void setSwrepProceedflg(String swrepProceedflg) {
		this.swrepProceedflg = swrepProceedflg;
	}

	public String getSwrepRemarks() {
		return this.swrepRemarks;
	}

	public void setSwrepRemarks(String swrepRemarks) {
		this.swrepRemarks = swrepRemarks;
	}

	public String getSwrepPpFfRemarks() {
		return this.swrepPpFfRemarks;
	}

	public void setSwrepPpFfRemarks(String swrepPpFfRemarks) {
		this.swrepPpFfRemarks = swrepPpFfRemarks;
	}

	public String getAdvrecReason() {
		return this.advrecReason;
	}

	public void setAdvrecReason(String advrecReason) {
		this.advrecReason = advrecReason;
	}

	public String getCdackAgree() {
		return this.cdackAgree;
	}

	public void setCdackAgree(String cdackAgree) {
		this.cdackAgree = cdackAgree;
	}

	public String getCdackAdvrecomm() {
		return this.cdackAdvrecomm;
	}

	public void setCdackAdvrecomm(String cdackAdvrecomm) {
		this.cdackAdvrecomm = cdackAdvrecomm;
	}

	public String getAdvdecRemarks() {
		return this.advdecRemarks;
	}

	public void setAdvdecRemarks(String advdecRemarks) {
		this.advdecRemarks = advdecRemarks;
	}

	public String getAdvrecFnanumber() {
		return this.advrecFnanumber;
	}

	public void setAdvrecFnanumber(String advrecFnanumber) {
		this.advrecFnanumber = advrecFnanumber;
	}

	public String getAckmgrComments() {
		return this.ackmgrComments;
	}

	public void setAckmgrComments(String ackmgrComments) {
		this.ackmgrComments = ackmgrComments;
	}

	public String getAckmgrFnanumber() {
		return this.ackmgrFnanumber;
	}

	public void setAckmgrFnanumber(String ackmgrFnanumber) {
		this.ackmgrFnanumber = ackmgrFnanumber;
	}

	

	public String getArchStatus() {
		return this.archStatus;
	}

	public void setArchStatus(String archStatus) {
		this.archStatus = archStatus;
	}

	public String getKycSentStatus() {
		return this.kycSentStatus;
	}

	public void setKycSentStatus(String kycSentStatus) {
		this.kycSentStatus = kycSentStatus;
	}

	public String getMgrApproveStatus() {
		return this.mgrApproveStatus;
	}

	public void setMgrApproveStatus(String mgrApproveStatus) {
		this.mgrApproveStatus = mgrApproveStatus;
	}

	public Date getMgrApproveDate() {
		return this.mgrApproveDate;
	}

	public void setMgrApproveDate(Date mgrApproveDate) {
		this.mgrApproveDate = mgrApproveDate;
	}

	public String getCdCustEmplyr() {
		return this.cdCustEmplyr;
	}

	public void setCdCustEmplyr(String cdCustEmplyr) {
		this.cdCustEmplyr = cdCustEmplyr;
	}

	public String getCdSpsEmplyr() {
		return this.cdSpsEmplyr;
	}

	public void setCdSpsEmplyr(String cdSpsEmplyr) {
		this.cdSpsEmplyr = cdSpsEmplyr;
	}

	public String getCdCustAnnlincome() {
		return this.cdCustAnnlincome;
	}

	public void setCdCustAnnlincome(String cdCustAnnlincome) {
		this.cdCustAnnlincome = cdCustAnnlincome;
	}

	public String getCdSpsAnnlincome() {
		return this.cdSpsAnnlincome;
	}

	public void setCdSpsAnnlincome(String cdSpsAnnlincome) {
		this.cdSpsAnnlincome = cdSpsAnnlincome;
	}

	public String getCdRemarks() {
		return this.cdRemarks;
	}

	public void setCdRemarks(String cdRemarks) {
		this.cdRemarks = cdRemarks;
	}

	public String getAdvrecRemarks1() {
		return this.advrecRemarks1;
	}

	public void setAdvrecRemarks1(String advrecRemarks1) {
		this.advrecRemarks1 = advrecRemarks1;
	}

	public String getAdvrecFnanumber1() {
		return this.advrecFnanumber1;
	}

	public void setAdvrecFnanumber1(String advrecFnanumber1) {
		this.advrecFnanumber1 = advrecFnanumber1;
	}

	public String getAdvrecRemarks2() {
		return this.advrecRemarks2;
	}

	public void setAdvrecRemarks2(String advrecRemarks2) {
		this.advrecRemarks2 = advrecRemarks2;
	}

	public String getAdvrecFnanumber2() {
		return this.advrecFnanumber2;
	}

	public void setAdvrecFnanumber2(String advrecFnanumber2) {
		this.advrecFnanumber2 = advrecFnanumber2;
	}

	public String getAckmgrComments1() {
		return this.ackmgrComments1;
	}

	public void setAckmgrComments1(String ackmgrComments1) {
		this.ackmgrComments1 = ackmgrComments1;
	}

	public String getAckmgrFnanumber1() {
		return this.ackmgrFnanumber1;
	}

	public void setAckmgrFnanumber1(String ackmgrFnanumber1) {
		this.ackmgrFnanumber1 = ackmgrFnanumber1;
	}

	public String getFatcaBirthplace() {
		return this.fatcaBirthplace;
	}

	public void setFatcaBirthplace(String fatcaBirthplace) {
		this.fatcaBirthplace = fatcaBirthplace;
	}

	public String getMgrStatusRemarks() {
		return this.mgrStatusRemarks;
	}

	public void setMgrStatusRemarks(String mgrStatusRemarks) {
		this.mgrStatusRemarks = mgrStatusRemarks;
	}

	public String getClientConsent() {
		return this.clientConsent;
	}

	public void setClientConsent(String clientConsent) {
		this.clientConsent = clientConsent;
	}

	public String getFatcaNocontryFlg() {
		return this.fatcaNocontryFlg;
	}

	public void setFatcaNocontryFlg(String fatcaNocontryFlg) {
		this.fatcaNocontryFlg = fatcaNocontryFlg;
	}

	public String getCustAttachFlg() {
		return this.custAttachFlg;
	}

	public void setCustAttachFlg(String custAttachFlg) {
		this.custAttachFlg = custAttachFlg;
	}

	public String getSuprevMgrFlg() {
		return this.suprevMgrFlg;
	}

	public void setSuprevMgrFlg(String suprevMgrFlg) {
		this.suprevMgrFlg = suprevMgrFlg;
	}

	public String getSuprevFollowReason() {
		return this.suprevFollowReason;
	}

	public void setSuprevFollowReason(String suprevFollowReason) {
		this.suprevFollowReason = suprevFollowReason;
	}

	public String getCdMrktmatPostalflg() {
		return this.cdMrktmatPostalflg;
	}

	public void setCdMrktmatPostalflg(String cdMrktmatPostalflg) {
		this.cdMrktmatPostalflg = cdMrktmatPostalflg;
	}

	public String getCdMrktmatEmailflg() {
		return this.cdMrktmatEmailflg;
	}

	public void setCdMrktmatEmailflg(String cdMrktmatEmailflg) {
		this.cdMrktmatEmailflg = cdMrktmatEmailflg;
	}

	public String getNtucPolicyId() {
		return this.ntucPolicyId;
	}

	public void setNtucPolicyId(String ntucPolicyId) {
		this.ntucPolicyId = ntucPolicyId;
	}

	public String getAdminKycSentStatus() {
		return this.adminKycSentStatus;
	}

	public void setAdminKycSentStatus(String adminKycSentStatus) {
		this.adminKycSentStatus = adminKycSentStatus;
	}

	public String getAdminApproveStatus() {
		return this.adminApproveStatus;
	}

	public void setAdminApproveStatus(String adminApproveStatus) {
		this.adminApproveStatus = adminApproveStatus;
	}

	public Date getAdminApproveDate() {
		return this.adminApproveDate;
	}

	public void setAdminApproveDate(Date adminApproveDate) {
		this.adminApproveDate = adminApproveDate;
	}

	public String getCompKycSentStatus() {
		return this.compKycSentStatus;
	}

	public void setCompKycSentStatus(String compKycSentStatus) {
		this.compKycSentStatus = compKycSentStatus;
	}

	public String getCompApproveStatus() {
		return this.compApproveStatus;
	}

	public void setCompApproveStatus(String compApproveStatus) {
		this.compApproveStatus = compApproveStatus;
	}

	public Date getCompApproveDate() {
		return this.compApproveDate;
	}

	public void setCompApproveDate(Date compApproveDate) {
		this.compApproveDate = compApproveDate;
	}

	public String getAdminRemarks() {
		return this.adminRemarks;
	}

	public void setAdminRemarks(String adminRemarks) {
		this.adminRemarks = adminRemarks;
	}

	public String getCompRemarks() {
		return this.compRemarks;
	}

	public void setCompRemarks(String compRemarks) {
		this.compRemarks = compRemarks;
	}

	public String getNtucPolNum() {
		return this.ntucPolNum;
	}

	public void setNtucPolNum(String ntucPolNum) {
		this.ntucPolNum = ntucPolNum;
	}

	public String getMgrapprstsByadvstf() {
		return this.mgrapprstsByadvstf;
	}

	public void setMgrapprstsByadvstf(String mgrapprstsByadvstf) {
		this.mgrapprstsByadvstf = mgrapprstsByadvstf;
	}

	public String getAdminapprstsByadvstf() {
		return this.adminapprstsByadvstf;
	}

	public void setAdminapprstsByadvstf(String adminapprstsByadvstf) {
		this.adminapprstsByadvstf = adminapprstsByadvstf;
	}

	public String getCompapprstsByadvstf() {
		return this.compapprstsByadvstf;
	}

	public void setCompapprstsByadvstf(String compapprstsByadvstf) {
		this.compapprstsByadvstf = compapprstsByadvstf;
	}

	public String getMgrEmailSentFlg() {
		return this.mgrEmailSentFlg;
	}

	public void setMgrEmailSentFlg(String mgrEmailSentFlg) {
		this.mgrEmailSentFlg = mgrEmailSentFlg;
	}

	public String getClientFeedbackFlag() {
		return this.clientFeedbackFlag;
	}

	public void setClientFeedbackFlag(String clientFeedbackFlag) {
		this.clientFeedbackFlag = clientFeedbackFlag;
	}

	public String getDisclosureFollowups() {
		return this.disclosureFollowups;
	}

	public void setDisclosureFollowups(String disclosureFollowups) {
		this.disclosureFollowups = disclosureFollowups;
	}

	public String getCallbackClient() {
		return this.callbackClient;
	}

	public void setCallbackClient(String callbackClient) {
		this.callbackClient = callbackClient;
	}

	public String getCallbackAdviser() {
		return this.callbackAdviser;
	}

	public void setCallbackAdviser(String callbackAdviser) {
		this.callbackAdviser = callbackAdviser;
	}

	public Date getCallbackApptdate() {
		return this.callbackApptdate;
	}

	public void setCallbackApptdate(Date callbackApptdate) {
		this.callbackApptdate = callbackApptdate;
	}

	public String getCallbackCustph() {
		return this.callbackCustph;
	}

	public void setCallbackCustph(String callbackCustph) {
		this.callbackCustph = callbackCustph;
	}

	public String getCallbackBy() {
		return this.callbackBy;
	}

	public void setCallbackBy(String callbackBy) {
		this.callbackBy = callbackBy;
	}

	public Date getCallbakOnDate() {
		return this.callbakOnDate;
	}

	public void setCallbakOnDate(Date callbakOnDate) {
		this.callbakOnDate = callbakOnDate;
	}

	public String getCallbackFeedbacks() {
		return this.callbackFeedbacks;
	}

	public void setCallbackFeedbacks(String callbackFeedbacks) {
		this.callbackFeedbacks = callbackFeedbacks;
	}

	public String getIntprtContact() {
		return this.intprtContact;
	}

	public void setIntprtContact(String intprtContact) {
		this.intprtContact = intprtContact;
	}

	public String getIntprtName() {
		return this.intprtName;
	}

	public void setIntprtName(String intprtName) {
		this.intprtName = intprtName;
	}

	public String getIntprtNric() {
		return this.intprtNric;
	}

	public void setIntprtNric(String intprtNric) {
		this.intprtNric = intprtNric;
	}

	public String getIntprtRelat() {
		return this.intprtRelat;
	}

	public void setIntprtRelat(String intprtRelat) {
		this.intprtRelat = intprtRelat;
	}

	public String getCdLanguages() {
		return this.cdLanguages;
	}

	public void setCdLanguages(String cdLanguages) {
		this.cdLanguages = cdLanguages;
	}

	public String getFwrDepntflg() {
		return this.fwrDepntflg;
	}

	public void setFwrDepntflg(String fwrDepntflg) {
		this.fwrDepntflg = fwrDepntflg;
	}

	public String getFwrFinassetflg() {
		return this.fwrFinassetflg;
	}

	public void setFwrFinassetflg(String fwrFinassetflg) {
		this.fwrFinassetflg = fwrFinassetflg;
	}

	public String getProdrecObjectives() {
		return this.prodrecObjectives;
	}

	public void setProdrecObjectives(String prodrecObjectives) {
		this.prodrecObjectives = prodrecObjectives;
	}

	/*
	 * public Set<FnaDependantDets> getFnaDependantDetses() { return
	 * this.fnaDependantDetses; }
	 * 
	 * public void setFnaDependantDetses(Set<FnaDependantDets>
	 * fnaDependantDetses) { this.fnaDependantDetses = fnaDependantDetses; }
	 * 
	 * public Set<FnaExpenditureDets> getFnaExpenditureDetses() { return
	 * this.fnaExpenditureDetses; }
	 * 
	 * public void setFnaExpenditureDetses( Set<FnaExpenditureDets>
	 * fnaExpenditureDetses) { this.fnaExpenditureDetses = fnaExpenditureDetses;
	 * }
	 * 
	 * public Set<FnaFinLiability> getFnaFinLiabilities() { return
	 * this.fnaFinLiabilities; }
	 */
	/*
	 * public void setFnaFinLiabilities(Set<FnaFinLiability> fnaFinLiabilities)
	 * { this.fnaFinLiabilities = fnaFinLiabilities; }
	 * 
	 * public Set<FnaCurassDets> getFnaCurassDetses() { return
	 * this.fnaCurassDetses; }
	 * 
	 * public void setFnaCurassDetses(Set<FnaCurassDets> fnaCurassDetses) {
	 * this.fnaCurassDetses = fnaCurassDetses; }
	 * 
	 * public Set<FnaOtherpersonDets> getFnaOtherpersonDetses() { return
	 * this.fnaOtherpersonDetses; }
	 * 
	 * public void setFnaOtherpersonDetses( Set<FnaOtherpersonDets>
	 * fnaOtherpersonDetses) { this.fnaOtherpersonDetses = fnaOtherpersonDetses;
	 * }
	 * 
	 * public Set<FnaRetireplanDets> getFnaRetireplanDetses() { return
	 * this.fnaRetireplanDetses; }
	 * 
	 * public void setFnaRetireplanDetses( Set<FnaRetireplanDets>
	 * fnaRetireplanDetses) { this.fnaRetireplanDetses = fnaRetireplanDetses; }
	 * 
	 * public Set<FnaContinDepnDets> getFnaContinDepnDetses() { return
	 * this.fnaContinDepnDetses; }
	 * 
	 * public void setFnaContinDepnDetses( Set<FnaContinDepnDets>
	 * fnaContinDepnDetses) { this.fnaContinDepnDetses = fnaContinDepnDetses; }
	 * 
	 * public Set<FnaSwtchrepPlanDet> getFnaSwtchrepPlanDets() { return
	 * this.fnaSwtchrepPlanDets; }
	 * 
	 * public void setFnaSwtchrepPlanDets( Set<FnaSwtchrepPlanDet>
	 * fnaSwtchrepPlanDets) { this.fnaSwtchrepPlanDets = fnaSwtchrepPlanDets; }
	 */

	/*
	 * public Set<FnaGroupValues> getFnaGroupValueses() { return
	 * this.fnaGroupValueses; }
	 * 
	 * public void setFnaGroupValueses(Set<FnaGroupValues> fnaGroupValueses) {
	 * this.fnaGroupValueses = fnaGroupValueses; }
	 * 
	 * public Set<FnaHistoryDetails> getFnaHistoryDetailses() { return
	 * this.fnaHistoryDetailses; }
	 * 
	 * public void setFnaHistoryDetailses( Set<FnaHistoryDetails>
	 * fnaHistoryDetailses) { this.fnaHistoryDetailses = fnaHistoryDetailses; }
	 */

	/*
	 * public Set<FnaInvsetmentDets> getFnaInvsetmentDetses() { return
	 * this.fnaInvsetmentDetses; }
	 * 
	 * public void setFnaInvsetmentDetses( Set<FnaInvsetmentDets>
	 * fnaInvsetmentDetses) { this.fnaInvsetmentDetses = fnaInvsetmentDetses; }
	 * 
	 * public Set<FnaOwnerinvsetDets> getFnaOwnerinvsetDetses() { return
	 * this.fnaOwnerinvsetDetses; }
	 * 
	 * public void setFnaOwnerinvsetDetses( Set<FnaOwnerinvsetDets>
	 * fnaOwnerinvsetDetses) { this.fnaOwnerinvsetDetses = fnaOwnerinvsetDetses;
	 * }
	 * 
	 * public Set<FnaFatcaTaxdet> getFnaFatcaTaxdets() { return
	 * this.fnaFatcaTaxdets; }
	 * 
	 * public void setFnaFatcaTaxdets(Set<FnaFatcaTaxdet> fnaFatcaTaxdets) {
	 * this.fnaFatcaTaxdets = fnaFatcaTaxdets; }
	 * 
	 * public Set<FnaCashassetDets> getFnaCashassetDetses() { return
	 * this.fnaCashassetDetses; }
	 * 
	 * public void setFnaCashassetDetses(Set<FnaCashassetDets>
	 * fnaCashassetDetses) { this.fnaCashassetDetses = fnaCashassetDetses; }
	 * 
	 * public Set<FnaClientFeedback> getFnaClientFeedbacks() { return
	 * this.fnaClientFeedbacks; }
	 * 
	 * public void setFnaClientFeedbacks(Set<FnaClientFeedback>
	 * fnaClientFeedbacks) { this.fnaClientFeedbacks = fnaClientFeedbacks; }
	 * 
	 * public Set<FnaPersprioIncsrc> getFnaPersprioIncsrcs() { return
	 * this.fnaPersprioIncsrcs; }
	 * 
	 * public void setFnaPersprioIncsrcs(Set<FnaPersprioIncsrc>
	 * fnaPersprioIncsrcs) { this.fnaPersprioIncsrcs = fnaPersprioIncsrcs; }
	 * 
	 * public Set<FnaContingencyDets> getFnaContingencyDetses() { return
	 * this.fnaContingencyDetses; }
	 * 
	 * public void setFnaContingencyDetses( Set<FnaContingencyDets>
	 * fnaContingencyDetses) { this.fnaContingencyDetses = fnaContingencyDetses;
	 * }
	 * 
	 * public Set<FnaSwtchrepFundDet> getFnaSwtchrepFundDets() { return
	 * this.fnaSwtchrepFundDets; }
	 * 
	 * public void setFnaSwtchrepFundDets( Set<FnaSwtchrepFundDet>
	 * fnaSwtchrepFundDets) { this.fnaSwtchrepFundDets = fnaSwtchrepFundDets; }
	 * 
	 * public Set<FnaFlowDets> getFnaFlowDetses() { return this.fnaFlowDetses; }
	 * 
	 * public void setFnaFlowDetses(Set<FnaFlowDets> fnaFlowDetses) {
	 * this.fnaFlowDetses = fnaFlowDetses; }
	 * 
	 * public Set<FnaPropownDets> getFnaPropownDetses() { return
	 * this.fnaPropownDetses; }
	 * 
	 * public void setFnaPropownDetses(Set<FnaPropownDets> fnaPropownDetses) {
	 * this.fnaPropownDetses = fnaPropownDetses; }
	 * 
	 * public Set<FnaOtherassetDets> getFnaOtherassetDetses() { return
	 * this.fnaOtherassetDetses; }
	 */

	/*
	 * public void setFnaOtherassetDetses( Set<FnaOtherassetDets>
	 * fnaOtherassetDetses) { this.fnaOtherassetDetses = fnaOtherassetDetses; }
	 * 
	 * public Set<FnaVehicleownDets> getFnaVehicleownDetses() { return
	 * this.fnaVehicleownDetses; }
	 * 
	 * public void setFnaVehicleownDetses( Set<FnaVehicleownDets>
	 * fnaVehicleownDetses) { this.fnaVehicleownDetses = fnaVehicleownDetses; }
	 * 
	 * public Set<FnaDisclosureChklist> getFnaDisclosureChklists() { return
	 * this.fnaDisclosureChklists; }
	 * 
	 * public void setFnaDisclosureChklists( Set<FnaDisclosureChklist>
	 * fnaDisclosureChklists) { this.fnaDisclosureChklists =
	 * fnaDisclosureChklists; }
	 */
	/*
	 * public Set<FnaSavingGoal> getFnaSavingGoals() { return
	 * this.fnaSavingGoals; }
	 * 
	 * public void setFnaSavingGoals(Set<FnaSavingGoal> fnaSavingGoals) {
	 * this.fnaSavingGoals = fnaSavingGoals; }
	 * 
	 * public Set<FnaHealthinsInfo> getFnaHealthinsInfos() { return
	 * this.fnaHealthinsInfos; }
	 * 
	 * public void setFnaHealthinsInfos(Set<FnaHealthinsInfo> fnaHealthinsInfos)
	 * { this.fnaHealthinsInfos = fnaHealthinsInfos; }
	 * 
	 * public Set<FnaSelfspouseDets> getFnaSelfspouseDetses() { return
	 * this.fnaSelfspouseDetses; }
	 * 
	 * public void setFnaSelfspouseDetses( Set<FnaSelfspouseDets>
	 * fnaSelfspouseDetses) { this.fnaSelfspouseDetses = fnaSelfspouseDetses; }
	 */
	/*
	 * public Set<FnaClientFeedbackReferal> getFnaClientFeedbackReferals() {
	 * return this.fnaClientFeedbackReferals; }
	 * 
	 * public void setFnaClientFeedbackReferals( Set<FnaClientFeedbackReferal>
	 * fnaClientFeedbackReferals) { this.fnaClientFeedbackReferals =
	 * fnaClientFeedbackReferals; }
	 */

	/*
	 * public Set<FnaSummaryAnalysis> getFnaSummaryAnalysises() { return
	 * this.fnaSummaryAnalysises; }
	 * 
	 * public void setFnaSummaryAnalysises( Set<FnaSummaryAnalysis>
	 * fnaSummaryAnalysises) { this.fnaSummaryAnalysises = fnaSummaryAnalysises;
	 * }
	 * 
	 * public Set<FnaCpfDets> getFnaCpfDetses() { return this.fnaCpfDetses; }
	 * 
	 * public void setFnaCpfDetses(Set<FnaCpfDets> fnaCpfDetses) {
	 * this.fnaCpfDetses = fnaCpfDetses; }
	 * 
	 * public Set<FnaCkaDets> getFnaCkaDetses() { return this.fnaCkaDetses; }
	 * 
	 * public void setFnaCkaDetses(Set<FnaCkaDets> fnaCkaDetses) {
	 * this.fnaCkaDetses = fnaCkaDetses; }
	 */

	/*
	 * public Set<FnaRecomPrdtplanDet> getFnaRecomPrdtplanDets() { return
	 * this.fnaRecomPrdtplanDets; }
	 * 
	 * public void setFnaRecomPrdtplanDets( Set<FnaRecomPrdtplanDet>
	 * fnaRecomPrdtplanDets) { this.fnaRecomPrdtplanDets = fnaRecomPrdtplanDets;
	 * }
	 * 
	 * public Set<FnaPolreplaceDets> getFnaPolreplaceDetses() { return
	 * this.fnaPolreplaceDetses; }
	 * 
	 * public void setFnaPolreplaceDetses( Set<FnaPolreplaceDets>
	 * fnaPolreplaceDetses) { this.fnaPolreplaceDetses = fnaPolreplaceDetses; }
	 */
	/*
	 * public Set<FnaRecomFundDet> getFnaRecomFundDets() { return
	 * this.fnaRecomFundDets; }
	 * 
	 * public void setFnaRecomFundDets(Set<FnaRecomFundDet> fnaRecomFundDets) {
	 * this.fnaRecomFundDets = fnaRecomFundDets; }
	 * 
	 * public Set<FnaLifeinsplansDets> getFnaLifeinsplansDetses() { return
	 * this.fnaLifeinsplansDetses; }
	 * 
	 * public void setFnaLifeinsplansDetses( Set<FnaLifeinsplansDets>
	 * fnaLifeinsplansDetses) { this.fnaLifeinsplansDetses =
	 * fnaLifeinsplansDetses; }
	 */

	public CustomerDetails getCustomerDetails() {
		return customerDetails;
	}

	public void setCustomerDetails(CustomerDetails customerDetails) {
		this.customerDetails = customerDetails;
	}

	public String getAdvdecOptions() {
		return advdecOptions;
	}

	public void setAdvdecOptions(String advdecOptions) {
		this.advdecOptions = advdecOptions;
	}

	public String getApptypesClient() {
		return apptypesClient;
	}

	public void setApptypesClient(String apptypesClient) {
		this.apptypesClient = apptypesClient;
	}

	public String getCdLanguageOth() {
		return cdLanguageOth;
	}

	public void setCdLanguageOth(String cdLanguageOth) {
		this.cdLanguageOth = cdLanguageOth;
	}

	public String getAdvrecReason1() {
		return advrecReason1;
	}

	public void setAdvrecReason1(String advrecReason1) {
		this.advrecReason1 = advrecReason1;
	}

	public String getAdvrecReason2() {
		return advrecReason2;
	}

	public void setAdvrecReason2(String advrecReason2) {
		this.advrecReason2 = advrecReason2;
	}

	public String getAdvrecReason3() {
		return advrecReason3;
	}

	public void setAdvrecReason3(String advrecReason3) {
		this.advrecReason3 = advrecReason3;
	}

	public String getmgrName() {
		return mgrName;
	}

	public void setmgrName(String txtFldMgrName) {
		this.mgrName = txtFldMgrName;
	}

	public String getNtucCaseLock() {
		return ntucCaseLock;
	}

	public void setNtucCaseLock(String ntucCaseLock) {
		this.ntucCaseLock = ntucCaseLock;
	}

	public String getNtucLockedCase() {
		return ntucLockedCase;
	}

	public void setNtucLockedCase(String ntucLockedCase) {
		this.ntucLockedCase = ntucLockedCase;
	}

	public String getNtucCaseId() {
		return ntucCaseId;
	}

	public void setNtucCaseId(String ntucCaseId) {
		this.ntucCaseId = ntucCaseId;
	}

	public String getNtucCaseStatus() {
		return ntucCaseStatus;
	}

	public void setNtucCaseStatus(String ntucCaseStatus) {
		this.ntucCaseStatus = ntucCaseStatus;
	}

	public String getPolicyCreateFlg() {
		return policyCreateFlg;
	}

	public void setPolicyCreateFlg(String policyCreateFlg) {
		this.policyCreateFlg = policyCreateFlg;
	}

	public String getCrRiskclassILP() {
		return crRiskclassILP;
	}

	public void setCrRiskclassILP(String crRiskclassILP) {
		this.crRiskclassILP = crRiskclassILP;
	}

	public String getCdConsentApprch() {
		return cdConsentApprch;
	}

	public void setCdConsentApprch(String cdConsentApprch) {
		this.cdConsentApprch = cdConsentApprch;
	}
	public String getFnaCreatedBy() {
		return fnaCreatedBy;
	}

	public void setFnaCreatedBy(String fnaCreatedBy) {
		this.fnaCreatedBy = fnaCreatedBy;
	}

	public Date getFnaCreatedDate() {
		return fnaCreatedDate;
	}

	public void setFnaCreatedDate(Date fnaCreatedDate) {
		this.fnaCreatedDate = fnaCreatedDate;
	}

}
