package com.avallis.ekyc.dto;

// Generated Mar 13, 2018 9:17:21 PM by Hibernate Tools 4.0.0

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Policy generated by hbm2java
 */
public class Policy implements java.io.Serializable {

	private String appid;
	private CustomerDetails customerDetailsByPolicyOwner;
	private MasterProductLine masterProductLine;
	private AdviserStaff adviserStaffByAdviserInitial;
	private AdviserStaff adviserStaffByAdviserCurrent;
	private MasterPrincipal masterPrincipal;
	private AdviserStaff adviserStaffByAdviserComm;
	private CustomerDetails customerDetailsByCustidProposed;
	private CustomerDetails customerDetailsByCustidAssured;
	private MasterPolicyStatus masterPolicyStatus;
	private FpmsDistributor fpmsDistributor;
	private String polType;
	private String policyno;
	private String prevPolNo;
	private Date registrationDate;
	private Date issueDate;
	private Date effDate;
	private Date renewalDate;
	private Date renewalReminderDate;
	private Date submDate;
	private Date termDate;
	private Date endDate;
	private Date counterStopDate;
	private Date polStatusDate;
	private Date prevPolnoChngeDate;
	private String spTopupFlg;
	private String cpfFlg;
	private String gstFlg;
	private String autoTranfFlg;
	private Short autoTranfAge;
	private String paymentMode;
	private String paymentMethod;
	private String currencyType;
	private String cpfAccNo;
	private Byte giroFirstDeductDate;
	private Byte giroSecondDeductDate;
	private String termsAndCond;
	private String remarks;
	private String createdBy;
	private Date createdDate;
	private String modifiedBy;
	private String distributorName;
	private Double totalSa;
	private Double totalPrem;
	private String polCorAddr1;
	private String polCorAddr2;
	private String polCorAddr3;
	private String polCorCity;
	private String polCorState;
	private String polCorCountry;
	private String polCorPostCde;
	private String polPlnName;
	private Date docDispatchDate;
	private String tempAppidFpms06Dm;
	private String sourceData;
	private String avivaSplit;
	private String ntucPolicyId;
	private Date polModifiedDate;
	private String ntucCaseId;
	
	/*private Set<PolAdviserShare> polAdviserShares = new HashSet<PolAdviserShare>(
			0);
	private Set<PolWillstrustsPlanDets> polWillstrustsPlanDetses = new HashSet<PolWillstrustsPlanDets>(
			0);
	private Set<PolInslifebasic> polInslifebasics = new HashSet<PolInslifebasic>(
			0);
	private Set<PolBenefDets> polBenefDetses = new HashSet<PolBenefDets>(0);
	private Set<PolIlptopup> polIlptopups = new HashSet<PolIlptopup>(0);
	private Set<PolAlterations> polAlterationses = new HashSet<PolAlterations>(
			0);
	private Set<PolAttachments> polAttachmentses = new HashSet<PolAttachments>(
			0);
	private Set<CommScreeningDets> commScreeningDetses = new HashSet<CommScreeningDets>(
			0);
	private Set<PolLoanDets> polLoanDetses = new HashSet<PolLoanDets>(0);
	private Set<PolIlpfunds> polIlpfundses = new HashSet<PolIlpfunds>(0);
	private Set<PolMedTest> polMedTests = new HashSet<PolMedTest>(0);
	private Set<PolWillstrustsDets> polWillstrustsDetses = new HashSet<PolWillstrustsDets>(
			0);
	private Set<CommstmtRiders> commstmtRiderses = new HashSet<CommstmtRiders>(
			0);
	private Set<PolTransfer> polTransfers = new HashSet<PolTransfer>(0);
	private Set<CommAmmendments> commAmmendmentses = new HashSet<CommAmmendments>(
			0);
	private Set<DocHandling> docHandlings = new HashSet<DocHandling>(0);
	private Set<PolInsliferider> polInsliferiders = new HashSet<PolInsliferider>(
			0);
	private Set<PolLobDistDets> polLobDistDetses = new HashSet<PolLobDistDets>(
			0);
	private Set<PolTrusteeDets> polTrusteeDetses = new HashSet<PolTrusteeDets>(
			0);
	private Set<PolPaymentTracking> polPaymentTrackings = new HashSet<PolPaymentTracking>(
			0);
	private Set<Commstmt> commstmts = new HashSet<Commstmt>(0);
	private Set<PolUnderwrite> polUnderwrites = new HashSet<PolUnderwrite>(0);
	private Set<PolLandbanking> polLandbankings = new HashSet<PolLandbanking>(0);
	private Set<PolAssignment> polAssignments = new HashSet<PolAssignment>(0);*/

	public Policy() {
	}

	public Policy(String appid, CustomerDetails customerDetailsByPolicyOwner,
			MasterProductLine masterProductLine,
			AdviserStaff adviserStaffByAdviserInitial,
			AdviserStaff adviserStaffByAdviserCurrent,
			MasterPrincipal masterPrincipal,
			AdviserStaff adviserStaffByAdviserComm,
			CustomerDetails customerDetailsByCustidProposed,
			CustomerDetails customerDetailsByCustidAssured,
			MasterPolicyStatus masterPolicyStatus,
			FpmsDistributor fpmsDistributor, String polType, String policyno,
			String createdBy, Date createdDate) {
		this.appid = appid;
		this.customerDetailsByPolicyOwner = customerDetailsByPolicyOwner;
		this.masterProductLine = masterProductLine;
		this.adviserStaffByAdviserInitial = adviserStaffByAdviserInitial;
		this.adviserStaffByAdviserCurrent = adviserStaffByAdviserCurrent;
		this.masterPrincipal = masterPrincipal;
		this.adviserStaffByAdviserComm = adviserStaffByAdviserComm;
		this.customerDetailsByCustidProposed = customerDetailsByCustidProposed;
		this.customerDetailsByCustidAssured = customerDetailsByCustidAssured;
		this.masterPolicyStatus = masterPolicyStatus;
		this.fpmsDistributor = fpmsDistributor;
		this.polType = polType;
		this.policyno = policyno;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
	}

	public Policy(String appid, CustomerDetails customerDetailsByPolicyOwner,
			MasterProductLine masterProductLine,
			AdviserStaff adviserStaffByAdviserInitial,
			AdviserStaff adviserStaffByAdviserCurrent,
			MasterPrincipal masterPrincipal,
			AdviserStaff adviserStaffByAdviserComm,
			CustomerDetails customerDetailsByCustidProposed,
			CustomerDetails customerDetailsByCustidAssured,
			MasterPolicyStatus masterPolicyStatus,
			FpmsDistributor fpmsDistributor, String polType, String policyno,
			String prevPolNo, Date registrationDate, Date issueDate,
			Date effDate, Date renewalDate, Date renewalReminderDate,
			Date submDate, Date termDate, Date endDate, Date counterStopDate,
			Date polStatusDate, Date prevPolnoChngeDate, String spTopupFlg,
			String cpfFlg, String gstFlg, String autoTranfFlg,
			Short autoTranfAge, String paymentMode, String paymentMethod,
			String currencyType, String cpfAccNo, Byte giroFirstDeductDate,
			Byte giroSecondDeductDate, String termsAndCond, String remarks,
			String createdBy, Date createdDate, String modifiedBy,
			String distributorName, Double totalSa, Double totalPrem,
			String polCorAddr1, String polCorAddr2, String polCorAddr3,
			String polCorCity, String polCorState, String polCorCountry,
			String polCorPostCde, String polPlnName, Date docDispatchDate,
			String tempAppidFpms06Dm, String sourceData, String avivaSplit,
			String ntucPolicyId, Date polModifiedDate,String ntucCaseId/*,
			Set<PolAdviserShare> polAdviserShares,
			Set<PolWillstrustsPlanDets> polWillstrustsPlanDetses,
			Set<PolInslifebasic> polInslifebasics,
			Set<PolBenefDets> polBenefDetses, Set<PolIlptopup> polIlptopups,
			Set<PolAlterations> polAlterationses,
			Set<PolAttachments> polAttachmentses,
			Set<CommScreeningDets> commScreeningDetses,
			Set<PolLoanDets> polLoanDetses, Set<PolIlpfunds> polIlpfundses,
			Set<PolMedTest> polMedTests,
			Set<PolWillstrustsDets> polWillstrustsDetses,
			Set<CommstmtRiders> commstmtRiderses,
			Set<PolTransfer> polTransfers,
			Set<CommAmmendments> commAmmendmentses,
			Set<DocHandling> docHandlings,
			Set<PolInsliferider> polInsliferiders,
			Set<PolLobDistDets> polLobDistDetses,
			Set<PolTrusteeDets> polTrusteeDetses,
			Set<PolPaymentTracking> polPaymentTrackings,
			Set<Commstmt> commstmts, Set<PolUnderwrite> polUnderwrites,
			Set<PolLandbanking> polLandbankings,
			Set<PolAssignment> polAssignments*/) {
		this.appid = appid;
		this.customerDetailsByPolicyOwner = customerDetailsByPolicyOwner;
		this.masterProductLine = masterProductLine;
		this.adviserStaffByAdviserInitial = adviserStaffByAdviserInitial;
		this.adviserStaffByAdviserCurrent = adviserStaffByAdviserCurrent;
		this.masterPrincipal = masterPrincipal;
		this.adviserStaffByAdviserComm = adviserStaffByAdviserComm;
		this.customerDetailsByCustidProposed = customerDetailsByCustidProposed;
		this.customerDetailsByCustidAssured = customerDetailsByCustidAssured;
		this.masterPolicyStatus = masterPolicyStatus;
		this.fpmsDistributor = fpmsDistributor;
		this.polType = polType;
		this.policyno = policyno;
		this.prevPolNo = prevPolNo;
		this.registrationDate = registrationDate;
		this.issueDate = issueDate;
		this.effDate = effDate;
		this.renewalDate = renewalDate;
		this.renewalReminderDate = renewalReminderDate;
		this.submDate = submDate;
		this.termDate = termDate;
		this.endDate = endDate;
		this.counterStopDate = counterStopDate;
		this.polStatusDate = polStatusDate;
		this.prevPolnoChngeDate = prevPolnoChngeDate;
		this.spTopupFlg = spTopupFlg;
		this.cpfFlg = cpfFlg;
		this.gstFlg = gstFlg;
		this.autoTranfFlg = autoTranfFlg;
		this.autoTranfAge = autoTranfAge;
		this.paymentMode = paymentMode;
		this.paymentMethod = paymentMethod;
		this.currencyType = currencyType;
		this.cpfAccNo = cpfAccNo;
		this.giroFirstDeductDate = giroFirstDeductDate;
		this.giroSecondDeductDate = giroSecondDeductDate;
		this.termsAndCond = termsAndCond;
		this.remarks = remarks;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.modifiedBy = modifiedBy;
		this.distributorName = distributorName;
		this.totalSa = totalSa;
		this.totalPrem = totalPrem;
		this.polCorAddr1 = polCorAddr1;
		this.polCorAddr2 = polCorAddr2;
		this.polCorAddr3 = polCorAddr3;
		this.polCorCity = polCorCity;
		this.polCorState = polCorState;
		this.polCorCountry = polCorCountry;
		this.polCorPostCde = polCorPostCde;
		this.polPlnName = polPlnName;
		this.docDispatchDate = docDispatchDate;
		this.tempAppidFpms06Dm = tempAppidFpms06Dm;
		this.sourceData = sourceData;
		this.avivaSplit = avivaSplit;
		this.ntucPolicyId = ntucPolicyId;
		this.polModifiedDate = polModifiedDate;
		this.ntucCaseId=ntucCaseId;
		/*this.polAdviserShares = polAdviserShares;
		this.polWillstrustsPlanDetses = polWillstrustsPlanDetses;
		this.polInslifebasics = polInslifebasics;
		this.polBenefDetses = polBenefDetses;
		this.polIlptopups = polIlptopups;
		this.polAlterationses = polAlterationses;
		this.polAttachmentses = polAttachmentses;
		this.commScreeningDetses = commScreeningDetses;
		this.polLoanDetses = polLoanDetses;
		this.polIlpfundses = polIlpfundses;
		this.polMedTests = polMedTests;
		this.polWillstrustsDetses = polWillstrustsDetses;
		this.commstmtRiderses = commstmtRiderses;
		this.polTransfers = polTransfers;
		this.commAmmendmentses = commAmmendmentses;
		this.docHandlings = docHandlings;
		this.polInsliferiders = polInsliferiders;
		this.polLobDistDetses = polLobDistDetses;
		this.polTrusteeDetses = polTrusteeDetses;
		this.polPaymentTrackings = polPaymentTrackings;
		this.commstmts = commstmts;
		this.polUnderwrites = polUnderwrites;
		this.polLandbankings = polLandbankings;
		this.polAssignments = polAssignments;*/
	}

	public String getAppid() {
		return this.appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public CustomerDetails getCustomerDetailsByPolicyOwner() {
		return this.customerDetailsByPolicyOwner;
	}

	public void setCustomerDetailsByPolicyOwner(
			CustomerDetails customerDetailsByPolicyOwner) {
		this.customerDetailsByPolicyOwner = customerDetailsByPolicyOwner;
	}

	public MasterProductLine getMasterProductLine() {
		return this.masterProductLine;
	}

	public void setMasterProductLine(MasterProductLine masterProductLine) {
		this.masterProductLine = masterProductLine;
	}

	public AdviserStaff getAdviserStaffByAdviserInitial() {
		return this.adviserStaffByAdviserInitial;
	}

	public void setAdviserStaffByAdviserInitial(
			AdviserStaff adviserStaffByAdviserInitial) {
		this.adviserStaffByAdviserInitial = adviserStaffByAdviserInitial;
	}

	public AdviserStaff getAdviserStaffByAdviserCurrent() {
		return this.adviserStaffByAdviserCurrent;
	}

	public void setAdviserStaffByAdviserCurrent(
			AdviserStaff adviserStaffByAdviserCurrent) {
		this.adviserStaffByAdviserCurrent = adviserStaffByAdviserCurrent;
	}

	public MasterPrincipal getMasterPrincipal() {
		return this.masterPrincipal;
	}

	public void setMasterPrincipal(MasterPrincipal masterPrincipal) {
		this.masterPrincipal = masterPrincipal;
	}

	public AdviserStaff getAdviserStaffByAdviserComm() {
		return this.adviserStaffByAdviserComm;
	}

	public void setAdviserStaffByAdviserComm(
			AdviserStaff adviserStaffByAdviserComm) {
		this.adviserStaffByAdviserComm = adviserStaffByAdviserComm;
	}

	public CustomerDetails getCustomerDetailsByCustidProposed() {
		return this.customerDetailsByCustidProposed;
	}

	public void setCustomerDetailsByCustidProposed(
			CustomerDetails customerDetailsByCustidProposed) {
		this.customerDetailsByCustidProposed = customerDetailsByCustidProposed;
	}

	public CustomerDetails getCustomerDetailsByCustidAssured() {
		return this.customerDetailsByCustidAssured;
	}

	public void setCustomerDetailsByCustidAssured(
			CustomerDetails customerDetailsByCustidAssured) {
		this.customerDetailsByCustidAssured = customerDetailsByCustidAssured;
	}

	public MasterPolicyStatus getMasterPolicyStatus() {
		return this.masterPolicyStatus;
	}

	public void setMasterPolicyStatus(MasterPolicyStatus masterPolicyStatus) {
		this.masterPolicyStatus = masterPolicyStatus;
	}

	public FpmsDistributor getFpmsDistributor() {
		return this.fpmsDistributor;
	}

	public void setFpmsDistributor(FpmsDistributor fpmsDistributor) {
		this.fpmsDistributor = fpmsDistributor;
	}

	public String getPolType() {
		return this.polType;
	}

	public void setPolType(String polType) {
		this.polType = polType;
	}

	public String getPolicyno() {
		return this.policyno;
	}

	public void setPolicyno(String policyno) {
		this.policyno = policyno;
	}

	public String getPrevPolNo() {
		return this.prevPolNo;
	}

	public void setPrevPolNo(String prevPolNo) {
		this.prevPolNo = prevPolNo;
	}

	public Date getRegistrationDate() {
		return this.registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public Date getIssueDate() {
		return this.issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public Date getEffDate() {
		return this.effDate;
	}

	public void setEffDate(Date effDate) {
		this.effDate = effDate;
	}

	public Date getRenewalDate() {
		return this.renewalDate;
	}

	public void setRenewalDate(Date renewalDate) {
		this.renewalDate = renewalDate;
	}

	public Date getRenewalReminderDate() {
		return this.renewalReminderDate;
	}

	public void setRenewalReminderDate(Date renewalReminderDate) {
		this.renewalReminderDate = renewalReminderDate;
	}

	public Date getSubmDate() {
		return this.submDate;
	}

	public void setSubmDate(Date submDate) {
		this.submDate = submDate;
	}

	public Date getTermDate() {
		return this.termDate;
	}

	public void setTermDate(Date termDate) {
		this.termDate = termDate;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getCounterStopDate() {
		return this.counterStopDate;
	}

	public void setCounterStopDate(Date counterStopDate) {
		this.counterStopDate = counterStopDate;
	}

	public Date getPolStatusDate() {
		return this.polStatusDate;
	}

	public void setPolStatusDate(Date polStatusDate) {
		this.polStatusDate = polStatusDate;
	}

	public Date getPrevPolnoChngeDate() {
		return this.prevPolnoChngeDate;
	}

	public void setPrevPolnoChngeDate(Date prevPolnoChngeDate) {
		this.prevPolnoChngeDate = prevPolnoChngeDate;
	}

	public String getSpTopupFlg() {
		return this.spTopupFlg;
	}

	public void setSpTopupFlg(String spTopupFlg) {
		this.spTopupFlg = spTopupFlg;
	}

	public String getCpfFlg() {
		return this.cpfFlg;
	}

	public void setCpfFlg(String cpfFlg) {
		this.cpfFlg = cpfFlg;
	}

	public String getGstFlg() {
		return this.gstFlg;
	}

	public void setGstFlg(String gstFlg) {
		this.gstFlg = gstFlg;
	}

	public String getAutoTranfFlg() {
		return this.autoTranfFlg;
	}

	public void setAutoTranfFlg(String autoTranfFlg) {
		this.autoTranfFlg = autoTranfFlg;
	}

	public Short getAutoTranfAge() {
		return this.autoTranfAge;
	}

	public void setAutoTranfAge(Short autoTranfAge) {
		this.autoTranfAge = autoTranfAge;
	}

	public String getPaymentMode() {
		return this.paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getPaymentMethod() {
		return this.paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getCurrencyType() {
		return this.currencyType;
	}

	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}

	public String getCpfAccNo() {
		return this.cpfAccNo;
	}

	public void setCpfAccNo(String cpfAccNo) {
		this.cpfAccNo = cpfAccNo;
	}

	public Byte getGiroFirstDeductDate() {
		return this.giroFirstDeductDate;
	}

	public void setGiroFirstDeductDate(Byte giroFirstDeductDate) {
		this.giroFirstDeductDate = giroFirstDeductDate;
	}

	public Byte getGiroSecondDeductDate() {
		return this.giroSecondDeductDate;
	}

	public void setGiroSecondDeductDate(Byte giroSecondDeductDate) {
		this.giroSecondDeductDate = giroSecondDeductDate;
	}

	public String getTermsAndCond() {
		return this.termsAndCond;
	}

	public void setTermsAndCond(String termsAndCond) {
		this.termsAndCond = termsAndCond;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getDistributorName() {
		return this.distributorName;
	}

	public void setDistributorName(String distributorName) {
		this.distributorName = distributorName;
	}

	public Double getTotalSa() {
		return this.totalSa;
	}

	public void setTotalSa(Double totalSa) {
		this.totalSa = totalSa;
	}

	public Double getTotalPrem() {
		return this.totalPrem;
	}

	public void setTotalPrem(Double totalPrem) {
		this.totalPrem = totalPrem;
	}

	public String getPolCorAddr1() {
		return this.polCorAddr1;
	}

	public void setPolCorAddr1(String polCorAddr1) {
		this.polCorAddr1 = polCorAddr1;
	}

	public String getPolCorAddr2() {
		return this.polCorAddr2;
	}

	public void setPolCorAddr2(String polCorAddr2) {
		this.polCorAddr2 = polCorAddr2;
	}

	public String getPolCorAddr3() {
		return this.polCorAddr3;
	}

	public void setPolCorAddr3(String polCorAddr3) {
		this.polCorAddr3 = polCorAddr3;
	}

	public String getPolCorCity() {
		return this.polCorCity;
	}

	public void setPolCorCity(String polCorCity) {
		this.polCorCity = polCorCity;
	}

	public String getPolCorState() {
		return this.polCorState;
	}

	public void setPolCorState(String polCorState) {
		this.polCorState = polCorState;
	}

	public String getPolCorCountry() {
		return this.polCorCountry;
	}

	public void setPolCorCountry(String polCorCountry) {
		this.polCorCountry = polCorCountry;
	}

	public String getPolCorPostCde() {
		return this.polCorPostCde;
	}

	public void setPolCorPostCde(String polCorPostCde) {
		this.polCorPostCde = polCorPostCde;
	}

	public String getPolPlnName() {
		return this.polPlnName;
	}

	public void setPolPlnName(String polPlnName) {
		this.polPlnName = polPlnName;
	}

	public Date getDocDispatchDate() {
		return this.docDispatchDate;
	}

	public void setDocDispatchDate(Date docDispatchDate) {
		this.docDispatchDate = docDispatchDate;
	}

	public String getTempAppidFpms06Dm() {
		return this.tempAppidFpms06Dm;
	}

	public void setTempAppidFpms06Dm(String tempAppidFpms06Dm) {
		this.tempAppidFpms06Dm = tempAppidFpms06Dm;
	}

	public String getSourceData() {
		return this.sourceData;
	}

	public void setSourceData(String sourceData) {
		this.sourceData = sourceData;
	}

	public String getAvivaSplit() {
		return this.avivaSplit;
	}

	public void setAvivaSplit(String avivaSplit) {
		this.avivaSplit = avivaSplit;
	}

	public String getNtucPolicyId() {
		return this.ntucPolicyId;
	}

	public void setNtucPolicyId(String ntucPolicyId) {
		this.ntucPolicyId = ntucPolicyId;
	}

	public Date getPolModifiedDate() {
		return this.polModifiedDate;
	}

	public void setPolModifiedDate(Date polModifiedDate) {
		this.polModifiedDate = polModifiedDate;
	}

	public String getNtucCaseId() {
		return ntucCaseId;
	}

	public void setNtucCaseId(String ntucCaseId) {
		this.ntucCaseId = ntucCaseId;
	}

	/*public Set<PolAdviserShare> getPolAdviserShares() {
		return this.polAdviserShares;
	}

	public void setPolAdviserShares(Set<PolAdviserShare> polAdviserShares) {
		this.polAdviserShares = polAdviserShares;
	}

	public Set<PolWillstrustsPlanDets> getPolWillstrustsPlanDetses() {
		return this.polWillstrustsPlanDetses;
	}

	public void setPolWillstrustsPlanDetses(
			Set<PolWillstrustsPlanDets> polWillstrustsPlanDetses) {
		this.polWillstrustsPlanDetses = polWillstrustsPlanDetses;
	}

	public Set<PolInslifebasic> getPolInslifebasics() {
		return this.polInslifebasics;
	}

	public void setPolInslifebasics(Set<PolInslifebasic> polInslifebasics) {
		this.polInslifebasics = polInslifebasics;
	}

	public Set<PolBenefDets> getPolBenefDetses() {
		return this.polBenefDetses;
	}

	public void setPolBenefDetses(Set<PolBenefDets> polBenefDetses) {
		this.polBenefDetses = polBenefDetses;
	}

	public Set<PolIlptopup> getPolIlptopups() {
		return this.polIlptopups;
	}

	public void setPolIlptopups(Set<PolIlptopup> polIlptopups) {
		this.polIlptopups = polIlptopups;
	}

	public Set<PolAlterations> getPolAlterationses() {
		return this.polAlterationses;
	}

	public void setPolAlterationses(Set<PolAlterations> polAlterationses) {
		this.polAlterationses = polAlterationses;
	}

	public Set<PolAttachments> getPolAttachmentses() {
		return this.polAttachmentses;
	}

	public void setPolAttachmentses(Set<PolAttachments> polAttachmentses) {
		this.polAttachmentses = polAttachmentses;
	}

	public Set<CommScreeningDets> getCommScreeningDetses() {
		return this.commScreeningDetses;
	}

	public void setCommScreeningDetses(
			Set<CommScreeningDets> commScreeningDetses) {
		this.commScreeningDetses = commScreeningDetses;
	}

	public Set<PolLoanDets> getPolLoanDetses() {
		return this.polLoanDetses;
	}

	public void setPolLoanDetses(Set<PolLoanDets> polLoanDetses) {
		this.polLoanDetses = polLoanDetses;
	}

	public Set<PolIlpfunds> getPolIlpfundses() {
		return this.polIlpfundses;
	}

	public void setPolIlpfundses(Set<PolIlpfunds> polIlpfundses) {
		this.polIlpfundses = polIlpfundses;
	}

	public Set<PolMedTest> getPolMedTests() {
		return this.polMedTests;
	}

	public void setPolMedTests(Set<PolMedTest> polMedTests) {
		this.polMedTests = polMedTests;
	}

	public Set<PolWillstrustsDets> getPolWillstrustsDetses() {
		return this.polWillstrustsDetses;
	}

	public void setPolWillstrustsDetses(
			Set<PolWillstrustsDets> polWillstrustsDetses) {
		this.polWillstrustsDetses = polWillstrustsDetses;
	}

	public Set<CommstmtRiders> getCommstmtRiderses() {
		return this.commstmtRiderses;
	}

	public void setCommstmtRiderses(Set<CommstmtRiders> commstmtRiderses) {
		this.commstmtRiderses = commstmtRiderses;
	}

	public Set<PolTransfer> getPolTransfers() {
		return this.polTransfers;
	}

	public void setPolTransfers(Set<PolTransfer> polTransfers) {
		this.polTransfers = polTransfers;
	}

	public Set<CommAmmendments> getCommAmmendmentses() {
		return this.commAmmendmentses;
	}

	public void setCommAmmendmentses(Set<CommAmmendments> commAmmendmentses) {
		this.commAmmendmentses = commAmmendmentses;
	}

	public Set<DocHandling> getDocHandlings() {
		return this.docHandlings;
	}

	public void setDocHandlings(Set<DocHandling> docHandlings) {
		this.docHandlings = docHandlings;
	}

	public Set<PolInsliferider> getPolInsliferiders() {
		return this.polInsliferiders;
	}

	public void setPolInsliferiders(Set<PolInsliferider> polInsliferiders) {
		this.polInsliferiders = polInsliferiders;
	}

	public Set<PolLobDistDets> getPolLobDistDetses() {
		return this.polLobDistDetses;
	}

	public void setPolLobDistDetses(Set<PolLobDistDets> polLobDistDetses) {
		this.polLobDistDetses = polLobDistDetses;
	}

	public Set<PolTrusteeDets> getPolTrusteeDetses() {
		return this.polTrusteeDetses;
	}

	public void setPolTrusteeDetses(Set<PolTrusteeDets> polTrusteeDetses) {
		this.polTrusteeDetses = polTrusteeDetses;
	}

	public Set<PolPaymentTracking> getPolPaymentTrackings() {
		return this.polPaymentTrackings;
	}

	public void setPolPaymentTrackings(
			Set<PolPaymentTracking> polPaymentTrackings) {
		this.polPaymentTrackings = polPaymentTrackings;
	}

	public Set<Commstmt> getCommstmts() {
		return this.commstmts;
	}

	public void setCommstmts(Set<Commstmt> commstmts) {
		this.commstmts = commstmts;
	}

	public Set<PolUnderwrite> getPolUnderwrites() {
		return this.polUnderwrites;
	}

	public void setPolUnderwrites(Set<PolUnderwrite> polUnderwrites) {
		this.polUnderwrites = polUnderwrites;
	}

	public Set<PolLandbanking> getPolLandbankings() {
		return this.polLandbankings;
	}

	public void setPolLandbankings(Set<PolLandbanking> polLandbankings) {
		this.polLandbankings = polLandbankings;
	}

	public Set<PolAssignment> getPolAssignments() {
		return this.polAssignments;
	}

	public void setPolAssignments(Set<PolAssignment> polAssignments) {
		this.polAssignments = polAssignments;
	}*/

}
