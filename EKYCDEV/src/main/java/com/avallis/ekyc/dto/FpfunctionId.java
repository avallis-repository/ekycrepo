package com.avallis.ekyc.dto;
// Generated Dec 23, 2014 3:18:02 AM by Hibernate Tools 3.2.0.CR1



/**
 * FpfunctionId generated by hbm2java
 */
public class FpfunctionId  implements java.io.Serializable {


     private String txtFldFunctid;
     private String txtFldScreenid;

    public FpfunctionId() {
    }

    public FpfunctionId(String txtFldFunctid, String txtFldScreenid) {
       this.txtFldFunctid = txtFldFunctid;
       this.txtFldScreenid = txtFldScreenid;
    }
   
    public String getTxtFldFunctid() {
        return this.txtFldFunctid;
    }
    
    public void setTxtFldFunctid(String txtFldFunctid) {
        this.txtFldFunctid = txtFldFunctid;
    }
    public String getTxtFldScreenid() {
        return this.txtFldScreenid;
    }
    
    public void setTxtFldScreenid(String txtFldScreenid) {
        this.txtFldScreenid = txtFldScreenid;
    }




}


