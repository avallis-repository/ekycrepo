package com.avallis.ekyc.services;

import java.io.IOException;
import java.util.List;

import org.springframework.context.ApplicationContext;

import com.avallis.ekyc.db.FNADb;
import com.avallis.ekyc.db.KycDb;
import com.avallis.ekyc.db.SignatureDb;
import com.avallis.ekyc.dbinterfaces.DBIntf;
import com.avallis.ekyc.dto.FnaSignature;
import com.avallis.ekyc.utils.ApplicationContextUtils;
import com.avallis.ekyc.utils.KycConst;



public class FnaSignatureService {

	public void saveSignature(FnaSignature sign) {
		ApplicationContext ctx = ApplicationContextUtils
				.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);
		SignatureDb signDb = new SignatureDb();
		
		signDb.insertData(dbDTO, sign);
	}
	
	
	public void clearSignature(FnaSignature sign) {
		ApplicationContext ctx = ApplicationContextUtils
				.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);
		SignatureDb signDb = new SignatureDb();
		
		signDb.clearSignature(dbDTO, sign);
	}
	
	
	
	public void clearSignByFnaId(String strFnaId) {
		ApplicationContext ctx = ApplicationContextUtils
				.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);
		SignatureDb signDb = new SignatureDb();
		
		signDb.clearSignByFnaId(dbDTO, strFnaId);
	}
	
	public List<FnaSignature> getAllData(String fnaId) throws IOException {
		ApplicationContext ctx = ApplicationContextUtils
				.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);
		SignatureDb signDb = new SignatureDb();
		
		return (List<FnaSignature>) signDb.getAllDataById(dbDTO, fnaId);
	}
	

	public List getClientSignAllCol(DBIntf dbDTO,String strFNAId,String strSignFor,String strPageRef ){
		SignatureDb signDb = new SignatureDb();
		return signDb.getClientSignAllCol(dbDTO,strFNAId,strSignFor,strPageRef);
	}
	

	public List getClientSign(DBIntf dbDTO,String strFNAId,String strSignFor,String strPageRef ){
		SignatureDb db = new SignatureDb();
		return db.getClientSign(dbDTO,strFNAId,strSignFor,strPageRef);
	}
	
//	public List<FnaSignature> getAllData(FnaSignature sign) {
//		ApplicationContext ctx = ApplicationContextUtils
//				.getApplicationContext();
//		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);
//		SignatureDb signDb = new SignatureDb();
//		
//		signDb.insertData(dbDTO, sign);
//	}
}
