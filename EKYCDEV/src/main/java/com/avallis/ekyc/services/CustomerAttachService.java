package com.avallis.ekyc.services;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.avallis.ekyc.db.CustomerAttachDB;
import com.avallis.ekyc.dbinterfaces.DBIntf;
import com.avallis.ekyc.dto.CustomerAttachments;
import com.avallis.ekyc.restservice.CustomerAttachmentsRest;
import com.avallis.ekyc.utils.ApplicationContextUtils;
import com.avallis.ekyc.utils.EKYCQuery;
import com.avallis.ekyc.utils.KycConst;
import com.avallis.ekyc.utils.KycUtils;

public class CustomerAttachService {
	
	static Logger kyclog = Logger.getLogger(CustomerAttachmentsRest.class.getName());
	
	public static String formatAttachId(String strScreen, String prefix, int padding, DBIntf dbDTO) {

		String strAttachId = KycConst.STR_NULL_STRING;

		try {

			if (strScreen.equalsIgnoreCase(KycConst.GLBL_MODULE_CLNT)) {

				strAttachId = prefix + dbDTO.fetchMaxSeqVal(EKYCQuery.STR_SQL_CUSTATTACH_SEQ_NOPREFIX);
			} else if (strScreen.equalsIgnoreCase(KycConst.GLBL_MODULE_AGNT)) {
				strAttachId = prefix + dbDTO.fetchMaxSeqVal(EKYCQuery.STR_SQL_ADVATTACH_SEQ_NOPREFIX);
			} else if (strScreen.equalsIgnoreCase(KycConst.GLBL_MODULE_POLICY)) {
				strAttachId = prefix + dbDTO.fetchMaxSeqVal(EKYCQuery.STR_SQL_POLATTACH_SEQ_NOPREFIX);
			}

		} catch (Exception ex) {
			kyclog.error("Error in formatAttachId ",ex);
		}
		return strAttachId;
	}
	
	public List<CustomerAttachments> getCustAttachByCustId(String strCustId) {
		// List CatList;
		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		List<CustomerAttachments> custDocsList = new ArrayList<CustomerAttachments>();
		CustomerAttachDB ekycdb = new CustomerAttachDB();
		custDocsList = ekycdb.getCustAttachByCustId(dbDTO, strCustId);
		return custDocsList;
	}
	
	public List<CustomerAttachments> getCustAttachByCustIdFnaId(String strCustId, String strFNAId) {
		// List CatList;
		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		List<CustomerAttachments> custDocsList = new ArrayList<CustomerAttachments>();
		CustomerAttachDB ekycdb = new CustomerAttachDB();
		custDocsList = ekycdb.getCustAttachByCustIdFnaId(dbDTO, strCustId, strFNAId);
		return custDocsList;
	}
	
	 public ResultSet downloadNRICAttachments(DBIntf dbDTO, String strClntId,String strDistId,WebApplicationContext contexts,String strFNAID){
		 CustomerAttachDB db = new CustomerAttachDB();
			return db.downloadNRICAttachments(dbDTO, strClntId,strDistId,contexts,strFNAID);
		}
	
	public String insertCustAttForFnaAdmin(DBIntf dbDTO,String strCustId,String strGenFileName,String strAtmtFileSize,
			String strAtmtCreatedBy,String custdir,String strBFILEName,String strDistId,String strFnaId){
		CustomerAttachDB db = new CustomerAttachDB();
		return db.insertCustAttForFnaAdmin(dbDTO,strCustId,strGenFileName,strAtmtFileSize,strAtmtCreatedBy,custdir,strBFILEName,strDistId,strFnaId);
	}
	
	public void uploadCustDocs(CommonsMultipartFile[] fileList, JSONObject[] customerAttachments,
			HttpServletRequest req) throws IOException {

		ResourceBundle resource = ResourceBundle.getBundle("AppResource");

		Map<String, JSONObject> paramMap = new HashMap<String, JSONObject>();
		for (JSONObject map : customerAttachments) {

			// JSONObject json = objectToJSONObject(map);
			paramMap.put((String) map.get("txtFldCustAttachFilename"), map);
		}

		List<String> attachList = new ArrayList<String>();

		HttpSession session = req.getSession(false);
		Map<String, String> sessMap = (Map<String, String>) session.getAttribute(KycConst.LOGGED_USER_INFO);

		String strFNAId = (String) session.getAttribute("CURRENT_FNAID");
		String strCustId = (String) session.getAttribute("CURRENT_CUSTID");
		String strCustName = (String) session.getAttribute("CURRENT_CUSTNAME");
		strCustName = KycUtils.checkNullVal(strCustName) ? "" : strCustName.replace("\'", "\''");

		String strAdvId = sessMap.get(KycConst.LOGGED_USER_ADVSTFID);
		String strAdvName = sessMap.get(KycConst.LOGGED_USER_ADVSTFNAME);
		String strUserId = sessMap.get(KycConst.LOGGED_USER_ID);
		String strDistId = sessMap.get(KycConst.LOGGED_DIST_ID);
		String strDistName = sessMap.get(KycConst.LOGGED_DIST_NAME);

		String regexchars = resource.getString("attach.bfile.SplCharVal");
		String custdir = resource.getString("attach.bfile.clnt.dir");
		String cuatAttachFolder = resource.getString("attach.bfile.desitMachine.drive.folder");
		String custDestFolder = resource.getString("attach.bfile.desitMachine");

		String strCustDir = custDestFolder + "/" + cuatAttachFolder + "/";

		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		String strAtmtId = null;

		for (CommonsMultipartFile file : fileList) {

			byte[] filedata = file.getBytes();

			String strAtmtFileName = file.getOriginalFilename();
			String strAtmtFileType = "";
			String strAtmtFileSize = Integer.toString(filedata.length);
			String strAtmtTitle = "", strAtmtRemarks = "", strAtmtAttachCateg = "", strScreen = "", strRef = "",
					strAtmtAttachCategName = "", strAttachFor = "", strAttachInsurer;
			JSONObject json = (JSONObject) paramMap.get(strAtmtFileName);
			strAtmtRemarks = json.getString("txtFldCustAttachRemarks");
			strAtmtAttachCateg = json.has("selCustAttachMasterAttachCateg")
					? json.getString("selCustAttachMasterAttachCateg")
					: "";
			strAtmtTitle = json.getString("selCustAttachTitle");
			strAtmtAttachCategName = json.getString("txtFldCustAttachAttachCategName");
			strScreen = json.getString("screenFrom");
			strAttachInsurer = json.getString("insurarName");

			// if(strScreen.equalsIgnoreCase("false")) {
			strRef = strFNAId;
			// }

			strAdvName = KycUtils.replaceSplChars(regexchars, strAdvName);
			strCustName = KycUtils.replaceSplChars(regexchars, strCustName);
			strAtmtFileName = KycUtils.formatFileName(strAtmtFileName);

			String strBFILEName = strAdvName + File.separator + strCustName + File.separator + strAtmtFileName;
			strBFILEName = strBFILEName.replace("\'", "\''");
			strAtmtRemarks = strAtmtRemarks.replace("\'", "\''");

			File srcFile = KycUtils.createPhysicalDir(KycConst.GLBL_MODULE_CLNT, strAdvName, strCustName,
					strAtmtFileName);
			String retMsg = KycUtils.createPhysicalFile(filedata, srcFile);

			strAtmtTitle = KycUtils.nullOrBlank(strAtmtTitle) ? "-NIL-" : strAtmtTitle;

			if (KycUtils.nullOrBlank(retMsg)) {

				if (strAtmtFileType.length() > 40) {
					strAtmtFileType = "document/text";
				}
				
				strAtmtFileType=file.getContentType();

				strAtmtId = formatAttachId(KycConst.GLBL_MODULE_CLNT, "CUSTATT", 11, dbDTO);

				String ATTCH_INSERT_QRY = "INSERT INTO CUSTOMER_ATTACHMENTS"
						+ "(CUST_ATTACH_ID,CUSTID,TITLE,PAGE_NUM,REMARKS,FILENAME,FILESIZE,FILETYPE,ATTACH_CATEG_ID,"
						+ "ATTACH_CATEG_NAME,CREATED_BY,CREATED_DATE,MODIFIED_BY,MODIFIED_DATE,DISTRIBUTOR_ID,DISTRIBUTOR_NAME,DOCUMENT,ATTACH_FOR,FNA_ID,INSURAR_NAME) "
						+ " VALUES('" + strAtmtId + "','" + strCustId + "','" + strAtmtTitle + "',null," + "'"
						+ strAtmtRemarks + "','" + strAtmtFileName + "','" + strAtmtFileSize + "','" + strAtmtFileType
						+ "'," + "'" + strAtmtAttachCateg + "','" + strAtmtAttachCategName + "','" + strUserId
						+ "',SYSDATE," + "'','','" + strDistId + "','" + strDistName + "',BFILENAME('" + custdir + "','"
						+ strBFILEName + "'),'" + strAttachFor.toUpperCase() + "','" + strRef + "','" + strAttachInsurer
						+ "')";
				

				attachList.add(ATTCH_INSERT_QRY);

			}

		}

		CustomerAttachDB db = new CustomerAttachDB();
		db.insertCustAttach(dbDTO, attachList);

	}

	
	
	public void deleteCustAttachemtDetails(String custAttachId) {
		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);

		CustomerAttachDB db = new CustomerAttachDB();
		db.deleteCustAttachemtDetls(dbDTO, custAttachId);
	}
	
	 
	 
	

}
