package com.avallis.ekyc.services;

import java.util.List;

import com.avallis.ekyc.dbinterfaces.DBIntf;
import com.avallis.ekyc.db.KycDb;


public class KycService {
	
	public List  getFnaArchDets(DBIntf dto, String strFnaId) {
		KycDb db = new KycDb();
		return db.getFnaArchDets(dto, strFnaId);
	}

	public List getFnaArchDepntDets(DBIntf dto, String strFnaId) {
		KycDb db = new KycDb();
		return db.getFnaArchDepntDets(dto, strFnaId);
	}
	
	public List getFnaArchAnnlExptDet(DBIntf dto, String strFnaId) {		
		KycDb db = new KycDb();
		return db.getFnaArchAnnlExptDet(dto, strFnaId);
	}
	public List getFnaArchOthPersDet(DBIntf dto, String strFnaId) {
		KycDb db = new KycDb();
		return db.getFnaArchOthPersDet(dto, strFnaId);
	}
	public List getFnaArchRecProdPlanDet(DBIntf dto, String strFnaId) {
		KycDb db = new KycDb();
		return db.getFnaArchRecProdPlanDet(dto, strFnaId);
	}
	public List getFnaArchRecFundDet(DBIntf dto, String strFnaId) {
		KycDb db = new KycDb();
		return db.getFnaArchRecFundDet(dto, strFnaId);
	}
	
	public List getFnaFatcaTaxDet(DBIntf dto, String strFnaId) {
		KycDb db = new KycDb();
		return db.getFnaFatcaTaxDet(dto, strFnaId);
	}
	
	public List getFnaArchSwRepPlanDet(DBIntf dto, String strFnaId) {
		KycDb db = new KycDb();
		return db.getFnaArchSwRepPlanDet(dto, strFnaId);
	}
	
	public List getFnaArchSwRepFundDet(DBIntf dto, String strFnaId) {
		KycDb db = new KycDb();
		return db.getFnaArchSwRepFundDet(dto, strFnaId);
	}
	
}
