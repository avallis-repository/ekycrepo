package com.avallis.ekyc.services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.avallis.ekyc.db.FpmsDB;
import com.avallis.ekyc.dto.AdviserStaff;
import com.avallis.ekyc.dto.FPMSLabelValueDTO;
import com.avallis.ekyc.dto.Login;
import com.avallis.ekyc.dto.Loginprvlg;
import com.avallis.ekyc.dto.MasterAttachCateg;
import com.avallis.ekyc.dto.MasterCustomerStatus;
import com.avallis.ekyc.dto.MasterDesignation;
import com.avallis.ekyc.dto.MasterPrincipal;
import com.avallis.ekyc.utils.KycConst;
import com.avallis.ekyc.utils.KycUtils;

public class FpmsService {
	
	ResourceBundle resource = ResourceBundle.getBundle("AppResource");

	public boolean chkValidDB() {
		FpmsDB db = new FpmsDB();
		return db.chkValidDB();

	}

	public List<MasterPrincipal> populatePrincipal() {
		FpmsDB db = new FpmsDB();
		return db.populatePrincipal();
	}

	public List validateLogin(String... userparams) {
		FpmsDB db = new FpmsDB();
		return db.validateLogin(userparams);
	}


	public List<Login> getuserRole(String strUserId) {
		FpmsDB db = new FpmsDB();
		return db.getuserRole(strUserId);
	}

	public List<AdviserStaff> getGloblAcessList(String strloggedAdvStaffId, String strloggedUserStaffType,
			String strloggedUsrDistId) {
		FpmsDB db = new FpmsDB();
		return db.getGloblAcessList(strloggedAdvStaffId, strloggedUserStaffType, strloggedUsrDistId);
	}

	public List srchGlblAccessLevel(String strAdvid) {
		FpmsDB db = new FpmsDB();
		return db.srchGlblAcessLvlDB(strAdvid);
	}
	
	
	 public List getGlobalAccessLvl(HttpServletRequest request) {
	    	HttpSession fpsess = request.getSession(false);
	    	 ArrayList lstAccessLvl = new ArrayList();
	    	 
	    	 Map<String,String> sessMap = (Map<String, String>) fpsess.getAttribute(KycConst.LOGGED_USER_INFO);
			 
//	        String strLoggedUser =	KycUtils.getCommonSessValue(request, KycConst.LOGGED_USER_ID) ;
	        
	        String strAgentStaffId = KycUtils.getCommonSessValue(request, KycConst.LOGGED_USER_ADVSTFID) ;
	        
//	        String strAgentStaffName =	KycUtils.getCommonSessValue(request, KycConst.LOGGED_USER_ADVSTFNAME) ;
	        
	        String strAgentStaffTyp = KycUtils.getCommonSessValue(request, KycConst.LOGGED_USER_STFTYPE) ;
	        
	        String strAgtStfCmpAccess = KycUtils.getCommonSessValue(request, KycConst.AGENT_REG_COMP_ACCESS) ;
	       
	        //int accessFlg=0;
	        FpmsService aserv = new FpmsService();
	        if(strAgentStaffTyp.equalsIgnoreCase("ADVISER")){
	        	
	        List advStfElem = aserv.srchGlblAccessLevel(strAgentStaffId);
	    	int accesLvlSize=advStfElem.size();
	    	String accessLvl=KycConst.STR_NULL_STRING;
	    	String mgrAccessFlg=KycConst.STR_NULL_STRING;
	    	String mgrQryVal=KycConst.STR_NULL_STRING;
	    	//fplog.info("ACCESS LEVL SIZE "+accesLvlSize);
	    	if(accesLvlSize>0){
	    		Iterator itr = advStfElem.iterator();
	    		 while (itr.hasNext()) {
	    			  Object[] row = (Object[])itr.next();
	    			  mgrAccessFlg=(String)row[0];
	    			 // fplog.info("mgrAccessFlgsss"+mgrAccessFlg);
	    			  accessLvl=(String)row[1];
	    		 }
	    	}
	   		//fplog.info("Acces level Object "+accessLvl);
	    	if(!KycUtils.nullOrBlank(accessLvl)){
	    	    String strArrMngrAcess[] =accessLvl.split(",");
	            int mngrLen = strArrMngrAcess.length;
	            for (int mngr = 0; mngr<mngrLen; mngr++) {
	            //	fplog.info("strArrMngrAcess[mngr]"+strArrMngrAcess[mngr]);
	                FPMSLabelValueDTO fpDto = new FPMSLabelValueDTO(strArrMngrAcess[mngr],strArrMngrAcess[mngr]);
	                lstAccessLvl.add(fpDto);
	    	 }
	       }
	    	
//	    	System.out.println(mgrAccessFlg+"mgrAccessFlg");
	    	if(!KycUtils.nullOrBlank(mgrAccessFlg)){
	    		if(mgrAccessFlg.equalsIgnoreCase("Y")){
	    			mgrQryVal=" Select ADVISERSTAFFID from ADVSTF_MANAGER CONNECT BY PRIOR ADVISERSTAFFID = MANAGER_ID START WITH ADVISERSTAFFID='"+strAgentStaffId+"' ";	
	    		}else{
	    		//	fplog.info("mgrAccessFlg"+mgrAccessFlg);
	    			mgrQryVal=" where brok.txtFldadvstfId='" + strAgentStaffId + "' ";
	    		}
	    		fpsess.setAttribute("GLBL_MGR_QRY", mgrQryVal);	
	    	}
	    }
	    if(strAgentStaffTyp.equalsIgnoreCase("STAFF")){
	    	String strcmpAccess = resource.getString("appln.glblAccessLvl.comp");
	    	FPMSLabelValueDTO fpDto = new FPMSLabelValueDTO(strcmpAccess,strcmpAccess);
	    	lstAccessLvl.add(fpDto);
	    }
	    if(!(strAgentStaffTyp.equalsIgnoreCase("STAFF")||strAgentStaffTyp.equalsIgnoreCase("ADVISER"))){
	    	//fplog.info(" INSIDE INTRODUCER OR PARAPLANNER ");
	    	String strAgntStfAccess = resource.getString("appln.glblAccessLvl.agnt");
	    	FPMSLabelValueDTO fpDto = new FPMSLabelValueDTO(strAgntStfAccess,strAgntStfAccess);
	    	lstAccessLvl.add(fpDto);
	    }
	    if(strAgentStaffTyp.equalsIgnoreCase("ADVISER")){
	    	//if((strAgtStfCmpAccess.equalsIgnoreCase("View.Change")||(strAgtStfCmpAccess.equalsIgnoreCase("View.Only")))&&(!(strAgentStaffTyp.equalsIgnoreCase("STAFF")))){
	    if((strAgtStfCmpAccess.equalsIgnoreCase("View.Change")||(strAgtStfCmpAccess.equalsIgnoreCase("View.Only")))){
	    	//fplog.info(" INSIDE COMPANY LEVEL ACCESS ");
	    	String strcmpAccess = resource.getString("appln.glblAccessLvl.comp");
	    	FPMSLabelValueDTO fpDto = new FPMSLabelValueDTO(strcmpAccess,strcmpAccess);
	    	lstAccessLvl.add(fpDto);
	    }
	    } 
	 	return lstAccessLvl;
	  }// End getAccessLvl

	public List<Loginprvlg> getLoginPrvlg(String strUserId) {
		FpmsDB db = new FpmsDB();
		return db.getLoginPrvlg(strUserId);
	}

	public List<MasterCustomerStatus> getMastCustStatus() {
		FpmsDB db = new FpmsDB();
		return db.getMastCustStatus();
	}

	public List<MasterAttachCateg> getattachCategList() {
		List<MasterAttachCateg> CatList = new ArrayList<MasterAttachCateg>();
		FpmsDB db = new FpmsDB();
		CatList = db.getattachCategList();
		return CatList;
	}

	public List<MasterDesignation> getGloblAcessListCombo(String strAdvid) {
		FpmsDB db = new FpmsDB();
		return db.getGloblAcessListCombo(strAdvid);
	}

}
