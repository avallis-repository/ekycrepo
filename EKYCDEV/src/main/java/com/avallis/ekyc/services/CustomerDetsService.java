package com.avallis.ekyc.services;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.context.ApplicationContext;

import com.avallis.ekyc.db.CustomerDetsDB;
import com.avallis.ekyc.db.FpmsDB;
import com.avallis.ekyc.dbinterfaces.DBIntf;
import com.avallis.ekyc.dto.AdviserStaff;
import com.avallis.ekyc.dto.CustomerDetails;
import com.avallis.ekyc.dto.FpmsDistributor;
import com.avallis.ekyc.dto.MasterCustomerStatus;
import com.avallis.ekyc.utils.ApplicationContextUtils;
import com.avallis.ekyc.utils.KycConst;
import com.avallis.ekyc.utils.KycUtils;

public class CustomerDetsService {
	
	public List<CustomerDetails> getAllClientsByAdvId(String strAdvId,String strClntName){
		CustomerDetsDB db = new CustomerDetsDB();
		
		return db.getAllClientsByAdvId(strAdvId,strClntName);
			
		}
	
	public List getCustDets(String strCustId, String strFormType,String strLoggAdvId) {

		CustomerDetsDB db = new CustomerDetsDB();
		return db.getCustDets(strCustId, strFormType,strLoggAdvId);
	}
	//get All Nric List of Client
	
	public  List<CustomerDetails> getClientNricLists(String strClntNric,String strCustId,String strAdvId){
		CustomerDetsDB db = new CustomerDetsDB();
		
		return db.getClientNricLists(strClntNric,strCustId,strAdvId);
			
		}
		
	public List getLatestFpmsCustData(DBIntf dbDTO,String strFnaId){
		CustomerDetsDB db = new CustomerDetsDB();
			return db.getLatestFpmsCustData(dbDTO, strFnaId);
		}
		public CustomerDetails getClientsByCustId(String strCustId){
			CustomerDetsDB db = new CustomerDetsDB();
			
			return db.getClientsByCustId(strCustId);
				
			}
		public CustomerDetails insertCompanyClient(CustomerDetails custCompDAO,HttpServletRequest request){
			
			HttpSession session = request.getSession(false);
			Map<String,String> sessMap = new HashMap<String,String>();	
			
//			session.removeAttribute(KycConst.CURRENT_CUSTID);
//			session.removeAttribute(KycConst.CURRENT_CUSTNAME);
			
			sessMap = (Map<String, String>) session.getAttribute(KycConst.LOGGED_USER_INFO);
			
			String strAdvId = "";//sessMap.get(KycConst.LOGGED_USER_ADVSTFID);
			String strUserId = sessMap.get(KycConst.LOGGED_USER_ID);
			String strDistId = sessMap.get(KycConst.LOGGED_DIST_ID);
			String strDistName = sessMap.get(KycConst.LOGGED_DIST_NAME);
			String strStfType = sessMap.get(KycConst.LOGGED_USER_STFTYPE);
			
			String strCustId = custCompDAO.getCustid();
			String strCustName = KycConst.STR_NULL_STRING;
			
//			if(!strStfType.equalsIgnoreCase("ADVISER")) {
			
				strAdvId = custCompDAO.getCurrAdviserId();
//			}
			
			
			
			AdviserStaff adviserStaffByAgentIdInitial = new AdviserStaff();
			adviserStaffByAgentIdInitial.setAdvstfId(strAdvId);
			custCompDAO.setAdviserStaffByAgentIdInitial(adviserStaffByAgentIdInitial);
			
			AdviserStaff adviserStaffByAgentIdCurrent = new AdviserStaff();
			adviserStaffByAgentIdCurrent.setAdvstfId(strAdvId);
			custCompDAO.setAdviserStaffByAgentIdCurrent(adviserStaffByAgentIdCurrent);
			
			FpmsDistributor fpmsDistributor= new FpmsDistributor();
			fpmsDistributor.setDistributorId(strDistId);
			custCompDAO.setFpmsDistributor(fpmsDistributor);
			custCompDAO.setDistributorName(strDistName);
			
			MasterCustomerStatus masterCustomerStatus = new MasterCustomerStatus();
			masterCustomerStatus.setCustomerStatusId("STA003");//??		
			custCompDAO.setMasterCustomerStatus(masterCustomerStatus);
			
			custCompDAO.setFnatype("SIMPLIFIED");
			custCompDAO.setContactPref("R");
			custCompDAO.setAddressPref("R");
			
			ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
			DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);
			CustomerDetsDB db = new CustomerDetsDB();	
			
			
			if(KycUtils.nullOrBlank(strCustId)){
				
				custCompDAO.setCreatedBy(strUserId);
				custCompDAO.setCreatedDate(new Date());
				
				strCustId = db.insertCompDBClient(dbDTO,custCompDAO);
			}else{
				
//				custCompDAO.setCreatedBy(strUserId);
//				custCompDAO.setCreatedDate(new Date());
				custCompDAO.setModifiedBy(strUserId);
				custCompDAO.setModifiedDate(new Date());
				db.updateCompDBClient(dbDTO, custCompDAO);
			}
			
			strCustName = custCompDAO.getCustName();
			
				
			CustomerDetails custDets = getClientsByCustId(strCustId);		
			session.setAttribute(KycConst.CURRENT_CUSTID, strCustId);
			session.setAttribute(KycConst.CURRENT_CUSTNAME, strCustName);
			
			return custDets;
		}

}
