package com.avallis.ekyc.restservice;



import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Produces;

import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.avallis.ekyc.db.FNADb;
import com.avallis.ekyc.dbinterfaces.DBIntf;
import com.avallis.ekyc.dto.FnaRecomPrdtplanDet;
import com.avallis.ekyc.dto.FnaSwtchrepPlanDet;
import com.avallis.ekyc.dto.MasterProduct;
import com.avallis.ekyc.services.FNAService;
import com.avallis.ekyc.utils.ApplicationContextUtils;
import com.avallis.ekyc.utils.KycConst;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/prodswtchReplrecomm")
public class FnaRecomPrdtplanSwtchReplaceDetRest {
	
	
	
	@GetMapping(value="/prodRecomSumDetls")
	public String allRecommendation(HttpServletRequest request) throws JsonProcessingException{
		
		
		HttpSession session = request.getSession(false);
		String strFNAId = (String) session.getAttribute("CURRENT_FNAID");
		
		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);
		
		FNADb ekycdb = new FNADb();
		
		
		List<FnaSwtchrepPlanDet> swtchrepProdLists = ekycdb.allFNASwtchrepProdPlan(dbDTO, strFNAId);
		
		String strswtchrepProdLists = "";
		if(swtchrepProdLists.size() > 0) {
			ObjectMapper mapper = new ObjectMapper();
			strswtchrepProdLists = mapper.writeValueAsString(swtchrepProdLists);
			
		}else{
			
			ObjectMapper mapper = new ObjectMapper();
			strswtchrepProdLists = mapper.writeValueAsString(swtchrepProdLists);
		}
		return strswtchrepProdLists;
		
	}
	
	
	@GetMapping(value="/products/prinname/{PRINNAME}")
	public List<MasterProduct> productsByPrinName(@PathVariable("PRINNAME") String strPrinName){
		
		FNAService service = new FNAService();
		return service.productsByPrinName(strPrinName);
		
	}
	
	
	
	@PostMapping(value="/add/{FNAID}")	
	public FnaSwtchrepPlanDet addProducts(@RequestBody FnaSwtchrepPlanDet swtchrep,@PathVariable("FNAID") String strFNAId,HttpServletRequest request){
		
		
		HttpSession session = request.getSession(false);
		Map<String,String> sessMap = new HashMap<String,String>();	
		sessMap = (Map<String, String>) session.getAttribute(KycConst.LOGGED_USER_INFO);
		

		String strUserId = sessMap.get(KycConst.LOGGED_USER_ID);

		
		FNAService service = new FNAService();
		swtchrep.setCreatedBy(strUserId);
		swtchrep.setCreatedDate(new Date());
		
		FnaSwtchrepPlanDet swrepPpId = service.addSwtchrepPlanDets(swtchrep,strFNAId);
		
		return swrepPpId;
		
	}
	
	@PostMapping(value="/update/{SWREPPPID}")	
	public String updateProducts(@RequestBody FnaSwtchrepPlanDet swtchrep,@PathVariable("SWREPPPID") String strswrepPpId,HttpServletRequest request){
		
		HttpSession session = request.getSession(false);
		String strFNAId = (String) session.getAttribute("CURRENT_FNAID");
		FNAService service = new FNAService();
		service.updateFnaSwtchrepPlanDet(swtchrep,strswrepPpId,strFNAId);
		
		JSONObject prodUpdate = new JSONObject();
		prodUpdate.put("Prod_UPDATE_STATUS", "Successfully Updated");
		
		return prodUpdate.toString();
		
	}
	
	@PostMapping(value="/deleteRider/{SWREPPPID}")	
	public void deleteRiderPlansofSwitchReplace(@PathVariable("SWREPPPID") String strswrepPpId,HttpServletRequest request){
		
		FNAService service = new FNAService();
		service.deleteRiderProdPlanofSwitchReplace(strswrepPpId);
		
	}
	
	@PostMapping(value="/deleteBasicPlnSet/{RECOMPPPRODVAL}")	
	public void deleteBasicPlansofSwitchReplace(@PathVariable("RECOMPPPRODVAL") String strRecomPpProdName,HttpServletRequest request){
		
		HttpSession session = request.getSession(false);
		String strFNAId = (String) session.getAttribute("CURRENT_FNAID");
		FNAService service = new FNAService();
		service.deleteBasicProdPlanofSwitchReplace(strRecomPpProdName,strFNAId);
		
	}
	
	
	
	@RequestMapping(value="/deleteBscPlnByRecommPpId/",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)	
	@ResponseBody
	public void deleteBasicPlnsBySwrepPpId(@RequestParam("recommPpId") String strRecomPpId,@RequestParam("prodVal") String strRecomPpProdName,HttpServletRequest request){
		HttpSession session = request.getSession(false);
		String strFNAId = (String) session.getAttribute("CURRENT_FNAID");
		FNAService service = new FNAService();
		service.deleteBasicPlnsBySwrepPpId(strRecomPpId,strRecomPpProdName,strFNAId);
		
	}
	
	@RequestMapping(value="/deletePrincipalByClntName/",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	//@PostMapping(value="/deletePrincipalByClntName/{RECOMPPPRIN}")	
	public void deletePrincipalByClntNameofSwitchReplace(@RequestParam("swrepPpName") String strswrepPpName,@RequestParam("swrepPpPrin") String strswrepPpPrin,HttpServletRequest request){
		
		HttpSession session = request.getSession(false);
		String strFNAId = (String) session.getAttribute("CURRENT_FNAID");
		FNAService service = new FNAService();
		service.deletePrincipalByClntNameofSwitchReplace(strswrepPpName,strswrepPpPrin,strFNAId);
		
	}
	
	
	
	@PostMapping(value="/deleteClient/{RECOMPPNAME}")	
	public void deleteProdRecomClntofSwitchReplace(@PathVariable("RECOMPPNAME") String strRecomPpName,HttpServletRequest request){
		
		HttpSession session = request.getSession(false);
		String strFNAId = (String) session.getAttribute("CURRENT_FNAID");
		FNAService service = new FNAService();
		service.deleteProdRecomClntProdPlanofSwitchReplace(strRecomPpName,strFNAId);
		
	}
	
	

}
