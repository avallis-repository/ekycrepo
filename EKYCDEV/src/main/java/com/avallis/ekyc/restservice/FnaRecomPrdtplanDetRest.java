package com.avallis.ekyc.restservice;



import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Produces;

import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.avallis.ekyc.db.FNADb;
import com.avallis.ekyc.dbinterfaces.DBIntf;
import com.avallis.ekyc.dto.FnaRecomPrdtplanDet;
import com.avallis.ekyc.dto.MasterProduct;
import com.avallis.ekyc.services.FNAService;
import com.avallis.ekyc.utils.ApplicationContextUtils;
import com.avallis.ekyc.utils.KycConst;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/productsrecommend")
public class FnaRecomPrdtplanDetRest {
	
	
	
	@GetMapping(value="/prodRecomSumDetls")
	public String allRecommendation(HttpServletRequest request) throws JsonProcessingException{
		
		
		HttpSession session = request.getSession(false);
		String strFNAId = (String) session.getAttribute("CURRENT_FNAID");
		
		ApplicationContext ctx = ApplicationContextUtils.getApplicationContext();
		DBIntf dbDTO = (DBIntf) ctx.getBean(KycConst.DB_BEAN);
		
		FNADb ekycdb = new FNADb();
		
		
		List<FnaRecomPrdtplanDet> prodRecommLists = ekycdb.allFNARecommProdPlan(dbDTO, strFNAId);
		//FNAService service = new FNAService();
		//List<FnaRecomPrdtplanDet> prodRecommList = service.getFNAProdRecomPlans(request,strFNAId);
		
		
		
		String strProdRecommList = "";
		if(prodRecommLists.size() > 0) {
			
			ObjectMapper mapper = new ObjectMapper();
			strProdRecommList = mapper.writeValueAsString(prodRecommLists);
			//request.setAttribute("PRODUCT_RECOM_PLAN_DETS", strProdRecommList);
		}else{
			
			ObjectMapper mapper = new ObjectMapper();
			strProdRecommList = mapper.writeValueAsString(prodRecommLists);
		}
		return strProdRecommList;
		
	}
	
	
	@GetMapping(value="/products/prinname/{PRINNAME}")
	public List<MasterProduct> productsByPrinName(@PathVariable("PRINNAME") String strPrinName){
		
		FNAService service = new FNAService();
		return service.productsByPrinName(strPrinName);
		
	}
	
	
	
	@PostMapping(value="/add/{FNAID}")	
	public FnaRecomPrdtplanDet addProducts(@RequestBody FnaRecomPrdtplanDet prod,@PathVariable("FNAID") String strFNAId,HttpServletRequest request){
		
		
		HttpSession session = request.getSession(false);
		Map<String,String> sessMap = new HashMap<String,String>();	
		sessMap = (Map<String, String>) session.getAttribute(KycConst.LOGGED_USER_INFO);
		
//		String strAdvId = sessMap.get(KycConst.LOGGED_USER_ADVSTFID);
		String strUserId = sessMap.get(KycConst.LOGGED_USER_ID);
//		String strDistId = sessMap.get(KycConst.LOGGED_DIST_ID);
//		String strDistName = sessMap.get(KycConst.LOGGED_DIST_NAME);
//		String strStfType = sessMap.get(KycConst.LOGGED_USER_STFTYPE);
		
		FNAService service = new FNAService();
		prod.setCreatedBy(strUserId);
		prod.setCreatedDate(new Date());
		
		FnaRecomPrdtplanDet strPPId = service.addRecomProdPlanDets(prod,strFNAId);
		
		//JSONObject keyJsn = new JSONObject();
		//keyJsn.put("recomm_pp_id", strPPId);
		
		return strPPId;
		
	}
	
	@PostMapping(value="/update/{RECOMPPID}")	
	public String updateProducts(@RequestBody FnaRecomPrdtplanDet prod,@PathVariable("RECOMPPID") String strRecomPpId,HttpServletRequest request){
		
		HttpSession session = request.getSession(false);
		String strFNAId = (String) session.getAttribute("CURRENT_FNAID");
		FNAService service = new FNAService();
		service.updateRecomProdPlanDets(prod,strRecomPpId,strFNAId);
		
		JSONObject prodUpdate = new JSONObject();
		prodUpdate.put("Prod_UPDATE_STATUS", "Successfully Updated");
		
		return prodUpdate.toString();
		
	}
	
	@PostMapping(value="/deleteRider/{RECOMPPID}")	
	public void deleteRiderPlans(@PathVariable("RECOMPPID") String strRecomPpId,HttpServletRequest request){
		
		FNAService service = new FNAService();
		service.deleteRiderProdPlan(strRecomPpId);
		
	}
	
	@PostMapping(value="/deleteBasicPlnSet/{RECOMPPPRODVAL}")	
	public void deleteBasicPlans(@PathVariable("RECOMPPPRODVAL") String strRecomPpProdName,HttpServletRequest request){
		
		HttpSession session = request.getSession(false);
		String strFNAId = (String) session.getAttribute("CURRENT_FNAID");
		FNAService service = new FNAService();
		service.deleteBasicProdPlan(strRecomPpProdName,strFNAId);
		
	}
	
	
	
	@RequestMapping(value="/deleteBscPlnByRecommPpId/",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)	
	@ResponseBody
	public void deleteBasicPlnsByRecommPpId(@RequestParam("recommPpId") String strRecomPpId,@RequestParam("prodVal") String strRecomPpProdName,HttpServletRequest request){
		HttpSession session = request.getSession(false);
		String strFNAId = (String) session.getAttribute("CURRENT_FNAID");
		FNAService service = new FNAService();
		service.deleteBasicPlnsByRecommPpId(strRecomPpId,strRecomPpProdName,strFNAId);
		
	}
	
	@RequestMapping(value="/deletePrincipalByClntName/",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	//@PostMapping(value="/deletePrincipalByClntName/{RECOMPPPRIN}")	
	public void deletePrincipalByClntName(@RequestParam("recommPpName") String strRecomPpName,@RequestParam("recomPpPrin") String strRecomPpPrin,HttpServletRequest request){
		
		HttpSession session = request.getSession(false);
		String strFNAId = (String) session.getAttribute("CURRENT_FNAID");
		FNAService service = new FNAService();
		service.deletePrincipalByClntName(strRecomPpName,strRecomPpPrin,strFNAId);
		
	}
	
	
	
	@PostMapping(value="/deleteClient/{RECOMPPNAME}")	
	public void deleteProdRecomClnt(@PathVariable("RECOMPPNAME") String strRecomPpName,HttpServletRequest request){
		
		HttpSession session = request.getSession(false);
		String strFNAId = (String) session.getAttribute("CURRENT_FNAID");
		FNAService service = new FNAService();
		service.deleteProdRecomClntProdPlan(strRecomPpName,strFNAId);
		
	}
	
	

}
