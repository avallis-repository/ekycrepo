package com.avallis.ekyc.controller;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.avallis.ekyc.dto.FnaDetails;
import com.avallis.ekyc.dto.Loginprvlg;
import com.avallis.ekyc.services.AdviserService;
import com.avallis.ekyc.services.FNAService;
import com.avallis.ekyc.services.FpmsService;
import com.avallis.ekyc.services.KycService;
import com.avallis.ekyc.utils.KycConst;
import com.avallis.ekyc.utils.KycUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping("/approval")
public class ApprovalController {
	
	@RequestMapping(value={"/manager","/admin","/compliance"})
	public ModelAndView managerApprovals(HttpServletRequest request) throws UnsupportedEncodingException {
		
		HttpSession fpsess = request.getSession();
		ModelAndView mv = new ModelAndView();
		
		StringBuffer uri = request.getRequestURL();		
		String strQry = (request.getQueryString());

		byte[] decoded = DatatypeConverter.parseBase64Binary(strQry);
		String params =new String(decoded, "UTF-8");
		String[] strParams = params.split("&");

		fpsess.removeAttribute("KYC_APPROVE_DISTID");
		fpsess.removeAttribute("KYC_APPROVE_ADVID");
		fpsess.removeAttribute("KYC_APPROVE_USERID");
		fpsess.removeAttribute("KYC_APPROVE_MGRACSFLG");
		fpsess.removeAttribute("KYC_APPROVE_FNAID");
		fpsess.removeAttribute("KYC_APPROVE_FLAG");
		
		String strAdvId = strParams[0] ;//request.getParameter("txtFldAdvId") != null ? request.getParameter("txtFldAdvId") : "";
		String strMgrAccsFlg = strParams[1];//request.getParameter("txtFldMgrAccsFlg") != null ? request.getParameter("txtFldMgrAccsFlg") : "";
		String strDistId = strParams[2];//request.getParameter("txtFldDistId") != null ? request.getParameter("txtFldDistId") : "";
		String strUserId = strParams[0];// request.getParameter("txtFldUserId") != null ? request.getParameter("txtFldUserId") : "";
		String strfnaId = strParams[3];//request.getParameter("txtFldFnaId") != null ? request.getParameter("txtFldFnaId") : "";
		String strFlag = strParams[4];
		
		fpsess.setAttribute("KYC_APPROVE_DISTID",strDistId);
		fpsess.setAttribute("KYC_APPROVE_ADVID",strAdvId);
//		fpsess.setAttribute("KYC_APPROVE_USERID",strUserId);
		fpsess.setAttribute("KYC_APPROVE_MGRACSFLG",strMgrAccsFlg);
		fpsess.setAttribute("KYC_APPROVE_FNAID",strfnaId);
		fpsess.setAttribute("KYC_APPROVE_FLAG",strFlag);
		
		mv.setViewName("login");
		return mv;
	}
	
	
	//vignesh add on 25-05-2021
	@RequestMapping(value={"/directmanager"})
	public ModelAndView directmanagerApprovals(HttpServletRequest request) throws UnsupportedEncodingException, JsonProcessingException {
		
		HttpSession fpsess = request.getSession(false);
		ModelAndView mv = new ModelAndView();
		Map<String,String> sessMap = new HashMap<String,String>();	
		String strQry = (request.getQueryString());
		
		ResourceBundle resource = ResourceBundle.getBundle("AppResource");

		byte[] decoded = DatatypeConverter.parseBase64Binary(strQry);
		String params =new String(decoded, "UTF-8");
		String[] strParams = params.split("&");


		
			String strAdvId = strParams[0] ;
			String mgrAcsFlg = strParams[1];
			String strDistId = strParams[2];
			String strSessFnaId = strParams[3];
			String strFlag = strParams[4];
			String strLoggedUser = strParams[5];
			
				JSONArray jsnSessArr = new JSONArray();
				JSONObject jsnSessMap= new JSONObject();
				
				
				
				FpmsService serv = new FpmsService();
				AdviserService  advserv = new AdviserService();
				 String  strEmailId="",strMgrAdvName= "",						
						strAdvName="",strAdvMgrId = "";
				 
				List advMgrList = advserv.getFNAAdvMgrDets(null, strSessFnaId);
				 Iterator fnaite = advMgrList.iterator();
				 while(fnaite.hasNext()){
					 FnaDetails fnaDets = (FnaDetails)fnaite.next();
					 strAdvName = KycUtils.nullOrBlank(fnaDets.getAdviserStaffByAdvstfId().getAdvstfName())?"":fnaDets.getAdviserStaffByAdvstfId().getAdvstfName();
					 strEmailId = KycUtils.checkNullVal(fnaDets.getAdviserStaffByAdvstfId())?"":fnaDets.getAdviserStaffByAdvstfId().getEmailId();
					 strMgrAdvName = KycUtils.checkNullVal(fnaDets.getAdviserStaffByMgrId()) ? "" : fnaDets.getAdviserStaffByMgrId().getAdvstfName();
					 strAdvMgrId = KycUtils.checkNullVal(fnaDets.getAdviserStaffByMgrId()) ? "" : fnaDets.getAdviserStaffByMgrId().getAdvstfId();
				 }
				 
				 
				 jsnSessMap.put("LOGGED_DISTID",strDistId);
					jsnSessMap.put("LOGGED_USER_EMAIL_ID",strEmailId);
					jsnSessMap.put("TODAY_DATE",KycUtils.formatDate(new Date()));
					
					jsnSessMap.put("MANAGER_ACCESSFLG",mgrAcsFlg);	
					sessMap.put("MANAGER_ACCESSFLG",mgrAcsFlg);
					
					jsnSessMap.put("LOGGED_ADVSTFID", strAdvId);
					sessMap.put("LOGGED_ADVSTFID", strAdvId);//new
					
					jsnSessMap.put("LOGGED_USERID", strLoggedUser);
					jsnSessMap.put("STR_LOGGEDUSER", strLoggedUser);
					jsnSessMap.put("LOGGED_USER", strLoggedUser);
					
					
					sessMap.put("KYC_APPROVE_USERID", strLoggedUser);//new
					sessMap.put("KYC_APPROVE_FNAID", strSessFnaId);//new
					
					jsnSessMap.put("RPT_FILE_LOC", resource.getString("fna.rpt.fileloc"));

				
				
				if(mgrAcsFlg.equalsIgnoreCase("Y")){
					
					Map<String,Integer> stsMap =new HashMap<String, Integer>();
					
					JSONArray jsnArrObj = KycUtils.getManagerFnaDetails(strAdvMgrId,strSessFnaId);
					
					JSONArray jsnArrALLObj = KycUtils.getManagerFnaDetails(strAdvMgrId,"");
					JSONObject jsonCountAllObj = jsnArrALLObj.getJSONObject(0);
					JSONArray jsnArrCountData = (JSONArray) jsonCountAllObj.get("MANAGER_FNA_DETAILS");
					
					stsMap = getStatusListCont(jsnArrCountData, "MANAGER");
					
					request.setAttribute("FNA_MGR_STATUS_LIST", stsMap);
					request.setAttribute("FNA_MGR_NOTUPDATED_DETS", jsnArrObj);				
					request.setAttribute("ALL_FNA_IDS", jsnArrALLObj);
					
					request.setAttribute("APPROVAL_SECTION_TITLE", "MANAGER");
					request.setAttribute("current_screen_load", strFlag);
					request.setAttribute("current_fna_id", strSessFnaId);
					fpsess.setAttribute("KYC_APPROVE_FNAID", strSessFnaId);
					
				}
				
				boolean compflg=false,adminflg=false,ntucpolacsflg=false,ntucsftpacsflg=false;
				
				
				
				jsnSessMap.put("NTUC_POLICY_ACCESS",String.valueOf(ntucpolacsflg));
				jsnSessMap.put("NTUC_SFTP_ACCESS",String.valueOf(ntucsftpacsflg));
				
				
				jsnSessArr.put(jsnSessMap);			
				fpsess.setAttribute("COMMON_SESS_OBJ", jsnSessArr.toString());
				
				FNAService fnaserv = new FNAService();
				FnaDetails fnaObj = fnaserv.findFnaLatestDetlsByFnaId(strSessFnaId);	
				ObjectMapper fnamapper = new ObjectMapper();
				String strFnaJson = fnamapper.writeValueAsString(fnaObj);
//				System.out.println(strFnaJson);
				
				JSONObject jsnFna = new JSONObject(strFnaJson);
				jsnFna.put("adviser_name",strAdvName);
				jsnFna.put("manager_name",strMgrAdvName);
				request.setAttribute("CURRENT_FNA_DETAILS", jsnFna.toString());
							 
		
		
		mv.setViewName("kycapproval");
		
		return mv;
	}
	
	//vignesh add on 25-05-2021
public static Map<String,Integer> getStatusListCont(JSONArray jsnArrCountData,String strWhom ){
		
		Map<String,Integer> stsMap =new HashMap<String, Integer>();
		
		int totalcnt = jsnArrCountData.length();
		
		for(int dat = 0 ;dat <totalcnt; dat++){
			
			JSONObject jsnData = (JSONObject)jsnArrCountData.get(dat);
			String strMgrSts = strWhom.equalsIgnoreCase("MANAGER") ? (String)jsnData.get("txtFldMgrApproveStatus") 
							 : strWhom.equalsIgnoreCase("ADMIN")? (String)jsnData.get("txtFldAdminApproveStatus") 
							 : (String)jsnData.get("txtFldCompApproveStatus");
			strMgrSts = KycUtils.nullOrBlank(strMgrSts) ? "PENDING" : strMgrSts;
			stsMap = KycUtils.incrementValue(stsMap, strMgrSts);
		}
		
		return stsMap;
	}

}
