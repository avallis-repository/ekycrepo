package com.avallis.ekyc.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.avallis.ekyc.dto.FnaDetails;
import com.avallis.ekyc.dto.FnaSelfspouseDets;
import com.avallis.ekyc.dto.Login;
import com.avallis.ekyc.dto.Loginprvlg;
import com.avallis.ekyc.services.AdviserService;
import com.avallis.ekyc.services.FNAService;
import com.avallis.ekyc.services.FpmsService;
import com.avallis.ekyc.utils.KycConst;
import com.avallis.ekyc.utils.KycUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class LoginController {
	
	
	static Logger kyclog = Logger.getLogger(LoginController.class.getName());
	
	@RequestMapping(value = {"/loginValidate","/approval/loginValidate"})
	public ModelAndView getAllClients(HttpServletRequest request) throws JsonProcessingException{
		
		
		ResourceBundle resource = ResourceBundle.getBundle("AppResource");
		ModelAndView mv = new ModelAndView();
		HttpSession session = request.getSession(false);
		
		if(!KycUtils.chkValidDB()){
			mv.setViewName("dbError");
			return mv;
		}
		
		
		if(!KycUtils.isValidUSession(request)){
			mv.setViewName("sessionExpired");
			return mv;
		}
		
		FpmsService fs = new FpmsService();
		AdviserService as = new AdviserService();
		
		Map<String,String> sessMap = new HashMap<String,String>();	 
		
		String strUid= KycUtils.getParamValue(request, "txtFldUserName");
		String strPwd=  KycUtils.getParamValue(request, "txtFldpassword");
		
		List<?> userList = fs.validateLogin(strUid,strPwd);
		
		int userListSize = userList.size();
		boolean isValidUser = false;
		String strAdvId = "",strAdviserName="";
		String strLogUserAdvId = "";
		String strLoggedUser="",mgrAcsFlg="",strDistributor="",strLogUserEmailId="",strStaffType="";
		
		String strFnaId ="";
		
		if(userListSize>0)isValidUser=true;
		
		if(isValidUser){
			
			kyclog.info("[IT] Logged User Details"+strUid+" at ->"+new Date());
			
			Iterator<?> ite = userList.iterator();
			
			while(ite.hasNext()){
				
//				FipaUser userDets = (FipaUser)ite.next();
				Object[] userDets = (Object[])ite.next();
				
				strAdvId = userDets[2].toString();
				strAdviserName = userDets[3].toString();
				strLogUserAdvId = userDets[2].toString();
				strLoggedUser = (String) userDets[0];
				mgrAcsFlg=(String) userDets[10];
				strDistributor = (String) userDets[12];
				strLogUserEmailId = (String) userDets[14];
				strStaffType = userDets[5].toString();
				
				session.setAttribute(KycConst.LOGGED_USER_ID,userDets[0]);
				session.setAttribute(KycConst.LOGGED_USER_ADVSTFINITIAL, userDets[4].toString());
				
				sessMap.put(KycConst.LOGGED_USER_ID, userDets[0].toString());		
				sessMap.put(KycConst.LOGGED_USER_ADVSTFID,strLogUserAdvId);
				sessMap.put(KycConst.LOGGED_USER_ADVSTFNAME, strAdviserName);
				sessMap.put(KycConst.FNA_ADVSTFNAME, userDets[3].toString());
				sessMap.put(KycConst.LOGGED_USER_ADVSTFINITIAL, userDets[4].toString());					
				sessMap.put(KycConst.LOGGED_USER_STFTYPE, strStaffType);
				sessMap.put(KycConst.LOGGED_USER_STFTYPE_MNEM, userDets[6].toString());
				sessMap.put(KycConst.LOGGED_USER_MGRADVSTFID,userDets[7].toString());
				sessMap.put(KycConst.LOGGED_USER_MGRADVSTFNAME,userDets[8].toString());
				sessMap.put(KycConst.FNA_ADVSTF_MGRNAME,userDets[8].toString());
				sessMap.put(KycConst.LOGGED_SYSDATE,KycUtils.formatDate((Date)userDets[9]));
				sessMap.put(KycConst.LOGGED_USER_MGRFLAG,userDets[10].toString());
				sessMap.put(KycConst.LOGGED_DESIGNATION,userDets[6].toString());
				sessMap.put(KycConst.LOGGED_DIST_ID,userDets[12].toString());
				sessMap.put(KycConst.LOGGED_DIST_NAME,userDets[13].toString());
				
			}	
			
			 List<Login> userRoles = fs.getuserRole(strLoggedUser);
			 int totalRoles =userRoles.size();
			 
			 if(totalRoles>0) {
				 for (Login loginObj : userRoles) {					 
					 sessMap.put(KycConst.AGENT_REG_COMP_ACCESS,KycUtils.checkNullVal(loginObj.getSelrole()) ? KycConst.STR_NULL_STRING: loginObj.getSelrole().getSelcompaccess());
	                } 
			 }
			 
			
			
			List<Loginprvlg> usr_prv_logn = fs.getLoginPrvlg(strLoggedUser);
		   // fplog.info("USER PRIVILEGES " + usr_prv_logn.size());
			sessMap.remove("GLBL_USR_PRVLG_LIST");
			List usrPrvlgList = new ArrayList();
			StringBuffer prvlgBuff = new StringBuffer();
			
			
			usrPrvlgList = getGlobalScreenPrivs(request, "KYC_APPROVAL",	usr_prv_logn);
			

			if (usrPrvlgList.size() > 0) {
				Iterator<String> iter = usrPrvlgList.iterator();
				
				while (iter.hasNext()) {
					String sUsrPrvl = iter.next();
					prvlgBuff.append(sUsrPrvl + ",");
				}
				sessMap.put("GLBL_USR_PRVLG_LIST", prvlgBuff.toString());
			}
			
			session.setAttribute(KycConst.LOGGED_USER_INFO, sessMap);
			
			
			setGlobalAccessLevel(request);
			
			JSONObject jsnParaAdvEmail = new JSONObject();
			JSONArray jsnParaAdvEmailArr = new JSONArray();
			
			if(strStaffType.equalsIgnoreCase("PARAPLANNER") || strStaffType.equalsIgnoreCase("INTRODUCER")){
				List paraList = new ArrayList();
				paraList = as.populateParaplannerDets(strAdvId,strDistributor);

				if(paraList.size()>0){
					Iterator paraite = paraList.iterator();

					while(paraite.hasNext()){
						jsnParaAdvEmail = new JSONObject();
						Object[] cols = (Object[])paraite.next();
						jsnParaAdvEmail.put("paraAdvStfId", KycUtils.getObjValue(cols[0]));
						jsnParaAdvEmail.put("paraAdvStfName", KycUtils.getObjValue(cols[1]));
						jsnParaAdvEmail.put("paraAdvStfEmailId", KycUtils.getObjValue(cols[2]));

						jsnParaAdvEmailArr.put(jsnParaAdvEmail);
					}
					
					session.setAttribute("PARA_ADV_EMAILID", jsnParaAdvEmailArr);
				}else {
					session.setAttribute("PARA_ADV_EMAILID", "[]");
				}

				
			}else {
				jsnParaAdvEmail.put("paraAdvStfId", KycUtils.getObjValue(strAdvId));
				jsnParaAdvEmail.put("paraAdvStfName", KycUtils.getObjValue(strAdviserName));
				jsnParaAdvEmail.put("paraAdvStfEmailId", KycUtils.getObjValue(strLogUserEmailId));
				jsnParaAdvEmailArr.put(jsnParaAdvEmail);
				session.setAttribute("PARA_ADV_EMAILID", jsnParaAdvEmailArr);
			}
			
			session.removeAttribute("KYC_APPROVE_DISTID");
			session.removeAttribute("KYC_APPROVE_ADVID");
			session.removeAttribute("KYC_APPROVE_USERID");
			session.removeAttribute("KYC_APPROVE_MGRACSFLG");
			session.removeAttribute("KYC_APPROVE_FNAID");
			
			String strDistId = KycUtils.getParamValue(request, "hTxtFldDistId");
//			String strSessAdvId = KycUtils.getParamValue(request, "hTxtFldAdvId");
//			String strSessMgrAcs = KycUtils.getParamValue(request, "hTxtFldMgrAcsFlg");
			String strSessFnaId = KycUtils.getParamValue(request, "hTxtFldFnaId");
			String strFlag = KycUtils.getParamValue(request, "hTxtFldFlag");
			
			session.setAttribute("KYC_APPROVE_DISTID",strDistId);
			session.setAttribute("KYC_APPROVE_ADVID",strAdvId);
//			session.setAttribute("KYC_APPROVE_USERID",strUserId);
			session.setAttribute("KYC_APPROVE_MGRACSFLG",mgrAcsFlg);
			session.setAttribute("KYC_APPROVE_FNAID",strSessFnaId);
			
			if(!KycUtils.nullOrBlank(strSessFnaId) && !KycUtils.nullOrBlank(strFlag)) {
				JSONArray jsnSessArr = new JSONArray();
				JSONObject jsnSessMap= new JSONObject();
				
				jsnSessMap.put("LOGGED_DISTID",strDistributor);
				jsnSessMap.put("LOGGED_USER_EMAIL_ID",strLogUserEmailId);
				jsnSessMap.put("TODAY_DATE",KycUtils.formatDate(new Date()));
				
				jsnSessMap.put("MANAGER_ACCESSFLG",mgrAcsFlg);	
				sessMap.put("MANAGER_ACCESSFLG",mgrAcsFlg);
				
				jsnSessMap.put("LOGGED_ADVSTFID", strLogUserAdvId);
				sessMap.put("LOGGED_ADVSTFID", strLogUserAdvId);//new
				
				jsnSessMap.put("LOGGED_USERID", strLoggedUser);
				jsnSessMap.put("STR_LOGGEDUSER", strLoggedUser);
				jsnSessMap.put("LOGGED_USER", strLoggedUser);
				
				
				sessMap.put("KYC_APPROVE_USERID", strLoggedUser);//new
				sessMap.put("KYC_APPROVE_FNAID", strFnaId);//new
				
				jsnSessMap.put("RPT_FILE_LOC", resource.getString("fna.rpt.fileloc"));
				
				
//				Map<String,Integer> stsMap =new HashMap<String, Integer>();
				
				FpmsService fpmsservice = new FpmsService();
				FNAService fnaService = new FNAService();
				String strEmailId=KycConst.STR_NULL_STRING,
						strMgrAdvName= KycConst.STR_NULL_STRING,
								strAdvName=KycConst.STR_NULL_STRING;
				
//				 String strMgrAdvStfId = "",
//						 strMgrEmailId ="",
//						strMgrAdvInitial="",
//						strCustId = "",strCustName = "";
//				
				List advMgrList = as.getFNAAdvMgrDets(null, strSessFnaId);
				 Iterator fnaite = advMgrList.iterator();
				 while(fnaite.hasNext()){
					 FnaDetails fnaDets = (FnaDetails)fnaite.next();
					 
					 strAdvName = KycUtils.nullOrBlank(fnaDets.getAdviserStaffByAdvstfId().getAdvstfName())?"":fnaDets.getAdviserStaffByAdvstfId().getAdvstfName();
					 strEmailId = KycUtils.checkNullVal(fnaDets.getAdviserStaffByAdvstfId())?"":fnaDets.getAdviserStaffByAdvstfId().getEmailId();
					 strMgrAdvName = KycUtils.checkNullVal(fnaDets.getAdviserStaffByMgrId()) ? "" : fnaDets.getAdviserStaffByMgrId().getAdvstfName();
					 
				 }
				 
				 FnaSelfspouseDets lstFnaSelfSps = fnaService.findFNASelfSpsDetailsByFnaId(strSessFnaId);
				String strSelfName =  KycUtils.checkNullVal(lstFnaSelfSps.getDfSelfName()) ? "" : lstFnaSelfSps.getDfSelfName();
				String strSpsName =  KycUtils.checkNullVal(lstFnaSelfSps.getDfSpsName()) ? null : lstFnaSelfSps.getDfSpsName();
				
				sessMap = (Map<String, String>) session.getAttribute(KycConst.LOGGED_USER_INFO);
				sessMap.put(KycConst.FNA_ADVSTFNAME, strAdvName);
				sessMap.put(KycConst.FNA_ADVSTF_MGRNAME,strMgrAdvName);
				sessMap.put(KycConst.FNA_SELF_NAME,strSelfName);
				sessMap.put(KycConst.FNA_SPOUSE_NAME,strSpsName);
				
				sessMap.put("CURR_ADVSTF_EMAIL", strEmailId);
				sessMap.put("CURR_ADVSTF_NAME", strAdvName);
				sessMap.put("CUST_NAME", strSelfName);
				sessMap.put("LOGGED_USERID", strLoggedUser);
				
				session.setAttribute(KycConst.LOGGED_USER_INFO, sessMap);
				request.setAttribute(KycConst.FNA_SELF_NAME, strSelfName);
				request.setAttribute(KycConst.FNA_SPOUSE_NAME, strSpsName);
				request.setAttribute("CURRENT_FNAID", strSessFnaId);
				
				jsnSessArr.put(jsnSessMap);			
				session.setAttribute("COMMON_SESS_OBJ", jsnSessArr);
				
				
				String[] accessLvls = prvlgBuff.toString().split(",");
				
				
				if(mgrAcsFlg.equalsIgnoreCase("Y")){
					
					Map<String,Integer> stsMap =new HashMap<String, Integer>();
					
					JSONArray jsnArrObj = KycUtils.getManagerFnaDetails(strLogUserAdvId,strFnaId);
					
					JSONArray jsnArrALLObj = KycUtils.getManagerFnaDetails(strLogUserAdvId,"");
					JSONObject jsonCountAllObj = jsnArrALLObj.getJSONObject(0);
					JSONArray jsnArrCountData = new JSONArray();
					
					if(jsonCountAllObj.has("MANAGER_FNA_DETAILS")) {
					
						jsnArrCountData = (JSONArray) jsonCountAllObj.get("MANAGER_FNA_DETAILS");
					}
					
					
					stsMap = getStatusList(jsnArrCountData, "MANAGER");
					
					request.setAttribute("FNA_MGR_STATUS_LIST", stsMap);
					request.setAttribute("FNA_MGR_NOTUPDATED_DETS", jsnArrObj);				
					request.setAttribute("ALL_FNA_IDS", jsnArrALLObj);
					
					request.setAttribute("APPROVAL_SECTION_TITLE", "MANAGER");
					
					
				}
				
				boolean compflg=false,adminflg=false,ntucpolacsflg=false,ntucsftpacsflg=false;
				
				for(int lvl=0;lvl<accessLvls.length;lvl++){
					
					if(!KycUtils.nullOrBlank(accessLvls[lvl])){
						if(accessLvls[lvl].equalsIgnoreCase(KycConst.COMPLIANCE_KYC_APPROVAL)){
							compflg=true;
						}
						
						if(accessLvls[lvl].equalsIgnoreCase(KycConst.ADMIN_KYC_APPROVAL)){
							adminflg=true;
						}
						
						if(accessLvls[lvl].equalsIgnoreCase(KycConst.NTUC_POLICY_ACCESS)){
							ntucpolacsflg=true;
						}
						if(accessLvls[lvl].equalsIgnoreCase(KycConst.NTUC_SFTP_ACCESS)){
							ntucsftpacsflg=true;
						}
					}
				}
				
				
				
				if(compflg && adminflg){    
					
					JSONArray jsnArrObj = KycUtils.getCompAndAdminFnaDets(strFnaId);
					request.setAttribute("COMPLIANCE_FNA_VERSION_DETS", jsnArrObj);
					
					jsnSessMap.put("COMPLIANCE_ACCESSFLG","Y");
					jsnSessMap.put("ADMIN_ACCESSFLG","Y");
					
					Map<String,Integer> stsMap =new HashMap<String, Integer>();
					
					JSONArray jsnArrALLObj = KycUtils.getCompAndAdminFnaDets("");
					JSONObject jsonCountAllObj = jsnArrALLObj.getJSONObject(0);
					
					JSONArray jsnArrCountData = new JSONArray();
					
					if(jsonCountAllObj.has("COMPLIANCE_FNA_DETAILS")) {
						jsnArrCountData = (JSONArray) jsonCountAllObj.get("COMPLIANCE_FNA_DETAILS");
					}
					
					stsMap = getStatusList(jsnArrCountData, "COMPLIANCE");
					
					request.setAttribute("FNA_MGR_STATUS_LIST", stsMap);
//					kycreq.setAttribute("FNA_MGR_NOTUPDATED_DETS", jsnArrObj);
					request.setAttribute("ALL_FNA_IDS", jsnArrALLObj);
					
					request.setAttribute("APPROVAL_SECTION_TITLE", "COMPANDADMIN");
					
				}
				
				
				if(compflg && !adminflg){
					
					
					JSONArray jsnArrObj = KycUtils.getComplianceFnaDetails(strFnaId);
					request.setAttribute("COMPLIANCE_FNA_VERSION_DETS", jsnArrObj);
					
					jsnSessMap.put("COMPLIANCE_ACCESSFLG","Y");
					jsnSessMap.put("ADMIN_ACCESSFLG","N");
					
					Map<String,Integer> stsMap =new HashMap<String, Integer>();
					
					JSONArray jsnArrALLObj = KycUtils.getComplianceFnaDetails("");
					JSONObject jsonCountAllObj = jsnArrALLObj.getJSONObject(0);
					JSONArray jsnArrCountData = new JSONArray();
					if(jsonCountAllObj.has("COMPLIANCE_FNA_DETAILS")) {
						jsnArrCountData = (JSONArray) jsonCountAllObj.get("COMPLIANCE_FNA_DETAILS");
					}
					
					stsMap = getStatusList(jsnArrCountData, "COMPLIANCE");
					
					request.setAttribute("FNA_MGR_STATUS_LIST", stsMap);
//					kycreq.setAttribute("FNA_MGR_NOTUPDATED_DETS", jsnArrObj);
					request.setAttribute("ALL_FNA_IDS", jsnArrALLObj);
					
					request.setAttribute("APPROVAL_SECTION_TITLE", "COMPLIANCE");
				}
				
				
				
				if(!compflg && adminflg){
					
					JSONArray jsnArrObj = KycUtils.getAdminFnaDetails(strFnaId);
					request.setAttribute("ADMIN_FNA_VERSION_DETS", jsnArrObj);
					
					jsnSessMap.put("ADMIN_ACCESSFLG","Y");
					jsnSessMap.put("COMPLIANCE_ACCESSFLG","N");
					
					Map<String,Integer> stsMap =new HashMap<String, Integer>();
					
					JSONArray jsnArrALLObj = KycUtils.getAdminFnaDetails("");
					JSONObject jsonCountAllObj = jsnArrALLObj.getJSONObject(0);
					JSONArray jsnArrCountData = new JSONArray();
					if(jsonCountAllObj.has("ADMIN_FNA_DETAILS")) {
						jsnArrCountData = (JSONArray) jsonCountAllObj.get("ADMIN_FNA_DETAILS");	
					}
					
					
					stsMap = getStatusList(jsnArrCountData, "ADMIN");
					
					request.setAttribute("FNA_MGR_STATUS_LIST", stsMap);
//					kycreq.setAttribute("FNA_MGR_NOTUPDATED_DETS", jsnArrObj);
					request.setAttribute("ALL_FNA_IDS", jsnArrALLObj);
					
					request.setAttribute("APPROVAL_SECTION_TITLE", "ADMIN");
				}
				
				
				if(!compflg && !adminflg){
					jsnSessMap.put("ADMIN_ACCESSFLG","N");
					jsnSessMap.put("COMPLIANCE_ACCESSFLG","N");
				}
				
				
				jsnSessMap.put("NTUC_POLICY_ACCESS",String.valueOf(ntucpolacsflg));
				jsnSessMap.put("NTUC_SFTP_ACCESS",String.valueOf(ntucsftpacsflg));
				
				
				jsnSessArr.put(jsnSessMap);			
				sessMap.put("COMMON_SESS_OBJ", jsnSessArr.toString());
				
				FNAService fnaserv = new FNAService();
				FnaDetails fnaObj = fnaserv.findFnaLatestDetlsByFnaId(strSessFnaId);	
				ObjectMapper fnamapper = new ObjectMapper();
				String strFnaJson = fnamapper.writeValueAsString(fnaObj);
				
				JSONObject jsnFna = new JSONObject(strFnaJson);
				jsnFna.put("adviser_name",strAdvName);
				jsnFna.put("manager_name",strMgrAdvName);
				request.setAttribute("CURRENT_FNA_DETAILS", jsnFna.toString());
				
				mv.setViewName("kycapproval");
				return mv;
				
			}else {
				mv.setViewName("index");	
				return mv;
			}
			
		}else{
//			Error page redirection
			request.setAttribute("error_message", "Invalid UserName or password!");
			mv.setViewName( "login");
			return mv;
		}
		
		
		
		
	}
	
	
	@RequestMapping(value = {"/logout","/relogin"})
	public String logout(HttpServletRequest request){
		HttpSession session = request.getSession(false);
		if(request.getSession(false) != null)
			session.invalidate();
		request.setAttribute("logout_message","Logged out!");
		return "";
	}
	

public static Map<String,Integer> getStatusList(JSONArray jsnArrCountData,String strWhom ){
		
		Map<String,Integer> stsMap =new HashMap<String, Integer>();
		
//		System.out.println("jsnArrCountData----------->"+jsnArrCountData);
		
		int totalcnt = jsnArrCountData.length();
		if(totalcnt > 0 ) {
		
			for(int dat = 0 ;dat <totalcnt; dat++){
				
				JSONObject jsnData = (JSONObject)jsnArrCountData.get(dat);
				
				if(!jsnData.has("NO_RECORDS_FOUND")) {
					
					String strMgrSts = strWhom.equalsIgnoreCase("MANAGER") ? (String)jsnData.get("txtFldMgrApproveStatus") 
							 : strWhom.equalsIgnoreCase("ADMIN")? (String)jsnData.get("txtFldAdminApproveStatus") 
							 : (String)jsnData.get("txtFldCompApproveStatus");
			
					strMgrSts = KycUtils.nullOrBlank(strMgrSts) ? "PENDING" : strMgrSts;
			
					stsMap = KycUtils.incrementValue(stsMap, strMgrSts);
			
				}
				
				
			}
			
		}
		
		
		
		return stsMap;
	}


public ArrayList getGlobalScreenPrivs(HttpServletRequest request, 
        String strScreenName,List<Loginprvlg> usr_prv_logn) {
	HttpSession fpsess = request.getSession(false);
	
	 
     List<Loginprvlg> elements = new ArrayList<Loginprvlg>();
     ArrayList<String> lstGlblUsrPriv = new ArrayList<String>();
      
     elements = usr_prv_logn; 
     for (Loginprvlg glblUsrPrivsObj : elements) {
    	 String strGlblUsrScrName = 
    			 KycUtils.convertObjToString(glblUsrPrivsObj.getSelScrfunction().getFpscreen().getTxtFldscreenname());
         String strGlblUsrFuncName = 
        		 KycUtils.convertObjToString(glblUsrPrivsObj.getSelScrfunction().getTxtFldfunctname());
         if (strScreenName.equalsIgnoreCase(strGlblUsrScrName)) {
             lstGlblUsrPriv.add(strGlblUsrFuncName.substring(0).toUpperCase());
     }
     }
     String strDefBtnOrder[] ="SEARCH,REGISTER,UPDATE,DELETE".split(",");
     int glblBtnlen = strDefBtnOrder.length;
     int glblPrivSize = lstGlblUsrPriv.size();
     if (glblPrivSize > 0) {
         int lstGlblCnt = 0;
         for (int glb = 0; glb < glblBtnlen; glb++) {
             if (lstGlblUsrPriv.contains(strDefBtnOrder[glb])) {
            	 lstGlblUsrPriv.remove(strDefBtnOrder[glb]);
            	 lstGlblUsrPriv.add(lstGlblCnt++, strDefBtnOrder[glb]);
             }
         } //end for{glb}
     } //(glblPrivSize > 0 )
     return lstGlblUsrPriv;
}

//get access level List

public void setGlobalAccessLevel(HttpServletRequest fpmsrequest ){
	List lstGlblAccessLvl = new ArrayList();
	FpmsService fs = new FpmsService();
	lstGlblAccessLvl= fs.getGlobalAccessLvl(fpmsrequest);
	
	
	
	HttpSession session = fpmsrequest.getSession(false);
	session.setAttribute("GLBL_ACCESS_LEVEL",lstGlblAccessLvl);
}

//vignesh 05-05-2021

//get Adviser Dashboard Related Details 
@SuppressWarnings("unchecked")
@RequestMapping(value = {"/viewDashBoardDetls"})
public ModelAndView getAdvDashboardDetls(HttpServletRequest request) throws JsonProcessingException{
	 
	ModelAndView mv = new ModelAndView();
	HttpSession session = request.getSession(false);
	Map<String,String> sessMap = new HashMap<String,String>();
	Map<String,Integer> stsMap = new HashMap<String, Integer>();
	FpmsService fs = new FpmsService();
	
	sessMap = (Map<String, String>) session.getAttribute(KycConst.LOGGED_USER_INFO);
	
	
	String strLogUserAdvId = sessMap.get(KycConst.LOGGED_USER_ADVSTFID);
	String strLogUserMgrFLG = sessMap.get("LOGGED_USER_MGRFLAG");
	
	session.setAttribute("KYC_APPROVE_MGRACSFLG",strLogUserMgrFLG);
	
		
		
		JSONArray jsnArrALLObj = KycUtils.getAdvtoMngrKycSendList(strLogUserAdvId);
		
		
		JSONObject jsonCountAllObj = jsnArrALLObj.getJSONObject(0);
		
		JSONArray jsnArrCountData = new JSONArray();
		
		if(jsonCountAllObj.has("MANAGER_FNA_DETAILS")) {
		
			jsnArrCountData = (JSONArray) jsonCountAllObj.get("MANAGER_FNA_DETAILS");
		}
		
		
				
		stsMap = getStatusList(jsnArrCountData, "MANAGER");
				
		request.setAttribute("FNA_MGR_STATUS_LIST", stsMap);
		request.setAttribute("ALL_FNA_IDS", jsnArrALLObj);
	 
		
		if(strLogUserMgrFLG.equalsIgnoreCase("Y")) {
			
			JSONArray jsnArrALLObjMgr = KycUtils.getManagerFnaDetails(strLogUserAdvId,"");
			JSONObject jsonCountAllObjMgr = jsnArrALLObjMgr.getJSONObject(0);
			JSONArray jsnArrCountDataMgr = (JSONArray) jsonCountAllObjMgr.get("MANAGER_FNA_DETAILS");
			
			stsMap = getStatusList(jsnArrCountDataMgr, "MANAGER");
			
			request.setAttribute("FNA_MGR_STATUS_LIST_DB", stsMap);
			request.setAttribute("ALL_FNA_IDS_DB", jsnArrALLObjMgr);
		}else {
			request.setAttribute("FNA_MGR_STATUS_LIST_DB", stsMap);
			request.setAttribute("ALL_FNA_IDS_DB", new JSONArray());	
		}
		
		

	mv.setViewName("advDashboardDetls");
		return mv;
}

//End



}
