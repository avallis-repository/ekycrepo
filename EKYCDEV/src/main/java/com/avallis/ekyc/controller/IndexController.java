package com.avallis.ekyc.controller;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import org.json.JSONArray;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.avallis.ekyc.dto.AdviserStaff;
import com.avallis.ekyc.dto.CustomerDetails;
import com.avallis.ekyc.dto.FPMSLabelValueDTO;
import com.avallis.ekyc.dto.FnaDetails;
import com.avallis.ekyc.dto.FnaExpenditureDets;
import com.avallis.ekyc.dto.FnaOthPersBenfDets;
import com.avallis.ekyc.dto.FnaOthPersPep;
import com.avallis.ekyc.dto.FnaOthPersTpp;
import com.avallis.ekyc.dto.FnaSelfspouseDets;
import com.avallis.ekyc.dto.MasterCustomerStatus;
import com.avallis.ekyc.services.AdviserService;
import com.avallis.ekyc.services.CustomerDetsService;
import com.avallis.ekyc.services.FNAService;
import com.avallis.ekyc.services.FpmsService;
import com.avallis.ekyc.services.KycService;
import com.avallis.ekyc.utils.KycConst;
import com.avallis.ekyc.utils.KycUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class IndexController {

	@RequestMapping("/")
	public String login(HttpServletRequest request) {
		
		HttpSession session = request.getSession();
		session.removeAttribute("CURRENT_DATAFORM_ID");
		session.removeAttribute(KycConst.FNA_SELF_NAME);
		session.removeAttribute(KycConst.FNA_SPOUSE_NAME);
		session.removeAttribute(KycConst.FNA_SELF_NAME);
		session.removeAttribute(KycConst.FNA_SPOUSE_NAME);
		session.removeAttribute("CURRENT_FNAID");
		
		session.removeAttribute("KYC_APPROVE_DISTID");
		session.removeAttribute("KYC_APPROVE_ADVID");
//		session.removeAttribute("KYC_APPROVE_USERID");
		session.removeAttribute("KYC_APPROVE_MGRACSFLG");
		session.removeAttribute("KYC_APPROVE_FNAID");
		
		session.removeAttribute("CREATE_NEW_FNA_FLG");
		session.removeAttribute("PREV_FNAID");
		session.removeAttribute("LAT_ARCHV_FNAID");
		
		return "login";
	}
	
	@RequestMapping("/index")
	public ModelAndView index(HttpServletRequest request) {
		
		ModelAndView mv = new ModelAndView();
		
		HttpSession session = request.getSession(false);
		

		if(!KycUtils.chkValidDB()){
			mv.setViewName("dbError");
			return mv;
		}
		
		if(!KycUtils.isValidUSession(request)){
			mv.setViewName("sessionExpired");
			return mv;
		}
		
		session.removeAttribute("CURRENT_DATAFORM_ID");
		session.removeAttribute(KycConst.FNA_SELF_NAME);
		session.removeAttribute(KycConst.FNA_SPOUSE_NAME);
		session.removeAttribute(KycConst.FNA_SELF_NAME);
		session.removeAttribute(KycConst.FNA_SPOUSE_NAME);
		session.removeAttribute("CURRENT_FNAID");
		
		session.removeAttribute("KYC_APPROVE_DISTID");
		session.removeAttribute("KYC_APPROVE_ADVID");
//		session.removeAttribute("KYC_APPROVE_USERID");
		session.removeAttribute("KYC_APPROVE_MGRACSFLG");
		session.removeAttribute("KYC_APPROVE_FNAID");
		
		session.removeAttribute("CREATE_NEW_FNA_FLG");
		session.removeAttribute("PREV_FNAID");
		session.removeAttribute("LAT_ARCHV_FNAID");
		
		Map<String,String> sessMap =(Map<String, String>) session.getAttribute(KycConst.LOGGED_USER_INFO);
		
		sessMap.put(KycConst.FNA_ADVSTF_MGRNAME,sessMap.get(KycConst.LOGGED_USER_MGRADVSTFNAME));
		sessMap.put(KycConst.FNA_ADVSTFNAME, sessMap.get(KycConst.LOGGED_USER_ADVSTFNAME));
		
		session.setAttribute(KycConst.LOGGED_USER_INFO, sessMap);
		
		mv.setViewName("index");
		
		return mv;
	}

	@RequestMapping("/clientInfo")
	public ModelAndView clientInfo(HttpServletRequest request)	throws JsonProcessingException {

		ModelAndView mv = new ModelAndView();
		
		if(!KycUtils.chkValidDB()){
			mv.setViewName("dbError");
			return mv;
		}
		
		if(!KycUtils.isValidUSession(request)){
			mv.setViewName("sessionExpired");
			return mv;
		}

		String strCustIdEnc = request.getParameter("c") != null ? request.getParameter("c").toString() : "";
		String strFnaIdEnc = request.getParameter("fnaId") != null ? request.getParameter("fnaId").toString() : "";
		String strNewFnaEnc = request.getParameter("newFna") != null ? request.getParameter("newFna").toString() : "";
		HttpSession session = request.getSession(false);
		session.removeAttribute("LAT_ARCHV_FNAID");
		
		//poovathi add on 29-06-2021 to decrypt the URL Parameter values
		
		// decode encrypted CustId here
		//String strCustIdDec = new String(DatatypeConverter.parseBase64Binary(strCustIdEnc));
		String strCustId = decodeURLParam(strCustIdEnc);
		
		// decode encrypted FNAID here 				
		//String strFnaIdDec = new String(DatatypeConverter.parseBase64Binary(strFnaIdEnc));
		String strFnaId = decodeURLParam(strFnaIdEnc);
		
		// decode encrypted  CreateNewfna
		//String strNewFnaDec = new String(DatatypeConverter.parseBase64Binary(strNewFnaEnc));
		String strNewFna = decodeURLParam(strNewFnaEnc);
		//end
		
		if(!KycUtils.nullOrBlank(strFnaId)) {
		    session.setAttribute("LAT_ARCHV_FNAID", strFnaId);
		}else {
		    session.setAttribute("LAT_ARCHV_FNAID", strNewFna);
		}
		
		AdviserService fs = new AdviserService();
		FpmsService fpmsser = new FpmsService();
		KycService kycs = new KycService();
		CustomerDetsService cs = new CustomerDetsService();
		
		Map<String, String> sessMap = new HashMap<String, String>();
		sessMap = (Map<String, String>) session.getAttribute(KycConst.LOGGED_USER_INFO);
		String strStfType = sessMap.get(KycConst.LOGGED_USER_STFTYPE);
		String strLogAdvId = sessMap.get(KycConst.LOGGED_USER_ADVSTFID);
		String strADVID = sessMap.get(KycConst.LOGGED_USER_ADVSTFID);
		String strADVNAME = sessMap.get(KycConst.LOGGED_USER_ADVSTFNAME);
			
		String strDistId = sessMap.get(KycConst.LOGGED_DIST_ID);
		
		List lstGlblAccessLvl = new ArrayList();
		lstGlblAccessLvl = (List) session.getAttribute("GLBL_ACCESS_LEVEL");
		
		
		boolean isCompAcs = false,isMgrAcs = false;
		Iterator acslvlite = lstGlblAccessLvl.iterator();
		while(acslvlite.hasNext()) {
			FPMSLabelValueDTO dto = (FPMSLabelValueDTO) acslvlite.next();
			String strAcsLvl = dto.getValue();
			if(strAcsLvl.equalsIgnoreCase("COMPANY")) {
				isCompAcs=true;
			}
			
			if(strAcsLvl.equalsIgnoreCase("MANAGER")) {
				isMgrAcs=true;
			}
		}
		
//		 List advStfElem = fs.srchGlblAccessLevel(strLogAdvId);
		
		if(strStfType.equalsIgnoreCase("STAFF")) {
			List<AdviserStaff> advDets = fs.getActiveAdvisers(strLogAdvId,strStfType,strDistId);
			request.setAttribute("ACTIVE_ADVISER",advDets);
		}else if(strStfType.equalsIgnoreCase("PARAPLANNER")||
				strStfType.equalsIgnoreCase("INTRODUCER")) {
//			List<AdviserStaff> advDets = fs.getGloblAcessList(strLogAdvId,strStfType,strDistId);
			List paraList = fs.populateParaplannerDets(strLogAdvId,strDistId);
			request.setAttribute("ACTIVE_ADVISER",paraList);
		}else {
			
			if(isCompAcs) {
				List<AdviserStaff> advDets = fs.getActiveAdvisers(strLogAdvId,strStfType,strDistId);
				request.setAttribute("ACTIVE_ADVISER",advDets);
			}else {
				
				List advDets = new ArrayList();
				if(isMgrAcs) {
					advDets = fpmsser.getGloblAcessList(strLogAdvId,strStfType,strDistId);
					request.setAttribute("ACTIVE_ADVISER",advDets);
				}else {
				
					String[] strArrCurr = {strADVID,strADVNAME};
					List<String[]> paraList = new ArrayList<String[]>();
					paraList.add(strArrCurr);
//					advDets.add(strArrCurr);
					request.setAttribute("ACTIVE_ADVISER",paraList);
				}
//				

					
			}
			
		}
		

		
		
		
		List<MasterCustomerStatus> custStsList = fpmsser.getMastCustStatus();
		request.setAttribute("MASTER_CUSTSTS", custStsList);
		
		if (!KycUtils.nullOrBlank(strCustId)) {
			
			CustomerDetails custDets = cs.getClientsByCustId(strCustId);			
			AdviserStaff agentCurr = custDets.getAdviserStaffByAgentIdCurrent();			
			custDets.setCurrAdviserId(agentCurr.getAdvstfId());
			custDets.setCurrAdviserName(agentCurr.getAdvstfName());
			
//			AdviserStaff tmpAdvstf = fs.getAdviserByAdvId(agentCurr.getAdvstfId());

			ObjectMapper mapper = new ObjectMapper();
			String custJson = mapper.writeValueAsString(custDets);			
			request.setAttribute("CUSTOMER_DETAILS", custJson);
			
//			mapper = new ObjectMapper();
//			String advJson = mapper.writeValueAsString(agentCurr);
//			request.setAttribute("CUST_AGENTCURR_DETAILS", advJson);
			
			
			mv.setViewName("clientInfo");
		} else {
			request.setAttribute("CUSTOMER_DETAILS", "{}");
//			request.setAttribute("CUST_AGENTCURR_DETAILS", "{}");
			mv.setViewName("clientInfo");
		}
			
		return mv;
	}

	@RequestMapping("/kycIntro")
	public ModelAndView kycIntro(HttpServletRequest request) throws JsonProcessingException {
		
		ModelAndView mv = new ModelAndView();
		
		if(!KycUtils.chkValidDB()){
			mv.setViewName("dbError");
			return mv;
		}
		
		if(!KycUtils.isValidUSession(request)){
			mv.setViewName("sessionExpired");
			return mv;
		}

		FNAService ekycServ = new FNAService();
		
		HttpSession session = request.getSession(false);
		String strCustId = (String) session.getAttribute(KycConst.CURRENT_CUSTID);
		
		
		//String strSelectedFnaIDEnc = KycUtils.getParamValue(request, "id");
		String strSelectedFnaIDEnc = request.getParameter("id") != null ? request.getParameter("id").toString() : "";
		
		//poovathi add on 29-06-2021 to decrypt the URL Parameter values
		       // decode encrypted FnaId here
		        //String strSelectedFnaIdDec = new String(DatatypeConverter.parseBase64Binary(strSelectedFnaIDEnc));
		String  strSelectedFnaID = decodeURLParam(strSelectedFnaIDEnc);
		
		//end
		
		if(KycUtils.nullOrBlank(strCustId)){
			mv.setViewName("clientInfo");
			
		}else{
			
			List fnaList = new ArrayList();
			
			if(KycUtils.nullOrBlank(strSelectedFnaID)) {
				fnaList = ekycServ.findFNADetailsByCustId(strCustId);
				
			}else {
				fnaList = ekycServ.findFNADetailsByFnaId(strSelectedFnaID);
				session.removeAttribute("CURRENT_FNAID");
				session.setAttribute("CURRENT_FNAID", strSelectedFnaID);
			}
			
			
		
			int totalFna = fnaList.size();
			
			String StrFnaLists = "";

			if (totalFna > 0) {

				ObjectMapper mapper = new ObjectMapper();
				StrFnaLists = mapper.writeValueAsString(fnaList.get(0));
				request.setAttribute("LATEST_FNA_DETAILS", StrFnaLists);
				 
			}else { 
				  request.setAttribute("LATEST_FNA_DETAILS", "{}");
			  }
			 
                   mv.setViewName("kycIntro");
			
			
		}
		

		
		return mv;
	}

	@RequestMapping("/kycHome")
	public ModelAndView kycHome(HttpServletRequest request)	
			throws JsonProcessingException {
		
		ModelAndView mv = new ModelAndView();
		
		if(!KycUtils.chkValidDB()){
			mv.setViewName("dbError");
			return mv;
		}
		
		if(!KycUtils.isValidUSession(request)){
			mv.setViewName("sessionExpired");
			return mv;
		}

		HttpSession session = request.getSession(false);
		String strCustId = KycUtils.nullObj(session.getAttribute(KycConst.CURRENT_CUSTID)) ? "" : 
			(String) session.getAttribute(KycConst.CURRENT_CUSTID);
		
		String strPrevFNAId =  session.getAttribute("PREV_FNAID")!= null ? (String) session.getAttribute("PREV_FNAID") :""; 
		String strFNAId = "";
		
		String strFNAIdFlg = (String) session.getAttribute("CREATE_NEW_FNA_FLG");
		
		
		
		if("true".equals(strFNAIdFlg) && !KycUtils.nullOrBlank(strPrevFNAId)){
			strFNAId = strPrevFNAId;
		}else {
			strFNAId = (String) session.getAttribute("CURRENT_FNAID");
		}
			
		String param1 = KycUtils.getParamValue(request, "param1");
		
//		To set the customer latest details.
		String strCustName=KycConst.STR_NULL_STRING,strCustDob=KycConst.STR_NULL_STRING,strCustNric=KycConst.STR_NULL_STRING,
				strCustFin=KycConst.STR_NULL_STRING,strCustPP=KycConst.STR_NULL_STRING,strCustAddr1=KycConst.STR_NULL_STRING,
				strCustAddr2=KycConst.STR_NULL_STRING,strCustAddr3=KycConst.STR_NULL_STRING,strCustCity=KycConst.STR_NULL_STRING,
				strCustState=KycConst.STR_NULL_STRING,strCustCountry=KycConst.STR_NULL_STRING,strCustPostal=KycConst.STR_NULL_STRING,
				strCustNatly=KycConst.STR_NULL_STRING,strCustHeight=KycConst.STR_NULL_STRING,strCustWeight=KycConst.STR_NULL_STRING,
				strCustRace=KycConst.STR_NULL_STRING,strCustSmoker=KycConst.STR_NULL_STRING, strICDetails = KycConst.STR_NULL_STRING,
			    strClientStatus=KycConst.STR_NULL_STRING,strCustCountofBirth=KycConst.STR_NULL_STRING;

		StringBuffer homeaddr=new StringBuffer();
		
		FNAService fnaserv = new FNAService();
		CustomerDetsService cs = new CustomerDetsService();
		
		CustomerDetails custDets = cs.getClientsByCustId(strCustId);
		AdviserStaff agentCurr = custDets.getAdviserStaffByAgentIdCurrent();			
		custDets.setCurrAdviserId(agentCurr.getAdvstfId());
//		custDets.setCurrAdviserName(agentCurr.getAdvstfName());
		
		
		
		
		
		FnaDetails fnaObj = fnaserv.findFnaLatestDetlsByFnaId(strFNAId);
		request.setAttribute("FNA_SERVADV_ID", agentCurr.getAdvstfId());
		
		FnaSelfspouseDets fnaSSObj = fnaserv.findFNASelfSpsDetailsByFnaId(strFNAId);
		
		List fnaOthPerList = fnaserv.findFNAOthPersDetailsByFnaId(strFNAId);
		
		List lstFnaDets = fnaserv.getFnaDetsByCustId(strCustId);
		
		strCustName = KycUtils.getObjValue(custDets.getCustName());
//		strCustDob = KycUtils.getObjValue(custDets.getCustomerDetail().getDob());
		strCustDob = KycUtils.convertObjToString(KycUtils.convertDateToDateDateString(custDets.getDob())); ;
		
		strCustNric = KycUtils.getObjValue(custDets.getNric());
		strCustFin = KycUtils.getObjValue(custDets.getCustFin());
		strCustPP = KycUtils.getObjValue(custDets.getCustPassportNum());
		
		strCustAddr1 = KycUtils.getObjValue(custDets.getResAddr1());
		strCustAddr2 = KycUtils.getObjValue(custDets.getResAddr2());
		strCustAddr3 = KycUtils.getObjValue(custDets.getResAddr3());
		strCustCity = KycUtils.getObjValue(custDets.getResCity());
		strCustState = KycUtils.getObjValue(custDets.getResState());
		strCustCountry = KycUtils.getObjValue(custDets.getResCountry());
		strCustPostal = KycUtils.getObjValue(custDets.getResPostalcode());
		
		strClientStatus = KycUtils.getObjValue(custDets.getCustStatus());
		strCustNatly = KycUtils.getObjValue(custDets.getNationality());
		
		strCustHeight=KycUtils.getObjValue(custDets.getHeight());
		strCustWeight=KycUtils.getObjValue(custDets.getWeight());
		strCustRace=KycUtils.getObjValue(custDets.getRace());
		strCustSmoker=KycUtils.getObjValue(custDets.getSmokerFlg());
		strCustCountofBirth = KycUtils.getObjValue(custDets.getCountryofBirth());
		
		strICDetails= !KycUtils.nullOrBlank(strCustNric) ? strCustNric : !KycUtils.nullOrBlank(strCustFin) ? strCustFin : strCustPP;
		
		if(!strCustAddr1.isEmpty())homeaddr.append(strCustAddr1).append(",");
		if(!strCustAddr2.isEmpty())homeaddr.append(strCustAddr2).append(",");
		if(!strCustAddr3.isEmpty())homeaddr.append(strCustAddr3).append(",");
		if(!strCustCity.isEmpty())homeaddr.append(strCustCity).append(",");
		if(!strCustState.isEmpty())homeaddr.append(strCustState).append(",");
		if(!strCustCountry.isEmpty())homeaddr.append(strCustCountry).append(",");
		if(!strCustPostal.isEmpty())homeaddr.append(strCustPostal).append(".");

		if (KycUtils.nullObj(fnaObj) ) {

			if (!KycUtils.nullOrBlank(strCustId)) {
				
				ObjectMapper mapper = new ObjectMapper();
				String custJson = mapper.writeValueAsString(custDets);
                                request.setAttribute("CUSTOMER_DETAILS", custJson);
				request.setAttribute("SELFSPOUSE_DETAILS", "{}");
				request.setAttribute("CURRENT_FNA_DETAILS", "{}");
			}
			
			session.setAttribute("SESS_DF_SELF_NAME", custDets.getCustName());
			session.setAttribute("SESS_DF_SPS_NAME", "");
			
			request.setAttribute("OTHERPERSON_DETAILS", "[]");

		} else {

			
			session.setAttribute("SESS_DF_SELF_NAME", custDets.getCustName());
			session.setAttribute("SESS_DF_SPS_NAME", "");
			
			ObjectMapper fnamapper = new ObjectMapper();
			String strFnaJson = fnamapper.writeValueAsString(fnaObj);
			request.setAttribute("CURRENT_FNA_DETAILS", strFnaJson);
			
//			CustomerDetails custDets = fs.getClientsByCustId(strCustId);
			ObjectMapper custmapper = new ObjectMapper();
			String custJson = custmapper.writeValueAsString(custDets);
			request.setAttribute("CUSTOMER_DETAILS",custJson);
			
			
			String strSSJson = "{}";
			if(!KycUtils.nullObj(fnaSSObj)){
				String strFnaCustName=KycConst.STR_NULL_STRING;
				String strFnaCustDOB =KycConst.STR_NULL_STRING; 
				String strFnaCustNric=KycConst.STR_NULL_STRING;
				String strFnaCustResiAddr = KycConst.STR_NULL_STRING;
				String strFnaCustAddr1 = KycConst.STR_NULL_STRING;String strFnaCustAddr2 = KycConst.STR_NULL_STRING;
				String strFnaCustAddr3 = KycConst.STR_NULL_STRING;String strFnaCustCity = KycConst.STR_NULL_STRING;
				String strFnaCustState = KycConst.STR_NULL_STRING;String strFnaCustCountry = KycConst.STR_NULL_STRING;
				String strFnaCustPostal = KycConst.STR_NULL_STRING;
				String strFnaCustNatly = KycConst.STR_NULL_STRING;
				//poovathi commented on 03-06-2021
				//String strFnaCustCountofBirth = KycConst.STR_NULL_STRING;
				
				strFnaCustName  = KycUtils.getObjValue(fnaSSObj.getDfSelfName());
//				strFnaCustDOB = KycUtils.getObjValue(fna.getTxtFldFnaClntDOB());
				strFnaCustDOB = KycUtils.convertObjToString(KycUtils.convertDateToDateDateString(fnaSSObj.getDfSelfDob())); 
				strFnaCustNric= KycUtils.getObjValue(fnaSSObj.getDfSelfNric());
				strFnaCustResiAddr = KycUtils.getObjValue(fnaSSObj.getDfSelfHomeaddr());
				
				strFnaCustAddr1 =KycUtils.getObjValue(fnaSSObj.getResAddr1());
				strFnaCustAddr2 = KycUtils.getObjValue(fnaSSObj.getResAddr2());
				strFnaCustAddr3=KycUtils.getObjValue(fnaSSObj.getResAddr3());
				strFnaCustCity=KycUtils.getObjValue(fnaSSObj.getResCity());
				strFnaCustState=KycUtils.getObjValue(fnaSSObj.getResState());
				strFnaCustCountry=KycUtils.getObjValue(fnaSSObj.getResCountry());
				strFnaCustPostal=KycUtils.getObjValue(fnaSSObj.getResPostalcode());
				strFnaCustNatly  = KycUtils.getObjValue(fnaSSObj.getDfSelfNationality());
				//poovathi commented on 03-06-2021
				//strFnaCustCountofBirth = KycUtils.getObjValue(fnaSSObj.getDfSelfBirthcntry());
				
//				if(!strClientStatus.equalsIgnoreCase(KycConst.STR_CLNTSTS_CLNT)){
					
					strFnaCustName = strCustName.equalsIgnoreCase(strFnaCustName) ? strFnaCustName : strCustName;
					strFnaCustDOB = KycUtils.nullOrBlank(strCustDob)?strFnaCustDOB:
						strCustDob.equalsIgnoreCase(strFnaCustDOB) ? strFnaCustDOB : strCustDob;
					strFnaCustNric = KycUtils.nullOrBlank(strICDetails)? strFnaCustNric :
						strICDetails.equalsIgnoreCase(strFnaCustNric) ? strFnaCustNric : strICDetails;
					strFnaCustResiAddr = homeaddr.toString().equalsIgnoreCase(strFnaCustResiAddr) ? strFnaCustResiAddr : homeaddr.toString();
					
					strFnaCustAddr1 = strCustAddr1.equalsIgnoreCase(strFnaCustAddr1) ? strFnaCustAddr1 : strCustAddr1;
					strFnaCustAddr2 = strCustAddr2.equalsIgnoreCase(strFnaCustAddr2) ? strFnaCustAddr2 : strCustAddr2;
					strFnaCustAddr3 = strCustAddr3.equalsIgnoreCase(strFnaCustAddr3) ? strFnaCustAddr3 : strCustAddr3;
					strFnaCustCity = strCustCity.equalsIgnoreCase(strFnaCustCity) ? strFnaCustCity : strCustCity;
					strFnaCustState = strCustState.equalsIgnoreCase(strFnaCustState) ? strFnaCustState : strCustState;
					strFnaCustCountry = strCustCountry.equalsIgnoreCase(strFnaCustCountry) ? strFnaCustCountry : strCustCountry;
					strFnaCustPostal = strCustPostal.equalsIgnoreCase(strFnaCustPostal) ? strFnaCustPostal : strCustPostal;
					strFnaCustNatly = strCustNatly.equalsIgnoreCase(strFnaCustNatly) ?strCustNatly : strFnaCustNatly;
					//poovathi commented on 03-06-2021
					//strFnaCustCountofBirth = strCustCountofBirth.equalsIgnoreCase(strFnaCustCountofBirth) ? strFnaCustCountofBirth : strCustCountofBirth;
					
					
					fnaSSObj.setDfSelfName(strFnaCustName);
					fnaSSObj.setDfSelfDob(KycUtils.convertDateStringToDate(strFnaCustDOB));
					fnaSSObj.setDfSelfNric(strFnaCustNric);
					fnaSSObj.setDfSelfHomeaddr(strFnaCustResiAddr);
					String strSameAdd = KycUtils.nullOrBlank(fnaSSObj.getDfSelfSameasregadd())? "Y" : fnaSSObj.getDfSelfSameasregadd();
					String strDiffAdd = KycUtils.nullOrBlank(fnaSSObj.getDfSelfMailaddrflg())? "N" : fnaSSObj.getDfSelfMailaddrflg();
					if(strSameAdd.equalsIgnoreCase("Y") && strDiffAdd.equalsIgnoreCase("N")) {
						fnaSSObj.setDfSelfMailaddr(strFnaCustResiAddr);
					}
					
					fnaSSObj.setResAddr1(strFnaCustAddr1);
					fnaSSObj.setResAddr2(strFnaCustAddr2);
					fnaSSObj.setResAddr3(strFnaCustAddr3);
					fnaSSObj.setResCity(strFnaCustCity);
					fnaSSObj.setResState(strFnaCustState);
					fnaSSObj.setResCountry(strFnaCustCountry);
					fnaSSObj.setResPostalcode(strFnaCustPostal);
					//poovathi commented on 03-06-2021
					//fnaSSObj.setDfSelfBirthcntry(strFnaCustCountofBirth);
//				}
				
				ObjectMapper mapper = new ObjectMapper();
				strSSJson = mapper.writeValueAsString(fnaSSObj);
				session.setAttribute("SESS_DF_SELF_NAME", fnaSSObj.getDfSelfName());
				session.setAttribute("SESS_DF_SPS_NAME", KycUtils.nullOrBlank(fnaSSObj.getDfSpsName())?null: fnaSSObj.getDfSpsName());
				
				session.setAttribute("CURRENT_DATAFORM_ID", fnaSSObj.getDataformId());
				session.setAttribute(KycConst.FNA_SELF_NAME, fnaSSObj.getDfSelfName());
				session.setAttribute(KycConst.FNA_SPOUSE_NAME, KycUtils.nullOrBlank(fnaSSObj.getDfSpsName())?null: fnaSSObj.getDfSpsName());
				
				
				
			}else {
			        //poovathi add on 18-06-2021
				session.removeAttribute("CREATE_NEW_FNA_FLG");
				session.removeAttribute("PREV_FNAID");
			}
			
			request.setAttribute("SELFSPOUSE_DETAILS", strSSJson);
			
		}
		
		
		if(fnaOthPerList.size() > 0) {

			FnaOthPersBenfDets a = (FnaOthPersBenfDets)fnaOthPerList.get(0);
			FnaOthPersPep b=  (FnaOthPersPep)fnaOthPerList.get(2);
			FnaOthPersTpp c= (FnaOthPersTpp)fnaOthPerList.get(1);
			ObjectMapper mapper = new ObjectMapper();
			String a1=mapper.writeValueAsString(a);
			String b1=mapper.writeValueAsString(b);
			mapper = new ObjectMapper();
			String c1=mapper.writeValueAsString(c);
			
			JSONArray jsnArr =  new JSONArray();
			jsnArr.put(new org.json.JSONObject(a1));
			jsnArr.put(new org.json.JSONObject(b1));
			jsnArr.put(new org.json.JSONObject(c1));
			
			request.setAttribute("OTHPERS_DETAILS", jsnArr);

		}else {
			request.setAttribute("OTHPERS_DETAILS", "[]");
		}
		
		
		
		if(lstFnaDets.size() > 0) {
			
			Map<String,String> sessMap =(Map<String, String>) session.getAttribute(KycConst.LOGGED_USER_INFO);	
			FnaDetails IFnaDets = (FnaDetails)lstFnaDets.get(0);
			String strFnaAdviserName = IFnaDets.getAdviserStaffByAdvstfId().getAdvstfName();
			String strFnaManagerrName = IFnaDets.getAdviserStaffByMgrId().getAdvstfName();
			sessMap.put(KycConst.FNA_ADVSTF_MGRNAME,strFnaManagerrName);
			sessMap.put(KycConst.FNA_ADVSTFNAME, strFnaAdviserName);
			session.setAttribute(KycConst.LOGGED_USER_INFO, sessMap);
		}
		
		mv.setViewName("kycHome");

		request.setAttribute("PREV_SCREEN", "kycIntro");
		request.setAttribute("NEXT_SCREEN", "taxResidency");
		request.setAttribute("SIGN_VALIDATION_MSG", param1);
		return mv;
	}

	@RequestMapping("/taxResidency")
	public ModelAndView taxResidency(HttpServletRequest request) throws JsonProcessingException {
		
		ModelAndView mv = new ModelAndView();
		
		if(!KycUtils.chkValidDB()){
			mv.setViewName("dbError");
			return mv;
		}
		
		if(!KycUtils.isValidUSession(request)){
			mv.setViewName("sessionExpired");
			return mv;
		}
		
		HttpSession session = request.getSession(false);
		
		
		
		String strCustId = KycUtils.nullObj(session.getAttribute(KycConst.CURRENT_CUSTID)) ? "" : 
			  (String) session.getAttribute(KycConst.CURRENT_CUSTID);
		
		 String strPrevFNAId =  session.getAttribute("PREV_FNAID")!= null ? (String) session.getAttribute("PREV_FNAID") :""; 
		 String strFNAId = "";
		 String strFNAIdFlg = (String) session.getAttribute("CREATE_NEW_FNA_FLG");
		
		
		
		
		if("true".equals(strFNAIdFlg) && !KycUtils.nullOrBlank(strPrevFNAId)){
			strFNAId = strPrevFNAId;
		}else {
			strFNAId = (String) session.getAttribute("CURRENT_FNAID");
		}
		
		
		String param1 = KycUtils.getParamValue(request, "param1");
		
		
		FNAService fnaserv = new FNAService();
		
		
		FnaDetails fnaObj = fnaserv.findFnaLatestDetlsByFnaId(strFNAId);
		FnaSelfspouseDets fnaSSObj = fnaserv.findFNASelfSpsDetailsByFnaId(strFNAId);
		
		ObjectMapper fnamapper = new ObjectMapper();
		String strFnaJson = fnamapper.writeValueAsString(fnaObj);
		request.setAttribute("CURRENT_FNA_DETAILS", strFnaJson);
		
		
		ObjectMapper mapper = new ObjectMapper();
		String strSSJson="{}";
		if(!KycUtils.nullObj(fnaSSObj) ){
			strSSJson = mapper.writeValueAsString(fnaSSObj);
			request.setAttribute("SELFSPOUSE_DETAILS", strSSJson);
		}else{
			request.setAttribute("SELFSPOUSE_DETAILS", strSSJson);
		}

		mv.setViewName("taxResidency");
		
		request.setAttribute("PREV_SCREEN", "kycHome");
		request.setAttribute("NEXT_SCREEN", "finanWellReview");
		request.setAttribute("SIGN_VALIDATION_MSG", param1);
		return mv;
	}

	@RequestMapping("/finanWellReview")
	public ModelAndView finanWellReview(HttpServletRequest request) throws JsonProcessingException {
		
		ModelAndView mv = new ModelAndView();
		
		if(!KycUtils.chkValidDB()){
			mv.setViewName("dbError");
			return mv;
		}
		
		if(!KycUtils.isValidUSession(request)){
			mv.setViewName("sessionExpired");
			return mv;
		}
		
		    HttpSession session = request.getSession(false);
		
			String strCustId = KycUtils.nullObj(session.getAttribute(KycConst.CURRENT_CUSTID)) ? "" : 
				(String) session.getAttribute(KycConst.CURRENT_CUSTID);
			
			 String strPrevFNAId =  session.getAttribute("PREV_FNAID")!= null ? (String) session.getAttribute("PREV_FNAID") :""; 
			 String strFNAId = "";
			 String strFNAIdFlg = (String) session.getAttribute("CREATE_NEW_FNA_FLG");
			
			
			
			if("true".equals(strFNAIdFlg) && !KycUtils.nullOrBlank(strPrevFNAId)){
				strFNAId = strPrevFNAId;
			}else {
				strFNAId = (String) session.getAttribute("CURRENT_FNAID");
			}
			
			   
			
		
		String param1 = KycUtils.getParamValue(request, "param1");
		FNAService fnaserv = new FNAService();
		
		FnaDetails fnaObj = fnaserv.findFnaLatestDetlsByFnaId(strFNAId);
		
		FnaSelfspouseDets fnaSSObj = fnaserv.findFNASelfSpsDetailsByFnaId(strFNAId);	
		
		FnaExpenditureDets fnaExp = fnaserv.findExpendByFnaId(strFNAId);
		
		List fnaDependatList = fnaserv.findFNADependantByFnaId(strFNAId);
		
		String strDepent="{}", strSSJson = "{}",strFnaJson="{}",expObj="{}";
		
		
		
		if (!KycUtils.nullObj(fnaObj) ) {
			
			ObjectMapper fnamapper = new ObjectMapper();
			strFnaJson = fnamapper.writeValueAsString(fnaObj);
			request.setAttribute("CURRENT_FNA_DETAILS", strFnaJson);
		}

		if (!KycUtils.nullObj(fnaSSObj) ) {
			ObjectMapper mapper = new ObjectMapper();
			strSSJson = mapper.writeValueAsString(fnaSSObj);
			request.setAttribute("SELFSPOUSE_DETAILS", strSSJson);  
		}else{
			request.setAttribute("SELFSPOUSE_DETAILS", strSSJson);
		}
		
		
		
		if (!KycUtils.nullObj(fnaExp) ) {
			
			ObjectMapper expMapper = new ObjectMapper();
			expObj = expMapper.writeValueAsString(fnaExp);
			request.setAttribute("FNA_EXPEND_DETAILS", expObj);
		}
		
		if (fnaDependatList.size() > 0 ) {
			
			ObjectMapper depntMapper = new ObjectMapper();
			strDepent = depntMapper.writeValueAsString(fnaDependatList);
			request.setAttribute("FNA_DEPENDANT_DETAILS", strDepent);
		}else {
			request.setAttribute("FNA_DEPENDANT_DETAILS", "[]");
		}
		
		mv.setViewName("finanWellReview");
		request.setAttribute("PREV_SCREEN", "taxResidency");
		request.setAttribute("NEXT_SCREEN", "productRecommend");
		request.setAttribute("SIGN_VALIDATION_MSG", param1);
		return mv;
	}

	@RequestMapping("/productRecommend")
	public ModelAndView productRecommend(ModelMap modal,HttpServletRequest request) throws JsonProcessingException {
		
		ModelAndView mv = new ModelAndView();
		
		if(!KycUtils.chkValidDB()){
			mv.setViewName("dbError");
			return mv;
		}
		
		if(!KycUtils.isValidUSession(request)){
			mv.setViewName("sessionExpired");
			return mv;
		}
		
		HttpSession session = request.getSession(false);
		String strFNAId = (String) session.getAttribute("CURRENT_FNAID");
		String param1 = KycUtils.getParamValue(request, "param1");
		
		FNAService fnaserv = new FNAService();
		KycService kycser = new KycService();
		FpmsService fpmsServ = new FpmsService();
		
		List<?> principalList = fpmsServ.populatePrincipal();
		modal.addAttribute("PRINCIPAL_LIST", principalList);
		
		FnaDetails fnaObj = fnaserv.findFnaLatestDetlsByFnaId(strFNAId);
		String strFnaJson="{}" ;
		if(!KycUtils.nullObj(fnaObj)){
			ObjectMapper fnamapper = new ObjectMapper();
			strFnaJson = fnamapper.writeValueAsString(fnaObj);
			request.setAttribute("CURRENT_FNA_DETAILS", strFnaJson);
		}
		
		FnaSelfspouseDets fnaSelfSps = fnaserv.findFNASelfSpsDetailsByFnaId(strFNAId);
		List<String> names = new ArrayList<String>();
		if (!KycUtils.nullObj(fnaSelfSps) ) {
		names.add(fnaSelfSps.getDfSelfName().toUpperCase());
			if(!KycUtils.nullObj(fnaSelfSps.getDfSpsName())){
				names.add(fnaSelfSps.getDfSpsName().toUpperCase());
			}
		}else {
			CustomerDetsService cs = new CustomerDetsService();
			String strCustId = KycUtils.nullObj(session.getAttribute(KycConst.CURRENT_CUSTID)) ? "" : 
				(String) session.getAttribute(KycConst.CURRENT_CUSTID);
			CustomerDetails custDets = cs.getClientsByCustId(strCustId);
			names.add(custDets.getCustName().toUpperCase());
		}		
		request.setAttribute("FNA_SELF_SPOUSE_NAMES", names);
			

		List<?> prodRecommList = fnaserv.getFNAProdRecomPlans(request,strFNAId);
		String strProdRecommList = "";
		if(prodRecommList.size() > 0) {
			
			ObjectMapper mapper = new ObjectMapper();
			strProdRecommList = mapper.writeValueAsString(prodRecommList);
			request.setAttribute("PRODUCT_RECOM_PLAN_DETS", strProdRecommList);
		}else{
			
			
			request.setAttribute("PRODUCT_RECOM_PLAN_DETS", "{}");
		}
		
		
		request.setAttribute("PREV_SCREEN", "finanWellReview");
		request.setAttribute("NEXT_SCREEN", "productSwitchRecomm");
		request.setAttribute("SIGN_VALIDATION_MSG", param1);
		mv.setViewName("productRecommend");
		return mv;
	}

	
	
	@RequestMapping("/productSwitchRecomm")
	public ModelAndView productSwitchRecomm(HttpServletRequest request) throws JsonProcessingException {
		
		ModelAndView mv = new ModelAndView();
		
		if(!KycUtils.chkValidDB()){
			mv.setViewName("dbError");
			return mv;
		}
		
		
		if(!KycUtils.isValidUSession(request)){
			mv.setViewName("sessionExpired");
			return mv;
		}
		HttpSession session = request.getSession(false);
		String strFNAId = (String) session.getAttribute("CURRENT_FNAID");
		String param1 = KycUtils.getParamValue(request, "param1");
		FNAService fnaserv = new FNAService();
		
		FnaDetails fnaObj = fnaserv.findFnaLatestDetlsByFnaId(strFNAId);
		
		FnaSelfspouseDets fnaSSObj = fnaserv.findFNASelfSpsDetailsByFnaId(strFNAId);
		
		String   strSSJson = "{}",strFnaJson="{}" ;
		
		if (!KycUtils.nullObj(fnaSSObj) ) {
			ObjectMapper mapper = new ObjectMapper();
			strSSJson = mapper.writeValueAsString(fnaSSObj);
			
		}
		
		request.setAttribute("SELFSPOUSE_DETAILS", strSSJson);
		
		if(!KycUtils.nullObj(fnaObj)){
			ObjectMapper fnamapper = new ObjectMapper();
			strFnaJson = fnamapper.writeValueAsString(fnaObj);
			request.setAttribute("CURRENT_FNA_DETAILS", strFnaJson);
		}
			
		
		
		mv.setViewName("productSwitchRecomm");
		request.setAttribute("PREV_SCREEN", "productRecommend");
		request.setAttribute("NEXT_SCREEN", "productSwitchReplace");
		request.setAttribute("SIGN_VALIDATION_MSG", param1);
		return mv;
	}
	
	
	
	//Product switching and replacement
	
		@RequestMapping("/productSwitchReplace")
		public ModelAndView productSwitchReplace(ModelMap modal,HttpServletRequest request) throws JsonProcessingException {
			
			ModelAndView mv = new ModelAndView();
			
			if(!KycUtils.chkValidDB()){
				mv.setViewName("dbError");
				return mv;
			}
			
			if(!KycUtils.isValidUSession(request)){
				mv.setViewName("sessionExpired");
				return mv;
			}
			
			HttpSession session = request.getSession(false);
			String strFNAId = (String) session.getAttribute("CURRENT_FNAID");
			String param1 = KycUtils.getParamValue(request, "param1");
			
			FNAService fnaserv = new FNAService();
			KycService kycser = new KycService();
			FpmsService fpmsServ = new FpmsService();
			List<?> principalList = fpmsServ.populatePrincipal();
			modal.addAttribute("PRINCIPAL_LIST", principalList);
			
			FnaDetails fnaObj = fnaserv.findFnaLatestDetlsByFnaId(strFNAId);
			String strFnaJson="{}" ;
			if(!KycUtils.nullObj(fnaObj)){
				ObjectMapper fnamapper = new ObjectMapper();
				strFnaJson = fnamapper.writeValueAsString(fnaObj);
				request.setAttribute("CURRENT_FNA_DETAILS", strFnaJson);
			}
			
			FnaSelfspouseDets fnaSelfSps = fnaserv.findFNASelfSpsDetailsByFnaId(strFNAId);
			List<String> names = new ArrayList<String>();
			if (!KycUtils.nullObj(fnaSelfSps) ) {
			names.add(fnaSelfSps.getDfSelfName().toUpperCase());
				if(!KycUtils.nullObj(fnaSelfSps.getDfSpsName())){
					names.add(fnaSelfSps.getDfSpsName().toUpperCase());
				}
			}else {
				CustomerDetsService cs = new CustomerDetsService();
				String strCustId = KycUtils.nullObj(session.getAttribute(KycConst.CURRENT_CUSTID)) ? "" : 
					(String) session.getAttribute(KycConst.CURRENT_CUSTID);
				CustomerDetails custDets = cs.getClientsByCustId(strCustId);
				names.add(custDets.getCustName().toUpperCase());
			}		
			request.setAttribute("FNA_SELF_SPOUSE_NAMES", names);
				

			List<?> prodRecommList = fnaserv.getFNASwtchrepRecomPlans(request,strFNAId);
			String strProdRecommList = "";
			if(prodRecommList.size() > 0) {
				
				ObjectMapper mapper = new ObjectMapper();
				strProdRecommList = mapper.writeValueAsString(prodRecommList);
				request.setAttribute("PRODUCT_RECOM_PLAN_DETS", strProdRecommList);
			}else{
				
				
				request.setAttribute("PRODUCT_RECOM_PLAN_DETS", "{}");
			}
			
			
			request.setAttribute("PREV_SCREEN", "productSwitchRecomm");
			request.setAttribute("NEXT_SCREEN", "adviceBasisRecomm");
			request.setAttribute("SIGN_VALIDATION_MSG", param1);
			mv.setViewName("productSwitchReplace");
			return mv;
		}
		
		//

    
	//poovathi commented ekyc investment menu related on 07-06-2021
		
	/*@RequestMapping("/investmentProduct")
	public ModelAndView UTAddNew(HttpServletRequest request) throws JsonProcessingException {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("investmentProduct");
		request.setAttribute("PREV_SCREEN", "productSwitchReplace");
		request.setAttribute("NEXT_SCREEN", "ProductSummary");
		return mv;
	}
	
	
	@RequestMapping("/ProductSummary")
	public ModelAndView UTSummary(HttpServletRequest request) throws JsonProcessingException {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("ProductSummary");
		request.setAttribute("PREV_SCREEN", "investmentProduct");
		request.setAttribute("NEXT_SCREEN", "adviceBasisRecomm");
		return mv;
	}*/ //end
	
	
	
	@RequestMapping("/adviceBasisRecomm")
	public ModelAndView adviceBasisRecomm(HttpServletRequest request) throws JsonProcessingException {
		ModelAndView mv = new ModelAndView();
		
		if(!KycUtils.isValidUSession(request)){
			mv.setViewName("sessionExpired");
			return mv;
		}
		
		if(!KycUtils.chkValidDB()){
			mv.setViewName("dbError");
			return mv;
		}
		
		HttpSession session = request.getSession(false);
		String strFNAId = (String) session.getAttribute("CURRENT_FNAID");
		String param1 = KycUtils.getParamValue(request, "param1");
		FNAService fnaserv = new FNAService();
		
		FnaDetails fnaObj = fnaserv.findFnaLatestDetlsByFnaId(strFNAId);
		
		FnaSelfspouseDets fnaSSObj = fnaserv.findFNASelfSpsDetailsByFnaId(strFNAId);
		
		String   strSSJson = "{}",strFnaJson="{}" ;
		
		if (!KycUtils.nullObj(fnaSSObj) ) {
			ObjectMapper mapper = new ObjectMapper();
			strSSJson = mapper.writeValueAsString(fnaSSObj);
			request.setAttribute("SELFSPOUSE_DETAILS", strSSJson);
		}else{
			request.setAttribute("SELFSPOUSE_DETAILS", "{}");
		}
		
		if(!KycUtils.nullObj(fnaObj)){
			ObjectMapper fnamapper = new ObjectMapper();
			strFnaJson = fnamapper.writeValueAsString(fnaObj);
			request.setAttribute("CURRENT_FNA_DETAILS", strFnaJson);
		}
		
		mv.setViewName("adviceBasisRecomm");
		request.setAttribute("PREV_SCREEN", "productSwitchReplace");
		request.setAttribute("NEXT_SCREEN", "clientSignature");
		request.setAttribute("SIGN_VALIDATION_MSG", param1);
		return mv;
	}
	
	
	//ekyc investment related commented by poovathi on 07-06-2021
	
	/*@RequestMapping("/advBasisUT")
	public ModelAndView advBasisUT(HttpServletRequest request) throws JsonProcessingException {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("advBasisUT");
		request.setAttribute("PREV_SCREEN", "adviceBasisRecomm");
		request.setAttribute("NEXT_SCREEN", "clientDeclaration");
		return mv;
	}*/

	@RequestMapping("/clientSignature")
	public ModelAndView clientSignature(HttpServletRequest request) throws JsonProcessingException {
		ModelAndView mv = new ModelAndView();
		
		if(!KycUtils.chkValidDB()){
			mv.setViewName("dbError");
			return mv;
		}
		
		if(!KycUtils.isValidUSession(request)){
			mv.setViewName("sessionExpired");
			return mv;
		}
		HttpSession session = request.getSession(false);
		String strFNAId = (String) session.getAttribute("CURRENT_FNAID");
		String param1 = KycUtils.getParamValue(request, "param1");
		FNAService fnaserv = new FNAService();
		
		FnaDetails fnaObj = fnaserv.findFnaLatestDetlsByFnaId(strFNAId);
		
		FnaSelfspouseDets fnaSSObj = fnaserv.findFNASelfSpsDetailsByFnaId(strFNAId);
		
		String   strSSJson = "{}",strFnaJson="{}" ;
		
		if (!KycUtils.nullObj(fnaSSObj) ) {
			ObjectMapper mapper = new ObjectMapper();
			strSSJson = mapper.writeValueAsString(fnaSSObj);
			request.setAttribute("SELFSPOUSE_DETAILS", strSSJson);
		}else{
			request.setAttribute("SELFSPOUSE_DETAILS", "{}");
		}
		
		if(!KycUtils.nullObj(fnaObj)){
			ObjectMapper fnamapper = new ObjectMapper();
			strFnaJson = fnamapper.writeValueAsString(fnaObj);
			request.setAttribute("CURRENT_FNA_DETAILS", strFnaJson);
		}
		
		mv.setViewName("clientSignature");
		request.setAttribute("PREV_SCREEN", "adviceBasisRecomm");
		request.setAttribute("NEXT_SCREEN", "clientDeclaration");
		request.setAttribute("SIGN_VALIDATION_MSG", param1);
		return mv;
	}
	
	
	

	@RequestMapping("/clientDeclaration")
	public ModelAndView advisorDeclaration(HttpServletRequest request) throws JsonProcessingException {
		ModelAndView mv = new ModelAndView();
		
		if(!KycUtils.chkValidDB()){
			mv.setViewName("dbError");
			return mv;
		}
		
		if(!KycUtils.isValidUSession(request)){
			mv.setViewName("sessionExpired");
			return mv;
		}
		HttpSession session = request.getSession(false);
		String strFNAId = (String) session.getAttribute("CURRENT_FNAID");
		String param1 = KycUtils.getParamValue(request, "param1");
		FNAService fnaserv = new FNAService();
		
		FnaDetails fnaObj = fnaserv.findFnaLatestDetlsByFnaId(strFNAId);
		
		FnaSelfspouseDets fnaSSObj = fnaserv.findFNASelfSpsDetailsByFnaId(strFNAId);
		
		String   strSSJson = "{}",strFnaJson="{}" ;
		
		if (!KycUtils.nullObj(fnaSSObj) ) {
			ObjectMapper mapper = new ObjectMapper();
			strSSJson = mapper.writeValueAsString(fnaSSObj);
			request.setAttribute("SELFSPOUSE_DETAILS", strSSJson);
		}else{
			request.setAttribute("SELFSPOUSE_DETAILS", "{}");
		}
		
		if(!KycUtils.nullObj(fnaObj)){
			ObjectMapper fnamapper = new ObjectMapper();
			strFnaJson = fnamapper.writeValueAsString(fnaObj);
			request.setAttribute("CURRENT_FNA_DETAILS", strFnaJson);
		}
		
		mv.setViewName("clientDeclaration");
		request.setAttribute("PREV_SCREEN", "clientSignature");
		request.setAttribute("NEXT_SCREEN", "managerDeclaration");
		request.setAttribute("SIGN_VALIDATION_MSG", param1);
		return mv;
	}

	@RequestMapping("/managerDeclaration")
	public ModelAndView managerDeclaration(HttpServletRequest request) throws JsonProcessingException {
		ModelAndView mv = new ModelAndView();
		
		if(!KycUtils.chkValidDB()){
			mv.setViewName("dbError");
			return mv;
		}
		
		if(!KycUtils.isValidUSession(request)){
			mv.setViewName("sessionExpired");
			return mv;
		}
		
		HttpSession session = request.getSession(false);
		String strFNAId = (String) session.getAttribute("CURRENT_FNAID");
		String param1 = KycUtils.getParamValue(request, "param1");
		Map<String,String> sessMap =(Map<String, String>) session.getAttribute(KycConst.LOGGED_USER_INFO);
		
		FNAService fnaserv = new FNAService();
		
		FnaDetails fnaObj = fnaserv.findFnaLatestDetlsByFnaId(strFNAId);
		
		FnaSelfspouseDets fnaSSObj = fnaserv.findFNASelfSpsDetailsByFnaId(strFNAId);
		
		String   strSSJson = "{}",strFnaJson="{}" ;
		
		if (!KycUtils.nullObj(fnaSSObj) ) {
			ObjectMapper mapper = new ObjectMapper();
			strSSJson = mapper.writeValueAsString(fnaSSObj);
			request.setAttribute("SELFSPOUSE_DETAILS", strSSJson);
		}else{
			request.setAttribute("SELFSPOUSE_DETAILS", "{}");
		}
		
		if(!KycUtils.nullObj(fnaObj)){
			ObjectMapper fnamapper = new ObjectMapper();
			strFnaJson = fnamapper.writeValueAsString(fnaObj);
			request.setAttribute("CURRENT_FNA_DETAILS", strFnaJson);
		}
		
		request.setAttribute(KycConst.LOGGED_USER_ADVSTFNAME,sessMap.get( KycConst.LOGGED_USER_ADVSTFNAME));
		
		mv.setViewName("managerDeclaration");
		request.setAttribute("PREV_SCREEN", "clientDeclaration");
		request.setAttribute("NEXT_SCREEN", "FINISHED");
		request.setAttribute("SIGN_VALIDATION_MSG", param1);
		return mv;
	}
	
	@RequestMapping("/downloadDoc/{i}")
	public ModelAndView managerDeclaration(HttpServletRequest request,HttpServletResponse response,@PathVariable(value="i")String strId) 
			throws  SQLException {
		ModelAndView mv = new ModelAndView();
		
		if(!KycUtils.chkValidDB()){
			mv.setViewName("dbError");
			return mv;
		}
		
		if(!KycUtils.isValidUSession(request)){
			mv.setViewName("sessionExpired");
			return mv;
		}
		FNAService serv = new FNAService();
		String strMsg = serv.downloadDoc(request, response,strId);
		
		if(strMsg.equalsIgnoreCase(KycConst.FILE_NOT_FOUND)){
			
			mv.setViewName(KycConst.FILE_NOT_FOUND);
			return mv;
		}
		
		mv.setViewName("");;
		
		
		return mv;
		
	}
	
	@GetMapping("/Sign{signPerson}")
	public ModelAndView getSignPage(@PathVariable("signPerson") String signPerson,HttpServletRequest request) throws JsonProcessingException, ParseException, org.json.simple.parser.ParseException {
			ModelAndView mv = new ModelAndView();
			
			if(!KycUtils.chkValidDB()){
				mv.setViewName("dbError");
				return mv;
			}
			
			//poovathi add on 15-06-2021
			// decode strSignPerData using BASE64
		        String strSgnPerDataDecode = new String(DatatypeConverter.parseBase64Binary(signPerson));
		        String[] personId = strSgnPerDataDecode.split("=");
	    		String perType = personId[0];
	    		String strFNAId = personId[1];
			
                FNAService fnaService=new FNAService();
		
		FnaSelfspouseDets fnaSSObj = fnaService.findFNASelfSpsDetailsByFnaId(strFNAId);
		
//		FnaSelfspouseDets selfSpouse=fnaService.findSpouseDetlsByDataFormId(dataFormId);
		SimpleDateFormat sm=new SimpleDateFormat("dd-MM-yyyy");
		if (!KycUtils.nullObj(fnaSSObj) ) {
			
			if(perType.equals(KycConst.SIGN_CLIENT)) {
				mv=getSelfData(fnaSSObj,mv,sm);
				
			}else if(perType.equals(KycConst.SIGN_SPS)) {
				
				mv=getSpouseData(fnaSSObj,mv,sm);
				
			}else if(perType.equals(KycConst.SIGN_ADVISOR)) {
				mv=getAdvisorData(fnaSSObj,mv,sm);
			}else {
				mv=getManagerData(fnaSSObj,mv,sm);
			}
			
		}

		
		String curPage=(String)request.getSession().getAttribute("CURRENT_SCREEN");
		mv.addObject("curPage",curPage);
		mv.addObject("perType",perType);
		ObjectMapper mapper = new ObjectMapper();
		String str = mapper.writeValueAsString(fnaSSObj);
//		
		//mv.addObject("PERSON_DETAILS",json);
		//request.setAttribute("PERSON_DETAILS", str);
		mv.setViewName("signature");
		return mv;
	}
		
		private ModelAndView getManagerData(FnaSelfspouseDets selfSpouse,ModelAndView mv, SimpleDateFormat sm) {
			mv.addObject("clientName",selfSpouse.getDfSelfName());
			mv.addObject("nric",selfSpouse.getDfSelfNric());
			if(selfSpouse.getDfSelfDob()!=null) {
			   String dob=sm.format(selfSpouse.getDfSelfDob());
			   mv.addObject("dateOfBirth",dob);
			}
			
			mv.addObject("phoneNo",selfSpouse.getDfSelfMobile());
			mv.addObject("fnaId",selfSpouse.getFnaDetails().getFnaId());
			mv.addObject("singtitle","Manager");
			return mv;
		}

		private ModelAndView getAdvisorData(FnaSelfspouseDets selfSpouse,ModelAndView mv, SimpleDateFormat sm) {
			mv.addObject("clientName",selfSpouse.getDfSelfName());
			mv.addObject("nric",selfSpouse.getDfSelfNric());
			if(selfSpouse.getDfSelfDob()!=null) {
			   String dob=sm.format(selfSpouse.getDfSelfDob());
			   mv.addObject("dateOfBirth",dob);
			}
			
			mv.addObject("phoneNo",selfSpouse.getDfSelfMobile());
			mv.addObject("fnaId",selfSpouse.getFnaDetails().getFnaId());
			mv.addObject("singtitle","Adviser");
			return mv;
		}

		private ModelAndView getSpouseData(FnaSelfspouseDets selfSpouse, ModelAndView mv, SimpleDateFormat sm) {
			mv.addObject("clientName",selfSpouse.getDfSpsName());
			mv.addObject("nric",selfSpouse.getDfSpsNric());
			
			String dob=sm.format(selfSpouse.getDfSpsDob());
			mv.addObject("dateOfBirth",dob);
			
			mv.addObject("phoneNo",selfSpouse.getDfSelfMobile());
			mv.addObject("fnaId",selfSpouse.getFnaDetails().getFnaId());
			mv.addObject("singtitle","Spouse");
			return mv;
			
		}

		private ModelAndView getSelfData(FnaSelfspouseDets selfSpouse, ModelAndView mv, SimpleDateFormat sm) {
			mv.addObject("clientName",selfSpouse.getDfSelfName());
			mv.addObject("nric",selfSpouse.getDfSelfNric());
			if(selfSpouse.getDfSelfDob()!=null) {
			   String dob=sm.format(selfSpouse.getDfSelfDob());
			   mv.addObject("dateOfBirth",dob);
			}
			
			mv.addObject("phoneNo",selfSpouse.getDfSelfMobile());
			mv.addObject("fnaId",selfSpouse.getFnaDetails().getFnaId());
			mv.addObject("singtitle","Client");
			return mv;
		}


		//Create New FNA Form Related function add Poovathi on 04-05-2021
		@RequestMapping("/CreateNewFNA")
		public ModelAndView CreateNewFNAForm(HttpServletRequest request) throws JsonProcessingException {
			//clear all session values
			ModelAndView mv = new ModelAndView();
			HttpSession session = request.getSession();
			session.removeAttribute("KYC_APPROVE_DISTID");
			session.removeAttribute("KYC_APPROVE_ADVID");
//			session.removeAttribute("KYC_APPROVE_USERID");
			session.removeAttribute("KYC_APPROVE_MGRACSFLG");
			session.removeAttribute("KYC_APPROVE_FNAID");
			session.removeAttribute("CREATE_NEW_FNA_FLG");
			session.removeAttribute(KycConst.FNA_SELF_NAME);
			session.removeAttribute(KycConst.FNA_SPOUSE_NAME);
			
			
			//poovathi Commented on 18-06-2021
			//String strFNAId = (String) session.getAttribute("CURRENT_FNAID");
			//session.setAttribute("PREV_FNAID",strFNAId );
			
			session.removeAttribute("CURRENT_FNAID");
			session.removeAttribute("CURRENT_DATAFORM_ID");
			
			 if(!KycUtils.chkValidDB()){
				mv.setViewName("dbError");
				return mv;
			}
			
			if(!KycUtils.isValidUSession(request)){
				mv.setViewName("sessionExpired");
				return mv;
			}

			FNAService ekycServ = new FNAService();
			
			String strCustId = KycUtils.nullObj(session.getAttribute(KycConst.CURRENT_CUSTID)) ? "" : 
				(String) session.getAttribute(KycConst.CURRENT_CUSTID);
			
			//if(KycUtils.nullOrBlank(strFNAId)) {
				List custfnaList = ekycServ.findFNADetailsByCustId(strCustId);
				     if (custfnaList.size() > 0) {
				        FnaDetails fnaDet = (FnaDetails) custfnaList.get(0);
				        String strFNAId = fnaDet.getFnaId();
				        session.removeAttribute("PREV_FNAID");
				        session.setAttribute("PREV_FNAID",strFNAId );
				        
				        List fnaList = new ArrayList();
					      fnaList = ekycServ.findFNADetailsByFnaId(strFNAId);
					      int totalFna = fnaList.size();
					
					String StrFnaLists = "";
					
					if (totalFna > 0) {
					    
						ObjectMapper mapper = new ObjectMapper();
						StrFnaLists = mapper.writeValueAsString(fnaList.get(0));
						request.setAttribute("LATEST_FNA_DETAILS", StrFnaLists);
						 
					}else {
						request.setAttribute("LATEST_FNA_DETAILS", "{}");
					}
				        
	                              }
			//}else {
			   // strFNAId =  session.getAttribute("PREV_FNAID")!= null ? (String) session.getAttribute("PREV_FNAID") :""; 
			//}
			
			
			
			String  strCreateNewFnaFlg = "true";
			session.setAttribute("CREATE_NEW_FNA_FLG",strCreateNewFnaFlg );
			
			mv.setViewName("kycIntro");
			return mv;
		

}
	//poovathi add common fun to decrypt all url param values on 29-06-2021	
	public String decodeURLParam(String UrlEncParam) {
	    String UrlDecParam = new String(DatatypeConverter.parseBase64Binary(UrlEncParam));
	    return UrlDecParam;
	}
  	
}
